const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const terser = require('gulp-terser');
const zip = require('gulp-zip');

var folderName = "Version-Unknown";

function getDateTime() {
    const currDate = new Date();
    const year = currDate.getFullYear();
    const month = currDate.getMonth() + 1; // Note: Month is zero-based, so January is 0
    const day = currDate.getDate();
    const hours = currDate.getHours();
    const minutes = currDate.getMinutes();
    const seconds = currDate.getSeconds();
    return `__${year}-${month}-${day}_${hours}-${minutes}-${seconds}`;
}

const paths = {
    styles: [
        './src/**/style.scss',
        '../assets/css/slick-theme.css',
        '../assets/css/slick.css',
        '../assets/css/rtl.css'
    ],
    scripts: [
        '../assets/js/customSlider.js',
        '../assets/js/flexmenu.js',
        '../assets/js/slick.js',
        '../assets/js/ultp.js'
    ]
};

gulp.task('sass_to_css_frontend_style', function() {
    return gulp.src(paths.styles)
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('../assets/css'))
});

gulp.task('sass_to_css_editor_style', function() {
    return gulp.src(['./src/**/editor.scss'])
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(concat('blocks.editor.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('../assets/css'))
});

// gulp.task('fs_sass_to_css_editor_style', function() {
//     return gulp.src(['./src/**/fs_style.scss'])
//         .pipe(sass())
//         .pipe(autoprefixer({
//             cascade: false
//         }))
//         .pipe(concat('fs.editor.style.css'))
//         .pipe(cleanCSS())
//         .pipe(gulp.dest('../assets/css'))
// });

gulp.task('js_combine_nd_minify', function() {
    return gulp.src(paths.scripts)
        .pipe(concat('ultp.min.js'))
        .pipe(terser())
        .pipe(gulp.dest('../assets/js'))
});

// For dev mode and debugging (skips minification)
gulp.task('js_combine_dev', function() {
    return gulp.src(paths.scripts)
        .pipe(concat('ultp.min.js'))
        .pipe(gulp.dest('../assets/js'))
});


// fires when run build command executes
gulp.task('build', gulp.series(
    'sass_to_css_frontend_style',
    'sass_to_css_editor_style',
    'js_combine_nd_minify'
));

gulp.task('watch', function() {
    gulp.watch(paths.styles, { ignoreInitial: false }, gulp.series('sass_to_css_frontend_style'));
    gulp.watch('src/**/editor.scss', { ignoreInitial: false }, gulp.series('sass_to_css_editor_style'));
    // gulp.watch('src/**/fs_style.scss', gulp.series('fs_sass_to_css_editor_style'));
    // gulp.watch(paths.scripts, gulp.series('js_combine_nd_minify'));
    gulp.watch(paths.scripts, { ignoreInitial: false }, gulp.series('js_combine_dev'));
});

gulp.task('copy_files', function() {
    const filePath = path.join(__dirname, '../ultimate-post.php');
    const date = getDateTime();
    folderName = "Version-Unknown" + date;

    try {
        const data = fs.readFileSync(filePath, 'utf8');
        const versionRegex = /define\('ULTP_VER', '(\d+\.\d+\.\d+)'\);/;
        const match = data.match(versionRegex);
        if (match) {
            folderName = 'V' + match[1] + date;
        } else {
            console.log('Version definition not found.');
        }
    } catch (err) {
        console.error('Error getting version: ', err);
    }

    return gulp
        .src([
            '../**/*', 
            "!../.git",
            "!../wp-cli.phar",
            "!../.gitignore",
            "!../reactjs/**",
            "!../build/**",
            "!../**/*.LICENSE.txt",
        ])
        .pipe(gulp.dest(`../build/${folderName}/ultimate-post/`, {overwrite: true}));
});

gulp.task('zip', function() {
    return gulp.src(`../build/${folderName}/**`)
                // @ts-ignore
                .pipe(zip('ultimate-post.zip')) 
                .pipe(gulp.dest(`../build/${folderName}/`))
});

gulp.task('package', gulp.series(
    'copy_files',
    'zip',
));