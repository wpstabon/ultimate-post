const path = require( 'path' );

var config = {
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' }
			},
			{
				test: /\.(s(a|c)ss)$/,
				use: ['style-loader','css-loader', 'sass-loader']
			}
		],
	},
	plugins: [],
};

var mainExport = Object.assign({}, config, {
	entry: {
		'./assets/js/editor.blocks' : './src/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

var builderCondition = Object.assign({}, config, {
	entry: {
		'./assets/js/conditions' : './src/conditions/index.js'
	},
	output: {
		path: path.join(__dirname, '../addons/builder'),
		filename: '[name].js',
	},
});

var initialSetup = Object.assign({}, config, {
	entry: {
		'./assets/js/ultp_initial_setup_min' : './src/initial_setup/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

var ultpDashboard = Object.assign({}, config, {
	entry: {
		'./assets/js/ultp_dashboard_min' : './src/dashboard/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

// Return Array of Configurations
module.exports = [
	mainExport, builderCondition, initialSetup, ultpDashboard
];