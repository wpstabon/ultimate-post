const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink, imageSize } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'

import { Section, Sections } from '../../helper/Sections'

export default function Edit(props) {
    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { 
			blockId, 
			advanceId, 
			altText, 
			enableCaption,
            currentPostId
		}
	} = props;
	const store = { setAttributes, name, attributes, clientId };
    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-featured-image', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="image" title={__('Image', 'ultimate-post')}  >
                        <CommonSettings
                            title={`inline`}
                                include={[
                                    { 
                                        position: 0, data: { type: 'toggle', key: 'defImgShow', help: __('When both an image and a video exist, prioritize displaying the image', 'ultimate-post'), label: __('Show Image', 'ultimate-post')} 
                                    },
                                    {
                                        position: 5, data: { type:'text',key:'altText', label:__('Image ALT text','ultimate-post') }
                                    },
                                    {
                                        position: 10, data: { type: 'range', key: 'imgWidth', min: 0, max: 1000, step: 1, responsive: true, unit: true, label: __('Image Width', 'ultimate-post') }
                                    },
                                    {
                                        position: 12, data: { type: 'range', key: 'imgHeight', min: 0, max: 1000, step: 1, responsive: true, unit: true, label: __('Image Height', 'ultimate-post') }
                                    },
                                    {
                                        position: 13, data: { type: 'group', key: 'imgScale', justify: true, options: [
                                                { value: 'cover', label: __('Cover', 'ultimate-post') },
                                                { value: 'contain', label: __('Container', 'ultimate-post') },
                                                { value: 'fill', label: __('Fill', 'ultimate-post') },
                                            ], label: __('Image Scale', 'ultimate-post'), step: 1, unit: true, responsive: true
                                        }
                                    },
                                    {
                                        position: 14, data: { type: 'range', key: 'imgRadius', min: 0, max: 1000, step: 1, responsive: true, unit: true, label: __('Image Border Radius', 'ultimate-post') }
                                    },
                                    {
                                        position: 15, data: { type: 'alignment', key: 'imgAlign', responsive: true, label: __('Image Alignment', 'ultimate-post'), options: ['start', 'center', 'end'] }
                                    },
                                    {
                                        position: 16, data: { type:'select',key:'imgCrop', help: 'Image Size Working Only Frontend', label:__('Image Size','ultimate-post'), options: imageSize },
                                    },
                                    { 
                                        position: 17, data: { type: 'toggle', key: 'imgSrcset', label: __('Enable Srcset', 'ultimate-post')} 
                                    },
                            ]} store={store} />
                        <CommonSettings
                            title={__('Enable Dynamic Caption','ultimate-post')}
                            depend="enableCaption"
                                include={[
                                    {
                                        position: 1, data: { type:'color', key:'captionColor', label:__('Caption Color','ultimate-post') }
                                    },
                                    {
                                        position: 2, data: { type:'color', key:'captionHoverColor', label:__('Caption Hover Color','ultimate-post') }
                                    },
                                    {
                                        position: 3, data: { type: 'typography', key:'captionTypo', label: __('Caption Typography', 'ultimate-post') }
                                    },
                                    {
                                        position: 4, data: { type: 'alignment', key: 'captionAlign', disableJustify: true, label: __('Caption Alignment', 'ultimate-post') }
                                    },
                                    {
                                        position: 5, data: { type: 'range', key: 'captionSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Caption Space', 'ultimate-post') }
                                    },

                            ]} store={store} />
                    </Section>
                    <Section slug="video" title={__('Video', 'ultimate-post')}  >
                        <CommonSettings initialOpen={true}
                            title={__('Video Setting','ultimate-post')}
                            include={[ 
                                { 
                                    position: 0, data: { type: 'range', key: 'videoWidth', label: __('Video Width', 'ultimate-post'), min: 0, max: 100, step: 1, responsive: true } 
                                },
                                { 
                                    position: 1, data: { type: 'range', key: 'videoHeight', label: __('Max Video Height', 'ultimate-post'), min: 0, max: 1500, step: 1, unit: true, responsive: true } 
                                },
                                {
                                    position: 2, data: { type: 'alignment', key: 'vidAlign', disableJustify: true, label: __('Video Alignment', 'ultimate-post') }
                                },
                                { 
                                    position: 17, data: { type: 'toggle', key: 'enableVideoCaption', label: __('Video Caption Enable', 'ultimate-post')} 
                                },
                                { 
                                    position: 3, data: { type: 'toggle', key: 'stickyEnable', pro: true, label: __('On Scroll Sticky Enable', 'ultimate-post')} 
                                },
                                { 
                                    position: 4, data: { type: 'range', key: 'stickyWidth', label: __('Sticky Video Width', 'ultimate-post'), min: 0, max: 1500, step: 1, responsive: true } 
                                },
                                { 
                                    position: 6, data: { type: 'select', key: 'stickyPosition', options:[
                                        { label:"Bottom Right", value:"bottomRight" },
                                        { label:"Bottom Left", value:"bottomLeft" },
                                        { label:"Top Right", value:"topRight" }, 
                                        { label:"Top Left", value:"topLeft" }, 
                                    ], label: __('Sticky Video Position', 'ultimate-post')} 
                                },
                                { 
                                    position: 7, data: { type: 'range', key: 'flexiblePosition', label: __('Flexible Sticky Position', 'ultimate-post'), min: 0, max: 500, step: 1, unit: true, responsive: true } 
                                },
                                {
                                    position: 8, data: { type: 'color', key: 'stickyBg', label: __('Background', 'ultimate-post') }
                                },
                                {
                                    position: 9, data: { type: 'border', key: 'stickyBorder', label: __('Border', 'ultimate-post') }
                                },
                                {
                                    position: 10, data: { type:'boxshadow',key:'stickyBoxShadow',label:__('BoxShadow','ultimate-post') }
                                },
                                {
                                    position: 12, data: { type:'dimension',key:'stickyPadding', label:__('Padding','ultimate-post'), step:1, unit:true, responsive:true }
                                },
                                {
                                    position: 13, data: { type:'separator', label:__('Close Button Style','ultimate-post') }
                                },
                                {
                                    position: 14, data: { type:'range', key:'stickyCloseSize', label:__('Sticky Close Size','ultimate-post') }
                                },
                                {
                                    position: 15, data: { type:'color', key:'stickyCloseColor', label:__('Sticky Close Color','ultimate-post') }
                                },
                                {
                                    position: 16, data: { type:'color', key:'stickyCloseBg', label:__('Close Background Color','ultimate-post') }
                                },
                            ]}
                            store={store}/>
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className="ultp-block-wrapper">
                    <div className={`ultp-image-wrapper`}>
                        <div className={`ultp-builder-image`}>
                            <img src={ultp_data.url + 'assets/img/ultp-placeholder.jpg'} alt={altText ? altText : 'Image'} />
                        </div>
                    </div>
                    {
                        enableCaption && 
                        <div className="ultp-featureImg-caption">
                            Dynamic Image Caption
                        </div>
                    }
                </div>
            </div>
        </Fragment>
    )
}