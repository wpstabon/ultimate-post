const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-featured-image', {
        title: __('Post Featured Image/Video','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/featured_img.svg'} alt="Post Feature Image"/>,
        category: 'postx-site-builder',
        description: __('Display the Featured Image/Video and adjust the visual look.','ultimate-post'),
        keywords: [ 
            __('Image','ultimate-post'),
            __('Video','ultimate-post'),
            __('Featured Image','ultimate-post'),
            __('Featured Video','ultimate-post'),
            __('Post Featured Video','ultimate-post'),
            __('Post Featured Image','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)