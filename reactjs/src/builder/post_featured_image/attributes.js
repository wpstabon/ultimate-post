import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  defImgShow:{
    type: "boolean",
    default: false,
  },
  altText: { type: "string", default: "Image" },
  imgWidth: {
    type: "object",
    default: { lg: "", ulg: "px", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper .ultp-builder-image { max-width: {{imgWidth}}; }",
      },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "", ulg: "px", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper .ultp-builder-image img {height: {{imgHeight}}; }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", ulg: "px", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-image img { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  imgScale: {
    type: "string",
    default: "cover",
    style: [
      {
        depends: [{ key: "imgScale", condition: "==", value: "cover" }],
        selector:
          "{{ULTP}} .ultp-block-wrapper .ultp-builder-image img { object-fit: cover }",
      },
      {
        depends: [{ key: "imgScale", condition: "==", value: "contain" }],
        selector:
          "{{ULTP}} .ultp-block-wrapper .ultp-builder-image img { object-fit: contain }",
      },
      {
        depends: [{ key: "imgScale", condition: "==", value: "fill" }],
        selector:
          "{{ULTP}} .ultp-block-wrapper .ultp-builder-image img { object-fit: fill }",
      },
    ],
  },
  imageScale: {
    type: "string",
    default: "cover",
    style: [
      {
        selector: "{{ULTP}} .ultp-builder-video {object-fit: {{imageScale}};}",
      },
    ],
  },
  imgAlign: {
    type: "object",
    default: { lg: "left" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-wrapper:has(.ultp-builder-image) {display: flex; justify-content:{{imgAlign}}; text-align: {{imgAlign}}; } ",
      },
    ],
  },
  imgCrop: {
    type: "string",
    default: "full",
  },
  imgSrcset:{
    type: "boolean",
    default: false,
  },
  /*============================
      Dynamic Captions
  ============================*/
  enableCaption:{
    type:"boolean",
    default:false
  },
  captionColor:{
    type:"string",
    default:"var(--postx_preset_Contrast_2_color)",
    style:[
      {
        selector:"{{ULTP}} .ultp-featureImg-caption { color: {{captionColor}}; }"
      }]
  },
  captionHoverColor:{
      type:"string",
      default:"",
      style:[{
        selector:"{{ULTP}} .ultp-featureImg-caption:hover { color: {{captionHoverColor}}; } "
      }]
  },
  captionTypo:{
      type:"object",
      default:{"openTypography":0,"size":{"lg":"","unit":"px"}},
      style:[{
        selector:"{{ULTP}} .ultp-featureImg-caption"
      }]
  },
  captionAlign:{
    type:"string",
    default:"center",
    style:[{
      selector:"{{ULTP}} .ultp-featureImg-caption { text-align:{{captionAlign}} }"
    }]
  },
  captionSpace:{
    type:"object",
    default:{"lg":"","unit":"px"},
    style:[{
      selector:"{{ULTP}} .ultp-featureImg-caption { margin-top:{{captionSpace}}; }"}]
  },

  /*============================
      Video Settings
  ============================*/
  enableVideoCaption:{
    type:"boolean",
    default:false
  },
  videoWidth: {
    type: "object",
    default: { lg: "100" },
    style: [
      { selector: "{{ULTP}} .ultp-builder-video:has( video ), {{ULTP}} .ultp-embaded-video {width:{{videoWidth}}%;}" },
    ],
  },
  videoHeight: {
    type: "object",
    default: { lg: "", ulg: "px", unit: "px" },
    style: [
      {
        selector: "{{ULTP}} .ultp-builder-video video, {{ULTP}} .ultp-builder-video .ultp-embaded-video {max-height:{{videoHeight}}; height: 100%;}",
      },
    ],
  },
  vidAlign: {
    type: "string",
    default: "",
    style: [
      {
        selector: "{{ULTP}} .ultp-image-wrapper:has( .ultp-embaded-video ) { text-align:{{vidAlign}}; } {{ULTP}} .ultp-image-wrapper:has( .ultp-video-html ) {  display: flex; justify-content:{{vidAlign}}; }",
      },
    ],
  },
  stickyEnable: { type: "boolean", default: false },
  stickyWidth: {
    type: "object",
    default: { lg: "450" },
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active {width:{{stickyWidth}}px !important;}",
      },
    ],
  },
  stickyPosition: {
    type: "string",
    default: "bottomRight",
    style: [
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "bottomRight" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { bottom: 0px; right: 20px; }",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "bottomLeft" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { bottom: 0px; left: 20px; }",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "topRight" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { top: 0px; right: 20px;; }",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "topLeft" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { top: 0px; left: 20px; }",
      },
    ],
  },
  flexiblePosition: {
    type: "object",
    default: { lg: "15", ulg: "px", unit: "px" },
    style: [
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "bottomRight" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { bottom:{{flexiblePosition}}!important;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "center" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active {display: flex; justify-content: center; align-items: center;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "bottomLeft" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { bottom:{{flexiblePosition}} !important;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "topRight" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { top:{{flexiblePosition}} !important;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "topLeft" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { top:{{flexiblePosition}}!important;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "rightMiddle" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { display: flex; justify-content: flex-end; align-items: center;}",
      },
      {
        depends: [
          { key: "stickyEnable", condition: "==", value: true },
          { key: "stickyPosition", condition: "==", value: "leftMiddle" },
        ],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { display: flex; justify-content: flex-start; align-items: center; }",
      },
    ],
  },
  stickyBg: {
    type: "string",
    default: "#000",
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { background:{{stickyBg}} }",
      },
    ],
  },
  stickyBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-sticky-video.ultp-sticky-active",
      },
    ],
  },
  stickyBoxShadow: {
    type: "object",
    default: {
      openShadow: 1,
      width: { top: 0, right: 0, bottom: 24, left: 1 },
      color: "#000000e6",
    },
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-sticky-video.ultp-sticky-active",
      },
    ],
  },
  stickyRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { border-radius:{{stickyRadius}}; }",
      },
    ],
  },
  stickyPadding: {
    type: "object",
    default: { lg: {} },
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active { padding:{{stickyPadding}} !important; }",
      },
    ],
  },
  stickyCloseSize: {
    type: "string",
    default: "47",
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active .ultp-sticky-close { height:{{stickyCloseSize}}px; width:{{stickyCloseSize}}px; } {{ULTP}} .ultp-sticky-video.ultp-sticky-active .ultp-sticky-close::after { font-size: calc({{stickyCloseSize}}px / 2);}",
      },
    ],
  },
  stickyCloseColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-active .ultp-sticky-close { color:{{stickyCloseColor}} }",
      },
    ],
  },
  stickyCloseBg: {
    type: "string",
    default: " rgb(43, 43, 43)",
    style: [
      {
        depends: [{ key: "stickyEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-sticky-video.ultp-sticky-close { background-color:{{stickyCloseBg}} }",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
