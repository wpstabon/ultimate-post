const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import IconPack from '../../helper/fields/tools/IconPack'
import { useBlockId } from '../../helper/hooks/use-blockid'
import ToolBarElement from '../../helper/ToolBarElement'
import Settings from './Settings'

export default function Edit(props) {

    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			headingEnable,
			imageShow,
			titleShow,
			navDivider,
			iconShow,
			dividerBorderShape,
			arrowIconStyle,
			dateShow,
			titlePosition,
			layout,
			prevHeadText,
			nextHeadText,
            currentPostId
		},
	} = props;
    const store = {setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    const content = (arrowLeft, arrowRight, left) => {
        const imageData = <div className={`ultp-nav-img ${iconShow && layout == 'style2'? 'ultp-npb-overlay' : ''  }`}>
                { (iconShow && layout == 'style2') && (left?arrowLeft:arrowRight) }
                { imageShow && <img src={ultp_data.url+'assets/img/ultp-placeholder.jpg'}/>}
            </div>

        return (
            <a className={(left ? `ultp-nav-block-prev ultp-nav-prev-${layout}` : `ultp-nav-block-next ultp-nav-next-${layout}` )} href="#">
                { headingEnable && !titlePosition && ( layout == 'style2' )&& 
                    <div className={`${left ? 'ultp-prev-title' : 'ultp-next-title'}`} >{left ? prevHeadText : nextHeadText}</div>
                } 
                { (left && iconShow && layout != 'style2') && arrowLeft }
                <div className={`ultp-nav-inside`}>
                    { headingEnable && !titlePosition && ( layout != "style2"  ) && <div className={`${left ? 'ultp-prev-title' : 'ultp-next-title'}`} >{left ? prevHeadText : nextHeadText}</div>  } 
                    <div className={`ultp-nav-inside-container`}>
                        { left == true && imageData }
                        { left == false && layout == 'style3' && imageData }
                        <div className={`ultp-nav-text-content`}>
                            { headingEnable && titlePosition && <div className={`${left ? 'ultp-prev-title' : 'ultp-next-title'}`} >{left ? prevHeadText : nextHeadText}</div>  } 
                            { dateShow &&  <div className={`ultp-nav-date`}>{left ? '25 Jan 2022' : '10 Feb 2024' }</div> }
                            { titleShow &&  
                                <div className={`ultp-nav-title`}>{left ? 'Sample Title of the Previous Post' : 'Sample Title of the Next Post'}</div>
                            }
                        </div>  
                        { left == false && layout != 'style3' && <span>{imageData}</span> }
                    </div>
                </div>
                { (left == false && iconShow && layout != 'style2') && arrowRight }
            </a>
        )
    }
     
    const arrowLeft = <span className={`ultp-icon ultp-icon-${arrowIconStyle}`}>{IconPack['left'+arrowIconStyle]}</span>
    const arrowRight = <span className={`ultp-icon ultp-icon-${arrowIconStyle}`}>{IconPack['right'+arrowIconStyle]}</span>
    
    if ( blockId ) { 
        CssGenerator(attributes, 'ultimate-post/next-previous', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>

            <ToolBarElement
                include={[
                    {
                        type: 'layout', key: 'layout', pro: true, tab: true, label: __('Style', 'ultimate-post'), block: 'next-preview', options: [
                            { img: 'assets/img/layouts/builder/next_prev/next_prev_1.png', label: __('Style 1', 'ultimate-post'), value: 'style1' },
                            { img: 'assets/img/layouts/builder/next_prev/next_prev_2.png', label: __('Style 2', 'ultimate-post'), value: 'style2', pro: true },
                            { img: 'assets/img/layouts/builder/next_prev/next_prev_3.png', label: __('Style 3', 'ultimate-post'), value: 'style3', pro: true},
                        ]
                    },
                ]} store={store} />

            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className}`}>
                <div className="ultp-block-wrapper">
                    <div className={`ultp-block-nav${imageShow ? " next-prev-img" : ""}`}>
                        { content(arrowLeft, arrowRight, true) }
                            { (navDivider && dividerBorderShape) && 
                                <span className="ultp-divider"/>
                            }
                        { content(arrowLeft, arrowRight, false) }
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
