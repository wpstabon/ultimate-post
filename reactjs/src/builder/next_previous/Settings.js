const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';

const Settings = ({ store }) => {
    return (
        <Sections>
            <Section slug="setting" title={__('Style', 'ultimate-post')}>
                <CommonSettings initialOpen={true} title={__('General', 'ultimate-post')}
                    include={[
                        {
                            data: {
                                type: 'layout', key: 'layout', pro: true, tab: true, label: __('Style', 'ultimate-post'), block: 'next-preview', options: [
                                    { img: 'assets/img/layouts/builder/next_prev/next_prev_1.png', label: __('Style 1', 'ultimate-post'), value: 'style1' },
                                    { img: 'assets/img/layouts/builder/next_prev/next_prev_2.png', label: __('Style 2', 'ultimate-post'), value: 'style2', pro: true },
                                    { img: 'assets/img/layouts/builder/next_prev/next_prev_3.png', label: __('Style 3', 'ultimate-post'), value: 'style3', pro: true },
                                ]
                            }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={true} title={__('Content', 'ultimate-post')}
                    include={[
                        {
                            data: { type: 'color', key: 'navItemBg', label: __('Background', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'navItemHovBg', label: __('Hover Background', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'navItemPadd', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'dimension', key: 'navItemRad', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'border', key: 'navItemBorder', label: __('Border', 'ultimate-post') }
                        },
                    ]} store={store} />
                <CommonSettings title={__('Label', 'ultimate-post')}
                    depend="headingEnable"
                    include={[
                        {
                            data:
                            {
                                type: 'tab', key: 'navHeadTab', content: [
                                    {
                                        name: 'previous', title: __('Previous', 'ultimate-post'), options: [
                                            { type: 'text', key: 'prevHeadText', label: __('Previous Post Text', 'ultimate-post') },
                                            { type: 'alignment', key: 'prevHeadAlign', disableJustify: true, label: __('Prev Nav Text Align', 'ultimate-post') },
                                            { type: 'alignment', key: 'prevContentAlign', disableJustify: true, label: __('Alignment', 'ultimate-post') },
                                        ]
                                    },
                                    {
                                        name: 'next', title: __('Next', 'ultimate-post'), options: [
                                            { type: 'text', key: 'nextHeadText', label: __('Next Post Text', 'ultimate-post') },
                                            { type: 'alignment', key: 'nextHeadAlign', disableJustify: true, label: __('Next Nav Text Align', 'ultimate-post') },
                                            { type: 'alignment', key: 'nextContentAlign', disableJustify: true, label: __('Alignment', 'ultimate-post') },
                                        ]
                                    },
                                ]
                            }
                        },
                        {
                            data: { type: 'separator' }
                        },
                        {
                            data: { type: 'color', key: 'prevHeadColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'prevHeadHovColor', label: __('Hover Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'prevHeadTypo', label: __('Typography', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'prevHeadingSpace', label: __('Heading Space', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'toggle', key: 'titlePosition', label: __('Align Beside Post Image', 'ultimate-post') }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={false} title={__('Title', 'ultimate-post')}
                    depend="titleShow"
                    include={[
                        {
                            data: { type: 'color', key: 'titleColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'titleHoverColor', label: __('Hover Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'titleTypo', label: __('Typography ', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'titleSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Title Spacing X', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'titleSpaceX', min: 0, max: 300, step: 1, responsive: true, unit: true, label: __('Title Spacing y', 'ultimate-post') }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={false} title={__('Date', 'ultimate-post')}
                    depend="dateShow"
                    include={[
                        {
                            data: { type: 'toggle', key: 'datePosition', label: __('Date Below Title', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'dateColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'dateHoverColor', label: __('Hover Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'dateTypo', label: __('Typography', 'ultimate-post') }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={false} title={__('Image', 'ultimate-post')}
                    depend="imageShow"
                    include={[
                        {
                            data: { type: 'range', key: 'navImgWidth', min: 0, max: 300, step: 1, responsive: true, unit: true, label: __('Width', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'navImgHeight', min: 0, max: 300, step: 1, responsive: true, unit: true, label: __('Height', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'navImgBorderRad', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={false} title={__('Divider', 'ultimate-post')}
                    depend="navDivider"
                    include={[
                        {
                            data: { type: 'color', key: 'dividerColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'dividerSpace', min: 0, max: 200, step: 1, responsive: true, unit: true, label: __('Space', 'ultimate-post') }
                        },
                        {
                            data: { type: 'toggle', key: 'dividerBorderShape', label: __('Dividers Shape', 'ultimate-post') }
                        },
                    ]} store={store} />
                <CommonSettings initialOpen={false} title={__('Navigation', 'ultimate-post')}
                    depend="iconShow"
                    include={[
                        {
                            data: {
                                type: 'select', key: 'arrowIconStyle', label: __('Arrow Icon Style', 'ultimate-post'), options: [
                                    { value: 'Angle', icon: "rightAngle", label: __('Arrow 1', 'ultimate-post') },
                                    { value: 'Angle2', icon: "rightAngle2", label: __('Arrow 2', 'ultimate-post') },
                                    { value: 'ArrowLg', icon: "rightArrowLg", label: __('Arrow 3', 'ultimate-post') },]
                            }
                        },
                        {
                            data: { type: 'color', key: 'arrowColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'arrowHoverColor', label: __('Hover Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', min: 0, max: 80, step: 1, responsive: true, unit: true, key: 'arrowIconSpace', label: __('space', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'arrowIconSize', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Icon Size', 'ultimate-post') }
                        },
                    ]} store={store} />
            </Section>
            <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                <GeneralAdvanced store={store} />
                <ResponsiveAdvanced pro={true} store={store} />
                <CustomCssAdvanced store={store} />
            </Section>
        </Sections>
    );
};

export default Settings;