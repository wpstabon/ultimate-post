const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/next-previous', {
        title: __('Post Next Previous','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/next_previous.svg'} alt="next-preview" />,
        category: 'postx-site-builder',
        description: __('Display Previous and Next Post links with thumbnails.','ultimate-post'),
        keywords: [ 
            __('Next Previous','ultimate-post'),
            __('Post Navigation','ultimate-post'),
            __('Navigation','ultimate-post'),
            __('Next','ultimate-post'),
            __('Previous','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)