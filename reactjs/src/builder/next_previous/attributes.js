import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      Next Preview Settings
  ============================*/
  layout: { type: "string", default: "style1" },
  headingEnable: {
    type: "boolean",
    default: true,
    style: [{ depends: [{ key: "layout", condition: "!=", 0: false }] }],
  },
  imageShow: { type: "boolean", default: true },
  titleShow: { type: "boolean", default: true },
  dateShow: { type: "boolean", default: true },
  navDivider: { type: "boolean", default: false },
  iconShow: { type: "boolean", default: true },
  navItemBg: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-nav-block-prev, 
          {{ULTP}} .ultp-nav-block-next { background:{{navItemBg}}; }`,
      },
    ],
  },
  navItemHovBg: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-nav-block-prev:hover, 
          {{ULTP}} .ultp-nav-block-next:hover { background:{{navItemHovBg}}; }`,
      },
    ],
  },
  navItemPadd: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-nav-block-next, 
          {{ULTP}} .ultp-nav-block-prev { padding:{{navItemPadd}}; }`,
      },
    ],
  },
  navItemRad: {
    type: "object",
    default: { lg: "4", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-nav-block-next, 
          {{ULTP}} .ultp-nav-block-prev { border-radius:{{navItemRad}}; }`,
      },
    ],
  },
  navItemBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#e5e5e5",
      type: "solid",
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-nav-block-next ,{{ULTP}} .ultp-nav-block-prev",
      },
    ],
  },
  /*============================
      Navigation Settings 
  ============================*/
  titlePosition: { type: "boolean", default: true },
  prevContentAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [
          { key: "prevContentAlign", condition: "==", value: "left" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-prev { text-align:{{prevContentAlign}}; justify-content:start;}",
      },
      {
        depends: [
          { key: "prevContentAlign", condition: "==", value: "center" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-prev { text-align:{{prevContentAlign}}; justify-content:center;}",
      },
      {
        depends: [
          { key: "prevContentAlign", condition: "==", value: "right" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-prev { text-align:{{prevContentAlign}}; justify-content:end;}",
      },
    ],
  },
  nextContentAlign: {
    type: "string",
    default: "right",
    style: [
      {
        depends: [
          { key: "nextContentAlign", condition: "==", value: "left" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-next { text-align:{{nextContentAlign}}; justify-content:start;}",
      },
      {
        depends: [
          { key: "nextContentAlign", condition: "==", value: "center" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-next { text-align:{{nextContentAlign}}; justify-content:center;}",
      },
      {
        depends: [
          { key: "nextContentAlign", condition: "==", value: "right" },
          { key: "layout", condition: "!=", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-nav-block-next { text-align:{{nextContentAlign}}; justify-content:end;}",
      },
    ],
  },
  prevHeadingSpace: {
    type: "object",
    default: { lg: "0", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-nav .ultp-prev-title, 
          {{ULTP}} .ultp-block-nav .ultp-next-title { margin:{{prevHeadingSpace}}; }`,
      },
    ],
  },
  prevHeadText: { type: "string", default: "Previous Post" },
  prevHeadColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-nav .ultp-prev-title, 
          {{ULTP}} .ultp-block-nav .ultp-next-title { color:{{prevHeadColor}}; }`,
      },
    ],
  },
  prevHeadHovColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-nav .ultp-prev-title:hover, 
          {{ULTP}} .ultp-block-nav .ultp-next-title:hover { color:{{prevHeadHovColor}}; }`,
      },
    ],
  },
  prevHeadTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 14, unit: "px" },
      height: { lg: 20, unit: "px" },
      transform: "capitalize",
      decoration: "none",
      family: "",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-nav .ultp-prev-title, 
          {{ULTP}} .ultp-block-nav .ultp-next-title`,
      },
    ],
  },
  nextHeadText: { type: "string", default: "Next Post" },
  prevHeadAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [{ key: "titlePosition", condition: "==", value: false }],
        selector: "{{ULTP}} .ultp-prev-title { text-align:{{prevHeadAlign}}; }",
      },
    ],
  },
  nextHeadAlign: {
    type: "string",
    default: "right",
    style: [
      {
        depends: [{ key: "titlePosition", condition: "==", value: false }],
        selector: "{{ULTP}} .ultp-next-title { text-align:{{nextHeadAlign}}; }",
      },
    ],
  },

  /*============================
      Title Settings 
  ============================*/
  titleColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-title { color:{{titleColor}}; }",
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-title:hover { color:{{titleHoverColor}}; }",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 16, unit: "px" },
      height: { lg: 22, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-title",
      },
    ],
  },
  titleSpace: {
    type: "object",
    default: { lg: "0", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content {gap:{{titleSpace}}}",
      },
    ],
  },
  titleSpaceX: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "imageShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-nav-block-next .ultp-nav-text-content {margin-right:{{titleSpaceX}}} 
          {{ULTP}} .ultp-nav-block-prev .ultp-nav-text-content { margin-left:{{titleSpaceX}}}`,
      },
      {
        depends: [
          { key: "imageShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "style3" },
        ],
        selector:
          `{{ULTP}} .ultp-nav-text-content {margin-left:{{titleSpaceX}}} 
          {{ULTP}} .ultp-nav-block-next .ultp-nav-text-content {margin-right:0}`,
      },
    ],
  },

  /*============================
      Date Settings
  ============================*/
  dateColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "dateShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-date { color:{{dateColor}}; }",
      },
    ],
  },
  dateHoverColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "dateShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-date:hover { color:{{dateHoverColor}}; }",
      },
    ],
  },
  dateTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "dateShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-text-content .ultp-nav-date",
      },
    ],
  },
  datePosition: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [{ key: "dateShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-nav-text-content .ultp-nav-date{ order:2; }",
      },
      {
        depends: [
          { key: "dateShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "style3" },
        ],
        selector: "{{ULTP}} .ultp-nav-text-content .ultp-nav-date{ order:0; }",
      },
    ],
  },
  /*============================
      Image Settings
  ============================*/
  navImgWidth: {
    type: "object",
    default: { lg: "75", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-img img{width:{{navImgWidth}}}",
      },
    ],
  },
  navImgHeight: {
    type: "object",
    default: { lg: "75", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-nav-inside .ultp-nav-img img{height:{{navImgHeight}}}",
      },
    ],
  },
  navImgBorderRad: {
    type: "object",
    default: { lg: "4", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-nav-img img { border-radius:{{navImgBorderRad}}; }",
      },
    ],
  },
  dividerColor: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        depends: [{ key: "dividerBorderShape", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-divider {background-color:{{dividerColor}};width:2px;}",
      },
    ],
  },
  dividerSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      { selector: "{{ULTP}} .ultp-block-nav {gap:{{dividerSpace}}}" },
      {
        depends: [{ key: "dividerBorderShape", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-nav {gap:{{dividerSpace}}}",
      },
    ],
  },
  dividerBorderShape: { type: "boolean", default: true },
  arrowIconStyle: { type: "string", default: "Angle2" },
  arrowColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [{ selector: "{{ULTP}} .ultp-icon > svg{ fill:{{arrowColor}}; }" }],
  },
  arrowHoverColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector: "{{ULTP}} .ultp-icon svg:hover { fill:{{arrowHoverColor}}; }",
      },
    ],
  },
  arrowIconSize: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [{ selector: "{{ULTP}} .ultp-icon svg{width:{{arrowIconSize}}}" }],
  },
  arrowIconSpace: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        depends: [{ key: "layout", condition: "!=", value: "style2" }],
        selector:
          `{{ULTP}} .ultp-nav-block-prev .ultp-icon svg{margin-right: {{arrowIconSpace}}} 
          {{ULTP}} .ultp-nav-block-next .ultp-icon svg{margin-left: {{arrowIconSpace}}}`,
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
