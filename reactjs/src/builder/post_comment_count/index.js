const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/post-comment-count', {
        title: __('Post Comment Count','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/comment_count.svg'} alt="Post Comment Count"/>,
        category: 'postx-site-builder',
        description: __('Display Post Comment count and customize it as you need.','ultimate-post'),
        keywords: [ 
            __('Post Comment Count','ultimate-post'),
            __('Post Comment','ultimate-post'),
            __('Comment Count','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)