import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      Post Comment Count Settings
  ============================*/
  commentLabel: { type: "boolean", default: true },
  commentIconShow: { type: "boolean", default: true },
  commentColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      { selector: "{{ULTP}} .ultp-comment-count { color:{{commentColor}} }" },
    ],
  },
  commentTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-comment-count" }],
  },
  commentCountAlign: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-comment-count { justify-content: {{commentCountAlign}};}",
      },
    ],
  },
  /*============================
      Comment Count Label Settings
  ============================*/
  commentLabelText: {
    type: "string",
    default: "comment ",
    style: [
      { depends: [{ key: "commentLabel", condition: "==", value: true }] },
    ],
  },
  commentLabelAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "commentLabelAlign", condition: "==", value: "before" },
          { key: "commentLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-comment-count .ultp-comment-label {order: -1;margin-right: 5px;}",
      },
      {
        depends: [
          { key: "commentLabelAlign", condition: "==", value: "after" },
          { key: "commentLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-comment-count .ultp-comment-label {order: unset; margin-left: 5px;}",
      },
    ],
  },
  /*============================
      Comment Count Icon Settings
  ============================*/
  iconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "commentIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count > svg { fill:{{iconColor}}; stroke:{{iconColor}};}",
      },
    ],
  },
  commentIconStyle: {
    type: "string",
    default: "commentCount1",
    style: [
      { depends: [{ key: "commentIconShow", condition: "==", value: true }] },
    ],
  },
  commentIconSize: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "commentIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count svg{ width:{{commentIconSize}}; height:{{commentIconSize}} }",
      },
    ],
  },
  commentSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "commentIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count > svg { margin-right: {{commentSpace}} }",
      },
      {
        depends: [
          { key: "commentIconShow", condition: "==", value: true },
          { key: "commentLabelAlign", condition: "==", value: "before" },
        ],
        selector:
          "{{ULTP}} .ultp-comment-count > svg { margin: {{commentSpace}} } {{ULTP}} .ultp-comment-count .ultp-comment-label {margin: 0px !important;}",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], [ 'loadingColor'])
};
export default attributes;
