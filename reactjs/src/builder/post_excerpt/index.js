const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-excerpt', {
        title: __('Post Excerpt','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/excerpt.svg'}/>,
        category: 'postx-site-builder',
        description: __('Display Post Excerpt if required and customize as you need.','ultimate-post'),
        keywords: [ 
            __('excerpts','ultimate-post'),
            __('post excerpt','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)