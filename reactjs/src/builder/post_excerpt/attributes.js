import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  excerptColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      { selector: "{{ULTP}} .ultp-builder-excerpt {color:{{excerptColor}}}" },
    ],
  },
  excerptTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-builder-excerpt" }],
  },
  excerptLimit: { type: "string", default: "150" },
  excerptAlignment: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-excerpt {text-align:{{excerptAlignment}}}",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
