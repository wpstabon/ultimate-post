const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'
import { Section, Sections } from '../../helper/Sections'

export default function Edit(props) {
    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, excerptLimit, currentPostId },
	} = props;
	const store = { setAttributes, name, attributes, clientId };
    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-excerpt', blockId);
    }

    let excerptContent = 'Dummy excerpt text sample excerpt text. Dummy excerpt text sample excerpt text.'
    if ( excerptLimit ) {
        excerptContent = excerptContent.split(' ').splice(0, excerptLimit).join(' ')
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Style', 'ultimate-post')}>
                        <CommonSettings initialOpen={true} title={`inline`}
                            include={[
                                {
                                    data: { type: 'color', key: 'excerptColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'excerptTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', min: 0, max: 200, key: 'excerptLimit', label: __('Excerpt Limit(Word)', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'excerptAlignment', responsive: true, label: __('Alignment', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-builder-excerpt`}>
                        {excerptContent}
                    </div>
                </div>
            </div>
        </Fragment>
    );
}