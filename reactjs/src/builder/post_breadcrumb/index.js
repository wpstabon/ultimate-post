const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/post-breadcrumb', {
        title: __('Post Breadcrumb','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/breadcrumb.svg'}/>,
        category: 'postx-site-builder',
        description: __('Display Breadcrumb to let visitors see navigational links.','ultimate-post'),
        keywords: [ 
            __('breadcrumb','ultimate-post'),
            __('Post breadcrumb','ultimate-post'),
            __('breadcrumbs','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)