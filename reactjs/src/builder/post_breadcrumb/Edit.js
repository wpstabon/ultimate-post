const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'
import { Section, Sections } from '../../helper/Sections'

export default function Edit(props) {

    const {
        setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			bcrumbSeparator,
			bcrumbSeparatorIcon,
			bcrumbName,
			bcrumbRootText,
            currentPostId
		},
    } = props;

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });
    const store = { setAttributes, name, attributes, clientId };

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-breadcrumb', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Style', 'ultimate-post')}>
                        <CommonSettings initialOpen={true} title={`inline`}
                            include={[
                                {
                                    data: { type: 'color', key: 'breadcrumbColor', label: __('Text Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'breadcrumbLinkColor', label: __('Link Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'bcrumbLinkHoverColor', label: __('Link Hover Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'bcrumbTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', min: 0, max: 50, key: 'bcrumbSpace', label: __('Space Between Items', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'bcrumbAlign', responsive: true, label: __('Alignment', 'ultimate-post'), options: ['flex-start', 'center', 'flex-end'] }
                                },
                                {
                                    data: { type: 'toggle', key: 'bcrumbName', label: __('Show Single Post Name', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'text', key: 'bcrumbRootText', label: __('Root Page Name', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Separator', 'ultimate-post')}
                            depend="bcrumbSeparator"
                            include={[
                                {
                                    data: {
                                        type: 'select', key: 'bcrumbSeparatorIcon', label: __('Separator', 'ultimate-post'), options: [
                                            { value: 'dot', label: __('Dot', 'ultimate-post') },
                                            { value: 'slash', label: __('Slash', 'ultimate-post') },
                                            { value: 'doubleslash', label: __('Double Slash', 'ultimate-post') },
                                            { value: 'close', label: __('Close', 'ultimate-post') },
                                            { value: 'dash', label: __('Dash', 'ultimate-post') },
                                            { value: 'verticalbar', label: __('Vertical Bar', 'ultimate-post') },
                                            { value: 'greaterThan', label: __('Greater Than', 'ultimate-post') },
                                            { value: 'emptyspace', label: __('Empty', 'ultimate-post') },
                                        ]
                                    },
                                },
                                {
                                    data: { type: 'color', key: 'bcrumbSeparatorColor', label: __('Separator Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', min: 0, max: 50, key: 'bcrumbSeparatorSize', label: __('Separator Size [px]', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <ul className={`ultp-builder-breadcrumb ultp-breadcrumb-${bcrumbSeparatorIcon}`}>
                        <li><a href="#">{bcrumbRootText.length > 0 ? bcrumbRootText : 'Home'}</a></li>
                        { bcrumbSeparator  &&
                            <li className={`ultp-breadcrumb-separator`}></li>
                        }
                        <li><a href="#">Parents</a></li>
                        { bcrumbName &&
                            <>
                                { bcrumbSeparator &&
                                    <li className={`ultp-breadcrumb-separator`}></li>
                                }
                                <li>Current Page</li>
                            </>
                        }
                    </ul>
                </div>
            </div>
        </Fragment>
    )
}
