import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      Breadcrumb Settings
  ============================*/
  bcrumbSeparator: { type: "boolean", default: true },
  breadcrumbColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li {color:{{breadcrumbColor}}}",
      },
    ],
  },
  breadcrumbLinkColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li > a{color:{{breadcrumbLinkColor}}}",
      },
    ],
  },
  bcrumbLinkHoverColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li a:hover {color:{{bcrumbLinkHoverColor}}}",
      },
    ],
  },
  bcrumbTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li , {{ULTP}} .ultp-builder-breadcrumb li a",
      },
    ],
  },
  bcrumbSpace: {
    type: "string",
    default: 12,
    style: [
      {
        selector:
          "{{ULTP}} li:not(.ultp-breadcrumb-separator) {margin: 0 {{bcrumbSpace}}px;}",
      },
    ],
  },
  bcrumbAlign: {
    type: "object",
    default: {},
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb { justify-content:{{bcrumbAlign}}; }",
      },
    ],
  },
  bcrumbName: {
    type: "boolean",
    default: true
  },
  bcrumbRootText: {
    type: "string",
    default: 'Home',
  },
  /*============================
      Separator Settings
  ============================*/
  bcrumbSeparatorIcon: {
    type: "string",
    default: "dash",
    style: [
      { depends: [{ key: "bcrumbSeparator", condition: "==", value: true }] },
    ],
  },
  bcrumbSeparatorColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "bcrumbSeparator", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb .ultp-breadcrumb-separator {color:{{bcrumbSeparatorColor}};}",
      },
      {
        depends: [
          { key: "bcrumbSeparator", condition: "==", value: true },
          { key: "bcrumbSeparatorIcon", condition: "==", value: "dot" },
        ],
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb .ultp-breadcrumb-separator {background:{{bcrumbSeparatorColor}};}",
      },
    ],
  },
  bcrumbSeparatorSize: {
    type: "string",
    default: "",
    style: [
      {
        depends: [
          { key: "bcrumbSeparatorIcon", condition: "==", value: "dot" },
          { key: "bcrumbSeparator", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li.ultp-breadcrumb-separator:after {height:{{bcrumbSeparatorSize}}px; width:{{bcrumbSeparatorSize}}px;}",
      },
      {
        depends: [{ key: "bcrumbSeparator", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-breadcrumb li.ultp-breadcrumb-separator:after {font-size:{{bcrumbSeparatorSize}}px;}",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], [ 'loadingColor'])
};
export default attributes;
