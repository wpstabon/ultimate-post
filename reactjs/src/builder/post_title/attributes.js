import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  titleColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-title, .edit-post-visual-editor {{ULTP}} .ultp-builder-title {color:{{titleColor}};}",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-title { margin:0 } {{ULTP}} .ultp-builder-title , .edit-post-visual-editor {{ULTP}} .ultp-builder-title",
      },
    ],
  },
  titleTag: { type: "string", default: "h1" },
  titleAlign: {
    type: "object",
    default: {},
    style: [
      { selector: "{{ULTP}} .ultp-builder-title {text-align:{{titleAlign}};}" },
    ],
  },

  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
