const { __ } = wp.i18n
import { CommonSettings ,GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'

const Settings = (props) => {
    const {store} = props
    return (
        <Sections>
            <Section slug="setting" title={__('Style','ultimate-post')}>
                <CommonSettings initialOpen={true}
                    title={`inline`}
                    include={[
                        { 
                            data:{ type:'color',key:'titleColor', label:__('Color','ultimate-post') } 
                        },
                        { 
                            data:{ type:'typography',key:'titleTypo', label:__('Typography','ultimate-post') } 
                        },
                        { 
                            data:{ type:'tag',key:'titleTag', label:__('Tag','ultimate-post') } 
                        },
                        {
                            data:{ type:'alignment', key:'titleAlign', disableJustify:false, responsive:true, label:__('Alignment','ultimate-post') } 
                        },
                    ]}
                store={store}/>
            </Section>
            <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                <GeneralAdvanced store={store}/>
                <ResponsiveAdvanced pro={true} store={store}/>
                <CustomCssAdvanced store={store}/>
            </Section>
        </Sections>
    )
}

export default Settings