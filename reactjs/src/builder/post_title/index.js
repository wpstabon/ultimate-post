const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-title', {
        title: __('Post Title','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/post_title.svg'}/>,
        category: 'postx-site-builder',
        description: __('Display the Post Title and customize it as you need.','ultimate-post'),
        keywords: [ 
            __('heading','ultimate-post'),
            __('title','ultimate-post'),
            __('post title','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)