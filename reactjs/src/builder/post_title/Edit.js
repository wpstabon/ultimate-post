const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { blockSupportLink, CustomHtmlTag } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator';
import { useBlockId } from '../../helper/hooks/use-blockid';
import Settings from './Settings';

export default function Edit(props) {
	const {
		setAttributes,
		clientId,
		name,
		attributes,
		className,
		attributes: { 
			blockId, 
			advanceId, 
			titleTag,
            currentPostId
		}
	} = props;
	const store = { setAttributes, name, attributes, clientId };
    
    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-title', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <CustomHtmlTag tag={titleTag} className={`ultp-builder-title`}>Sample Post Title</CustomHtmlTag>
                </div>
            </div>
        </Fragment>
    );
}