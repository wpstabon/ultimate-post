import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  layout: { type: "string", default: "layout1" },
  currentPostId: { type: "string" , default: "" },
/*============================
      Author Box Settings
============================*/
  imgShow: { type: "boolean", default: true },
  writtenByShow: { type: "boolean", default: true },
  authorBioShow: { type: "boolean", default: true },
  metaShow: { type: "boolean", default: true },
  allPostLinkShow: { type: "boolean", default: true },
  authorBoxAlign: {
    type: "object",
    default: "center",
    style: [
      {
        depends: [
          { key: "layout", condition: "!=", value: "layout2" },
          { key: "layout", condition: "!=", value: "layout4" },
        ],
        selector: "{{ULTP}} .ultp-author-box {text-align:{{authorBoxAlign}};}",
      },
    ],
  },

  /*============================
        Container Settings
  ============================*/
  boxContentBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Base_2_color)" },
    style: [{ selector: "{{ULTP}} .ultp-author-box" }],
  },
  boxContentBorder: {
    type: "object",
    default: { openBorder: 0 },
    style: [{ selector: "{{ULTP}} .ultp-author-box" }],
  },
  boxContentRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-author-box { border-radius:{{boxContentRadius}}; }",
      },
    ],
  },
  boxContentPad: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      { selector: "{{ULTP}} .ultp-author-box { padding:{{boxContentPad}}; }" },
    ],
  },

  /*============================
      Author Image Settings
  ============================*/
  imgSize: {
    type: "object",
    default: { lg: "100" },
    style: [{
        depends: [
          { key: "imgShow", condition: "==", value: true }
        ],
        selector: "{{ULTP}} .ultp-author-box img { height:{{imgSize}}px !important; width:{{imgSize}}px !important; }",
      },
    ]
  },
  imgSpace: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout1" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section > img { margin-bottom: {{imgSpace}}; }",
      },
      {
        depends: [
          { key: "authImgStack", condition: "==", value: false },
          { key: "imgShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout2" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section { margin-right: {{imgSpace}}; }",
      },
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout3" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section > img { margin-bottom: {{imgSpace}}; }",
      },
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "authImgStack", condition: "==", value: false },
          { key: "layout", condition: "==", value: "layout4" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section { margin-left: {{imgSpace}}; }",
      },
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "authImgStack", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout2" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section { margin-right: {{imgSpace}}; } @media only screen and (max-width: 600px) { {{ULTP}} .ultp-post-author-image-section { margin-bottom: {{imgSpace}}; margin-right: 0px; }  }",
      },
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "authImgStack", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout4" },
        ],
        selector:
          "{{ULTP}} .ultp-post-author-image-section { margin-left: {{imgSpace}}; } @media only screen and (max-width: 600px) { {{ULTP}} .ultp-post-author-image-section { margin-bottom: {{imgSpace}}; margin-left: 0px; }}", 
      },
    ],
  },
  imgUp: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [
          { key: "imgShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout3" },
        ],
        selector:
          "{{ULTP}} .ultp-author-box-layout3-content .ultp-post-author-image-section > img { margin-top: -{{imgUp}}; } {{ULTP}} .ultp-block-wrapper { margin-top: {{imgUp}}; }",
      },
    ],
  },
  imgBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#000",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "imgShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-image-section > img",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "100", unit: "px" },
    style: [
      {
        depends: [{ key: "imgShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-author-image-section > img { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  authImgStack:{
    type: "boolean",
    default: true,
    style: [
      {
        selector: "@media only screen and (max-width: 600px) { .ultp-author-box-layout2-content {  display: block; text-align: center; } }",
      },
    ],
  },
  imgRatio: {
    type: "string",
    default: '100',
  },
  /*============================
      Written by Settings
  ============================*/
  writtenByText: { type: "string", default: "Written by" },
  writtenByColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "writtenByShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-author-written-by {color:{{writtenByColor}};}",
      },
    ],
  },
  writtenByTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "20", unit: "px" },
      height: { lg: "", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "writtenByShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-written-by",
      },
    ],
  },
  /*============================
      Author Name Settings
  ============================*/
  authorNameTag: { type: "string", default: "h4" },
  authorNameColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-author-name a {color:{{authorNameColor}} !important; }",
      },
    ],
  },
  authorNameHoverColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-author-name a:hover { color:{{authorNameHoverColor}} !important; }",
      },
    ],
  },
  authorNameTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "20", unit: "px" },
      height: { lg: "", unit: "px" },
    },
    style: [{ selector: "{{ULTP}} .ultp-post-author-name" }],
  },
  /*============================
      Author Bio Settings
  ============================*/
  authorBioColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "authorBioShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-author-bio-meta {color:{{authorBioColor}};}",
      },
    ],
  },
  authorBioTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "14", unit: "px" },
      height: { lg: "22", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "authorBioShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-bio",
      },
    ],
  },
  authorBioMargin: {
    type: "object",
    default: { lg: { top: "20", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "authorBioShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-author-bio { margin:{{authorBioMargin}}; }",
      },
    ],
  },
  /*============================
      Meta Setting/Style Settings
  ============================*/
  metaPosition: { type: "string", default: "bottom" },
  metaColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-total-post, {{ULTP}} .ultp-total-comment { color: {{metaColor}}; }",
      },
    ],
  },
  metaTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-meta",
      },
    ],
  },
  metaMargin: {
    type: "object",
    default: { lg: { top: "12", unit: "px" } },
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-meta { margin:{{metaMargin}}; }",
      },
    ],
  },
  metaPadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-author-meta { padding:{{metaPadding}}; }",
      },
    ],
  },
  metaBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-meta { background:{{metaBg}}; }",
      },
    ],
  },
  metaBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: "0", bottom: "0", left: "0" },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-post-author-meta",
      },
    ],
  },

  /*============================
      View all Post Button Settings
  ============================*/
  viewAllPostText: { type: "string", default: "View All Posts" },
  viewAllPostTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "14", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-author-post-link-text",
      },
    ],
  },
  viewAllPostColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link a:not(.wp-block-button__link), {{ULTP}} .ultp-author-post-link-text {color:{{viewAllPostColor}};}",
      },
    ],
  },
  viewAllPostBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-author-post-link-text",
      },
    ],
  },
  viewAllPostRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link-text { border-radius:{{viewAllPostRadius}}; }",
      },
    ],
  },
  viewAllPostHoverColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link .ultp-author-post-link-text:hover { color:{{viewAllPostHoverColor}}; }",
      },
    ],
  },
  viewAllPostBgHoverColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-author-post-link-text:hover",
      },
    ],
  },
  viewAllPostHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link-text:hover { border-radius:{{viewAllPostHoverRadius}}; }",
      },
    ],
  },
  viewAllPostPadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link-text { padding:{{viewAllPostPadding}}; }",
      },
    ],
  },
  viewAllPostMargin: {
    type: "object",
    default: { lg: { top: "15", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "allPostLinkShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-author-post-link { margin:{{viewAllPostMargin}}; }",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
