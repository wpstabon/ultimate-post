const { __ } = wp.i18n
const { Fragment } = wp.element
const { InspectorControls, RichText } = wp.blockEditor
import { blockSupportLink, CustomHtmlTag } from "../../helper/CommonPanel";
import { CssGenerator } from "../../helper/CssGenerator";
import { useBlockId } from "../../helper/hooks/use-blockid";
import ToolBarElement from "../../helper/ToolBarElement";
import Settings from "./Settings";

export default function Edit(props) {
    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			layout,
			imgShow,
			writtenByShow,
			writtenByText,
			authorBioShow,
			metaShow,
			metaPosition,
			allPostLinkShow,
			viewAllPostText,
			authorNameTag,
            currentPostId,
		},
	} = props;
    const store = { setAttributes, name, attributes, clientId }

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    const image_section = <div className={'ultp-post-author-image-section ultp-post-author-image-editor'}>
        <img src={ultp_data.url + `assets/img/ultp-author.jpg`} className={`ultp-post-author-image`} alt='author_image' />
    </div>

    if ( blockId ) { 
        CssGenerator(attributes, 'ultimate-post/author-box', blockId); 
    }

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>
            
            <ToolBarElement
                include={[
                    { 
                        type: 'layout', block: 'post-author', key: 'layout', label: __('Layout', 'ultimate-post'), options: [
                            { img: 'assets/img/layouts/builder/auth_box/auth_box1.png', label: 'Layout 1', value: 'layout1' },
                            { img: 'assets/img/layouts/builder/auth_box/auth_box2.png', label: 'Layout 2', value: 'layout2', pro: true },
                            { img: 'assets/img/layouts/builder/auth_box/auth_box4.png', label: 'Layout 3', value: 'layout3', pro: true },
                            { img: 'assets/img/layouts/builder/auth_box/auth_box3.png', label: 'Layout 4', value: 'layout4', pro: true }
                        ]
                    }
                ]} store={store} />

            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-author-box ultp-author-box-` + layout + `-content`}>
                        {imgShow && layout !== 'layout4' &&
                            image_section
                        }
                        <div className={'ultp-post-author-details'}>
                            <div className={`ultp-post-author-title`}>
                                {writtenByShow &&
                                    <RichText
                                        key="editable"
                                        tagName={'span'}
                                        className={'ultp-post-author-written-by'}
                                        keepPlaceholderOnFocus
                                        placeholder={__('Change Text...', 'ultimate-post')}
                                        onChange={value => setAttributes({ writtenByText: value })}
                                        value={writtenByText}
                                    />
                                }
                                <CustomHtmlTag tag={authorNameTag} className={'ultp-post-author-name'}><a href="#">Author Name</a></CustomHtmlTag>
                            </div>
                            {metaShow && (metaPosition == 'top') &&
                                <div className={`ultp-post-author-meta`}>
                                    <span className={`ultp-total-post`}>72 Posts</span>
                                    <span className={`ultp-total-comment`}>32 Comments</span>
                                </div>
                            }
                            {authorBioShow &&
                                <div className={`ultp-post-author-bio`}>
                                    <span className={'ultp-post-author-bio-meta'}>
                                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable
                                    </span>
                                </div>
                            }
                            {metaShow && (metaPosition == 'bottom') &&
                                <div className={`ultp-post-author-meta`}>
                                    <span className={`ultp-total-post`}>72 Posts</span>
                                    <span className={`ultp-total-comment`}>32 Comments</span>
                                </div>
                            }
                            {allPostLinkShow &&
                                <div className={`ultp-author-post-link`}>
                                    <a className={`ultp-author-post-link-text`} href="#">{viewAllPostText}</a>
                                </div>
                            }
                        </div>
                        {imgShow && layout === 'layout4' &&
                            image_section
                        }
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
