const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/author-box', {
        title: __('Post Author Box', 'ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/author_box.svg'} alt="Post Author"/>,
        category: 'postx-site-builder',
        description: __('Display Post Author details and customize them as you need.', 'ultimate-post'),
        keywords: [
            __('Post Author Box', 'ultimate-post'),
            __('Author', 'ultimate-post'),
            __('Blog Post Author', 'ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)