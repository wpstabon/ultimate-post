import React from 'react';
const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';

const Settings = ({ store }) => {

    return (
        <Sections>
            <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                <CommonSettings
                    title={`inline`}
                    include={[
                        {
                            position: 0, data: {
                                type: 'layout', block: 'post-author', key: 'layout', label: __('Layout', 'ultimate-post'), options: [
                                    { img: 'assets/img/layouts/builder/auth_box/auth_box1.png', label: 'Layout 1', value: 'layout1' },
                                    { img: 'assets/img/layouts/builder/auth_box/auth_box2.png', label: 'Layout 2', value: 'layout2', pro: true },
                                    { img: 'assets/img/layouts/builder/auth_box/auth_box4.png', label: 'Layout 3', value: 'layout3', pro: true },
                                    { img: 'assets/img/layouts/builder/auth_box/auth_box3.png', label: 'Layout 4', value: 'layout4', pro: true },
                                ]
                            }
                        },
                        {
                            data: { type: 'alignment', key: 'authorBoxAlign', disableJustify: true, label: __('Alignment', 'ultimate-post') }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Content Style', 'ultimate-post')}
                    include={[
                        {
                            data: { type: 'color2', key: 'boxContentBg', label: __('Content Background', 'ultimate-post') }
                        },
                        {
                            data: { type: 'border', key: 'boxContentBorder', label: __('Border', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'boxContentRadius', label: __('Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'dimension', key: 'boxContentPad', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Author Image', 'ultimate-post')}
                    depend="imgShow"
                    include={[
                        {
                            data: { type: 'range', key: 'imgSize', min: 0, max: 400, step: 1, responsive: true ,label: __('Size', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'imgSpace', min: 0, max: 250, step: 1, responsive: true, unit: ['px', 'em', 'rem', '%'], label: __('Space', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'imgUp', min: 0, max: 250, step: 1, responsive: true, unit: ['px', 'em', 'rem', '%'], label: __('Image Top Position', 'ultimate-post') }
                        },
                        {
                            data: { type: 'border', key: 'imgBorder', label: __('Border', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'imgRadius', label: __('Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'toggle', key: 'authImgStack', label: __('Stack on Mobile', 'ultimate-post') }
                        },
                        {
                            data: { type: 'group', key: 'imgRatio', justify: true, options:[
                                { value:'100',label:__('Low', 'ultimate-post') },
                                { value:'200',label:__('Medium', 'ultimate-post') },
                                { value:'300',label:__('Heigh', 'ultimate-post') }
                            ], label: __('Image Ratio', 'ultimate-post') }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Written By Label', 'ultimate-post')}
                    depend="writtenByShow"
                    include={[
                        {
                            data: {
                                type: 'text', key: 'writtenByText', label: __('Text', 'ultimate-post')
                            },
                        },
                        {
                            data: { type: 'color', key: 'writtenByColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'writtenByTypo', label: __('Typography', 'ultimate-post') }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Author Name', 'ultimate-post')}
                    include={[
                        {
                            data: { type: 'tag', key: 'authorNameTag', label: __('Tag', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'authorNameColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'authorNameHoverColor', label: __('Hover Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'authorNameTypo', label: __('Typography', 'ultimate-post') }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Author Bio', 'ultimate-post')}
                    depend="authorBioShow"
                    include={[
                        {
                            data: { type: 'color', key: 'authorBioColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'authorBioTypo', label: __('Typography', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'authorBioMargin', label: __('Margin', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('Meta', 'ultimate-post')}
                    depend="metaShow"
                    include={[
                        {
                            data:
                            {
                                type: 'group', key: 'metaPosition', label: __('Meta Position', 'ultimate-post'), justify: true, options: [
                                    { value: 'top', label: __('Top', 'ultimate-post') },
                                    { value: 'bottom', label: __('Bottom', 'ultimate-post') }
                                ]

                            },

                        },
                        {
                            data: { type: 'color', key: 'metaColor', label: __('Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'metaTypo', label: __('Typography', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'metaBg', label: __('Background', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'metaPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'dimension', key: 'metaMargin', label: __('Margin', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'border', key: 'metaBorder', label: __('Border', 'ultimate-post') }
                        },
                    ]}
                    store={store} />
                <CommonSettings
                    title={__('View All Post Link', 'ultimate-post')}
                    depend="allPostLinkShow"
                    include={[
                        {
                            data: { type: 'text', key: 'viewAllPostText', label: __('Text', 'ultimate-post') },
                        },
                        {
                            data: { type: 'typography', key: 'viewAllPostTypo', label: __('Typography', 'ultimate-post') }
                        },
                        {
                            data: {
                                type: 'tab', content: [
                                    {
                                        name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                            { type: 'color', key: 'viewAllPostColor', label: __('Color', 'ultimate-post') },
                                            { type: 'color2', key: 'viewAllPostBg', label: __('Background Color', 'ultimate-post') },
                                            { type: 'dimension', key: 'viewAllPostRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                        ]
                                    },
                                    {
                                        name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                            { type: 'color', key: 'viewAllPostHoverColor', label: __('Hover Color', 'ultimate-post') },
                                            { type: 'color2', key: 'viewAllPostBgHoverColor', label: __('Hover Bg Color', 'ultimate-post') },
                                            { type: 'dimension', key: 'viewAllPostHoverRadius', label: __('Hover Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                        ]
                                    },
                                ]
                            }
                        },
                        {
                            data: { type: 'separator' }
                        },
                        {
                            data: { type: 'dimension', key: 'viewAllPostPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'dimension', key: 'viewAllPostMargin', label: __('Margin', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                    ]}
                    store={store} />
            </Section>
            <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                <GeneralAdvanced store={store} />
                <ResponsiveAdvanced pro={true} store={store} />
                <CustomCssAdvanced store={store} />
            </Section>
        </Sections>
    );
};

export default Settings;