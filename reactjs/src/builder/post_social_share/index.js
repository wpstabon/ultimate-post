const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-social-share', {
        title: __('Post Social Share', 'ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/share.svg'} alt="Post Share"/>,
        category: 'postx-site-builder',
        description: __('Display Social Share Buttons to let visitors share posts to their social profiles.', 'ultimate-post'),
        keywords: [
            __('Post Share', 'ultimate-post'),
            __('Social Share', 'ultimate-post'),
            __('Social Media', 'ultimate-post'),
            __('Share', 'ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null
        },
    }
)