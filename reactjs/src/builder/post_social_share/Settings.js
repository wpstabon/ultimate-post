import React from 'react';
const { __ } = wp.i18n
import { CommonSettings, GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'

const Settings = ({ store }) => {
    return (
        <Sections>
            <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                <CommonSettings
                    title={__('General', 'ultimate-post')}
                    store={store}
                    initialOpen={true}
                    include={[    
                        { position: 1, data: {
                            type: "repetable", 
                            key: "repetableField", 
                            label: __('Social Share Style', 'ultimate-post'),
                            fields: [
                                { type:'select',key:'type',label:__('Social Media','ultimate-post'), options:[
                                    {value:'facebook',label:__('Facebook','ultimate-post')},
                                    {value:'twitter',label:__('Twitter','ultimate-post')},
                                    {value:'messenger',label:__('Messenger','ultimate-post')},
                                    {value:'linkedin',label:__('Linkedin','ultimate-post')},
                                    {value:'reddit',label:__('Reddit','ultimate-post')},
                                    {value:'mail',label:__('Mail','ultimate-post')},
                                    {value:'whatsapp',label:__('WhatsApp','ultimate-post')},
                                    {value:'skype',label:__('Skype','ultimate-post')},
                                    {value:'pinterest',label:__('Pinterest','ultimate-post')},
                                ] },
                                { 
                                    type: 'toggle', key: 'enableLabel', label: __('Label Enable', 'ultimate-post') 
                                },
                                { 
                                    type: 'text', key: 'label', label: __('Label', 'ultimate-post') 
                                },
                                { 
                                    type: 'color', key: 'iconColor', label: __('Color', 'ultimate-post') 
                                },
                                { 
                                    type: 'color', key: 'iconColorHover', label: __('Color Hover', 'ultimate-post') 
                                },
                                { 
                                    type: 'color', key: 'shareBg', label: __('Background', 'ultimate-post') 
                                },
                                { 
                                    type: 'color', key: 'shareBgHover', label: __('Hover Background', 'ultimate-post') 
                                },
                            ]}
                        },
                        {
                            position: 2, data: { type: 'separator', label: __('Common Style', 'ultimate-post') }
                        },
                        {
                            position: 2,  data: { type:'tab',  content:[
                                { name:'normal', title:__('Normal','ultimate-post'), options:[
                                    { type:'color',key:'shareColor',label:__('Color','ultimate-post') },
                                    { type:'color',key:'shareCommonBg',label:__('Background','ultimate-post') },
                                ]},
                                { name:'hover', title:__('Hover','ultimate-post'), options:[
                                    { type:'color',key:'shareHoverColor',label:__('Hover Color','ultimate-post') },
                                    { type:'color',key:'shareHoverBg',label:__('Hover Background','ultimate-post') },
                                ]},
                            ]},
                        },
                        {
                            position: 3, data: { type: 'typography', key: 'shareItemTypo', label: __('Typography', 'ultimate-post') } 
                        }
                    ]}
                />
                <CommonSettings
                    title={__('Item Setting', 'ultimate-post')}
                    include={[
                        {
                            data: { type: 'toggle', key: 'disInline', label: __('Item Inline', 'ultimate-post') }
                        },
                        {
                            data: { type: 'range', key: 'shareIconSize', min: 0, max: 150, unit: false, responsive: true, step: 1, label: __('Icon Size', 'ultimate-post')}
                        },
                        {
                            data: { type: 'border', key: 'shareBorder', label: __('Border', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'shareRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'dimension', key: 'itemPadding', min: 0, max: 80, step: 1, unit: true, responsive: true, label: __('Item Padding', 'ultimate-post') }
                        },
                        {
                            data: { type: 'dimension', key: 'itemSpacing', min: 0, max: 80, step: 1, unit: true, responsive: true, label: __('Spacing', 'ultimate-post') }
                        },
                        { 
                            data: { type: 'group', key: 'itemContentAlign', options:[
                                    {  label:"Left", value: "flex-start"  },
                                    {  label:"Center", value: "center" },
                                    {  label:"Right", value: "flex-end" },
                                ], justify: true, label: __('Content Alignment' ,'ultimate-post') 
                            }
                        },
                        { 
                            data: { type: 'alignment', key: 'itemAlign', responsive: false, label: __('Alignment', 'ultimate-post'), disableJustify: true } 
                        },
                    ]}
                    store={store}
                />
                <CommonSettings
                    title={__('Share Label & Count', 'ultimate-post')}
                    include={[
                        { data: { type: 'toggle', key: 'shareLabelShow', label: __('Show Share label & Count', 'ultimate-post') } },
                        {
                            data: {
                                type: 'select', key: 'shareLabelStyle', label: __('Share Label Style', 'ultimate-post'), options: [
                                    {
                                        value: 'style1', label: __('Style 1', 'ultimate-post')
                                    },
                                    {
                                        value: 'style2', label: __('Style 2', 'ultimate-post')
                                    },
                                    {
                                        value: 'style3', label: __('Style 3', 'ultimate-post')
                                    },
                                    {
                                        value: 'style4', label: __('Style 4', 'ultimate-post')
                                    },
                                ]
                            }
                        },
                        {
                            data: { type: 'range', key: 'labelIconSize', min: 0, max: 100, unit: false, responsive: true, step: 1, label: __('Label Icon Size', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'dimension', key: 'labelIconSpace', label: __('Space', 'ultimate-post'), step: 1, unit: true, responsive: true } 
                        },
                        {
                            data: { type: 'color', key: 'shareLabelIconColor', label: __('Icon Color', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'color', key: 'Labels1BorderColor', label: __('Border Color', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'color', key: 'shareLabelBackground', label: __('Background Color', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'border', key: 'shareLabelBorder', label: __('Border', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'dimension', key: 'shareLabelRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true } 
                        },
                        {
                            data: { type: 'dimension', key: 'shareLabelPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true } 
                        },
                        {
                            data: { type: 'toggle', key: 'shareCountShow', label: __('Share Count', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'color', key: 'shareCountColor', label: __('Count Color', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'typography', key: 'shareCountTypo', label: __('Count Typography', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'text', key: 'shareCountLabel', label: __('Share Count Label', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'color', key: 'shareLabelColor', label: __('Label Color', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'typography', key: 'shareLabelTypo', label: __('Label Typography', 'ultimate-post') } 
                        },
                    ]}
                    store={store}
                />
                <CommonSettings
                    title={__('Sticky Position', 'ultimate-post')}
                    depend="enableSticky"
                    include={[
                        {
                            data: {
                                type: 'select', key: 'itemPosition', label: __('Display Style', 'ultimate-post'), options: [
                                    {
                                        value: 'left', label: __('Left', 'ultimate-post')
                                    },
                                    {
                                        value: 'right', label: __('right', 'ultimate-post')
                                    },
                                    {
                                        value: 'bottom', label: __('bottom', 'ultimate-post')
                                    },
                                ]
                            },
                        },
                        {
                            data: { type: 'range', key: 'stickyLeftOffset', min: 0, max: 1500, unit: false, responsive: false, step: 1, label: __('Left Offset', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'range', key: 'stickyRightOffset', min: 0, max: 1500, unit: false, responsive: false, step: 1, label: __('Right Offset', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'range', key: 'stickyTopOffset', min: 0, max: 1500, unit: false, responsive: false, step: 1, label: __('Top Offset', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'range', key: 'stickyBottomOffset', min: 0, max: 1500, unit: false, responsive: false, step: 1, label: __('Bottom Offset', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'toggle', key: 'resStickyPost', label: __('Disable Sticky ( Responsive )', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'range', key: 'floatingResponsive', min: 0, max: 1200, unit: false, responsive: false, step: 1, label: __('Horizontal floating responsiveness', 'ultimate-post') } 
                        },
                        {
                            data: { type: 'toggle', key: 'stopSticky', label: __('Disable Sticky ( when footer is visible )', 'ultimate-post') } 
                        }
                    ]}
                    store={store}
                />
            </Section>
            <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                <GeneralAdvanced store={store} />
                <ResponsiveAdvanced pro={true} store={store} />
                <CustomCssAdvanced store={store} />
            </Section>
        </Sections>
    );
};

export default Settings;