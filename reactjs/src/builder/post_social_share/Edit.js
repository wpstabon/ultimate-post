const { __ } = wp.i18n
const { Fragment } = wp.element
const { InspectorControls } = wp.blockEditor
import { blockSupportLink } from "../../helper/CommonPanel";
import { CssGenerator } from "../../helper/CssGenerator";
import IconPack from "../../helper/fields/tools/IconPack";
import { useBlockId } from "../../helper/hooks/use-blockid";
import Settings from "./Settings";

export default function Edit(props) {

    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			disInline,
			shareLabelShow,
			shareCountLabel,
			shareCountShow,
			repetableField,
			shareLabelStyle,
            currentPostId
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) { 
        CssGenerator(attributes, 'ultimate-post/post-social-share', blockId); 
    }

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-post-share`}>
                        <div className={`ultp-post-share-layout ultp-inline-${disInline}`}>
                            { shareLabelShow &&
                                <div className={`ultp-post-share-count-section ultp-post-share-count-section-` + shareLabelStyle}>
                                    { (shareLabelStyle != "style2" && shareCountShow) &&
                                        <span className={`ultp-post-share-count`}>350</span>
                                    }
                                    { (shareLabelStyle == "style2") &&
                                        <span className={`ultp-post-share-icon-section`}>
                                            {IconPack.share}
                                        </span>
                                    }
                                    { (shareLabelStyle != "style2" && shareCountLabel) &&
                                        <span className={`ultp-post-share-label`}>{shareCountLabel}</span>
                                    }
                                </div>
                            }
                            <div className={`ultp-post-share-item-inner-block`}>
                                { repetableField.map((content, i) => {
                                    return (
                                        <div key={i} className={`ultp-post-share-item ultp-repeat-${i} ultp-social-${content.type}`}>
                                            <a href="#" className={`ultp-post-share-item-${content.type}`}>
                                                <span className={`ultp-post-share-item-icon`}>
                                                    {IconPack[content.type]}
                                                </span>
                                                {
                                                    content.enableLabel && 
                                                    <span className={`ultp-post-share-item-label`}>
                                                        { content.label }
                                                    </span>
                                                }
                                                
                                            </a>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}