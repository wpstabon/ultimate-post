import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  /*============================
      General Settings
  ============================*/
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  shareColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [{ 
      selector: 
      `{{ULTP}} .ultp-post-share-item-icon svg { fill:{{shareColor}}; }
      {{ULTP}}  .ultp-post-share-item-label { color:{{shareColor}} }`,
    }],
  },
  shareCommonBg: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style:[{
      selector: "{{ULTP}} .ultp-post-share-item a { background-color: {{shareCommonBg}}; }",
    }],
  },
  shareHoverColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [{
        selector: 
        `{{ULTP}} .ultp-post-share-item:hover .ultp-post-share-item-icon svg { fill:{{shareHoverColor}}; }
        {{ULTP}} .ultp-post-share-item:hover .ultp-post-share-item-label{ color:{{shareHoverColor}} }`,
      }],
  },
  shareHoverBg: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style:[{
      selector: "{{ULTP}} .ultp-post-share-item a:hover { background-color:{{shareHoverBg}}; } ",
    }],
  },
  repetableField: {
    type: "array",
    fields: {
      type: { type: "string", default: "facebook" },
      enableLabel: { type: "boolean", default: true },
      label: { type: "string", default: "Share" },
      iconColor: {
        type: "string",
        default: "#fff",
        style: [
          {
            selector:
              `{{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item a .ultp-post-share-item-icon svg { fill:{{iconColor}} !important; }
              {{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item .ultp-post-share-item-label { color:{{iconColor}} }`,
          },
        ],
      },
      iconColorHover: {
        type: "string",
        default: "#d2d2d2",
        style: [
          {
            selector:
              `{{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item:hover .ultp-post-share-item-icon svg { fill:{{iconColorHover}} !important; }
              {{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item:hover .ultp-post-share-item-label{ color:{{iconColorHover}} }`,
          },
        ],
      },
      shareBg: {
        type: "string",
        default: "#7a49ff",
        style: [
          {
            selector:
              "{{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item a { background-color: {{shareBg}}; }",
          },
        ],
      },
      shareBgHover: {
        type: "object",
        default: "#7a49ff",
        style: [
          {
            selector:
              "{{ULTP}} {{REPEAT_CLASS}}.ultp-post-share-item a:hover { background-color:{{shareBgHover}}; }",
          },
        ],
      },
    },
    default: [
      {
        type: "facebook",
        enableLabel: true,
        label: "Facebook",
        iconColor: "#fff",
        iconColorHover: "#d2d2d2",
        shareBg: "#4267B2",
        bgHoverColor: "#f5f5f5",
      },
      {
        type: "twitter",
        enableLabel: true,
        label: "Twitter",
        iconColor: "#fff",
        iconColorHover: "#d2d2d2",
        shareBg: "#1DA1F2",
        bgHoverColor: "#f5f5f5",
      },
      {
        type: "pinterest",
        enableLabel: true,
        label: "Pinterest",
        iconColor: "#fff",
        iconColorHover: "#d2d2d2",
        shareBg: "#E60023",
        bgHoverColor: "#f5f5f5",
      },
      {
        type: "linkedin",
        enableLabel: true,
        label: "Linkedin",
        iconColor: "#fff",
        iconColorHover: "#d2d2d2",
        shareBg: "#0A66C2",
        bgHoverColor: "#f5f5f5",
      },
      {
        type: "mail",
        enableLabel: true,
        label: "Mail",
        iconColor: "#fff",
        iconColorHover: "#d2d2d2",
        shareBg: "#EA4335",
        bgHoverColor: "#f5f5f5",
      },
    ],
  },
  /*============================
      Item Settings
  ============================*/
  disInline: { type: "boolean", default: true },
  shareItemTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "18", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-item a .ultp-post-share-item-label",
      },
    ],
  },
  shareIconSize: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-item .ultp-post-share-item-icon svg { height:{{shareIconSize}} !important; width:{{shareIconSize}} !important;}",
      },
    ],
  },
  shareBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#c3c3c3",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-post-share-item a" }],
  },
  shareRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-item a { border-radius:{{shareRadius}}; }",
      },
    ],
  },
  itemPadding: {
    type: "object",
    default: {
      lg: { top: "15", bottom: "15", left: "15", right: "15", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-item-inner-block .ultp-post-share-item a { padding:{{itemPadding}} !important; }",
      },
    ],
  },
  itemSpacing: {
    type: "object",
    default: {
      lg: { top: "5", bottom: "5", left: "5", right: "5", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-item-inner-block .ultp-post-share-item {margin:{{itemSpacing}} !important; }",
      },
    ],
  },
  itemContentAlign: {
    type: "string",
    default: "flex-start",
    style: [
      {
        depends: [
          { key: "disInline", condition: "==", value: true },
          { key: "enableSticky", condition: "==", value: false },
        ],
        selector: "{{ULTP}} .ultp-post-share-layout {display: flex; align-items: center; justify-content:{{itemContentAlign}}; width: 100%;} ",
      },
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style3" },
        ],
        selector: "{{ULTP}} .ultp-post-share-layout {display: flex; justify-content: center; align-items:{{itemContentAlign}}; width: 100%;} ",
      },
    ],
  },
  itemAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [
          { key: "disInline", condition: "==", value: false },
          { key: "enableSticky", condition: "==", value: false },
          { key: "itemAlign", condition: "==", value: 'left' },
        ],
        selector: `{{ULTP}} .ultp-post-share { text-align:{{itemAlign}}; } .rtl
        {{ULTP}} .ultp-post-share { text-align: right !important; }`,
      },
      {
        depends: [
          { key: "disInline", condition: "==", value: false },
          { key: "enableSticky", condition: "==", value: false },
          { key: "itemAlign", condition: "==", value: 'right' },
        ],
        selector: `{{ULTP}} .ultp-post-share { text-align:{{itemAlign}}; } .rtl
        {{ULTP}} .ultp-post-share { text-align: left !important;}`,
      },
      {
        depends: [
          { key: "disInline", condition: "==", value: false },
          { key: "enableSticky", condition: "==", value: false },
          { key: "itemAlign", condition: "==", value: 'center' },
        ],
        selector: "{{ULTP}} .ultp-post-share { text-align:{{itemAlign}}; }",
      },
    ],
  },
  /*============================
      Share Label & Count
  ============================*/
  shareLabelShow: { type: "boolean", default: true },
  labelIconSize: {
    type: "object",
    default: { lg: "24" },
    style: [
      {
        depends: [{ key: "shareLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-share-icon-section svg { height: {{labelIconSize}}px; width: {{labelIconSize}}px; }",
      },
    ],
  },
  labelIconSpace: {
    type: "object",
    default: {
      lg: { top: "0", bottom: "0", left: "0", right: "15", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-share-count-section { margin:{{labelIconSpace}} }",
      },
    ],
  },
  shareLabelIconColor: {
    type: "string",
    default: "#002dff",
    style: [
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-icon-section svg{ fill:{{shareLabelIconColor}}; }",
      },
    ],
  },
  shareLabelStyle: {
    type: "string",
    default: "style1",
    style: [
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style1" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-layout{display: flex; align-items:center;}",
      },
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style2" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-layout{display: flex; align-items:center;}",
      },
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style3" },
        ],
        selector: "{{ULTP}} .ultp-post-share .ultp-post-share-layout{display: flex; flex-direction: column}",
      },
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style4" },
        ],
        selector: "{{ULTP}} .ultp-post-share-layout{display: flex}",
      },
    ],
  },
  shareCountShow: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style1" },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
      },
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style3" },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
      },
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style4" },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
      },
    ],
  },
  Labels1BorderColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "==", value: "style1" },
        ],
        selector:
          `{{ULTP}} .ultp-post-share-count-section-style1,
          {{ULTP}} .ultp-post-share-count-section-style1:after {border-color:{{Labels1BorderColor}} !important; }`,
      },
    ],
  },
  shareCountColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style1" },
          { key: "shareCountShow", condition: "==", value: true },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-count {color:{{shareCountColor}} !important; }",
      },
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style3" },
          { key: "shareCountShow", condition: "==", value: true },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-count {color:{{shareCountColor}} !important; }",
      },
    ],
  },
  shareCountTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "14", unit: "px" },
      height: { lg: "20", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style1" },
          { key: "shareCountShow", condition: "==", value: true },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-post-share-count",
      },
      {
        depends: [
          { key: "shareLabelStyle", condition: "==", value: "style3" },
          { key: "shareCountShow", condition: "==", value: true },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-post-share-count",
      },
    ],
  },
  shareCountLabel: {
    type: "string",
    default: "Shares",
    style: [
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "!=", value: "style2" },
        ],
      },
    ],
  },
  shareLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [
          { key: "shareLabelStyle", condition: "!=", value: "style2" },
          { key: "shareCountLabel", condition: "!=", value: "" },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-post-share-count-section .ultp-post-share-label {color:{{shareLabelColor}}; }",
      },
    ],
  },
  shareLabelTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "12", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [
          { key: "shareLabelStyle", condition: "!=", value: "style2" },
          { key: "shareCountLabel", condition: "!=", value: "" },
          { key: "shareLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-post-share-label",
      },
    ],
  },
  shareLabelBackground: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        depends: [{ key: "shareLabelShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-post-share-count-section,
          {{ULTP}} .ultp-post-share-count-section-style1::after {background-color:{{shareLabelBackground}}; }`,
      },
    ],
  },
  shareLabelBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#c3c3c3",
      type: "solid",
    },
    style: [
      {
        depends: [
          { key: "shareLabelShow", condition: "==", value: true },
          { key: "shareLabelStyle", condition: "!=", value: "style1" },
        ],
        selector:
          `{{ULTP}} .ultp-post-share-count-section,
          {{ULTP}} .ultp-post-share-count-section-style1::after`,
      },
    ],
  },
  shareLabelRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "shareLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-share-count-section { border-radius:{{shareLabelRadius}}; }",
      },
    ],
  },
  shareLabelPadding: {
    type: "object",
    default: {
      lg: { top: "10", bottom: "10", left: "25", right: "25", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "shareLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-post-share-count-section { padding:{{shareLabelPadding}}; }",
      },
    ],
  },
  /*============================
      Sticky Positions
  ============================*/
  enableSticky: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [{ key: "enableSticky", condition: "==", value: false }],
        selector:
          "{{ULTP}} .ultp-post-share-layout {display: flex; align-items:center;}",
      },
    ],
  },
  itemPosition: {
    type: "string",
    default: "bottom",
    style: [
      { depends: [{ key: "enableSticky", condition: "==", value: true }] },
    ],
  },
  stickyLeftOffset: {
    type: "string",
    default: "20",
    style: [
      {
        depends: [
          { key: "enableSticky", condition: "==", value: true },
          { key: "itemPosition", condition: "!=", value: "right" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share .ultp-post-share-layout { position:fixed;left:{{stickyLeftOffset}}px;}",
      },
    ],
  },
  stickyRightOffset: {
    type: "string",
    default: "20",
    style: [
      {
        depends: [
          { key: "enableSticky", condition: "==", value: true },
          { key: "itemPosition", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share .ultp-post-share-layout { position:fixed; right:{{stickyRightOffset}}px;}",
      },
    ],
  },
  stickyTopOffset: {
    type: "string",
    default: "20",
    style: [
      {
        depends: [
          { key: "enableSticky", condition: "==", value: true },
          { key: "itemPosition", condition: "!=", value: "bottom" },
        ],
        selector:
          "{{ULTP}} .ultp-post-share .ultp-post-share-layout { position:fixed;top:{{stickyTopOffset}}px;z-index:9999999;}",
      },
    ],
  },
  stickyBottomOffset: {
    type: "string",
    default: "20",
    style: [
      {
        depends: [
          { key: "itemPosition", condition: "==", value: "bottom" },
          { key: "enableSticky", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-post-share .ultp-post-share-layout { position:fixed;bottom:{{stickyBottomOffset}}px; z-index:9999999;}",
      },
    ],
  },
  resStickyPost: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "enableSticky", condition: "==", value: true }] },
    ],
  },
  floatingResponsive: {
    type: "string",
    default: "600",
    style: [
      {
        depends: [{ key: "resStickyPost", condition: "==", value: true }],
        selector:
          "@media only screen and (max-width: {{floatingResponsive}}px) { .ultp-post-share-layout { position: unset !important; display: flex !important; justify-content: center; } .ultp-post-share-item-inner-block { display: flex !important; } .ultp-post-share-count-section-style1:after { bottom: auto !important; transform: rotate(44deg) !important; top: 40% !important; right: -8px !important; }} ",
      },
    ],
  },
  stopSticky: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "enableSticky", condition: "==", value: true }] },
    ],
  },

  /*============================
      Advanced Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
