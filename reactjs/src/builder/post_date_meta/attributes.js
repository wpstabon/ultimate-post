import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  prefixEnable: { type: "boolean", default: false },
  metaDateIconShow: { type: "boolean", default: true },
  dateFormat: { type: "string", default: "updated" },
  metaDateFormat: { type: "string", default: "M j, Y" },
  metaDateColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        selector: "{{ULTP}} .ultp-date-meta-format { color:{{metaDateColor}} }",
      },
    ],
  },
  metaDateTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-date-meta" }],
  },
  metaDateCountAlign: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper { text-align: {{metaDateCountAlign}};}",
      },
    ],
  },
  datePubLabel: {
    type: "string",
    default: "Publish Date",
    style: [
      {
        depends: [
          { key: "prefixEnable", condition: "==", value: true },
          { key: "dateFormat", condition: "==", value: "publish" },
        ],
      },
    ],
  },
  dateUpLabel: {
    type: "string",
    default: "Updated Date",
    style: [
      {
        depends: [
          { key: "prefixEnable", condition: "==", value: true },
          { key: "dateFormat", condition: "==", value: "updated" },
        ],
      },
    ],
  },
  datePrefixColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "prefixEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-meta-prefix { color:{{datePrefixColor}} }",
      },
    ],
  },
  datePrefixSpace: {
    type: "object",
    default: { lg: "12", unit: "px" },
    style: [
      {
        depends: [{ key: "prefixEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-meta-prefix { margin-right: {{datePrefixSpace}} }",
      },
    ],
  },
  metaDateIconStyle: {
    type: "string",
    default: "date1",
    style: [
      { depends: [{ key: "metaDateIconShow", condition: "==", value: true }] },
    ],
  },
  iconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "metaDateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-meta-icon > svg { fill:{{iconColor}}; stroke:{{iconColor}};}",
      },
    ],
  },
  metaDateIconSize: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "metaDateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-meta-icon svg { width:{{metaDateIconSize}}; height:{{metaDateIconSize}} }",
      },
    ],
  },
  metaDateIconSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "metaDateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-meta-icon > svg { margin-right: {{metaDateIconSpace}} }",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
