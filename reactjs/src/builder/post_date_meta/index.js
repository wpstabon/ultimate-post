const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-date-meta', {
        title: __('Post Date Meta','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/post_date.svg'} alt="Post Date Meta"/>,
        category: 'postx-site-builder',
        description: __('Display Post Publish/Modified Date and customize it as you need.','ultimate-post'),
        keywords: [ 
            __('Date','ultimate-post'),
            __('Modify','ultimate-post'),
            __('Modified','ultimate-post'),
            __('Date Meta','ultimate-post'),
            __('Post Date','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)