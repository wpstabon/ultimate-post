const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
const { dateI18n } = wp.date
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, _dateFormat, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import IconPack from "../../helper/fields/tools/IconPack"
import { useBlockId } from '../../helper/hooks/use-blockid'

export default function Edit(props) {
    const {
        setAttributes, 
        name,
        attributes,
        clientId,
        className,
        attributes: {
            blockId, 
            advanceId,
            metaDateFormat,
            metaDateIconShow,
            metaDateIconStyle,
            prefixEnable,
            datePubLabel,
            dateUpLabel,
            dateFormat,
            currentPostId
        }
    } = props;
    const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if (blockId) {
        CssGenerator(attributes, 'ultimate-post/post-date-meta', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')} >
                        <CommonSettings
                            title={`inline`}
                            initialOpen={true}
                            include={[
                                {
                                    data: {
                                        type: "group", key: "dateFormat", justify: true, options: [
                                            { label: "Publish Date", value: "publish" },
                                            { label: "Updated Date", value: "updated" }
                                        ],
                                        responsive: false, label: __('Date/Time Format', 'ultimate-post'),
                                    }
                                },
                                {
                                    data: { type: 'toggle', key: 'prefixEnable', label: __('Prefix Enable', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'text', key: 'datePubLabel', label: __('Prefix Published Label', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'text', key: 'dateUpLabel', label: __('Prefix Update Label', 'ultimate-post') }
                                },
                                {
                                    data: {
                                        type: 'select', key: 'metaDateFormat', label: __('Date/Time Format', 'ultimate-post'),
                                        options: [
                                            { value: 'M j, Y', label: 'Feb 7, 2022' },
                                            { value:'default_date',label: 'WordPress Default Date Format', pro: true },
                                            { value:'default_date_time',label: 'WordPress Default Date & Time Format', pro: true },
                                            { value: 'g:i A', label: '1:12 PM', pro: true },
                                            { value: 'F j, Y', label: 'February 7, 2022', pro: true },
                                            { value: 'F j, Y g:i A', label: 'February 7, 2022 1:12 PM', pro: true },
                                            { value: 'M j, Y g:i A', label: 'Feb 7, 2022 1:12 PM', pro: true },
                                            { value: 'j M Y', label: '7 Feb 2022', pro: true },
                                            { value: 'j M Y g:i A', label: '7 Feb 2022 1:12 PM', pro: true },
                                            { value: 'j F Y', label: '7 February 2022', pro: true },
                                            { value: 'j F Y g:i A', label: '7 February 2022 1:12 PM', pro: true },
                                            { value: 'j. M Y', label: '7. Feb 2022', pro: true },
                                            { value: 'j. M Y | H:i', label: '7. Feb 2022 | 1:12', pro: true },
                                            { value: 'j. F Y', label: '7. February 2022', pro: true },
                                            { value: 'j.m.Y', label: '7.02.2022', pro: true },
                                            { value: 'j.m.Y g:i A', label: '7.02.2022 1:12 PM', pro: true },
                                        ]
                                    }
                                },
                                {
                                    data: { type: 'color', key: 'metaDateColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'metaDateTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'metaDateCountAlign', disableJustify: true, responsive: true, label: __('Alignment', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'datePrefixColor', label: __('Prefix Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'datePrefixSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Prefix label Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Icon', 'ultimate-post')}
                            depend="metaDateIconShow"
                            include={[
                                {
                                    data: { type: 'icon', key: 'metaDateIconStyle', label: __('Select Icon', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'iconColor', label: __('Icon Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'metaDateIconSize', min: 0, max: 100, responsive: true, step: 1, unit: ['px', 'em', 'rem'], label: __('Icon Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'metaDateIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: ['px', 'em', 'rem'], label: __('Icon Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-date-meta`}>
                        {
                            prefixEnable &&
                            <span className={`ultp-date-meta-prefix`}>
                                {dateFormat == "publish" && datePubLabel}
                                {dateFormat == "updated" && dateUpLabel}
                            </span>
                        }
                        { metaDateIconShow &&
                            <span className={`ultp-date-meta-icon`}>
                                {(metaDateIconStyle != '') && IconPack[metaDateIconStyle]}
                            </span>
                        }
                        { metaDateFormat &&
                            <span className={`ultp-date-meta-format`}>
                                { dateI18n(_dateFormat(metaDateFormat)) }
                            </span>
                        }
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
