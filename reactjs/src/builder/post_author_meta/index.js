const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-author-meta', {
        title: __('Post Author Meta','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/author.svg'} alt="Post Author Meta"/>,
        category: 'postx-site-builder',
        description: __('Display Post Author Meta with the ultimate controls.','ultimate-post'),
        keywords: [ 
            __('Post Author Meta','ultimate-post'),
            __('Author Meta','ultimate-post'),
            __('Post Author','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)