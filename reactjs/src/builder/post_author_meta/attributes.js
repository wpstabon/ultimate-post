import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  /*============================
      Post Author Meta  Settings
  ============================*/
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  authMetaIconColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-authMeta-count > .ultp-authMeta-name { color:{{authMetaIconColor}} }",
      },
    ],
  },
  authMetaHoverColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-authMeta-count > .ultp-authMeta-name:hover{color:{{authMetaHoverColor}} }",
      },
    ],
  },
  authMetaTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-authMeta-count .ultp-authMeta-name" }],
  },
  authMetAvatar: { type: "boolean", default: true },
  authMetaIconShow: { type: "boolean", default: false },
  authMetaCountAlign: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper { text-align: {{authMetaCountAlign}};}",
      },
    ],
  },
/*============================
    Post Author Avatar Style
============================*/
  authMetAvSize: {
    type: "object",
    default: { lg: "30", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetAvatar", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count .ultp-authMeta-avatar > img { width:{{authMetAvSize}}; height:{{authMetAvSize}} }",
      },
    ],
  },
  authMetAvSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetAvatar", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count .ultp-authMeta-avatar > img { margin-right: {{authMetAvSpace}} }",
      },
    ],
  },
  authMetAvRadius: {
    type: "object",
    default: { lg: "100", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetAvatar", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count .ultp-authMeta-avatar > img { border-radius:{{authMetAvRadius}}; }",
      },
    ],
  },
  authMetaLabel: { type: "boolean", default: true },
  /*============================
    Post Author Icon Settings
  ============================*/
  authMetaIconStyle: {
    type: "string",
    default: "author1",
    style: [
      { depends: [{ key: "authMetaIconShow", condition: "==", value: true }] },
    ],
  },
  iconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "authMetaIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count > svg, {{ULTP}} .ultp-authMeta-count > div > svg { fill:{{iconColor}}; stroke:{{iconColor}}}",
      },
    ],
  },
  authMetaIconSize: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetaIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count > svg { width:{{authMetaIconSize}}; height:{{authMetaIconSize}} }",
      },
    ],
  },
  authMetaSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetaIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-count > svg { margin-right: {{authMetaSpace}} }",
      },
    ],
  },
  
  /*============================
    Post Author Label Settings
  ============================*/
  authMetaLabelText: {
    type: "string",
    default: "By",
    style: [
      { depends: [{ key: "authMetaLabel", condition: "==", value: true }] },
    ],
  },
  authMetaLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "authMetaLabel", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-label { color:{{authMetaLabelColor}} }",
      },
    ],
  },
  authMetaLabelTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "authMetaLabel", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-authMeta-label",
      },
    ],
  },
  authMetaLabelSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "authMetaLabel", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-authMeta-label { margin-right: {{authMetaLabelSpace}} }",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
