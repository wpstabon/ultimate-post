const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import IconPack from "../../helper/fields/tools/IconPack"
import { useBlockId } from '../../helper/hooks/use-blockid'

export default function Edit(props) {

    const {
        setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
            blockId,
            advanceId,
            authMetaLabelText,
            authMetAvatar,
            authMetaIconStyle,
            authMetaLabel,
            authMetaIconShow,
            currentPostId,
        }
    } = props
    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });
    const store = { setAttributes, name, attributes, clientId }

   if (blockId) {
        CssGenerator(attributes, 'ultimate-post/post-author-meta', blockId);
    }
    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                        <CommonSettings
                            title={`inline`}
                            include={[
                                {
                                    data: { type: 'color', key: 'authMetaIconColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'authMetaHoverColor', label: __('Hover Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'authMetaTypo', label: __('Text Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'authMetaCountAlign', disableJustify: true, responsive: true, label: __('Alignment', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Avatar', 'ultimate-post')}
                            depend="authMetAvatar"
                            include={[
                                {
                                    data: { type: 'range', key: 'authMetAvSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'authMetAvRadius', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Radius', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'authMetAvSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Icon', 'ultimate-post')}
                            depend="authMetaIconShow"
                            include={[
                                {
                                    data: { type: 'color', key: 'iconColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'icon', key: 'authMetaIconStyle', label: __('Style', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'authMetaIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'authMetaSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Label', 'ultimate-post')}
                            depend="authMetaLabel"
                            include={[
                                {
                                    data: { type: 'text', key: 'authMetaLabelText', label: __('authMeta Label Text', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'authMetaLabelColor', label: __('Label Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'authMetaLabelTypo', label: __('Label Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'authMetaLabelSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Label Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <span className={`ultp-authMeta-count`}>
                        {authMetaIconShow && (authMetaIconStyle != '') && IconPack[authMetaIconStyle]}
                        <div className={`ultp-authMeta-avatar`}>
                            {authMetAvatar && <img src={ultp_data.url + 'assets/img/ultp-placeholder.jpg'} alt="author img" />}
                        </div>
                        {authMetaLabel &&
                            <span className={`ultp-authMeta-label`}>
                                {authMetaLabelText}
                            </span>
                        }
                        <a href="#" className={`ultp-authMeta-name`} >David Rikson </a>
                    </span>
                </div>
            </div>
        </Fragment>
    )
}
