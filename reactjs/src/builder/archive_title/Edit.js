const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CustomCssAdvanced, CustomHtmlTag, ExcerptStyle, GeneralAdvanced, GeneralContent, ImageStyle, PrefixStyle, ResponsiveAdvanced, TaxWrapStyle, TitleStyle, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'
import { Section, Sections } from '../../helper/Sections'
import ToolBarElement from '../../helper/ToolBarElement'

export default function Edit(props) {
    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			titleShow,
			prefixShow,
			prefixText,
			advanceId,
			showImage,
			layout,
			excerptShow,
			customTaxColor,
			customTaxTitleColor,
			titleTag,
            currentPostId
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) { 
        CssGenerator(attributes, 'ultimate-post/archive-title', blockId); 
    }

    // Dummy Content
    const dummyName = 'Archive Title';
    const dummyImage = ultp_data.url + 'assets/img/builder-fallback.jpg';
    const dynamiColor = '#037fff';
    const dummyDesc = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam molestie aliquet molestie.';

    const styleCSS = dummyImage ? {backgroundImage: `url(${dummyImage})`} : {background: `${dynamiColor}`};
    
    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting','ultimate-post')}>
                        <GeneralContent 
                            store={store} 
                            initialOpen={true} 
                            exclude={['columns','columnGridGap','contentTag','openInTab']}
                            include={[
                                {  
                                    position: 0, data:{  type:'layout', col: 2, imgPath: "assets/img/layouts/archive/popup/ar", block:"archive-title", key:'layout', label:__('Layout','ultimate-post'),
                                        options: [
                                            { 
                                                img: 'assets/img/layouts/archive/al1.png', 
                                                demoUrl: "",
                                                label: __('Layout 1','ultimate-post'), value: '1', 
                                            },
                                            { 
                                                img: 'assets/img/layouts/archive/al2.png', 
                                                demoUrl: "",
                                                label: __('Layout 2','ultimate-post'), value: '2'
                                            }
                                        ]
                                    }
                                }]}
                        />
                        <TitleStyle 
                            depend="titleShow" 
                            store={store} 
                            exclude={['titlePosition','titleHoverColor','titleLength','titleBackground','titleStyle', 'titleAnimColor']} 
                            include={[
                                { 
                                    position: 0, data:{ type:'toggle',key:'customTaxTitleColor', label:__('Specific Color','ultimate-post'), pro: true } 
                                },
                                { 
                                    position: 1, data:{ type:'linkbutton',key:'seperatorTaxTitleLink', placeholder:__('Choose Color','ultimate-post'), label:__('Taxonomy Specific (Pro)','ultimate-post'), text:'Choose Color' } 
                                },
                            ]}
                        />
                        <PrefixStyle 
                            depend="prefixShow"
                            include={[{ 
                                    position: 1, data:{ type:'toggle',key:'prefixTop', label:__('Show on Top','ultimate-post')} 
                            }]}
                            store={store}/>
                        { 
                            layout== '2' && 
                            <TaxWrapStyle 
                                store={store} 
                                exclude={['TaxAnimation']} /> 
                        }
                        {   
                            layout== '1' && 
                            <ImageStyle 
                                depend="showImage"
                                store={store} 
                                exclude={
                                    ['imgMargin', 'imgCropSmall','imgCrop','imgAnimation','imgOverlay','imageScale','imgOpacity','overlayColor','imgOverlayType','imgGrayScale','imgHoverGrayScale','imgShadow','imgHoverShadow','imgTab','imgHoverRadius','imgRadius','imgSrcset','imgLazy']
                                }
                                include={[
                                    {
                                        position: 3, data:{ type:'alignment',key:'contentAlign', responsive:false, label:__('Alignment','ultimate-post'), disableJustify:true } 
                                    },
                                    { 
                                        position: 2, data:{ type:'range',key:'imgSpacing', label:__('Img Spacing','ultimate-post'),min:0, max:100, step:1, responsive:true } 
                                    }
                                ]} 
                            />
                        }
                        <ExcerptStyle 
                            depend="excerptShow" 
                            store={store}
                            title={__('Description','ultimate-post')}
                            exclude={['showSeoMeta','excerptLimit','showFullExcerpt']}/>
                    </Section>
                    <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                        <GeneralAdvanced store={store}/>
                        <ResponsiveAdvanced store={store}/>
                        <CustomCssAdvanced store={store}/>
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            
            <ToolBarElement
                include={[
                    { 
                        type:'layout', col: 2, imgPath: "assets/img/layouts/archive/popup/ar", block:"archive-title", key:'layout', 
                        options: [
                            { 
                                img: 'assets/img/layouts/archive/al1.png', 
                                demoUrl: "",
                                label: __('Layout 1','ultimate-post'), value: '1', 
                            },
                            { 
                                img: 'assets/img/layouts/archive/al2.png', 
                                demoUrl: "",
                                label: __('Layout 2','ultimate-post'), value: '2'
                            }
                        ], label:__('Layout','ultimate-post' )
                    }
                ]}
                store={store}
            />

            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={ `ultp-block-archive-title  ultp-archive-layout-${layout}` }>
                        { layout == 1 &&
                            <div>
                                { dummyImage && showImage &&
                                    <img className="ultp-archive-image" src={ dummyImage } alt={ dummyName }/>
                                }
                                { titleShow && <CustomHtmlTag tag={titleTag} className="ultp-archive-name" style={customTaxTitleColor ? {color: dynamiColor} : {}}>{ prefixShow && <span className="ultp-archive-prefix">{prefixText} </span>}{ dummyName }</CustomHtmlTag> }
                                { excerptShow && <div className="ultp-archive-desc">{ dummyDesc }</div> }
                            </div>
                        }
                        { layout == 2 &&
                            <div className="ultp-archive-content" style={styleCSS}>
                                <div className="ultp-archive-overlay" style={customTaxColor ? {backgroundColor: dynamiColor} : {}}></div>
                                { titleShow &&
                                    <CustomHtmlTag tag={titleTag} className="ultp-archive-name"> 
                                        { prefixShow &&
                                            <span className="ultp-archive-prefix">{prefixText} </span>
                                        }
                                        { dummyName }
                                    </CustomHtmlTag>
                                }
                                { excerptShow && <div className="ultp-archive-desc">{ dummyDesc }</div> }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </Fragment>
    )
}