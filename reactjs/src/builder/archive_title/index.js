const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/archive-title', {
        title: __('Archive Title','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url+'assets/img/blocks/archive-title.svg'}/>,
        category: 'postx-site-builder',
        description: __('Display archive titles and customize them as you need.','ultimate-post'),
        keywords: [ 
            __('archive','ultimate-post'),
            __('dynamic title','ultimate-post'),
            __('title','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)