import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      General Settings
  ============================*/
  layout: { type: "string", default: "1" },
  contentAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          "{{ULTP}} .ultp-block-archive-title { text-align:{{contentAlign}}; }",
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          "{{ULTP}} .ultp-block-archive-title { text-align:{{contentAlign}}; }",
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          "{{ULTP}} .ultp-block-archive-title { text-align:{{contentAlign}}; }",
      },
    ],
  },
  titleShow: { type: "boolean", default: true },
  excerptShow: { type: "boolean", default: true },
  prefixShow: { type: "boolean", default: false },
  showImage: { type: "boolean", default: false },

  /*============================
      Title Settings
  ============================*/
  titleTag: { type: "string", default: "h1" },
  customTaxTitleColor: { type: "boolean", default: false },
  seperatorTaxTitleLink: {
    type: "string",
    default: ultp_data.category_url,
    style: [
      {
        depends: [{ key: "customTaxTitleColor", condition: "==", value: true }],
      },
    ],
  },
  titleColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-name { color:{{titleColor}}; }",
      },
      {
        depends: [
          { key: "customTaxTitleColor", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-name { color:{{titleColor}}; }",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "28", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "32", unit: "px" },
      transform: "",
      decoration: "none",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-archive-title .ultp-archive-name",
      },
    ],
  },
  titlePadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-name { padding:{{titlePadding}}; }",
      },
    ],
  },

  /*============================
      Content Settings
  ============================*/
  excerptColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-desc { color:{{excerptColor}}; }",
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: "22", unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-archive-title .ultp-archive-desc",
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: "0", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-desc { padding: {{excerptPadding}}; }",
      },
    ],
  },
  /*============================
      Prefix Settings
  ============================*/
  prefixText: { type: "string", default: "Sample Prefix Text" },
  prefixTop: { type: "boolean", default: false,
    style: [{ selector: "{{ULTP}} .ultp-archive-prefix { display: block; }" }],
  },
  prefixColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [{ key: "prefixShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-prefix { color:{{prefixColor}}; }",
      },
    ],
  },
  prefixTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "", unit: "px" },
      transform: "",
      decoration: "none",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "prefixShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-archive-title .ultp-archive-prefix",
      },
    ],
  },
  prefixPadding: {
    type: "object",
    default: { lg: { top: 10, bottom: 5, unit: "px" } },
    style: [
      {
        depends: [{ key: "prefixShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-prefix { padding:{{prefixPadding}}; }",
      },
    ],
  },

  /*============================
      Image Settings
  ============================*/
  imgWidth: {
    type: "object",
    default: { lg: "", ulg: "%" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-image { max-width: {{imgWidth}}; }",
      },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-image {object-fit: cover; height: {{imgHeight}}; }",
      },
    ],
  },
  imgSpacing: {
    type: "object",
    default: { lg: "10" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-image { margin-bottom: {{imgSpacing}}px; }",
      },
    ],
  },

  /*============================
      Custom Wrapper Settings
  ============================*/
  customTaxColor: { type: "boolean", default: false },
  seperatorTaxLink: {
    type: "string",
    default: ultp_data.category_url,
    style: [
      { depends: [{ key: "customTaxColor", condition: "==", value: true }] },
    ],
  },
  TaxAnimation: { type: "string", default: "none" },
  TaxWrapBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "customTaxColor", condition: "!=", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content .ultp-archive-overlay { background:{{TaxWrapBg}}; }",
      },
    ],
  },
  TaxWrapHoverBg: {
    type: "string",
    style: [
      {
        depends: [{ key: "customTaxColor", condition: "!=", value: true }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content:hover .ultp-archive-overlay { background:{{TaxWrapHoverBg}}; }",
      },
    ],
  },
  TaxWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector: "{{ULTP}} .ultp-block-archive-title .ultp-archive-content",
      },
    ],
  },
  TaxWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content:hover",
      },
    ],
  },
  TaxWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector: "{{ULTP}} .ultp-block-archive-title .ultp-archive-content",
      },
    ],
  },
  TaxWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content:hover",
      },
    ],
  },
  TaxWrapRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content { border-radius: {{TaxWrapRadius}}; }",
      },
    ],
  },
  TaxWrapHoverRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content:hover { border-radius: {{TaxWrapHoverRadius}}; }",
      },
    ],
  },
  customOpacityTax: {
    type: "string",
    default: 0.6,
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content .ultp-archive-overlay { opacity: {{customOpacityTax}}; }",
      },
    ],
  },
  customTaxOpacityHover: {
    type: "string",
    default: 0.9,
    style: [
      {
        selector:
          "{{ULTP}} .ultp-taxonomy-items li a:hover .ultp-archive-overlay { opacity: {{customTaxOpacityHover}}; }",
      },
    ],
  },
  TaxWrapPadding: {
    type: "object",
    default: {
      lg: { top: "20", bottom: "20", left: "20", right: "20", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2"] }],
        selector:
          "{{ULTP}} .ultp-block-archive-title .ultp-archive-content { padding: {{TaxWrapPadding}}; }",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], [ 'loadingColor'])
};

export default attributes;