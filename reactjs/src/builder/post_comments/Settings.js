const { __ } = wp.i18n;
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';

const Settings = ({ store }) => {
    return (
        <>
            <Sections>
                <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                    <CommonSettings initialOpen={true}
                            title={'inline'}
                            include={[
                                {
                                    data: {
                                        type: 'layout', key: 'layout', pro: false, tab: true, label: __('Select Advanced Layout', 'ultimate-post'), block: 'related-posts',
                                        options: [
                                            { img: 'assets/img/layouts/builder/comments/comment1.png', label: __('Layout 1', 'ultimate-post'), value: 'layout1', pro: false },
                                            { img: 'assets/img/layouts/builder/comments/comment2.png', label: __('Layout 2', 'ultimate-post'), value: 'layout2', pro: true },
                                            { img: 'assets/img/layouts/builder/comments/comment3.png', label: __('Layout 3', 'ultimate-post'), value: 'layout3', pro: true },
                                        ]
                                    }
                                },
                            ]}
                            store={store} />
                    <CommonSettings initialOpen={false}
                        title={__('Comments Form Heading', 'ultimate-post')}
                        depend="replyHeading"
                        include={[
                            {
                                data: { type: 'text', key: 'leaveRepText', label: __('Leave a reply text', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'HeadingColor', label: __('Heading Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'HeadingTypo', label: __('Heading Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'subHeadingColor', label: __('Sub Heading Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'headingSpace', min: 1, max: 150, step: 1, unit: true, responsive: true, label: __('Heading Spacing', 'ultimate-post') }
                            },
                        ]}
                        store={store} />
                    <CommonSettings initialOpen={false}
                        title={__('Comments Form Input', 'ultimate-post')}
                        include={[
                            {
                                data: { type: 'separator', label: __('Input Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'inputPlaceHolder', label: __('Textarea Placeholder', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'inputPlaceValueColor', label: __('Placeholder Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'inputValueColor', label: __('Input Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'inputValueBg', label: __('Input Background Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'inputValueTypo', label: __('Input Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'inputValuePad', step: 1, unit: true, responsive: true, label: __('Input Padding', 'ultimate-post') }
                            },
                            {
                                data: { type: 'border', key: 'inputBorder', label: __('Input Border', 'ultimate-post') }
                            },
                            {
                                data: { type: 'border', key: 'inputHovBorder', label: __('Input Hover Border', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'inputRadius', step: 1, unit: true, responsive: true, label: __('Input Border Radius', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'inputHovRadius', step: 1, unit: true, label: __('Input Hover Radius', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'inputSpacing', min: 1, max: 300, unit: true, responsive: true, step: 1, label: __('Spacing', 'ultimate-post') }
                            },
                            {
                                data: { type: 'separator', label: __('Label Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'toggle', key: 'inputLabel', label: __('Input Label', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'cmntInputText', label: __('Input Label', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'nameInputText', label: __('Input Label', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'emailInputText', label: __('Input Label', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'webInputText', label: __('Input Label', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'inputLabelColor', label: __('Label Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'inputLabelTypo', label: __('Label Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'toggle', key: 'disableWebUrl', label: __('Disable Web URL', 'ultimate-post') }
                            },
                            {
                                data: { type: 'toggle', key: 'cookiesEnable', label: __('Cookies Enable', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'cookiesText', label: __('Cookies Text', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'cookiesColor', label: __('Cookies Text Color', 'ultimate-post') }
                            },
                        ]}
                        store={store} />
                    <CommonSettings initialOpen={false}
                        title={__('Submit Button', 'ultimate-post')}
                        include={[
                            {
                                data: { type: 'text', key: 'subBtnText', label: __('Submit Button Text', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'subBtnTypo', label: __('Text Typography', 'ultimate-post') }
                            },
                            {
                                data: {
                                    type: "tab", key: "submitButton", content: [
                                        {
                                            name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                {
                                                    type: 'color', key: 'subBtnColor', label: __('Text Color', 'ultimate-post')
                                                },
                                                {
                                                    type: 'color2', key: 'subBtnBg', label: __('Background Color', 'ultimate-post')
                                                },
                                                {
                                                    type: 'border', key: 'subBtnBorder', label: __('Border', 'ultimate-post')
                                                },
                                                {
                                                    type: 'dimension', key: 'subBtnRadius', step: 1, unit: true, responsive: true, label: __('Border Radius', 'ultimate-post')
                                                },
                                            ]
                                        },
                                        {
                                            name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                {
                                                    type: 'color', key: 'subBtnHovColor', label: __('Text Hover Color', 'ultimate-post')
                                                },
                                                {
                                                    type: 'color2', key: 'subBtnHovBg', label: __('Background Hover Color', 'ultimate-post')
                                                },
                                                {
                                                    type: 'border', key: 'subBtnHovBorder', label: __('Hover Border', 'ultimate-post')
                                                },
                                                {
                                                    type: 'dimension', key: 'subBtnHovRadius', step: 1, unit: true, responsive: true, label: __('Hover Radius', 'ultimate-post')
                                                },
                                            ]
                                        },
                                    ]
                                }
                            },
                            {
                                data: { type: 'dimension', key: 'subBtnPad', step: 1, unit: true, responsive: true, label: __('Button Padding', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'subBtnSpace', min: 1, max: 300, unit: true, responsive: true, step: 1, label: __('Spacing', 'ultimate-post') }
                            },
                            {
                                data: { type: 'alignment', key: 'subBtnAlign', disableJustify: true, label: __('Alignment', 'ultimate-post') }
                            },
                        ]}
                        store={store} />
                    <CommonSettings initialOpen={true}
                        title={__('Comments & Reply', 'ultimate-post')}
                        include={[
                            {
                                data: { type: 'separator', label: __('Commenter Name Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'authColor', label: __('Name Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'authHovColor', label: __('Name Hover Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'authorTypo', label: __('Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'commentSpace', step: 1, unit: true, responsive: true, label: __('Spacing', 'ultimate-post') }
                            },
                            {
                                data: { type: 'text', key: 'replyText', label: __('Comments Text', 'ultimate-post') }
                            },
                            {
                                data: { type: 'toggle', key: 'commentCount', label: __('Comment Count', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'commentCountColor', label: __('Text Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'commentCountTypo', label: __('Text Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'commentCountSpace', min: 1, max: 300, unit: true, responsive: true, step: 1, label: __('Comment Count Spacing', 'ultimate-post') }
                            },
                            // Reply Button
                            {
                                data: { type: 'separator', label: __('Button Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'replyBtnTypo', label: __('Button Typography', 'ultimate-post') }
                            },
                            {
                                data: {
                                    type: "tab", key: "replyButton", content: [
                                        {
                                            name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                {
                                                    type: 'color', key: 'replyBtnColor', label: __('Reply Button', 'ultimate-post')
                                                },
                                                {
                                                    type: 'color2', key: 'replyBtnBg', label: __(' Background', 'ultimate-post')
                                                },
                                            ]
                                        },
                                        {
                                            name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                {
                                                    type: 'color', key: 'replyBtnHovColor', label: __('Reply Button', 'ultimate-post')
                                                },
                                                {
                                                    type: 'color2', key: 'replyBtnBgHov', label: __('Background', 'ultimate-post')
                                                },
                                            ]
                                        }
                                    ]
                                },
                            },
                            {
                                data: { type: 'border', key: 'replyBtnBorder', label: __('Border', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'replyBtnRadius', step: 1, unit: true, responsive: true, label: __('Radius', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'replyBtnPad', step: 1, unit: true, responsive: true, label: __('Padding', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'replyBtnSpace', min: 1, max: 300, responsive: true, step: 1, unit: true, label: __('Reply Button Spacing', 'ultimate-post') }
                            },
                            {
                                data: { type: 'separator', label: __('Commenter Meta', 'ultimate-post')  }
                            },
                            {
                                data: { type: 'range', key: 'authMetaSpace', min: 1, max: 300, unit: true, responsive: true, step: 1, label: __('Meta Spacing', 'ultimate-post') }
                            },
                            // Auth Meta
                            {
                                data: { type: 'toggle', key: 'authMeta', label: __('Commenter Meta', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'authMetaColor', label: __('Meta Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'authMetaHovColor', label: __('Meta Color Hover', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'authMetaTypo', label: __('Meta Typography', 'ultimate-post') }
                            },
                            {
                                data: { type: 'separator', label: __('Commenter Image', 'ultimate-post') }
                            },
                            // Author img
                            {
                                data: { type: 'toggle', key: 'authImg', label: __('Commenter Image', 'ultimate-post') }
                            },
                            {
                                data: { type: 'dimension', key: 'authImgRadius', step: 1, unit: true, responsive: true, label: __('Image Radius', 'ultimate-post') }
                            },
                            // Reply Content Style
                            {
                                data: { type: 'separator',  label: __('Reply Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'replyColor', label: __('Reply Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'replyHovColor', label: __('Reply Hover Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'typography', key: 'replyTypo', label: __('Reply Typography', 'ultimate-post') }
                            },
                            // Reply Separator
                            {
                                data: { type: 'separator', label: __('Reply Separator', 'ultimate-post') }
                            },
                            {
                                data: { type: 'toggle', key: 'replySeparator', label: __('Reply Separator', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'replySepColor', label: __('Separator Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'range', key: 'replySepSpace', min: 1, max: 300, unit: true, responsive: true, step: 1, label: __('Separator Space', 'ultimate-post') }
                            },
                            // Cancel Button Color
                            {
                                data: { type: 'separator', label: __('Replay Cancel Style', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'replyCancelColor', label: __('Replay Cancel Color', 'ultimate-post') }
                            },
                            {
                                data: { type: 'color', key: 'replyCancelHoverColor', label: __('Replay Cancel Hover Color', 'ultimate-post') }
                            },
                        ]}
                        store={store} />
                </Section>
                <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                    <GeneralAdvanced store={store} />
                    <ResponsiveAdvanced pro={true} store={store} />
                    <CustomCssAdvanced store={store} />
                </Section>
            </Sections>
        </>
    );
};

export default Settings;