const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-comments', {
        title: __('Post Comments','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/comments.svg'} alt="Post Comments" />,
        category: 'postx-site-builder',
        description: __('Display the Post Comment section and customize it as you need.','ultimate-post'),
        keywords: [ 
            __('Comments','ultimate-post'),
            __('Comment Form','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)