const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'
import ToolBarElement from '../../helper/ToolBarElement'
import Settings from './Settings'

export default function Edit(props) {

    const {
        setAttributes,
        name,
        attributes,
        clientId,
        className,
        attributes: {
            blockId,
            advanceId,
            layout,
            leaveRepText,
            replyHeading,
            inputLabel,
            cookiesText,
            subBtnText,
            replyText,
            commentCount,
            authMeta,
            authImg,
            cookiesEnable,
            cmntInputText,
            emailInputText,
            nameInputText,
            webInputText,
            inputPlaceHolder,
            currentPostId
        }
    } = props;

    const store = { setAttributes, name, attributes, clientId }

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if (blockId) {
        CssGenerator(attributes, 'ultimate-post/post-comments', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>

            <ToolBarElement
                include={[
                    {
                        type: 'layout', key: 'layout', pro: false, tab: true, label: __('Select Advanced Layout', 'ultimate-post'), block: 'related-posts',
                        options: [
                            { img: 'assets/img/layouts/builder/comments/comment1.png', label: __('Layout 1', 'ultimate-post'), value: 'layout1', pro: false },
                            { img: 'assets/img/layouts/builder/comments/comment2.png', label: __('Layout 2', 'ultimate-post'), value: 'layout2', pro: false },
                            { img: 'assets/img/layouts/builder/comments/comment3.png', label: __('Layout 3', 'ultimate-post'), value: 'layout3', pro: false },
                        ]
                    }
                ]} store={store} />

            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                    <div className={`ultp-block-wrapper ultp-comment-form ultp-comments-${layout}`}>
                        <div className={`ultp-builder-comment-reply`}>
                            <div className="ultp-comment-reply-heading">
                                {commentCount && '05'} {replyText}
                            </div>
                            <ul className="ultp-comment-wrapper ultp-builder-comment-reply">
                                <li>
                                    <div className="ultp-comment-content-heading">
                                        {authImg &&
                                            <img src={ultp_data.url + `assets/img/ultp-placeholder.jpg`} />
                                        }
                                        <div className="ultp-comment-meta">
                                            <div>Christopher Timmons</div>
                                            {authMeta &&
                                                <span> July 19, 2021 at 3:45 PM" </span>
                                            }
                                        </div>
                                    </div>
                                    <div className="ultp-comment-desc">
                                        Consequuntur magni dolores Eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                                    </div>
                                    <button className="ultp-reply-btn">Reply</button>
                                    <ul className="ultp-reply-wrapper">
                                        <li className="ultp-reply-content">
                                            <img src={ultp_data.url + 'assets/img/blocks/builder/replay_icon.svg'} />
                                            <div className="ultp-reply-content-main">
                                                <div className="ultp-comment-content-heading">
                                                    {authImg &&
                                                        <img src={ultp_data.url + `assets/img/ultp-placeholder.jpg`} />
                                                    }
                                                    <div className="ultp-comment-meta">
                                                        <div> Richard Jackson</div>
                                                        {authMeta &&
                                                            <span> July 19, 2021 at 3:45 PM" </span>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ultp-comment-desc">
                                                    Consequuntur magni dolores Eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                                                </div>
                                                <button className="ultp-reply-btn">Reply</button>
                                            </div>
                                        </li>
                                        <li className="ultp-reply-content">
                                            <img src={ultp_data.url + 'assets/img/blocks/builder/replay_icon.svg'} />
                                            <div className="ultp-reply-content-main">
                                                <div className="ultp-comment-content-heading">
                                                    {authImg &&
                                                        <img src={ultp_data.url + `assets/img/ultp-placeholder.jpg`} />
                                                    }
                                                    <div className="ultp-comment-meta">
                                                        <div>Joan May</div>
                                                        {authMeta &&
                                                            <span> July 19, 2021 at 3:45 PM" </span>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ultp-comment-desc">
                                                    Consequuntur magni dolores Eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                                                </div>
                                                <button className="ultp-reply-btn">Reply</button>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div className="ultp-comment-content-heading">
                                        {authImg &&
                                            <img src={ultp_data.url + `assets/img/ultp-placeholder.jpg`} />
                                        }
                                        <div className="ultp-comment-meta">
                                            <div>Gary Bogart</div>
                                            {
                                                authMeta &&
                                                <span>
                                                    July 19, 2021 at 3:45 PM
                                                </span>
                                            }
                                        </div>
                                    </div>
                                    <div className="ultp-comment-desc">
                                        Consequuntur magni dolores Eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                                    </div>
                                    <button className="ultp-reply-btn">Reply</button>
                                    <ul className="ultp-reply-wrapper">
                                        <li className="ultp-reply-content">
                                            <img src={ultp_data.url + 'assets/img/blocks/builder/replay_icon.svg'} />
                                            <div className="ultp-reply-content-main">
                                                <div className="ultp-comment-content-heading">
                                                    <img src={ultp_data.url + `assets/img/ultp-placeholder.jpg`} />
                                                    <div className="ultp-comment-meta">
                                                        <div>Mario Whitted</div>
                                                        {
                                                            authMeta &&
                                                            <span>
                                                                July 19, 2021 at 3:45 PM
                                                            </span>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ultp-comment-desc">
                                                    Consequuntur magni dolores Eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                                                </div>
                                                <button className="ultp-reply-btn">Reply</button>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div className={`ultp-builder-comments ultp-comments-${layout}`}>
                            <div className="ultp-comments-heading">
                                {replyHeading &&
                                    <div className="ultp-comments-title">
                                        {leaveRepText}
                                    </div>
                                }
                                <span className="ultp-comments-subtitle">
                                    Your email address will not be published. Required fields are marked *
                                </span>
                            </div>
                            <div className="ultp-comment-form comment-form-comment">
                                <div className="ultp-comment-input ultp-field-control ">
                                    {inputLabel &&
                                        <label>{cmntInputText} <span>*</span></label>
                                    }
                                    <textarea placeholder={inputPlaceHolder} />
                                </div>
                                <div className="ultp-comment-name ultp-field-control ">
                                    {inputLabel &&
                                        <label> {nameInputText} <span>*</span></label>
                                    }
                                    <input type="name" />
                                </div>
                                <div className="ultp-comment-email ultp-field-control ">
                                    {inputLabel &&
                                        <label>{emailInputText} <span>*</span></label>
                                    }
                                    <input type="email" />
                                </div>
                                <div className="ultp-comment-website ultp-field-control ">
                                    {inputLabel &&
                                        <label>{webInputText} <span>*</span></label>
                                    }
                                    <input type="text" />
                                </div>
                            </div>
                            {cookiesEnable &&
                                <p className="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" /> <label htmlFor="wp-comment-cookies-consent">{cookiesText}</label>
                                </p>
                            }
                            <button className="ultp-comment-btn" id="submit">
                                {subBtnText}
                            </button>
                        </div>
                    </div>
                </div>
        </Fragment>
    )
}
