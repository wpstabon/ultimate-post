import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  layout: { type: "string", default: "layout1" },
  replyHeading: { type: "boolean", default: true },
  leaveRepText: {
    type: "string",
    default: "Leave a Reply",
    style: [
      { depends: [{ key: "replyHeading", condition: "==", value: true }] },
    ],
  },
  HeadingColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [{ key: "replyHeading", condition: "==", value: true }],
        selector: 
        `{{ULTP}} .ultp-comments-title, 
        {{ULTP}} .comment-reply-title { color:{{HeadingColor}} }`,
      },
    ],
  },
  HeadingTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 24, unit: "px" },
      height: { lg: 26, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "replyHeading", condition: "==", value: true }],
        selector: 
        `{{ULTP}} .ultp-comments-title, 
        {{ULTP}} .comment-reply-title, 
        {{ULTP}} .comment-reply-title a, 
        {{ULTP}} #reply-title`,
      },
    ],
  },
  subHeadingColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "replyHeading", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comments-subtitle,
          {{ULTP}} .logged-in-as, 
          {{ULTP}} .comment-notes { color:{{subHeadingColor}} }`,
      },
    ],
  },
  headingSpace: {
    type: "object",
    default: { lg: "5", unit: "px" },
    style: [
      {
        depends: [{ key: "replyHeading", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comments-title { margin-bottom:{{headingSpace}} !important; }",
      },
    ],
  },
  inputPlaceHolder: {
    type: "string",
    default:
      "Express your thoughts, idea or write a feedback by clicking here & start an awesome comment",
  },
  inputPlaceValueColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-comment-form ::placeholder { color:{{inputPlaceValueColor}} }",
      },
    ],
  },
  inputValueColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input,
          {{ULTP}} .ultp-comment-form textarea { color:{{inputValueColor}} }`,
      },
    ],
  },
  inputValueBg: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input, 
          {{ULTP}} .ultp-comment-form textarea {background-color:{{inputValueBg}}}`,
      },
    ],
  },
  inputValueTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input, 
          {{ULTP}} .ultp-comment-form textarea`,
      },
    ],
  },
  inputValuePad: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input, 
          {{ULTP}} .ultp-comment-form textarea { padding:{{inputValuePad}} }`,
      },
    ],
  },
  inputBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#e2e2e2",
      type: "solid",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input, 
          {{ULTP}} .ultp-comment-input textarea`,
      },
    ],
  },
  inputHovBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#777",
      type: "solid",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input:hover,{{ULTP}} .ultp-comment-form input:focus,{{ULTP}} .ultp-comment-form textarea:hover, 
          {{ULTP}} .ultp-comment-form textarea:focus`,
      },
    ],
  },
  inputRadius: {
    type: "object",
    default: { lg: "0", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input, 
          {{ULTP}} .ultp-comment-form textarea{ border-radius:{{inputRadius}} }`,
      },
    ],
  },
  inputHovRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-form input:hover, 
          {{ULTP}} .ultp-comment-form textarea:hover{ border-radius:{{inputHovRadius}} }`,
      },
    ],
  },
  inputSpacing: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [
          { key: "inputLabel", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout1" },
        ],
        selector:
          `{{ULTP}} .ultp-comment-form > div > label, 
          {{ULTP}} .ultp-comment-form div > p, 
          {{ULTP}} .ultp-comment-input,{{ULTP}} .ultp-comment-form > p,{{ULTP}} .ultp-comment-form > input, .oceanwp-theme 
          {{ULTP}} .comment-form-author,  .oceanwp-theme 
          {{ULTP}} .comment-form-email, .oceanwp-theme 
          {{ULTP}} .comment-form-url { margin:{{inputSpacing}} 0px 0px !important;}`,
      },
      {
        depends: [
          { key: "inputLabel", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout2" },
        ],
        selector:
          `{{ULTP}} .ultp-comment-form > div > label, 
          {{ULTP}} .ultp-comment-form div > p, 
          {{ULTP}} .ultp-comment-input,{{ULTP}} .ultp-comment-form > p, 
          {{ULTP}} .ultp-comment-form > input { margin:{{inputSpacing}} 0px 0px !important} ;`,
      },
      {
        depends: [
          { key: "inputLabel", condition: "==", value: true },
          { key: "layout", condition: "==", value: "layout3" },
        ],
        selector:
          `{{ULTP}} .ultp-comment-form > div > label, 
          {{ULTP}} .ultp-comment-form div > p, 
          {{ULTP}} .ultp-comment-input, 
          {{ULTP}} .ultp-comment-form > p, 
          {{ULTP}} .ultp-comment-form > input { margin:{{inputSpacing}} 0px 0px !important;}`,
      },
    ],
  },
  inputLabel: { type: "boolean", default: true },
  cmntInputText: {
    type: "string",
    default: "Comment's",
    style: [{ depends: [{ key: "inputLabel", condition: "==", value: true }] }],
  },
  nameInputText: {
    type: "string",
    default: "Name",
    style: [{ depends: [{ key: "inputLabel", condition: "==", value: true }] }],
  },
  emailInputText: {
    type: "string",
    default: "Email",
    style: [{ depends: [{ key: "inputLabel", condition: "==", value: true }] }],
  },
  webInputText: {
    type: "string",
    default: "Website Url",
    style: [{ 
        depends: [
          { key: "inputLabel", condition: "==", value: true },
          { key: "disableWebUrl", condition: "==", value: false },
        ], 
      }],
  },
  inputLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "inputLabel", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-form label { color:{{inputLabelColor}} }",
      },
    ],
  },
  inputLabelTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 16, unit: "px" },
      height: { lg: 20, unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "inputLabel", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-comment-form label",
      },
    ],
  },
  disableWebUrl: { 
    type: "boolean", 
    default: false,
    style: [
      {
        depends: [{ key: "disableWebUrl", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-comment-website, {{ULTP}} .comment-form-url { display: none !important; }",
      },
      {
        depends: [{ key: "disableWebUrl", condition: "==", value: false }],
      },
    ],
  },
  cookiesEnable: { type: "boolean", default: true },
  cookiesText: {
    type: "string",
    default:
      "Save my name, email, and website in this browser for the next time I comment.",
    style: [
      { depends: [{ key: "cookiesEnable", condition: "==", value: true }] },
    ],
  },
  cookiesColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "cookiesEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .comment-form-cookies-consent label { color:{{cookiesColor}} }",
      },
    ],
  },
  subBtnText: { type: "string", default: "Post Comment" },
  subBtnTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit`,
      },
    ],
  },
  submitButton: { type: "string", default: "normal" },
  subBtnColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-comment-btn,{{ULTP}} .form-submit > input#submit { color:{{subBtnColor}} }",
      },
    ],
  },
  subBtnBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit`,
      },
    ],
  },
  subBtnBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 0, right: 0, bottom: 0, left: 0 },
      color: "var(--postx_preset_Primary_color)",
      type: "solid",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit`,
      },
    ],
  },
  subBtnRadius: {
    type: "object",
    default: { top:"3", right:"3", bottom:"3", left:"3", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit { border-radius:{{subBtnRadius}} }`,
      },
    ],
  },
  subBtnHovColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn:hover, 
          {{ULTP}} .form-submit > input#submit:hover { color:{{subBtnHovColor}} }`,
      },
    ],
  },
  subBtnHovBg: {
    type: "object",
    default: { openColor: 0, type: "color", color: "var(--postx_preset_Secondary_color)" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn:hover, 
          {{ULTP}} .form-submit > input#submit:hover`,
      },
    ],
  },
  subBtnHovBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 0, right: 0, bottom: 0, left: 0 },
      color: "#151515",
      type: "solid",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn:hover, 
          {{ULTP}} .form-submit > input#submit:hover`,
      },
    ],
  },
  subBtnHovRadius: {
    type: "object",
    default: { top:"3", right:"3", bottom:"3", left:"3", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn:hover, 
          {{ULTP}} .form-submit > input#submit:hover { border-radius:{{subBtnHovRadius}} }`,
      },
    ],
  },
  subBtnPad: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit { padding:{{subBtnPad}} }`,
      },
    ],
  },
  subBtnSpace: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit { margin:{{subBtnSpace}} 0px 0px}`,
      },
    ],
  },
  subBtnAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [{ key: "subBtnAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit, 
          {{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit { display: block !important; margin-right: auto !important; }`,
      },
      {
        depends: [{ key: "subBtnAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit, 
          {{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit { display: block !important; margin-left: auto !important; margin-right: auto !important}`,
      },
      {
        depends: [{ key: "subBtnAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit > input#submit, 
          {{ULTP}} .ultp-comment-btn, 
          {{ULTP}} .form-submit { display: block !important; margin-left:auto !important;}`,
      },
    ],
  },
  authColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-meta div, 
          {{ULTP}} .comment-author a.url,{{ULTP}} .comment-author .fn {color:{{authColor}} }`,
      },
    ],
  },
  authHovColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-meta div:hover,
          {{ULTP}} .comment-author a.url:hover, 
          {{ULTP}} .comment-author b:hover {color: {{authHovColor}} }`,
      },
    ],
  },
  authorTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 18, unit: "px" },
      height: { lg: 25, unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-meta div ,
          {{ULTP}} .comment-author a.url, 
          {{ULTP}} .comment-author b`,
      },
    ],
  },
  commentSpace: {
    type: "object",
    default: {
      lg: { top: "15", bottom: "0", left: "30", right: "0", unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-wrapper, 
          {{ULTP}} .children { margin: {{commentSpace}} }`,
      },
    ],
  },
  replyText: { type: "string", default: "Comments Text" },
  commentCount: { type: "boolean", default: true },
  commentCountColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-comment-reply-heading {color:{{commentCountColor}} }",
      },
    ],
  },
  commentCountTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 30, unit: "px" },
      height: { lg: 28, unit: "px" },
      weight: "600",
    },
    style: [{ selector: "{{ULTP}} .ultp-comment-reply-heading" }],
  },
  commentCountSpace: {
    type: "object",
    default: { lg: "30", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-comment-reply-heading { margin:0px 0px {{commentCountSpace}} }",
      },
    ],
  },
  authMetaSpace: {
    type: "object",
    default: { lg: "7", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-content-heading, 
          {{ULTP}} .comment-meta { margin:0px 0px {{authMetaSpace}} }`,
      },
    ],
  },
  replyButton: { type: "string", default: "normal" },
  replyBtnTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a`,
      },
    ],
  },
  replyBtnColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a {color: {{replyBtnColor}} }`,
      },
    ],
  },
  replyBtnBg: {
    type: "object",
    default: { openColor: 0, type: "color", color: "#f5f5f5" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a`,
      },
    ],
  },
  replyBtnHovColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn:hover, 
          {{ULTP}} .comment-body .reply a:hover, 
          {{ULTP}} .comment-reply-link:hover {color: {{replyBtnHovColor}} }`,
      },
    ],
  },
  replyBtnBgHov: {
    type: "object",
    default: { openColor: 0, type: "color", color: "#f5f5f5" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn:hover, 
          {{ULTP}} .comment-reply-link:hover, 
          {{ULTP}} .comment-body .reply a:hover`,
      },
    ],
  },
  replyBtnBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a`,
      },
    ],
  },
  replyBtnRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a { border-radius:{{replyBtnRadius}}; }`,
      },
    ],
  },
  replyBtnPad: {
    type: "object",
    default: {
      lg: { top: "5", bottom: "5", left: "5", right: "5", unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a { padding:{{replyBtnPad}}; }`,
      },
    ],
  },
  replyBtnSpace: {
    type: "object",
    default: { lg: "7", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-reply-btn, 
          {{ULTP}} .comment-reply-link, 
          {{ULTP}} .comment-body .reply a { margin: {{replyBtnSpace}} 0px}`,
      },
    ],
  },
  authMeta: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [{ key: "authMeta", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-meta span, 
          {{ULTP}} .comment-metadata , 
          {{ULTP}} .comment-meta {display:block}`,
      },
      {
        depends: [{ key: "authMeta", condition: "==", value: false }],
        selector:
          `{{ULTP}} .ultp-comment-meta span, 
          {{ULTP}} .comment-metadata , 
          {{ULTP}} .comment-meta {display:none}`,
      },
    ],
  },
  authMetaColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "authMeta", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-meta span , 
          {{ULTP}} .comment-metadata a ,
          {{ULTP}} .comment-meta a { color:{{authMetaColor}} }`,
      },
    ],
  },
  authMetaHovColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "authMeta", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-meta span:hover , 
          {{ULTP}} .comment-metadata a:hover ,
          {{ULTP}} .comment-meta a:hover { color:{{authMetaHovColor}} }`,
      },
    ],
  },
  authMetaTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 12, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "authMeta", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-meta span, 
          {{ULTP}} .comment-metadata a, 
          {{ULTP}} .comment-meta a`,
      },
    ],
  },
  authImg: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [{ key: "authImg", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-wrapper .ultp-comment-content-heading > img,
          {{ULTP}} .comment-author img {display: inline }`,
      },
      {
        depends: [{ key: "authImg", condition: "==", value: false }],
        selector:
          `{{ULTP}} .ultp-comment-content-heading > img,
          {{ULTP}} .comment-author img {display: none }`,
      },
    ],
  },
  authImgRadius: {
    type: "object",
    default: { lg: "50", unit: "px" },
    style: [
      {
        depends: [{ key: "authImg", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-comment-content-heading > img,
          {{ULTP}} .comment-author img {border-radius: {{authImgRadius}} }`,
      },
    ],
  },
  replyColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-desc,
          {{ULTP}} .comment-content, 
          {{ULTP}} .comment-body .reply ,
          {{ULTP}} .comment-body > p { color:{{replyColor}} }`,
      },
    ],
  },
  replyHovColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-desc:hover, 
          {{ULTP}} .comment-content:hover, 
          {{ULTP}} .comment-body .reply:hover,
          {{ULTP}} .comment-body > p:hover {color: {{replyHovColor}} }`,
      },
    ],
  },
  replyTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-comment-desc, 
          {{ULTP}} .comment-content, 
          {{ULTP}} .comment-body .reply, 
          {{ULTP}} .comment-body > p`,
      },
    ],
  },
  replySeparator: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [{ key: "replySeparator", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-comment-reply > li:after { display: block }",
      },
      {
        depends: [{ key: "replySeparator", condition: "==", value: false }],
        selector:
          "{{ULTP}} .ultp-builder-comment-reply > li:after { display: none }",
      },
    ],
  },
  replySepColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "replySeparator", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-comment-reply > li:after { background-color:{{replySepColor}} }",
      },
    ],
  },
  replyCancelColor: {
    type: "string",
    default: "",
    style: [
      {
        selector: "{{ULTP}} .comment-reply-title a { color:{{replyCancelColor}} !important;  }"
      },
    ],
  },
  replyCancelHoverColor: {
    type: "string",
    default: "",
    style: [
      {
        selector: "{{ULTP}} .comment-reply-title a:hover { color:{{replyCancelHoverColor}} !important;  }"
      },
    ],
  },
  replySepSpace: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "replySeparator", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-comment-reply > li:after { margin:{{replySepSpace}} 0px }",
      },
    ],
  },
  /*============================
      Advanced Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], [ 'loadingColor'])
};
export default attributes;
