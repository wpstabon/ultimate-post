const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-reading-time', {
        title: __('Post Reading Time','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/reading_time.svg'} alt="Post Reading Time"/>,
        category: 'postx-site-builder',
        description: __('Display Expecting Reading Time and customize it as you need.','ultimate-post'),
        keywords: [
            __('Post Reading Time','ultimate-post'),
            __('Post Reading','ultimate-post'),
            __('Reading Time','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)