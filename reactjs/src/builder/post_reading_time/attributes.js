import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  readLabel: { type: "boolean", default: true },
  readIconShow: { type: "boolean", default: true },
  readColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [{ selector: "{{ULTP}} .ultp-read-count { color:{{readColor}} }" }],
  },
  readTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-read-count" }],
  },
  readCountAlign: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper { text-align: {{readCountAlign}};}",
      },
    ],
  },
  readLabelText: {
    type: "string",
    default: "Reading Time",
    style: [{ depends: [{ key: "readLabel", condition: "==", value: true }] }],
  },
  readLabelAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "readLabelAlign", condition: "==", value: "before" },
          { key: "readLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-read-count .ultp-read-label {order: -1; margin-right: 5px;}",
      },
      {
        depends: [
          { key: "readLabelAlign", condition: "==", value: "after" },
          { key: "readLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-read-count .ultp-read-label {order: unset; margin-left: 5px;}",
      },
    ],
  },

  /*============================
      Post Reading Icon Settings
  ============================*/
  iconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "readIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-read-count > svg { fill:{{iconColor}}; stroke:{{iconColor}};}",
      },
    ],
  },
  readIconStyle: {
    type: "string",
    default: "readingTime1",
    style: [
      { depends: [{ key: "readIconShow", condition: "==", value: true }] },
    ],
  },
  readIconSize: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "readIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-read-count svg{ width:{{readIconSize}}; height:{{readIconSize}} }",
      },
    ],
  },
  readSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "readIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-read-count > svg { margin-right: {{readSpace}} }",
      },
      {
        depends: [
          { key: "readIconShow", condition: "==", value: true },
          { key: "readLabelAlign", condition: "==", value: "before" },
        ],
        selector:
          "{{ULTP}} .ultp-read-count > svg { margin: {{readSpace}} } {{ULTP}}  .ultp-read-count .ultp-read-label {margin: 0px !important;}",
      },
    ],
  },
  
  /*============================
      Advanced Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
