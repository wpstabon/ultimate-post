const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import IconPack from "../../helper/fields/tools/IconPack"
import { useBlockId } from '../../helper/hooks/use-blockid'

export default function Edit(props) {
    const {
        setAttributes,
        name,
        attributes,
        clientId,
        className,
        attributes: {
            blockId,
            advanceId,
            readLabelText,
            readIconStyle,
            readLabel,
            readIconShow,
            currentPostId
        }
    } = props;
    const store = { setAttributes, name, attributes, clientId };
    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-reading-time', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')}  >
                        <CommonSettings
                            title={`inline`}
                            include={[
                                {
                                    data: { type: 'toggle', key: 'readLabel', label: __('Enable Label', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'readColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'readTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'readCountAlign', disableJustify: true, responsive: true, label: __('Alignment', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'text', key: 'readLabelText', label: __('Text', 'ultimate-post') }
                                },
                                {
                                    data: {
                                        type: 'group', key: 'readLabelAlign', options: [
                                            { label: "After Content", value: "after" },
                                            { label: "Before Content", value: "before" }
                                        ], justify: true, label: __('Prefix Position', 'ultimate-post')
                                    }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Icon Style', 'ultimate-post')}
                            depend="readIconShow"
                            initialOpen={true}
                            include={[
                                {
                                    data: { type: 'color', key: 'iconColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'icon', key: 'readIconStyle', label: __('Style', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'readIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'readSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <span className={`ultp-read-count`}>
                        {(readIconShow && readIconStyle) &&
                            IconPack[readIconStyle]
                        }
                        <div>12</div>
                        {readLabel &&
                            <span className={`ultp-read-label`}>
                                {readLabelText}
                            </span>
                        }
                    </span>
                </div>
            </div>
        </Fragment>
    )
}
