import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  tagLabelShow: { type: "boolean", default: true },
  tagIconShow: { type: "boolean", default: true },
  tagColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a, {{ULTP}} .tag-builder-content {color:{{tagColor}};}",
      },
    ],
  },
  tagBgColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      { selector: "{{ULTP}} .ultp-builder-tag a {background:{{tagBgColor}};}" },
    ],
  },
  tagItemBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#e2e2e2",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-builder-tag a" }],
  },
  tagRadius: {
    type: "object",
    default: { lg: "2", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a { border-radius:{{tagRadius}}; }",
      },
    ],
  },
  tagHovColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a:hover { color:{{tagHovColor}}; }",
      },
    ],
  },
  tagBgHovColor: {
    type: "string",
    default: "var(--postx_preset_Secondary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a:hover {background:{{tagBgHovColor}};}",
      },
    ],
  },
  tagItemHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#000",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-builder-tag a:hover" }],
  },
  tagHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a:hover{ border-radius:{{tagHoverRadius}}; }",
      },
    ],
  },
  tagTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [{ selector: "{{ULTP}} .ultp-builder-tag a" }],
  },
  tagSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag a:not(:last-child) {margin-right:{{tagSpace}}}",
      },
    ],
  },
  tagItemPad: {
    type: "object",
    default: {
      lg: { top: "0", bottom: "0", left: "10", right: "10", unit: "px" },
    },
    style: [
      { selector: "{{ULTP}} .ultp-builder-tag a{ padding:{{tagItemPad}} }" },
    ],
  },
  tagAlign: {
    type: "object",
    default: {},
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag { justify-content:{{tagAlign}}; }",
      },
    ],
  },
  tagLabel: {
    type: "string",
    default: "Tags: ",
    style: [
      { depends: [{ key: "tagLabelShow", condition: "==", value: true }] },
    ],
  },
  labelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag .tag-builder-label {color:{{labelColor}};}",
      },
    ],
  },
  labelBgColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag .tag-builder-label {background:{{labelBgColor}};}",
      },
    ],
  },
  labelTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-builder-tag .tag-builder-label",
      },
    ],
  },
  tagLabelSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag .tag-builder-label{margin-right:{{tagLabelSpace}}}",
      },
    ],
  },
  tagLabelBorder: {
    type: "object",
    default: { openBorder: 0 },
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-builder-tag .tag-builder-label",
      },
    ],
  },
  tagLabelRadius: {
    type: "object",
    default: {},
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-tag .tag-builder-label{ border-radius:{{tagLabelRadius}}; }",
      },
    ],
  },
  tagLabelPad: {
    type: "object",
    default: {},
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .tag-builder-label{ padding:{{tagLabelPad}} }",
      },
    ],
  },
  tagIconStyle: {
    type: "string",
    default: "",
    style: [
      { depends: [{ key: "tagIconShow", condition: "==", value: true }] },
    ],
  },
  tagIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag svg {fill:{{tagIconColor}}; stroke:{{tagIconColor}} }",
      },
    ],
  },
  tagIconHovColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag svg:hover {fill:{{tagIconHovColor}}; stroke:{{tagIconHovColor}} }",
      },
    ],
  },
  tagIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag svg {height:{{tagIconSize}}; width:{{tagIconSize}};}",
      },
    ],
  },
  tagIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-tag svg {margin-right:{{tagIconSpace}} }",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
