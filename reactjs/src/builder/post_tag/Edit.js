const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import IconPack from "../../helper/fields/tools/IconPack"
import { useBlockId } from '../../helper/hooks/use-blockid'

export default function Edit(props) {

    const { 
		setAttributes, 
		name, 
		attributes, 
		clientId, 
		className, 
		attributes: { 
			blockId, 
			advanceId, 
			tagLabel, 
			tagLabelShow, 
			tagIconShow, 
			tagIconStyle,
            currentPostId
		} 
	} = props;
	const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if (blockId) {
        CssGenerator(attributes, 'ultimate-post/post-tag', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')}  >
                    <CommonSettings
                            title={"inline"}
                            initialOpen={true}
                            include={[
                                {
                                    data: { type: 'alignment', key: 'tagAlign', disableJustify: true, responsive: true, label: __('Alignment', 'ultimate-post'), options: ['flex-start', 'center', 'flex-end'] }
                                },
                                {
                                    data: {
                                        type: 'tab', content: [
                                            {
                                                name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                    { type: 'color', key: 'tagColor', label: __('Color', 'ultimate-post') },
                                                    { type: 'color', key: 'tagBgColor', label: __('Background', 'ultimate-post') },
                                                    { type: 'border', key: 'tagItemBorder', label: __('Border', 'ultimate-post') },
                                                    { type: 'dimension', key: 'tagRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                                ]
                                            },
                                            {
                                                name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                    { type: 'color', key: 'tagHovColor', label: __('Hover Color', 'ultimate-post') },
                                                    { type: 'color', key: 'tagBgHovColor', label: __('Hover Background', 'ultimate-post') },
                                                    { type: 'border', key: 'tagItemHoverBorder', label: __('Hover Border', 'ultimate-post') },
                                                    { type: 'dimension', key: 'tagHoverRadius', label: __('Hover Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                                ]
                                            },
                                        ]
                                    }
                                },
                                {
                                    data: { type: 'typography', key: 'tagTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'tagSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Space Between Tags', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'dimension', key: 'tagItemPad', step: 1, unit: true, responsive: true, label: __('Padding', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Tag Label', 'ultimate-post')}
                            depend="tagLabelShow"
                            initialOpen={false}
                            include={[
                                {
                                    data: { type: 'text', key: 'tagLabel', label: __('Tag Label', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'labelColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'labelTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'tagLabelSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Spacing', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'labelBgColor', label: __('Background', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'dimension', key: 'tagLabelPad', step: 1, unit: true, responsive: true, label: __('Padding', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'border', key: 'tagLabelBorder', label: __('Border', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'dimension', key: 'tagLabelRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Icon Style', 'ultimate-post')}
                            depend="tagIconShow"
                            include={[
                                {
                                    data: { type: 'icon', key: 'tagIconStyle', label: __('Select Icon', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'tagIconColor', label: __('Icon Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'tagIconHovColor', label: __('Hover Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'tagIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'tagIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`} >
                    <div className="ultp-builder-tag" >
                        {
                            tagIconShow && IconPack[tagIconStyle]
                        }
                        <div className="tag-builder-label" >
                            {tagLabelShow && tagLabel}
                        </div>
                        <div className="tag-builder-content" >
                            <a href="#">Dummy Tag 1</a>
                            <a href="#">Dummy Tag 2</a>
                            <a href="#">Dummy Tag 3</a>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}