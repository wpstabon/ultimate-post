const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-tag', {
        title: __('Post Tag','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/post_tag.svg'} alt="Post Tag" />,
        category: 'postx-site-builder',
        description: __('Display Post Tags and style them as you need.','ultimate-post'),
        keywords: [ 
            __('Tag','ultimate-post'),
            __('Post Tags','ultimate-post'),
            __('Tags','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)