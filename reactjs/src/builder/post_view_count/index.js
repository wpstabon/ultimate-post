const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-view-count', {
        title: __('Post View Count','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/view_count.svg'} alt="Post View Count"/>,
        category: 'postx-site-builder',
        description: __('Display the post view count and customize it as you need.','ultimate-post'),
        keywords: [ 
            __('Post View Count','ultimate-post'),
            __('Post View','ultimate-post'),
            __('View','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)