import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  viewLabel: { type: "boolean", default: true },
  viewIconShow: { type: "boolean", default: true },
  viewTypo: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [{ selector: "{{ULTP}} .ultp-view-count > span" }],
  },
  viewCountAlign: {
    type: "object",
    default: [],
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-wrapper { text-align: {{viewCountAlign}};}",
      },
    ],
  },
  viewLabelText: {
    type: "string",
    default: "View",
    style: [{ depends: [{ key: "viewLabel", condition: "==", value: true }] }],
  },
  viewLabelAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "viewLabelAlign", condition: "==", value: "before" },
          { key: "viewLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-view-count .ultp-view-label {order: -1;margin-right: 5px;}",
      },
      {
        depends: [
          { key: "viewLabelAlign", condition: "==", value: "after" },
          { key: "viewLabel", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-view-count .ultp-view-label {order: unset; margin-left: 5px;}",
      },
    ],
  },
  viewIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [{ selector: "{{ULTP}} .ultp-view-count >span{ color:{{viewIconColor}} }" }],
  },
  viewIconStyle: {
    type: "string",
    default: "viewCount1",
    style: [
      { depends: [{ key: "viewIconShow", condition: "==", value: true }] },
    ],
  },
  iconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count > svg { fill:{{iconColor}}; stroke:{{iconColor}};} ",
      },
    ],
  },
  viewIconSize: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count svg{ width:{{viewIconSize}}; height:{{viewIconSize}} }",
      },
    ],
  },
  viewSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count > span.ultp-view-count-number { margin-left: {{viewSpace}} }",
      },
      {
        depends: [
          { key: "viewIconShow", condition: "==", value: true },
          { key: "viewLabelAlign", condition: "==", value: "before" },
        ],
        selector:
          "{{ULTP}} .ultp-view-count > svg { margin: {{viewSpace}} } {{ULTP}} .ultp-view-count .ultp-view-label {margin: 0px !important;}",
      },
    ],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
