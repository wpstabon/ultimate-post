const { __ } = wp.i18n
const { Fragment } = wp.element
const { InspectorControls } = wp.blockEditor
import { _dateFormat, blockSupportLink } from '../../helper/CommonPanel';
import { CssGenerator } from "../../helper/CssGenerator";
import IconPack from "../../helper/fields/tools/IconPack";
import { useBlockId } from '../../helper/hooks/use-blockid';
import Settings from "./Settings";
const { dateI18n } = wp.date;


export default function Edit(props) {

    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
	} = props;
    const { 
        blockId,
        advanceId,
        dateText,
        tagLabel,
        tagLabelShow,
        catLabelShow,
        catLabel,
        viewLabel,
        viewLabelShow,
        cmtLabel,
        cmtLabelShow,
        datePubText,
        authorShow,
        readTimeShow,
        dateShow,
        viewCountShow,
        cmtCountShow,
        catShow,
        tagShow,
        authLabel,
        metaSeparator,
        authLabelShow,
        readTimeText,
        authImgShow,
        readTimePrefix,
        authIconShow,
        DateIconShow,
        readTimeIcon,
        viewIconShow,
        cmtIconShow,
        metaItemSort,
        catAlign,
        tagAlign,
        cmntAlign,
        viewAlign,
        readAlign,
        authAlign,
        dateAlign,
        catIconShow,
        catIconStyle,
        tagIconShow,
        tagIconStyle,
        cmntIconStyle,
        viewIconStyle,
        readIconStyle,
        authIconStyle,
        dateIconStyle,
        dateFormat,
        metaDateFormat,
        enablePrefix,
        currentPostId
    } = attributes;
    const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) { 
        CssGenerator(attributes, 'ultimate-post/advance-post-meta', blockId); 
    }

    const align = catAlign || tagAlign || cmntAlign || viewAlign || readAlign || authAlign || dateAlign;


    const authorContent = (
        <span className="ultp-post-auth ultp-meta-separator">
            <span className="ultp-auth-heading">
                { authImgShow && 
                    <img src={ultp_data.url + 'assets/img/ultp-author.jpg'} alt="author-img"/>
                }
                { authIconShow &&
                    IconPack[authIconStyle]
                }
                { authLabelShow &&
                    <span className="ultp-auth-label">
                        {authLabel}
                    </span>
                }
            </span>
            <span className="ultp-auth-name">Sapiente Delectus</span>
        </span>
    )

    const dateContent = (
        <span className="ultp-date-meta ultp-meta-separator">
            { DateIconShow &&
                <span className="ultp-date-icon">{IconPack[dateIconStyle]}</span>
            }
            { dateFormat == "updated" &&
                <>
                    { enablePrefix &&
                        <span className="ultp-date-prefix">{dateText}</span>
                    }
                    <span className="ultp-post-date__val">
                        {dateI18n(_dateFormat(metaDateFormat))}
                    </span>
                </>
            }
            { dateFormat == "publish" &&
                <>
                    { enablePrefix &&
                        <span className="ultp-date-prefix">{datePubText}</span>
                    }
                    <span className="ultp-post-date__val">
                        {dateI18n(_dateFormat(metaDateFormat))}
                    </span>
                </>
            }
        </span>
    )

    return (
        <Fragment>
            <InspectorControls>
                <Settings store={store} />
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-advance-post-meta ${align ? "ultp-contentMeta-align" : "ultp-contentMeta"} ultp-post-meta-${metaSeparator}`}>
                        <div>
                            {metaItemSort.map((content, i) =>
                                <Fragment key={content}>
                                    { content == "author" && authorShow && !authAlign &&
                                        authorContent
                                    }
                                    { content == "date" && dateShow && !dateAlign &&
                                        dateContent
                                    }
                                    { content == "cmtCount" && cmtCountShow && !cmntAlign &&
                                        <PostContent key={content} title="comment" labelEnable={cmtLabelShow} labelText={cmtLabel} iconEnable={cmtIconShow} iconVal={cmntIconStyle} />
                                    }
                                    { content == "viewCount" && viewCountShow && !viewAlign &&
                                        <PostContent key={content} title="view" labelEnable={viewLabelShow} labelText={viewLabel} iconEnable={viewIconShow} iconVal={viewIconStyle} />
                                    }
                                    { content == "readTime" && readTimeShow && !readAlign &&
                                        <PostContent key={content} title="readTime" labelEnable={readTimePrefix} labelText={readTimeText} iconEnable={readTimeIcon} iconVal={readIconStyle} />
                                    }
                                    { content == "cat" && catShow && !catAlign &&
                                        <PostContent title="cat" key={content} labelEnable={catLabelShow} labelText={catLabel} iconEnable={catIconShow} iconVal={catIconStyle} />
                                    }
                                    { content == "tag" && tagShow && !tagAlign &&
                                        <PostContent key={content} title="tag" labelEnable={tagLabelShow} labelText={tagLabel} iconEnable={tagIconShow} iconVal={tagIconStyle} />
                                    }
                                </Fragment>
                            )}
                        </div>
                        <div>
                            { align && metaItemSort.map((val, i) =>
                                <Fragment key={val + "sort"}>
                                    {val == "author" && authorShow && authAlign && 
                                        authorContent
                                    }
                                    {val == "date" && dateShow && dateAlign &&
                                        dateContent
                                    }
                                    {val == "cmtCount" && cmtCountShow && cmntAlign &&
                                        <PostContent key={val} title="comment" labelEnable={cmtLabelShow} labelText={cmtLabel} iconEnable={cmtIconShow} iconVal={cmntIconStyle} />
                                    }
                                    {val == "viewCount" && viewCountShow && viewAlign &&
                                        <PostContent key={val} title="view" labelEnable={viewLabelShow} labelText={viewLabel} iconEnable={viewIconShow} iconVal={viewIconStyle} />
                                    }
                                    {val == "readTime" && readTimeShow && readAlign &&
                                        <PostContent key={val} title="readTime" labelEnable={readTimePrefix} labelText={readTimeText} iconEnable={readTimeIcon} iconVal={readIconStyle} />
                                    }
                                    {val == "cat" && catShow && catAlign &&
                                        <PostContent title="cat" key={val} labelEnable={catLabelShow} labelText={catLabel} iconEnable={catIconShow} iconVal={catIconStyle} />
                                    }
                                    {val == "tag" && tagShow && tagAlign &&
                                        <PostContent key={val} title="tag" labelEnable={tagLabelShow} labelText={tagLabel} iconEnable={tagIconShow} iconVal={tagIconStyle} />
                                    }
                                </Fragment>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}


const PostContent = ({ title, labelEnable, labelText, iconEnable, iconVal }) => {
    let isTax = title == "tag" || title == "cat";
    let isReadTime = title == "readTime";
    return (
        <span className={`ultp-${title}-wrap ultp-meta-separator`}>
            { isTax &&
                <span className={`ultp-${title}-count`}>
                    {iconEnable && IconPack[iconVal]}
                </span>
            }
            { isTax && labelEnable && 
                <span className={`ultp-${title}-label`}>{labelText}</span>
            }
            { isTax &&
                <span className={`ultp-post-${title}`} >
                    <a>Example{title}</a> <a>Example{title}</a>
                </span>
            }
            {!isTax && !isReadTime &&
                <span className={`ultp-${title}-count`} >
                    {iconEnable && IconPack[iconVal]} 12
                </span>
            }
            { isReadTime && iconEnable && 
                IconPack[iconVal]
            }
            { !isTax && !isReadTime && labelEnable &&
                <span className={`ultp-${title}-label`} >
                    {labelText}
                </span>
            }
            { isReadTime &&
                <>
                    <div>12</div>
                    {
                        labelEnable && 
                        <span className="ultp-read-label">{labelText}</span>
                    }
                </>
            }
        </span>
    )
}