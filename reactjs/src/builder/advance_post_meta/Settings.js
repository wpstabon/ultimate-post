const { __ } = wp.i18n
import { CommonSettings, GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'


export default function Settings({store}) {
    const finalStore = store || { name: 'ultimate-post/post-title', attributes: {} }

    return (
        <Sections>
            <Section slug="setting" title={__('Style', 'ultimate-post')}>
                <CommonSettings initialOpen={true}
                    title={`inline`}
                    include={[
                        {
                            data: { type: 'range', key: 'metaSpacing', label: __('Item Spacing', 'ultimate-post'), min: 0, max: 50, step: 1, unit: true, responsive: true }
                        },
                        {
                            data: { type: 'alignment', key: 'metaAlign', disableJustify: true, label: __('Alignment', 'ultimate-post') }
                        },
                        {
                            data: {
                                type: 'select', key: 'metaSeparator', label: __('Separator', 'ultimate-post'), options: [
                                    { value: 'dot', label: __('Dot', 'ultimate-post') },
                                    { value: 'slash', label: __('Slash', 'ultimate-post') },
                                    { value: 'doubleslash', label: __('Double Slash', 'ultimate-post') },
                                    { value: 'close', label: __('Close', 'ultimate-post') },
                                    { value: 'dash', label: __('Dash', 'ultimate-post') },
                                    { value: 'verticalbar', label: __('Vertical Bar', 'ultimate-post') },
                                    { value: 'emptyspace', label: __('Empty', 'ultimate-post') }]
                            },
                        },
                        {
                            data: { type: 'color', key: 'separatorColor', label: __('Separator Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'color', key: 'metaCommonColor', label: __('Common Color', 'ultimate-post') }
                        },
                        {
                            data: { type: 'typography', key: 'commonTypo', label: __('Common Typography', 'ultimate-post') }
                        },
                        {
                            data: {
                                type: "sort", key: "metaItemSort", label: __('Meta List', 'ultimate-post'), options: {
                                    author: {
                                        label: __('Post Author', 'ultimate-post'),
                                        action: 'authorShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'authColor', label: __('Author Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'authHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'authTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            // Avatar
                                            {
                                                type: 'separator', label: 'Avatar',
                                            },
                                            {
                                                type: 'toggle', key: 'authImgShow', label: __('Author Avatar', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authImgSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authImgRadius', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Radius', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authImgSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Space X', 'ultimate-post')
                                            },
                                            // Label
                                            {
                                                type: 'separator', label: 'Label',
                                            },
                                            {
                                                type: 'toggle', key: 'authLabelShow', label: __('Author Label', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'authLabel', label: __('Author Label Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'authLabelColor', label: __('Label Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'authLabelTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authLabelSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Space X', 'ultimate-post')
                                            },
                                            // Auth Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'authIconShow', pro: true, label: __('Enable Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'icon', key: 'authIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'authIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'authIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'authAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    date: {
                                        label: __('Post Publish Date', 'ultimate-post'),
                                        action: 'dateShow',
                                        inner: [
                                            {
                                                type: "group", key: "dateFormat",
                                                options: [
                                                    { value: "publish", label: "Publish Date" },
                                                    { value: "updated", label: "Updated Date" },
                                                ],
                                                justify: true,
                                                label: __('Date Format', 'ultimate-post')
                                            },
                                            {
                                                type: 'select', key: 'metaDateFormat', label: __('Date/Time Format', 'ultimate-post'),
                                                options: [
                                                    { value: 'M j, Y', label: 'Feb 7, 2022' },
                                                    { value:'default_date',label: 'WordPress Default Date Format', pro: true },
                                                    { value:'default_date_time',label: 'WordPress Default Date & Time Format', pro: true },
                                                    { value: 'g:i A', label: '1:12 PM', pro: true },
                                                    { value: 'F j, Y', label: 'February 7, 2022', pro: true },
                                                    { value: 'F j, Y g:i A', label: 'February 7, 2022 1:12 PM', pro: true },
                                                    { value: 'M j, Y g:i A', label: 'Feb 7, 2022 1:12 PM', pro: true },
                                                    { value: 'j M Y', label: '7 Feb 2022', pro: true },
                                                    { value: 'j M Y g:i A', label: '7 Feb 2022 1:12 PM', pro: true },
                                                    { value: 'j F Y', label: '7 February 2022', pro: true },
                                                    { value: 'j F Y g:i A', label: '7 February 2022 1:12 PM', pro: true },
                                                    { value: 'j. M Y', label: '7. Feb 2022', pro: true },
                                                    { value: 'j. M Y | H:i', label: '7. Feb 2022 | 1:12', pro: true },
                                                    { value: 'j. F Y', label: '7. February 2022', pro: true },
                                                    { value: 'j.m.Y', label: '7.02.2022', pro: true },
                                                    { value: 'j.m.Y g:i A', label: '7.02.2022 1:12 PM', pro: true },
                                                    { value: 'Y m j', label: '2022 7 02', pro: true },
                                                ]
                                            },
                                            {
                                                type: 'color', key: 'dateColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'dateTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            // Prefix
                                            {
                                                type: 'separator', label: 'Prefix',
                                            },
                                            {
                                                type: 'toggle', key: 'enablePrefix', label: __('Enable Prefix', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'datePubText', label: __('Date Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'dateText', label: __('Date Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'datePrefixColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'DateIconShow', label: __('Date Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'icon', key: 'dateIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'dateIconColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'dateIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'dateIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'dateAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    cmtCount: {
                                        label: __('Comments', 'ultimate-post'),
                                        action: 'cmtCountShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'commentColor', label: __(' Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'commentHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'commentTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            // Prefix
                                            {
                                                type: 'separator', label: 'Prefix',
                                            },
                                            {
                                                type: 'toggle', key: 'cmtLabelShow', label: __('Prefix Enable', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'cmtLabel', label: __('Prefix Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'cmtLabelColor', label: __('Prefix Color', 'ultimate-post')
                                            },
                                            { 
                                                type: 'group', key: 'commentLabelAlign', options:[
                                                    {  label:"After Content", value: "after"  },
                                                    {  label:"Before Content", value: "before"  }
                                                ], justify: true, label: __('Prefix Position' ,'ultimate-post') 
                                            },
                                            // Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'cmtIconShow', pro: true, label: __('Enable Icon ', 'ultimate-post')
                                            },
                                            {
                                                type: 'icon', key: 'cmntIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'cmntIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'commentIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'cmntIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'cmntAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    viewCount: {
                                        label: __('Views', 'ultimate-post'),
                                        action: 'viewCountShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'viewColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'viewHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'viewTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            // Prefix
                                            {
                                                type: 'separator', label: 'Prefix',
                                            },
                                            {
                                                type: 'toggle', key: 'viewLabelShow', label: __('Enable Prefix', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'viewLabel', label: __('Prefix Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'viewLabelColor', label: __('Prefix Color', 'ultimate-post')
                                            },
                                            { 
                                                type: 'group', key: 'viewLabelAlign', options:[
                                                    {  label:"After Content", value: "after"  },
                                                    {  label:"Before Content", value: "before"  }
                                                ], justify: true, label: __('Prefix Position' ,'ultimate-post') 
                                            },
                                            // Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'viewIconShow', label: __('Enable Icon', 'ultimate-post'), pro: true
                                            },
                                            {
                                                type: 'icon', key: 'viewIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'viewIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'viewIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'viewIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'viewAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    readTime: {
                                        label: __('Reading Time', 'ultimate-post'),
                                        action: 'readTimeShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'readColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'readHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'readTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            // Prefix
                                            {
                                                type: 'separator', label: 'Prefix',
                                            },
                                            {
                                                type: 'toggle', key: 'readTimePrefix', label: __('Enable Prefix', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'readTimeText', label: __('Time Text', 'ultimate-post')
                                            },
                                            { 
                                                type: 'group', key: 'readPrefixAlign', options:[
                                                    {  label:"After Content", value: "after"  },
                                                    {  label:"Before Content", value: "before"  }
                                                ], justify: true, label: __('Prefix Position' ,'ultimate-post') 
                                            },
                                            // Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'readTimeIcon', label: __('Enable Icon', 'ultimate-post'), pro: true
                                            },
                                            {
                                                type: 'icon', key: 'readIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'readIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'readIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'readIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'readAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    cat: {
                                        label: __('Category', 'ultimate-post'),
                                        action: 'catShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'catColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'catHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'catTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'catSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Category Spacing', 'ultimate-post')
                                            },
                                            // Label
                                            {
                                                type: 'separator', label: 'Label',
                                            },
                                            {
                                                type: 'toggle', key: 'catLabelShow', label: __('Category Label Show', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'catLabel', label: __('Category Label Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'catLabelColor', label: __('Label Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'catLabelSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Label Spacing', 'ultimate-post')
                                            },
                                            // Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'toggle', key: 'catIconShow', label: __('Enable Icon', 'ultimate-post'), pro: true
                                            },
                                            {
                                                type: 'icon', key: 'catIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'catIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'catIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'catIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'catAlign', label: __('Align Right', 'ultimate-post')
                                            }
                                        ]
                                    },
                                    tag: {
                                        label: __('Tags', 'ultimate-post'),
                                        action: 'tagShow',
                                        inner: [
                                            {
                                                type: 'color', key: 'tagColor', label: __('Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'tagHovColor', label: __('Hover Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'typography', key: 'tagTypo', label: __('Typography', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'tagSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Category Spacing', 'ultimate-post')
                                            },
                                            // Label
                                            {
                                                type: 'separator', label: 'Label',
                                            },
                                            {
                                                type: 'toggle', key: 'tagLabelShow', label: __('Tag Label Show', 'ultimate-post')
                                            },
                                            {
                                                type: 'text', key: 'tagLabel', label: __('Tag Label Text', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'tagLabelColor', label: __('Label Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'tagLabelSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Label Spacing', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'tagIconShow', label: __('Enable Icon', 'ultimate-post'), pro: true
                                            },
                                            // Icon
                                            {
                                                type: 'separator', label: 'Icon',
                                            },
                                            {
                                                type: 'icon', key: 'tagIconStyle', label: __('Select Icon', 'ultimate-post')
                                            },
                                            {
                                                type: 'color', key: 'tagIconColor', label: __('Icon Color', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'tagIconSize', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post')
                                            },
                                            {
                                                type: 'range', key: 'tagIconSpace', min: 0, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post')
                                            },
                                            {
                                                type: 'toggle', key: 'tagAlign', label: __('Right Align', 'ultimate-post')
                                            }
                                        ]
                                    },
                                }
                            }
                        }
                    ]}
                    store={finalStore} />
            </Section>
            <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                <GeneralAdvanced store={finalStore} />
                <ResponsiveAdvanced pro={true} store={finalStore} />
                <CustomCssAdvanced store={finalStore} />
            </Section>
        </Sections>
    )
}