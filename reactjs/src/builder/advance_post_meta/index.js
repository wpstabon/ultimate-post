const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/advance-post-meta', {
        title: __('Advanced Post Meta','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/post_meta.svg'} alt="Advanced Post Meta" />,
        category: 'postx-site-builder',
        description: __('Display Advance Post Meta with the ultimate controls.','ultimate-post'),
        keywords: [ 
            __('Advanced Post Meta','ultimate-post'),
            __('Post Meta','ultimate-post'),
            __('Meta','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)