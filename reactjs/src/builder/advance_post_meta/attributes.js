import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      Advanced Post Meta Settings
  ============================*/
  authorShow: { 
    type: "boolean", 
    default: true 
  },
  dateShow: {
    type: "boolean", 
    default: true 
  },
  cmtCountShow: {
    type: "boolean", 
    default: true 
  },
  viewCountShow: {
    type: "boolean", 
    default: false 
  },
  readTimeShow: {
    type: "boolean", 
    default: false 
  },
  catShow: {
    type: "boolean", 
    default: false 
  },
  tagShow: {
    type: "boolean", 
    default: false 
  },
  metaSpacing: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-meta-separator::after {margin: 0 {{metaSpacing}};}",
      },
      {
        depends: [
          { key: "metaSeparator", condition: "==", value: "emptyspace" },
        ],
        selector:
          "{{ULTP}} .ultp-advance-post-meta > div { gap:{{metaSpacing}}; }",
      },
    ],
  },
  metaAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [
          { key: "metaAlign", condition: "==", value: "right" },
          { key: "authAlign", condition: "==", value: false },
          { key: "dateAlign", condition: "==", value: false },
          { key: "cmntAlign", condition: "==", value: false },
          { key: "viewAlign", condition: "==", value: false },
          { key: "readAlign", condition: "==", value: false },
          { key: "catAlign", condition: "==", value: false },
          { key: "tagAlign", condition: "==", value: false },
        ],
        selector:
          `{{ULTP}} .ultp-block-wrapper .ultp-advance-post-meta, 
          {{ULTP}} .ultp-contentMeta > div { justify-content:{{metaAlign}}; }`,
      },
      {
        depends: [
          { key: "metaAlign", condition: "==", value: "center" },
          { key: "authAlign", condition: "==", value: false },
          { key: "dateAlign", condition: "==", value: false },
          { key: "cmntAlign", condition: "==", value: false },
          { key: "viewAlign", condition: "==", value: false },
          { key: "readAlign", condition: "==", value: false },
          { key: "catAlign", condition: "==", value: false },
          { key: "tagAlign", condition: "==", value: false },
        ],
        selector:
          `{{ULTP}} .ultp-block-wrapper .ultp-advance-post-meta, 
          {{ULTP}} .ultp-contentMeta > div { justify-content:{{metaAlign}}; }`,
      },
      {
        depends: [
          { key: "metaAlign", condition: "==", value: "left" },
          { key: "authAlign", condition: "==", value: false },
          { key: "dateAlign", condition: "==", value: false },
          { key: "cmntAlign", condition: "==", value: false },
          { key: "viewAlign", condition: "==", value: false },
          { key: "readAlign", condition: "==", value: false },
          { key: "catAlign", condition: "==", value: false },
          { key: "tagAlign", condition: "==", value: false },
        ],
        selector:
          `{{ULTP}} .ultp-block-wrapper .ultp-advance-post-meta, 
          {{ULTP}} .ultp-contentMeta > div { justify-content:{{metaAlign}}; }`,
      },
    ],
  },
  metaSeparator: { 
    type: "string", 
    default: "dot" 
  },
  separatorColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-meta-separator::after { color: {{separatorColor}};}",
      },
      {
        depends: [{ key: "metaSeparator", condition: "==", value: "dot" }],
        selector:
          "{{ULTP}} .ultp-meta-separator::after { background:{{separatorColor}};}",
      },
    ],
  },
  metaCommonColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector: "{{ULTP}} .ultp-advance-post-meta  span { color:{{metaCommonColor}};}",
      },
    ],
  },
  commonTypo: {
    type: "object",
    default: { openTypography: 1, size: { lg: 15, unit: "px" }, height: { lg: 20, unit: "px" } },
    style: [
      { selector: "{{ULTP}}  .ultp-advance-post-meta div > span > span"  },
    ],
  },
  metaItemSort: {
    type: "array",
    default: [
      "author",
      "date",
      "cmtCount",
      "viewCount",
      "readTime",
      "cat",
      "tag",
    ],
  },
/*============================
    Post Author Style
============================*/
  authColor: {
    type: "string",
    default: "",
    style: [{ 
      selector: 
      `{{ULTP}} .ultp-advance-post-meta span.ultp-auth-name, 
      {{ULTP}} .ultp-advance-post-meta a.ultp-auth-name { color:{{authColor}} }` 
    }],
  },
  authHovColor: {
    type: "string",
    default: "",
    style: [
      { selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-auth-name:hover, 
        {{ULTP}} .ultp-advance-post-meta a.ultp-auth-name:hover { color:{{authHovColor}} }` 
      },
    ],
  },
  authTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      { 
        selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-auth-name, 
        {{ULTP}} .ultp-advance-post-meta span.ultp-auth-heading, 
        {{ULTP}} .ultp-advance-post-meta a.ultp-auth-name` 
      },
    ],
  },
  authImgShow: { 
    type: "boolean", 
    default: false 
  },
  authImgSize: {
    type: "object",
    default: { lg: "18", unit: "px" },
    style: [
      {
        depends: [{ key: "authImgShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading img { width:{{authImgSize}}; height:{{authImgSize}} }",
      },
    ],
  },
  authImgRadius: {
    type: "object",
    default: { lg: "50", unit: "px" },
    style: [
      {
        depends: [{ key: "authImgShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading img { border-radius:{{authImgRadius}} }",
      },
    ],
  },
  authImgSpace: {
    type: "object",
    default: { lg: "5", unit: "px" },
    style: [
      {
        depends: [{ key: "authImgShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading img { margin-right:{{authImgSpace}} }",
      },
    ],
  },
  authLabelShow: { 
    type: "boolean", 
    default: true 
  },
  authLabel: {
    type: "string",
    default: "Author",
    style: [
      { depends: [{ key: "authLabelShow", condition: "==", value: true }] },
    ],
  },
  authLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "authLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-auth-heading .ultp-auth-label { color:{{authLabelColor}} }",
      },
    ],
  },
  authLabelTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        depends: [{ key: "authLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-auth-heading .ultp-auth-label",
      },
    ],
  },
  authLabelSpace: {
    type: "object",
    default: { lg: "5", unit: "px" },
    style: [
      {
        depends: [{ key: "authLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading .ultp-auth-label { margin-right:{{authLabelSpace}}; }",
      },
    ],
  },
  authIconShow: { type: "boolean", default: false },
  authIconStyle: {
    type: "string",
    default: "author1",
    style: [
      { depends: [{ key: "authIconShow", condition: "==", value: true }] },
    ],
  },
  authIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "authIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading svg { fill:{{authIconColor}}; stroke:{{authIconColor}} }",
      },
    ],
  },
  authIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "authIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading svg { height:{{authIconSize}}; width:{{authIconSize}} }",
      },
    ],
  },
  authIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "authIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-auth-heading svg { margin-right:{{authIconSpace}} }",
      },
    ],
  },
  authAlign: {
    type: "boolean",
    default: false,
    style: [{ depends: [{ key: "authorShow", condition: "==", value: true }] }],
  },
  /*============================
      Post Publish Time Settings
  ============================*/
  dateFormat: { type: "string", default: "updated" },
  metaDateFormat: { type: "string", default: "M j, Y" },
  dateColor: {
    type: "string",
    default: "",
    style: [
      {
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-date-meta span.ultp-post-date__val { color:{{dateColor}} }",
      },
      /* {
        depends: [{ key: "dateFormat", condition: "==", value: "updated" }],
        selector:
          "{{ULTP}} .ultp-advance-post-meta  span.ultp-post-date__val { color:{{dateColor}} }",
      }, */
    ],
  },
  dateTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        // depends: [{ key: "dateFormat", condition: "==", value: "publish" }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-date-meta > span",
      },
      // {
      //   depends: [{ key: "dateFormat", condition: "==", value: "updated" }],
      //   selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-date-meta",
      // },
    ],
  },
  enablePrefix: { type: "boolean", default: true },
  datePubText: {
    type: "string",
    default: "Publish Update",
    style: [
      {
        depends: [
          { key: "enablePrefix", condition: "==", value: true },
          { key: "dateFormat", condition: "==", value: "publish" },
        ],
      },
    ],
  },
  dateText: {
    type: "string",
    default: "Latest Update",
    style: [
      {
        depends: [
          { key: "dateFormat", condition: "==", value: "updated" },
          { key: "enablePrefix", condition: "==", value: true },
        ],
      },
    ],
  },
  datePrefixColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "enablePrefix", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-date-meta > span.ultp-date-prefix {color:{{datePrefixColor}}}",
      },
    ],
  },
  DateIconShow: { type: "boolean", default: false },
  dateIconStyle: {
    type: "string",
    default: "date1",
    style: [
      { depends: [{ key: "DateIconShow", condition: "==", value: true }] },
    ],
  },
  dateIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "DateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-icon svg { fill:{{dateIconColor}}; stroke:{{dateIconColor}}; }",
      },
    ],
  },
  dateIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "DateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-icon svg { width:{{dateIconSize}}; height:{{dateIconSize}} }",
      },
    ],
  },
  dateIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "DateIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-date-icon svg { margin-right:{{dateIconSpace}} }",
      },
    ],
  },
  dateAlign: {
    type: "boolean",
    default: false,
    style: [{ depends: [{ key: "dateShow", condition: "==", value: true }] }],
  },

/*============================
    Comment Style
============================*/
  commentColor: {
    type: "string",
    default: "",
    style: [
      { selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-comment-count { color:{{commentColor}} }" },
    ],
  },
  commentHovColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-advance-post-meta span.ultp-comment-count:hover { color:{{commentHovColor}} }",
      },
    ],
  },
  commentTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      {
        selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-comment-count, 
        {{ULTP}} .ultp-advance-post-meta span.ultp-comment-label`,
      },
    ],
  },
  cmtLabelShow: { type: "boolean", default: true },
  cmtLabel: {
    type: "string",
    default: "Comment",
    style: [
      { depends: [{ key: "cmtLabelShow", condition: "==", value: true }] },
    ],
  },
  cmtLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "cmtLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-comment-label { color:{{cmtLabelColor}} }",
      },
    ],
  },
  commentLabelAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "commentLabelAlign", condition: "==", value: "before" },
          { key: "cmtLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-comment-label {order: -1;margin-right: 5px;}",
      },
      {
        depends: [
          { key: "commentLabelAlign", condition: "==", value: "after" },
          { key: "cmtLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-comment-label {order: 0; margin-left: 5px;}",
      },
    ],
  },
  cmtIconShow: { type: "boolean", default: false },
  cmntIconStyle: {
    type: "string",
    default: "commentCount1",
    style: [
      { depends: [{ key: "cmtIconShow", condition: "==", value: true }] },
    ],
  },
  cmntIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "cmtIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count svg { fill:{{cmntIconColor}}; stroke:{{cmntIconColor}}}",
      },
    ],
  },
  commentIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "cmtIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count svg{ width:{{commentIconSize}}; height:{{commentIconSize}} }",
      },
    ],
  },
  cmntIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "cmtIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-comment-count svg { margin-right:{{cmntIconSpace}} }",
      },
      {
        depends: [
          { key: "cmtIconShow", condition: "==", value: true },
          { key: "cmtLabelShow", condition: "==", value: true },
          { key: "commentLabelAlign", condition: "==", value: "before" },
        ],
        selector:
          `{{ULTP}} .ultp-comment-count svg { margin:0px {{cmntIconSpace}} } 
          {{ULTP}} .ultp-comment-label {margin:0px !important;}`,
      },
    ],
  },
  cmntAlign: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "cmtCountShow", condition: "==", value: true }] },
    ],
  },
  /*============================
      View Settings
  ============================*/
  viewColor: {
    type: "string",
    default: "",
    style: [{ selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-view-count { color:{{viewColor}} }" }],
  },
  viewHovColor: {
    type: "string",
    default: "",
    style: [
      {
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-view-count:hover { color:{{viewHovColor}} }",
      },
    ],
  },
  viewTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [{ 
        selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-view-count, 
        {{ULTP}} .ultp-advance-post-meta span.ultp-view-label` 
      }],
  },
  viewLabelShow: { type: "boolean", default: true },
  viewLabel: {
    type: "string",
    default: "View",
    style: [
      { depends: [{ key: "viewLabelShow", condition: "==", value: true }] },
    ],
  },
  viewLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "viewLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-view-label { color:{{viewLabelColor}} }",
      },
    ],
  },
  viewLabelAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "viewLabelAlign", condition: "==", value: "before" },
          { key: "viewLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-view-label {order: -1;margin-right: 5px;}",
      },
      {
        depends: [
          { key: "viewLabelAlign", condition: "==", value: "after" },
          { key: "viewLabelShow", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-view-label {order: 0; margin-left: 5px;}",
      },
    ],
  },
  viewIconShow: { type: "boolean", default: false },
  viewIconStyle: {
    type: "string",
    default: "viewCount1",
    style: [
      { depends: [{ key: "viewIconShow", condition: "==", value: true }] },
    ],
  },
  viewIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count svg { fill:{{viewIconColor}}; stroke:{{viewIconColor}} }",
      },
    ],
  },
  viewIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count svg{ width:{{viewIconSize}}; height:{{viewIconSize}} }",
      },
    ],
  },
  viewIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "viewIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-view-count svg {margin-right:{{viewIconSpace}}}",
      },
      {
        depends: [
          { key: "viewIconShow", condition: "==", value: true },
          { key: "viewLabelShow", condition: "==", value: true },
          { key: "viewLabelAlign", condition: "==", value: "before" },
        ],
        selector:
          `{{ULTP}} .ultp-view-count svg { margin:0px {{viewIconSpace}} } 
          {{ULTP}} .ultp-view-label {margin:0px !important;}`,
      },
    ],
  },
  viewAlign: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "viewCountShow", condition: "==", value: true }] },
    ],
  },

/*============================
    Reading Time Style
============================*/
  readColor: {
    type: "string",
    default: "",
    style: [
      { selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-readTime-wrap { color:{{readColor}} }" },
    ],
  },
  readHovColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-advance-post-meta span.ultp-readTime-wrap:hover { color:{{readHovColor}} }",
      },
    ],
  },
  readTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [{ selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-readTime-wrap *" }],
  },
  readTimePrefix: { type: "boolean", default: true },
  readTimeText: {
    type: "string",
    default: "Minute Read",
    0: { depends: [{ key: "readTimePrefix", condition: "==", value: true }] },
  },
  readPrefixAlign: {
    type: "string",
    default: "after",
    style: [
      {
        depends: [
          { key: "readPrefixAlign", condition: "==", value: "before" },
          { key: "readTimePrefix", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-read-label {order: -1;margin-right: 5px;}",
      },
      {
        depends: [
          { key: "readPrefixAlign", condition: "==", value: "after" },
          { key: "readTimePrefix", condition: "==", value: true },
        ],
        selector: "{{ULTP}} .ultp-read-label {order: 0; margin-left: 5px;}",
      },
    ],
  },
  readTimeIcon: { type: "boolean", default: false },
  readIconStyle: {
    type: "string",
    default: "readingTime2",
    style: [
      { depends: [{ key: "readTimeIcon", condition: "==", value: true }] },
    ],
  },
  readIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "readTimeIcon", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-readTime-wrap svg { fill:{{readIconColor}}; stroke:{{readIconColor}}; }",
      },
    ],
  },
  readIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "readTimeIcon", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-readTime-wrap svg { width:{{readIconSize}}; height:{{readIconSize}} }",
      },
    ],
  },
  readIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "readTimeIcon", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-readTime-wrap svg { margin-right:{{readIconSpace}} }",
      },
      {
        depends: [
          { key: "readTimeIcon", condition: "==", value: true },
          { key: "readTimePrefix", condition: "==", value: true },
          { key: "readPrefixAlign", condition: "==", value: "before" },
        ],
        selector:
          `{{ULTP}} .ultp-readTime-wrap svg { margin:0px {{readIconSpace}} } 
          {{ULTP}} .ultp-read-label { margin:0px !important;}`,
      },
    ],
  },
  readAlign: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "readTimeShow", condition: "==", value: true }] },
    ],
  },
/*============================
    Categories Style
============================*/
  catColor: {
    type: "string",
    default: "",
    style: [{ selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-post-cat a { color:{{catColor}} }" }],
  },
  catHovColor: {
    type: "string",
    default: "",
    style: [
      { selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-post-cat a:hover { color:{{catHovColor}} }" },
    ],
  },
  catTypo: {
    type: "object",
    default: {
      openTypography: 0,
      decoration: "none",
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [{
      selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-post-cat a, 
        {{ULTP}} .ultp-advance-post-meta span.ultp-cat-label` 
      }],
  },
  catSpace: {
    type: "object",
    default: { lg: "7", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-cat a:not(:first-child) { margin-left:{{catSpace}} }",
      },
    ],
  },
  catLabelShow: { type: "boolean", default: true },
  catLabel: {
    type: "string",
    default: "Category",
    style: [
      { depends: [{ key: "catLabelShow", condition: "==", value: true }] },
    ],
  },
  catLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-cat-label { color:{{catLabelColor}} }",
      },
    ],
  },
  catLabelSpace: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-cat-label { margin-right:{{catLabelSpace}};}",
      },
    ],
  },
  catIconShow: { type: "boolean", default: false },
  catIconStyle: {
    type: "string",
    default: "cat2",
    style: [
      { depends: [{ key: "catIconShow", condition: "==", value: true }] },
    ],
  },
  catIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-cat-wrap svg { height:{{catIconSize}}; width:{{catIconSize}} }",
      },
    ],
  },
  catIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-cat-wrap svg, 
          {{ULTP}} .ultp-cat-wrap svg path, 
          {{ULTP}} .ultp-cat-wrap svg rect{ fill:{{catIconColor}}; stroke:{{catIconColor}} }`,
      },
    ],
  },
  catIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-cat-wrap svg {margin-right:{{catIconSpace}} }",
      },
    ],
  },
  catAlign: {
    type: "boolean",
    default: false,
    style: [{ depends: [{ key: "catShow", condition: "==", value: true }] }],
  },

/*============================
      Tag Style
  ============================*/
  tagColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [{ selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-post-tag a { color:{{tagColor}} }" }],
  },
  tagHovColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      { selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-post-tag a:hover { color:{{tagHovColor}} }" },
    ],
  },
  tagTypo: {
    type: "object",
    default: {
      openTypography: 0,
      decoration: "none",
      size: { lg: 15, unit: "px" },
      height: { lg: 20, unit: "px" },
    },
    style: [
      { selector: 
        `{{ULTP}} .ultp-advance-post-meta span.ultp-post-tag a, 
        {{ULTP}} .ultp-advance-post-meta span.ultp-tag-label` 
      },
    ],
  },
  tagSpace: {
    type: "object",
    default: { lg: "7", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-post-tag a:not(:first-child) { margin-left:{{tagSpace}};}",
      },
    ],
  },
  tagLabelShow: { type: "boolean", default: true },
  tagLabel: {
    type: "string",
    default: "Tag - ",
    style: [
      { depends: [{ key: "tagLabelShow", condition: "==", value: true }] },
    ],
  },
  tagLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-advance-post-meta span.ultp-tag-label { color:{{tagLabelColor}} }",
      },
    ],
  },
  tagLabelSpace: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
      {
        depends: [{ key: "tagLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-tag-label { margin-right:{{tagLabelSpace}};}",
      },
    ],
  },
  tagIconShow: { type: "boolean", default: false },
  tagIconStyle: {
    type: "string",
    default: "tag2",
    style: [
      { depends: [{ key: "tagIconShow", condition: "==", value: true }] },
    ],
  },
  tagIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-tag-wrap svg, 
          {{ULTP}} .ultp-tag-wrap svg path {fill:{{tagIconColor}}; stroke:{{tagIconColor}} }`,
      },
    ],
  },
  tagIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-tag-wrap svg {height:{{tagIconSize}}; width:{{tagIconSize}}}",
      },
    ],
  },
  tagIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "tagIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-tag-wrap svg {margin-right:{{tagIconSpace}} }",
      },
    ],
  },
  tagAlign: {
    type: "boolean",
    default: false,
    style: [{ depends: [{ key: "tagShow", condition: "==", value: true }] }],
  },
  
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
