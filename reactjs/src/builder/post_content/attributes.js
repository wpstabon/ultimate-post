import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  inheritWidth: {
    type: "boolean",
    default: false,
    style: [
      {
        selector:
          "{{ULTP}} > .ultp-block-wrapper .ultp-builder-content { margin:0 auto; max-width: 700px; width:100%;}",
      },
    ],
  },
  contentWidth: {
    type: "object",
    default: { lg: "", ulg: "px" },
    style: [
      {
        depends: [{ key: "inheritWidth", condition: "!=", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper .ultp-builder-content {margin: 0 auto; max-width:{{contentWidth}}; width:100%}",
      },
    ],
  },
  descColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} >  .ultp-block-wrapper .ultp-builder-content, {{ULTP}} > .ultp-block-wrapper .ultp-builder-content p {color:{{descColor}} ;}",
      },
    ],
  },
  linkColor:{
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} >  .ultp-block-wrapper .ultp-builder-content a, {{ULTP}} >  .ultp-block-wrapper .ultp-builder-content * a {color:{{linkColor}} ;}",
      },
    ],
  },
  descTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "14", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        selector:
          "{{ULTP}} > .ultp-block-wrapper .ultp-builder-content, html :where(.editor-styles-wrapper) {{ULTP}} > .ultp-block-wrapper .ultp-builder-content p",
      },
    ],
  },
  contentAlign: {
    type: "string",
    default: "0 auto",
    style: [
      {
        depends: [{ key: "inheritWidth", condition: "!=", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper .ultp-builder-content {margin:{{contentAlign}} }",
      },
    ],
  },
  showCap: { type: "boolean", default: false },
  firstCharSize: {
    type: "string",
    default: "110",
    style: [
      {
        depends: [{ key: "showCap", condition: "==", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper > .ultp-builder-content > p:first-child::first-letter {font-size:{{firstCharSize}}px;float:left;}",
      },
    ],
  },
  firstCharXSpace: {
    type: "string",
    default: "25",
    style: [
      {
        depends: [{ key: "showCap", condition: "==", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper > .ultp-builder-content > p:first-child::first-letter {margin-right:{{firstCharXSpace}}px;}",
      },
    ],
  },
  firstCharYSpace: {
    type: "string",
    default: "100",
    style: [
      {
        depends: [{ key: "showCap", condition: "==", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper > .ultp-builder-content > p:first-child::first-letter {line-height:{{firstCharYSpace}}px;}",
      },
    ],
  },
  firstLatterColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "showCap", condition: "==", value: true }],
        selector:
          "{{ULTP}} > .ultp-block-wrapper > .ultp-builder-content > p:first-child::first-letter {color:{{firstLatterColor}};}",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], ['loadingColor'])
};
export default attributes;
