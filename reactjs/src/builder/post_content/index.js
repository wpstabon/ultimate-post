const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-content', {
        title: __('Post Content','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/content.svg'} alt='Post Content'/>,
        category: 'postx-site-builder',
        description: __('Display Post Content in a customized and organized way.','ultimate-post'),
        keywords: [
            __('Content','ultimate-post'),
            __('Desctiption','ultimate-post'),
            __('Post Content','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)