const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, 
    CustomCssAdvanced, 
    GeneralAdvanced, 
    ResponsiveAdvanced, 
    blockSupportLink 
} from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { useBlockId } from '../../helper/hooks/use-blockid'
import { Section, Sections } from '../../helper/Sections'

export default function Edit(props) {
    const {
        setAttributes,
        name,
        attributes,
        clientId,
        className,
        attributes: {
            blockId,
            advanceId,
            showCap,
            currentPostId
        }
    } = props;
    const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if ( blockId ) {
        CssGenerator(attributes, 'ultimate-post/post-content', blockId);
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Style', 'ultimate-post')}>
                        <CommonSettings
                            initialOpen={true}
                            title={`inline`}
                            include={[
                                {
                                    data: { type: 'toggle', key: 'showCap', label: __('Enable Drop Cap', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'toggle', key: 'inheritWidth', label: __('Inherit Default Width', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'contentWidth', step: 1, responsive: true, unit: true, min: 0, max: 1000, label: __('Content Width', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'descColor', label: __('Text Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'linkColor', label: __('Link Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'descTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'alignment', key: 'contentAlign', responsive: true, label: __('Alignment', 'ultimate-post'), options: ['0 auto 0 0', '0 auto', '0 0 0 auto'] }
                                }
                            ]}
                            store={store} />
                        {
                            showCap &&  <CommonSettings
                            title={__('Drop Cap Style', 'ultimate-post')}
                            include={[
                                {
                                    data: { type: 'range', key: 'firstCharSize', min: 1, max: 200, label: __('First Latter Size', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'firstCharYSpace', min: 1, max: 300, label: __('Vertical Space', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'firstCharXSpace', min: 1, max: 300, label: __('Horizontal Space', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'firstLatterColor', label: __('Color', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        }
                        
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className={`ultp-builder-content`}>
                        <p>{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget faucibus enim. Vivamus ac dui euismod velit convallis consequat. Aliquam eget malesuada nisi. Etiam mauris neque, varius vel blandit non, imperdiet facilisis eros. Aliquam ac odio id mi cursus posuere. Nunc in ex nec justo iaculis sodales. Donec ullamcorper sem non metus eleifend, sed molestie urna egestas. Vestibulum gravida mattis varius. Vivamus nec nulla mi. Nulla varius quam in lorem molestie posuere.', 'ultimate-post')}</p>
                        <h2>{__('Heading 2', 'ultimate-post')}</h2>
                        <h3>{__('Heading 3', 'ultimate-post')}</h3>
                        <h4>{__('Heading 4', 'ultimate-post')}</h4>
                        <h5>{__('Heading 5', 'ultimate-post')}</h5>
                        <h6>{__('Heading 6', 'ultimate-post')}</h6>
                        <h3>{__('Ordered List', 'ultimate-post')}</h3>
                        <ol>
                            <li>{__('List Item 1', 'ultimate-post')}</li>
                            <li>{__('List Item 2', 'ultimate-post')}</li>
                            <li>{__('List Item 3', 'ultimate-post')}</li>
                        </ol>
                        <h3>{__('Unordered List', 'ultimate-post')}</h3>
                        <ul>
                            <li>{__('List Item 1', 'ultimate-post')}</li>
                            <li>{__('List Item 2', 'ultimate-post')}</li>
                            <li>{__('List Item 3', 'ultimate-post')}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
