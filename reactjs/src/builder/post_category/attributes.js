import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /*============================
      Post Category Settings
  ============================*/
  catLabelShow: { type: "boolean", default: true },
  catIconShow: { type: "boolean", default: true },
  catColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .cat-builder-content a, {{ULTP}} .cat-builder-content {color:{{catColor}} !important;}",
      },
    ],
  },
  catBgColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
    style: [{ selector: "{{ULTP}} .ultp-category-list" }],
  },
  catItemBorder: {
    type: "object",
    default: {
      openBorder: 1,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#e2e2e2",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-category-list" }],
  },
  catRadius: {
    type: "object",
    default: { top: 3, right: 3, bottom: 3, left: 3, unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-category-list { border-radius:{{catRadius}}; }",
      },
    ],
  },
  catHovColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-category .cat-builder-content > a:hover { color:{{catHovColor}} !important; }",
      },
    ],
  },
  catBgHovColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)" },
    style: [{ selector: "{{ULTP}} .ultp-category-list:hover" }],
  },
  catItemHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#323232",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-category-list:hover" }],
  },
  catHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-category-list:hover { border-radius:{{catHoverRadius}}; }",
      },
    ],
  },
  catTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [{ selector: "{{ULTP}} .ultp-category-list" }],
  },
  catSeparator: { type: "string", default: "" },
  catSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-category-list:not(:first-child) {margin-left:{{catSpace}}}",
      },
    ],
  },
  catItemPad: {
    type: "object",
    default: {
      lg: { top: "0", bottom: "0", left: "10", right: "10", unit: "px" },
    },
    style: [
      { selector: "{{ULTP}} .ultp-category-list { padding:{{catItemPad}} }" },
    ],
  },
  catAlign: {
    type: "object",
    default: {},
    style: [
      {
        selector:
          "{{ULTP}} .ultp-builder-category {justify-content:{{catAlign}}}",
      },
    ],
  },
  /*============================
      Categories Label Settings
  ============================*/
  catLabel: {
    type: "string",
    default: "Category : ",
    style: [
      { depends: [{ key: "catLabelShow", condition: "==", value: true }] },
    ],
  },
  catLabelColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .cat-builder-label {color:{{catLabelColor}};}",
      },
    ],
  },
  catLabelTypo: {
    type: "object",
    default: {
      openTypography: 0,
      size: { lg: "", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .cat-builder-label",
      },
    ],
  },
  catLabelSpace: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .cat-builder-label {margin-right:{{catLabelSpace}}}",
      },
    ],
  },
  catLabelBgColor: {
    type: "object",
    default: [],
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .cat-builder-label",
      },
    ],
  },
  catLabelBorder: {
    type: "object",
    default: { openTypography: 0, size: { lg: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .cat-builder-label",
      },
    ],
  },
  catLabelRadius: {
    type: "object",
    default: [],
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .cat-builder-label { border-radius:{{catLabelRadius}}; }",
      },
    ],
  },
  catLabelPad: {
    type: "object",
    default: {},
    style: [
      {
        depends: [{ key: "catLabelShow", condition: "==", value: true }],
        selector: "{{ULTP}} .cat-builder-label { padding:{{catLabelPad}} }",
      },
    ],
  },
  /*============================
      Categories Icon Settings
  ============================*/
  catIconStyle: {
    type: "string",
    default: "",
    style: [
      { depends: [{ key: "catIconShow", condition: "==", value: true }] },
    ],
  },
  catIconColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-category svg { fill:{{catIconColor}}; stroke:{{catIconColor}} }",
      },
    ],
  },
  catIconHovColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-category svg:hover { fill:{{catIconHovColor}}; stroke:{{catIconHovColor}} }",
      },
    ],
  },
  catIconSize: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-category svg { height:{{catIconSize}}; width:{{catIconSize}} }",
      },
    ],
  },
  catIconSpace: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "catIconShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-builder-category svg {margin-right:{{catIconSpace}} }",
      },
    ],
  },
  /*============================
      Advance Settings
  ============================*/
  ...commonAttributes(['advanceAttr'], [ 'loadingColor'])
};
export default attributes;
