const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { Fragment } = wp.element
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import IconPack from "../../helper/fields/tools/IconPack"
import { useBlockId } from '../../helper/hooks/use-blockid'

export default function Edit(props) {

    const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			catLabelShow,
			catLabel,
			catIconShow,
			catIconStyle,
			catSeparator,
            currentPostId
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

    useBlockId({
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        checkRef: false
    });

    if (blockId) {
        CssGenerator(attributes, 'ultimate-post/post-category', blockId);
    }
    
    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')} >
                    <CommonSettings
                            title={'inline'}
                            initialOpen={true}
                            include={[
                                {
                                    data: { type: 'alignment', key: 'catAlign', responsive: true, label: __('Alignment', 'ultimate-post'), options: ['flex-start', 'center', 'flex-end'] }
                                },
                                {
                                    data: {
                                        type: 'tab', content: [
                                            {
                                                name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                    { type: 'color', key: 'catColor', label: __('Color', 'ultimate-post') },
                                                    { type: 'color2', key: 'catBgColor', label: __('Background', 'ultimate-post') },
                                                    { type: 'border', key: 'catItemBorder', label: __('Border', 'ultimate-post') },
                                                    { type: 'dimension', key: 'catRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                                ]
                                            },
                                            {
                                                name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                    { type: 'color', key: 'catHovColor', label: __('Hover Color', 'ultimate-post') },
                                                    { type: 'color2', key: 'catBgHovColor', label: __('Hover Background', 'ultimate-post') },
                                                    { type: 'border', key: 'catItemHoverBorder', label: __('Hover Border', 'ultimate-post') },
                                                    { type: 'dimension', key: 'catHoverRadius', label: __('Hover Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true },
                                                ]
                                            },
                                        ]
                                    }
                                },
                                {
                                    data: { type: 'typography', key: 'catTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'text', key: 'catSeparator', label: __('Separator', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'catSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Space Between Categories', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'dimension', key: 'catItemPad', step: 1, unit: true, responsive: true, label: __('Category Padding', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            initialOpen={false}
                            title={__('Category Label', 'ultimate-post')}
                            depend="catLabelShow"
                            include={[
                                {
                                    data: { type: 'text', key: 'catLabel', label: __('Label Text', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'catLabelColor', label: __('Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'typography', key: 'catLabelTypo', label: __('Typography', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'range', key: 'catLabelSpace', min: 0, max: 100, step: 1, responsive: true, unit: true, label: __('Label Spacing', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color2', key: 'catLabelBgColor', label: __('Background', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'border', key: 'catLabelBorder', label: __('Border', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'dimension', key: 'catLabelRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
                                },
                                {
                                    data: { type: 'dimension', key: 'catLabelPad', step: 1, unit: true, responsive: true, label: __('Padding', 'ultimate-post') }
                                },
                            ]}
                            store={store} />
                        <CommonSettings
                            title={__('Category Icon', 'ultimate-post')}
                            depend="catIconShow"
                            include={[
                                {
                                    data: { type: 'icon', key: 'catIconStyle', label: __('Select Icon', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'catIconColor', label: __('Icon Color', 'ultimate-post') }
                                },
                                {
                                    data: { type: 'color', key: 'catIconHovColor', label: __('Icon Hover Color', 'ultimate-post') },
                                },
                                {
                                    data: { type: 'range', key: 'catIconSize', min: 10, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Size', 'ultimate-post') },
                                },
                                {
                                    data: { type: 'range', key: 'catIconSpace', min: 10, max: 100, responsive: true, step: 1, unit: true, label: __('Icon Space X', 'ultimate-post') },
                                }
                            ]}
                            store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced pro={true} store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                <div className={`ultp-block-wrapper`}>
                    <div className="ultp-builder-category" >
                        {catIconShow && IconPack[catIconStyle]}
                        {catLabelShow && <div className="cat-builder-label" >
                            {catLabel}
                        </div>}
                        <div className={`cat-builder-content`}>
                            <a className={`ultp-category-list`} href="#">Dummy Cat1</a>
                            { catSeparator ? ' '+catSeparator:''} <a className={`ultp-category-list`} href="#">Dummy Cat2</a>
                            { catSeparator ? ' '+catSeparator:''} <a className={`ultp-category-list`} href="#">Dummy Cat3</a>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}