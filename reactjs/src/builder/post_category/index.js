const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";

registerBlockType(
    'ultimate-post/post-category', {
        title: __('Post Category','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/builder/category.svg'} alt="Post Category" />,
        category: 'postx-site-builder',
        description: __('Display Post Categories and style them as you need.','ultimate-post'),
        keywords: [ 
            __('Category','ultimate-post'),
            __('Taxonomy','ultimate-post'),
            __('Categories','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)