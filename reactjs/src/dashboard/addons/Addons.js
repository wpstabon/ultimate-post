const { __ } = wp.i18n;
import { useEffect, useState } from 'react';
import {
	DashboardSidebar,
	getPrimaryButton,
	getProHtml,
	getSecondaryButton,
	getSettingsData,
	getUpgradeProBtn,
} from '../Settings';
import IconPack from '../../helper/fields/tools/IconPack';
import Toast from '../utility/Toast';
import Skeleton from '../utility/Skeleton';
import './addons.scss';
import FrontendSubmission from './FrontendSubmission';
import icons from '../../helper/icons';
import Modal from '../utility/Modal';
import { all_addons } from './addonsData';

const Addons = ({ integrations, generalDiscount }) => {
	const [popUp, setPopUp] = useState('');
	const [lockShow, setLockShow] = useState(false);
	const [settings, setSettings] = useState({});
	const [generalDiscountContent, setGeneralDiscountContent] = useState('');
	const [toastMessages, setToastMessages] = useState({
		state: false,
		status: '',
	});
	const [modalContent, setModalContent] = useState('');
	const [loading, setLoading] = useState(false);

	let addons = all_addons;
	const addons_settings = ultp_dashboard_pannel.addons_settings;

	const addonArr = Object.entries(addons);
	addonArr.sort((a, b) => a[1].position - b[1].position);
	addons = Object.fromEntries(addonArr);

	useEffect(() => {
		getAllSettings();
		document.addEventListener('mousedown', handleClickOutside);
		return () =>
			document.removeEventListener('mousedown', handleClickOutside);
	}, []);

	const keyfeature = [
		{
			label: __('40+ Blocks', 'ultimate-post'),
			descp: __(
				'PostX comes with over 40 Gutenberg blocks',
				'ultimate-post'
			),
		},
		{
			label: __('250+ Patterns', 'ultimate-post'),
			descp: __(
				'Get full access to all ready post sections',
				'ultimate-post'
			),
		},
		{
			label: __('17+ Starter Sites', 'ultimate-post'),
			descp: __(
				'Pre-built websites are ready to import in one click',
				'ultimate-post'
			),
		},
		{
			label: __('Global Styles', 'ultimate-post'),
			descp: __(
				'Control the full website’s colors and typography globally',
				'ultimate-post'
			),
		},
		{
			label: __('Dark/Light Mode', 'ultimate-post'),
			descp: __(
				'Let your readers switch between light and dark modes',
				'ultimate-post'
			),
		},
		{
			label: __('Advanced Query Builder', 'ultimate-post'),
			descp: __(
				'Display/reorder posts, pages, and custom post types',
				'ultimate-post'
			),
		},
		{
			label: __('Dynamic Site Builder', 'ultimate-post'),
			descp: __(
				'Dynamically create templates for essential pages',
				'ultimate-post'
			),
		},
		{
			label: __('Ajax Powered Filter', 'ultimate-post'),
			descp: __(
				'Let your visitors filter posts by categories and tags',
				'ultimate-post'
			),
		},
		{
			label: __('Advanced Post Slider', 'ultimate-post'),
			descp: __(
				'Display posts in engaging sliders and carousels',
				'ultimate-post'
			),
		},
		{
			label: __('SEO Meta Support', 'ultimate-post'),
			descp: __(
				'Replace the post excerpts with meta descriptions',
				'ultimate-post'
			),
		},
		{
			label: __('Custom Fonts', 'ultimate-post'),
			descp: __(
				'Upload custom fonts per your requirements',
				'ultimate-post'
			),
		},
		{
			label: __('Ajax Powered Pagination ', 'ultimate-post'),
			descp: __(
				'PostX comes with three types of Ajax pagination',
				'ultimate-post'
			),
		},
	];

	const getAllSettings = () => {
		wp.apiFetch({
			path: '/ultp/v2/get_all_settings',
			method: 'POST',
			data: {
				key: 'key',
				value: 'value',
			},
		}).then((res) => {
			if (res.success) {
				setSettings(res.settings);
			}
		});
	};

	const changeAddonStatus = (key) => {
		const value = settings[key] == 'true' ? 'false' : 'true';
		if (!ultp_dashboard_pannel.active && addons[key].is_pro) {
			setSettings({ ...settings, [key]: 'false' });
			setLockShow(true);
		} else {
			setSettings({ ...settings, [key]: value });
			wp.apiFetch({
				path: '/ultp/v2/addon_block_action',
				method: 'POST',
				data: {
					key: key,
					value: value,
				},
			}).then((res) => {
				if (res.success) {
					if (
						[
							'ultp_templates',
							'ultp_custom_font',
							'ultp_builder',
						].includes(key)
					) {
						document.getElementById(
							'postx-submenu-' +
								key
									.replace('templates', 'saved_templates')
									.replace('ultp_', '')
									.replace('_', '-')
						).style.display = value == 'true' ? 'block' : 'none';
						document.getElementById(
							key
								.replace('templates', 'saved_templates')
								.replace('ultp_', 'ultp-dasnav-')
								.replace('_', '-')
						).style.display = value == 'true' ? '' : 'none';
					}
					setTimeout(function () {
						setToastMessages({
							status: 'success',
							messages: [res.message],
							state: true,
						});
					}, 400);
				}
			});
		}
	};
	const handleFormSubmit = (e) => {
		setLoading(true);
		e.preventDefault();
		const formData = new FormData(e.target);
		let formObject = {};

		for (let element of e.target.elements) {
			if (element.name) {
				if (element.type === 'checkbox' && !element.checked) {
					formObject[element.name] = '';
				} else if (element.type === 'radio') {
					if (element.checked) {
						formObject[element.name] = element.value;
					}
				} else if (element.type === 'select-multiple') {
					formObject[element.name] = formData.getAll(element.name);
				} else if (
					element.dataset['customprop'] == 'custom_multiselect'
				) {
					formObject[element.name] = element.value
						? element.value.split(',')
						: [];
				} else {
					formObject[element.name] = element.value;
				}
			}
		}

		wp.apiFetch({
			path: '/ultp/v2/save_plugin_settings',
			method: 'POST',
			data: {
				settings: formObject,
				action: 'action',
				type: 'type',
			},
		}).then((res) => {
			setLoading(false);
			if (res.success) {
				setToastMessages({
					status: 'success',
					messages: [res.message],
					state: true,
				});
			}
		});
	};
	const handleClickOutside = (e) => {
		if (!e.target.closest('.ultp-addon-settings-popup')) {
			setPopUp('');
		}
	};

	const getAddons = (integration = false) => {
		let count = 0;
		return (
			<div className={`ultp-addons-grid ${integration ? '' : 'ultp-gs'}`}>
				{Object.keys(addons).map((key, index) => {
					const addon = addons[key];
					if (
						(integration && !addon.integration) ||
						(!integration && addon.integration)
					) {
						return;
					}
					count++;
					let isChecked = true;
					isChecked =
						settings[key] == 'true' || settings[key] == true
							? addon.is_pro && !ultp_dashboard_pannel.active
								? false
								: true
							: false;

					return (
						<div
							className="ultp-addon-item ultp-dash-item-con"
							key={key}
						>
							{addon?.new && (
								<span className="ultp-new-tag">New</span>
							)}
							<div
								className="ultp-addon-item-contents"
								style={{
									paddingBottom: addon.notice
										? '10px'
										: 'auto',
								}}
							>
								<div className="ultp-addon-item-name">
									<img src={`${ultp_dashboard_pannel.url}assets/img/addons/${addon.img}`} alt={addon.name} />
									<div className="ultp_h6">{addon.name}</div>
								</div>
								<div className="ultp-description">
									{addon.desc}

									{addon.notice && (
										<div className="ultp-description-notice">
											{addon.notice}
										</div>
									)}
								</div>
								{addon.required && addon.required?.name && (
									<span className="ultp-plugin-required">
										{' '}
										{__(
											'This addon required this plugin: ',
											'ultimate-post'
										)}
										{addon.required.name}
									</span>
								)}
								{addon.is_pro &&
									!ultp_dashboard_pannel.active && (
										<span
											onClick={() => {
												setLockShow(true);
											}}
											className="ultp-pro-lock"
										>
											PRO
										</span>
									)}
							</div>
							<div className="ultp-addon-item-actions ultp-dash-control-options">
								<input
									type="checkbox"
									datatype={key}
									className={
										'ultp-addons-enable ' +
										(addon.is_pro &&
										!ultp_dashboard_pannel.active
											? 'disabled'
											: '')
									}
									id={key}
									checked={isChecked}
									onChange={() => {
										changeAddonStatus(key);
									}}
								/>
								<label
									htmlFor={key}
									className="ultp-control__label"
								>
									{addon.is_pro &&
										!ultp_dashboard_pannel.active && (
											<span className="dashicons dashicons-lock"></span>
										)}
								</label>
								<div className="ultp-docs-action">
									{addon.live && (
										<a
											href={addon.live.replace(
												'live_demo_args',
												`?utm_source=${integration ? 'db-postx-integration' : 'db-postx-addons'}&utm_medium=${integration ? '' : key + '-'}demo&utm_campaign=postx-dashboard`
											)}
											className="ultp-option-tooltip"
											target="_blank"
										>
											<div className="dashicons dashicons-external"></div>
											{__('Demo', 'ultimate-post')}
										</a>
									)}
									{addon.docs && (
										<a
											href={
												addon.docs +
												(integration
													? '?utm_source=db-postx-integration'
													: '?utm_source=db-postx-addons') +
												'&utm_medium=docs&utm_campaign=postx-dashboard'
											}
											className="ultp-option-tooltip"
											target="_blank"
										>
											<div className="dashicons dashicons-media-document"></div>
											{__('Docs', 'ultimate-post')}
										</a>
									)}
									{addon.video && (
										<a
											href={addon.video}
											className="ultp-option-tooltip"
											target="_blank"
										>
											<div className="dashicons dashicons-admin-collapse"></div>
											{__('Video', 'ultimate-post')}
										</a>
									)}
									{addons_settings[key] && (
										<span
											className="ultp-popup-setting dashicons dashicons-admin-generic"
											onClick={() => {
												setPopUp(key);
											}}
										></span>
									)}
									{popUp == key && (
										<div className="ultp-addon-settings">
											<div className="ultp-addon-settings-popup">
												{addons_settings[key] && (
													<>
														<div className="ultp-addon-settings-title">
															<div className="ultp_h6">
																{addon.name}
																:&nbsp;
																{__(
																	'Settings',
																	'ultimate-post'
																)}
															</div>
														</div>
														{
															<form
																onSubmit={
																	handleFormSubmit
																}
																action=""
															>
																<div className="ultp-addon-settings-body">
																	{key ===
																		'ultp_frontend_submission' && (
																		<FrontendSubmission
																			attr={
																				addons_settings[
																					key
																				][
																					'attr'
																				]
																			}
																			settings={
																				settings
																			}
																			setSettings={
																				setSettings
																			}
																		/>
																	)}
																	{key !=
																		'ultp_frontend_submission' &&
																		getSettingsData(
																			addons_settings[
																				key
																			][
																				'attr'
																			],
																			settings
																		)}
																	<div className="ultp-data-message"></div>
																</div>
																<div className="ultp-addon-settings-footer">
																	<button
																		type="submit"
																		className={`cursor ultp-primary-button ${loading ? 'onloading' : ''}`}
																	>
																		{__(
																			'Save Settings',
																			'ultimate-post'
																		)}
																		{loading &&
																			IconPack[
																				'refresh'
																			]}
																	</button>
																</div>
															</form>
														}
														<button
															onClick={() => {
																setPopUp('');
															}}
															className="ultp-popup-close"
														></button>
													</>
												)}
											</div>
										</div>
									)}
								</div>
							</div>
						</div>
					);
				})}
			</div>
		);
	};

	const bannerPopup = () => {
		return (
			<iframe
				width="1100"
				height="500"
				src="https://www.youtube.com/embed/FYgSe7kgb6M?autoplay=1"
				title={__(
					'How to add Product Filter to WooCommerce Shop Page',
					'ultimate-post'
				)}
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
				allowFullScreen
			/>
		);
	};
	const startProList = [
		__('Access to Pro Starter Site Templates', 'ultimate-post'),
		__('Access to All Pro Features', 'ultimate-post'),
		__('Fully Unlocked Site Builder', 'ultimate-post'),
		__('And more... ', 'ultimate-post'),
	];

	const getAddonsWelcome = () => {
		return (
			<div className="ultp-gettingstart-message">
				<div className="ultp-start-left">
					<img
						src={
							ultp_dashboard_pannel.url +
							'assets/img/dashboard/dashboard_banner.jpg'
						}
						alt="Banner"
					/>
					<div className="ultp-start-content">
						<span className="ultp-start-text">
							{__(
								'Enjoy Pro-level Ready Templates!',
								'ultimate-post'
							)}
						</span>
						<div className="ultp-start-btns">
							{getPrimaryButton(
								'https://www.wpxpo.com/postx/starter-sites/?utm_source=db-postx-started&utm_medium=starter-sites&utm_campaign=postx-dashboard&pux_link=dbstartersite',
								'',
								__('Explore Starter Sites', 'ultimate-post'),
								''
							)}
							{getSecondaryButton(
								'https://www.wpxpo.com/postx/?utm_source=db-postx-started&utm_medium=details&utm_campaign=postx-dashboard',
								'',
								__('Plugin Details', 'ultimate-post'),
								''
							)}
						</div>
					</div>
				</div>
				<div className="ultp-start-right">
					<div
						className="ultp-dashborad-banner"
						style={{ cursor: 'pointer' }}
						onClick={() => setModalContent(bannerPopup())}
					>
						<img
							src={
								ultp_dashboard_pannel.url +
								'assets/img/dashboard/dashboard_right_banner.jpg'
							}
							className="ultp-banner-img"
						/>
						<div className="ultp-play-icon-container">
							{/*{icons.play}*/}
							<img
								className="ultp-play-icon"
								src={
									ultp_dashboard_pannel.url +
									'/assets/img/dashboard/play.png'
								}
								alt={__('Play', 'ultimate-post')}
							/>
							<span className="ultp-animate"></span>
						</div>
					</div>
					<div className="ultp-dashboard-content">
						{!ultp_dashboard_pannel.active ? (
							<>
								<div className="ultp-title">
									{__('Do More with', 'ultimate-post')}{' '}
									<span
										style={{
											color: 'var(--postx-primary-color)',
										}}
									>
										{__('PRO:', 'ultimate-post')}
									</span>
								</div>
								<div className="ultp-description">
									{__(
										'Unlock powerful customizations with PostX Pro:',
										'ultimate-post'
									)}
								</div>
								<div className="ultp-lists">
									{startProList.map((content, index) => {
										return (
											<span
												className="ultp-list"
												key={index}
											>
												{icons.rightMark} {content}
											</span>
										);
									})}
								</div>
								<a
									href={
										'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-started&utm_medium=upgrade-pro-hero&utm_campaign=postx-dashboard'
									}
									className="ultp-upgrade-btn"
									target='_blank'
								>
									{__('Upgrade to Pro', 'ultimate-post')}
									{icons.rocketPro}
								</a>
							</>
						) : (
							<>
								<div className="ultp-title _pro">
									{__('What Do You Need?', 'ultimate-post')}
								</div>
								<div className="ultp-description _pro">
									{__(
										'Do you have something in mind you want to share? Both we and our users would like to hear about it. Share your ideas on our Facebook group and let us know what you need.',
										'ultimate-post'
									)}
								</div>
								{getPrimaryButton(
									'https://www.facebook.com/groups/gutenbergpostx',
									'',
									'Share Ideas',
									''
								)}
							</>
						)}
					</div>
				</div>
			</div>
		);
	};

	return (
		<>
			<div
				className={`ultp-dashboard-addons-container ${Object.keys(settings).length > 0 ? '' : ' skeletonOverflow'}`}
			>
				{!integrations && getAddonsWelcome()}
				{lockShow &&
					getProHtml({
						tags: 'addons_popup',
						func: (val) => {
							setLockShow(val);
						},
						data: {
							icon: 'addon_lock.svg',
							title: __(
								'Unlock All Addons of PostX',
								'ultimate-post'
							),
							description: __(
								'Sorry, this addon is not available in the free version of PostX. Please upgrade to a pro plan to unlock all pro addons and features of PostX.',
								'ultimate-post'
							),
						},
					})}
				<div
					className={`ultp-addons-container-grid ${integrations ? '' : 'ultp-gs'} `}
				>
					<div>
						<div className="ultp_h2 ultp-addon-parent-heading">
							{integrations ? (
								<>
									{__('Integration', 'ultimate-post')} &nbsp;
									<span className="ultp-short-span">
										{__('Addons', 'ultimate-post')}
									</span>
								</>
							) : (
								'Build Smartly'
							)}
						</div>
						{Object.keys(settings).length > 0 ? (
							getAddons(integrations ? true : false)
						) : (
							<div
								className={`ultp-addons-grid ${integrations ? '' : 'ultp-gs'}`}
							>
								{Array(6)
									.fill(1)
									.map((val, k) => {
										return (
											<div
												key={k}
												className="ultp-addon-item ultp-dash-item-con"
											>
												<div className="ultp-addon-item-contents">
													<div className="ultp-addon-item-name">
														<Skeleton
															type="custom_size"
															c_s={{
																size1: 50,
																unit1: 'px',
																size2: 50,
																unit2: 'px',
																br: 18,
															}}
														/>
														<Skeleton
															type="custom_size"
															c_s={{
																size1: 190,
																unit1: 'px',
																size2: 28,
																unit2: 'px',
																br: 4,
															}}
														/>
													</div>
													<div className="ultp-description">
														<Skeleton
															type="custom_size"
															classes="loop"
															c_s={{
																size1: 100,
																unit1: '%',
																size2: 14,
																unit2: 'px',
																br: 2,
															}}
														/>
														<Skeleton
															type="custom_size"
															classes="loop"
															c_s={{
																size1: 70,
																unit1: '%',
																size2: 14,
																unit2: 'px',
																br: 2,
															}}
														/>
													</div>
												</div>
												<div className="ultp-addon-item-actions ultp-dash-control-options">
													<Skeleton
														type="custom_size"
														c_s={{
															size1: 40,
															unit1: 'px',
															size2: 20,
															unit2: 'px',
															br: 8,
														}}
													/>
													<div className="ultp-docs-action">
														<Skeleton
															type="custom_size"
															c_s={{
																size1: 60,
																unit1: 'px',
																size2: 22,
																unit2: 'px',
																br: 2,
															}}
														/>
														<Skeleton
															type="custom_size"
															c_s={{
																size1: 50,
																unit1: 'px',
																size2: 22,
																unit2: 'px',
																br: 2,
															}}
														/>
														<Skeleton
															type="custom_size"
															c_s={{
																size1: 60,
																unit1: 'px',
																size2: 22,
																unit2: 'px',
																br: 2,
															}}
														/>
													</div>
												</div>
											</div>
										);
									})}
							</div>
						)}
						{!integrations && (
							<div className="ultp_dash_key_features">
								<div className="ultp_h2">
									{__(
										'Key Features of PostX',
										'ultimate-post'
									)}
								</div>
								<div className="ultp_dash_key_features_content">
									{keyfeature?.map((val, k) => {
										return (
											<div
												key={k}
												className="ultp-dash-item-con"
											>
												<div className="ultp_dash_key_features_label">
													{val.label}
												</div>
												<div className="ultp-description">
													{val.descp}
												</div>
											</div>
										);
									})}
								</div>
							</div>
						)}
						{generalDiscount != 'hide' &&
							!ultp_dashboard_pannel.active && (
								<div
									className={`addons_promotions ${integrations ? '' : 'ultp-gs'}`}
								>
									<div className="addons_promotions_label">
										{__(
											'Unlock all exclusive features with ',
											'ultimate-post'
										)}
										<strong>
											{__(
												' PostX Pro. ',
												'ultimate-post'
											)}
										</strong>
										{/* {__(' and reduce ','ultimate-post')}
                                    <strong>{__('80% development time','ultimate-post')}</strong> */}
										{__(
											'Get Special special discount while upgrading',
											'ultimate-post'
										)}
									</div>
									<div className="addons_promotions_btns">
										<a
											href="https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-started&utm_medium=upgrade-pro-bottom&utm_campaign=postx-dashboard"
											className="addons_promotions_btn btn1"
											target='_blank'
										>
											{__(
												'Upgrade to PRO Now',
												'ultimate-post'
											)}
										</a>
										<a
											href="https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-started&utm_medium=upgrade-pro-bottom&utm_campaign=postx-dashboard"
											className="addons_promotions_btn btn2"
											target='_blank'
										>
											{__(
												'Give Me Premium Access',
												'ultimate-post'
											)}
										</a>
										<a
											href="#"
											className="addons_promotions_btn addons_promotions_close btn3"
										>
											{__(
												'I don’t Want to Save Money',
												'ultimate-post'
											)}
										</a>
									</div>
								</div>
							)}
					</div>
					{integrations && (
						<DashboardSidebar
							FRBtnTag="settingsFR"
							proBtnTags={'postx_dashboard_settings'}
						/>
					)}
				</div>
			</div>
			{toastMessages.state && (
				<Toast
					delay={2000}
					toastMessages={toastMessages}
					setToastMessages={setToastMessages}
				/>
			)}
			{modalContent && (
				<Modal
					title={__('Postx Intro', 'ultimate-post')}
					modalContent={modalContent}
					setModalContent={setModalContent}
				/>
			)}
		</>
	);
};

export default Addons;
