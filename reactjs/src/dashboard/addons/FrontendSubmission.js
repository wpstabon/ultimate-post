import React from 'react';
import { useState,useEffect} from 'react';
import Tooltip from '../../helper/common/Tooltip';
const { __ } = wp.i18n;

import IconPack from "../../helper/fields/tools/IconPack";


const FrontendSubmission = ({attr, settings,setSettings}) => {

    let data = attr;

    const Multiselect = ({ multikey, value, multiValue}) => {
        const [multiFieldValues, setMultiFieldValues] = useState([...multiValue]);
        const [open, setOpen] = useState(false);
        const [options, setOptions] = useState(value['options']??{});
        const [searchValue, setSearchValue] = useState('');
    
        const addItem = (val) => {
            let data = [];
            if( multiFieldValues.indexOf(val) == -1 && val!='all' ) {
                data = [...multiFieldValues, val];
            }
            if ( val==='all' ) {
                const allItems = Object.fromEntries( Object.entries(value['options']).filter(([key, value]) => !value.toLowerCase().includes('all')) );      
                data = Object.keys(allItems);
            }
            setMultiFieldValues(data);
            setSettings({ ...settings, [multikey]: data });
        }
        const removeItem = (val) => {
            const data = multiFieldValues.filter(item => item != val); 
            setMultiFieldValues(data);
            setSettings({ ...settings, [multikey]: data });
        }
        const handleClickOutside = (e) => {
            if (!e.target.closest('.ultp-ms-container') ) {
                setOpen(false);
            }
        }
        useEffect(() => {
            document.addEventListener('mousedown', handleClickOutside);
            return () =>  document.removeEventListener('mousedown', handleClickOutside);
        },[]);
    
        useEffect(()=>{
            setTimeout(()=>{
                const searchResult = Object.fromEntries( Object.entries(value['options']).filter(([key, value]) => value.toLowerCase().includes(searchValue.toLowerCase())||key.toLowerCase().includes(searchValue.toLowerCase())) );       
                setOptions(searchResult); 
            },500);
        },[searchValue]);
    
        return (
            <div className="ultp-settings-field">
                <input type="hidden" name={multikey} value={multiFieldValues} data-customprop="custom_multiselect"/>
                <div className="ultp-ms-container">
                    <div onClick={()=>setOpen(!open)} className="ultp-ms-results-con cursor">
                        <div className="ultp-ms-results">
                            {
                                multiFieldValues.length > 0 ? multiFieldValues?.map((key, index) => {
                                    return <span key={index} className="ultp-ms-selected">
                                        {value['options'][key]}
                                        <span className="ultp-ms-remove cursor" onClick={(e)=> {e.stopPropagation();removeItem(key)}}>{IconPack['close_circle_line']}</span>
                                    </span>
                                }) 
                                :
                                <span>{__('Select options')}</span>
                            }
                        </div>
                        <span onClick={()=>setOpen(!open)} className="ultp-ms-results-collapse cursor">{IconPack['collapse_bottom_line']}</span>
                    </div>
                    { 
                        open && options && <div className="ultp-ms-options">
                             <input type='text' className="ultp-multiselect-search" value={searchValue} onChange={(e)=>setSearchValue(e.target.value)} />
                            {
                                Object.keys(options)?.map((key, index) => {
                                    return <span className="ultp-ms-option cursor" onClick={()=> addItem(key)} key={index} value={key} >{options[key]}</span>
                                })
                            }
                        </div> 
                    }
                </div>
            </div>
        )
    }


    
    const checkDepends = (key,value)=>{
        let status = true;;

        if(value.hasOwnProperty('depends_on')){
            value['depends_on'].forEach(element => {
                if(element.condition=='==') {
                    if(settings[element['key']]!=element.value) {
                        status = false;
                    }

                }
            });
        }
        return status;
    }
    
    const getField = (key, value, settingsData) => {
        const val = settings.hasOwnProperty(key) ? settings[key] : ( settings['default'] ? settings['default'] : ( value['type'] == 'multiselect' ? [] : '' ) );
        const onChangeHandler = (e)=>{
            setSettings((preValue)=>{
                // if(e.target.type!='radio') {
                    if(e.target.type==='checkbox') {
                        return {...preValue,[e.target.name]:e.target.checked?'yes':'no'};
                    } else {
                        return {...preValue,[e.target.name]:e.target.value};
                    }
                // }

            });
        }

        
        switch(value['type']) {
            case 'select' :
                return (
                    <div className="ultp-settings-field">
                        <select defaultValue={val} name={key} id={key} onChange={onChangeHandler}>
                            { Object.keys(value['options']).map((key, index) => {
                                return <option key={index} value={key} >{value['options'][key]}</option>
                            })}
                        </select>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'radio' :
                return (
                    <div className="ultp-settings-field">
                        <div className="ultp-field-radio">
                                <div className="ultp-field-radio-items">
                                { Object.keys(value['options']).map((radioval, index) => {
                                    return <div key={index} className='ultp-field-radio-item' >
                                        <input type="radio" id={radioval} name={key} value={radioval} defaultChecked={radioval==val} onChange={onChangeHandler}  />
                                        <label htmlFor={radioval}>{value['options'][radioval]}</label>
                                    </div>
                                })}
                                </div>
                            
                        </div>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'color':
                return (
                    <div className="ultp-settings-field">
                        <input type="text" defaultValue={val} className="ultp-color-picker" />
                        <span className="ultp-settings-input-field">
                            <input type="text" name={key} className="ultp-color-code" defaultValue={val}/>
                        </span>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'number':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <input type="number" name={key} defaultValue={val} onChange={onChangeHandler}/>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'switch': 
                return (
                    <div className="ultp-settings-field ultp-settings-field-inline">
                        <input value="yes" type="checkbox" name={key} defaultChecked={(val=='yes' || val=='on')} onChange={onChangeHandler}/>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'switchButton':
                return (
                    <div className="ultp-settings-field ultp-settings-field-inline">
                        <input type="checkbox" value="yes" name={key} defaultChecked={(val=='yes' || val=='on')} />
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                        <div>
                            <span id="postx-regenerate-css" className={`ultp-upgrade-pro-btn cursor ${val=='yes' ? 'active':''} `}>
                                <span className="dashicons dashicons-admin-generic"></span>
                                <span className="ultp-text">
                                    {__('Re-Generate Font Files', 'ultimate-post')}
                                </span>
                            </span>
                        </div>
                    </div>
                )
            case 'multiselect':
                const multiValue = Array.isArray(val) ? val : [val];
                return   <Multiselect multikey={key} value={value} multiValue={multiValue} />
            case 'text':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <input type="text" name={key} defaultValue={val} onChange={onChangeHandler}/>
                        <span className="ultp-description">
                            {value['desc']}
                            { value['link'] && 
                                <a className="settingsLink" target="_blank" href={value['link']}>{value['linkText']}</a> 
                            }
                        </span>
                    </div>
                );
            case 'textarea':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <textarea name={key} defaultValue={val}></textarea>
                        <span className="ultp-description">
                            {value['desc']}
                            { value['link'] && 
                                <a className="settingsLink" target="_blank" href={value['link']}>{value['linkText']}</a> 
                            }
                        </span>
                    </div>
                );
            case 'shortcode':
                return (
                    <code className="ultp-shortcode-copy">[{value['value']}]</code>
                );
            default:
                break;
        }
        return 
    }


    return (
        <>
            { Object.keys(data).map((key, index) => {
                let value = data[key];
                return (
                    <span key={index}> 
                        { value['type'] == 'hidden' && 
                            <input key={key} type="hidden" name={key} defaultValue={value['value']}/>
                        }
                        { value['type'] != 'hidden' &&
                            <>
                                { value['type'] == 'heading' &&
                                    <h2 className="ultp-settings-heading">{value['label']}</h2> &&
                                    <>
                                        { value['desc'] &&
                                            <div className="ultp-settings-subheading">{value['desc']}</div>
                                        }
                                    </>
                                }
                                { value['type'] != 'heading'  && checkDepends(key,value) && 
                                    <div className="ultp-settings-wrap">
                                        { value['label'] && 
                                            <strong>
                                                {value['label']}
                                                {value['tooltip'] && <Tooltip content={value['tooltip']}>
                                                    <span className=" cursor dashicons dashicons-editor-help"></span>
                                                    </Tooltip>
                                                }
                                            </strong>
                                        }
                                        <div className="ultp-settings-field-wrap">
                                            { getField(key, value) }
                                        </div>
                                    </div>
                                }
                            </>
                        }
                    </span>
                )
            })}
        </>
    )
    
}

export default FrontendSubmission;