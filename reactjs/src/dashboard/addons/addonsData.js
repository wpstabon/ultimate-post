const { __ } = wp.i18n
export const all_addons = {
    ultp_wpbakery: {
        name: "WPBakery",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the WPBakery Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "wpbakery.svg",
        is_pro: false,
        docs: "https://wpxpo.com/docs/postx/add-on/wpbakery-page-builder-addon/",
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        video: "https://www.youtube.com/watch?v=f99NZ6N9uDQ",
        position: 20,
        integration: true
    },
    ultp_templates: {
        name: "Saved Templates",
        desc: __( 'Create unlimited templates by converting Gutenberg blocks into shortcodes to use them anywhere.', 'ultimate-post' ),
        img: "saved-template.svg",
        is_pro: false,
        docs: "https://wpxpo.com/docs/postx/add-on/shortcodes-support/",
        live: "https://www.wpxpo.com/postx/addons/save-template/live_demo_args",
        video: "https://www.youtube.com/watch?v=6ydwiIp2Jkg",
        position: 10
    },
    ultp_table_of_content: {
        name: "Table of Contents",
        desc: __( 'It enables a highly customizable block to the Gutenberg blocks library to display the Table of Contents.', 'ultimate-post' ),
        img: "table-of-content.svg",
        is_pro: false,
        docs: "https://wpxpo.com/docs/postx/add-on/table-of-content/",
        live: "https://www.wpxpo.com/postx/addons/table-of-content/live_demo_args",
        video: "https://www.youtube.com/watch?v=xKu_E720MkE",
        position: 25
    },
    ultp_oxygen: {
        name: "Oxygen",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the Oxygen Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "oxygen.svg",
        is_pro: false,
        docs: "https://wpxpo.com/docs/postx/add-on/oxygen-builder-addon/",
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        video: "https://www.youtube.com/watch?v=iGik4w3ZEuE",
        position: 20,
        integration: true
    },
    ultp_elementor: {
        name: "Elementor",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the Elementor Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "elementor-icon.svg",
        is_pro: false,
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/elementor-addon/",
        video: "https://www.youtube.com/watch?v=GJEa2_Tow58",
        position: 20,
        integration: true
    },
    ultp_dynamic_content: {
        name: "Dynamic Content",
        desc: __( 'Insert dynamic, real-time content like excerpts, dates, author names, etc. in PostX blocks. ', 'ultimate-post' ),
        img: "dynamic-content.svg",
        is_pro: false,
        docs: "https://wpxpo.com/docs/postx/postx-features/dynamic-content/",
        live: "https://www.wpxpo.com/create-custom-fields-in-wordpress/live_demo_args",
        video: "https://www.youtube.com/watch?v=4oeXkHCRVCA",
        position: 6,
        notice: "ACF, Meta Box and Pods (PRO)",
        new: true
    },
    ultp_divi: {
        name: "Divi",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the Divi Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "divi.svg",
        is_pro: false,
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/divi-addon/?utm_source=postx-menu&utm_medium=addons-demo&utm_campaign=postx-dashboard",
        video: "https://www.youtube.com/watch?v=p9RKTYzqU48",
        position: 20,
        integration: true
    },
    ultp_custom_font: {
        name: "Custom Font",
        desc: __( 'It allows you to upload custom fonts and use them on any PostX blocks with all typographical options.', 'ultimate-post' ),
        img: "custom_font.svg",
        is_pro: false,
        live: "https://www.wpxpo.com/wordpress-custom-fonts/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/custom-fonts/",
        video: "https://www.youtube.com/watch?v=tLqUpj_gL-U",
        position: 7
    },
    ultp_bricks_builder: {
        name: "Bricks Builder",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the Bricks Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "bricks.svg",
        is_pro: false,
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/bricks-builder-addon/",
        video: "https://www.youtube.com/watch?v=t0ae3TL48u0",
        position: 20,
        integration: true
    },
    ultp_beaver_builder: {
        name: "Beaver",
        desc: __( 'It lets you use PostX’s Gutenberg blocks in the Beaver Builder by using the Saved Template Addon.', 'ultimate-post' ),
        img: "beaver.svg",
        is_pro: false,
        live: "https://www.wpxpo.com/postx/page-builder-integration/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/beaver-builder-addon/",
        video: "https://www.youtube.com/watch?v=aLfI0RkJO6g",
        position: 20,
        integration: true
    },
    ultp_frontend_submission: {
        name: "Front End Post Submission",
        desc: __( 'Registered/guest writers can submit posts from frontend. Admins can easily manage, review, and publish posts.', 'ultimate-post' ),
        img: "frontend_submission.svg",
        docs: "https://wpxpo.com/docs/postx/add-on/front-end-post-submission/",
        live: "https://www.wpxpo.com/postx/front-end-post-submission/live_demo_args",
        video: "https://www.youtube.com/watch?v=KofF7BUwNC0",
        is_pro: true,
        position: 6,
        integration: false,
    },
    ultp_category: {
        name: "Taxonomy Image & Color",
        desc: __( 'It allows you to add category or taxonomy-specific featured images and colors to make them attractive.', 'ultimate-post' ),
        is_pro: true,
        docs: "https://wpxpo.com/docs/postx/add-on/category-addon/",
        live: "https://www.wpxpo.com/postx/taxonomy-image-and-color/live_demo_args",
        video: "https://www.youtube.com/watch?v=cd75q-lJIwg",
        img: "category-style.svg",
        position: 15
    },
    ultp_progressbar: {
        name: "Progress Bar",
        desc: __( 'Display a visual indicator of the reading progression of blog posts and the scrolling progression of pages.', 'ultimate-post' ),
        img: "progressbar.svg",
        docs: "https://wpxpo.com/docs/postx/add-on/progress-bar/",
        live: "https://www.wpxpo.com/postx/progress-bar/live_demo_args",
        video: "https://www.youtube.com/watch?v=QErQoDhWi4c",
        is_pro: true,
        position: 30
    },
    ultp_yoast: {
        name: "Yoast",
        desc: __( 'It allows you to display custom meta descriptions added with the Yoast SEO plugin instead of excerpts.', 'ultimate-post' ),
        img: "yoast.svg",
        is_pro: true,
        live: "https://www.wpxpo.com/postx/seo-meta-support/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/seo-meta/",
        video: "https://www.youtube.com/watch?v=H8x-hHC0JBM",
        required: {
            name: "Yoast",
            slug: "wordpress-seo/wp-seo.php"
        },
        position: 55,
        integration: true
    },
    ultp_aioseo: {
        name: "All in One SEO",
        desc: __( 'It allows you to display custom meta descriptions added with the All in One SEO plugin instead of excerpts.', 'ultimate-post' ),
        img: "aioseo.svg",
        is_pro: true,
        live: "https://www.wpxpo.com/postx/seo-meta-support/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/seo-meta/",
        video: "https://www.youtube.com/watch?v=H8x-hHC0JBM",
        required: {
            name: "All in One SEO",
            slug: "all-in-one-seo-pack/all_in_one_seo_pack.php"
        },
        position: 35,
        integration: true
    },
    ultp_rankmath: {
        name: "Rank Math",
        desc: __( 'It allows you to display custom meta descriptions added with the Rank Math plugin instead of excerpts.', 'ultimate-post' ),
        img: "rankmath.svg",
        is_pro: true,
        live: "https://www.wpxpo.com/postx/seo-meta-support/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/seo-meta/",
        video: "https://www.youtube.com/watch?v=H8x-hHC0JBM",
        required: {
            name: "Rank Math",
            slug: "seo-by-rank-math/rank-math.php"
        },
        position: 40,
        integration: true
    },
    ultp_seopress: {
        name: "SEOPress",
        desc: __( 'It allows you to display custom meta descriptions added with the SEOPress plugin instead of excerpts.', 'ultimate-post' ),
        img: "seopress.svg",
        is_pro: true,
        live: "https://www.wpxpo.com/postx/seo-meta-support/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/seo-meta/",
        video: "https://www.youtube.com/watch?v=H8x-hHC0JBM",
        required: {
            name: "SEOPress",
            slug: "wp-seopress/seopress.php"
        },
        position: 45,
        integration: true
    },
    ultp_squirrly: {
        name: "Squirrly",
        desc: __( 'It allows you to display custom meta descriptions added with the Squirrly plugin instead of excerpts.', 'ultimate-post' ),
        img: "squirrly.svg",
        is_pro: true,
        live: "https://www.wpxpo.com/postx/seo-meta-support/live_demo_args",
        docs: "https://wpxpo.com/docs/postx/add-on/seo-meta/",
        video: "https://www.youtube.com/watch?v=H8x-hHC0JBM",
        required: {
            name: "Squirrly",
            slug: "squirrly-seo/squirrly.php"
        },
        position: 50,
        integration: true
    },
    ultp_builder: {
        name: "Dynamic Site Builder",
        desc: __( 'The Gutenberg-based Builder allows users to create dynamic templates for Home and all Archive pages.', 'ultimate-post' ),
        img: "builder-icon.svg",
        docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
        live: "https://www.wpxpo.com/postx/gutenberg-site-builder/live_demo_args",
        video: "https://www.youtube.com/watch?v=0qQmnUqWcIg",
        is_pro: false,
        position: 5
    },
    ultp_chatgpt: {
        name: "ChatGPT",
        desc: __( 'PostX brings the ChatGPT into the WordPress Dashboard to let you generate content effortlessly. ', 'ultimate-post' ),
        img: "ChatGPT.svg",
        docs: "https://wpxpo.com/docs/postx/add-on/chatgpt-addon/",
        live: "https://www.wpxpo.com/postx-chatgpt-wordpress-ai-content-generator/live_demo_args",
        video: "https://www.youtube.com/watch?v=NE4BPw4OTAA",
        is_pro: false,
        position: 6
    }
}