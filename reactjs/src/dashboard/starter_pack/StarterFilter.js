import TemplateSearchBox from "../../helper/TemplateSearchBox";
import IconPack from "../../helper/fields/tools/IconPack";
import icons from "../../helper/icons";
import Select from "./Select";
const { __ } = wp.i18n

const StarterFilter = (props) => {
    const { changeStates, column, showWishList, _fetchFile, fetching, searchQuery, fields, fieldValue, fieldOptions, useState, useEffect, useRef } = props;
    
    return (
        <div className="ultp-templatekit-layout-search-container">
            <div className="ultp-templatekit-search-container">
                {/* {
                    fields?.filter && <> <span>{__('Filter:', 'ultimate-post')}</span>
                        <select value={fieldValue?.filter} onChange = { v => {
                            changeStates('filter', v.target.value)
                        }}>
                            <option value='all'>All</option>
    
                            {
                                fieldOptions?.filterArr.map((item, i) => (
                                    <option key={item.value+i} value={item.value}>{item.label}</option>
                                ))
                            }
                        </select>
                    </>
                } */}
                {
                    fields?.filter && <> <span>{__('Filter:', 'ultimate-post')}</span>
                        <Select
                            useState={useState}
                            useEffect={useEffect}
                            useRef={useRef}
                            value={fieldValue?.filter}
                            contentWH = {{ height: '190px', width: '150px' } }
                            onChange={(v) => {
                                changeStates('filter', v);
                            }}
                            options={
                                fieldOptions?.filterArr || []
                            }
                        />
                    </>
                }
                {
                    fields?.trend && fieldValue?.trend && 
                    <Select
                        useState={useState}
                        useEffect={useEffect}
                        useRef={useRef}
                        value={fieldValue?.trend}
                        onChange={(v) => {
                            changeStates('trend', v);
                        }}
                        options={
                            [
                                { value: 'all',         label: __('Popular / Latest', 'ultimate-post') },
                                { value: 'popular',     label: __('Popular', 'ultimate-post') },
                                { value: 'latest',      label: __('Latest', 'ultimate-post') }
                            ]
                        }
                    />
                }
                {
                    fields?.freePro && 
                    <Select
                        useState={useState}
                        useEffect={useEffect}
                        useRef={useRef}
                        value={fieldValue?.freePro}
                        onChange={(v) => {
                            changeStates('freePro', v);
                        }}
                        options={
                            [
                                { value: 'all',      label: __('Free / Pro', 'ultimate-post') },
                                { value: 'free',     label: __('Free', 'ultimate-post') },
                                { value: 'pro',      label: __('Pro', 'ultimate-post') }
                            ]
                        }
                    />
                }
                
                {/* {
                    fields?.trend && fieldValue?.trend && 
                    <select value={fieldValue?.trend} onChange = { v => {
                        changeStates('trend', v.target.value);
                    }}>
                        <option value='all'>{__('Popular & Latest', 'ultimate-post')}</option>
                        <option value='popular'>{__('Popular', 'ultimate-post')}</option>
                        <option value='latest'>{__('Latest', 'ultimate-post')}</option>
                    </select>
                } */}
                {/* {
                    fields?.freePro && <select value={fieldValue?.freePro} onChange = { v => {
                        changeStates('freePro', v.target.value);
                    }}>
                        <option value='all'>{__('Free & Pro', 'ultimate-post')}</option>
                        <option value='free'>{__('Free', 'ultimate-post')}</option>
                        <option value='pro'>{__('Pro', 'ultimate-post')}</option>
                    </select>
                } */}
            </div>
            <div className="ultp-templatekit-layout-container">
                <TemplateSearchBox changeStates={changeStates} searchQuery={searchQuery} />
                <span className={ `ultp-templatekit-iconcol2 ${column == '2' ? 'ultp-lay-active' : ''}`} onClick ={ ()=> changeStates('column', '2') }>{icons.grid_col1}</span>
                <span className={ `ultp-templatekit-iconcol3 ${column == '3' ? 'ultp-lay-active' : ''}`} onClick ={ ()=> changeStates('column', '3') }>{icons.grid_col2}</span>
                <div className="ultp-premade-wishlist-con"><span className={`ultp-premade-wishlist cursor ${showWishList ? 'ultp-wishlist-active' : ''}`} onClick={()=> { changeStates('wishlist', showWishList ? false : true)}}>{IconPack[showWishList ? 'love_solid' : 'love_line']}</span></div>
                {
                    _fetchFile && <div onClick={() => _fetchFile()} className="ultp-filter-sync">
                        <span className={`dashicons dashicons-update-alt ${( fetching ? ' rotate' : '')}`}></span>{__('Synchronize','ultimate-post')}
                    </div>
                }
            </div>
        </div>
    );
};

export default StarterFilter;