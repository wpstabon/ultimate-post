import IconPack from "../../helper/fields/tools/IconPack";

const Select = (props) => {
    const {  useState, useEffect,useRef, onChange, options, value, contentWH } = props;
    const [isOpen, setIsOpen] = useState(false);
    const selectRef = useRef(null);

    const handleOptionClick = (option) => {
        setIsOpen(false);
        onChange(option.value);
    };

    const handleClickOutside = (e) => {
        if (selectRef?.current && !selectRef?.current.contains(e.target)) {
            setIsOpen(false);
        } else if ( selectRef?.current && selectRef?.current.contains(e.target) && !e.target.classList?.contains('ultp-reserve-button') ) {
            setIsOpen(selectRef?.current.classList?.contains('open') ? false : true);
        }
    }
    
    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () =>  document.removeEventListener('mousedown', handleClickOutside);
    },[]);

    const selectedOption = options?.find( item => item.value === value );
    
    return (
        <div ref={selectRef} className={`starter_filter_select ${isOpen ? 'open' : ''}`} >
            <div className="starter_filter_selected">
                { selectedOption ? selectedOption.label : 'Select an option' }
                {IconPack['collapse_bottom_line']}
            </div>
            {isOpen && (
                <ul className="starter_filter_select_options" style={{minWidth: contentWH?.width || '100px', maxHeight: contentWH?.height || '160px'}}>
                { options.map((option, k) => (
                    <li className="ultp-reserve-button starter_filter_select_option" key={k} onClick={() => handleOptionClick(option)}>
                    {option.label}
                    </li>
                ))}
                </ul>
            )}
        </div>
    );
};

export default Select;