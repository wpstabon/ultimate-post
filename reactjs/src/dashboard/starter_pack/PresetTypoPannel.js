const { __ } = wp.i18n
import { useState, useEffect } from 'react';
import { handlePostxPresets, postxPresetAttr } from "../../helper/CommonFunctions";

const PresetTypoPannel = ({ultpPresetTypos, currentPresetTypos, setCurrentPresetTypos}) => {
    // presetTypoKeys
    const netSitePresetTypo = {
        ...ultpPresetTypos,
        ['presetTypoCSS']: postxPresetAttr('typoCSS', ultpPresetTypos)
    } 
    const [currentPreset, setCurrentPreset] = useState({...currentPresetTypos, ...netSitePresetTypo});
    const [currentTypoStack, setCurrentTypoStack] = useState('');

    const presetTypoKeys = postxPresetAttr('presetTypoKeys');
    const typoStacks= postxPresetAttr('typoStacks');

    useEffect(() => {
        handlePostxPresets('get','ultpPresetTypos', '', (res) => {
            if(res.data) {
                const newObj = {...res.data, ...currentPreset};
                setCurrentPreset({...newObj, ['presetTypoCSS']: postxPresetAttr('typoCSS', newObj) });
                setCurrentPresetTypos({...newObj, ['presetTypoCSS']: postxPresetAttr('typoCSS', newObj) });
            }
        } );
        
    }, []);

    const setGlobalTypo = (data = {}) => {

        const styleCss = postxPresetAttr('typoCSS', data);

        const starterIframe = document.querySelector('#ultp-starter-preview');
        if(starterIframe.contentWindow) {
            const data = {
                type: 'replaceColorRoot',
                id: '#ultp-preset-typo-style-inline-css',
                styleCss: styleCss,
            }
            starterIframe.contentWindow.postMessage(data, "*");
        }
        return styleCss;
    }

    const changeStates = (value) => {
        const newObj = {...currentPresetTypos, ...value};
        const presetTypoCSS = setGlobalTypo(newObj);
        setCurrentPreset(newObj);
        setCurrentPresetTypos({...newObj, ['presetTypoCSS']: presetTypoCSS });
    }

    return (
        <div className='ultp_starter_preset_container'>
            <div className="ultp_starter_reset_container">
                <div className="packs_title">{__('Change Font & Typography', 'ultimate-post')}</div>
                { currentTypoStack && <span className="dashicons dashicons-image-rotate" onClick={() =>{ setCurrentTypoStack(''); changeStates(netSitePresetTypo) }}></span> }
            </div>
            <ul className='ultp-typo-group'>
                { typoStacks.map((val, key) => {
                    return <li title={`${val[0].family}/${val[1].family}`}  className={`ultp_starter_preset_typo_list ${currentTypoStack == key+1 ? 'active' : ''}`} key={key} onClick={() => {
                            const presentObj = {};
                            setCurrentTypoStack(key+1);
                            presetTypoKeys.forEach((el, i) => {
                                presentObj[el] = {...currentPresetTypos[el]};
                                presentObj[el]['family'] = val[i]['family'];
                                presentObj[el]['type'] = val[i]['type'];
                                presentObj[el]['weight'] = val[i]['weight'];
                            });
                            changeStates(presentObj);
                        }}>
                        {val.map((v, k) => {
                            return <span key={k}>
                                <style>{postxPresetAttr('font_load', v, true)}</style>
                                <span key={k} className={``} style={{fontFamily: `${v.family}, ${v.type}`}}>{k == 0 ? 'A' : 'a'}</span>
                            </span>
                        })}
                    </li>
                }) }
            </ul>
        </div>
    );
};

export default PresetTypoPannel;