const { __ } = wp.i18n
import { useState, useEffect } from 'react';
import IconPack from "../../helper/fields/tools/IconPack";
import "./starterPack.scss"
import PresetColorPanel from './PresetColorPanel';
import PresetTypoPannel from './PresetTypoPannel';
import { handlePostxPresets, jsonDataParsing, postxPresetAttr } from '../../helper/CommonFunctions';
import { getUpgradeProBtn } from '../Settings';
import Skeleton from '../utility/Skeleton';

const StarterDemo = (props) => {
    const {_val, setLiveUrl, liveUrl } = props;
    const [currentPresetColors, setCurrentPresetColors] = useState({});
    const [currentPresetTypos, setCurrentPresetTypos] = useState({});
    const [currentPostxGlobal, setCurrentPostxGlobal] = useState({});
    const initialImportArgs = {deletePrevious: 'yes', installPlugin: 'yes', user_email: ultp_dashboard_pannel.user_email, get_newsletter: 'yes', importDummy: 'yes'};
    
    const [settinsShow, setSettinsShow] = useState(false);
    const [importing, setImporting] = useState(false);
    const [loading, setLoading] = useState(false);
    const [excludePage, setExcludePage] = useState([]);
    const [importArgs, setImporArgs] = useState(initialImportArgs);
    const [percentageData, setPercentageData] = useState(0);
    const [responsiveData, setResponsiveData] = useState({type: 'desktop', width: '100%'});
    const [showMore, setShowMore] = useState(false);
    const [collapseActive, setCollapseActive] = useState(true);
    const [isIframeLoaded, setIsIframeLoaded] = useState(false);
    const [importLoadingArgs, setImportLoadingArgs] = useState({
        plugin: false,
        content: false
    });
    const [importFailed, setImportFailed] = useState(false);

    const [demoSiteColor, setDemoSiteColor] = useState({});
    const [demoSiteTypo, setDemoSiteTypo] = useState({});
    const [demoSitePlugin, setDemoSitePlugin] = useState([]);

    const fetchDemoSiteGlobals = () => {
        window.fetch('https://postxkit.wpxpo.com/'+_val.live+'/wp-json/importer/global_settings', {
            method: 'GET'
        })
        .then(response => response.text())
        .then((res) => {
            const data = jsonDataParsing(res, {});
            if ( data.success ) {
                fetchUseSiteGlobals(data.postx_global);
                setDemoSiteColor(data.ultpPresetColors);
                setDemoSiteTypo(data.ultpPresetTypos);
                setDemoSitePlugin(data.plugins);
            }
        })
        .catch((error) => {
            // console.log(error);
        })
    }

    const fetchUseSiteGlobals = (postx_global={}) => {
        wp.apiFetch({
            path: '/ultp/v1/action_option',
            method: 'POST',
            data: { type: 'get'}
        }).then( res => {
            if (res.success) {
                let newObj = {...res.data, ...postx_global};
                newObj = {...newObj, ['globalCSS']: postxPresetAttr('globalCSS', newObj)}
                setCurrentPostxGlobal(newObj);
            }
        })
    }

    useEffect(() => {
        fetchDemoSiteGlobals();
    }, []);

    const startImporting = async (api_endpoint) => {
        setImporting(true);
        let percent = 0;
		let interval;
        async function setPercentageValue(start, upto, delay) {
            interval = setInterval(() => {
                if (start >= upto) {
                    clearInterval(interval);
                    return;
                }
                start++;
                percent++;
                setPercentageData(start);
            }, delay);
        }
        setPercentageValue(percent, 70, importArgs.importDummy == 'yes' ? 800 : 400 );

        if ( api_endpoint ) {
            setImporting(true);
            setLoading(true);
            const pluginList = demoSitePlugin;

            // update global settings for colors and typo
            handlePostxPresets('set','ultpPresetColors', currentPresetColors );
            handlePostxPresets('set','ultpPresetTypos', currentPresetTypos );
            
            // update postx global
            wp.apiFetch({
                method: 'POST',
                path: '/ultp/v1/action_option',
                data: { type: 'set', data: currentPostxGlobal }
            });
            
            // Delete previous post, terms and get newsletters
            if ( importArgs.deletePrevious == 'yes' || importArgs.get_newsletter == 'yes' ) {
                const deletePromise = new Promise((resolve, reject) => {
                    wp.apiFetch({
                        path: '/ultp/v3/deletepost_getnewsletters',
                        method: 'POST',
                        data: { deletePrevious: importArgs.deletePrevious, get_newsletter: importArgs.get_newsletter }
                    })
                    .then((response) => {
                        resolve('responsed');
                    })
                });
                const deleteResults = await deletePromise;
            }

            //import dummy pages
            if ( importArgs.importDummy == 'yes' ) {
                const dummyPostPromise = new Promise((resolve, reject) => {
                    wp.apiFetch({
                        path: '/ultp/v3/starter_dummy_post',
                        method: 'POST',
                        data: {
                            api_endpoint: api_endpoint,
                            importDummy: importArgs['importDummy']
                        }
                    })
                    .then((response) => {
                        resolve('responsed');
                    })
                    .catch((error) => {
                        console.log(error);
                        resolve('responsed');
                    })
                });
                const dummyPostResults = await dummyPostPromise;
            }
            
            // Plugin Imports
            if ( importArgs?.installPlugin == 'yes' ) {
                setImportLoadingArgs({...importLoadingArgs, plugin: true});
                const pluginPromises = pluginList.map((plugin,k) => {
                    return new Promise((resolve, reject) => {
                        jQuery.ajax({
                            type: 'POST',
                            url: ultp_dashboard_pannel.ajax,
                            data: {
                                action: 'install_required_plugin',
                                wpnonce: ultp_dashboard_pannel.security,
                                plugin: JSON.stringify(plugin)
                            },
                        }).done( function( response) {
                            resolve('responsed');
                        })
                    });
                });
                
                //  wait till all plugin install complete.
                const installResults = await Promise.all(pluginPromises);
                setImportLoadingArgs({...importLoadingArgs, plugin: false});
            }

            clearInterval(interval);
            setPercentageValue(percent+1, 80, 500);
            
            // Page, Builder Template Imports
            const pages = _val.templates?.filter(item => !excludePage.includes(item.name));
            if ( pages.length > 0 ) {
                setImportLoadingArgs({...importLoadingArgs, content: true});
                wp.apiFetch({
                    path: '/ultp/v3/starter_import_content',
                    method: 'POST',
                    data: { 
                        excludepages: JSON.stringify(excludePage),
                        api_endpoint: api_endpoint,
                        importDummy: importArgs['importDummy']
                    }
                })
                .then((response) => {
                    clearInterval(interval);
                    setPercentageValue(percent+1, 100, 50);
                    setTimeout(() => {
                        setLoading(false);
                        setImportLoadingArgs({...importLoadingArgs, content: false});
                    }, (100-percent+2)*50);
                    if ( !response.success ) {
                        setImportFailed(true);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    setPercentageValue(percent+1, 100, 50);
                    setTimeout(() => {
                        setLoading(false);
                        setImportLoadingArgs({...importLoadingArgs, content: false});
                    }, (100-percent+2)*50);
                })
            }
        }
    }
    const inputFields = (key, type, desc, classes='') => {
        return <div className="input_container">
            {
                type == 'checkbox' && <input
                    id={key}
                    className={classes}
                    name={key}
                    type={type}
                    defaultChecked={(importArgs[key] && (importArgs[key] == 'yes') ? true : false)}
                    onChange={(e) => {
                        const _final = e.target.checked ? 'yes' : 'no';
                        setImporArgs({ ...importArgs, [key]: _final });
                    }}
                />
            }
            {

                type != 'checkbox' && <input
                    id={key}
                    className={classes}
                    name={key}
                    type={type}
                    defaultValue={importArgs[key] || ''}
                    onChange={(e) => {
                        const _final = e.target.value;
                        setImporArgs({ ...importArgs, [key]: _final });
                    }}
                />
            }
            {
                desc && <span>
                    <span className="ultp-info">{desc}</span>
                </span>
            }
        </div>
    }

    const setGlobalColorAtDemo = () => {
        const starterIframe = document.querySelector('#ultp-starter-preview');
        if ( currentPresetColors.hasOwnProperty('Base_1_color')) {
            const styleCss = postxPresetAttr('styleCss', currentPresetColors);
            if(starterIframe.contentWindow) {
                const colorData = {
                    type: 'replaceColorRoot',
                    id: '#ultp-preset-colors-style-inline-css',
                    styleCss: styleCss,
                    dlMode: currentPostxGlobal.enableDark ? 'ultpDark' : 'ultpLight',
                }
                starterIframe.contentWindow.postMessage(colorData, "*");
            }
        }

        // change typo
        if ( currentPresetTypos.hasOwnProperty('Body_and_Others_typo') ) {
            const typoCss = postxPresetAttr('typoCSS', currentPresetTypos);
            if(starterIframe.contentWindow) {
                const typoData = {
                    type: 'replaceColorRoot',
                    id: '#ultp-preset-typo-style-inline-css',
                    styleCss: typoCss,
                }
                starterIframe.contentWindow.postMessage(typoData, "*");
            }
        }
    }

    const settingsPopup = () => {

        return <div className="ultp-stater-container-settings-overlay">
            <div className="ultp-stater-settings-container">
                <div>
                    <div className="ultp-popup-stater">
                        { 
                            importing ? <>
                                {
                                    loading ? 
                                    <div className="ultp_processing_import">
                                        <div className="stater_title">{__(`Started building ${_val.title} website`,'ultimate-post')}</div>
                                        <div>
                                            <div className="ultp_import_builders ultp-info">
                                                {__('The import process can take a few seconds depending on the size of the kit you are importing and speed of the connection.', 'ultimate-post')} <br/>
                                                {
                                                    ( importLoadingArgs.plugin || importLoadingArgs.content ) && <div className="progress">
                                                        <div><strong>Progress:</strong> { importLoadingArgs.plugin ? 'Plugin Installation is' :  importLoadingArgs.content ? 'Page/Posts/Media Importing is' : 'Site Importing'} on progress..</div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        <div className="ultp_processing_show">
                                            <div className="ultp-importer-loader">
                                                <div id="ultp-importer-loader-bar" style={{width: percentageData+'%'}}></div>
                                                <div id="ultp-importer-loader-percentage" style={{color: percentageData > 52 ? '#fff' : '#000' }}>{percentageData+'%'}</div>
                                            </div>
                                        </div>
                                        <div className="ultp_import_notice">
                                            <span>{__('Note: ', 'ultimate-post')}</span>
                                            {__('Please do not close this browser window until import is completed.', 'ultimate-post')}
                                        </div>
                                    </div>
                                    :
                                    <div className="ultp_successful_import">
                                        <div className="stater_title">{_val.title}{__(` Imported ${importFailed ? 'Failed' : 'Successfully'} `,'ultimate-post')}</div>
                                        <div>
                                            {
                                                importFailed ?
                                                <>
                                                    <div className="ultp_import_builders">
                                                        {__(' Due to resquest timeout this import is failed', 'ultimate-post')}
                                                        <a className='cursor' onClick={()=> { window.location.reload() } } >{__('Refresh', 'ultimate-post')}</a>
                                                        {__(' page and try again', 'ultimate-post')}
                                                    </div>
                                                </>
                                                :
                                                <>
                                                    <div className="ultp_import_builders">
                                                        {__('Navigate to ', 'ultimate-post')}
                                                        <a className='cursor' onClick={()=> { window.location.href = ultp_dashboard_pannel.builder_url; window.location.reload() } } >{__('Site Builder to edit', 'ultimate-post')}</a>
                                                        {__(' your Archive, Post, Default Page and other templates.', 'ultimate-post')}
                                                    </div>
                                                    <a className="ultp-primary-button" href={ultp_dashboard_pannel.home_url} >{__('View Your Website', 'ultimate-post')}</a>
                                                </>
                                            }
                                        </div>
                                    </div>
                                }
                            </> 
                            : 
                            <div>
                                <div className="ultp-info ultp-info-desc"> {__('Import the entire site including posts, images, pages, content and plugins.','ultimate-post')}</div>
                                <div>
                                    <div className="stater_title">{__('Import Settings','ultimate-post')}</div>
                                    { inputFields('importDummy', 'checkbox', __('Import dummy post, taxonomy and featured images', 'ultimate-post')) }
                                    { inputFields('deletePrevious', 'checkbox', __('Delete Previously imported sites', 'ultimate-post')) }
                                    { inputFields('installPlugin', 'checkbox', __('Install required plugins', 'ultimate-post')) }
                                </div>
                                <div className="starter_page_impports">
                                    <div className="stater_title">{__('Template/Pages','ultimate-post')}</div>
                                    {
                                        _val?.templates?.map((val, k) => {
                                            return ((!showMore && k < 3) || showMore) && <div key={k} className="input_container">
                                                <input
                                                    type="checkbox"
                                                    defaultChecked={excludePage.includes(val.name) ? false : true}
                                                    onChange={(e) => {
                                                        if(e.target.checked && excludePage.includes(val.name)) {
                                                            setExcludePage(excludePage.filter(item => item !== val.name))
                                                        } else if(!e.target.checked && !excludePage.includes(val.name)) {
                                                            setExcludePage([...excludePage, val.name])
                                                        }
                                                    }}
                                                />
                                                <span>
                                                    <span className="ultp-info">{val.name}</span>
                                                </span>
                                            </div>
                                        })
                                    }
                                    {
                                        _val.templates.length > 3 && <div className="cursor" onClick={() => { setShowMore(!showMore) }}>{`Show ${showMore ? 'less' : 'more'}`} {IconPack['videoplay']}</div>
                                    }
                                </div>
                                <div>
                                    <div className="stater_title">{__('Subscribe','ultimate-post')}</div>
                                    <span>{__('Stay up to date with the latest started templates and special offers', 'ultimate-post')}</span>
                                    { inputFields('user_email', 'email', '', 'email_box') }
                                    { inputFields('get_newsletter', 'checkbox', __('Stay updated with exciting features and news.'), 'get_newsletter') }
                                </div>
                            </div>
                        }
                    </div>
                    {
                        !importing && <div className='starter_import '>
                            <a className='ultp-primary-button' onClick={()=> startImporting('https://postxkit.wpxpo.com/'+_val.live)} >{__('Start Importing','ultimate-post')}</a>
                        </div>
                    }
                    <button onClick={()=> { if(!loading) { setImporArgs(initialImportArgs); setImporting(false); setPercentageData(0); setLoading(false); setSettinsShow(false) }}} className={`ultp-popup-close ${loading ? 's_loading' : ''}`}>{IconPack['close_line']}</button>
                </div>
            </div>
        </div>
    }
    
    return (
        <>
            <div className={` ultp_starter_packs_demo theme-install-overlay wp-full-overlay expanded ${collapseActive ? 'active' : 'inactive'}`} style={{display: 'block'}}>
                <div className={`wp-full-overlay-sidebar `}>
                    <div className="wp-full-overlay-header">
                        
                        <div className="ultp_starter_packs_demo_header">
                            <div className="packs_title">
                                {_val.title}
                                <span>{_val.category}</span>
                            </div>
                            <button onClick={() => setLiveUrl('')} className="close-full-overlay"></button>
                        </div>
                        <div 
                            className={`ultp-starter-collapse ${collapseActive ? 'active' : 'inactive'}`}
                            onClick={() => {setCollapseActive(!collapseActive)}}
                        >
                            {IconPack['collapse_bottom_line']}
                        </div>
                    </div>
                    {Array(6).fill(1).map((val, k) => {
                        return (
                            <div key={k} className="ultp-addon-item ultp-dash-item-con">
                                <div className="ultp-addon-item-contents">
                                    <div className="ultp-addon-item-name">
                                        <Skeleton type="custom_size" c_s={{ size1: 50, unit1: 'px',  size2: 50, unit2: 'px', br: 18 }}/>
                                        <Skeleton type="custom_size" c_s={{ size1: 190, unit1: 'px',  size2: 28, unit2: 'px', br: 4 }}/>
                                    </div>
                                    <div className="ultp-description">
                                        <Skeleton type="custom_size" classes="loop" c_s={{ size1: 100, unit1: '%',  size2: 14, unit2: 'px', br: 2 }}/>
                                        <Skeleton type="custom_size" classes="loop" c_s={{ size1: 70, unit1: '%',  size2: 14, unit2: 'px', br: 2 }}/>
                                    </div>
                                </div>
                                <div className="ultp-addon-item-actions ultp-dash-control-options">
                                    <Skeleton type="custom_size" c_s={{ size1: 40, unit1: 'px',  size2: 20, unit2: 'px', br: 8 }}/>
                                    <div className="ultp-docs-action">
                                        <Skeleton type="custom_size" c_s={{ size1: 60, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                        <Skeleton type="custom_size" c_s={{ size1: 50, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                        <Skeleton type="custom_size" c_s={{ size1: 60, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                    <div className="wp-full-overlay-sidebar-content">
                        {
                            ( !demoSiteColor.hasOwnProperty('Base_1_color')  ) ? <div className="skeletonOverflow">
                                <Skeleton type="custom_size" c_s={{ size1: 140, unit1: 'px',  size2: 40, unit2: 'px', br: 40 }}/>
                                <div className="skeletonOverflow_colors">
                                    <Skeleton type="custom_size" c_s={{ size1: 140, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                    <div className="demos-color">
                                    {Array(10).fill(1).map((val, k) => {
                                        return (
                                            <Skeleton key={k} type="custom_size" c_s={{ size1: 100, unit1: 'px',  size2: 30, unit2: 'px', br: 6 }}/>
                                        )
                                    })}
                                    </div>
                                </div>
                            </div> 
                            :
                            <>
                                <PresetColorPanel
                                    ultpPresetColors={demoSiteColor}
                                    currentPresetColors={currentPresetColors}
                                    setCurrentPresetColors={setCurrentPresetColors}
                                    currentPostxGlobal={currentPostxGlobal}
                                    setCurrentPostxGlobal={setCurrentPostxGlobal}
                                />
                            </>
                        }
                        {
                            ( !demoSiteTypo.hasOwnProperty('Body_and_Others_typo')  ) ? <div className="skeletonOverflow">
                                <div className="skeletonOverflow_colors">
                                    <Skeleton type="custom_size" c_s={{ size1: 140, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                    <div className="demos-color">
                                    {Array(10).fill(1).map((val, k) => {
                                        return (
                                            <Skeleton key={k} type="custom_size" c_s={{ size1: 100, unit1: 'px',  size2: 30, unit2: 'px', br: 6 }}/>
                                        )
                                    })}
                                    </div>
                                </div>
                            </div> 
                            :
                            <>
                                <PresetTypoPannel
                                    ultpPresetTypos={demoSiteTypo}
                                    currentPresetTypos={currentPresetTypos}
                                    setCurrentPresetTypos={setCurrentPresetTypos}
                                />
                            </>
                        }
                    </div>
                    <div className="wp-full-overlay-footer">
                        <div className="ultp_starter_import_options" >
                            <div className="option_buttons">
                                {
                                    _val.pro && !ultp_dashboard_pannel.active  ? getUpgradeProBtn(`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-starter&utm_medium=${_val.live}-upgrade-pro&utm_campaign=postx-dashboard`, '')
                                    :
                                    <a className='ultp-primary-button' onClick={() => {
                                        setSettinsShow(true);
                                    }}>{__('Import Site','ultimate-post')}</a>
                                }
                            </div>
                            <div className="ultp-starter-packs-device-container">
                                <span onClick={() => setResponsiveData({type: 'desktop', width: '100%'})}  className={`ultp-starter-packs-device ${responsiveData['type'] == 'desktop' ? 'd-active' : ''}`}>{IconPack.desktop}</span>
                                <span onClick={() => setResponsiveData({type: 'tablet', width: (currentPostxGlobal['breakpointSm'] || '990') + 'px'})}  className={`ultp-starter-packs-device ${responsiveData['type'] == 'tablet' ? 'd-active' : ''}`}>{IconPack.tablet}</span>
                                <span onClick={() => setResponsiveData({type: 'mobile', width: (currentPostxGlobal['breakpointXs'] || '767') + 'px'})}  className={`ultp-starter-packs-device ${responsiveData['type'] == 'mobile' ? 'd-active' : ''}`}>{IconPack.mobile}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="wp-full-overlay-main">
                    {
                        !isIframeLoaded && <div className="iframe_loader">
                            <div className="iframe_container">
                                <div className="iframe_header">
                                    <div className="iframe_header_top">
                                        <div className="header_top_left">
                                            <Skeleton type="custom_size" c_s={{ size1: 60, unit1: 'px',  size2: 60, unit2: 'px', br: 60 }}/>
                                        </div>
                                        <div className="header_top_right">
                                            <Skeleton type="custom_size" c_s={{ size1: 90, unit1: 'px',  size2: 30, unit2: 'px', br: 4 }}/>
                                            <Skeleton type="custom_size" c_s={{ size1: 90, unit1: 'px',  size2: 30, unit2: 'px', br: 4 }}/>
                                            <Skeleton type="custom_size" c_s={{ size1: 90, unit1: 'px',  size2: 30, unit2: 'px', br: 4 }}/>
                                            <Skeleton type="custom_size" c_s={{ size1: 90, unit1: 'px',  size2: 30, unit2: 'px', br: 4 }}/>
                                            { 
                                                Array(3).fill(1).map((val, k) => {
                                                    return (
                                                        <Skeleton key={k} type="custom_size" c_s={{ size1: 30, unit1: 'px',  size2: 30, unit2: 'px', br: 30 }}/>
                                                        )
                                                    })
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="iframe_body_content">
                                    <div className="iframe_body_slider">
                                        <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 300, unit2: 'px', br: 2 }}/>
                                    </div>
                                    <Skeleton type="custom_size" c_s={{ size1: 190, unit1: 'px',  size2: 36, unit2: 'px', br: 2 }}/>
                                    <div className="iframe_body">
                                        <div className="iframe_body_left">
                                            <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 300, unit2: 'px', br: 2 }}/>
                                        </div>
                                        <div className="iframe_body_right">
                                            <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 140, unit2: 'px', br: 2 }}/>
                                            <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 140, unit2: 'px', br: 2 }}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                    <iframe className={`${responsiveData['type']}View`} onLoad={()=> {setIsIframeLoaded(true); setGlobalColorAtDemo()}} style={{ display: 'block', margin: '0 auto', maxWidth: responsiveData['width']}} id="ultp-starter-preview" src={'https://postxkit.wpxpo.com/'+liveUrl}></iframe>
                </div>
            </div>
            {
                settinsShow && settingsPopup()
            }
        </>
    );
};

export default StarterDemo;