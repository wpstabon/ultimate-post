const { __ } = wp.i18n
import { useState, useEffect } from 'react';
import { getPresetColorLists, handlePostxPresets, postxPresetAttr } from '../../helper/CommonFunctions';
import dlIcons from '../../blocks/dark_light/dlIcons';

const PresetColorPanel = ({ultpPresetColors, currentPresetColors, setCurrentPresetColors, setCurrentPostxGlobal, currentPostxGlobal}) => {
    const netSitePresetColors = {
        ...ultpPresetColors,
        ['rootCSS']: postxPresetAttr('styleCss', ultpPresetColors)
    } 
    const [currentPreset, setCurrentPreset] = useState({...currentPresetColors, ...netSitePresetColors});
    const [currentColorStack, setCurrentColorStack] = useState('');
    const [isDarkChecked, setIsDarkChecked] = useState(false);

    const colorStacks = postxPresetAttr('colorStacks');
    const presetColorKeys = postxPresetAttr('presetColorKeys');
    
    useEffect(() => {
        handlePostxPresets('get','ultpPresetColors', '', (res) => {
            if(res.data) {
                const newObj = {...res.data, ...currentPreset};
                setCurrentPresetColors({...newObj, ['rootCSS']: postxPresetAttr('styleCss', newObj) });
            }
        } );
        
    }, []);
    
    const setGlobalColor = (data = {}, src='') => {

        const styleCss = postxPresetAttr('styleCss', data);

        const starterIframe = document.querySelector('#ultp-starter-preview');
        if(starterIframe.contentWindow) {
            const data = {
                type: 'replaceColorRoot',
                id: '#ultp-preset-colors-style-inline-css',
                styleCss: styleCss,
                dlMode: src == 'darkhandle' ? ( isDarkChecked ? 'ultpLight' : 'ultpDark' ) : ( isDarkChecked ? 'ultpDark' : 'ultpLight' ),
            }
            starterIframe.contentWindow.postMessage(data, "*");
        }
        return styleCss;
    }

    const changeStates = ( value, src='' ) => {
        let newObj = {...currentPresetColors, ...value};

        if( src !='darkhandle' && isDarkChecked) {
            newObj = darkSwitcher(newObj);
        }
        const rootCSS = setGlobalColor(newObj, src);
        setCurrentPreset(newObj);
        setCurrentPresetColors({...newObj, ['rootCSS']: rootCSS });
    }

    const darkSwitcher = (swithObj={}) => {
        const newObj = { 
            ...swithObj,
            ['Base_1_color']: swithObj['Contrast_1_color'],
            ['Base_2_color']: swithObj['Contrast_2_color'],
            ['Base_3_color']: swithObj['Contrast_3_color'],
            ['Contrast_1_color']: swithObj['Base_1_color'],
            ['Contrast_2_color']: swithObj['Base_2_color'],
            ['Contrast_3_color']: swithObj['Base_3_color'],
        }
        return newObj;
    }

    const handleDarkMode = () => {

        const newObj = darkSwitcher(currentPresetColors);
        document.querySelector('.ultp-dl-container .ultp-dl-svg-con').style = `transform: translateX(${isDarkChecked ? '' : 'calc( 100% + 71px )'}); transition: transform .4s ease`;
        document.querySelector('.ultp-dl-container .ultp-dl-svg-title').style = `transform: translateX(${isDarkChecked ? '' : 'calc( -100% + 50px )'}); transition: transform .4s ease`;
        setTimeout(() => {
            setIsDarkChecked(!isDarkChecked);
            setCurrentPostxGlobal({...currentPostxGlobal, ['enableDark']: !isDarkChecked })
            changeStates(newObj, 'darkhandle');
        }, 400);
    }

    return (
        <div className="ultp_starter_preset_container">
            <div  className="ultp_starter_dark_container">
                <div onClick={()=> handleDarkMode()} className={` ultp-dl-container `}>
                    <div className={`ultp-dl-svg-con ${isDarkChecked ? 'dark' : ''}`}>
                        <div className="ultp-dl-svg">{dlIcons[isDarkChecked ? 'moon' : 'sun' ]}</div>
                    </div>
                    <div className="ultp-dl-svg-title">{isDarkChecked ? 'Dark Mode' : 'Light Mode'}</div>
                </div>
            </div>
            <div className="ultp_starter_reset_container">
                <div className="packs_title">{__('Change Color Palette', 'ultimate-post')}</div>
                { currentColorStack && <span className="dashicons dashicons-image-rotate" onClick={() =>{ setCurrentColorStack(''); changeStates(netSitePresetColors) }}></span> }
            </div>
            <ul className='ultp-color-group'>
                { colorStacks.map((val, key) => {
                    return <li className={`ultp_starter_preset_list ${currentColorStack == key+1 ? 'active' : ''}`} key={key} onClick={() => {
                            const presentObj = {};
                            setCurrentColorStack(key+1);
                            presetColorKeys.forEach((el, i) => {
                                presentObj[el] = val[i];
                            });
                            changeStates(presentObj);
                        }}>
                        {val.map((v, k) => {
                            return ![1, 2, 6, 8, 9].includes(k+1) && <span key={k} className={`ultp-global-color`} style={{backgroundColor:v}}></span>
                        })}
                    </li>
                }) }
            </ul>
        </div>
    );
};

export default PresetColorPanel;