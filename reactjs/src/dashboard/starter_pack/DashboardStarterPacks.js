const { __ } = wp.i18n
import { useState, useEffect, useRef } from 'react';
import Toast from '../utility/Toast';
import StarterFilter from './StarterFilter';
import IconPack from '../../helper/fields/tools/IconPack';
import Skeleton from '../utility/Skeleton';
import StarterDemo from './StarterDemo';

const DashboardStarterPacks = (props) => {
    const [state, setState] = useState({
        templates: [],
        designs: [],
        reloadId: '',
        reload: false,
        isTemplate: true,
        error: false,
        fetching: false,
        loading: false
    });
    const [ searchQuery , setSearchQuery] = useState('');
    const [ trend , setTrend] = useState('all');
    const [ filter , setFilter] = useState('all');
    const [ freePro , setFreePro] = useState('all');
    const [ wishListArr , setWishlistArr] = useState([]);
    const [ showWishList , setShowWishList] = useState(false);
    const [toastMessages, setToastMessages] = useState({
        state: false,
		status: ''
	});

    const [column, setColumn] = useState('3');
    const [liveUrl, setLiveUrl] = useState('');
    
    const [ starterLists, setStarterLists] = useState([]);
    const { loading, fetching,} = state;
    
    const fetchTemplates = async () => {
        setState({...state, loading: true});
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'starter_lists'
            }
        })
        .then((response) => {
            if (response.success) {
                if(response.data){
                    const data = JSON.parse(response.data);
                    setStarterLists(data);
                    setState({...state, loading: false});
                }
            }
        })
    }

    const filterCatArr = [
        { value: 'all',          label: __('All Categories','ultimate-post') },
        { value: 'news',         label: __('News','ultimate-post') },
        { value: 'magazine',     label: __('Magazine','ultimate-post') },
        { value: 'blog',         label: __('Blog','ultimate-post') },
        { value: 'sports',       label: __('Sports','ultimate-post') },
        { value: 'fashion',      label: __('Fashion','ultimate-post') },
        { value: 'tech',         label: __('Tech','ultimate-post') },
        { value: 'travel',       label: __('Travel','ultimate-post') },
        { value: 'food',         label: __('Food','ultimate-post') },
        { value: 'movie',        label: __('Movie','ultimate-post') },
        { value: 'health',       label: __('Health','ultimate-post') },
        { value: 'gaming',       label: __('Gaming','ultimate-post') },
        { value: 'nft',          label: __('NFT','ultimate-post') },
    ]

    useEffect(() => {
        setWListAction('', '', 'fetchData');
        fetchTemplates();
    },[]);

    const _fetchFile = () => {
        setState({...state, fetching: true});
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'fetch_all_data'
            }
        })
        .then((response) => {
            if (response.success) {
                fetchTemplates();
                setState({...state, fetching: false});
                setToastMessages({
                    status: 'success',
                    messages: [response.message],
                    state: true
                });
            }
        })
    }
    const setWListAction = (id, action = '', type = '') => {
        wp.apiFetch({
            path: '/ultp/v2/premade_wishlist_save',
            method: 'POST',
            data: {
                id: id,
                action: action,
                type: type
            }
        })
        .then((res) => {
            if(res.success) {
                setWishlistArr( Array.isArray(res.wishListArr) ? res.wishListArr : Object.values(res.wishListArr || {}));
                if (type != 'fetchData') {
                    setToastMessages({
                        status: 'success',
                        messages: [res.message],
                        state: true
                    });
                }
            }
        })
    }

    const changeStates = (type, value) => {
        
        if(type == 'freePro') {
            setFreePro(value);
        } else if(type == 'search') {
            setSearchQuery(value);
        } else if(type == 'column') {
            setColumn(value);
        } else if(type == 'wishlist') {
            setShowWishList(value);
        } else if(type == 'trend') {
            setTrend(value);
            if (value == 'latest' || value == 'all') {
                starterLists.sort((a, b) => b.ID - a.ID)
            } else if (value == 'popular') {
                if (starterLists[0] && starterLists[0].hit) {
                    starterLists.sort((a, b) => b.hit - a.hit)
                }
            }
        } else if(type == 'filter') {
            setFilter(value);
        } 
    }

    if (liveUrl) { // Showing Live demo of full template
        document.querySelector('#adminmenumain').style = "display: none;";
        document.querySelector('.ultp-settings-container').style = "min-height: unset;";

        const _val = starterLists.filter(e => e.live == liveUrl)[0];
        return <StarterDemo
            _val={_val}
            setLiveUrl={setLiveUrl}
            liveUrl ={liveUrl }
        />
    } else {
        document.querySelector('#adminmenumain').style = "";
        document.querySelector('.ultp-settings-container').style = "";
    }

    return (
        <>  
            <div className="ultp-templatekit-wrap">
                { toastMessages.state && (
                    <Toast
                        delay={2000}
                        toastMessages={toastMessages}
                        setToastMessages={setToastMessages}
                    />
                )}
                <div className={`ultp-templatekit-list-container `}>
                    <StarterFilter 
                        changeStates={changeStates}
                        useState={useState}
                        useEffect={useEffect}
                        useRef={useRef}
                        column={column}
                        showWishList={showWishList}
                        _fetchFile={_fetchFile}
                        fetching={fetching}
                        searchQuery={searchQuery}
                        fields={{filter: true, trend: true, freePro: true}}
                        fieldOptions={{filterArr: filterCatArr, trendArr: [], freeProArr: []}}
                        fieldValue={{filter: filter, trend: trend, freePro: freePro}}
                    />
                    {
                        starterLists.length > 0 ?
                        <>
                            <div className={`ultp-premade-grid ultp-templatekit-col`+column}>

                                { starterLists.map(data => (

                                    (
                                        data.title?.toLowerCase().includes(searchQuery.toLowerCase()) && 
                                        ( filter=='all' || ( (filter != 'all' && ( filter == data.category || (Array.isArray(data.parent_cat) ? data.parent_cat : Object.values(data.parent_cat || {})).includes(filter) )) ) ) && 
                                        ( freePro=='all' || ( (freePro == 'pro' && data.pro) || (freePro == 'free' && !data.pro) ) ) && 
                                        ( !showWishList || (showWishList && wishListArr?.includes(data.ID)) ) 
                                    ) 
                                    &&
                                    <div key={data.ID} className={`ultp-item-wrapper ultp-starter-group `}>
                                        <div  className={`ultp-item-list`}>
                                            <div className="ultp-item-list-overlay">
                                                <a className="ultp-templatekit-img bg-image-aspect" href="#" style={{backgroundImage: `url(https://postxkit.wpxpo.com/${data.live}/wp-content/uploads/sites/${data.ID}/postx_importer_img/pages/home.jpg)`}}></a>
                                                <div className="ultp-list-dark-overlay">
                                                    {!ultp_dashboard_pannel.active && <>
                                                        {
                                                            data.pro ?
                                                            <span className="ultp-templatekit-premium-btn">{__('Pro','ultimate-post')}</span>
                                                            :
                                                            <span className="ultp-templatekit-premium-btn ultp-templatekit-premium-free-btn">{__('Free','ultimate-post')}</span>
                                                        }
                                                        </>
                                                    }
                                                    <a className="ultp-overlay-view ultp-dashboverlay" onClick={()=> setLiveUrl(data.live)}>
                                                        <span className="dashicons dashicons-visibility"></span> {__('Live Preview','ultimate-post')}
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div className="ultp-item-list-info">
                                                <div className="ultp-list-info" onClick={()=> setLiveUrl(data.live)}>
                                                    <span className="ultp-list-info-title">{data.title}</span> <br/>
                                                    <span className="ultp-list-info-count">{data.templates?.length && data.templates?.length + ' templates'}</span>
                                                </div>
                                                <span className="ultp-action-btn">
                                                    <span className="ultp-premade-wishlist" onClick={()=> { setWListAction(data.ID, wishListArr?.includes(data.ID) ? 'remove' : '')}}>{IconPack[wishListArr?.includes(data.ID) ? 'love_solid' : 'love_line']}</span>
                                                </span>
                                            </div>
                                            
                                        </div>
                                        <div className='ultp-starter-shape'></div>
                                    </div>
                                ))}
                            </div>
                        </>
                        :
                        loading ? 
                        <div className="ultp-premade-grid ultp-templatekit-col3 skeletonOverflow">
                            { Array(25).fill(1).map((v, i) => {
                                return(
                                    <div key={i} className="ultp-item-list">
                                        <div className="ultp-item-list-overlay">
                                            <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 400, unit2: 'px' }}/>
                                        </div>
                                        <div className="ultp-item-list-info">
                                            <Skeleton type="custom_size" c_s={{ size1: 50, unit1: '%',  size2: 25, unit2: 'px', br: 2 }}/>
                                            <span className="ultp-action-btn">
                                                <span className='ultp-premade-wishlist'><Skeleton type="custom_size" c_s={{ size1: 30, unit1: 'px',  size2: 25, unit2: 'px', br: 2 }}/></span>
                                            </span>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                        : 
                        <span className="ultp-image-rotate">{__('No Data Available...','ultimate-post')}</span>
                    }

                </div>
                
            </div>
        </>
    );
}

export default DashboardStarterPacks;