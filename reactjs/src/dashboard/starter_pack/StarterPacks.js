const { __ } = wp.i18n
import UltpLinkGenerator from '../../helper/UltpLinkGenerator'
import IconPack from '../../helper/fields/tools/IconPack';
import Skeleton from "../utility/Skeleton"
import Toast from '../utility/Toast';
import StarterFilter from './StarterFilter';

const patternCat = [
    { label: __('All', 'ultimate-post'),                value: 'all'},
    { label: __('Menu - PostX', 'ultimate-post'),       value: 'menu'},
    { label: __('Post Grid #1', 'ultimate-post'),       value: 'post-grid-1'},
    { label: __('Post Grid #2', 'ultimate-post'),       value: 'post-grid-2'},
    { label: __('Post Grid #3', 'ultimate-post'),       value: 'post-grid-3'},
    { label: __('Post Grid #4', 'ultimate-post'),       value: 'post-grid-4'},
    { label: __('Post Grid #5', 'ultimate-post'),       value: 'post-grid-5'},
    { label: __('Post Grid #6', 'ultimate-post'),       value: 'post-grid-6'},
    { label: __('Post Grid #7', 'ultimate-post'),       value: 'post-grid-7'},
    { label: __('Post List #1', 'ultimate-post'),       value: 'post-list-1'},
    { label: __('Post List #2', 'ultimate-post'),       value: 'post-list-2'},
    { label: __('Post List #3', 'ultimate-post'),       value: 'post-list-3'},
    { label: __('Post List #4', 'ultimate-post'),       value: 'post-list-4'},
    { label: __('Post Slider #1', 'ultimate-post'),     value: 'post-slider-1'},
    { label: __('Post Slider #2', 'ultimate-post'),     value: 'post-slider-2'},
    { label: __('Post Module #1', 'ultimate-post'),     value: 'post-module-1'},
    { label: __('Post Module #2', 'ultimate-post'),     value: 'post-module-2'},
    { label: __('News ticker', 'ultimate-post'),        value: 'news-ticker'},
    { label: __('Taxonomy', 'ultimate-post'),           value: 'ultp-taxonomy'},
    { label: __('Table of Contents', 'ultimate-post'),  value: 'table-of-content'},
    { label: __('Button Group', 'ultimate-post'),       value: 'button-group'},
    { label: __('List - PostX', 'ultimate-post'),       value: 'advanced-list'},
    { label: __('Search - PostX', 'ultimate-post'),     value: 'advanced-search'},
];

const filterCatArr = [
    { value: 'all',          label: __('All Categories','ultimate-post') },
    { value: 'news',         label: __('News','ultimate-post') },
    { value: 'magazine',     label: __('Magazine','ultimate-post') },
    { value: 'blog',         label: __('Blog','ultimate-post') },
    { value: 'sports',       label: __('Sports','ultimate-post') },
    { value: 'fashion',      label: __('Fashion','ultimate-post') },
    { value: 'tech',         label: __('Tech','ultimate-post') },
    { value: 'travel',       label: __('Travel','ultimate-post') },
    { value: 'food',         label: __('Food','ultimate-post') },
    { value: 'movie',        label: __('Movie','ultimate-post') },
    { value: 'health',       label: __('Health','ultimate-post') },
    { value: 'gaming',       label: __('Gaming','ultimate-post') },
    { value: 'nft',          label: __('NFT','ultimate-post') },
]

const StarterPacks = (props) => {
    const {filterValue, state, setState, useState, useEffect, useRef,  _changeVal, splitArchiveData, dashboard, setWListAction, wishListArr, starterListModule, setStarterListModule } = props;
    const { current, isStarterLists, error, starterLists, designs, reload, reloadId, fetching, loading, starterChildLists, starterParentLists } = state;
    const localizedData = dashboard ? ultp_dashboard_pannel : ultp_data;

    const [column, setColumn] = useState('3');
    // const [starterListModule, setStarterListModule] = useState('');
    const [ searchQuery , setSearchQuery] = useState('');
    const [ trend , setTrend] = useState('all');
    const [ freePro , setFreePro] = useState('all');
    const [ showWishList , setShowWishList] = useState(false);
    const [ toastMessages, setToastMessages ] = useState({
		state: false,
		status: ''
	});

    const isHomePage = localizedData?.archive == 'front_page';

    let premadeLists = [...current];
    let isStarterParents = isStarterLists ? ( searchQuery ? false : true ) : false;
    const isBuilder = ["singular", "archive", "header", "footer", "404"].includes(localizedData.archive);
    
    if( isStarterLists && searchQuery ) {
        premadeLists = [...starterChildLists];
        isStarterParents = false;
    }
    if (trend == 'latest' || trend == 'all') {
        premadeLists.sort((a, b) => b.ID - a.ID)
    } else if (trend == 'popular') {
        if (premadeLists[0] && premadeLists[0].hit) {
            premadeLists.sort((a, b) => b.hit - a.hit)
        }
    }

    const _setData = (type) => {
        let data = [];
        const filterTxt = isStarterLists ? 'starterListsFilter' : 'designFilter';
        data = isStarterLists ? starterLists : designs;
        if (type == 'all') {
            setState({...state, [filterTxt]: type, current: data});
        } else {
            setState({...state, [filterTxt]: type, current: isStarterLists ? data.filter( (item) => { return item.parent_cat ? ( (Array.isArray(item.parent_cat) ? item.parent_cat : Object.values(item.parent_cat || {})).includes(type) || item.category == type ) : item.category == type }) : splitArchiveData(data , type)});
        }
    }

    const changeStates = (type, value) => {
        
        if(type == 'freePro') {
            setFreePro(value);
        } else if(type == 'search') {
            setSearchQuery(value);
        } else if(type == 'column') {
            setColumn(value);
        } else if(type == 'wishlist') {
            setShowWishList(value);
        } else if(type == 'trend') {
            setTrend(value);
        } else if(type == 'filter') {
            _setData(value);
        }
    }

    const importStarterTemplate = (api_endpoint, ID, isPro ) => {
        if(isPro && !localizedData.active) {
            return ;
        }
        setState({...state, reload:true, reloadId:ID});
        wp.apiFetch({
            path: '/ultp/v3/single_page_import',
            method: 'POST',
            data: { api_endpoint: api_endpoint, ID: ID }
        })
        .then((response) => {
            if(response.success) {
                wp.data.dispatch('core/block-editor').insertBlocks(wp.blocks.parse(response.content.content));
                setStarterListModule('');
                setState({...state, isPopup:false, reload:false, reloadId:'', error: false});
            }
        })
    }
    return (
        <>
            <div className="ultp-templatekit-wrap">
                    {toastMessages.state && (
                        <Toast
                            delay={2000}
                            toastMessages={toastMessages}
                            setToastMessages={setToastMessages}
                        />
                    )}
                <div className={`ultp-templatekit-list-container ${isStarterLists && starterListModule ? 'ultp-block-editor': ''}`}>
                    {
                        ( ( isStarterLists && !starterListModule) || !isStarterLists ) && 
                            <StarterFilter
                                changeStates={changeStates}
                                useState={useState}
                                useEffect={useEffect}
                                useRef={useRef}
                                column={column}
                                showWishList={showWishList}
                                searchQuery={searchQuery}
                                fetching={fetching}
                                fields={{filter: isBuilder ? false : true, trend: true, freePro: true}}
                                fieldOptions={{filterArr: isBuilder ? false : isStarterLists ? filterCatArr : patternCat, trendArr: [], freeProArr: []}}
                                fieldValue={{filter: isBuilder ? 'all' : filterValue , trend: trend, freePro: freePro}}
                            />
                    }
                    {
                        starterListModule && isStarterLists && !isBuilder ? 
                            <StarterModuleDetails 
                                useState={useState}
                                starterListModule={starterListModule}
                                starterLists={starterLists}
                                setStarterListModule={setStarterListModule}
                                importStarterTemplate={importStarterTemplate}
                                state={state}
                                setState={setState}
                            /> 
                            :
                            premadeLists?.length > 0 ?
                                <>
                                    <div className={`ultp-premade-grid ultp-templatekit-col`+column}>

                                        { premadeLists.map((data, i) => (
                                            ( 
                                                ( (isBuilder || isHomePage) ? (data.name + ' ' + data.parent)?.toLowerCase().includes(searchQuery.toLowerCase()) : (((isStarterLists && !searchQuery) ? data.title : data.name)?.toLowerCase().includes(searchQuery.toLowerCase())) ) 
                                                && ( (searchQuery && ( (isStarterLists && data.type != 'ultp_builder') || !isStarterLists  ) ) || !searchQuery ) 
                                                && ( freePro=='all' || ((freePro == 'pro' && data.pro) || (freePro == 'free' && !data.pro)) ) 
                                                && ( !showWishList || (showWishList && wishListArr?.includes(data.ID)) ) 
                                            ) &&
                                            <div key={i} className={`ultp-item-wrapper ${isStarterParents ? 'ultp-starter-group' : ''} ${data.parent ? 'ultp-single-item' : ''}`}>
                                                <div  className={`ultp-item-list`}>
                                                    <div className="ultp-item-list-overlay">
                                                        <a className={`ultp-templatekit-img ${['header', 'footer'].includes(data.builder_type) ? 'ultp_hf' : ''} ${( isBuilder || isStarterLists || isHomePage ) ? ' bg-image-aspect' : ''} `} style={ ( isBuilder || isHomePage ) ? {  backgroundImage: `url(https://postxkit.wpxpo.com/${data.live}/wp-content/uploads/sites/${data.parentID}/postx_importer_img/pages/${data.name?.toLowerCase().replaceAll(' ', '_')}.jpg )` } :  isStarterLists ? { backgroundImage: `url(${ isStarterParents ? getStarterImageUrl('https://postxkit.wpxpo.com/'+data.live, data.ID, 'home') : getStarterImageUrl('https://postxkit.wpxpo.com/'+starterParentLists[data.parent]?.live, starterParentLists[data.parent]?.ID, data.name ) })`} : {}}>
                                                            {
                                                                !( isBuilder || isStarterLists || isHomePage ) && <img src={data.image} loading="lazy" alt={ data.name} />
                                                            }
                                                        </a>
                                                        <div className={"ultp-list-dark-overlay"}>
                                                            {   !localizedData.active && <>
                                                                    {
                                                                        data.pro ?
                                                                        <span className="ultp-templatekit-premium-btn">{__('Pro','ultimate-post')}</span>
                                                                        :
                                                                        <span className="ultp-templatekit-premium-btn ultp-templatekit-premium-free-btn">{__('Free','ultimate-post')}</span>
                                                                    }
                                                                </>
                                                            }
                                                            { 
                                                                isStarterLists ? <>
                                                                    {
                                                                        isStarterParents ?
                                                                            <a className="ultp-overlay-view" onClick={() => { setStarterListModule(data.live) }} >{ IconPack.search_line }</a> 
                                                                            :
                                                                            <a className={`ultp-overlay-view ultp-dashboverlay`} href={'https://postxkit.wpxpo.com/'+data.live + '/' +data.name.toLowerCase().replaceAll(' ', '-')} target="_blank">
                                                                                <span className="dashicons dashicons-visibility"></span> {__('Live Preview','ultimate-post')}
                                                                            </a>
                                                                    }
                                                                </>
                                                                : 
                                                                <a className={`ultp-overlay-view ultp-dashboverlay ${['header', 'footer'].includes(data.builder_type) ? 'ultp_hf' : ''}`} href={( isBuilder || isHomePage ) ? 'https://postxkit.wpxpo.com/' + ( (isHomePage || ['header', 'footer'].includes(data.builder_type)) ? data.live : data.live+'/postx_' + ( data.builder_type == 'archive' ? data.archive_type : data.builder_type ) ) : `https://www.wpxpo.com/postx/patterns/#demoid${data.ID}`} target="_blank">
                                                                    <span className="dashicons dashicons-visibility"></span> {__('Live Preview','ultimate-post')}
                                                                </a>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="ultp-item-list-info">
                                                        <div className="ultp-list-info">
                                                            {
                                                                isStarterLists ? 
                                                                     searchQuery ?
                                                                        <>
                                                                            <span>{data.name}</span>
                                                                            <div className="parent">{data.parent} </div>
                                                                        </>
                                                                        :
                                                                        <>
                                                                            <span>{data.title}</span>
                                                                            <div className="parent">{ data?.templates?.length +' templates'} </div>
                                                                        </>
                                                                :
                                                                <span>
                                                                    {data.name}
                                                                    {
                                                                        ( isBuilder || isHomePage ) && <div className="parent">{data.parent} </div>
                                                                    }
                                                                </span>
                                                            }
                                                        </div>
                                                        <span className="ultp-action-btn">
                                                            <span className="ultp-premade-wishlist" onClick={()=> { setWListAction(data.ID, wishListArr?.includes(data.ID) ? 'remove' : '')}}>{IconPack[wishListArr?.includes(data.ID) ? 'love_solid' : 'love_line']}</span>
                                                            {
                                                                isStarterLists ? 
                                                                    ( searchQuery ) && <>
                                                                        {
                                                                            (data.pro && !localizedData.active) ?
                                                                            <a className="ultp-btns ultpProBtn" target="_blank" href={`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=starter-site-upgrade&utm_campaign=postx-dashboard`}>{__('Upgrade to Pro','ultimate-post')}&nbsp; &#10148;</a>
                                                                            :
                                                                            data.type !== 'ultp_builder' && <span onClick={() => importStarterTemplate('https://postxkit.wpxpo.com/'+data.live, data.ID, data.pro)} className="ultp-btns ultp-btn-import"> 
                                                                                { 
                                                                                    !(reload && reloadId == data.ID) && IconPack['arrow_down_line']}{__('Import','ultimate-post')}{(reload && reloadId == data.ID) && <span className="dashicons dashicons-update rotate" />
                                                                                }
                                                                            </span>
                                                                        }
                                                                    </>
                                                                :
                                                                (data.pro && !localizedData.active) ?
                                                                    <a className="ultp-btns ultpProBtn" target="_blank" href={`https://www.wpxpo.com/postx/pricing/${isBuilder ? `?utm_source=db-postx-builder&utm_medium=${localizedData?.archive}-buider-library&utm_campaign=postx-dashboard` : '?utm_source=db-postx-editor&utm_medium=pattern-upgrade&utm_campaign=postx-dashboard'}`}>{__('Upgrade to Pro','ultimate-post')}&nbsp; &#10148;</a>
                                                                    :
                                                                    <span onClick={() => { ( isBuilder || isHomePage ) ? importStarterTemplate('https://postxkit.wpxpo.com/'+data.live, data.ID, data.pro ) :  _changeVal(data.ID, data.pro)}} className="ultp-btns ultp-btn-import"> 
                                                                        { 
                                                                            !(reload && reloadId == data.ID) && IconPack['arrow_down_line']}{__('Import','ultimate-post')}{(reload && reloadId == data.ID) && <span className="dashicons dashicons-update rotate" />
                                                                        }
                                                                    </span>
                                                            }
                                                        </span>
                                                    </div>
                                                    
                                                </div>
                                                <div className={`${isStarterLists && !isBuilder && isHomePage ? 'ultp-starter-shape' : 'ultp-shape-hide'} `}></div>
                                            </div>
                                        ))}
                                    </div>
                                </> 
                                :
                                <>
                                    {
                                        loading  ? 
                                        <div className="ultp-premade-grid ultp-templatekit-col3 skeletonOverflow">
                                            { Array(25).fill(1).map((v, i) => {
                                                return(
                                                    <div key={i} className="ultp-item-list">
                                                        <div className="ultp-item-list-overlay">
                                                            <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 400, unit2: 'px' }}/>
                                                        </div>
                                                        <div className="ultp-item-list-info">
                                                            <Skeleton type="custom_size" c_s={{ size1: 50, unit1: '%',  size2: 25, unit2: 'px', br: 2 }}/>
                                                            <span className="ultp-action-btn">
                                                                <span className='ultp-premade-wishlist'><Skeleton type="custom_size" c_s={{ size1: 30, unit1: 'px',  size2: 25, unit2: 'px', br: 2 }}/></span>
                                                                <Skeleton type="custom_size" c_s={{ size1: 70, unit1: 'px',  size2: 25, unit2: 'px', br: 2 }}/>
                                                            </span>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                        :
                                        <span className="ultp-image-rotate">{__('No Data found...','ultimate-post')}</span>
                                    }
                                </>
                    }
                </div>
            </div>
        </>
    );
};

const getStarterImageUrl = (live, siteID, postTitle) => {
    return `${live}/wp-content/uploads/sites/${siteID}/postx_importer_img/pages/${postTitle.toLowerCase().replaceAll(' ', '_')}.jpg`
}

const StarterModuleDetails = ({ starterListModule, starterLists, setStarterListModule, useState, state, importStarterTemplate }) => {
    const currentStarter = starterLists?.filter( (item) => {
        return item.live == starterListModule ? true : false;
    });
    const { title, live, pro } = currentStarter[0];
    let { templates } = currentStarter[0];

    // Sort
    ['contact', 'about', 'blog', 'home'].forEach( el => {
        templates.forEach( (e, i) => {
            if (e.name.toLowerCase().includes(el)) {
                templates.splice(0, 0, templates.splice(i, 1)[0]);
            }
        });
    });

    const [ lgImg, setLgImg ] = useState(templates[0].name);
    const [ templateID, setTemplateID ] = useState(templates[0].ID);

    const selectedTemplates = templates?.filter( (item) => {
        return item.ID == templateID ? true : false;
    })[0];

    const previewUrl = live + ( selectedTemplates.type == 'page' ? ( selectedTemplates.home_page == 'home_page' ? '' : '/' + selectedTemplates.name?.toLowerCase().replaceAll(' ', '-')) : '/postx_' + ( selectedTemplates.builder_type == 'archive' ? selectedTemplates.archive_type : selectedTemplates.builder_type ) );

    return (
        <div>
            <div className="ultp-module-templates-footer">
                <div className="ultp-module-title">
                    <span onClick={()=> setStarterListModule('')} > { IconPack.collapse_bottom_line } Back </span>   { title }
                </div>
                <div className="ultp-module-btn">
                    { 
                        selectedTemplates.type != 'ultp_builder' && ( !ultp_data.active && pro ? <a className="ultp-btns ultpProBtn" target="_blank" href={`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=starter-site-upgrade&utm_campaign=postx-dashboard`}>{__('Upgrade to Pro','ultimate-post')}&nbsp; &#10148;</a>
                        : <button onClick={() =>  importStarterTemplate('https://postxkit.wpxpo.com/'+live, templateID, pro )} className={`ultp-template-btn ultp-template-btn-fill ${state.reload ? 's_loading' : ''} `}>{__('Import Template', 'ultimate-post')} { IconPack[state.reload ? 'refresh' : 'upload_solid'] }</button> )
                    }
                    {
                        selectedTemplates.type == 'ultp_builder' && <a href={ultp_data.builder_url} target='_blank' className="ultp-template-btn ultp-template-btn-fill">
                            { __('Go to Site Builder', 'ultimate-post') }
                        </a>
                    }
                    <a href={'https://postxkit.wpxpo.com/'+previewUrl} target='_blank' className="ultp-template-btn ultp-template-btn-line">{__('Live Preview', 'ultimate-post')} { IconPack.eye }</a>
                </div>
            </div>
            <div className="ultp-module">
                <div>
                    {
                        templates?.length && 
                        <div className="ultp-module-templates">
                            {templates.map(({type, name, img, ID  }, i) =>
                                {
                                    return (
                                        !( type == 'ultp_builder' && name.toLowerCase().includes('header') || name.toLowerCase().includes('footer') ) && 
                                        <div key={name} className={`ultp-module-item ${templateID == ID ? 'active' : '' }`} onClick={()=> { setTemplateID(ID); setLgImg(name) } }>
                                            <div className="bg-image-aspect" style={{backgroundImage: `url(${getStarterImageUrl( 'https://postxkit.wpxpo.com/'+live, currentStarter[0].ID, name )})`}}></div>
                                            <div className="ultp-module-info">
                                                {name}
                                            </div>
                                        </div>
                                    )
                                }
                            )}
                        </div>
                    }
                </div>
                <div>
                    { selectedTemplates.type == 'ultp_builder' && 
                        <div className='ultp-module-notice'><strong>{__('Note:', 'ultimate-post') }</strong> { __('This is a builder template. Go to ', 'ultimate-post') } <a href={ultp_data.builder_url} target='_blank' > { __('Site Builder ', 'ultimate-post')}</a> { __('and explore more', 'ultimate-post') }</div>
                    }
                    <div className="ultp-module-page">
                        <img key={lgImg} src={`https://postxkit.wpxpo.com/${live}/wp-content/uploads/sites/${ currentStarter[0].ID}/postx_importer_img/pages/large/${lgImg.toLowerCase().replaceAll(' ', '_')}.jpg`} alt={lgImg}  />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default StarterPacks;