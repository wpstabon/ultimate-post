import React, { useState, useEffect } from "react";
import Toast from "../utility/Toast"
import './license.scss'
const { __ } = wp.i18n

const License = (props) => {
    const { expire } = props;
    const [licenseKey, setLicenseKey] = useState(ultp_dashboard_pannel.license);
    const [setting, setSetting] = useState(false);
    const [toastMessages, setToastMessages] = useState({
		state: false,
		status: ''
	});
    const [type, setType] = useState( props.status );
    const [saving, setSaving] = useState(false)

    
    useEffect(() => {
        setType( props.status )
    }, [props.status])


    const licenseAction = () => {
        setSaving(true);
        wp.apiFetch({
            path: '/ultp/v2/dashborad',
            method: 'POST',
            data: {type: 'license_action', edd_ultp_license_key: licenseKey}
        })
        .then((res) => {
            if (res.success) {
                setType('valid')
                setSetting(false)
            } else {
                setType('invalid')
            }
            setSaving(false);
            setToastMessages({
                status: res.success ? 'success' : 'error',
                messages: [res.message],
                state: true
            });
        })
    }


    const licenseContent = () => {
        if (type == 'valid' && !setting && ultp_dashboard_pannel.active) {
            return (
                <div className="ultp-dash-item-con licenseActivated">
                    <span className="dashicons dashicons-saved activatedIcon"></span>
                    <div className="activationMessage">
                        {__('Your License is', 'ultimate-post')} <strong>{__('Activated.', 'ultimate-post')}</strong>
                        { expire && 
                            <>
                                {__(' Expiry Date: ', 'ultimate-post')}<strong>{expire}</strong>
                            </>
                        }
                    </div>
                    <div className="deactiveMessage">
                        {__('Want to reset this license? ', 'ultimate-post')} 
                        <a onClick={(e) => {
                            setSetting(true)
                        }} className="cursor deactiveLink">{__('Click ', 'ultimate-post')}</a>
                    </div>
                </div>
            )
        } else if (type == 'invalid' && !setting) {
            return (
                <div className="ultp-dash-item-con licenseExpired">
                    <span className="dashicons dashicons-megaphone expiredIcon"></span>
                    <div className="expiredMessage">
                        {__('Your License is ', 'ultimate-post')}<strong>{__('Invalid / Expired.', 'ultimate-post')}</strong>{__(' To unlock pro features, please', 'ultimate-post')} <a target="_blank" className="renewLink" href="https://www.wpxpo.com/postx/pricing/?utm_source=postx-menu&utm_medium=license_page-renew&utm_campaign=postx-dashboard">{__('Renew your license', 'ultimate-post')}</a>
                    </div>
                    <div className="deactiveMessage">
                        {__('Want to add a new license?', 'ultimate-post')} <a onClick={()=>setSetting(true)} className="cursor deactiveLink">{__('Click here', 'ultimate-post')}</a>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="ultp-dash-item-con newLicense">
                    <label htmlFor="ultp_license_key" className="newLicenseLabel">{__('Add New License', 'ultimate-post')}</label>
                    <div className="newLicenseFields">
                        <input type="password" value={licenseKey} name="edd_ultp_license_key" placeholder="Enter your license key here" onChange={(e)=>{
                            setLicenseKey(e.target.value);
                        }}></input>
                        <button onClick={() => {
                            licenseAction();
                        }} className="ultp-primary-button cursor">{__('Activate License', 'ultimate-post')}{saving && <span className="dashicons dashicons-update rotate" />}</button>
                    </div>
                    <p className="ultp-description">{__('Enter your license key', 'ultimate-post')}</p>
                </div>
            )
        }
    }


    const licenseDoc = () => {
        if (type == 'valid' && !setting && ultp_dashboard_pannel.active) {
            return (
                <>
                    <a href="https://wpxpo.com/docs/postx/getting-started/development-site-license-deactivation/?utm_source=postx-menu&utm_medium=license_page-deactivate&utm_campaign=postx-dashboard" target="_blank" className="ultp-upgrade-pro-btn">{__('Deactivate Site License Docs', 'ultimate-post')}</a>
                    <a href="https://wpxpo.com/docs/postx/getting-started/upgrade-existing-plan/?utm_source=postx-menu&utm_medium=license_page-install-guide&utm_campaign=postx-dashboard" target="_blank" className="ultp-upgrade-pro-btn">{__('Upgrade License Docs', 'ultimate-post')}</a>
                </>
            )
        } else if (type == 'invalid' && !setting) {
            return (
                <>
                    <a href="https://wpxpo.com/docs/postx/getting-started/renew-existing-license/?utm_source=postx-menu&utm_medium=license_page-renew&utm_campaign=postx-dashboard" target="_blank" className="ultp-upgrade-pro-btn">{__('Renew License Docs', 'ultimate-post')}</a>
                    <a href="https://wpxpo.com/docs/postx/getting-started/upgrade-existing-plan/?utm_source=postx-menu&utm_medium=license_page-install-guide&utm_campaign=postx-dashboard" target="_blank" className="ultp-upgrade-pro-btn">{__('Upgrade License Docs', 'ultimate-post')}</a>
                </>
            )
        } else {
            return (
                <>
                    <a href="https://wpxpo.com/docs/postx/getting-started/pro-version-installation/?utm_source=postx-menu&utm_medium=license_page-install-guide&utm_campaign=postx-dashboard" target="_blank" className="ultp-upgrade-pro-btn">{__('Install PostX Pro Docs', 'ultimate-post')}</a>
                </>
            )
        }
    }


    return (
        <div className="licenseWrapper">
            { toastMessages.state && (
                <Toast
                    delay={2000}
                    toastMessages={toastMessages}
                    setToastMessages={setToastMessages}
                />
            )}
            <div className="ultpLicense">
                { licenseContent() }
                <div className="licenseGuide">
                    <div className="licenseGuideLeft">
                        <div className="ultp_h5">{__('How to use license key?', 'ultimate-post')}</div>
                        <div className="guidMessage">
                            <div className="errorNote">
                                <strong>{__('Note:', 'ultimate-post')}</strong> 
                                {__(' Make sure you have installed both the', 'ultimate-post')} 
                                <strong>{__(' free and pro version of PostX.', 'ultimate-post')}</strong>
                            </div>
                        </div>
                        <div className="ultp-description">{__('You have to add the license key and activate it to start using all the amazing features of PostX.', 'ultimate-post')}</div>
                    </div> 
                    <div className="licenseGuideRight ultp-dash-item-con">
                        <div className="ultp_h5">{__('Do you need any assistance?', 'ultimate-post')}</div>
                        <div className="ultp-description">{__('Check out the complete documentation of PostX to get your perfect solution.', 'ultimate-post')}</div>
                        { licenseDoc() }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default License;