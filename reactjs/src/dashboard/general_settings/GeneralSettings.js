import {
    useEffect,
    useState} from 'react';
const { __ } = wp.i18n
import {
    generalAttr,
    DashboardSidebar,
    getSettingsData
} from '../Settings';
import Skeleton from "../utility/Skeleton"
import Toast from "../utility/Toast"
import "./generalSettings.scss"
import IconPack from '../../helper/fields/tools/IconPack';


const GeneralSettings = () => {
    const [settings, setSettings] = useState({});
    const [toastMessages, setToastMessages] = useState({
        state: false,
		status: ''
	});
    const [loading, setLoading] = useState(false);
    
    useEffect(() => {
        getAllSettings();
    },[]);

    const getAllSettings = () => {
        wp.apiFetch({
            path: '/ultp/v2/get_all_settings',
            method: 'POST',
            data: {
                key: 'key',
                value: 'value'
            }
        })
        .then((res) => {
            if(res.success) {
                setSettings(res.settings);
            }
        })
    }

    const handleFormSubmit = (e) => {
        setLoading(true);
        e.preventDefault();
        const formData = new FormData(e.target);
        let formObject = {};

        for (let element of e.target.elements) {
            if(element.name) {
                if (element.type === 'checkbox' && !element.checked) {
                    formObject[element.name] = '';
                } else if(element.type === 'select-multiple') {
                    formObject[element.name] = formData.getAll(element.name);
                }  else if(element.dataset['customprop'] == 'custom_multiselect') {
                    formObject[element.name] = element.value ? element.value.split(',') : [];
                } else {
                    formObject[element.name] = element.value;
                }
            }
        }

        wp.apiFetch({
            path: '/ultp/v2/save_plugin_settings',
            method: 'POST',
            data: {
                settings: formObject,
                action: 'action',
                type: 'type'
            }
        })
        .then((res) => {
            if(res.success) {
                setToastMessages({
                    status: 'success',
                    messages: [res.message],
                    state: true
                });
            }
            setLoading(false);
        })
    }

    return (
        <div className="ultp-dashboard-general-settings-container">
            <div className="ultp-general-settings ultp-dash-item-con">
                <div>
                    { toastMessages.state && (
						<Toast
							delay={2000}
							toastMessages={toastMessages}
							setToastMessages={setToastMessages}
						/>
					)}
                    <div className="ultp_h5 heading">{__('General Settings', 'ultimate-post')}</div>
                    { Object.keys(settings).length > 0 ?  
                        <form onSubmit={handleFormSubmit} action=''>
                            {getSettingsData(generalAttr, settings)}
                            <div className="ultp-data-message"></div>
                            <div>
                                <button type='submit' className={`cursor ultp-primary-button ${loading ? 'onloading' : ''}`}>{__('Save Settings', 'ultimate-post')}{loading && IconPack['refresh']}</button>
                            </div>
                        </form>
                        :
                        <div className='skeletonOverflow'>
                            {   Array(6).fill(1).map((val, k) => {
                                return (
                                    <div key={k}>
                                        <Skeleton type="custom_size" classes="loop" c_s={{ size1: 150, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }} />
                                        <Skeleton type="custom_size" classes="loop" c_s={{ size1: '', unit1: '',  size2: 34, unit2: 'px', br: 2 }} />
                                        <Skeleton type="custom_size" classes="loop" c_s={{ size1: 350, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }} />
                                    </div>
                                )
                            })}
                            <Skeleton type="custom_size" c_s={{ size1: 120, unit1: 'px',  size2: 36, unit2: 'px', br: 2 }}/>
                        </div>
                    }
                </div>
            </div>
            <div className="ultp-general-settings-content-right">
                <DashboardSidebar FRBtnTag="settingsFR" proBtnTags={'postx_dashboard_settings'}/>
            </div>
        </div> 
    );
};

export default GeneralSettings;