import React from 'react';

const Modal = (props) => {
    const {title, modalContent, setModalContent} = props
    return (
        <div
            className="ultp-dashboard-modal-wrapper"
            onClick={(e) => {
                if (!e.target?.closest('.ultp-dashboard-modal')) {
                    setModalContent('');
                }
            }}
        >
            <div className="ultp-dashboard-modal">
                <div className="ultp-modal-header">
                    {title && <span className="ultp-modal-title">{title}</span>}
                    <a className="ultp-popup-close" onClick={() => setModalContent('')} />
                </div>
                <div className="ultp-modal-body">
                    {modalContent}
                </div>
            </div>
        </div>
    )
}

export default Modal;