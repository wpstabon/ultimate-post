const { __ } = wp.i18n
import React, { useState } from "react";
import Toast from "../utility/Toast"
import './support.scss'
import IconPack from "../../helper/fields/tools/IconPack";
import { getSecondaryButton } from "../Settings";

const Support = (props) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [subject, setSubject] = useState('');
    const [desc, setDesc] = useState('');
    const [loading, setLoading] = useState(false);
    const [fetchingData, setFetchingData] = useState(false);
    
    const [type, setType] = useState('');
    const [showForm, setShowForm] = useState(false);
    const [contactLink, setContactLink] = useState(false);
    const [toastMessages, setToastMessages] = useState({
		state: false,
		status: ''
	});

    const fieldData = [
        { value: 'Technical Support', pro: true },
        { value: 'Free Support (WordPress ORG)' },
        { value: 'Presale Questions' },
        { value: 'License Activation Issues' },
        { value: 'Bug Report' },
        { value: 'Compatibility Issues' },
        { value: 'Feature Request' },
    ];
    const supportLink = [
        { label: 'PostX Pro Installation', pro: true, link: 'https://wpxpo.com/docs/postx/getting-started/pro-version-installation/?utm_source=quick_support&utm_medium=pro_install&utm_campaign=postx-dashboard' },
        { label: 'Dynamic Site Builder', link: 'https://wpxpo.com/docs/postx/dynamic-site-builder/?utm_source=quick_support&utm_medium=site_builder&utm_campaign=postx-dashboard' },
        { label: 'How to Use PostX With Elementor Builder', link: 'https://wpxpo.com/docs/postx/add-on/elementor-addon/?utm_source=quick_support&utm_medium=elementor&utm_campaign=postx-dashboard' },
        { label: 'How to Use PostX With Divi Builder', link: 'https://wpxpo.com/docs/postx/add-on/divi-addon/?utm_source=quick_support&utm_medium=divi&utm_campaign=postx-dashboard' },
        { label: 'How to Use PostX With Bricks Builder', link: 'https://wpxpo.com/docs/postx/add-on/bricks-builder-addon/?utm_source=quick_support&utm_medium=bricks&utm_campaign=postx-dashboard' },
        { label: 'How to Use Table of Content', link: 'https://wpxpo.com/docs/postx/add-on/table-of-content/?utm_source=quick_support&utm_medium=table_of_content&utm_campaign=postx-dashboard' },
    ];

    const fetchData = ( action = 'support_data' ) => {
        if (action == 'support_action') {
            let error = (name && subject && desc) ? false : true;
            error = error ? error : /\S+@\S+\.\S+/.test(email) ? false : true;
            
            if (error || !type) {
                setToastMessages({
                    status: 'error',
                    messages: [ !type ? __('Please Select Support type.', 'ultimate-post') :  __('Please Fill all the Input Field.', 'ultimate-post')],
                    state: true
                });
                return;
            }
            setLoading(true);
        } else {
            setFetchingData(true);
        }

        const actionObj = (action == 'support_data') ? { type: action } : { type: action, name, email, subject: '[PostX- ' + type + '] ' + subject, desc }
        
        wp.apiFetch({
            path: '/ultp/v2/dashborad',
            method: 'POST',
            data: actionObj
        })
        .then((res) => {
            if (res.success) {
                if (action == 'support_data') {
                    setName(res.data.name);
                    setEmail(res.data.email);
                    setFetchingData(false);
                } else {
                    setSubject('');
                    setDesc('');
                }
            } else {
                setSubject('');
                setDesc('');
                setContactLink(true);
            }
            if (res.message) {
                setToastMessages({
                    status: res.success ? 'success' : 'error',
                    messages: [res.message],
                    state: true
                });
            }
            setLoading(false);
        })
    }

    return (
        <div className="ultp-dashboard-general-settings-container">
            <div className="ultp-general-settings ultp-dash-item-con">
                <div className="ultp-support">
                    { toastMessages.state && (
						<Toast
							delay={2000}
							toastMessages={toastMessages}
							setToastMessages={setToastMessages}
						/>
					)}
                    <div className="ultp-support-heading">{__('Having Difficulties? We are here to help', 'ultimate-post')}</div>
                    <div className="ultp-support-sub-heading">{__('Let us know how we can help you.', 'ultimate-post')}</div>
                    
                    <div className="ultp-support-options">
                        { fieldData.map((data, key) => {
                            return ( data.value != 'Free Support (WordPress ORG)' || ( data.value == 'Free Support (WordPress ORG)' && ! ultp_option_panel.active ) ) && 
                            <div className="ultp-support-option" key={key}>
                                <input type="radio" id={`${data.value.replace(/[()\s]/g, '')}`} className={`${data.value == 'Technical Support' ? ( !(data.pro) || ultp_option_panel.active ? 'pro_active' : '') : ''}`}  name="type" value={data.value} onClick={() => {
                                    let openState = false;
                                    if ( data.value == 'Technical Support' && !ultp_option_panel.active && data.pro ) {
                                        window.open('https://www.wpxpo.com/postx/pricing/?utm_source=quick_support&utm_medium=pro&utm_campaign=postx-dashboard', '_blank');
                                    } else if ( data.value == 'Free Support (WordPress ORG)' ) {
                                        window.open('https://wordpress.org/support/plugin/ultimate-post/', '_blank');
                                    } else { openState = true }

                                    setShowForm(openState);
                                    setContactLink(false);
                                    openState && !(name?.length > 0) && !(email?.length > 0) && fetchData();
                                    openState && setType(data.value);
                                }}/>
                                <label htmlFor={data.value.replace(/[()\s]/g, '')} >
                                    { data.value }
                                    { data.pro && ! ultp_option_panel.active && <a target="_blank" href="https://www.wpxpo.com/postx/pricing/?utm_source=quick_support&utm_medium=pro&utm_campaign=postx-dashboard">(Pro)</a> }
                                </label>
                            </div>
                        })}
                    </div>
                    {
                        !showForm && <div className="ultp-support-create">
                            <a href="#" className={'ultp-primary-button'} onClick={(e) => {
                                    e.preventDefault();
                                    setShowForm(true);
                                    fetchData();
                            }}>
                                { __('Create a Ticket', 'ultimate-post') }
                            </a>
                        </div>
                    }
                    {
                        showForm && <div className="ultp-support-form">
                            {
                                !type && <div className="ultp-support-type">{__('Select Support Type from above', 'ultimate-post') }</div>
                            }
                            <div id="postx-contact">
                                <div className="ultp-support-form-double">
                                    <input type="text" placeholder="Name" required value={name} onChange={(e) => setName(e.target.value)}/>
                                    <input type="email" placeholder="Email" required value={email} onChange={(e) => setEmail(e.target.value)}/>
                                </div>
                                <input type="text" placeholder="Subject" required value={subject} onChange={(e) => setSubject(e.target.value)}/>
                                <textarea placeholder="Description" required value={desc} onChange={(e) => setDesc(e.target.value)}></textarea>
                            </div>
                            {
                                contactLink ? <div>
                                    { __('You can Contact in Support via our ', 'ultimate-post') }
                                    <a className="" target="_blank" href="https://www.wpxpo.com/contact/?utm_source=quick_support&utm_medium=tech_support&utm_campaign=postx-dashboard">
                                        {__('Contact Form ', 'ultimate-post') }
                                    </a>
                                </div>
                                :
                                <a href="#" className={'ultp-primary-button'} onClick={(e) => {
                                    e.preventDefault();
                                    fetchData( 'support_action' );
                                }}>
                                    { __('Submit Ticket', 'ultimate-post') }
                                    { loading && <span className="dashicons loadericon dashicons-admin-generic"></span> }
                                </a>
                            }
                        </div>
                    }

                </div>
            </div>

            <div className="ultp-general-settings-content-right ultp-support-lists">
                <div className="ultp-dashboard-promotions-features">
                    <div className="ultp-dashboard-pro-features ultp-dash-item-con">
                        <h5>{__('Useful Guides', 'ultimate-post')}</h5>
                        <span className="ultp-description">{__('Explore in-depth documentations.', 'ultimate-post')}</span>
                        <div className="ultp-pro-feature-lists">
                            { supportLink.map((data, key) => {
                                return ( !data.pro || ( data.pro && !ultp_option_panel.active ) ) && 
                                <div key={key}>
                                    {IconPack['dot_solid']}
                                    <a target="_blank" href={data.link}>
                                        {data.label}
                                        {/* <span className="dashicons dashicons-external"></span> */}
                                    </a>
                                </div>
                            })}
                        </div>
                        { getSecondaryButton('https://wpxpo.com/docs/postx/?utm_source=quick_support&utm_medium=docs&utm_campaign=postx-dashboard', '' , 'Explore Docs', '')}
                    </div>
                    <div className="ultp-dashboard-web-community ultp-dash-item-con">
                        <h5>{__('PostX Community', 'ultimate-post')}</h5>
                        <span className="ultp-description">{__('Join the Facebook community of PostX to stay up-to-date and share your thoughts and feedback.', 'ultimate-post')}</span>
                        { getSecondaryButton('https://www.facebook.com/groups/1260674804266403','',  __('Join Community', 'ultimate-post'), '') }
                    </div>
                </div>
            </div>

        </div>        
    );
};

export default Support;