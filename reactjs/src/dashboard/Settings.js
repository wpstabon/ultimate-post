const { __ } = wp.i18n;
import UltpLinkGenerator from "../helper/UltpLinkGenerator";
import Tooltip from "../helper/common/Tooltip";
import IconPack from "../helper/fields/tools/IconPack";
import { useEffect, useState } from 'react';

const generalAttr = {
    preloader_style: {
        type: "select",
        label: __("Preloader Style", 'ultimate-post'),
        options: {
            style1: __("Preloader Style 1", 'ultimate-post'),
            style2: __("Preloader Style 2", 'ultimate-post'),
        },
        default: "style1",
        desc: __("Select Preloader Style.", 'ultimate-post'),
        tooltip: __("PostX has two preloader variations that display while loading PostX's blocks if you enable the preloader for that blocks.", 'ultimate-post'),
    },
    container_width: {
        type: "number",
        label: __("Container Width", 'ultimate-post'),
        default: "1140",
        desc: __("Change Container Width of the Page Template(PostX Template).", 'ultimate-post'),
        tooltip: __("Here you can increase or decrease the container width. It will be applicable when you create any dynamic template with the PostX Builder or select PostX's Template while creating a page.", 'ultimate-post'),
    },
    hide_import_btn: {
        type: "switch",
        label: __("Hide Template Kits Button", 'ultimate-post'),
        default: "",
        desc: __("Hide Template Kits Button from toolbar of the Gutenberg Editor.", 'ultimate-post'),
        tooltip: __("Click on the check box to hide the Template Kits button from posts, pages, and the Builder of PostX.", 'ultimate-post'),
    },
    disable_image_size: {
        type: "switch",
        label: __("Disable Image Size", 'ultimate-post'),
        default: "",
        desc: __("Disable Image Size of the Plugins.", 'ultimate-post'),
        tooltip: __("Click on the check box to turn off the PostX's size of the post images.", 'ultimate-post'),
    },
    disable_view_cookies: {
        type: "switch",
        label: __("Disable All Cookies", 'ultimate-post'),
        default: "",
        desc: __("Disable All Frontend Cookies (Cookies Used for Post View Count).", 'ultimate-post'),
        tooltip: __("Click on the check box to restrict PostX from collecting cookies. PostX contains cookies to display the post view count.", 'ultimate-post'),
    },
    disable_google_font: {
        type: "switchButton",
        label: __("Disable All Google Fonts", 'ultimate-post'),
        default: "",
        desc: __("Disable All Google Fonts From Frontend and Backend PostX Blocks.", 'ultimate-post'),
        tooltip: __("Click the check box to disable all Google Fonts from PostX's typography options.", 'ultimate-post'),
    }
}
const Multiselect = ({ multikey, value, multiValue}) => {
    const [multiFieldValues, setMultiFieldValues] = useState([...multiValue]);
    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState(value['options']??{});
    const [searchValue, setSearchValue] = useState('');

    const addItem = (val) => {
        if(multiFieldValues.indexOf(val) == -1 && val!='all') {
            setMultiFieldValues([...multiFieldValues, val]);
        }
        if(val==='all') {
            const allItems = Object.fromEntries( Object.entries(value['options']).filter(([key, value]) => !value.toLowerCase().includes('all')) );      
            setMultiFieldValues(Object.keys(allItems)); 
        }
    }
    const removeItem = (val) => {
        setMultiFieldValues(multiFieldValues.filter(item => item != val))
    }
    const handleClickOutside = (e) => {
        if (!e.target.closest('.ultp-ms-container') ) {
            setOpen(false);
        }
    }
    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () =>  document.removeEventListener('mousedown', handleClickOutside);
    },[]);

    useEffect(()=>{
        setTimeout(()=>{
            const searchResult = Object.fromEntries( Object.entries(value['options']).filter(([key, value]) => value.toLowerCase().includes(searchValue.toLowerCase())||key.toLowerCase().includes(searchValue.toLowerCase())) );       
            setOptions(searchResult); 
        },500);
    },[searchValue]);

    return (
        <div className="ultp-settings-field">
            <input type="hidden" name={multikey} value={multiFieldValues} data-customprop="custom_multiselect"/>
            <div className="ultp-ms-container">
                <div onClick={()=>setOpen(!open)} className="ultp-ms-results-con cursor">
                    <div className="ultp-ms-results">
                        {
                            multiFieldValues.length > 0 ? multiFieldValues?.map((key, index) => {
                                return <span key={index} className="ultp-ms-selected">
                                    {value['options'][key]}
                                    <span className="ultp-ms-remove cursor" onClick={(e)=> {e.stopPropagation();removeItem(key)}}>{IconPack['close_circle_line']}</span>
                                </span>
                            }) 
                            :
                            <span>{__('Select options')}</span>
                        }
                    </div>
                    <span onClick={()=>setOpen(!open)} className="ultp-ms-results-collapse cursor">{IconPack['collapse_bottom_line']}</span>
                </div>
                { 
                    open && options && <div className="ultp-ms-options">
                         <input type='text' className="ultp-multiselect-search" value={searchValue} onChange={(e)=>setSearchValue(e.target.value)} />
                        {
                            Object.keys(options)?.map((key, index) => {
                                return <span className="ultp-ms-option cursor" onClick={()=> addItem(key)} key={index} value={key} >{options[key]}</span>
                            })
                        }
                    </div> 
                }
            </div>
        </div>
    )
}

const getSettingsData = (data, settingsData) => {
    
    const getField = (key, value, settingsData) => {
        const val = settingsData.hasOwnProperty(key) ? settingsData[key] : ( value['default'] ? value['default'] : ( value['type'] == 'multiselect' ? [] : '' ) );

        switch(value['type']) {
            case 'select' :
                return (
                    <div className="ultp-settings-field">
                        <select defaultValue={val} name={key} id={key}>
                            { Object.keys(value['options']).map((key, index) => {
                                return <option key={index} value={key} >{value['options'][key]}</option>
                            })}
                        </select>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'radio' :
                return (
                    <div className="ultp-settings-field">
                        <div className="ultp-field-radio">
                                <div className="ultp-field-radio-items">
                                { Object.keys(value['options']).map((radioval, index) => {
                                    return <div key={index} className='ultp-field-radio-item'>
                                        <input type="radio" id={radioval} name={key} value={radioval} defaultChecked={radioval===val} />
                                        <label htmlFor={radioval}>{value['options'][radioval]}</label>
                                    </div>
                                })}
                                </div>
                            
                        </div>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'color':
                return (
                    <div className="ultp-settings-field">
                        <input type="text" defaultValue={val} className="ultp-color-picker" />
                        <span className="ultp-settings-input-field">
                            <input type="text" name={key} className="ultp-color-code" defaultValue={val}/>
                        </span>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'number':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <input type="number" name={key} defaultValue={val}/>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'switch': 
                return (
                    <div className="ultp-settings-field ultp-settings-field-inline">
                        <input value="yes" type="checkbox" name={key} defaultChecked={(val=='yes' || val=='on')}/>
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                    </div>
                )
            case 'switchButton':
                return (
                    <div className="ultp-settings-field ultp-settings-field-inline">
                        <input type="checkbox" value="yes" name={key} defaultChecked={(val=='yes' || val=='on')} />
                        { value['desc'] &&
                            <span className="ultp-description">{value['desc']}</span>
                        }
                        <div>
                            <span id="postx-regenerate-css" className={`ultp-upgrade-pro-btn cursor ${val=='yes' ? 'active':''} `}>
                                <span className="dashicons dashicons-admin-generic"></span>
                                <span className="ultp-text">
                                    {__('Re-Generate Font Files', 'ultimate-post')}
                                </span>
                            </span>
                        </div>
                    </div>
                )
            case 'multiselect':
                const multiValue = Array.isArray(val) ? val : [val];
                return   <Multiselect multikey={key} value={value} multiValue={multiValue} />
            case 'text':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <input type="text" name={key} defaultValue={val}/>
                        <span className="ultp-description">
                            {value['desc']}
                            { value['link'] && 
                                <a className="settingsLink" target="_blank" href={value['link']}>{value['linkText']}</a> 
                            }
                        </span>
                    </div>
                );
            case 'textarea':
                return (
                    <div className="ultp-settings-field ultp-settings-input-field">
                        <textarea name={key} defaultValue={val}></textarea>
                        <span className="ultp-description">
                            {value['desc']}
                            { value['link'] && 
                                <a className="settingsLink" target="_blank" href={value['link']}>{value['linkText']}</a> 
                            }
                        </span>
                    </div>
                );
            case 'shortcode':
                return (
                    <code className="ultp-shortcode-copy">[{value['value']}]</code>
                );
            default:
                break;
        }
        return 
    }


    return (
        <>
            { Object.keys(data).map((key, index) => {
                let value = data[key];
                return (
                    <span key={index}> 
                        { value['type'] == 'hidden' && 
                            <input key={key} type="hidden" name={key} defaultValue={value['value']}/>
                        }
                        { value['type'] != 'hidden' &&
                            <>
                                { value['type'] == 'heading' &&
                                    <div className="ultp_h2 ultp-settings-heading">{value['label']}</div> &&
                                    <>
                                        { value['desc'] &&
                                            <div className="ultp-settings-subheading">{value['desc']}</div>
                                        }
                                    </>
                                }
                                { value['type'] != 'heading' &&
                                    <div className="ultp-settings-wrap">
                                        { value['label'] && 
                                            <strong>
                                                {value['label']}
                                                {value['tooltip'] && <Tooltip content={value['tooltip']}>
                                                    <span className=" cursor dashicons dashicons-editor-help"></span>
                                                    </Tooltip>
                                                }
                                            </strong>
                                        }
                                        <div className="ultp-settings-field-wrap">
                                            { getField(key, value, settingsData) }
                                        </div>
                                    </div>
                                }
                            </>
                        }
                    </span>
                )
            })}
        </>
    )
}


const getUpgradeProBtn = (url, tags, label='') => {
    const title = label ? label : __('Upgrade to Pro', 'ultimate-post');
    return <a href={UltpLinkGenerator(url, tags, '')} className={"ultp-upgrade-pro-btn"} target="_blank" >{title}&nbsp; &#10148;</a>
}


const getProHtml = ({tags, func, data}) => {
    return (
        <div className="ultp-addon-lock-container-overlay">
            <div className="ultp-addon-lock-container">
                <div className="ultp-popup-unlock">
                    <img src={`${ultp_option_panel.url}/assets/img/dashboard/${data.icon}`} alt="lock icon"/>
                    <div className="title ultp_h5">{data?.title}</div>
                    <div className="ultp-description">
                        {data?.description}
                    </div>
                    { getUpgradeProBtn('', tags) }
                    <button onClick={()=> {func(false)}} className="ultp-popup-close">{IconPack['close_line']}</button>
                </div>
            </div>
        </div>
    )
}


const getPrimaryButton = (url, tags, label, classname = '') => {
    return <a href={UltpLinkGenerator(url, tags, '')} className={"ultp-primary-button " + classname} target="_blank" >{label}</a>
}


const getSecondaryButton = (url, tags, label, classname = '') => {
    return <a href={UltpLinkGenerator(url, tags, '')} className={"ultp-secondary-button " + classname} target="_blank" >{label}</a>
}


const DashboardSidebar = ({proBtnTags, FRBtnTag}) => {
    return (
        <div className="ultp-dashboard-promotions-features">
            { !ultp_option_panel.active && 
            <>
                {
                    ( ( new Date() >= new Date('2024-03-07') ) && ( new Date() <= new Date('2024-03-13') ) ) && <div className="ultp-dashboard-pro-features ultp-dash-item-con">
                        <a href="https://www.wpxpo.com/postx/pricing/?utm_source=postx-ad&utm_medium=sidebar-banner&utm_campaign=postx-dashboard" target="_blank" style={{textDecoration: 'none !important'}}>
                            <img src={ultp_option_panel.url+'assets/img/dashboard/db_sidebar.jpg'} style={{width: "100%", height: "100%"}} alt="40k+ Banner" />
                        </a>
                    </div>
                }
                <div className="ultp-dashboard-pro-features ultp-dash-item-con">
                    <div className="ultp_h5">{__('Pro Features', 'ultimate-post')}</div>
                    <span className="ultp-description">{__('Unlock the full potential of PostX to create and manage professional News Magazines and Blogging sites with complete creative freedom.', 'ultimate-post')}</span>
                    <div className="ultp-pro-feature-lists">
                        <span>{IconPack['right_circle_line']} {__('Full Access to Dynamic Site Builder', 'ultimate-post')} </span>
                        <span>{IconPack['right_circle_line']} {__('Access to 300+ Patterns', 'ultimate-post')} </span>
                        <span>{IconPack['right_circle_line']} {__('All Starter Packs Access', 'ultimate-post')} </span>
                        <span>{IconPack['right_circle_line']} {__('Custom Fonts with Typography Control', 'ultimate-post')} </span>
                        <span>{IconPack['right_circle_line']} {__('Advanced Query Builder', 'ultimate-post')} </span>
                        <span>{IconPack['right_circle_line']} {__('Ajax Filter and Pagination', 'ultimate-post')} </span>

                    </div>
                    {getUpgradeProBtn('', proBtnTags)}
                </div>
            </>
            }

            {
                <div className="ultp-dashboard-feature-request ultp-dash-item-con">
                    <div className="ultp_h5">{__('Feature Request', 'ultimate-post')}</div>
                    <span className="ultp-description">{__("Can’t find your desired feature? Let us know your requirements. We will definitely take them into our consideration.", 'ultimate-post')}</span>
                    {getSecondaryButton('https://www.wpxpo.com/postx/roadmap/?utm_source=postx-menu&utm_medium=DB-roadmap&utm_campaign=postx-dashboard', '' , 'Request a Feature', '')}
                </div>
            }
            {   
                <div className="ultp-dashboard-web-community ultp-dash-item-con">
                    <div className="ultp_h5">{__('PostX Community', 'ultimate-post')}</div>
                    <span className="ultp-description">{__('Join the Facebook community of PostX to stay up-to-date and share your thoughts and feedback.', 'ultimate-post')}</span>
                    {getSecondaryButton('https://www.facebook.com/groups/gutenbergpostx','', 'Join PostX Community', '')}
                </div>
            }
            {   
                <div className="ultp-dashboard-news-tips ultp-dash-item-con">
                    <div className="ultp_h5">{__('News, Tips & Update', 'ultimate-post')}</div>
                    <a target="_blank" href="https://wpxpo.com/docs/postx/getting-started/?utm_source=postx-menu&utm_medium=DB-news-postx_GT&utm_campaign=postx-dashboard">{__('Getting Started with PostX', 'ultimate-post')}</a>
                    <a target="_blank" href="https://wpxpo.com/docs/postx/dynamic-site-builder/?utm_source=postx-menu&utm_medium=DB-news-DSB_guide&utm_campaign=postx-dashboard">{__('How to use the Dynamic Site Builder', 'ultimate-post')}</a>
                    <a target="_blank" href="https://wpxpo.com/docs/postx/postx-features/?utm_source=postx-menu&utm_medium=DB-news-feature_guide&utm_campaign=postx-dashboard">{__('How to use the PostX Features', 'ultimate-post')}</a>
                    <a target="_blank" href="https://www.wpxpo.com/category/postx/?utm_source=postx-menu&utm_medium=DB-news-blog&utm_campaign=postx-dashboard								">{__('PostX Blog', 'ultimate-post')}</a>
                </div>
            }
            {
                <div className="ultp-dashboard-rating ultp-dash-item-con">
                    <div className="ultp_h5">{__('Show your love', 'ultimate-post')}</div>
                    <span className="ultp-description">{__('Enjoying PostX? Give us a 5 Star review to support our ongoing work.', 'ultimate-post')}</span>
                    {getSecondaryButton('https://wordpress.org/support/plugin/ultimate-post/reviews/','', 'Rate it Now', '')}
                </div>
            }
        </div>
    )
}

export {
    getSettingsData,
    getUpgradeProBtn,
    getProHtml,
    generalAttr,
    DashboardSidebar,
    getPrimaryButton,
    getSecondaryButton
}