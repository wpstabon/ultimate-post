import { React, useEffect, useState } from 'react';
import './SavedTemplates.scss';
import IconPack from '../../helper/fields/tools/IconPack';
import { getProHtml } from '../Settings';
import Skeleton from "../utility/Skeleton"
const { __ } = wp.i18n
import Toast from "../../dashboard/utility/Toast"

const SavedTemplates = (props) => {

    const [data, setData] = useState([])
    const [noData, setNoData] = useState(false)
    const [pages, setPages] = useState(1)
    const [found, setFound] = useState(0)
    const [newLink, setNewLink] = useState('')
    const [totalPage, setTotalPage] = useState(0)
    const [bulkSelect, setBulkSelect] = useState(false)
    const [bulkType, setBulkType] = useState('')
    const [checkedState, setCheckedState] = useState([]);
    const [actionId, setActionId] = useState('');
    const [actionPop, setActionPop] = useState(false);
    const [toastMessages, setToastMessages] = useState({
		state: false,
		status: ''
	});
    const [lockShow, setLockShow] = useState(false);
    
    useEffect(() => {
        _fetchData();
        document.addEventListener('mousedown', handleClickOutside);
        return () =>  document.removeEventListener('mousedown', handleClickOutside);
    }, [])

    const _fetchData = (arg = {}) => {
        _fetchQuery({
            action: 'dashborad', 
            data: Object.assign( {}, {
                type: 'saved_templates',
                pages: pages,
                pType: props.type
            }, arg )
        });
    }

    const _fetchQuery = (attr) => {
        wp.apiFetch({
            path: '/ultp/v2/' + attr.action,
            method: 'POST',
            data: attr.data
        })
        .then((res) => {
            if (res.success) {
                switch (attr.data.type) {
                    case 'saved_templates':
                        setCheckedState(Array(res.data.length).fill(false))
                        setData(res.data)
                        setNoData( res.data.length > 0 ? false : true )
                        setNewLink(res.new)
                        setFound(res.found)
                        setTotalPage(res.pages)
                        setBulkType('')
                        setBulkSelect(false)
                        if(attr.data.search) {
                            setPages(1);
                        }
                        break;
                    case 'status':
                    case 'delete':
                    case 'duplicate':
                    case 'action_draft':
                    case 'action_delete':
                    case 'action_publish':
                        _fetchData();
                        setBulkSelect(false)
                        setToastMessages({
                            status: attr.data.type.includes('delete') ? 'error' : 'success',
                            messages: [res.message],
                            state: true
                        });
                        break;
                    default:
                        break;
                }
            }
        })
    }

    const getHeader = () => {
        return(
            <>
                { props.type == 'ultp_templates' ?
                    <>
                        <th className="title">{__('Title', 'ultimate-post')}</th>
                        <th>{__('Shortcode', 'ultimate-post')}</th>
                    </>
                    :
                    <>
                        <th className="title">{__('Font Family', 'ultimate-post')}</th>
                        <th className='fontpreview'>{__('Preview', 'ultimate-post')}</th>
                        <th className='fontType'>{__('WOFF', 'ultimate-post')}</th>
                        <th className='fontType'>{__('WOFF2', 'ultimate-post')}</th>
                        <th className='fontType'>{__('TTF', 'ultimate-post')}</th>
                        <th className='fontType'>{__('SVG', 'ultimate-post')}</th>
                        <th className='fontType'>{__('EOT', 'ultimate-post')}</th>
                    </>
                }
                <th className='dateHead'>{__('Date', 'ultimate-post')}</th>
                <th>{__('Action', 'ultimate-post')}</th>
            </>
        )
    }

    const handleCopy = (e) => {
        let succeed = false;
        if(navigator.clipboard) {
            succeed = navigator.clipboard.writeText(e.target.innerHTML);
        } else {
            const hiddenInput = document.createElement("input");
            hiddenInput.setAttribute("value", e.target.innerHTML);
            document.body.appendChild(hiddenInput);
            hiddenInput.select();
            succeed = document.execCommand("copy");
            document.body.removeChild(hiddenInput);
        }
        if(succeed) {
            const spanElement = document.createElement('span');
            spanElement.innerText = 'Copied!';
            e.target.appendChild(spanElement);
            setTimeout(() => {
            e.target.removeChild(spanElement);
            }, 800);
        }   
    }
    const handleClickOutside = (e) => {
        if (!e.target.parentNode.classList.contains('ultp-reserve-button')) {
            setActionId('')
            setActionPop(false)
        }
    }

    const getContent = (val) => {

        let fonts = '';
        let styles = {
            fontFamily: '',
            fontWeight: ''
        };
        if(props.type != 'ultp_templates' && val?.font_settings?.length > 0) {
            const f = val.font_settings[0];
            const fontSrc = [];

            if(f.woff) {
                fontSrc.push(`url(${f.woff}) format('woff')`);
            }
            if(f.woff2) {
                fontSrc.push(`url(${f.woff2}) format('woff2')`);
            }
            if(f.ttf) {
                fontSrc.push(`url(${f.ttf}) format('TrueType')`);
            }
            if(f.svg) {
                fontSrc.push(`url(${f.svg}) format('svg')`);
            }
            if(f.eot) {
                fontSrc.push(`url(${f.eot}) format('eot')`);
            }
            fonts += ` @font-face {
                font-family: "${val.title}";
                font-weight: ${f.weight};
                font-display: auto;
                src: ${fontSrc.join(', ')};
            } `;
            styles = {
                fontFamily: val.title,
                fontWeight: f.weight
            };
        }

        return(
            <>
                { props.type == 'ultp_templates' ?
                    <>
                        <td className="title"><a href={val?.edit?.replace('&amp;','&')} target="_blank">{val.title  || 'Untitled'}</a></td>
                        <td ><span className="shortCode" onClick={(e) => {handleCopy(e)}}>[postx_template id="{val.id}"]</span></td>
                    </>
                    :
                    <>
                        <td className="title"><a href={val?.edit?.replace('&amp;','&')} target="_blank">{val.title || 'Untitled'}</a></td>
                        { val.title && <style type="text/css">{fonts}</style>}
                        <td style={styles}>{__('The quick brown fox jumps over the lazy dog.', 'ultimate-post')}</td>
                        <td className='fontType'><span className={`dashicons ${val.woff ? 'dashicons-yes' : 'dashicons-no-alt'}`}/></td>
                        <td className='fontType'><span className={`dashicons ${val.woff2 ? 'dashicons-yes' : 'dashicons-no-alt'}`}/></td>
                        <td className='fontType'><span className={`dashicons ${val.ttf ? 'dashicons-yes' : 'dashicons-no-alt'}`}/></td>
                        <td className='fontType'><span className={`dashicons ${val.svg ? 'dashicons-yes' : 'dashicons-no-alt'}`}/></td>
                        <td className='fontType'><span className={`dashicons ${val.eot ? 'dashicons-yes' : 'dashicons-no-alt'}`}/></td>
                    </>
                }
            </>
        )
    }

    return (
        <div className={`ultp-${props.type == 'ultp_templates' ? 'saved-template' : 'custom-font'}-container`}>
            { toastMessages.state && (
                <Toast
                    delay={2000}
                    toastMessages={toastMessages}
                    setToastMessages={setToastMessages}
                />
            )}
            { newLink ?
                <>
                    { ( props.type == 'ultp_templates' && !ultp_dashboard_pannel.active && data?.length > 0 ) ? 
                        <a className="ultp-primary-button cursor" onClick={()=> setLockShow(true)}>{__('Add New', 'ultimate-post')}</a>
                        : 
                        <>
                            <a className="ultp-primary-button " target="_blank" href={newLink}>{__('Add New', 'ultimate-post')}</a>
                        </>
                    }
                </>
                :
                <Skeleton type="custom_size" c_s={{ size1: 108, unit1: 'px',  size2: 46, unit2: 'px', br: 4 }}/>
            }
                <div className="tableCon">
                    <div className="ultp-bulk-con ultp-dash-item-con">
                        <div>
                            <select value={bulkType} onChange = { v => setBulkType(v.target.value) }>
                                <option value="">{__('Bulk Action', 'ultimate-post')}</option>
                                <option value="publish">{__('Publish', 'ultimate-post')}</option>
                                <option value="draft">{__('Draft', 'ultimate-post')}</option>
                                <option value="delete">{__('Delete', 'ultimate-post')}</option>
                            </select>
                            <a className="ultp-primary-button cursor" onClick={() => {
                                const items = checkedState.filter(v => Number.isInteger(v))
                                if(bulkType && items.length > 0) {
                                    if(bulkType == 'delete') {
                                        if(confirm('Are you sure you want to apply the action?')) {
                                            _fetchQuery({
                                                action: 'dashborad',
                                                data: {
                                                    type: 'action_' + bulkType,
                                                    ids: items
                                                }
                                            });
                                        }
                                    } else {
                                        _fetchQuery({
                                            action: 'dashborad',
                                            data: {
                                                type: 'action_' + bulkType,
                                                ids: items
                                            }
                                        });
                                    }
                                }
                            }}>{__('Apply', 'ultimate-post')}</a>
                        </div>
                        <input 
                            type="text" 
                            placeholder="Search..."
                            onChange={(e) => {
                                _fetchData({search: e.target.value})
                            }}
                        />
                    </div>

                    <div className="ultpTable">
                        <table className={`${data.length == 0 && !noData ? 'skeletonOverflow' : ''}`}>
                            <thead>
                                <tr> 
                                    <th>
                                        <input type="checkbox" checked={bulkSelect} onChange={(e) => {
                                            if (bulkSelect) {
                                                setCheckedState( Array(data.length).fill(false) )
                                            } else {
                                                setCheckedState( data.map(v => v.id) )
                                            }
                                            setBulkSelect(!bulkSelect)
                                        }}/>
                                    </th>
                                    {getHeader()}
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data?.map((val, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>
                                                    <input
                                                        type="checkbox"
                                                        checked={checkedState[key] ? true : false}
                                                        onChange={() => {
                                                            let final = [...checkedState]
                                                            final.splice(key, 1, checkedState[key] ? false : val.id);
                                                            setCheckedState(final)
                                                        }}
                                                    />
                                                </td>
                                                {getContent(val)}
                                                <td className='typeDate'>{val.status == 'publish' ? 'Published' : val.status } <br/>{val.date}</td>
                                                <td>
                                                    <span className="actions ultp-reserve-button" onClick={(e) =>{
                                                        setActionPop(!actionPop);
                                                        setActionId(val.id)
                                                    }}>
                                                        <span className="dashicons dashicons-ellipsis"></span>
                                                        { actionId == val.id  && actionPop && <ul className="ultp-dash-item-con actionPopUp ultp-reserve-button">
                                                                <li className="ultp-reserve-button"><a target="_blank" href={val?.edit?.replace('&amp;','&')}><span className="dashicons dashicons-edit-large"></span>{__('Edit', 'ultimate-post')}</a></li>
                                                                <li onClick={(e) => {
                                                                    setActionId('');
                                                                    setActionPop(false);
                                                                    e.preventDefault();
                                                                    if (confirm('Are you sure?')) {
                                                                        _fetchQuery({
                                                                            action: 'template_action', 
                                                                            data: {
                                                                                type: 'status',
                                                                                id: val.id,
                                                                                status: (val.status == 'publish' ? 'draft' : 'publish')
                                                                            }
                                                                        });
                                                                    }
                                                                }}><span className="dashicons dashicons-open-folder"></span>{__('Set to', 'ultimate-post')} {val.status == 'draft' ? 'Publish' : 'Draft'}</li>
                                                                <li onClick={(e) => {
                                                                    setActionId('');
                                                                    setActionPop(false);
                                                                    e.preventDefault();
                                                                    if (confirm('Are you sure you want to delete?')) {
                                                                        _fetchQuery({
                                                                            action: 'template_action', 
                                                                            data: {
                                                                                type: 'delete',
                                                                                id: val.id
                                                                            }
                                                                        });
                                                                    }
                                                                }}><span className="dashicons dashicons-trash"></span>{__('Delete', 'ultimate-post')}</li>
                                                                {
                                                                    props.type == 'ultp_templates' && <li onClick={(e) => {
                                                                        setActionId('');
                                                                        setActionPop(false);
                                                                        e.preventDefault();
                                                                        if (confirm('Are you sure you want to duplicate this template?')) {
                                                                            _fetchQuery({
                                                                                action: 'template_action', 
                                                                                data: {
                                                                                    type: 'duplicate',
                                                                                    id: val.id
                                                                                }
                                                                            });
                                                                        }
                                                                    }}><span className="dashicons dashicons-admin-page"/>{__('Duplicate', 'ultimate-post')}</li>
                                                                    
                                                                }
                                                            </ul>
                                                        }
                                                    </span>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                { (data.length == 0 && noData) &&
                                    <tr>
                                        { props.type == 'ultp_templates' ?
                                            <td colSpan={5}><div className="ultp_h2">{__('No Data Found !!!', 'ultimate-post')}</div></td>
                                        :
                                            <td colSpan={10}><div className="ultp_h2">{__('No Data Found !!!', 'ultimate-post')}</div></td>
                                        }
                                    </tr>
                                }
                                { data.length == 0 && !noData &&
                                    <>
                                    {Array(5).fill(1).map((val, k) => {
                                        return (
                                            <tr key={k}>
                                                <td><Skeleton type="custom_size" c_s={{ size1: 22, unit1: 'px',  size2: 20, unit2: 'px', br: 4 }}/></td>
                                                { props.type == 'ultp_templates' ?
                                                    <>{Array(4).fill(1).map((v, key) => <td key={key}><Skeleton type="title" size="99"/></td> )}</>
                                                    :
                                                    <>{Array(9).fill(1).map((v, key) => <td key={key}><Skeleton type="title" size="99"/></td> )}</>
                                                }
                                            </tr>
                                        )
                                    })}
                                    </>
                                }
                            </tbody>
                            
                        </table>

                    </div>
                    <div className="pageCon">
                        <div>{__('Page', 'ultimate-post')} {totalPage > 0 ? pages : totalPage} {__('of', 'ultimate-post')} {totalPage} [ {found} {__('items', 'ultimate-post')} ]</div>
                        {   totalPage > 0 &&  <div className="ultpPages">
                                {
                                    pages > 1 && <span onClick={() => {
                                        const count = pages - 1;
                                        _fetchData({pages: count});
                                        setPages(count);
                                    }}>{IconPack['leftAngle2']}</span>
                                }
                                <span className="currentPage">{pages}</span>
                                {
                                    totalPage > pages && <span onClick={() => {
                                        const count = pages + 1;
                                        _fetchData({pages: count});
                                        setPages(count);
                                    }}>{IconPack['rightAngle2']}</span>
                                }
                            </div>
                        }
                    </div>
                </div>
            { lockShow && getProHtml({tags: 'menu_save_temp_pro', func: (val)=> {setLockShow(val)}, data: { icon: 'saved_template_lock.svg', title: __('Create Unlimited Saved Templates with PostX Pro', 'ultimate-post'), description : __('You can create only one saved template with the free version of PostX. Please upgrade to a pro plan to create unlimited saved templates.', 'ultimate-post') }}) }
        </div>
    );
};

export default SavedTemplates;