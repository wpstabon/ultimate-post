import React from 'react';
const { __ } = wp.i18n
import { 
    DashboardSidebar, 
    getPrimaryButton, 
} from '../Settings';
import "./tutorial.scss"


const tutorialData = [
    { 
        video: 'https://www.youtube.com/embed/DuGzILsO9rw',
        title: __('How to use Advanced Query Builder', 'ultimate-post'),
        desc: __('Sort the posts and pages based on multiple criteria that include categories, tags, popular, recent, related, most commented, and much more. You can also display custom post types with ease.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/postx-features/advanced-query-builder/',
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/rSlj3mWgXz0',
        title: __('How to use AJAX Filtering', 'ultimate-post'),
        desc: __('The Ajax filtering feature helps to let visitors filter posts by specific or multiple categories and tags without reloading the whole page.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/postx-features/postx-ajax-filtering/',
        videoLink: ''
    }, 
    { 
        video: 'https://www.youtube.com/embed/nr2kOOJKhI0',
        title: __('How to use Advance Pagination', 'ultimate-post'),
        desc: __('PostX offers three types of paginations that include Navigation, Load More, and Numeric Paginations. While using any of the types you can also make it AJAX-powered pagination within a single click.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/postx-features/pagination/', 
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/q9KVVoE4MBg',
        title: __('How to use Ready Design Library', 'ultimate-post'),
        desc: __('PostX comes with multiple Starter Packs (Premade Templates) and numerous design variations for the post blocks. You can use them from the block library while editing a page.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/layout-and-design/', 
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/xxM0ypM8Oc0',
        title: __('How to use Post Blocks with Elementor', 'ultimate-post'),
        desc: __('PostX also lets you use its Post Blocks on any page while editing with Elementor Builder using the Saved Template and Elementor Addons.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/add-on/elementor-addon/', 
        videoLink: ''
    }, 
    { 
        video: 'https://www.youtube.com/embed/6ydwiIp2Jkg',
        title: __('How to use Saved Template via Shortcode', 'ultimate-post'),
        desc: __('With the help Saved Template addon, you can create your own designs with the available blocks and save them as templates to use anywhere via shortcode.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/add-on/shortcodes-support/', 
        videoLink: ''
    },  
    { 
        video: 'https://www.youtube.com/embed/wJzJRcSCFrI',
        title: __('How to use Meta', 'ultimate-post'),
        desc: __('The PostX Meta functionality allows users to add specific information about their news or blog posts.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/postx-features/post-meta/', 
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/xKu_E720MkE',
        title: __('How to use Table of Contents', 'ultimate-post'),
        desc: __('PostX comes with an extraordinary Table of Contents blocks with all essential features and customization options. You can add it to all blog post pages and custom post types.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/add-on/table-of-content/', 
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/7wxCNehEu_I',
        title: __('How To Use Dynamic Site Builder', 'ultimate-post'),
        desc: __('The Dynamic Site Builder addon of PostX allows you to create templates for essential pages including, Home, Blog Posts, Categories, and all other Archive pages.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/dynamic-site-builder/', 
        videoLink: ''
    },
    { 
        video: 'https://www.youtube.com/embed/cd75q-lJIwg',
        title: __('How to USe Dynamic Category Colors', 'ultimate-post'),
        desc: __('The PostX Category addon lets you add desired specific colors of the category names that display on the PostX blocks.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/add-on/category-addon/', 
        videoLink: ''
    },     
    { 
        video: 'https://www.youtube.com/embed/QErQoDhWi4c',
        title: __('How to use Progress Bar', 'ultimate-post'),
        desc: __('The Progress Bar addon helps to enable a fully functional and customizable reading bar to display a visual indication of the reading progression of blog posts.', 'ultimate-post'),
        docLink: 'https://wpxpo.com/docs/postx/add-on/progress-bar/', 
        videoLink: ''
    }
];


const Tutorial = () => {
    return (
        <div className="ultp-dashboard-tutorials-container">
            <div className="ultp-dashboard-tutorials-contents">
                { tutorialData.map((item, key) => {
                    return (
                        <div key={key} className="ultp-dashboard-tutorials-item ultp-dash-item-con">
                            <iframe width="560" height="350" src={item.video} title={item.title} frameBorder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            <div className="ultp-dashboard-tutorials-item-right">
                                <div className='ultp_h5' >{item.title}</div>
                                <div className="ultp-description">{item.desc}</div>
                                <div className="ultp-dashboard-tutorials-btns ultp-dash-two-btns">
                                    {/* {getSecondaryButton(item.videoLink, 'postx_dashboard_tutorials', __('Live Video Tutorials', 'ultimate-post'))} */}
                                    {getPrimaryButton(item.docLink, 'postx_dashboard_tutorialsdocs', __('Documentation', 'ultimate-post'))}
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className="ultp-tutorials-content-right">
                <DashboardSidebar FRBtnTag="tutorialsFR" proBtnTags={'postx_dashboard_tutorials'}/>
            </div>
        </div>
    );
};


export default Tutorial;