import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Addons from "./addons/Addons";
import GeneralSettings from './general_settings/GeneralSettings';
import "./dashboard.scss"
import SavedTemplates from './saved_templates/SavedTemplates';
import Blocks from './blocks/Blocks';
import Panel from './builder/Panel';
import License from './license/License';
import Support from './support/Support';
import IconPack from '../helper/fields/tools/IconPack';
import UltpLinkGenerator from '../helper/UltpLinkGenerator';
import DashboardStarterPacks from './starter_pack/DashboardStarterPacks';
const { __ } = wp.i18n

const Dashborad = () => {
    const [current, setCurrent] = useState('xx');
    const [helloBar, setHelloBar] = useState(ultp_dashboard_pannel.helloBar);
    const [generalDiscount, setGeneralDiscount] = useState(ultp_dashboard_pannel.generalDiscount);
    const status = ultp_dashboard_pannel.status;
    const expire = ultp_dashboard_pannel.expire;
    const [showFAQ, setShowFAQ] = useState(false);

    const dashMenu = [
        { 
            link: "#home",
            label: __("Getting Started", "ultimate-post"),
            showin: "both"
        },
        {
            link: "#builder",
            label: __("Site Builder", "ultimate-post"),
            showin: ultp_dashboard_pannel.settings.hasOwnProperty('ultp_builder') && ultp_dashboard_pannel.settings['ultp_builder'] != "false" ? "both" : "none",
            showhide: true
        },
        {
            link: "#startersites",
            label: __("Starter Sites", "ultimate-post"),
            showin: "both"
        },
        {
            link: "#blocks",
            label: __("Blocks", "ultimate-post"),
            showin: "both"
        },
        {
            link: "#custom-font",
            label: __("Custom Font", "ultimate-post"),
            showin: ultp_dashboard_pannel.settings.hasOwnProperty('ultp_custom_font') && ultp_dashboard_pannel.settings['ultp_custom_font'] != "false" ? "both" : "none",
            showhide: true
        },
        {
            link: "#saved-templates",
            label: __("Saved Templates", "ultimate-post"),
            showin: ultp_dashboard_pannel.settings.hasOwnProperty('ultp_templates') && ultp_dashboard_pannel.settings['ultp_templates'] != "false" ? "both" : "none",
            showhide: true
        },
        {
            link: "#integrations",
            label: __("Integrations", "ultimate-post"),
            showin: "both"
        },
        {
            link: "#settings",
            label: __("Settings", "ultimate-post"),
            showin: "both"
        },
    ]
    const dashHelpMenu = [
        {
            label: __('Get Support', 'ultimate-post'),
            icon: "dashicons-phone",
            link: "https://www.wpxpo.com/contact/?utm_source=postx-menu&utm_medium=all_que-support&utm_campaign=postx-dashboard"
        },
        {
            label: __('Welcome Guide', 'ultimate-post'),
            icon: "dashicons-megaphone",
            link: ultp_dashboard_pannel.setup_wizard_link
        },
        {
            label: __('Join Community', 'ultimate-post'),
            icon: "dashicons-facebook-alt",
            link: "https://www.facebook.com/groups/gutenbergpostx"
        },
        {
            label: __('Feature Request', 'ultimate-post'),
            icon: "dashicons-email-alt",
            link: "https://www.wpxpo.com/postx/roadmap/?utm_source=postx-menu&utm_medium=all_que-FR&utm_campaign=postx-dashboard"
        },
        {
            label: __('Youtube Tutorials', 'ultimate-post'),
            icon: "dashicons-youtube",
            link: "https://www.youtube.com/@wpxpo"
        },
        {
            label: __('Documentation', 'ultimate-post'),
            icon: "dashicons-book",
            link: "https://wpxpo.com/docs/postx/?utm_source=postx-menu&utm_medium=all_que-docs&utm_campaign=postx-dashboard"
        },
        {
            label: __('What’s New', 'ultimate-post'),
            icon: "dashicons-edit",
            link: "https://www.wpxpo.com/category/postx/?utm_source=postx-menu&utm_medium=all_que-roadmap&utm_campaign=postx-dashboard"
        }
    ]

    
    const _fetchQuery = () => {
        let pageUrl = window.location.href
        if (pageUrl.includes('page=ultp-settings#')) {
            pageUrl = pageUrl.split('page=ultp-settings#');
            if (pageUrl[1]) {
                setCurrent(pageUrl[1])
            }
        }
    }
    const helloBarAction = (type='helloBarAction') => {
        if ( type == 'helloBarAction' ) {
            setHelloBar('hide');
        } else {
            setGeneralDiscount('hide');
        }
        wp.apiFetch({
            path: '/ultp/v2/dashborad',
            method: 'POST',
            data: { type: type }
        })
    }

    const handleClickOutside = (e) => {
        if (e.target && !e.target.classList?.contains('ultp-reserve-button')) {
            if (e.target.href) {
                if (e.target.href.indexOf('page=ultp-settings#') > 0 ) {
                    const slug = e.target.href.split('#')
                    if (slug[1]) {
                        setCurrent(slug[1]);
                        window.scrollTo({ top: 0, behavior: 'smooth' });
                    }
                }
            }
        }
        if (!e.target.closest('.dash-faq-container') && !e.target.classList?.contains('ultp-reserve-button')) {
            setShowFAQ(false);
        }
        if ( e.target.classList?.contains('addons_promotions_close') ) {
            helloBarAction('generalDiscountAction');
        }
    }
    
    useEffect(() => {
        _fetchQuery();
        document.addEventListener('mousedown', handleClickOutside);
        return () =>  document.removeEventListener('mousedown', handleClickOutside);
    },[]);

    const renderDiscount = ()=>{
        const holidayTimeLimit =  ( new Date() >= new Date('2024-12-24') ) && ( new Date() <= new Date('2025-01-10') );

        const campName = 'holiday-offer';
        const HolidayGiftFree = <div className="ultp-setting-hellobar">
            <span className="dashicons dashicons-bell ultp-ring"></span>
            {__('Holiday Offer: ', 'ultimate-post')}
            {__(" Enjoy ", 'ultimate-post')}
            <strong> {__('up to 50% Discount', 'ultimate-post')} </strong>
            {__(" on PostX Pro - ", 'ultimate-post')}
            <a href={UltpLinkGenerator(`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-topbar&utm_medium=upgrade-${campName}&utm_campaign=postx-dashboard`, '', '')} target="_blank">{__('Grab Now', 'ultimate-post')} &nbsp; &#10148;</a>
        <span className="helobarClose" onClick={() => {helloBarAction()}}>{IconPack['close_line']}</span>
        </div>;
        
        // Holiday Gift Pro
        const HolidayGiftPro = <div className="ultp-setting-hellobar"><span className="dashicons dashicons-bell ultp-ring"></span><strong> {__('Enjoy Up to 45% Off', 'ultimate-post')} </strong> {__(' - Upgrade to a Lifetime Plan -', 'ultimate-post')} <a href={UltpLinkGenerator(`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-topbar&utm_medium=upgrade-${campName}&utm_campaign=postx-dashboard`, '', '')} target="_blank">{__('Skip Renewal Hassle!', 'ultimate-post')} &nbsp; &#10148;</a>
        <span className="helobarClose" onClick={() => {helloBarAction()}}>{IconPack['close_line']}</span>
        </div>;

        // Default Topbar
        if ( !holidayTimeLimit && !ultp_dashboard_pannel.active ) {
            return <>
            <div className="ultp-setting-hellobar">
            <span className="dashicons dashicons-bell ultp-ring"></span>
            <strong> {__('Starter Sites:', 'ultimate-post')} </strong> {__('Build Complete Site in 3 Simple Steps -', 'ultimate-post')} <a href={'https://www.wpxpo.com/postx-starter-sites/?utm_source=db-postx-topbar&utm_medium=explore-starter-sites&utm_campaign=postx-dashboard'} target="_blank">{__('Explore Now', 'ultimate-post')} &nbsp; &#10148;</a>
            <span className="helobarClose" onClick={() => {helloBarAction()}}>{IconPack['close_line']}</span></div>
            </>;
        }
        if ( 
            holidayTimeLimit && 
            ultp_dashboard_pannel.is_free 
        ) {
            return HolidayGiftFree;
        }
    }

    const discountDate = () => {
        const currentDate = new Date().setHours(0, 0, 0, 0)/1000; // converto to seconds
        const intervals = 345600;   // 4 days
        const dateBind = {
            start: new Date('2024-05-21').setHours(0, 0, 0, 0)/1000, // converto to seconds
            end: new Date('2024-07-22').setHours(0, 0, 0, 0)/1000 // converto to seconds
        }
        if ( currentDate < dateBind.start || currentDate > dateBind.end ) { return false }
        if ( ultp_dashboard_pannel.settings.activated_date && ( Number(ultp_dashboard_pannel.settings.activated_date ) + intervals ) >= currentDate ) {
            return false;
        }
        const dateStarted = Number(localStorage.getItem('ultpCouponDiscount'));
        if ( dateStarted ) {
            if (  dateStarted <= currentDate && currentDate <= dateStarted + intervals ) { // return true if current date is between started and started+intervals time
                return true;
            } else if ( currentDate >=  dateStarted + intervals*2 ) { // return true ans update storage value if current date is greater or equal than started+intervals+intervals time
                localStorage.setItem('ultpCouponDiscount', String(currentDate));
                return true;
            }
        } else {
            localStorage.setItem('ultpCouponDiscount', String(currentDate));  // return true ans update storage value if its fisrt time rendering
            return true;
        }
        return false
    }

   	return (
        <div className="ultp-settings-tab-wrap">
            {
                helloBar != 'hide' &&  
                renderDiscount()
            }
            <div className="ultp-setting-header">
                <div className="ultp-setting-logo">
                    <img className="ultp-setting-header-img" loading="lazy" src={ultp_dashboard_pannel.url + '/assets/img/logo-option.svg'} alt="PostX"/>
                    <span>{ultp_dashboard_pannel.version}</span>
                </div>
                <ul className="ultp-settings-tab" id="ultp-dashboard-ultp-settings-tab">
                    {dashMenu.map((menu, i) => {
                        return (menu.showin == 'both' || menu.showin == 'menu' || menu['showhide']) ? <li style={{display: menu['showin'] == 'none' ? 'none' : ''}} id={'ultp-dasnav-'+menu.link.replace('#', '')} key={i} className={menu.link == '#'+current ? 'current' : ''} onClick={() => setCurrent(menu.link.replace('#',''))}><a href={menu.link}>{menu.label}</a></li> : '';
                    })}
                </ul>
                <div className='ultp-dash-faq-con'>
                    <span onClick={()=> setShowFAQ(!showFAQ)} className="ultp-dash-faq-icon ultp-reserve-button dashicons dashicons-editor-help"></span>
                    {
                        showFAQ && <div className="dash-faq-container">
                            {dashHelpMenu.map((menu, i) => {
                                return <a key={i} href={menu.link} target="_blank"><span className={`dashicons ${menu.icon}`}/>{menu.label}</a>	
                            })}
                        </div>
                    }
                </div>
            </div>
            
            <div className={`ultp-settings-container ${current == 'startersites' ? 'ultp-settings-container-startersites' : ''}`}>
                <ul className="ultp-settings-content">
                    <li className="current">
                        { current != 'xx' && ( current == 'home' || ![ 'builder', 'startersites', 'integrations', 'saved-templates', 'custom-font', 'addons', 'blocks', 'settings', 'tutorials', 'license', 'support' ].includes(current) ) &&
                            <Addons generalDiscount={generalDiscount} integrations={false}/>
                        }
                        { current == 'saved-templates' &&
                            <SavedTemplates type="ultp_templates"/>
                        }
                        { current == 'custom-font' &&
                            <SavedTemplates type="ultp_custom_font"/>
                        }
                        { current == 'builder' &&
                            <Panel/>
                        }
                        { current == 'startersites' && 
                            <DashboardStarterPacks />
                        }
                        { current == 'integrations' &&
                            <Addons generalDiscount={generalDiscount} integrations={true}/>
                        }
                        { current == 'blocks' &&
                            <Blocks/>
                        }
                        { current == 'settings' &&
                            <GeneralSettings/>
                        }
                        { current == 'license' &&
                            <License status={status} expire={expire}/>
                        }
                        { current == 'support' &&
                            <Support/>
                        }
                    </li>
                </ul>
            </div>
            {!ultp_dashboard_pannel.active && discountDate() &&
                <a
                    className="ultp-discount-wrap"
                    href="https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-discount&utm_medium=coupon&utm_campaign=postx-dashboard&pux_link=postxdbcoupon"
                    target="_blank"
                >
                    <span className="ultp-discount-text">
                        {__('Get Discount', 'ultimate-post')}
                    </span>
                </a>
            }
        </div>
	);
}

if (document.body.contains(document.getElementById('ultp-dashboard'))) {
    ReactDOM.render(
        <React.StrictMode><Dashborad/></React.StrictMode>,
        document.getElementById('ultp-dashboard')
    );
}