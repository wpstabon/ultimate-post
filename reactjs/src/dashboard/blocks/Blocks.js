const { __ } = wp.i18n
import {useState, useEffect} from 'react';
import Toast from "../utility/Toast"
import "./blocks.scss";
import Skeleton from '../utility/Skeleton';

const blockSettings = {
    grid: {
        label: __("Post Grid Blocks", "ultimate-post"),
        attr: {
            post_grid_1: {
                label: __("Post Grid #1", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6829",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-1/",
                icon: "post-grid-1.svg"
            },
            post_grid_2: {
                label: __("Post Grid #2", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6830",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-2/",
                icon: "post-grid-2.svg"
            },
            post_grid_3: {
                label: __("Post Grid #3", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6831",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-3/",
                icon: "post-grid-3.svg"
            },
            post_grid_4: {
                label: __("Post Grid #4", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6832",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-4/",
                icon: "post-grid-4.svg"
            },
            post_grid_5: {
                label: __("Post Grid #5", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6833",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-5/",
                icon: "post-grid-5.svg"
            },
            post_grid_6: {
                label: __("Post Grid #6", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6834",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-6/",
                icon: "post-grid-6.svg"
            },
            post_grid_7: {
                label: __("Post Grid #7", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6835",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-grid-7/",
                icon: "post-grid-7.svg"
            }
        }
    },
    list: {
        label: __("Post List Blocks", "ultimate-post"),
        attr: {
            post_list_1: {
                label: __("Post List #1", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6836",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-list-1/",
                icon: "post-list-1.svg"
            },
            post_list_2: {
                label: __("Post List #2", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6837",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-list-2/",
                icon: "post-list-2.svg"
            },
            post_list_3: {
                label: __("Post List #3", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6838",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-list-3/",
                icon: "post-list-3.svg"
            },
            post_list_4: {
                label: __("Post List #4", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6839",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-list-4/",
                icon: "post-list-4.svg"
            }
        }
    },
    slider: {
        label: __("Post Slider Blocks", "ultimate-post"),
        attr: {
            post_slider_1: {
                label: __("Post Slider #1", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6840",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-slider-1/",
                icon: "post-slider-1.svg"
            },
            post_slider_2: {
                label: __("Post Slider #2", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid7487",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-slider-2/",
                icon: "post-slider-2.svg"
            }
        }
    },
    other: {
        label: __("Others PostX Blocks", "ultimate-post"), 
        attr: {
            menu: {
                label: __("Menu - PostX", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/introducing-postx-mega-menu/",
                docs: "https://wpxpo.com/docs/postx/postx-menu/",
                icon: "/menu/menu.svg"
            },
            post_module_1: {
                label: __("Post Module #1", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6825",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-module-1/",
                icon: "post-module-1.svg"
            },
            post_module_2: {
                label: __("Post Module #2", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6827",
                docs: "https://wpxpo.com/docs/postx/all-blocks/post-module-2/",
                icon: "post-module-2.svg"
            },
            heading: {
                label: __("Heading", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6842",
                docs: "https://wpxpo.com/docs/postx/all-blocks/heading-blocks/",
                icon: "heading.svg"
            },
            image: {
                label: __("Image", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6843",
                docs: "https://wpxpo.com/docs/postx/all-blocks/image-blocks/",
                icon: "image.svg"
            },
            taxonomy: {
                label: __("Taxonomy", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6841",
                docs: "https://wpxpo.com/docs/postx/all-blocks/taxonomy-1/",
                icon: "ultp-taxonomy.svg"
            },
            wrapper: {
                label: __("Wrapper", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6844",
                docs: "https://wpxpo.com/docs/postx/all-blocks/wrapper/",
                icon: "wrapper.svg"
            },
            news_ticker: {
                label: __("News Ticker", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid6845",
                docs: "https://wpxpo.com/docs/postx/all-blocks/news-ticker-block/",
                icon: "news-ticker.svg"
            },
            advanced_list: {
                label: __("List - PostX", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid7994",
                docs: "https://wpxpo.com/docs/postx/all-blocks/list-block/",
                icon: "advanced-list.svg"
            },
            button_group: {
                label: __("Button Group", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid7952",
                docs: "https://wpxpo.com/docs/postx/all-blocks/button-block/",
                icon: "button-group.svg"
            },
            row: {
                label: __("Row", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx-row-column-block/",
                docs: "https://wpxpo.com/docs/postx/postx-features/row-column/",
                icon: "row.svg"
            },
            advanced_search: {
                label: __("Search - PostX", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid8233",
                docs: "https://wpxpo.com/docs/postx/all-blocks/search-block",
                icon: "advanced-search.svg"
            },
            dark_light: {
                label: __("Dark Light", "ultimate-post"),
                default: true,
                live: "https://www.wpxpo.com/postx/blocks/#demoid8233",
                docs: "https://wpxpo.com/docs/postx/all-blocks/search-block",
                icon: "advanced-search.svg"
            },
        }
    },
    builder: {
        label: __("Site Builder Blocks", "ultimate-post"),
        attr: {
            builder_post_title: {
                label: __("Post Title", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/post_title.svg"
            },
            builder_advance_post_meta: {
                label: __("Advance Post Meta", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/post_meta.svg"
            },
            builder_archive_title: {
                label: __("Archive Title", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "archive-title.svg"
            },
            builder_author_box: {
                label: __("Post Author Box", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/author_box.svg"
            },
            builder_post_next_previous: {
                label: __("Post Next Previous", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/next_previous.svg"
            },
            builder_post_author_meta: {
                label: __("Post Author Meta", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/author.svg"
            },
            builder_post_breadcrumb: {
                label: __("Post Breadcrumb", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/breadcrumb.svg"
            },
            builder_post_category: {
                label: __("Post Category", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/category.svg"
            },
            builder_post_comment_count: {
                label: __("Post Comment Count", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/comment_count.svg"
            },
            builder_post_comments: {
                label: __("Post Comments", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/comments.svg"
            },
            builder_post_content: {
                label: __("Post Content", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/content.svg"
            },
            builder_post_date_meta: {
                label: __("Post Date Meta", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/post_date.svg"
            },
            builder_post_excerpt: {
                label: __("Post Excerpt", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/excerpt.svg"
            },
            builder_post_featured_image: {
                label: __("Post Featured Image/Video", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/featured_img.svg"
            },
            builder_post_reading_time: {
                label: __("Post Reading Time", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/reading_time.svg"
            },
            builder_post_social_share: {
                label: __("Post Social Share", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/share.svg"
            },
            builder_post_tag: {
                label: __("Post Tag", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/post_tag.svg"
            },
            builder_post_view_count: {
                label: __("Post View Count", "ultimate-post"),
                default: true,
                docs: "https://wpxpo.com/docs/postx/dynamic-site-builder/",
                icon: "builder/view_count.svg"
            }
        }
    }
};

const Blocks = () => {
    const [settings, setSettings] = useState({});
    const [toastMessages, setToastMessages] = useState({
		state: false,
		status: ''
	});
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getAllSettings();
    },[]);

    const getAllSettings = () => {
        setLoading(true);
        wp.apiFetch({
            path: '/ultp/v2/get_all_settings',
            method: 'POST',
            data: {
                key: 'key',
                value: 'value'
            }
        })
        .then((res) => {
            if(res.success) {
                setSettings(res.settings);
                setLoading(false);
            }
        })
    }

    const changeBlockStatus = (key) => {
        const value = (settings?.hasOwnProperty(key) && settings[key] != 'yes') ? 'yes' : '';
        setSettings({...settings, [key]: value});

        wp.apiFetch({
            path: '/ultp/v2/addon_block_action',
            method: 'POST',
            data: {
                key: key,
                value: value
            }
        })
        .then((res) => {
            if(res.success) {
                setToastMessages({
                    status: 'success',
                    messages: [res.message],
                    state: true
                });
            }
        })
    }
    
    return (
        <div className="ultp-dashboard-blocks-container">
            { toastMessages.state && (
                <Toast
                    delay={2000}
                    toastMessages={toastMessages}
                    setToastMessages={setToastMessages}
                />
            )}
            { Object.keys(blockSettings).map((key, index) => {
                const blockList = blockSettings[key];
                return (
                    <div className="ultp-dashboard-blocks-group" key={index}>
                        {
                            !loading ? <>
                                <div className="ultp_h5">{blockList.label}</div>
                                <div className="ultp-dashboard-group-blocks">
                                    { Object.keys(blockList.attr).map((key, i) => {   
                                        const block = blockList.attr[key];
                                        let checked = block.default ? true : false;
                                        if(settings[key] == '') {
                                            checked = settings[key] == 'yes' ? true : false;
                                        }
                                        return (
                                            <div className="ultp-dashboard-group-blocks-item ultp-dash-item-con" key={i}>
                                                <div className="ultp-blocks-item-meta">
                                                    <img src={`${ultp_dashboard_pannel.url}assets/img/blocks/${block.icon}`} alt={block.label} />
                                                    <div>{block.label}</div>
                                                </div>
                                                <div className="ultp-blocks-control-option ultp-dash-control-options">
                                                    { block.docs &&
                                                        <a href={block.docs+'?utm_source=db-postx-blocks&utm_medium=docs&utm_campaign=postx-dashboard'} className="ultp-option-tooltip" target="_blank">
                                                            <div className="dashicons dashicons-media-document"></div>
                                                            {__('Docs','ultimate-post')}
                                                        </a>
                                                    }
                                                    { block.live &&
                                                        <a href={block.live} className="ultp-option-tooltip" target="_blank">
                                                            <div className="dashicons dashicons-external"></div>
                                                            {__('Live','ultimate-post')}
                                                        </a>
                                                    }
                                                    <input type="checkbox" className="ultp-blocks-enable" id={key} defaultChecked={checked} onChange={() => {changeBlockStatus(key) }} />
                                                    <label className="ultp-control__label" htmlFor={key}></label>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </>
                            :
                            <>
                                <Skeleton type="custom_size" c_s={{ size1: 180, unit1: 'px',  size2: 32, unit2: 'px', br: 4 }}/>
                                <div className="ultp-dashboard-group-blocks">
                                    {Array(3).fill(1).map((val, i) => { 
                                        return (
                                            <div className="ultp-dashboard-group-blocks-item ultp-dash-item-con" key={i}>
                                                <div className="ultp-blocks-item-meta">
                                                    <Skeleton type="custom_size" c_s={{ size1: 22, unit1: 'px',  size2: 25, unit2: 'px', br: 4 }}/>
                                                    <Skeleton type="custom_size" c_s={{ size1: 100, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                </div>
                                                <div className="ultp-blocks-control-option ultp-dash-control-options">
                                                    <Skeleton type="custom_size" c_s={{ size1: 46, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                    <Skeleton type="custom_size" c_s={{ size1: 40, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                    <Skeleton type="custom_size" c_s={{ size1: 36, unit1: 'px',  size2: 20, unit2: 'px', br: 8 }}/>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </>
                        }
                    </div>
                )
            })}
        </div>
    );
};

export default Blocks;