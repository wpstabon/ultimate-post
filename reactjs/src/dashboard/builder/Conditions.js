import Fields from './Fields';
import { useState } from 'react';
import ExtendFields from './ExtendFields';
const { __ } = wp.i18n


const Conditions = (props) => {
	const {id, type, settings, defaults, setShowCondition} = props
	const [value, setValue] = useState((id && typeof settings[type] != 'undefined' && typeof settings[type][id] != 'undefined') ? settings[type][id] : ['include/'+type] )
	const [saveData, setSaveData] = useState({reload: false, dataSaved: false});

	return (
		<div className="ultp-modal-content">
			<div className="ultp-condition-wrap">
				<div className="ultp_h3">{__('Where Do You Want to Display Your Template?', 'ultimate-post')}</div>
                <p className="ultp-description">{__('Set the conditions that determine where your Template is used throughout your site.', 'ultimate-post')}</p>
				<div className="ultp-condition-items">
					{ value.map((val, k) => {
						if (val) {
							return (
								<div key={k} className="ultp-condition-wrap__field">
									{ (type == 'header' || type == 'footer') ?
										<ExtendFields 
											key={k} 
											id={id} 
											index={k}
											type={type} 
											value={val} 
											defaults={defaults}
											setChange={(v, _k) => {
												setSaveData({ dataSaved: false});
												let _v = JSON.parse(JSON.stringify(value));
												_v[_k] = v
												setValue(_v)
											}}
										/>
										:
										<Fields 
											key={k} 
											id={id} 
											index={k}
											type={type} 
											value={val} 
											defaults={defaults}
											setChange={(v, _k) => {
												setSaveData({ dataSaved: false});
												let _v = JSON.parse(JSON.stringify(value));
												_v[_k] = v
												setValue(_v)
											}}
										/>
									}
									<span 
										className="dashicons dashicons-no-alt ultp-condition_cancel"
										onClick={() => {
											setSaveData({ dataSaved: false});
											let _v = JSON.parse(JSON.stringify(value));
											_v.splice(k, 1)
											setValue(_v)
										}}>
									</span>
								</div>
							)
						}						
					})}
				</div>
				<button 
					className={`btnCondition cursor`}
					onClick={() => {
						const v = type=='singular' ? 'include/singular/post' : ((type=='header' || type=='footer') ? 'include/'+type+'/entire_site' : 'include/'+type)
						setValue([...value, v])
					}}>
					{__('Add Conditions', 'ultimate-post')}
				</button>
			</div>
			<button 
				className="ultp-save-condition cursor" 
				onClick={() => {
					setSaveData({reload: true});
					let _v = Object.assign({}, settings) 
					if (typeof _v[type] != 'undefined') {
						_v[type][id] = value.filter(n => n)
					} else {
						_v[type] = {}
						_v[type][id] = value.filter(n => n)
					}
					
					wp.apiFetch({
						path: '/ultp/v2/condition_save',
						method: 'POST',
						data: { settings: _v }
					})
					.then((response) => {
						if (response.success) {
							setSaveData({reload: false, dataSaved: true});
							setTimeout(function(){
								setSaveData({reload: false, dataSaved: false});
								setShowCondition && setShowCondition(''); 
							}, 2000);
						}
					})
				}}>
					{
						saveData.dataSaved  ? "Condition Saved." : "Save Condition"
					}
					<span style={{visibility: `${saveData.reload ? 'visible' : 'hidden'}`}} className="dashicons dashicons-update rotate ultp-builder-import" />
			</button>
		</div>
	);
}
export default Conditions;