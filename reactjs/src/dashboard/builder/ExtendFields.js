import { useState, useRef, useEffect } from 'react';

const ExtendFields = (props) => {
	const myRef = useRef();
    const {value, type, defaults, setChange, index} = props
    let valueSplit = value.split('/')
    const [list, setList] = useState([])
    const [isOpen, setOpen] = useState(false)
    const [isExtra, setExtra] = useState(false)
    const [search, setSearch] = useState( valueSplit[2] || '' )
    const [lable, setLable] = useState('')

    const handleClickOutside = e => {
        if (myRef.current != null) {
            if (!myRef.current.contains(e.target)) {
                setOpen(false)
            }
        }
    };

    useEffect(() => {        
        if (valueSplit[4]) {
            getTitle(valueSplit[4], true)
        } else {
            getType()
        }
        document.addEventListener('mousedown', handleClickOutside);
        return () => document.removeEventListener('mousedown', handleClickOutside);
    }, []);

    
    const getType = () => {
        let _search = '';
        const _val = valueSplit[3]
        if (_val && defaults[valueSplit[2]]) {
            defaults[valueSplit[2]].forEach(element => {
                if (element.value == _val) {
                    _search = element.search;
                    setExtra(true)
                } else if (element.attr) {
                    element.attr.forEach(el => {
                        if (el.value == _val) {
                            _search = el.search;
                            setExtra(true)
                        }
                    })   
                }
            });
        }
        return _search;
    }

    const getTitle = (val, title_return) => {
        wp.apiFetch({
            path: '/ultp/v2/condition',
            method: 'POST',
            data: { type: getType(), term: val, title_return }
        })
        .then((response) => {
            if (response.success) {
                if (title_return) {
                    setLable(response.data)
                } else {
                    setList(response.data)
                }
            }
        })
    }

	return (
		<div className="ultp-condition-fields">

			<select value={valueSplit[0]||'include'} onChange={(e) => {
                    valueSplit.splice(0, 1, e.target.value)
                    setChange(valueSplit.join('/'), index)
                }}>
				<option value="include">Include</option>
				<option value="exclude">Exclude</option>
			</select>

            <select value={valueSplit[2]||'entire_site'} onChange={(e) => {
                    valueSplit.splice(2, 1, (e.target.value || 'entire_site'))
                    valueSplit.splice(3)
                    if (e.target.value == 'singular') {
                        valueSplit.push('post')
                    }
                    setChange(valueSplit.join('/'), index)
                }}>
                    {defaults[type].map( (val, k) => {
                        return <option key={k} value={val.value}>{val.label}</option>
                    })}
			</select>

            { valueSplit[2] && valueSplit[2] != 'entire_site' &&
                <>
                <select value={valueSplit[3]||''} onChange={(e) => {
                        const _val = e.target.options[e.target.options.selectedIndex].dataset.search
                        setExtra(_val ? true : false)
                        setSearch('')
                        setList([])
                        valueSplit.splice(3,1,e.target.value)
                        valueSplit.splice(e.target.value ? 4 : 3)
                        const final = valueSplit.filter(function(el) { return el; });
                        setChange(final.join('/'), index)
                    }}>
                    {defaults[valueSplit[2]] &&
                        defaults[valueSplit[2]].map( (val, k) => {
                        return (
                            val.attr ? 
                            <optgroup label={val.label} key={k}>
                                {val.attr.map( (v, _k) => {
                                    return !v.attr && <option value={v.value} data-search={v.search} key={_k}>{v.label}</option>
                                })}
                            </optgroup>
                            :
                             <option value={val.value} data-search={val.search} key={k}>{val.label}</option>
                        )
                    })}
                </select>

                { (isExtra || valueSplit[4]) &&
                    <div ref={myRef} className="ultp-condition-dropdown">
                        <div onClick={()=>setOpen(true)} className={'ultp-condition-text ' + `${search && 'ultp-condition-dropdown__content'}`}>
                            { (search && lable) ?
                                    <>
                                    <span className="ultp-condition-dropdown__label">
                                        {lable}
                                        <span className="dashicons dashicons-no-alt ultp-dropdown-value__close" onClick={() => {
                                            setExtra(true)
                                            setSearch('')
                                            valueSplit.splice(4,1,'')
                                            const final = valueSplit.filter(function(el) { return el; });
                                            setChange(final.join('/'), index)
                                        }} />
                                    </span>
                                    </>
                                :
                                <span className="ultp-condition-dropdown__default"> All </span>
                            }
                            <span className="ultp-condition-arrow dashicons dashicons-arrow-down-alt2"/>
                        </div>
                        { isOpen &&
                            <div className="ultp-condition-search">
                                <input type="text" name="search"  autoComplete="off" placeholder="Search" onChange={(v) => {
                                    getTitle(v.target.value, false)
                                }} />
                                { list.length > 0 &&
                                    <ul>
                                        {list.map( (v, k) => {
                                            return <li 
                                                key={k} 
                                                onClick={() => {
                                                    setOpen(false);
                                                    setSearch(v.value); 
                                                    setLable(v.title);
                                                    valueSplit.splice(4,1,v.value)
                                                    setChange(valueSplit.join('/'), index)
                                                }}>
                                                {v.title}
                                            </li>
                                        })}
                                    </ul>
                                }
                            </div>
                        }
                    </div>
                }
                </>
            }
			

		</div>
	);
}
export default ExtendFields;