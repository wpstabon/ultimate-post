const { __ } = wp.i18n
import { useState, useEffect, Fragment } from 'react';
import Conditions from './Conditions';
import "./builder.scss"
import IconPack from '../../helper/fields/tools/IconPack';
import { getProHtml } from '../Settings';
import Toast from "../../dashboard/utility/Toast"
import Skeleton from "../utility/Skeleton"

const Panel = (props) => {
    const localizedData = props.has_ultp_condition ? ultp_condition : ultp_dashboard_pannel;
    const {notEditor} = props
	const defaultType = ['singular', 'archive', 'category', 'search', 'author', 'post_tag', 'date', 'header', 'footer', '404'] // Default menu items
    const [type, setType] = useState('') // Type
    const [postList, setPostList] = useState([]) // List of Save Posts
	const [menu, setMenu] = useState('all') // Menu active deactive check
    const [isNew, setIsNew] = useState(false) // New screen check
    const [settings, setSettings] = useState([]) // Current Save Settings
    const [showCondition, setShowCondition] = useState(notEditor||'') // Condition Popup
    const [defaults, setDefault] = useState([]) // Default Array
    const [fetch, setFetch] = useState(false) // Default Array
    const [premade, setPremade] = useState('') // Default String
    const [premadeList, setPremadeList] = useState([]) // Default String
    const [reload, setReload] = useState('') // Reload Animation Toggle
    const [lockShow, setLockShow] = useState(false);
    const [isError, setError] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [valAction, setAction] = useState('');
    const [actionPop, setActionPop] = useState(false);
    const postId = notEditor == 'yes' ? wp.data.select("core/editor").getCurrentPostId() : ''
    const [premadeFrontPage, setPremadeFrontPage] = useState([]) // Default list for front page template
    const [backOpt, setBackOpt] = useState(false) // Default list for front page template
    const [toastMessages, setToastMessages] = useState({
		state: false,
		status: ''
	});
    
    const setList = async () => {
        await wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'starter_lists'
            }
        })
        .then((response) => {
            if (response.success) {
                if (response.data) {
                    const data = JSON.parse(response.data);
                    handleBuilderPremade(data);
                }
            }
        })
    }

    const handleBuilderPremade = ( data ) => {
        const homePages = [];
        const builderPages = [];
        data.forEach((item) => {
            item.templates.forEach((val)=> {
                const temp = { ...val, parentID: item.ID };
                if( temp.hasOwnProperty('home_page') && temp.home_page == 'home_page') {
                    homePages.push(temp);
                }
                if( temp.type == 'ultp_builder' ) {
                    builderPages.push(temp)
                }
            })
        })
        setPremadeList(builderPages);
        setPremadeFrontPage(homePages);
    }

    const fetchData = async () => {
        await wp.apiFetch({
            path: '/ultp/v2/data_builder',
            method: 'POST',
            data: { pid: postId }
        })
        .then((response) => {
            if (response.success) {
                setPostList(response.postlist)
                setSettings(response.settings)
                setDefault(response.defaults)
                setType(response.type)
                setLoading(true)
                setList();
            }
        })
    };

	useEffect(() => {
        fetchData();
        document.addEventListener('mousedown', handleClickOutside);
        return () =>  document.removeEventListener('mousedown', handleClickOutside);
    }, [])

    const importAction = ( id, apiEndPoint ) => {
        wp.apiFetch({
            path: '/ultp/v2/get_single_premade',
            method: 'POST',
            data: { 
                type: premade,
                ID: id,
                apiEndPoint: apiEndPoint
            }
        })
        .then((response) => {
            if (response.success) {
                setReload('');
                window.open(response?.link?.replaceAll('&amp;','&'));
            } else {
                setLockShow(true);
                setReload('');
                setError(true);
            }
        })
    }

    const _fetchFile = () => {
        setFetch(true);
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'fetch_all_data'
            }
        })
        .then((response) => {
            if (response.success) {
                fetchData();
                setFetch(false);
                setToastMessages({
                    status: 'success',
                    messages: [response.message],
                    state: true
                });
            }
        })
    }
    const handleClickOutside = (e) => {
        if (e.target && !e.target.classList?.contains('ultp-reserve-button')) {
            setAction('')
            setActionPop(false)
        }
    }
    
    // support for Front Page Builder since v2.8.6
    const getPremadeScreenBody = (val, k) => {
        const imgSrc = `https://postxkit.wpxpo.com/${val.live}/wp-content/uploads/sites/${val.parentID}/postx_importer_img/pages/${val.name.toLowerCase().replaceAll(' ', '_')}.jpg`;
        const previewUrl = 'https://postxkit.wpxpo.com/' + (['header', 'footer', 'front_page'].includes(premade) ? val.live : val.live+'/postx_' + ( val.builder_type == 'archive' ? val.archive_type : val.builder_type ));
        return(
            <div key={k} className="ultp-item-list ultp-premade-item">
                <div className="listInfo">
                    <div className="title">
                        <span>{val.name}</span>
                        <div className='parent'>{val.parent}</div>
                    </div>
                    {(val.pro && !localizedData.active) ?
                        <a className="ultp-upgrade-pro-btn" target="_blank" href={`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-builder&utm_medium=${premade}-template&utm_campaign=postx-dashboard`}>{__('Upgrade to Pro','ultimate-post')}&nbsp; &#10148;</a>
                        :( (val.pro && isError) ?
                            <a className="ultp-btn-success" target="_blank" href={`https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-builder&utm_medium=${premade}-template&utm_campaign=postx-dashboard`}>{__('Get License','ultimate-post')}</a>
                            :
                            <span onClick={() => {
                                    setReload(val.ID);
                                    importAction(val.ID, 'https://postxkit.wpxpo.com/'+val.live);
                                }} className="btnImport cursor"> {IconPack['arrow_down_line']}
                                {__('Import','ultimate-post')}{(reload && reload == val.ID) ? <span className="dashicons dashicons-update rotate" /> : '' }
                            </span>
                        )
                    }

                </div>
                <div className="listOverlay bg-image-aspect" style={{backgroundImage: `url(${imgSrc})`}}>
                    <div className={"ultp-list-dark-overlay"}>
                        <a className="ultp-overlay-view ultp-dashboverlay" href={previewUrl} target="_blank">
                            <span className="dashicons dashicons-visibility"></span> {__('Live Preview','ultimate-post')}
                        </a>
                    </div>
                </div>
            </div>
        )
    }

    const getPremadeScreen = () => {
        return (
            <div className={`premadeScreen ${premade && " ultp-builder-items ultp"+premade}`}>
                <div className="ultp-list-blank-img ultp-item-list ultp-premade-item">
                    <div className="ultp-item-list-overlay ultp-p20 ultp-premade-img__blank">
                        <img src={localizedData.url + 'assets/img/dashboard/start-scratch.svg'}/>
                            <a className="cursor" onClick={(e) => {
                                e.preventDefault();
                                importAction();
                            }}>
                                <span className="ultp-list-white-overlay"><span className="dashicons dashicons-plus-alt"/> {__('Start from Scratch', 'ultimate-post')} </span>
                            </a>
                    </div>
                </div>
                {
                    premade == 'front_page' ? 
                        premadeFrontPage.map((val, k) => { return getPremadeScreenBody(val, k); }) : 
                        premadeList.map((val, k) => {
                            if ( 
                                ( val.builder_type != 'archive' && val.builder_type == premade ) ||
                                ( 
                                    val.builder_type == 'archive' && 
                                    ( val.archive_type == premade || 'archive' == premade ) 
                                ) 
                            ) {
                                return getPremadeScreenBody(val, k);
                            }
                        })
                }
            </div>
        )
    }

    const getNewScreen = () => {
        if(menu != 'all' && menu != 'archive') {
            setPremade(menu);
            setMenu(menu);
            if (premadeList.length <= 0 || ( menu == 'front_page' && premadeFrontPage.length <= 0)) {
                setList();
            }
            return;
        }

        return (
            <div className="ultp-builder-items">
                { (menu == 'all' ? ['front_page', ...defaultType ] : ( menu == 'archive' ? defaultType.filter(v => v != 'singular') : [menu])).map((val, k) => {
                    return (
                        <div
                        key={k}
                        onClick={() => {
                            setPremade(val);
                            setMenu(val);
                            if (premadeList.length <= 0 || ( val == 'front_page' && premadeFrontPage.length <= 0)) {
                                setList();
                            }
                        }}>
                            <div className="newScreen ultp-item-list ultp-premade-item">
                                <div className="listInfo">
                                    <div className="ultp_h6">
                                        {val}
                                    </div>
                                </div>
                                <div className="listOverlays">
                                    <img src={localizedData.url+`addons/builder/assets/icons/template/${val.toLowerCase()}.svg`} />
                                    <span className="ultp-list-white-overlay">
                                        <span className="dashicons dashicons-plus-alt"/>
                                            {__('Add', 'ultimate-post')} {val}
                                    </span>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }

    const getPostChild = (val , menu) => {
        let _v = [];
        if (val?.id && typeof settings[val?.type] != 'undefined') {
            settings[val.type][val.id]?.map((el, k) => {
                if (el) {
                    _v = el.split('/');
                }
            });
        }
        return _v;
    }
    
    const getData = (_type = 'all') => {
        let count = 0;
        return (
            <div className="ultp-template-list__tab">
                { (isNew == false && postList.length > 0) ?
                    postList.map((val, k) => {
                        const postChild = getPostChild(val , _type);
                        if ( 
                            _type == 'all' ||
                            _type == val.type ||
                            ( _type == postChild[2] && val.type == postChild[1] && !['header', 'footer'].includes(val.type) ) 
                        ) {
                            count++;
                            return (
                                <div key={k} className="ultp-template-list__wrapper">
                                    <div className="ultp-template-list__heading">
                                        <div className="ultp-template-list__meta">
                                            <div><span>{ val.type == 'front_page' ? 'Front Page' : val.type} :</span> {val.title} <span>ID :</span> #{val.id}</div>
                                            { val.id && typeof settings[val.type] != 'undefined' &&
                                                <div className="ultp-condition__previews">(
                                                    { (settings[val.type][val.id] || []).map((el, k) => {
                                                        if (el) {
                                                            const _v = el.split('/');
                                                            return <Fragment key={k}>{k==0?'':', '}<span>{(typeof _v[2] != 'undefined') ? _v[2] : _v[1]}</span></Fragment>;
                                                        }
                                                    })})
                                                </div>
                                            }
                                        </div>
                                        <div className="ultp-template-list__control ultp-template-list__content">
                                            { (val.type !='front_page' && val.type != '404') &&
                                                <button onClick={() => {
                                                    setType(val.type);
                                                    setShowCondition(val.id);
                                                }} className="ultp-condition__edit">{__('Conditions', 'ultimate-post')}</button>
                                            }
                                            <a className='status'> { val.status == 'publish' ? 'Published' : val.status }</a>
                                            <a href={val?.edit?.replaceAll('&amp;','&')} className="ultp-condition-action" target="_blank">
                                                <span className="dashicons dashicons-edit-large"/>
                                                {__('Edit', 'ultimate-post')}
                                            </a>
                                            <a className="ultp-condition-action ultp-single-popup__btn cursor" onClick={(e) => {
                                                    e.preventDefault()
                                                    if (confirm('Are you sure you want to duplicate this template?')) {
                                                        wp.apiFetch({
                                                            path: '/ultp/v2/template_action',
                                                            method: 'POST',
                                                            data: { id: val.id, type: 'duplicate', section: 'builder' }
                                                        })
                                                        .then((res) => {
                                                            if (res.success) {
                                                                fetchData();
                                                                setToastMessages({
                                                                    status: 'success',
                                                                    messages: [res.message],
                                                                    state: true
                                                                });
                                                            }
                                                        })
                                                    }
                                                }} >
                                                <span className="dashicons dashicons-admin-page"/>
                                                {__('Duplicate', 'ultimate-post')}
                                            </a>
                                            <a className="ultp-condition-action ultp-single-popup__btn cursor" onClick={(e) => {
                                                    e.preventDefault()
                                                    if (confirm('Are you sure you want to delete this template?')) {
                                                        wp.apiFetch({
                                                            path: '/ultp/v2/template_action',
                                                            method: 'POST',
                                                            data: { id: val.id, type: 'delete', section: 'builder' }
                                                        })
                                                        .then((res) => {
                                                            if (res.success) {
                                                                setPostList(postList.filter(v => v.id != val.id));
                                                                setToastMessages({
                                                                    status: 'error',
                                                                    messages: [res.message],
                                                                    state: true
                                                                });
                                                            }
                                                        })
                                                    }
                                                }} >
                                                <span className="dashicons dashicons-trash"/>
                                                {__('Delete', 'ultimate-post')}
                                            </a>
                                            <span onClick={(e) =>{
                                                setActionPop(!actionPop);
                                                setAction(val.id)
                                            }}>
                                                <span className="dashicons dashicons-ellipsis ultp-builder-dashboard__action ultp-reserve-button"></span>
                                            </span>
                                            { valAction == val.id  && actionPop &&
                                                <span className={`ultp-builder-action__active ultp-reserve-button`}  onClick={(e) => {
                                                    setAction('')
                                                    setActionPop(false)
                                                    e.preventDefault()
                                                    if (confirm(`Are you sure you want to ${val.status == 'publish' ? 'draft' : 'publish'} this template?`)) {
                                                        wp.apiFetch({
                                                            path: '/ultp/v2/template_action',
                                                            method: 'POST',
                                                            data: { id: val.id, type: 'status', status: (val.status == 'publish' ? 'draft' : 'publish') }
                                                        })
                                                        .then((res) => {
                                                            if (res.success) {
                                                                fetchData();
                                                                setToastMessages({
                                                                    status: 'success',
                                                                    messages: [res.message],
                                                                    state: true
                                                                });
                                                            }
                                                        })
                                                    }
                                                }}><span className="dashicons dashicons-open-folder ultp-reserve-button"/> {__('Set to', 'ultimate-post')} {val.status == 'publish' ? __('Draft', 'ultimate-post') : __('Publish', 'ultimate-post')}</span>
                                            }
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    })
                    :
                    getNewScreen()
                }
                {
                    (isNew == false && postList.length > 0) && !count && getNewScreen()
                }
            </div>
        )
    }

	return (
		<div className="ultp-builder-dashboard">
            { toastMessages.state && (
                <Toast
                    delay={2000}
                    toastMessages={toastMessages}
                    setToastMessages={setToastMessages}
                />
            )}
            { !notEditor &&
                <div className="ultp-builder-dashboard__content ultp-builder-tab">
                    <div className="ultp-builder-tab__option">
                        <span onClick={() => _fetchFile()} className="ultp-popup-sync"><i className={'dashicons dashicons-update-alt'+( fetch ? ' rotate' : '')}></i>{__('Synchronize', 'ultimate-post')}</span>
                        <ul>
                            <li {...(menu == 'all' && {className:'active'})} onClick={() => {setMenu('all');setIsNew(false);setPremade('')}}><span className="dashicons dashicons-admin-home"/> {__('All Template', 'ultimate-post')}</li>
                            <li {...(menu == 'front_page' && {className:'active'})} onClick={() => {setMenu('front_page');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/front_page.svg`} />
                                {__('Front Page', 'ultimate-post')}
                            </li>
                            <li {...(menu == 'singular' && {className:'active'})} onClick={() => {setMenu('singular');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/singular.svg`} />
                                {__('Singular', 'ultimate-post')}
                            </li>
                            <li {...(menu == 'search' && {className:'active'})} onClick={() => {setMenu('search');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/search.svg`} />
                                {__('Search Result', 'ultimate-post')}
                            </li>
                            <li {...(menu == 'archive' && {className:'active'})} onClick={() => {setMenu('archive');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/archive.svg`} />
                                {__('Archive', 'ultimate-post')}
                            </li>
                            <ul>
                                <li {...(menu == 'category' && {className:'active'})} onClick={() => {setMenu('category');setIsNew(false);setPremade('')}}>
                                    <img src={localizedData.url+`addons/builder/assets/icons/menu/category.svg`} />
                                    {__('Category', 'ultimate-post')}
                                </li>
                                <li {...(menu == 'author' && {className:'active'})} onClick={() => {setMenu('author');setIsNew(false);setPremade('')}}>
                                    <img src={localizedData.url+`addons/builder/assets/icons/menu/author.svg`} />
                                    {__('Authors', 'ultimate-post')}
                                </li>
                                <li {...(menu == 'post_tag' && {className:'active'})} onClick={() => {setMenu('post_tag');setIsNew(false);setPremade('')}}>
                                    <img src={localizedData.url+`addons/builder/assets/icons/menu/tag.svg`} />
                                    {__('Tags', 'ultimate-post')}
                                </li>
                                <li {...(menu == 'date' && {className:'active'})} onClick={() => {setMenu('date');setIsNew(false);setPremade('')}}>
                                    <img src={localizedData.url+`addons/builder/assets/icons/menu/date.svg`} /> 
                                    {__('Date', 'ultimate-post')}
                                </li>
                            </ul>
                            <li {...(menu == 'header' && {className:'active'})} onClick={() => {setMenu('header');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/header.svg`} />
                                {__('Header', 'ultimate-post')}
                            </li>
                            <li {...(menu == 'footer' && {className:'active'})} onClick={() => {setMenu('footer');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/footer.svg`} />
                                {__('Footer', 'ultimate-post')}
                            </li>
                            <li {...(menu == '404' && {className:'active'})} onClick={() => {setMenu('404');setIsNew(false);setPremade('')}}>
                                <img src={localizedData.url+`addons/builder/assets/icons/menu/404.svg`} />
                                404
                            </li>
                        </ul>
                    </div>
                    <div className="ultp-builder-tab__content ultp-builder-tab__template">    
                        <div className="ultp-builder-tab__heading">
                            <div className="ultp-builder-heading__title">
                                {(premade != '' || isNew) && backOpt && <span onClick={()=> { setBackOpt(false); setIsNew(false); setPremade('')}}> {IconPack['leftAngle2']}{__('Back', 'ultimate-post')}</span>}
                                <div className='ultp_h5 heading'>{__('All', 'ultimate-post')} { menu == 'all' ? '' : menu.replace('_', ' ')} {__('Templates', 'ultimate-post')}</div>
                            </div>
                            { postList.length > 0 && premade =='' && !isNew ?
                                <button className="cursor ultp-primary-button ultp-builder-create-btn" onClick={ () => {
                                    setIsNew(true);
                                    setBackOpt(true);
                                    setPremade((menu == 'all' || menu == 'archive') ? '' : menu);
                                }}> + {__('Create', 'ultimate-post')} {(menu == 'all') ? '' : menu.replace('_', ' ')} {__('Template', 'ultimate-post')}</button>
                                :
                                (!isLoading ? 
                                    <Skeleton type="custom_size"  c_s={{ size1: 170, unit1: 'px',  size2: 42, unit2: 'px', br: 4 }}/>
                                :
                                '')
                            }
                        </div>
                        <div className={`ultp-tab__content active`}>
                            { isLoading ?
                                premade == '' ?
                                getData(menu)
                                :
                                getPremadeScreen()
                                :
                                <div className={ `skeletonOverflow`} label={__('Loading...','ultimate-post')}>
                                    { Array(6).fill(1).map((v, k) => {
                                        return (
                                            <div key={k} className="ultp-template-list__tab" style={{marginBottom: '15px'}}>
                                                <div className="ultp-template-list__wrapper">
                                                    <div className="ultp-template-list__heading">
                                                        <Skeleton type="custom_size"  c_s={{ size1: 40, unit1: '%',  size2: 22, unit2: 'px', br: 2 }}/>
                                                        <div className="ultp-template-list__control ultp-template-list__content">
                                                            { (k==2 || k==4) && <Skeleton type="custom_size"  c_s={{ size1: 60, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>}
                                                            <Skeleton type="custom_size"  c_s={{ size1: 42, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                            <Skeleton type="custom_size"  c_s={{ size1: 42, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                            <Skeleton type="custom_size"  c_s={{ size1: 56, unit1: 'px',  size2: 20, unit2: 'px', br: 2 }}/>
                                                            <Skeleton type="custom_size"  c_s={{ size1: 25, unit1: 'px',  size2: 12, unit2: 'px', br: 2 }}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            }
                        </div>
                    </div>
                </div>
            }
            
            { showCondition &&
                <div className={`ultp-condition-wrapper ultp-condition--active`}>
                    <div className="ultp-condition-popup ultp-popup-wrap">
                        <button className="ultp-save-close" onClick={()=>setShowCondition('')}>{IconPack['close_line']}</button>
                        {
                            (Object.keys(defaults).length && type) ?
                                <Conditions 
                                    type={type} 
                                    id={showCondition=='yes'?postId:showCondition} 
                                    settings={settings} 
                                    defaults={defaults}
                                    setShowCondition={notEditor == 'yes' ? setShowCondition : ''}
                                />
                                :
                                <div className="ultp-modal-content">
                                    <div className="ultp-condition-wrap">
                                        <div className="ultp_h3 ultp-condition-wrap-heading"><Skeleton type="custom_size"  c_s={{ size1: 330, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/></div>
                                        <Skeleton type="custom_size"  c_s={{ size1: 460, unit1: 'px',  size2: 22, unit2: 'px', br: 2 }}/>
                                        <div className="ultp-condition-items">
                                            <div className="ultp-condition-wrap__field">
                                                <Skeleton type="custom_size"  c_s={{ size1: 100, unit1: '%',  size2: 30, unit2: 'px', br: 2 }}/>
                                            </div>
                                            <div className="ultp-condition-wrap__field">
                                                <Skeleton type="custom_size"  c_s={{ size1: 100, unit1: '%',  size2: 30, unit2: 'px', br: 2 }}/>
                                            </div>
                                            <div className="ultp-condition-wrap__field">
                                                <Skeleton type="custom_size"  c_s={{ size1: 100, unit1: '%',  size2: 30, unit2: 'px', br: 2 }}/>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        }
                        
                    </div>
                </div>
            }
            { lockShow && getProHtml({tags: 'builder_popup', func: (val)=> {setLockShow(val)}, data: { icon: 'template_lock.svg', title: __('Create Unlimited Templates With PostX Pro', 'ultimate-post'), description : __('We are sorry. Unfortunately, the free version of PostX lets you create only one template. Please upgrade to a pro version that unlocks the full capabilities of the dynamic site builder.', 'ultimate-post') }}) }

        </div>
	);
}
export default Panel;