const { __ } = wp.i18n

const SiteStatus = (props) => {
    const {siteArgs, setSiteArgs} = props;
    const featuresData = [
        { label: __('Fully Functional Builder', 'ultimate-post')},
        { label: __('Readymade Starter Sites ', 'ultimate-post') },
        { label: __('Advanced Post Filter', 'ultimate-post')  },
        { label: __('Advanced Query Builder', 'ultimate-post') },
        { label: __('Optimized Block Patterns', 'ultimate-post') },
        { label: __('Advanced Post Slider', 'ultimate-post') },
        { label: __('AI Integration', 'ultimate-post') },
        { label: __('Global Styles', 'ultimate-post') },
        
    ];
    return (
        <div>
            <div className="headerCon">
                <div className="ultp_h3">{__('What’s Your Website About?', 'ultimate-post')}</div>
                <span className="ultp-description ">
                    {__('Share some info on the type of site you want', 'ultimate-post')}
                </span>
            </div>
            <div className="siteStatusData">
                <div>
                    <label className="ultp_aster">{__('Website Type', 'ultimate-post')}</label>
                    <select onChange={(e)=> setSiteArgs({...siteArgs, siteType: e.target.value})}>
                        <option  value="" >{__('Select Website Type', 'ultimate-post')}</option>
                        <option  value="news" >{__('News', 'ultimate-post')}</option>
                        <option  value="magazine" >{__('Magazine', 'ultimate-post')}</option>
                        <option  value="blog" >{__('Blog', 'ultimate-post')}</option>
                        <option  value="travel_blog" >{__('Travel Blog', 'ultimate-post')}</option>
                        <option  value="portfolio" >{__('Portfolio', 'ultimate-post')}</option>
                        <option  value="agency" >{__('Agency', 'ultimate-post')}</option>
                        <option  value="others" >{__('Other', 'ultimate-post')}</option>
                    </select>
                </div>
            </div>
            <div className="ultp-feature-con">
                <div className="ultp-feature-group ultp-addons">
                    <div className="feature_label">{__('Features You Need for Success!', 'ultimate-post')}</div>
                    <div className="ultp-feature-section">
                        <ul>
                            {
                                featuresData.map((data, key) => {
                                    return <li key={key}>
                                        <span className="ultp-feature-name">{data.label}</span>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SiteStatus;