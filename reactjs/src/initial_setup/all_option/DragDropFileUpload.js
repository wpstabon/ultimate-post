import React, {useState } from "react";
import "./dropFileUpload.scss";
const { __ } = wp.i18n

const DragDropFileUpload = ({label,help,onChange,name}) => {

  const [dragActive, setDragActive] = useState(false);
  const [fileName, setFileName] = useState('');
  
  // handle drag events
  const handleDrag = function(e) {
    e.preventDefault();
    e.stopPropagation();
    if(e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true);
    } else if (e.type === "dragleave") {
      setDragActive(false);
    }
  };

  // triggers when file is dropped
  const handleDrop = function(e) {
      e.preventDefault();
      e.stopPropagation();
      setDragActive(false);
      if (e.dataTransfer.files && e.dataTransfer.files[0]) {
        setFileName(e.dataTransfer.files[0].name);
        onChange(e.dataTransfer.files[0]);
      }
    };

  const fileOnChange = (e)=>{
    setFileName(e.target.files[0].name);
    onChange(e.target.files[0]);
  }

  return(
      <div className="ultp_drag_drop_file_upload">
          <label className="ultp_drag_drop_file_upload__label">
              {label}
          </label>
          <input type="file" id='ultp_file_upload' name={name} onChange={fileOnChange}></input>
          <label htmlFor="ultp_file_upload" className={`ultp_drag_drop_file_upload__content_wrapper ${dragActive?'ultp_file_upload__drag_active':''}`} onDragEnter={handleDrag} onDragLeave={handleDrag} onDragOver={handleDrag} onDrop={handleDrop}>
              <div className="ultp_drag_drop_file_upload__content">
                  {fileName === '' && <>
                  <span className="dashicons dashicons-cloud-upload ultp_cloud_upload_icon ultp_icon"></span>
                  <span className="ultp_drag_drop_file_upload__text">
                      {__('Drop File Here or ','ultimate-post')}<span className="ultp_link_text"> {__(' Click to upload','ultimate-post')}</span>
                  </span>
                    </>}
                    {fileName && <span className="ultp_drag_drop_file_upload__file_name">{fileName}</span>}
              </div>
          </label>
          {help && <div className="ultp_help_message">{help}</div>}
      </div>
  );
}

export default DragDropFileUpload;