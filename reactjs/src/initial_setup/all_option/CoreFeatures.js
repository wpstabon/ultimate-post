import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
const { __ } = wp.i18n

const featuresData = [
    { label: __('Dynamic Site Builder', 'ultimate-post') , icon: 'site_builder.svg' },
    { label: __('Advanced Query Builder ', 'ultimate-post') , icon: 'query_builder.svg' },
    { label: __('Ajax Filter', 'ultimate-post') , icon: 'ajax_filter.svg'  },
    { label: __('Ajax Pagination', 'ultimate-post') , icon: 'ajax_pagination.svg' },
    { label: __('Advanced Post Slider', 'ultimate-post') , icon: 'post_slider.svg' },
    { label: __('ChatGPT Integration', 'ultimate-post') , icon: 'chat_gpt.svg' },
    { label: __('Elementor Integration', 'ultimate-post') , icon: 'elementor.svg' },
    { label: __('Custom Fonts', 'ultimate-post') , icon: 'custom_font.svg' },
    
];
    
const CoreFeatures = () => {
    return (
        <div className="coreFeatures">
            <div className="headerCon">
                <h4>{__('Core Features of PostX', 'ultimate-post')}</h4>
                <span className="ultp-description">{__('Take a look at the amazing offerings of  PostX that can make your site stand out from competitors', 'ultimate-post')}</span>
            </div>
            <div>
                <div className="ultp-feature-group ultp-addons">
                    <h5 className="">{__('Most Advanced and Unique Features', 'ultimate-post')}</h5>
                    <div className="ultp-feature-section">
                        <ul>
                            {
                                featuresData.map((data, key) => {
                                    return <li key={key}>
                                        <img src={ ultp_option_panel.url + 'assets/img/wizard/wizard_features/' + data.icon } alt={data.label}/>
                                        <span className="ultp-feature-name">{data.label}</span>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </div>
                <div className="ultp-feature-group ultp-features">
                    <h5 className="">{__('Ready Design Library', 'ultimate-post')}</h5>
                    <div className="ultp-feature-section">
                        <ul>
                            <li className="starterPacks">
                                <a href={UltpLinkGenerator('https://www.wpxpo.com/postx/starter-packs/', 'wizardStaterPackPro')} target="_blank">
                                    <span className="ultp-feature-name"><span>{__('32+', 'ultimate-post')}</span>{__('Starter Pack Templates', 'ultimate-post')}</span>
                                </a>
                            </li>
                            <li className="patterns">
                                <a href={UltpLinkGenerator('https://www.wpxpo.com/postx/patterns/', 'wizardPatternPro')} target="_blank">
                                    <span className="ultp-feature-name "><span>{__('250+', 'ultimate-post')}</span>{__('Premade Patterns', 'ultimate-post')}</span>
                                </a>
                            </li>
                            <li className="premadeTemplates">
                                <span className="ultp-feature-name "><span>{__('40+', 'ultimate-post')}</span>{__('Premade Templates', 'ultimate-post')}</span>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    );
};

export default CoreFeatures;