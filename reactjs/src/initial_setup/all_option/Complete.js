import {useState} from 'react';
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
const { __ } = wp.i18n

const helpInfo = [
    { 'icon': 'explore_pro.svg', 'label': __('Explore All Features', 'ultimate-post'), 'link': UltpLinkGenerator('https://www.wpxpo.com/postx/all-features/', 'explore_pro_feature') },
    { 'icon': 'knowledge_base.svg', 'label': __('PostX Documentation', 'ultimate-post'),'link': UltpLinkGenerator('https://wpxpo.com/docs/postx/', 'postx_doc') },
    { 'icon': 'forum_support.svg', 'label': __('Forum Support', 'ultimate-post'),'link':'https://wordpress.org/support/plugin/ultimate-post/' },
    { 'icon': 'ticket_support.svg', 'label': __('Pre-Sale & Pro Support', 'ultimate-post'),'link': UltpLinkGenerator('https://www.wpxpo.com/contact/', 'ticket_support') },
];
const socialMediaHandles = [
    { 'icon': '', 'label': __('Join the community', 'ultimate-post'), 'link': 'https://www.facebook.com/groups/gutenbergpostx' },
];

const Complete = ({siteArgs, setSiteArgs, sendPluginData}) => {
    const [playVideo, setPlayVideo] = useState(false);
    return (
        <div className='completePage'>
            <div className="headerCon">
                <img className="ready_image" src={ultp_option_panel.url + '/assets/img/wizard/ready_image.jpg'} alt={__('PostX', 'ultimate-post')} />
                <h3 className="completeTitle">{__('Congratulations…You are Ready!', 'ultimate-post')}</h3>
                <span className="ultp-description">{__('You are prepared to bring your dream site to life. Take advantage of the advanced features of PostX and start building…', 'ultimate-post')}</span>
                <div className="completeStep">
                    <a className="ultp-primary-button cursor readyButton" onClick={() => sendPluginData()}>
                        <span>{__('Ready to Go', 'ultimate-post')}</span>
                        <span className="dashicons dashicons-arrow-right-alt2"/>
                    </a>
                </div>
            </div>
            <div className='completeContent'>
                <div className='contentLeft'>
                    <div className="intro-video">
                        <img className="thumbnail" src={ultp_option_panel.url + '/assets/img/wizard/intro.png'} alt={__('PostX Intro Video', 'ultimate-post')} />
                        { <div className="play-icon-container" onClick={() => { setPlayVideo(true) }}>
                            <img className="play-icon" src={ultp_option_panel.url + '/assets/img/wizard/play.png'} alt={__('Play', 'ultimate-post')} />
                            <span className="animate"></span>
                        </div>}
                        {playVideo && <iframe className="video-frame"
                            src={`https://www.youtube.com/embed/FYgSe7kgb6M?autoplay=1`}
                            frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
                            title="Embedded youtube"
                            />}
                    </div>
                    <div className='inputFields'>
                        <div >
                            <input
                                id={'get_updates'}
                                name={'get_updates'}
                                type="checkbox"
                                defaultChecked={(siteArgs['get_updates'] && (siteArgs['get_updates'] == 'yes') ? true : false)}
                                onChange={(e) => {
                                    let _final;
                                    _final = e.target.checked ? 'yes' : 'no';
                                    setSiteArgs({ ...siteArgs, ['get_updates']: _final });
                                }}
                            />
                            <span>
                                <span>{__('Get updates', 'ultimate-post')}</span>
                                <span className='fieldDesc'>{__('We will send important tips & tricks for effective usage of PostX.', 'ultimate-post')}</span>
                            </span>
                        </div>
                        <div >
                            <input
                                id={'data_share_consent'}
                                name={'data_share_consent'}
                                type="checkbox"
                                defaultChecked={(siteArgs['data_share_consent'] && (siteArgs['data_share_consent'] == 'yes') ? true : false)}
                                onChange={(e) => {
                                    let _final;
                                    _final = e.target.checked ? 'yes' : 'no';
                                    setSiteArgs({ ...siteArgs, ['data_share_consent']: _final });
                                }}
                            />
                            <span>
                                <span>{__('Share Essentials', 'ultimate-post')}</span>
                                <span className='fieldDesc'>{__('Let us collect non-sensitive diagnosis data and usage information.', 'ultimate-post')}</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div className='contentRight'>
                    <div className="ultp_h5">{__('Learn More', 'ultimate-post')}</div>
                    <ul className="learn-more-options">
                        {helpInfo.map((option, index) => {
                            return <li key={index}>
                                <span className="learn-more-option" onClick={() => { window.open(option.link, '_blank') }}><img className="learn-more-option-icon" src={ultp_option_panel.url + 'assets/img/wizard/' + option.icon} alt={option.label} /> <span className="learn-more-option-label">{option.label}</span> </span>
                            </li>
                        })}
                    </ul>
                    {socialMediaHandles.length && socialMediaHandles.map((socialMedia, index) => {
                        return <button key={index} className="social-media-button" onClick={() => { window.open(socialMedia.link, '_blank') }}>
                            <span className="dashicons dashicons-facebook"></span>
                            <span className="social-media-button-label">
                                {socialMedia.label}
                            </span>
                        </button>
                    })}
                </div>
            </div>
        </div>
    );
};

export default Complete;