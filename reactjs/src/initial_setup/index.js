import React from 'react';
import ReactDOM from 'react-dom';
import Main from "./Main";
import '../dashboard/dashboard.scss'

if (document.body.contains(document.getElementById('ultp-wizard-page'))) {
    ReactDOM.render(
        <React.StrictMode><Main/></React.StrictMode>,
        document.getElementById('ultp-wizard-page')
    );
}