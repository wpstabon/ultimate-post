import { useState } from 'react';
const { __ } = wp.i18n
import './admin.scss';
import CoreFeatures from './all_option/CoreFeatures';
import Complete from './all_option/Complete';
import SiteStatus from './all_option/SiteStatus';

const tabItems = {
    welcome: {
        label: __('Welcome', 'ultimate-post'),
    },
    siteStatus: {
        label: __('Site Info', 'ultimate-post'),
    },
    ready: {
        label: __('Ready!', 'ultimate-post'),
    }
}

const tabItemKeys = Object.keys(tabItems);

const Main = () => {
    const url = new URL(window.location);
    const [loader, setLoader] = useState(false);
    // Current Tab
    const [currentTab, changeCurrentTab] = useState(
            url.searchParams.get('tab')
            ? url.searchParams.get('tab')
            : 'welcome'
    );
    const [siteArgs, setSiteArgs] = useState({siteType: '', data_share_consent: 'yes', get_updates: 'yes'});

    url.searchParams.set('tab', currentTab);
    history.pushState('', '', url);

    const sendPluginData = () => {
        if ( currentTab == 'ready' && 
            (siteArgs.data_share_consent == 'yes' || siteArgs.get_updates == 'yes')) {
                wp.apiFetch({
                    path: '/ultp/v2/send_initial_plugin_data',
                    method: 'POST',
                    data: {
                        wpnonce: ultp_option_panel.security,
                        site: siteArgs['siteType']
                    },
                })
                .then((res) => {
                    if(res.success) {
                        // console.log(res);
                    }
                })
        }
        completeSetup();
    }

    const saveSiteStatus = () => {
        const formData = new FormData();
        formData.append('wpnonce', ultp_option_panel.security);

        if (siteArgs['siteType']) {
            formData.append('siteType', siteArgs['siteType']);
        }
        

        wp.apiFetch({
            path: '/ultp/v2/wizard_site_status',
            method: 'POST',
            body: formData
        })
        .then((res) => {
            if(res.success) {
                // console.log(res);
            }
        })
    }

    const completeSetup = () => {
        setLoader(true);
        wp.apiFetch({
            path: '/ultp/v2/initial_setup_complete',
            method: 'POST',
            data: {
                wpnonce: ultp_option_panel.security,
            },
        })
        .then((res) => {
            if(res.success) {
                // console.log(res);
                window.location.href = res.redirect;
            }
        })
    }

    const Welcome = () => {
        return (
            <>
                <h1 className="ultp-title">{__('Welcome!', 'ultimate-post')}</h1>
                <span className="ultp-subtitle">
                    {__('Get Started with ', 'ultimate-post')}
                    <span> {__('PostX - the #1 Gutenberg Post Blocks', 'ultimate-post')} </span>
                    {__(' Plugin for Building News Sites, Magazine Portals, Personal Blogs, and more…', 'ultimate-post')}
                </span>
                <span className="ultp-short-desc">
                {__('It only takes 3-steps to get started', 'ultimate-post')}
                </span>
                <a className="ultp-primary-button cursor letsStartButton" onClick={() => changeCurrentTab(tabItemKeys[tabItemKeys.indexOf(currentTab) + 1])}>
                    <span>{__("Lets's Start", 'ultimate-post')}</span>
                    <span className="dashicons dashicons-arrow-right-alt2"/>
                </a>
            </>
        )
    }

    const Pagination = () => {
        return (
            <div className="ultp-pagination">
                { currentTab && currentTab == 'siteStatus' &&
                    <a className="ultp-secondary-button skipButton" 
                        onClick={() => { 
                            window.scrollTo({ top: 0, behavior: 'smooth' })
                            changeCurrentTab(tabItemKeys[tabItemKeys.indexOf(currentTab) + 1])
                        }}
                    >
                        <span>{__('Skip this Step', 'ultimate-post')}</span>
                    </a>
                }
                <div>
                    { currentTab && currentTab != 'welcome' && currentTab != 'ready' &&
                        <a className="ultp-secondary-button" 
                            onClick={() => {
                                window.scrollTo({ top: 0, behavior: 'smooth' });
                                changeCurrentTab(tabItemKeys[tabItemKeys.indexOf(currentTab) - 1])
                            }}
                        >
                            <span className="dashicons dashicons-arrow-left-alt2"/>
                            <span>{__('Previous', 'ultimate-post')}</span>
                        </a>
                    }
                    { currentTab && currentTab != 'ready' && currentTab !='welcome' &&
                        <a className="ultp-primary-button " 
                            onClick={() => {
                                if(currentTab == 'siteStatus') {
                                    saveSiteStatus();
                                }
                                window.scrollTo({ top: 0, behavior: 'smooth' });
                                changeCurrentTab(tabItemKeys[tabItemKeys.indexOf(currentTab) + 1]);
                            }}
                        >
                            <span>{__('Next', 'ultimate-post')}</span>
                            <span className="dashicons dashicons-arrow-right-alt2"/>
                        </a>
                    }
                </div>
            </div>
        )
    }

    return (
        <div>
            <div className="ultp-setting-header">
                <div className="ultp-setting-logo">
                    <img className="ultp-setting-header-img" loading="lazy" src={ultp_option_panel.url + '/assets/img/logo-option.svg'} alt="PostX"/>
                    <span>{ultp_option_panel.version}</span>
                </div>
            </div>
            <div className="ultp-setup-wizard-container">
                <div className={loader ? "ultp-processing" : ''}>
                    <div className="ultp-header">
                        <div className='progressbar'>
                            <ul className={`ultp-tab tab-${tabItemKeys.indexOf(currentTab) + 1}`}>
                                {
                                    tabItemKeys.map((key, index) => {
                                        index = index + 1;
                                        let item = tabItems[key];
                                        return (
                                            <li onClick={() => changeCurrentTab(key)} className={`cursor progress-step-wrapper ${(currentTab && (tabItemKeys.indexOf(currentTab) + 1) >= index ) ? "ultp-tab-pass" : ""} `} key={key}>
                                                <div className="progress-step-style"></div>
                                                <div className="progress-step-label-container">
                                                        <a className="progress-step-label" onClick={() => changeCurrentTab(key)}>
                                                            <span>{item.label}</span>
                                                        </a>
                                                </div>
                                            </li>
                                        )

                                    })
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="ultp-content ultp-settings-tab-wrap">
                        <ul className="ultp-content-items ">
                            <li
                                className={"ultp-welcome" +
                                (currentTab != 'welcome' ? " ultp-d-none" : '') }>
                                <Welcome />
                            </li>
                            <li
                                className={"ultp-site-status" +
                                (currentTab != 'siteStatus' ? " ultp-d-none" : '') }>
                                <SiteStatus siteArgs={siteArgs} setSiteArgs={setSiteArgs} />
                            </li>
                            <li
                                className={`ultp-coreFeatures ${currentTab != 'coreFeatures' ? " ultp-d-none" : ''} `}>
                                <CoreFeatures />
                            </li>
                            <li
                                className={"ultp-complete" +
                                (currentTab == 'ready' ? "" : ' ultp-d-none')}>
                                <Complete sendPluginData={sendPluginData} siteArgs={siteArgs} setSiteArgs={setSiteArgs} />
                            </li>
                            <Pagination />
                        </ul>
                    </div>
                    <div className="readySkipCon">
                        { currentTab && currentTab !== 'ready' &&
                            <a className="ultp-secondary-button cursor" onClick={() => completeSetup()}>{__('Return to Dashboard', 'ultimate-post')}</a>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Main;