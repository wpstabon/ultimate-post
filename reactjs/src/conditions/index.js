import React from 'react';
import ReactDOM from 'react-dom';
import Panel from '../dashboard/builder/Panel';

if (document.querySelector('.block-editor-page')) {
    const { subscribe } = wp.data
    const unsubscribe = subscribe(() => {
        let toolbar = document.querySelector('.editor-header__toolbar');
        if (!toolbar) {
            toolbar = document.querySelector('.edit-post-header__toolbar');
        }
        if (!toolbar) {
            toolbar = document.querySelector('.edit-post-header-toolbar');
        }
        if (!toolbar) {
            return;
        }

        let buttonDiv = document.createElement( 'div' );
        buttonDiv.className = 'toolbar-insert-layout';
        buttonDiv.innerHTML = '<button id="UltpConditionButton" class="ultp-popup-button" aria-label="Insert Layout"><span class="dashicons dashicons-admin-settings"></span>Condition</button>';
        if( !['404','front_page'].includes(ultp_data.archive) ) {
            toolbar.appendChild(buttonDiv);
        }

        // Builder Back Button
        setTimeout(function() {
            if (typeof document.getElementsByClassName('edit-post-fullscreen-mode-close')[0] != 'undefined') {
                document.getElementsByClassName('edit-post-fullscreen-mode-close')[0].href = ultp_condition.builder_url
            }
        }, 0);

        let isDisable = 1;
        function popupCondition() {
            if(isDisable){
                let node = document.createElement('div')
                node.id = "ultp-modal-conditions"
                node.className = "ultp-builder-modal ultp-blocks-layouts"
                document.body.appendChild(node)
                ReactDOM.render(<Panel has_ultp_condition={true} notEditor={'yes'}/>, node)
                isDisable = 0;
                setTimeout(function() {isDisable = 1}, 2000);
            }
        }

        if (typeof document.getElementsByClassName('editor-post-publish-button__button editor-post-publish-panel__toggle')[0] != 'undefined') {
            if( !['404','front_page'].includes(ultp_data.archive) ) {
                popupCondition();
            }
        }
        if( !['404','front_page'].includes(ultp_data.archive) ) {
            document.getElementById('UltpConditionButton')?.addEventListener('click', function () {
                popupCondition();
            })
        }
        unsubscribe()
    })
}