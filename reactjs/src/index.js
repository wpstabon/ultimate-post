const { __ } = wp.i18n
const { domReady } = wp
const { updateCategory, unregisterBlockType } = wp.blocks
const { subscribe, dispatch } = wp.data
const { render } = wp.element
const { registerPlugin } = wp.plugins;
// Just for site logo
const { addFilter } = wp.hooks;
const { InspectorControls } = wp.blockEditor;

import Library from './helper/Library'

import "./blocks/advanced_filter"
import "./blocks/advanced_filter/filter-clear"
import "./blocks/advanced_filter/filter-search"
import "./blocks/advanced_filter/filter-select"
import "./blocks/post_grid_pagination"

import "./blocks/post_list_1"
import "./blocks/post_list_2"
import "./blocks/post_list_3"
import "./blocks/post_list_4"

import "./blocks/post_slider_1"
import "./blocks/post_slider_2"

import "./blocks/post_grid_1"
import "./blocks/post_grid_2"
import "./blocks/post_grid_3"
import "./blocks/post_grid_4"
import "./blocks/post_grid_5"
import "./blocks/post_grid_6"
import "./blocks/post_grid_7"
import "./blocks/post_grid_parent"

import "./blocks/post_module_1"
import "./blocks/post_module_2"

import "./blocks/advance_list"
import "./blocks/advance_list/list"
import "./blocks/advanced_search"
import "./blocks/button_group"
import "./blocks/button_group/button"
import "./blocks/dark_light"
import "./blocks/heading"
import "./blocks/image"
import "./blocks/news_ticker"
import "./blocks/row"
import "./blocks/row/column"
import "./blocks/social_icons"
import "./blocks/social_icons/social"
import "./blocks/table_of_content"
import "./blocks/taxonomy"
import "./blocks/wrapper"

import "./blocks/menu"
import "./blocks/menu/menu_item"
import "./blocks/menu/list_menu"
import "./blocks/menu/mega_menu"
// import "./blocks/tabs"
// import "./blocks/tabs/tab_item"

// Builder Blocks
import "./builder/advance_post_meta"
import "./builder/archive_title"
import "./builder/author_box"
import "./builder/next_previous"
import "./builder/post_author_meta"
import "./builder/post_breadcrumb"
import "./builder/post_category"
import "./builder/post_comment_count"
import "./builder/post_comments"
import "./builder/post_content"
import "./builder/post_date_meta"
import "./builder/post_excerpt"
import "./builder/post_featured_image"
import "./builder/post_reading_time"
import "./builder/post_social_share"
import "./builder/post_tag"
import "./builder/post_title"
import "./builder/post_view_count"

import './helper/ChatGptToolbar'
import './helper/FrontendSubmissionToolbar'

import './helper/dynamic_content'

// Global Settings
import ChatGPT from './helper/ChatGPT'
import Media from './helper/fields/Media'
import Toggle from './helper/fields/Toggle'
import PostxSettings from './helper/global_settings/PostxSettings'
import { ParseCssGen, ParseWidgetGen } from './helper/ParseCss'

window.ultpDevice = 'lg'
window.bindCssPostX = false
let ticker = false;

// initialize postx blocks category
updateCategory('ultimate-post', { icon: <img className="ultp-insert-box-popup" style={{ height: '30px', marginTop: '-1px' }} src={ultp_data.url + 'assets/img/logo-sm.svg'} alt={'PostX'} /> });

// css save function from gutenberg post/page/custom editor
const savePostCss = () => {
    const unsubscribe = subscribe(() => {
        const { isSavingPost, isAutosavingPost, isPreviewingPost, isEditedPostSaveable } = wp.data.select("core/editor");
        const editor = wp.data.select("core/editor");
        if ( editor != null ) {
            if ( isEditedPostSaveable() && !isPreviewingPost() ) {
                ticker = false;
                window.bindCssPostX = false;
            }
            if ( !isAutosavingPost() ) {
                if ( isSavingPost() ) {
                    if ( window.bindCssPostX == false ) {
                        window.bindCssPostX = true;
                        ticker = true;
                        ParseCssGen({ handleBindCss: false, preview: false });
                    }
                }
            } else if ( isPreviewingPost() && window.bindCssPostX == false ) {
                window.bindCssPostX = true;
                ParseCssGen({ handleBindCss: true, preview: true});
            }
        }

        jQuery('[aria-controls="ultp-postx-settings:postx-settings"] span[arial-label="Go Back"]').css("display", "none");
    })
}

// FSE Editor is save
jQuery(document).on( 'click', '.editor-entities-saved-states__save-button', function () {
    if ( !ticker ) {
        ticker = true;
        ParseCssGen({ handleBindCss: true, preview: false });
        setTimeout(() => { ticker = false; }, 1000 );
    }
});

// save css inside widget area
const saveWidgetAction = () => {
    setTimeout(() => {
        jQuery('.edit-widgets-header__actions .components-button').click(() => {
            let data = [];
            if ( window.bindCssPostX === false ) {
                jQuery('.block-editor-block-list__block').each(function() {
                    if (jQuery(this).data('type').includes('ultimate-post/')) {
                        data.push({id:jQuery(this).data('block'), name:jQuery(this).data('type')})
                    }
                });
                if (data.length > 0) {
                    ParseWidgetGen(data);
                }
            }
        });
    }, 0 );
}

// append Template Kits/Builder Library Button in editor
const appendImportButton = () => {
    const unsubscribe = subscribe(() => {
        let toolbar;
        if ( wp.data.select("core/edit-site") ) {
            toolbar = document.querySelector('.editor-header__toolbar');
            if (!toolbar) {
                toolbar = window.document.querySelector('.edit-site-header-edit-mode__center');
            }
        } else {
            toolbar = document.querySelector('.editor-header__toolbar');
            if (!toolbar) {
                toolbar = document.querySelector('.edit-post-header__toolbar');
            }
            if (!toolbar) {
                toolbar = document.querySelector('.edit-post-header-toolbar');
            }
        }
        if (!toolbar) {
            return;
        }

        const chatGPTbutton = `<button id="UltpChatGPTButton" class="ultp-popup-button" aria-label="ChatGPT"><img class="ultp-popup-bar-image" src="${ultp_data.url+'assets/img/addons/ChatGPT.svg'}">${ __("ChatGPT","ultimate-post")}</button>`;

        let buttonDiv = document.createElement( 'div' );
        buttonDiv.className = 'toolbar-insert-layout';
        const html = `${ultp_data.settings['ultp_chatgpt'] == 'true'?chatGPTbutton:''}<button id="UltpInsertButton" class="ultp-popup-button" aria-label="Insert Layout">
        <img class="ultp-popup-bar-image" src="${ultp_data.url+'assets/img/logo-sm.svg'}">
                        ${["singular", "archive", "header", "footer", "404"].includes(ultp_data.archive) ? __("Builder Library","ultimate-post") : __("Template Kits","ultimate-post")}
                    </button>`;
        buttonDiv.innerHTML = html;
        toolbar.appendChild(buttonDiv);

        document.getElementById('UltpInsertButton')?.addEventListener('click', function () {
            let node = document.createElement('div')
            node.className = "ultp-builder-modal ultp-blocks-layouts"
            document.body.appendChild(node)
            render(<Library isShow={true} />, node)
            document.body.classList.add('ultp-popup-open')
        })
        document.getElementById('UltpChatGPTButton')?.addEventListener('click', function () {
            let node = document.createElement('div')
            node.className = "ultp-builder-modal ultp-blocks-layouts"
            document.body.appendChild(node)
            render(<ChatGPT isShow={true} fromEditPostToolbar={true}/>, node)
            document.body.classList.add('ultp-popup-open')
        })
        unsubscribe();
    })
}

domReady(function() {
    if (document.querySelector('.widgets-php')) {
        saveWidgetAction();
    } else if (document.querySelector('.block-editor-page')) {
        savePostCss();
        if (ultp_data.hide_import_btn != 'yes') {
            appendImportButton();
        }
    }

    // Unregister Block Type Depending on Settings
    const postxBlocks = {
        heading: 'heading',
        image: 'image',
        news_ticker: 'news-ticker',
        post_grid_1: 'post-grid-1',
        post_grid_2: 'post-grid-2',
        post_grid_3: 'post-grid-3',
        post_grid_4: 'post-grid-4',
        post_grid_5: 'post-grid-5',
        post_grid_6: 'post-grid-6',
        post_grid_7: 'post-grid-7',
        post_list_1: 'post-list-1',
        post_list_2: 'post-list-2',
        post_list_3: 'post-list-3',
        post_list_4: 'post-list-4',
        post_module_1: 'post-module-1',
        post_module_2: 'post-module-2',
        post_slider_1: 'post-slider-1',
        post_slider_2: 'post-slider-2',
        taxonomy: 'ultp-taxonomy',
        wrapper: 'wrapper',
        advanced_list: 'advanced-list',
        button_group: 'button-group',
        row: 'row',
        advanced_search: 'advanced-search',
        dark_light: 'dark-light',
        social_icons: 'social-icons',
        menu: 'menu',
    };
    Object.keys(postxBlocks).forEach( (key) => {
        if (ultp_data.settings?.hasOwnProperty(key) && ultp_data.settings[key] != 'yes') {
            unregisterBlockType( 'ultimate-post/' + postxBlocks[key] );
        }
    });

    const builderBlocks = {
        builder_advance_post_meta: 'advance-post-meta',
        builder_archive_title: 'archive-title',
        builder_author_box: 'author-box',
        builder_post_next_previous: 'next-previous',
        builder_post_author_meta: 'post-author-meta',
        builder_post_breadcrumb: 'post-breadcrumb',
        builder_post_category: 'post-category',
        builder_post_comment_count: 'post-comment-count',
        builder_post_comments: 'post-comments',
        builder_post_content: 'post-content',
        builder_post_date_meta: 'post-date-meta',
        builder_post_excerpt: 'post-excerpt',
        builder_post_featured_image: 'post-featured-image',
        builder_post_reading_time: 'post-reading-time',
        builder_post_social_share: 'post-social-share',
        builder_post_tag: 'post-tag',
        builder_post_title: 'post-title',
        builder_post_view_count: 'post-view-count'
    };
    if (ultp_data.post_type == 'ultp_builder') {
        Object.keys(builderBlocks).forEach( (key) => {
            if (ultp_data.settings?.hasOwnProperty(key) && ultp_data.settings[key] != 'yes') {
                unregisterBlockType( 'ultimate-post/'+builderBlocks[key] );
            }
        })
    } else {
        Object.keys(builderBlocks).forEach( (key) => {
            unregisterBlockType( 'ultimate-post/'+builderBlocks[key] );
        });
    }

    if (ultp_data.settings?.hasOwnProperty('ultp_table_of_content') && ultp_data.settings.ultp_table_of_content != 'true') {
        unregisterBlockType( 'ultimate-post/table-of-content' );
    }
})

// Dark mode site icon 
addFilter('blocks.registerBlockType', 'ultimate-post/ultp-site-logo-attributes', function( settings ){
    if(settings.name == 'core/site-logo') {
        const customAttribute = {
            ultpEnableDarkLogo: {
                type: 'boolean',
                default: false,
            },
            ultpdarkLogo: {
                type: 'object'
            },
        }
        settings.attributes = { ...settings.attributes, ...customAttribute }
    }
    return settings;
})

addFilter( 'editor.BlockEdit', 'core/site-logo', function( BlockEdit ) {
    const siteLogoClientIds = [];

    return function( props ) {
        
        const { attributes: { ultpEnableDarkLogo, ultpdarkLogo, className }, setAttributes } = props;

        if ( props.name == 'core/site-logo' ) {
            const hasIndex =  siteLogoClientIds.indexOf(props.clientId);
            if ( ultpEnableDarkLogo && hasIndex < 0 ) {
                siteLogoClientIds.push(props.clientId);
            } else if ( hasIndex > -1 ) {
                siteLogoClientIds.splice(hasIndex, 1);
            }
        }

        const handleLogoSubmission = ( value ) => {
            wp.apiFetch({
                path: '/ultp/v2/init_site_dark_logo',
                method: 'POST',
                data: {
                    wpnonce: ultp_data.security,
                    logo: value
                },
            })
            .then((res) => {
            });

            setAttributes({ ultpdarkLogo: value  });
            if ( siteLogoClientIds.length && dispatch ) {
                siteLogoClientIds.forEach( (clientId) => {
                    dispatch( 'core/block-editor' ).updateBlockAttributes(clientId, {ultpdarkLogo: value });
                });
            }
        }

        return (
            [
                <BlockEdit key={props?.clientId} {...props}  />,
                props.isSelected && props.name == 'core/site-logo' &&
                <InspectorControls>
                    <div className="ultp-dark-logo-control">
                        <Toggle 
                            label={__('Enable Dark Mode Logo(PostX)', 'ultimate-post')} 
                            value={ultpEnableDarkLogo} 
                            onChange={val => {
                                props.setAttributes({ 
                                    ultpEnableDarkLogo: val,
                                    className: val ? (className || '') + " ultp-dark-logo" 
                                    : className?.replaceAll("ultp-dark-logo", '')
                                })
                            }} />
                        {
                            ultpEnableDarkLogo && 
                            <Media
                                label={__('Upload Dark Mode Image', 'ultimate-post')} 
                                multiple={false} 
                                type={['image']} 
                                panel={true} 
                                value={ultpdarkLogo ?? { url: ultp_data.dark_logo }} 
                                onChange={(val) => {
                                    handleLogoSubmission(val);
                                }} />
                        }
                    </div>
                </InspectorControls>
            ]
        )
    }
}, 0)


registerPlugin( 'ultp-postx-settings', { render() {return <PostxSettings />} } );