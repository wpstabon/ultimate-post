import { getPostTypes } from '../helper/gridFunctions';

const { createReduxStore, register, controls: wpControls } = wp.data;

const DEFAULT_STATE = {
	customFields: {
		options: {
			acfFields: [],
			acfMsg: '',
			mbFields: [],
			mbfMsg: '',
			podsFields: [],
			podsMsg: '',
			cMetaFields: [],
			cMetaMsg: '',
		},
		hasResolved: false,
		error: '',
	},
	postTypes: [],
	cfValue: {},
	siteInfo: {},
	postInfo: {},
	authorInfo: {},
	link: {},
};

export const ULTP_STORE = 'ultimate-post/store';

const actions = {
	setCustomFields(customFields) {
		return {
			type: 'SET_CF',
			customFields,
		};
	},
	setCFValue(post_id, key, value) {
		return {
			type: 'SET_CF_VALUE',
			post_id,
			key,
			value,
		};
	},
	setSiteInfo(key, value) {
		return {
			type: 'SET_SITE_INFO',
			key,
			value,
		};
	},
	setPostInfo(post_id, key, value) {
		return {
			type: 'SET_POST_INFO',
			post_id,
			key,
			value,
		};
	},
	setAuthorInfo(post_id, key, value) {
		return {
			type: 'SET_AUTHOR_INFO',
			post_id,
			key,
			value,
		};
	},
	setPostTypes(postTypes) {
		return {
			type: 'SET_POST_TYPES',
			postTypes,
		};
	},
	setLink(post_id, key, value) {
		return {
			type: 'SET_LINK',
			post_id,
			key,
			value,
		};
	},
	fetchFromAPI(options) {
		return {
			type: 'FETCH_FROM_API',
			options,
		};
	},
};

const selectors = {
	getCustomFields(state) {
		return state.customFields;
	},
	getCFValue(state, post_id, key) {
		return state.cfValue?.[post_id]?.[key] ?? '';
	},
	getSiteInfo(state, key) {
		return state.siteInfo?.[key] ?? '';
	},
	getPostInfo(state, post_id, key) {
		return state.postInfo?.[post_id]?.[key] ?? '';
	},
	getAuthorInfo(state, post_id, key) {
		return state.authorInfo?.[post_id]?.[key] ?? '';
	},
	getPostTypes(state) {
		return state.postTypes;
	},
	getLink(state, post_id, key) {
		return state.link?.[post_id]?.[key] ?? '';
	},
};

const resolvers = {
	*getCustomFields(options = {}) {
		const path = 'ultp/v2/get_custom_fields';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: options,
			});
			return actions.setCustomFields({
				options: {
					acfFields: res.data.acf.fields || [],
					acfMsg: res.data.acf.message || '',
					cMetaFields: res.data.custom_metas.fields || [],
					cMetaMsg: res.data.custom_metas.message || '',
					mbFields: res.data.mb.fields || [],
					mbMsg: res.data.mb.message || '',
					podsFields: res.data.pods.fields || [],
					podsMsg: res.data.pods.message || '',
				},
				hasResolved: true,
				error: '',
			});
		} catch (ex) {
			return actions.setCustomFields({
				...DEFAULT_STATE.customFields,
				error: ex.message,
			});
		}
	},
	*getCFValue(post_id, key) {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					post_id: post_id,
					key,
					data_type: 'cf'
				},
			});
			return actions.setCFValue(post_id, key, res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setCFValue(DEFAULT_STATE.cfValue);
		}
	},
	*getPostTypes() {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					data_type: 'post_types'
				}
			});
			return actions.setPostTypes(res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setPostTypes(DEFAULT_STATE.postTypes);
		}
	},
	*getSiteInfo(key) {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					key,
					data_type: 'site_info'
				},
			});
			return actions.setSiteInfo(key, res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setSiteInfo(DEFAULT_STATE.siteInfo);
		}
	},
	*getLink(post_id, key) {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					post_id,
					key,
					data_type: 'link'
				},
			});
			return actions.setLink(post_id, key, res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setLink(DEFAULT_STATE.link);
		}
	},
	*getPostInfo(post_id, key) {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					post_id,
					key,
					data_type: 'post_info'
				},
			});
			return actions.setPostInfo(post_id, key, res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setPostInfo(DEFAULT_STATE.postInfo);
		}
	},
	*getAuthorInfo(post_id, key) {
		const path = 'ultp/v2/get_dynamic_content';
		try {
			const res = yield actions.fetchFromAPI({
				path,
				method: 'POST',
				data: {
					post_id,
					key,
					data_type: 'author_info'
				},
			});
			return actions.setAuthorInfo(post_id, key, res.data);
		} catch (ex) {
			console.log(ex);
			// actions.setAuthorInfo(DEFAULT_STATE.authorInfo);
		}
	},
};

const controls = {
	...wpControls,
	FETCH_FROM_API(action) {
		return wp.apiFetch(action.options);
	},
};

const reducer = (state = DEFAULT_STATE, action) => {
	switch (action.type) {
		case 'SET_CF':
			return {
				...state,
				customFields: action.customFields,
			};
		case 'SET_CF_VALUE':
			return {
				...state,
				cfValue: {
					...state.cfValue,
					[action.post_id]: {
						...state.cfValue[action.post_id],
						[action.key]: action.value,
					},
				},
			};
		case 'SET_SITE_INFO':
			return {
				...state,
				siteInfo: {
					...state.siteInfo,
					[action.key]: action.value,
				},
			};
		case 'SET_POST_INFO':
			return {
				...state,
				postInfo: {
					...state.postInfo,
					[action.post_id]: {
						...state.postInfo[action.post_id],
						[action.key]: action.value,
					},
				},
			};
		case 'SET_AUTHOR_INFO':
			return {
				...state,
				authorInfo: {
					...state.authorInfo,
					[action.post_id]: {
						...state.authorInfo[action.post_id],
						[action.key]: action.value,
					},
				},
			};
		case 'SET_POST_TYPES':
			return {
				...state,
				postTypes: action.postTypes,
			};
		case 'SET_LINK':
			return {
				...state,
				link: {
					...state.link,
					[action.post_id]: {
						...state.link[action.post_id],
						[action.key]: action.value,
					},
				},
			};
		default:
			return state;
	}
};

const store = createReduxStore(ULTP_STORE, {
	reducer,
	actions,
	selectors,
	resolvers,
	controls,
});

register(store);

const { invalidateResolution, invalidateResolutionForStoreSelector } =
	wp.data.dispatch(ULTP_STORE);

export {
	invalidateResolution as ultpStoreClearCache,
	invalidateResolutionForStoreSelector as ultpStoreClearSelectorCache,
};
