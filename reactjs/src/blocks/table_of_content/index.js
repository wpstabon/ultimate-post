const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/add-on/table-of-content/', 'block_docs');

registerBlockType(
    'ultimate-post/table-of-content', {
        title: __('Table of Contents','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/table-of-content.svg'}/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Add a Customizable Table of Contents into your blog posts and custom post types.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('Table','ultimate-post'),
            __('Table of Contents','ultimate-post'),
            __('Contents','ultimate-post'),
            __('toc','ultimate-post'),
        ],
        supports: {
            align: ['center', 'wide', 'full'],
            reusable: false,
        },
        attributes,
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/tableofcontents.svg',
            },
        },
        edit: Edit,
        save: Save,
    }
)