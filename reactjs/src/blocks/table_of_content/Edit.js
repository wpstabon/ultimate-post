const { __ } = wp.i18n
const { InspectorControls, RichText, BlockControls } = wp.blockEditor
const { useEffect, useState, Fragment } = wp.element
const { Toolbar, Button } = wp.components;
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink, isInlineCSS, renderList, updateCurrentPostId } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator';
import IconPack from '../../helper/fields/tools/IconPack';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import ToolBarElement from '../../helper/ToolBarElement';
const { getBlocks } = wp.data.select('core/block-editor');
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, name, attributes, clientId, className, attributes: { previewImg, blockId, tag, advanceId, headText, layout, collapsible, collapsibleType, togglePosition, topTop, toTopIcon, sticky, listData, open, close, initialCollapsible, currentPostId } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		blocksBlocks()
	}, [attributes]);

	function removeTag(heading) {
		return heading.replace(/<\/?[^>]+(>|$)|(amp;)/g, "");
	}

	function blockParse(blocks, resetData = true) {
		let heading = []
		const currentTag = JSON.parse(tag || []);

		blocks.forEach(child => {
			if (child.name == 'core/heading' || child.name == 'kadence/advancedheading') {
				const temp = child.attributes.content.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
				const link = (typeof child.attributes.anchor != 'undefined' && resetData) ? child.attributes.anchor : temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				child.attributes.anchor = link;
				if (temp) {
					if (currentTag.includes('h' + child.attributes.level)) {
						heading.push({ content: removeTag(child.attributes.content), level: child.attributes.level, link: link });
					}
				}
			} else if (child.name == 'greenshift-blocks/heading') {
				const temp = child.attributes.headingContent.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
				const link = (typeof child.attributes.id != 'undefined' && resetData) ? child.attributes.id : temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				child.attributes.id = link;
				if (temp) {
					if (currentTag.includes(child.attributes.headingTag)) {
						heading.push({ content: removeTag(child.attributes.headingContent), level: Number(child.attributes.headingTag.replace("h", "")), link: "gspb_heading-id-" + link });
					}
				}
			} else if (child.name == 'ultimate-post/heading') {
				const temp = child.attributes.headingText.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
				const link = (child.attributes.advanceId != '' && resetData) ? child.attributes.advanceId : temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				child.attributes.advanceId = link;
				if (temp) {
					if (currentTag.includes(child.attributes.headingTag)) {
						heading.push({ content: removeTag(child.attributes.headingText), level: Number(child.attributes.headingTag.replace("h", "")), link: link });
					}
				}
			} else if (child.name == 'uagb/advanced-heading') {
				const temp = child.attributes.headingTitle.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
				const link = (typeof child.attributes.anchor != 'undefined' && resetData) ? child.attributes.anchor : temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				child.attributes.anchor = link;
				if (temp) {
					if (currentTag.includes(child.attributes.headingTag)) {
						heading.push({ content: removeTag(child.attributes.headingTitle), level: child.attributes.level, link: link });
					}
				}
			} else if (child.name == 'uagb/container') {
				if (child.innerBlocks?.length) {
					heading = [...heading, ...blockParse(child.innerBlocks)]
				}
			} else if (child.name == 'ugb/heading') {
				const temp = child.attributes.title.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
				const link = (typeof child.attributes.anchor != 'undefined' && resetData) ? child.attributes.anchor : temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				child.attributes.anchor = link;
				if (temp) {
					const tag = child.attributes.titleTag ? child.attributes.titleTag : 'h2'
					if (currentTag.includes(tag)) {
						heading.push({ content: removeTag(child.attributes.title), level: Number(tag.replace("h", "")), link: link });
					}
				}
			} else if (
				child.name == 'ultimate-post/post-grid-1' ||
				child.name == 'ultimate-post/post-grid-2' ||
				child.name == 'ultimate-post/post-grid-3' ||
				child.name == 'ultimate-post/post-grid-4' ||
				child.name == 'ultimate-post/post-grid-5' ||
				child.name == 'ultimate-post/post-grid-6' ||
				child.name == 'ultimate-post/post-grid-7' ||
				child.name == 'ultimate-post/post-list-1' ||
				child.name == 'ultimate-post/post-list-2' ||
				child.name == 'ultimate-post/post-list-3' ||
				child.name == 'ultimate-post/post-list-4' ||
				child.name == 'ultimate-post/post-module-1' ||
				child.name == 'ultimate-post/post-module-2' ||
				child.name == 'ultimate-post/post-slider-1'
			) {

				if (child.attributes.headingText && child.attributes.headingShow) {
					let temp = child.attributes.headingText.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
					const link = temp.replace(/( |<.+?>|&nbsp;)/g, "_");
				console.log(link, "Test");

					child.attributes.advanceId = link;
					if (temp) {
						heading.push({ content: removeTag(child.attributes.headingText), level: Number(child.attributes.headingTag.replace("h", "")), link: link });
					}
				}
			} else if (child.name == 'ultimate-post/row') {
				if (child.innerBlocks?.length) {
					heading = [...heading, ...blockParse(child.innerBlocks)]
				}
			} else if (child.name == 'ultimate-post/column') {
				if (child.innerBlocks?.length) {
					heading = [...heading, ...blockParse(child.innerBlocks)]
				}
			} else if (child.name == 'core/columns') {
				child.innerBlocks.forEach(element => {
					if (element.name == 'core/column') {
						heading = [...heading, ...blockParse(element.innerBlocks)]
					}
				});
			} else if (child.name == 'core/group') {
				if (child.innerBlocks?.length) {
					heading = [...heading, ...blockParse(child.innerBlocks)]
				}
			} else if (child.name == 'ub/content-toggle-block') {
				child.innerBlocks.forEach(element => {
					if (element.name == 'ub/content-toggle-panel-block') {
						if (element.attributes.panelTitle) {
							const temp = element.attributes.panelTitle.replace(/(<\/?[^>]+(>|$))|[_|.$'`*&"#!~@%)(]/g, '');
							const link = temp.replace(/( |<.+?>|&nbsp;)/g, "_");
							const tag = element.attributes.titleTag ? element.attributes.titleTag : 'h2'
							element.attributes.toggleID = link;
							if (currentTag.includes(tag)) {
								heading.push({ content: removeTag(element.attributes.panelTitle), level: Number(tag.replace("h", "")), link: link });
							}
						}
					}
				});
			}
		});
		return heading;
	}

	function blocksBlocks() {
		let heading = blockParse(getBlocks())
		function sortData(curr, acc) {
			let lastIndex = curr.length - 1;
			if (curr.length === 0 || curr[0].level === acc.level) {
				curr.push(Object.assign({}, acc));
			} else if (curr[lastIndex].level < acc.level) {
				if (!curr[lastIndex].child) {
					curr[lastIndex].child = [Object.assign({}, acc)];
				} else {
					sortData(curr[lastIndex].child, acc);
				}
			} else {
				curr[lastIndex + 1] = acc;
			}
		}

		var formattedNode = [];
		heading.forEach(header => sortData(formattedNode, header));

		setAttributes({ listData: JSON.stringify(formattedNode) });

		return heading;
	}

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/table-of-content', blockId, isInlineCSS());
	}

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<BlockControls>
				<Toolbar>
					<Button
						label={'Reset'}
						className="ultp-btn-reset"
						icon="image-rotate"
						aria-haspopup="true"
						tooltip={'Reset Table of Content'}
						onClick={() => blockParse(getBlocks(), false)}>
						Reset
					</Button>
				</Toolbar>
			</BlockControls>
			<InspectorControls>
				<TemplateModal
					prev="https://www.wpxpo.com/postx/blocks/#demoid6822"
					store={store}
				/>
				<Sections>
					<Section slug="setting" title={__('Setting', 'ultimate-post')}>
						<CommonSettings initialOpen={true} store={store} title="General"
							include={[
								{
									position: 0, data: {
										type: 'layout', block: 'table-of-content', key: 'layout', label: __('Layout', 'ultimate-post'), options: [
											{ img: 'assets/img/layouts/toc/toc1.png', label: __('Style 1', 'ultimate-post'), value: 'style1', pro: false },
											{ img: 'assets/img/layouts/toc/toc2.png', label: __('Style 2', 'ultimate-post'), value: 'style2', pro: false },
											{ img: 'assets/img/layouts/toc/toc3.png', label: __('Style 3', 'ultimate-post'), value: 'style3', pro: true },
											{ img: 'assets/img/layouts/toc/toc4.png', label: __('Style 4', 'ultimate-post'), value: 'style4', pro: true },
											{ img: 'assets/img/layouts/toc/toc5.png', label: __('Style 5', 'ultimate-post'), value: 'style5', pro: true },
											{ img: 'assets/img/layouts/toc/toc6.png', label: __('Style 6', 'ultimate-post'), value: 'style6', pro: true },
											{ img: 'assets/img/layouts/toc/toc7.png', label: __('Style 7', 'ultimate-post'), value: 'style7', pro: true },
											{ img: 'assets/img/layouts/toc/toc8.png', label: __('Style 8', 'ultimate-post'), value: 'style8', pro: true },
										]
									}
								},
								{
									position: 1,
									data: {
										type: 'select', key: 'tag', label: __('Tag', 'ultimate-post'), multiple: true,
										options: [
											{ value: 'h1', label: 'H1' },
											{ value: 'h2', label: 'H2' },
											{ value: 'h3', label: 'H3' },
											{ value: 'h4', label: 'H4' },
											{ value: 'h5', label: 'H5' },
											{ value: 'h6', label: 'H6' },
										]
									}
								},
								{
									position: 2,
									data: { type: 'toggle', key: 'initialCollapsible', label: __('Initial Close Table', 'ultimate-post') }
								},
							]}
						/>
						<CommonSettings initialOpen={false} store={store} title="Heading"
							include={[
								{
									position: 1,
									data: { type: 'text', key: 'headText', label: __('Header Text', 'ultimate-post') }
								},
								{
									position: 2,
									data: { type: 'color', key: 'headColor', label: __('Color', 'ultimate-post') }
								},
								{
									position: 3,
									data: { type: 'color', key: 'headBg', label: __('Background', 'ultimate-post') }
								},
								{
									position: 4,
									data: { type: 'typography', key: 'headTypo', label: __('Typography', 'ultimate-post') }
								},
								{
									position: 5,
									data: { type: 'border', key: 'headBorder', label: __('Border', 'ultimate-post') }
								},
								{
									position: 6,
									data: { type: 'dimension', key: 'headRadius', label: __('Button Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
								},
								{
									position: 7,
									data: { type: 'dimension', key: 'headPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
								}
							]}
						/>
						<CommonSettings initialOpen={false} store={store} title={__("List Body", 'ultimate-post')}
							include={[
								{
									position: 1,
									data: {
										type: 'select', key: 'hoverStyle', label: __('Hover Style', 'ultimate-post'), pro: true,
										options: [
											{ value: 'none', label: 'Select' },
											{ value: 'style1', label: 'Style 1' },
											{ value: 'style2', label: 'Style 2' },
											{ value: 'style3', label: 'Style 3' },
											{ value: 'style4', label: 'Style 4' },
											{ value: 'style5', label: 'Style 5' }
										]
									}
								},
								{
									position: 2,
									data: { type: 'range', key: 'borderWidth', label: __('Border Width', 'ultimate-post'), min: 0, max: 10, step: 1, unit: false, responsive: false }
								},
								{
									position: 3,
									data: { type: 'color', key: 'borderColor', label: __('Border Color', 'ultimate-post') }
								},
								{
									position: 4,
									data: { type: 'toggle', key: 'sticky', pro: true, label: __('Enable Sticky', 'ultimate-post') }
								},
								{
									position: 5,
									data: {
										type: 'tag', key: 'stickyPosition', label: __('Sticky Position', 'ultimate-post'), options: [
											{ value: 'left', label: __('Left', 'ultimate-post') },
											{ value: 'right', label: __('Right', 'ultimate-post') },
											{ value: 'top', label: __('Top', 'ultimate-post') }
										]
									}
								},
								{
									position: 6,
									data: { type: 'range', key: 'listWidth', label: __('Width', 'ultimate-post'), min: 0, max: 700, step: 1, unit: true, responsive: true }
								},
								{
									position: 7,
									data: { type: 'range', key: 'listSpacingX', label: __('Gap Between Lists', 'ultimate-post'), min: 0, max: 50, step: 1, unit: false, responsive: true }
								},
								{
									position: 8,
									data: { type: 'range', key: 'listSpacingY', label: __('Spacing Y', 'ultimate-post'), min: 0, max: 100, step: 1, unit: false, responsive: true }
								},
								{
									position: 9,
									data: { type: 'color', key: 'listColor', label: __('Color', 'ultimate-post') }
								},
								{
									position: 10,
									data: { type: 'color', key: 'listHoverColor', label: __('Hover Color', 'ultimate-post') }
								},
								{
									position: 11,
									data: { type: 'color', key: 'listBg', label: __('Background Color', 'ultimate-post') }
								},
								{
									position: 12,
									data: { type: 'typography', key: 'listTypo', label: __('Typography', 'ultimate-post') }
								},
								{
									position: 13,
									data: { type: 'dimension', key: 'listPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
								},
							]}
						/>
						<CommonSettings depend="collapsible" initialOpen={false} store={store} title={__("Collapsible", 'ultimate-post')}
							include={[
								{
									position: 1,
									data: {
										type: 'tag', key: 'togglePosition', label: __('Button Position', 'ultimate-post'), options: [
											{ value: 'withTitle', label: __('Beside Title', 'ultimate-post') },
											{ value: 'right', label: __('Right', 'ultimate-post') }
										]
									}
								},
								{
									position: 2,
									data: {
										type: 'tag', key: 'collapsibleType', label: __('Collapsible Type', 'ultimate-post'), options: [
											{ value: 'text', label: __('Text', 'ultimate-post') },
											{ value: 'icon', label: __('Icon', 'ultimate-post') }
										]
									}
								},
								{
									position: 3,
									data: { type: 'text', key: 'open', label: __('Open Text', 'ultimate-post') }
								},
								{
									position: 4,
									data: { type: 'text', key: 'close', label: __('Close Text', 'ultimate-post') }
								},
								{
									position: 6,
									data: { type: 'color', key: 'collapsibleColor', label: __('Color', 'ultimate-post') }
								},
								{
									position: 7,
									data: { type: 'color', key: 'collapsibleBg', label: __('Background', 'ultimate-post') }
								},
								{
									position: 8,
									data: { type: 'color', key: 'collapsibleHoverColor', label: __('Hover Color', 'ultimate-post') }
								},
								{
									position: 9,
									data: { type: 'color', key: 'collapsibleHoverBg', label: __('Hover Background', 'ultimate-post') }
								},
								{
									position: 10,
									data: { type: 'typography', key: 'collapsibleTypo', label: __('Typography', 'ultimate-post') }
								},
								{
									position: 11,
									data: { type: 'border', key: 'collapsibleBorder', label: __('Border', 'ultimate-post') }
								},
								{
									position: 12,
									data: { type: 'dimension', key: 'collapsibleRadius', label: __('Border Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
								},
								{
									position: 13,
									data: { type: 'dimension', key: 'collapsiblePadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
								}
							]}
						/>
						<CommonSettings depend="topTop" initialOpen={false} store={store} title={__("Back To Top", 'ultimate-post')}
							include={[
								{
									position: 1,
									data: {
										type: 'tag', key: 'toTopPosition', label: __('Position', 'ultimate-post'), options: [
											{ value: 'left', label: __('Left', 'ultimate-post') },
											{ value: 'right', label: __('Right', 'ultimate-post') }
										]
									}
								},
								{
									position: 2,
									data: {
										type: 'tag', key: 'toTopIcon', label: __('Icon', 'ultimate-post'), options: [
											{ value: 'arrowUp2', label: __('Angle', 'ultimate-post') },
											{ value: 'longArrowUp2', label: __('Arrow', 'ultimate-post') },
											{ value: 'caretArrow', label: __('Caret', 'ultimate-post') }
										]
									}
								},
								{
									position: 3,
									data: { type: 'color', key: 'toTopColor', label: __('Color', 'ultimate-post') }
								},
								{
									position: 4,
									data: { type: 'color', key: 'toTopBg', label: __('Background', 'ultimate-post') }
								},
								{
									position: 5,
									data: { type: 'color', key: 'toTopHoverColor', label: __('Hover Color', 'ultimate-post') }
								},
								{
									position: 6,
									data: { type: 'color', key: 'toTopHoverBg', label: __('Hover Background', 'ultimate-post') }
								},
								{
									position: 7,
									data: { type: 'range', key: 'toTopSize', label: __('Icon Size', 'ultimate-post'), min: 0, max: 80, step: 1, unit: false, responsive: true }
								},

								{
									position: 8,
									data: { type: 'border', key: 'toTopBorder', label: __('Border', 'ultimate-post') }
								},
								{
									position: 9,
									data: { type: 'dimension', key: 'toTopRadius', label: __('Button Radius', 'ultimate-post'), step: 1, unit: true, responsive: true }
								},
								{
									position: 10,
									data: { type: 'dimension', key: 'toTopPadding', label: __('Padding', 'ultimate-post'), step: 1, unit: true, responsive: true }
								}
							]}
						/>
					</Section>
					<Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
						<GeneralAdvanced initialOpen={true} store={store} />
						<ResponsiveAdvanced store={store} />
						<CustomCssAdvanced store={store} />
					</Section>
				</Sections>
				{blockSupportLink()}
			</InspectorControls>

			<ToolBarElement
				include={[
					{
						type: 'template'
					},
					{
						type: 'layout', block: 'table-of-content', key: 'layout', options: [
							{ img: 'assets/img/layouts/toc/toc1.png', label: __('Style 1', 'ultimate-post'), value: 'style1', pro: false },
							{ img: 'assets/img/layouts/toc/toc2.png', label: __('Style 2', 'ultimate-post'), value: 'style2', pro: false },
							{ img: 'assets/img/layouts/toc/toc3.png', label: __('Style 3', 'ultimate-post'), value: 'style3', pro: true },
							{ img: 'assets/img/layouts/toc/toc4.png', label: __('Style 4', 'ultimate-post'), value: 'style4', pro: true },
							{ img: 'assets/img/layouts/toc/toc5.png', label: __('Style 5', 'ultimate-post'), value: 'style5', pro: true },
							{ img: 'assets/img/layouts/toc/toc6.png', label: __('Style 6', 'ultimate-post'), value: 'style6', pro: true },
							{ img: 'assets/img/layouts/toc/toc7.png', label: __('Style 7', 'ultimate-post'), value: 'style7', pro: true },
							{ img: 'assets/img/layouts/toc/toc8.png', label: __('Style 8', 'ultimate-post'), value: 'style8', pro: true },
						]
					}
				]}
				store={store} />

			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className} ${(sticky == true) ? 'ultp-toc-sticky' : ''}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className={`ultp-block-wrapper`}>
					<div className={`ultp-block-toc`}>
						<div className={`ultp-toc-header`}>
							<div className={`ultp-toc-heading`}>
								<RichText
									key="editable"
									tagName={'span'}
									keepPlaceholderOnFocus
									placeholder={__('Heading Table of Contents...', 'ultimate-post')}
									onChange={value => setAttributes({ headText: value })}
									value={headText} />
							</div>
							{collapsible &&
								<div className={`ultp-collapsible-toggle ${initialCollapsible ? 'ultp-toggle-collapsed' : ''} ${(togglePosition == 'right') ? 'ultp-collapsible-right' : ''}`}>
									{(collapsibleType !== 'icon') ? (
										<Fragment>
											<a className={`ultp-collapsible-text ultp-collapsible-open`} href="javascript:;">[{open}]</a>
											<a className={`ultp-collapsible-text ultp-collapsible-hide`} href="javascript:;">[{close}]</a>
										</Fragment>
									) : (
										<Fragment>
											<a className={`ultp-collapsible-icon ultp-collapsible-open`} href="javascript:;">{IconPack.arrowUp2}</a>
											<a className={`ultp-collapsible-icon ultp-collapsible-hide`} href="javascript:;">{IconPack.arrowUp2}</a>
										</Fragment>
									)
									}
								</div>
							}
						</div>
						<div className={`ultp-block-toc-${layout} ultp-block-toc-body`}>
							{renderList(listData, layout)}
						</div>
						{topTop &&
							<a href="#" className="ultp-toc-backtotop tocshow">{IconPack[toTopIcon]}</a>
						}
					</div>
				</div>
			</div>
		</Fragment>
	)
}