const { Fragment } = wp.element;
import { renderList } from '../../helper/CommonPanel'
import IconPack from '../../helper/fields/tools/IconPack'

export default function Save(props) {

	const { blockId, advanceId, headText, collapsible, collapsibleType, togglePosition, topTop, toTopIcon, layout, sticky, listData, open, close, initialCollapsible } = props.attributes

	return (
		<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${(sticky == true) ? 'ultp-toc-sticky' : ''}`}>
			<div className={`ultp-block-wrapper`}>
				<div className={`ultp-block-toc`}>
					<div className={`ultp-toc-header`}>
						<div className={`ultp-toc-heading`}>
							{headText}
						</div>
						{collapsible &&
							<div className={`ultp-collapsible-toggle ${initialCollapsible ? 'ultp-toggle-collapsed' : ''} ${(togglePosition == 'right') ? 'ultp-collapsible-right' : ''}`}>
								{(collapsibleType !== 'icon') ? (
									<Fragment>
										<a className="ultp-collapsible-text ultp-collapsible-open" href="javascript:;">[{open}]</a>
										<a className="ultp-collapsible-text ultp-collapsible-hide" href="javascript:;">[{close}]</a>
									</Fragment>
								) : (
									<Fragment>
										<a className="ultp-collapsible-icon ultp-collapsible-open" href="javascript:;">{IconPack.arrowUp2}</a>
										<a className="ultp-collapsible-icon ultp-collapsible-hide" href="javascript:;">{IconPack.arrowUp2}</a>
									</Fragment>
								)
								}
							</div>
						}
					</div>
					<div className={`ultp-block-toc-${layout} ultp-block-toc-body`} style={{ display: (initialCollapsible ? 'none;' : 'block;') }}>
						{renderList(listData, layout)}
					</div>
					{topTop &&
						<a href="#" className="ultp-toc-backtotop tocshow">{IconPack[toTopIcon]}</a>
					}
				</div>
			</div>
		</div>
	)
}