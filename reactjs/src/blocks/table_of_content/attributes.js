import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    listData: {
        type: 'string',
        default: '[]',
    },
    currentPostId: { type: "string" , default: "" },

    /*============================
        General Settings
    ============================*/
    tag: {
        type: 'string',
        default: '["h1","h2","h3","h4","h5","h6"]',
    },
    collapsible: {
        type: 'boolean',
        default: true,
    },
    initialCollapsible: {
        type: 'boolean',
        default: false,
    },
    topTop: {
        type: 'boolean',
        default: false,
    },

    /*============================
        Heading Settings
    ============================*/
    headText: {
        type: 'string',
        default: 'Table of Contents',
    },
    headColor: {
        type: 'string',
        default: '#222',
        style: [{ selector: '{{ULTP}} .ultp-toc-heading { color:{{headColor}}; }'}],
    },
    headBg: {
        type: 'string',
        default: '#f7f7f7',
        style: [{ selector: '{{ULTP}} .ultp-toc-header { background:{{headBg}}; }'}],
    },
    headTypo: {
        type: 'object',
        default: {openTypography: 1,size: {lg:22, unit:'px'},height: {lg:'', unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [{ selector: '{{ULTP}} .ultp-toc-heading'}],
    },
    headBorder: {
        type: 'object',
        default: {openBorder: 1, width: {top: 0, right: 0, bottom: 1, left: 0},color: '#eee',type: 'solid'},
        style: [{ selector: '{{ULTP}} .ultp-toc-header'}],
    },
    headRadius: {
        type: 'object',
        default: {lg:{top:'',bottom:'',left:'',right:'',unit:'px'}},
        style: [{ selector: '{{ULTP}} .ultp-toc-header { border-radius:{{headRadius}}; }'}],
    },
    headPadding: {
        type: 'object',
        default: {lg:{top:'16',bottom:'16',left:'20',right:'20',unit:'px'}},
        style: [{ selector: '{{ULTP}} .ultp-toc-header { padding:{{headPadding}}; }'}],
    },

    /*============================
        List Body Settings
    ============================*/
    layout: {
        type: 'string',
        default: 'style1',
    },
    hoverStyle: {
        type: 'string',
        default: 'none',
        style: [ 
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'none' }
                ]
            },
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'style1' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover { border-bottom-style:dotted; }'
            },
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'style2' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover { border-bottom-style:solid; }'
            },
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'style3' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover { border-bottom-style:dashed; }'
            },
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'style4' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover { text-decoration:underline; }'
            },
            {
                depends: [
                    { key:'hoverStyle',condition:'==',value: 'style5' }
                ],
                selector: 
                `{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a { position:relative; } 
                {{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:after {content: ""; position:absolute; left:0; width:0; top:calc(100% + 2px); transition: width 350ms,opacity 350ms;} 
                {{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover:after { opacity:1; width:100%; }`
            },
        ],
    },
    borderColor: {
        type: 'string',
        default: '#037fff',
        style: [
            {
                depends:[
                    { key:'hoverStyle',condition:'==',value: ['style1','style2','style3'] }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover { border-bottom-color:{{borderColor}}; }'
            },
            {
                depends:[
                    { key:'hoverStyle',condition:'==',value: 'style5' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:after {background:{{borderColor}}; }'
            }
        ],
    },
    borderWidth: {
        type: 'string',
        default: '1',
        style: [
            {
                depends:[
                    { key:'hoverStyle',condition:'==',value: ['style1','style2','style3'] }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:hover {border-bottom-width: {{borderWidth}}px; }'
            },
            {
                depends:[
                    { key:'hoverStyle',condition:'==',value: 'style5' }
                ],
                selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists a:after {height:{{borderWidth}}px; }'
            }
    ],
    },
    sticky: {
        type: 'boolean',
        default: false,
    },
    stickyPosition: {
        type: 'string',
        default: 'right',
        style: [ 
            {
                depends: [
                    { key:'stickyPosition',condition:'==',value: 'left' },
                    { key:'sticky',condition:'==',value: true }
                ],
                selector: '{{ULTP}}.ultp-toc-sticky.ultp-toc-scroll { right:auto;left:0; }'
            },
            {
                depends: [
                    { key:'stickyPosition',condition:'==',value: 'right' },
                    { key:'sticky',condition:'==',value: true }
                ],
                selector: '{{ULTP}}.ultp-toc-sticky.ultp-toc-scroll { right:0;left:auto; }'
            },
            {
                depends: [
                    { key:'stickyPosition',condition:'==',value: 'top' },
                    { key:'sticky',condition:'==',value: true }
                ],
                selector: 
                `{{ULTP}}.ultp-toc-sticky.ultp-toc-scroll { top:0;left:0;right:0;margin: 0 auto; } 
                .admin-bar .ultp-toc-sticky.ultp-toc-scroll { top: 32px}`
            }
        ],
    },
    listWidth: {
        type: 'object',
        default: {lg:{unit:'px'}},
        style: [
            {
            depends:[{ key:'sticky',condition:'==',value: true }],
                selector: 
                `{{ULTP}}.wp-block-ultimate-post-table-of-content.ultp-toc-sticky { max-width: {{listWidth}}; width: {{listWidth}}; } 
                .single {{ULTP}}.wp-block-ultimate-post-table-of-content.ultp-toc-sticky { max-width: {{listWidth}}; width: {{listWidth}}; }`
            },
            {
                depends:[{ key:'sticky',condition:'==',value: false }],
                selector: 
                `{{ULTP}} .ultp-block-wrapper { max-width: {{listWidth}}; width: {{listWidth}}; } 
                .single {{ULTP}} .ultp-block-wrapper { max-width: {{listWidth}}; width: {{listWidth}}; }`
            }
        ],
    },
    listSpacingX: {
        type: 'object',
        default: {lg:'8'},
        style: [{ selector: '{{ULTP}} .ultp-toc-lists li { margin-top: {{listSpacingX}}px; }'}],
    },
    listSpacingY: {
        type: 'object',
        default: {lg:''},
        style: [{ selector: '{{ULTP}} .ultp-toc-lists li a { margin-left: {{listSpacingY}}px;}'}],
    },
    listColor: {
        type: 'string',
        default: '#222',
        style: [{ selector: 
            `{{ULTP}} .ultp-block-toc-body .ultp-toc-lists li a::before, 
            {{ULTP}} .ultp-block-toc-body .ultp-toc-lists a, 
            {{ULTP}} .ultp-block-toc-body .ultp-toc-lists { color:{{listColor}}; }`
        }],
    },
    listHoverColor: {
        type: 'string',
        default: '#037fff',
        style: [{ 
            selector: 
            `{{ULTP}} .ultp-block-toc-body .ultp-toc-lists > li a:hover::before, 
            {{ULTP}} .ultp-block-toc-body .ultp-toc-lists > li a:hover { color:{{listHoverColor}}; }`
        }]
    },
    listBg: {
        type: 'string',
        default: '#fff',
        style: [{ selector: '{{ULTP}} .ultp-block-toc-body { background-color:{{listBg}}; }'}]
    },
    listTypo: {
        type: 'object',
        default: {openTypography: 1,size: {lg:'16', unit:'px'},height: {lg:'', unit:'px'}, decoration: 'none',family: '', weight:500},
        style: [{ selector: '{{ULTP}} .ultp-block-toc-body .ultp-toc-lists li a'}],
    },
    listPadding: {
        type: 'object',
        default: {lg:{top: '20',bottom: '20',left: '20', right: '20', unit:'px'}},
        style: [{ selector: '{{ULTP}} .ultp-block-toc-body { padding:{{listPadding}}; }'}]
    },

    /*============================
        Collapsible Settings
    ============================*/
    togglePosition: {
        type: 'string',
        default: 'right',
        style: [ 
            {
                depends: [
                    { key:'togglePosition',condition:'==',value: 'withTitle' },
                    { key:'collapsible',condition:'==',value: true }
                ],
            },
            {
                depends: [
                    { key:'togglePosition',condition:'==',value: 'right' },
                    { key:'collapsible',condition:'==',value: true }
                ],
                selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-toggle.ultp-collapsible-right { margin-left:auto; }'
            }
        ],

    },
    collapsibleType: {
        type: 'string',
        default: 'text',
        style: [ 
            [{depends:{ key:'collapsible',condition:'==',value: true }}]
        ],
    },
    open: {
        type: 'string',
        default: 'Open',
        style: [ {
            depends: [
                { key:'collapsibleType',condition:'==',value: 'text' },
                { key:'collapsible',condition:'==',value: true }
            ],
        }],
    },
    close: {
        type: 'string',
        default: 'Close',
        style: [ {
            depends: [
                { key:'collapsibleType',condition:'==',value: 'text' },
                { key:'collapsible',condition:'==',value: true }
            ],
        }],
    },
    collapsibleColor: {
        type: 'string',
        default: '#037fff',
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: 
            `{{ULTP}} .ultp-block-toc .ultp-toc-header .ultp-collapsible-text { color:{{collapsibleColor}}; } 
            {{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a svg { fill:{{collapsibleColor}}; }`
        }],
    },
    collapsibleBg: {
        type: 'string',
        default: '',
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-collapsible-toggle a { background:{{collapsibleBg}}; }'
        }],
    },
    collapsibleHoverColor: {
        type: 'string',
        default: '',
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: 
            `{{ULTP}} .ultp-toc-header .ultp-collapsible-text:hover { color:{{collapsibleHoverColor}}; } 
            {{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a:hover svg { fill:{{collapsibleHoverColor}}; }`
        }],
    },
    collapsibleHoverBg: {
        type: 'string',
        default: '',
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a:hover { background:{{collapsibleHoverBg}}; }'
        }],
    },
    collapsibleTypo: {
        type: 'object',
        default: {openTypography: 1,size: {lg:16, unit:'px'},height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [{
            depends: [
                { key:'collapsibleType',condition:'==',value: 'text' },
                { key:'collapsible',condition:'==',value: true }
            ],
            selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-text'
        }],
    },
    collapsibleBorder: {
        type: 'object',
        default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a'
        }],
    },
    collapsibleRadius: {
        type: 'object',
        default: {lg:{top:'',bottom:'',left:'',right:'',unit:'px'}},
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a { border-radius:{{collapsibleRadius}}; }'
        }],
    },
    collapsiblePadding: {
        type: 'object',
        default: {lg:{top:'',bottom:'',left:'',right:'',unit:'px'}},
        style: [{
            depends:[{ key:'collapsible',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-toc-header .ultp-collapsible-toggle a { padding:{{collapsiblePadding}}; }'
        }],
    },
    
    /*============================
        To Top Settings
    ============================*/
    toTopPosition: {
        type: 'string',
        default: 'right',
        style: [ 
            {
                depends: [
                    { key:'toTopPosition',condition:'==',value: 'left' },
                    { key:'topTop',condition:'==',value: true }
                ],
                selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop { left:40px;right:auto; }'
            },
            {
                depends: [
                    { key:'toTopPosition',condition:'==',value: 'right' },
                    { key:'topTop',condition:'==',value: true }
                ],
                selector: 
                `{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop { right:40px;left:auto; }
                .editor-styles-wrapper .ultp-toc-backtotop {margin-right: 260px; margin-left: 150px;}`
            }
        ],
    },
    toTopIcon: {
        type: 'string',
        default: 'arrowUp2',
        style: [ 
            [{depends:{ key:'topTop',condition:'==',value: true }}]
        ],
    },
    toTopColor: {
        type: 'string',
        default: '#fff',
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop svg { fill:{{toTopColor}}; }'
        }],
    },
    toTopBg: {
        type: 'string',
        default: '#222',
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop { background:{{toTopBg}}; }'
        }],
    },
    toTopHoverColor: {
        type: 'string',
        default: '#fff',
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop:hover svg { fill:{{toTopHoverColor}}; }'
        }],
    },
    toTopHoverBg: {
        type: 'string',
        default: '#000',
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: 
            `{{ULTP}}.wp-block-ultimate-post-table-of-content .ultp-block-wrapper .ultp-toc-backtotop:hover, 
            {{ULTP}}.wp-block-ultimate-post-table-of-content .ultp-block-wrapper .ultp-toc-backtotop:focus { background:{{toTopHoverBg}}; }`
        }],
    },
    toTopSize: {
        type: 'object',
        default: {lg:'18'},
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop svg { width:{{toTopSize}}px; }'
        }],
    },
    toTopBorder: {
        type: 'object',
        default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop'
        }],
    },
    toTopRadius: {
        type: 'object',
        default: {lg:{top:'4',bottom:'4',left:'4',right:'4',unit:'px'}},
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop { border-radius:{{toTopRadius}}; }'
        }],
    },
    toTopPadding: {
        type: 'object',
        default: {lg:{top:'13',bottom:'15',left:'11',right:'11',unit:'px'}},
        style: [{
            depends:[{ key:'topTop',condition:'==',value: true }],
            selector: '{{ULTP}} .ultp-block-wrapper .ultp-toc-backtotop { padding:{{toTopPadding}}; }'
        }],
    },

    /*============================
        Wrapper Settings
    ============================*/
    advanceId: {
        type: 'string',
        default: '',
    },
    ...commonAttributes(['advanceAttr'], ['loadingColor', 'advanceId'], [
        { 
            key: 'wrapShadow',
            default: {openShadow: 1, width: {top: 2, right: 5, bottom: 15, left: -2, unit: 'px'},color: 'rgba(0,0,0,0.1)'},
            style: [{selector: '{{ULTP}} .ultp-block-wrapper'}]
        },
        { 
            key: 'wrapShadow',
            default: {openShadow: 1, width: {top: 2, right: 5, bottom: 15, left: -2, unit: 'px'},color: 'rgba(0,0,0,0.1)'},
            style: [{selector: '{{ULTP}} .ultp-block-wrapper'}]
        },
    ])
}

export default attributes;