const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-List-4/', 'block_docs');

registerBlockType(
    'ultimate-post/post-list-4', {
        title: __('Post List #4','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-list-4.svg'}/>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('Listing your posts with a big image on top and small images at the bottom.','ultimate-post')}
        //     <a target="_blank" href={docsUrl}  >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [
            __('Post List','ultimate-post'),
            __('List View','ultimate-post'),
            __('Article','ultimate-post'),
            __('Listing','ultimate-post'),
            __('List','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/postlist4.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)