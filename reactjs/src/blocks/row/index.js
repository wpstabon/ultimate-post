const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/postx-features/row-column/', 'block_docs');

registerBlockType(
    'ultimate-post/row', {
        title: __('Row','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/row.svg'}/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Add Row block for your layout.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('row','ultimate-post'),
            __('wrap','ultimate-post'),
            __('column','ultimate-post')
        ],
        supports: {
            align: ['center', 'wide', 'full'],
            html: false,
            reusable: false,
        },
        attributes,
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/wrapper.svg',
            },
        },
        edit: Edit,
        save: Save,
    }
)