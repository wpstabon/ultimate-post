const attributes =  {
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    currentPostId: { type: "string" , default: "" },
    /*==========================
        Column Style
    ==========================*/
    layout: {
        type: 'object',
        default: {},
    },
    columnGap: {
        type: 'object',
        default: { lg:'20', sm: '10' , xs: '5' },
        style:[
            {
                selector: 
                `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { column-gap: {{columnGap}}px;}`
            }
        ],
    },
    rowGap: {
        type: 'object',
        default: {lg:'20'},
        style: [{
            selector: 
            `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
            {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { row-gap: {{rowGap}}px }`
        }]
    },
    inheritThemeWidth: {
        type: 'boolean',
        default: false,
    },
    rowContentWidth: {
        type: 'object',
        default: {lg:'1140', ulg:'px'},
        style:[{
                depends: [
                    { key:'inheritThemeWidth', condition:'==', value: false },
                ],
                selector: 
                ` {{ULTP}} > .ultp-row-wrapper  > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { max-width: {{rowContentWidth}}; margin-left: auto !important; margin-right: auto !important;}` 
            },
        ],
    },
    contentHeightType: {
        type: 'string',
        default: 'normal',
        style:[
            {
                depends: [
                    { key:'contentHeightType', condition:'==', value: 'custom' },
                ], 
            },
            {
                depends: [
                    { key:'contentHeightType', condition:'==', value: 'normal' },
                ], 
            },
            {
                depends: [
                    { key:'contentHeightType', condition:'==', value: 'windowHeight' },
                ],
                selector: 
                `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { height: 100vh; }`
            }
        ],
    },
    contentHeight: {
        type: 'object',
        default: {lg:'100', ulg:'px'},
        style:[
            {
                depends: [
                    { key:'contentHeightType', condition:'==', value: 'custom' },
                ],
                selector: 
                `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { min-height: {{contentHeight}} }`
            }
        ],
    },
    contentOverflow: {
        type: 'string',
        default: 'visible',
        style: [{
            selector:  
            `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout,  
            {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { overflow: {{contentOverflow}} }`
        }],
    },
    HtmlTag: {
        type: 'string',
        default: 'div',
    },
    enableRowLink: {
        type: 'boolean',
        default: false,
    },
    rowWrapperLink: {
        type: 'string',
        default: '',
        style:[
            {
                depends: [
                    { key:'enableRowLink', condition:'==', value: true },
                ],
            }
        ],
    },
    enableNewTab: {
        type: 'boolean',
        default: false,
        style:[
            {
                depends: [
                    { key:'enableRowLink', condition:'==', value: true },
                ],
            }
        ],
    },

    /*==========================
        Flex Properties
    ==========================*/ 
    reverseEle: {
        type: 'boolean',
        default: false,
        style: [{
            selector: 
            `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
            {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { flex-direction: row; }`
        },
        {
            depends: [
                { key:'reverseEle', condition:'==', value: true },
            ], 
            selector: 
            `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
            {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { flex-direction: row-reverse; }`
        }]
    },
    ColumnAlign: {
        type: 'string',
        default: '',
        style:[
            {
                depends: [{ key:'ColumnAlign', condition:'==', value: 'stretch' },],
                selector:
                `{{ULTP}} > .ultp-row-wrapper > .ultp-row-content > .wp-block-ultimate-post-column { height: auto } 
                {{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-column, 
                {{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-column > .ultp-column-wrapper { height: 100%; box-sizing: border-box;}`
            },
            {
                depends: [{ key:'ColumnAlign', condition:'!=', value: 'stretch' },],
            },
            { 
                selector: 
                `{{ULTP}} > .ultp-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}} > .ultp-row-wrapper > .ultp-row-content { align-items: {{ColumnAlign}} } ` 
            }
        ],
    },

    /*==========================
        Background & Wrapper
    ==========================*/
    rowBgImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat', loop: true},
        style:[{ 
            selector: '{{ULTP}} > .ultp-row-wrapper'
        }],
    },

    /*========== Hover Background & Wrapper =========*/
    rowWrapHoverImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-row-wrapper:hover'
        }],
    },

    /*==========================
        Overlay Background
    ==========================*/
    rowOverlayBgImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5',  size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-row-overlay'
        }],
    },
    rowOverlayOpacity: {
        type: 'string',
        default: '50',
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-row-overlay { opacity:{{rowOverlayOpacity}}%; }'
        }]
    },
    rowOverlayBgFilter: {
        type: 'object',
        default: {openFilter: 0, hue: 0, saturation: 100, brightness: 100, contrast: 100, invert: 0, blur: 0},
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-row-overlay { {{rowOverlayBgFilter}} }'
        }]
    },
    rowOverlayBgBlend: {
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-row-overlay { mix-blend-mode:{{rowOverlayBgBlend}}; }'
        }]
    },
    /*========   Overlay Hover   =======*/
    rowOverlaypHoverImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-row-wrapper:hover > .ultp-row-overlay'
        }],
    },
    rowOverlayHovOpacity: {
        type: 'string',
        default: '50',
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper:hover > .ultp-row-overlay { opacity:{{rowOverlayHovOpacity}}% }'
        }]
    },
    rowOverlayHoverFilter: {
        type: 'object',
        default: {openFilter: 0, hue: 0, saturation: 100, brightness: 100, contrast: 100, invert: 0, blur: 0},
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper:hover > .ultp-row-overlay { {{rowOverlayHoverFilter}} }'
        }]
    },
    rowOverlayHoverBgBlend: {
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper:hover > .ultp-row-overlay { mix-blend-mode:{{rowOverlayHoverBgBlend}}; }'
        }]
    },

    /*==========================
        Spacing % Border Style
    ==========================*/
    rowWrapMargin: {
        type: 'object',
        default: {lg:{top: '',bottom: '', unit:'px'}},
        style:[{ selector:'{{ULTP}} > .ultp-row-wrapper { margin:{{rowWrapMargin}}; }' }],
    },
    rowWrapPadding: {
        type: 'object',
        default: { lg:{ top: '15', bottom: '15', unit:'px'} },
        style: [{ 
            selector:
            `{{ULTP}}.wp-block-ultimate-post-row > .ultp-row-wrapper:not(:has( > .components-resizable-box__container)), 
            {{ULTP}}.wp-block-ultimate-post-row > .ultp-row-wrapper:has( > .components-resizable-box__container) > .block-editor-inner-blocks {padding: {{rowWrapPadding}}; }` 
        }],
    },

    /*===== Border Style ======*/
    rowWrapBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [{
            selector:'{{ULTP}} > .ultp-row-wrapper'
        }]
    },
    rowWrapRadius: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid' },
        style: [{
            selector: 
            `{{ULTP}} > .ultp-row-wrapper { border-radius: {{rowWrapRadius}};} 
            {{ULTP}} > .ultp-row-wrapper > .ultp-row-overlay { border-radius: {{rowWrapRadius}}; }`,
        }]
    },
    rowWrapShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ selector: '{{ULTP}} > .ultp-row-wrapper' }],
    },

    /*===== Border Hover Style ======*/
    rowWrapHoverBorder: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4' },
        style: [{
            selector:'{{ULTP}} > .ultp-row-wrapper:hover'
        }]
    },
    rowWrapHoverRadius: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid' },
        style: [{
            selector: 
            `{{ULTP}} > .ultp-row-wrapper:hover, 
            {{ULTP}} > .ultp-row-wrapper:hover > .ultp-row-overlay { border-radius: {{rowWrapHoverRadius}};}`,
        }]
    },
    rowWrapHoverShadow: {
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ selector: '{{ULTP}} > .ultp-row-wrapper:hover' }],
    },

    /*==========================
        Shape Style
    ==========================*/
    /*===== Top Shape Style ======*/
    rowTopShape:{
        type: 'string',
        default: 'empty',
    },
    rowTopGradientColor: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#CCCCCC',},
    },
    rowTopShapeWidth: {
        type: 'object',
        default: {lg:''},
        style: [{ selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-top > svg { width:calc({{rowTopShapeWidth}}% + 1.3px); }'}]
    },
    rowTopShapeHeight:{
        type: 'object',
        default: {lg:''},
        style: [{selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-top > svg { height:{{rowTopShapeHeight}}px; } ' }]
    },
    rowTopShapeFlip: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [{ key:'rowTopShapeFlip', condition:'==', value: false }]
            },
            {
                depends: [{ key:'rowTopShapeFlip', condition:'==', value: true }],
                selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-top > svg { transform:rotateY(180deg); }',
            },
        ]
    },
    rowTopShapeFront: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [{ key:'rowTopShapeFront', condition:'==', value: false }]
            },
            {
                depends: [{ key:'rowTopShapeFront', condition:'==', value: true },],
                selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-top { z-index: 1; }',
            },
        ]
    },
    rowTopShapeOffset: {
        type: 'object',
        default: {lg:'',},
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-top { top: {{rowTopShapeOffset}}px !important; }'
        }]
    },

    /*===== Bottom Shape Style ======*/
    rowBtmShape:{
        type: 'string',
        default: 'empty',
    },
    rowBtmGradientColor: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#CCCCCC'},
    },
    rowBtmShapeWidth: {
        type: 'object',
        default: {lg:''},
        style: [{
            selector:
            `{{ULTP}} > .ultp-row-wrapper > .ultp-shape-bottom > svg, 
            {{ULTP}} > .ultp-shape-bottom > svg { width: calc({{rowBtmShapeWidth}}% + 1.3px); }`
        }]
    },
    rowBtmShapeHeight:{
        type: 'object',
        default: {lg:''},
        style: [{
            selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-bottom > svg { height: {{rowBtmShapeHeight}}px; }'
        }]
    },
    rowBtmShapeFlip: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'rowBtmShapeFlip', condition:'==', value: false },
                ]
            },
            {
                depends: [
                    { key:'rowBtmShapeFlip', condition:'==', value: true },
                ],
                selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-bottom > svg { transform: rotateY(180deg) rotate(180deg); }',
            },
        ]
    },
    rowBtmShapeFront: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'rowBtmShapeFront', condition:'==', value: false },
                ]
            },
            {
                depends: [
                    { key:'rowBtmShapeFront', condition:'==', value: true },
                ],
                selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-bottom { z-index: 1; }',
            },
        ]
    },
    rowBtmShapeOffset: {
        type: 'object',
        default: {lg:''},
        style: [{selector: '{{ULTP}} > .ultp-row-wrapper > .ultp-shape-bottom { bottom: {{rowBtmShapeOffset}}px !important; }'}]
    },

    /*==========================
        Row Color
    ==========================*/
    rowColor:{
        type: 'string',
        default: '',
        style: [{selector:'{{ULTP}} > .ultp-row-wrapper { color:{{rowColor}} } '}]
    },
    rowLinkColor:{
        type: 'string',
        default: '',
        style: [{selector:'{{ULTP}} > .ultp-row-wrapper a { color:{{rowLinkColor}} }'}]
    },
    rowLinkHover:{
        type: 'string',
        default: '',
        style: [{
            selector: `{{ULTP}} > .ultp-row-wrapper a:hover { color:{{rowLinkHover}}; }`
        }]
    },
    rowTypo: {
        type: 'object',
        default: {openTypography: 0,size: {lg:16, unit:'px'},height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [{ selector: '{{ULTP}} > .ultp-row-wrapper' }]
    },

    /*==========================
        Row Position Style/Settings
    ==========================*/
    rowStickyPosition: {
        type: 'string',
        default: '',
    },
    rowDisableSticky: {
        type: 'boolean',
        default: false,
        style:[
            {
                depends: [{ key:'rowStickyPosition', condition:'!=', value: '' }],
            },
        ],
    },
    disableStickyRanges: {
        type: 'string',
        default: '',
        style:[
            {
                depends: [
                    { key:'rowDisableSticky', condition:'==', value: true },
                    { key:'rowStickyPosition', condition:'==', value: 'row_scrollToStickyTop' }
                ],
                selector: ` @media (max-width: {{disableStickyRanges}}px) { .postx-page {{ULTP}} { position: static !important; } }`
            },
            // {
            //     depends: [
            //         { key:'rowDisableSticky', condition:'==', value: true },
            //         { key:'rowStickyPosition', condition:'==', value: 'row_stickyFixed' }
            //     ],
            //     selector: ` @media (max-width: {{disableStickyRanges}}px) { .postx-page {{ULTP}} { position: static !important; } }`
            // },
            {
                depends: [
                    { key:'rowDisableSticky', condition:'==', value: true },
                    { key:'rowStickyPosition', condition:'==', value: 'row_sticky' }
                ],
                selector: ` @media (max-width: {{disableStickyRanges}}px) { .postx-page {{ULTP}} { position: static !important; } }`
            },
        ],
    },
    /*==========================
        Advanced Setting
    ==========================*/
    advanceId:{
        type: 'string',
        default: '',
    },
    advanceZindex:{
        type: 'string',
        default: '',
        style:[{ selector: 'body {{ULTP}}.wp-block-ultimate-post-row {z-index:{{advanceZindex}} !important;}' }],
    },
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{selector: ''}],
    }
}

export default attributes;