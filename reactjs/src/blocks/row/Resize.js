const { ResizableBox } = wp.components;
const { useState } = wp.element
export const Resize = (props) => {
    const {position, setLengthFunc, unit, previousLength, toggleSelection, minHeight, maxHeight, minWidth, maxWidth , currentColumnParentWidth, stateHandle} = props;
    const [previewLength, setPreviewLength]= useState(previousLength);
    const [resizing, setResizing]= useState(false);
    
    return (
        <ResizableBox
            size={ {
                height: position == 'right' ? '100%' : previousLength + unit,
                width: position == 'right' ? previousLength + unit : 'auto'
            } }
            minHeight={ 0 }
            minWidth= {minWidth ? minWidth+unit : "0%" }
            maxWidth= {maxWidth ? maxWidth+unit : "100%" }
            enable={ { [position]: true } }
            onResize={ ( event, direction, elt, delta ) => {
                event.preventDefault();
                setResizing(true);
                setPreviewLength(previousLength + delta.height);
                if(stateHandle) {
                    stateHandle(previousLength, position == 'right' ? delta.width : delta.height, position)
                }
                toggleSelection( true );
            } }
            onResizeStop={ ( event, direction, elt, delta ) => {
                setLengthFunc(previousLength, position == 'right' ? delta.width : delta.height, position);
                toggleSelection( true );
                setResizing(false);
            } }
            onResizeStart={ () => {
                toggleSelection( false );
            } }
        >
            { ( previewLength && ['top', 'bottom'].includes(position) ) ?
                <div className={`ultp-dragable-padding ultp-dragable-padding-${position}`}>
                    <span>{ (resizing ? previewLength : previousLength) + unit }</span>
                </div> : ''
            }
        </ResizableBox>
    )
}
