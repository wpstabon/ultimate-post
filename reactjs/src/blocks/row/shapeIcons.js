import { getPresetGradientLists, handleTypoFieldPresetData } from "../../helper/CommonFunctions";

export const renderShapeIcons = (shapeName, id, position, gradientColor) => {
    const shapes = {
        asymmetrical_triangle : <g><polygon points="1000,5 732.93,100 0,5 0,0 1000,0 	"/></g>,
        asymmetrical_triangle_invert : <polygon points="1000,0 1000,100 732.9,5 0,100 0,0 "/>,
        curve : <path d="M1000,0L1000,0c0,1.3-1,2.5-2.7,3.2C854,61.3,683.2,100,500,100C316.8,100,146,61.3,2.7,3.2C1,2.5,0,1.3,0,0H1000z"/>,
        curve_invert : <g><path d="M1000,100V0H0v100c0-1.3,1-2.5,2.7-3.2C146,38.7,316.8,3.3,500,3.3s354,35.4,497.3,93.6C999,97.5,1000,98.7,1000,100z"/></g>,
        mountain : <g><path style={{opacity: '0.34'}} className="st0" d="M642.9,36.2c13-11.4,25.1-21.2,39.6-21c37.6,0.6,59.9,52.6,98.2,60.7c1.6,0.3,3.2,0.6,4.8,0.9C768.9,88,750,94.2,726.9,88.5C689.9,79.3,682.5,45.1,642.9,36.2z"/><path style={{opacity: '0.34'}} className="st0" d="M415.5,29.7c26.1-12.2,54-14.2,86.7,9.8c17.6,13,33,21.2,46.5,25.8c-18.1,13.1-37.2,23.4-70,14.2C453.6,72.5,436.7,48.9,415.5,29.7z"/><path style={{opacity: '0.6'}} className="st1" d="M548.8,65.3c17.9-12.9,34.9-28.6,63.2-30.8c12.3-0.9,22.4-0.2,31,1.7C620.1,56.2,594.6,81,548.8,65.3z"/><path style={{opacity: '0.6'}} className="st1" d="M785.5,76.8c34.4-23.1,59.1-67.4,91.7-70.7c52.1-5.2,68,33.1,122.8,33.1v42.4c-43.9,0-56.9-54.7-98-54.4C870.1,28.1,859.2,89.7,785.5,76.8z"/><path style={{opacity: '0.6'}} className="st1" d="M0,81.6V39.2c62.5,0,62.5-31.9,125-31.9S208.2,61,260,54.2c36-4.7,47.2-51.6,93.2-51.6c27.2,0,46.1,12.2,62.4,27c-63,29.3-115.8,117.1-202.6,38.6c-28.3-25.7-47.6-53.8-103.3-48.3C60.2,24.8,43.9,81.6,0,81.6z"/><path d="M0,39.2V0.1h1000v39.1c-54.8,0-70.7-38.4-122.8-33.2c-32.6,3.3-57.3,47.6-91.7,70.7c-1.6-0.3-3.1-0.6-4.8-0.9c-38.3-8.1-60.6-60-98.2-60.7c-14.5-0.2-26.6,9.6-39.6,21c-8.6-2-18.7-2.7-31-1.7c-28.3,2.2-45.2,17.9-63.2,30.8c-13.6-4.6-28.9-12.8-46.5-25.8c-32.7-24.1-60.6-22-86.7-9.8c-16.3-14.8-35.2-27-62.4-27c-45.9,0-57.1,46.9-93.2,51.6c-51.7,6.8-72.5-46.9-135-46.9S62.5,39.2,0,39.2z"/></g>,
        tilt :  <polygon points="0,0 1000,0 1000,100 "/>,
        waves : <path d="M1000,0v65.8c-15.6-11.2-31.2-22.4-62.5-22.4c-46.2,0-64.7,33.2-116.2,33.2c-92.3,0-118-65-225.2-65c-121.4,0-132.5,88.5-238.5,88.5c-70.3,0-89.6-51.8-167.4-51.8c-65.4,0-73.4,40-127.8,40C31.3,88.2,15.6,77,0,65.8V0H1000z"/>,
        waves_invert :<path d="M1000,45.7V0H0v45.7c15.6-11.2,31.3-22.4,62.5-22.4c54.4,0,62.4,40,127.8,40c77.7,0,97.1-51.8,167.4-51.8c106,0,117,88.5,238.5,88.5c107.2,0,132.9-65,225.2-65c51.4,0,69.9,33.2,116.2,33.2C968.8,68.1,984.4,56.9,1000,45.7z"/>,
    };

    const Splitter = (str) => {
        str = handleTypoFieldPresetData(str) ? str : getPresetGradientLists('colorcode', str);
        const regex = /linear-gradient\(|\);/gi;
        str = str.replace(regex, '');
        str = (str.slice(-1) == ')') ? str.substring(0, str.length-1) : str;

        str = str.split('deg,')
        const deg = str[0];
        str = str[1].split(/(%,|%)/ig);
        str = str.filter(function(item){return !['','%','%,'].includes(item);});
        return (
            <linearGradient id={`${position}shape-${id}`} gradientTransform={`rotate(${deg})`}>
                { str.map((el, i) => {
                    let final = el.replace(')', ')|||')
                    final = final.split('|||')
                    return (<stop key={i} offset={final[1]+'%'} stopColor={final[0]}></stop>)
                })}
            </linearGradient>
        )
    }

    return (
        <svg viewBox="0 0 1000 100" preserveAspectRatio="none"  fill= { gradientColor?.openColor ? gradientColor.type == 'gradient' ? `url(#${position}shape-${id})` : gradientColor.color : '#f5f5f5' }>
            {shapes[shapeName]}
           <defs>
                {gradientColor?.openColor && gradientColor?.type == 'gradient' &&
                    Splitter(gradientColor.gradient)
                }
            </defs>
        </svg>
    )
}