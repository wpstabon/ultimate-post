const { __ } = wp.i18n
import { CommonSettings } from '../../../helper/CommonPanel'

const ColumnSettings = (props) => {
    const { store } = props


    return (
            <>
                <CommonSettings title={__('Column Item','ultimate-post')} include={[
                    {
                        position: 1, data: { type:'tag', key:'columnOrderNumber', options: [
                            { value: '1', label: __('1', 'ultimate-post') },
                            { value: '2', label: __('2', 'ultimate-post') }, 
                            { value: '3', label: __('3', 'ultimate-post') },
                            { value: '4', label: __('4', 'ultimate-post') },
                            { value: '5', label: __('5', 'ultimate-post') },
                            { value: '6', label: __('6', 'ultimate-post') },
                        ] , label:__('Column Order','ultimate-post') } 
                    },
                    { 
                        position: 3, data: { type:'range',key:'columnHeight', min: 0, max: 1300, step: 1, unit:['px', '%', 'vh', 'rem'], responsive: true, label :__('Min Height','ultimate-post') } 
                    },
                    {
                        position: 6, data: { type:'group', key:'columnDirection', justify: true, options: [
                            { value: 'column', label: __('Vertical', 'ultimate-post') },
                            { value: 'row', label: __('Horizontal', 'ultimate-post') },
                        ], label:__('Flex Direction','ultimate-post') } 
                    },
                    { 
                        position: 4, data: { type: 'alignment', key: 'columnJustify', disableJustify: true, responsive: true, icons: ['juststart', 'justcenter', 'justend', 'justbetween', 'justaround', 'justevenly'], options:['flex-start', 'center', 'flex-end','space-between', 'space-around', 'space-evenly'], label: __('Inside Alignment ( Horizontal )', 'ultimate-post') } 
                    },
                    {
                        position: 5, data: { type: 'alignment', block: 'column-column', key: 'columnAlign', disableJustify: true, icons: ['algnStart', 'algnCenter', 'algnEnd', 'stretch'], options:['flex-start', 'center', 'flex-end', 'space-between'], label: __('Inside Content Alignment ( Vertical )', 'ultimate-post') }
                    },
                    {
                        position: 5, data: { type: 'alignment', block: 'column-column', key: 'columnAlignItems', disableJustify: true, icons: ['algnStart', 'algnCenter', 'algnEnd'], options:['flex-start', 'center', 'flex-end'], label: __('Inside Items Alignment', 'ultimate-post') }
                    },
                    { 
                        position: 7, data: { type:'range',key:'columnItemGap', min: 0, max: 300, step: 1, responsive: false, label:__('Gap','ultimate-post') } 
                    },
                    {
                        position: 8, data: { type:'group', key:'columnWrap', justify: true, options: [
                            { value: 'no', label: __('No', 'ultimate-post') },
                            { value: 'wrap', label: __('Wrap', 'ultimate-post') }, 
                            { value: 'nowrap', label: __('No Wrap', 'ultimate-post') },
                        ], label:__('Column Wrap','ultimate-post') } 
                    },
                    {
                        position: 9, data: { type:'toggle',key:'reverseCol',label:__('Reverse','ultimate-post')} 
                    }
                ]} initialOpen={true}  store={store}/>
                <CommonSettings title={__('Background & Wrapper','ultimate-post')} include={[
                    {   position: 1, data: { type: 'tab', content: [
                        {  name: 'normal', title: __('Normal', 'ultimate-post'),options:[
                            {   
                                type: 'color2', key: 'colBgImg', image: true, label: __('Background', 'ultimate-post') 
                            }
                        ]},
                        {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                            {   
                                type: 'color2', key: 'columnWrapHoverImg', image: true, label: __('Background', 'ultimate-post') 
                            },
                        ]}
                    ]},
                }
            ]} initialOpen={false}  store={store}/>
            <CommonSettings title={__('Background Overlay','ultimate-post')} include={[
                {   position: 1, data: { type: 'tab', content: [
                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options:[
                        {   
                            type: 'color2', key: 'columnOverlayImg', image: true,  label: __('Background', 'ultimate-post') 
                        },
                        { 
                            type:'range',key:'colOverlayOpacity', min: 0, max: 100, step: 1, responsive: false, unit: false, label:__('Opacity','ultimate-post')
                        },
                        {
                            type:'filter', key: 'columnOverlayilter', label: __('CSS Filter', 'ultimate-post')
                        },
                        {
                            type: 'select', key: 'columnOverlayBlend', options: [
                                { value: '', label: __('Select', 'ultimate-post') },
                                { value: 'normal', label: __('Normal', 'ultimate-post') },
                                { value: 'multiply', label: __('Multiply', 'ultimate-post') },
                                { value: 'screen', label: __('Screen', 'ultimate-post') },
                                { value: 'overlay', label: __('Overlay', 'ultimate-post') },
                                { value: 'darken', label: __('Darken', 'ultimate-post') },
                                { value: 'lighten', label: __('Lighten', 'ultimate-post') },
                                { value: 'color-dodge', label: __('Color Dodge', 'ultimate-post') },
                                { value: 'color-burn', label: __('Color Burn', 'ultimate-post') },
                                { value: 'hard-light', label: __('Hard Light', 'ultimate-post') },
                                { value: 'soft-light', label: __('Soft Light', 'ultimate-post') },
                                { value: 'difference', label: __('Difference', 'ultimate-post') },
                                { value: 'exclusion', label: __('Exclusion', 'ultimate-post') },
                                { value: 'hue', label: __('Hue', 'ultimate-post') },
                                { value: 'saturation', label: __('Saturation', 'ultimate-post') },
                                { value: 'luminosity', label: __('Luminosity', 'ultimate-post') },
                                { value: 'color', label: __('Color', 'ultimate-post') },
                            ], help: 'Notice: Background Color Requierd', label: __('Blend Mode', 'ultimate-post')
                        },
                    ]},
                    {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                        {   
                            type: 'color2', key: 'columnOverlaypHoverImg', image: true, label: __('Background', 'ultimate-post') 
                        },
                        { 
                            type:'range',key:'colOverlayHovOpacity', min: 0, max: 100, step: 1, responsive: false, unit: false, label:__('Opacity','ultimate-post')
                        },
                        {
                            type:'filter', key: 'columnOverlayHoverFilter', label: __('CSS Filter', 'ultimate-post')
                        },
                        { 
                            type: 'select', key: 'columnOverlayHoverBlend', options: [
                                { value: '', label: __('Select', 'ultimate-post') },
                                { value: 'normal', label: __('Normal', 'ultimate-post') },
                                { value: 'multiply', label: __('Multiply', 'ultimate-post') },
                                { value: 'screen', label: __('Screen', 'ultimate-post') },
                                { value: 'overlay', label: __('Overlay', 'ultimate-post') },
                                { value: 'darken', label: __('Darken', 'ultimate-post') },
                                { value: 'lighten', label: __('Lighten', 'ultimate-post') },
                                { value: 'color-dodge', label: __('Color Dodge', 'ultimate-post') },
                                { value: 'color-burn', label: __('Color Burn', 'ultimate-post') },
                                { value: 'hard-light', label: __('Hard Light', 'ultimate-post') },
                                { value: 'soft-light', label: __('Soft Light', 'ultimate-post') },
                                { value: 'difference', label: __('Difference', 'ultimate-post') },
                                { value: 'exclusion', label: __('Exclusion', 'ultimate-post') },
                                { value: 'hue', label: __('Hue', 'ultimate-post') },
                                { value: 'saturation', label: __('Saturation', 'ultimate-post') },
                                { value: 'luminosity', label: __('Luminosity', 'ultimate-post') },
                                { value: 'color', label: __('Color', 'ultimate-post') },
                            ], help: 'Notice: Background Color Requierd', label: __('Blend Mode', 'ultimate-post')
                        },
                    ]}
                ]}}
            ]} initialOpen={false}  store={store}/>
            <CommonSettings title={__('Spacing & Border Style','ultimate-post')} include={[
                { 
                    position: 1, data: { type:'dimension', key:'colWrapMargin',step:1 ,unit:true , responsive:true ,label:__('Margin','ultimate-post') }
                },
                { 
                    position: 2, data: { type:'dimension', key:'colWrapPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }
                },
                {   position: 3, data: { type: 'tab', content: [
                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options:[
                        {  
                            type: 'border', key: 'columnWrapBorder', label: __('Border', 'ultimate-post')
                        },
                        { 
                            type:'dimension', key:'columnWrapRadius', step:1 , unit:true , responsive:true , label:__('Border Radius','ultimate-post') 
                        },
                        { 
                            type:'boxshadow', key:'columnWrapShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post') 
                        },
                    ]},
                    {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                        {  
                            type: 'border', key: 'columnWrapHoverBorder', label: __('Border', 'ultimate-post')
                        },
                        { 
                            type:'dimension', key:'columnWrapHoverRadius', step:1 , unit:true , responsive:true , label:__('Border Radius','ultimate-post') 
                        },
                        { 
                            type:'boxshadow', key:'columnWrapHoverShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post') 
                        },
                    ]}
                ]}}
            ]}  store={store}/>
            <CommonSettings title={__('Column Color','ultimate-post')} include={[
                { 
                    position: 1, data: { type:'color', key:'columnColor',label:__('Color','ultimate-post') }
                },
                { 
                    position: 2, data: { type:'color', key:'colLinkColor',label:__('Link Color','ultimate-post') }
                },
                { 
                    position: 3, data: { type:'color', key:'colLinkHover',label:__('Link Hover Color','ultimate-post') }
                },
                { 
                    position: 4, data: { type:'typography',key:'colTypo',label:__('Typography','ultimate-post') } 
                },
            ]}  store={store}/>
            <CommonSettings 
                depend="columnSticky" 
                title={__('Sticky Column','ultimate-post')} include={[
                { 
                    position: 7, data: { type:'range', key:'columnStickyOffset', min: 0, max: 300, step: 1, unit: ['px', 'rem', 'vh'], responsive: true, help:"Note: Sticky Column Working only Front End", label:__('Gap','ultimate-post') } 
                },
            ]} initialOpen={false}  store={store}/>
        </>
    
    )
}
export default ColumnSettings;