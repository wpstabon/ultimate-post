const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
registerBlockType(
    'ultimate-post/column', {
        title: __('Column','ultimate-post'),
        parent:  ["ultimate-post/row"],
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/column.svg'}/>,
        category: 'ultimate-post',
        description: __('Add Column block for your layout.','ultimate-post'),
        keywords: [ 
            __('Column','ultimate-post'),
            __('Row','ultimate-post')
        ],
        supports: {
            reusable: false,
            html: false,
        },
        attributes,
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/wrapper.svg',
            },
        },
        edit: Edit,
        save: Save,
    }
)