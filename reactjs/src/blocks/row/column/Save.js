const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {

    const { blockId, advanceId, columnEnableLink, columnWrapperLink, columnTargetLink, columnOverlayImg } = props.attributes;

    return (
        <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId}`}>
            <div className={`ultp-column-wrapper`}>
                {
                    columnEnableLink && 
                    <a 
                        className="ultp-column-link" 
                        target={`${columnTargetLink ? '_blank' : '_self' }`} 
                        href={`${columnWrapperLink}`} 
                    />
                }
                {
                    columnOverlayImg.openColor == 1 && 
                    <div className="ultp-column-overlay"/>
                }
                <InnerBlocks.Content />
            </div>
        </div>
    )
}