const attributes = {
    // Using Double Selector For Front And Backend Side
    /* .ultp-row-content > {{ULTP}}  // This prefix for stop applying the css in backend */
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    columnOrderNumber: {
        type: 'string',
        default: '',
        style:[{ 
            selector: 
            `[data-ultp="{{ULTP}}"], 
            .ultp-row-content > {{ULTP}} { order:{{columnOrderNumber}}; }` 
        }],
    },
    columnGutter: {
        type: 'object',
        default: {lg: '0', sm: '0', xs: '0'},
    },
    columnWidth: {
        type: 'object',
        default: {},
        anotherKey: 'columnGutter',
        style: [{ 
            selector: 
            `[data-ultp="{{ULTP}}"], 
            .ultp-row-content > {{ULTP}} { flex-basis: calc({{columnWidth}} - {{columnGutter}}px);}` 
        }],
    },
    columnHeight: {
        type: 'object',
        default: {lg: '', ulg:'px', unit:'px'},
        style: [{ 
            selector:
            `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
            .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { min-height: {{columnHeight}};}`
        }],
    },

    /*==========================
        Flex Property
    ==========================*/
    columnDirection: {
        type: 'string',
        default: 'column',
        style: [
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'row' },
                ],
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { display: flex; flex-direction: {{columnDirection}} }`
            },
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'column' },
                ],
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { display: flex;  flex-direction: column;}`
            },
            {
                depends: [
                    { key:'reverseCol', condition:'==', value: true },
                    { key:'columnDirection', condition:'==', value: 'row' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { display: flex; flex-direction: row-reverse; }`
            },
            {
                depends: [
                    { key:'reverseCol', condition:'==', value: true },
                    { key:'columnDirection', condition:'==', value: 'column' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { display: flex; flex-direction: column-reverse; }`
            },
        ]    
    },
    columnJustify: {
        type: 'object',
        default: { lg: '', },
        style: [
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'row' },
                ],
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { justify-content: {{columnJustify}}; }` 
            },
        ],
    },
    columnAlign: {
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'row' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { align-content: {{columnAlign}}; }` 
            },
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'column' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { justify-content: {{columnAlign}}; }` 
            }
        ],
    },
    columnAlignItems:  {
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'columnDirection', condition:'==', value: 'row' }
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { align-items: {{columnAlignItems}}; }` 
            }
        ],
    },
    columnItemGap: {
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'columnDirection', condition:'==', value: "row" },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper  { gap: {{columnItemGap}}px; }` 
            }
        ],
    },
    columnWrap: {
        type: 'string',
        default: 'wrap',
        style:[
            {
                depends: [
                    { key:'columnWrap', condition:'==', value: 'no' },
                    { key:'columnDirection', condition:'==', value: 'row' },
                ],
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} { flex-wrap: unset; }` 
            },
            {
                depends: [
                    { key:'columnWrap', condition:'==', value: 'wrap' },
                    { key:'columnDirection', condition:'==', value: 'row' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { flex-wrap: {{columnWrap}}; }`
            },
            {
                depends: [
                    { key:'columnWrap', condition:'==', value: 'nowrap' },
                    { key:'columnDirection', condition:'==', value: 'row' },
                ], 
                selector: 
                `{{ULTP}} > .ultp-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { flex-wrap: nowrap; }`
            }
        ],
    },
    reverseCol: {
        type: 'boolean',
        default: false,
    },
    /*==========================
        Background Wrapper
    ==========================*/
    colBgImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-column-wrapper'
        }],
    },

    /*========== Hover Background & Wrapper =========*/
    columnWrapHoverImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-column-wrapper:hover'
        }],
    },

    /*==========================
        Overlay
    ==========================*/
    columnOverlayImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-column-wrapper > .ultp-column-overlay'
        }],
    },
    colOverlayOpacity: {
        type: 'string',
        default: '50',
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper > .ultp-column-overlay { opacity: {{colOverlayOpacity}}%; }'
        }]
    },
    columnOverlayilter: {
        type: 'object',
        default: {openFilter: 0},
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper > .ultp-column-overlay { {{columnOverlayilter}} }'
        }]
    },
    columnOverlayBlend: {
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper > .ultp-column-overlay { mix-blend-mode:{{columnOverlayBlend}}; }'
        }]
    },

    /*========== Hover Overlay =========*/
    columnOverlaypHoverImg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
            selector: '{{ULTP}} > .ultp-column-wrapper:hover > .ultp-column-overlay'
        }],
    },
    colOverlayHovOpacity: {
        type: 'string',
        default: '50',
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper:hover > .ultp-column-overlay { opacity: {{colOverlayHovOpacity}}%; }'
        }]
    },
    columnOverlayHoverFilter: {
        type: 'object',
        default: {openFilter: 0},
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper:hover > .ultp-column-overlay { {{columnOverlayHoverFilter}} }'
        }]
    },
    columnOverlayHoverBlend: {
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper:hover > .ultp-column-overlay { mix-blend-mode:{{columnOverlayHoverBlend}}; }'
        }]
    },
    
    /*==========================
        Spacing & Border Style 
    ==========================*/
    colWrapMargin: {
        type: 'object',
        default: {lg: {top: '',bottom: '', unit:'px'}},
        style:[{ selector:'{{ULTP}} > .ultp-column-wrapper { margin: {{colWrapMargin}}; }' }],
    },
    colWrapPadding: {
        type: 'object',
        default: {lg:{top: '0', bottom: '0', left: '0', right: '0', unit:'px'}},
        style:[{ selector:'{{ULTP}} > .ultp-column-wrapper { padding: {{colWrapPadding}}; }' }],
    },

    /* =========== Border Style =========== */ 
    columnWrapBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#505050' },
        style: [{
            selector:'{{ULTP}} > .ultp-column-wrapper'
        }]
    },
    columnWrapRadius: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid' },
        style: [{
            selector: '{{ULTP}} > .ultp-column-wrapper { border-radius: {{columnWrapRadius}};}',
        }]
    },
    columnWrapShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ 
            selector:'{{ULTP}} > .ultp-column-wrapper'
        }]
    },

    /* =========== Border Hover Style =========== */ 
    columnWrapHoverBorder: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4' },
        style: [{
            selector:
            `[data-ultp="{{ULTP}}"]:hover > {{ULTP}} > .ultp-column-wrapper, 
            .ultp-row-content > {{ULTP}}:hover > .ultp-column-wrapper`
        }]
    },
    columnWrapHoverRadius: {
        type: 'object',
        default: { openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid' },
        style: [{
            selector: 
            `[data-ultp="{{ULTP}}"]:hover > {{ULTP}} > .ultp-column-wrapper, 
            .ultp-row-content > {{ULTP}}:hover > .ultp-column-wrapper { border-radius: {{columnWrapHoverRadius}};}`,
        }]
    },
    columnWrapHoverShadow: {
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ 
            selector: 
            `[data-ultp="{{ULTP}}"]:hover > {{ULTP}} > .ultp-column-wrapper, 
            .ultp-row-content > {{ULTP}}:hover > .ultp-column-wrapper` 
        }],
    },

    /*==========================
        Column Color
    ==========================*/
    columnColor: {
        type: 'string',
        default: '',
        style: [{ selector: '{{ULTP}} { color: {{columnColor}} } ' }]
    },
    colLinkColor: {
        type: 'string',
        default: '',
        style: [{ selector: '{{ULTP}} > .ultp-column-wrapper a { color: {{colLinkColor}} } ' }]
    },
    colLinkHover: {
        type: 'string',
        default: '',
        style: [{ selector:'{{ULTP}} > .ultp-column-wrapper a:hover { color: {{colLinkHover}}; } ' }]
    },
    colTypo: {
        type: 'object',
        default: {openTypography: 0,size: {lg:16, unit:'px'},height: {lg:'', unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [{ selector: '{{ULTP}}' }]
    },

    /*==========================
        Sticky Style
    ==========================*/
    columnSticky: {
        type: 'boolean',
        default: false,
    },
    columnStickyOffset: {
        type: 'object',
        default: {lg: '0', ulg:'px'},
        style: [{
            depends: [
                { key:'columnSticky', condition:'==', value: true },
            ],
            selector: '{{ULTP}} { position: sticky; top: calc( 32px + {{columnStickyOffset}}); align-self: start; }'
        }]
    },
    /*==========================
        Advanced Setting
    ==========================*/
    advanceId:{
        type: 'string',
        default: '',
    },
    advanceZindex:{
        type: 'string',
        default: '',
        style:[{ selector: '{{ULTP}} {z-index:{{advanceZindex}};}' }],
    },
    columnOverflow: {
        type: 'string',
        default: 'visible',
        style: [{
            selector:  
            `.block-editor-block-list__block > {{ULTP}} > .ultp-column-wrapper, 
            .ultp-row-content > {{ULTP}} > .ultp-column-wrapper { overflow: {{columnOverflow}}; }`
        }],
    },
    columnEnableLink: {
        type: 'boolean',
        default: false,
    },
    columnWrapperLink: {
        type: 'string',
        default: 'example.com',
        style:[{
                depends: [
                    { key:'columnEnableLink', condition:'==', value: true },
                ], 
            },
        ]
    },
    columnTargetLink: {
        type: 'boolean',
        default: false,
        style:[{
            depends: [
                { key:'columnEnableLink', condition:'==', value: true },
            ], 
        },
        ]
    },
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{selector: ''}],
    }
}
export default attributes;