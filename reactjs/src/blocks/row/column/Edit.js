const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element
import { CssGenerator } from '../../../helper/CssGenerator'
import { CommonSettings, CustomCssAdvanced, getWindowDocument, ResponsiveAdvanced, isInlineCSS } from '../../../helper/CommonPanel'
import {Sections, Section} from '../../../helper/Sections';
import ColumnSettings from './Setting'
import { Resize } from '../Resize'
import useDevice from '../../../helper/hooks/use-device'
const { select, dispatch } = wp.data;
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {
    const [isResizing, setIsResizing] = useState(false);
    const [tempColWidth, setTempColWidthFunc] = useState("");
    const [device] = useDevice();

    const {
        setAttributes,
        name,
        attributes,
        clientId,
        className,
        toggleSelection,
    } = props;

    const {
        previewImg,
        blockId,
        advanceId,
        columnWidth,
        columnOverlayImg,
        columnGutter,
    } = attributes;

    const store = {
        setAttributes,
        name,
        attributes,
        clientId,
    };

    useEffect(() => {
        const _client = clientId.substr(0, 6)
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );

        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
        
    }, [clientId]);

    
    let __preview_css = ''
    if ( blockId ) { 
        __preview_css = CssGenerator(attributes, 'ultimate-post/column', blockId, isInlineCSS());
    }

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} alt="preview-image" />
    }

    const setValue = (e , dragableWidth) => {
		const _fixedValue = dragableWidth ? 
            parseFloat(parseFloat(dragableWidth).toFixed(2)) 
            :
            parseFloat(parseFloat(e.target.value).toFixed(2));
        
        const diff = _fixedValue > columnWidth.lg ?
            - (_fixedValue - parseFloat(columnWidth.lg)) 
            :
            (parseFloat(columnWidth.lg) - _fixedValue);
        
        if ( diff ) {
            let blocksID = select( 'core/block-editor' ).getNextBlockClientId(clientId);
            if ( !blocksID ) {
                blocksID = select( 'core/block-editor' ).getPreviousBlockClientId(clientId);
            }
            setAttributes({
                columnWidth: Object.assign(
                    {}, columnWidth ,{ lg: _fixedValue }
                )
            });
            const current = select( 'core/block-editor' ).getBlockAttributes( blocksID );
            dispatch( 'core/block-editor' ).updateBlockAttributes(blocksID, 
                { columnWidth: Object.assign(
                        {}, current.columnWidth, {lg: (parseFloat(current.columnWidth.lg) + diff).toFixed(2)}
                    ) 
                }
            );
        }
	}

    const stateHandle = (previousLength , length , position) => {
        const currentColumnParentWidth = getWindowDocument().querySelector(`#block-${clientId}`)?.parentNode?.offsetWidth;
        const newWidth = ( parseFloat(previousLength) + parseFloat( length*100/currentColumnParentWidth ));
        setIsResizing(true);
        setTempColWidthFunc(parseFloat(newWidth).toFixed(2));
    }
    const setColWidth = (previousLength , length , position) => {
        const currentBlock = getWindowDocument().querySelector(`#block-${clientId}`);
        const currentColumnParentWidth = currentBlock?.parentNode?.offsetWidth;
        const nextBlocksID = select( 'core/block-editor' ).getNextBlockClientId(clientId);
        const nextBlock = getWindowDocument().querySelector(`#block-${nextBlocksID}`);
        const newWidth = ( parseFloat(previousLength) + parseFloat( length*100/currentColumnParentWidth ));
        
        setValue('event', newWidth);
        if ( currentBlock && nextBlock ) {
            currentBlock.removeAttribute('style');
            nextBlock.removeAttribute('style');
        }
        setIsResizing(false);
    }

    const setTempColWidth = (currentBlock, nextBlock, nextBlockColWidth, differ, newGutter) => {
        let currentGutter = tempColWidth < 95 ? newGutter : 0;
        let nextGutter = nextBlockColWidth < 95 ? newGutter : 0;
        if ( currentBlock && nextBlock) {
            currentBlock.style.flexBasis = `calc(${tempColWidth}% - ${currentGutter}px)`;
            nextBlock.style.flexBasis = `calc(${(parseFloat(nextBlockColWidth) + differ).toFixed(2)}% - ${nextGutter}px)`;
            
            setColWidthHtml(currentBlock, nextBlock, parseFloat(tempColWidth).toFixed(2), (parseFloat(nextBlockColWidth) + differ).toFixed(2) );
        }
    }
    const setColWidthHtml = (currentBlock, nextBlock , currentBlockWidth, nextBlockWidth) => {
        if ( currentBlock && nextBlock) {
            const currentTooltipNext  = getWindowDocument().querySelector(`#${currentBlock.id} > .wp-block-ultimate-post-column > .ultp-column-wrapper > .ultp-colwidth-next-temp`);
            const currentTooltipPrev  = getWindowDocument().querySelector(`#${currentBlock.id} > .wp-block-ultimate-post-column > .ultp-column-wrapper > .ultp-colwidth-prev-temp`);
            const nextTooltipNext     = getWindowDocument().querySelector(`#${nextBlock.id} > .wp-block-ultimate-post-column > .ultp-column-wrapper > .ultp-colwidth-next-temp`);
            const nextTooltipPrev     = getWindowDocument().querySelector(`#${nextBlock.id} > .wp-block-ultimate-post-column > .ultp-column-wrapper > .ultp-colwidth-prev-temp`);
             
            currentTooltipNext ? currentTooltipNext.innerHTML = `${currentBlockWidth}%` : '';
            currentTooltipPrev ? currentTooltipPrev.innerHTML = `${currentBlockWidth}%` : '';
            nextTooltipNext    ? nextTooltipNext.innerHTML    = `${nextBlockWidth}%` : '';
            nextTooltipPrev    ? nextTooltipPrev.innerHTML    = `${nextBlockWidth}%` : '';
        }
    }

    

    let hasInnerBlocks = select( 'core/block-editor' ).getBlockOrder( clientId ).length > 0;

    const IDs = select( 'core/block-editor' ).getBlockParents(clientId);
    const columnNum = select( 'core/block-editor' ).getBlockAttributes(IDs[IDs.length - 1])?.layout;

    // Gutterspace calc for each column
    const parentBlockAttr = select( 'core/block-editor' ).getBlockAttributes(IDs[IDs.length - 1]);
    const colGap = parentBlockAttr?.columnGap;
    let newGutter = {};
    ['lg', 'sm', 'xs'].forEach((d) => {
        const tempLength = columnNum[d]?.filter(x => x!=100).length;
        newGutter[d] = tempLength ? ( columnWidth[d] < 95 ? ( colGap[d] != 0 && colGap[d] ? (colGap[d]*(tempLength - 1)/tempLength) : 0 ) : 0 ) : 0;  
    });
    
    if ( JSON.stringify(newGutter) != JSON.stringify(columnGutter) ) {
        setAttributes({columnGutter: newGutter});
    }

    // Dragable Column Width
    const currElement = getWindowDocument().querySelector(`#block-${clientId}`);
    if ( currElement ) {
        currElement.setAttribute('data-ultp', `.ultp-block-${blockId}`);
    }
    const currentColumnParentWidth = currElement?.parentNode?.offsetWidth;

    const prevBlocksID = select( 'core/block-editor' ).getPreviousBlockClientId(clientId);
    const nextBlocksID = select( 'core/block-editor' ).getNextBlockClientId(clientId);
    const currentBlock = getWindowDocument().querySelector(`#block-${clientId}`);
    const nextBlock = getWindowDocument().querySelector(`#block-${nextBlocksID}`);
    const nextBlockColWidth = select( 'core/block-editor' ).getBlockAttributes( nextBlocksID )?.columnWidth.lg;
    const differ = tempColWidth > columnWidth.lg ? - (tempColWidth - parseFloat(columnWidth.lg)) : (parseFloat(columnWidth.lg) - tempColWidth);
    let colMaxWidth = 85;

    if ( columnNum?.lg.length == 2 && !prevBlocksID && nextBlocksID ) {
        if ( isResizing ) {
            setTempColWidth(currentBlock, nextBlock, nextBlockColWidth, differ, newGutter[device]);
        }
        else {
            setColWidthHtml(currentBlock, nextBlock, columnWidth.lg, nextBlockColWidth, newGutter[device]);
        }
    }
    else if ( columnNum?.lg.length == 3 ) {
        if ( !prevBlocksID && nextBlocksID ) {
            const lastBlockId = select( 'core/block-editor' ).getNextBlockClientId(nextBlocksID);
            const lastBlockColwidth = lastBlockId ? select( 'core/block-editor' ).getBlockAttributes( lastBlockId )?.columnWidth.lg : 15;
            colMaxWidth = 100 - 15 - parseFloat(lastBlockColwidth);
        }
        else if ( prevBlocksID && nextBlocksID ) {
            const prevCol = select( 'core/block-editor' ).getBlockAttributes( prevBlocksID );
            colMaxWidth = 100 - 15 - parseFloat(prevCol.columnWidth.lg);
        }
        if ( isResizing ) {
            setTempColWidth(currentBlock, nextBlock, nextBlockColWidth, differ, newGutter[device]);
        }
        else {
            setColWidthHtml(currentBlock, nextBlock, columnWidth.lg, nextBlockColWidth, newGutter[device]);
        }
    }
        
    return (
        <Fragment>
            <InspectorControls>
            <Sections>
                <Section slug="setting" title={__('Setting','ultimate-post')}>
                { (device == 'lg' && columnNum?.lg.length > 1 && columnNum?.lg.length < 4) && (prevBlocksID || !prevBlocksID) && nextBlocksID && columnWidth.lg < 100 &&
                    <div className="ultp-field-wrap ultp-field-range ultp-field-columnwidth">
                        <label>Column Width</label>
                        <div className="ultp-range-control">
                            <div className="ultp-range-input">
                                <input
                                    type="range"
                                    min={15}
                                    max={colMaxWidth}
                                    value={columnWidth.lg}
                                    step={0.01}
                                    onChange={e => {
                                        setValue(e);
                                    }}
                                />
                                <input
                                    type="number"
                                    min={15}
                                    max={colMaxWidth}
                                    value={columnWidth.lg}
                                    step={0.01}
                                    onChange={e => {
                                        setValue(e);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                }
                <ColumnSettings store={store}/>
                </Section>    
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={__('General','ultimate-post')} 
                        include={[
                            {
                                position: 1, data: { type:'text',key:'advanceId', label:__('ID','ultimate-post') },
                            },
                            {
                                position: 2, data: { type:'range',key:'advanceZindex',min:-100, max:10000, step:1, label:__('z-index','ultimate-post') }
                            },
                            {
                                position: 3, data: { type:'select',key:'columnOverflow',label:__('Overflow','ultimate-post'), options:[
                                    { value:'visible',label:__('Visible','ultimate-post') },
                                    { value:'hidden',label:__('Hidden','ultimate-post') },
                                ]} 
                            },
                            { 
                                position: 4, data: { type:'toggle',key:'columnEnableLink',label:__('Enable Wrapper Link','ultimate-post')} 
                            },
                            { 
                                position: 5, data: { type:'text',key:'columnWrapperLink',label:__('Url','ultimate-post')} 
                            },
                            { 
                                position: 6, data: { type:'toggle',key:'columnTargetLink',label:__('Enable Target Blank','ultimate-post')} 
                            },
                            ]}  initialOpen={true} store={store}/>
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            </InspectorControls>
            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className} ${hasInnerBlocks?'':'no-inner-block'}`}>
                { __preview_css &&
                    <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                }
                <div className={`ultp-column-wrapper`}>
                    {
                        columnOverlayImg.openColor == 1 && 
                        <div className="ultp-column-overlay"/>
                    }
                    {
                        prevBlocksID && columnWidth.lg != 100 && (device == 'lg' && columnNum?.lg.length > 1 && columnNum?.lg.length < 4) &&
                        <span className="ultp-colwidth-tooltip ultp-colwidth-prev ultp-colwidth-prev-temp">{columnWidth[device]}%</span>
                    }
                    <InnerBlocks
                        templateLock={ false }
                        // renderAppender={hasInnerBlocks ? InnerBlocks.DefaultBlockAppender : InnerBlocks.ButtonBlockAppender}
                        renderAppender={hasInnerBlocks ? undefined : () => <InnerBlocks.ButtonBlockAppender />}
                    />
                    {
                        nextBlocksID && columnWidth.lg != 100 && (device == 'lg' && columnNum?.lg.length > 1 && columnNum?.lg.length < 4) &&
                        <span className="ultp-colwidth-tooltip ultp-colwidth-next ultp-colwidth-next-temp">{columnWidth[device]}%</span>
                    }
                </div>
            </div>
            { (device == 'lg' && columnNum?.lg.length > 1 && columnNum?.lg.length < 4) && (prevBlocksID || !prevBlocksID) && nextBlocksID && columnWidth.lg < 100 &&
                <div className='ultp-resizer-container' style={{width: currentColumnParentWidth}}>
                    <Resize 
                        position="right"
                        setLengthFunc={setColWidth }
                        stateHandle={stateHandle}
                        unit={'%'}
                        previousLength={ columnWidth.lg }
                        toggleSelection={toggleSelection}
                        minWidth={15}
                        maxWidth={ colMaxWidth || 85}
                    />
                </div>
            }
        </Fragment>
    )
}