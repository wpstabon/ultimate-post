const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { Fragment, useEffect } = wp.element
import { bgVideoBg, blockSupportLink, CommonSettings, CustomCssAdvanced, isInlineCSS, ResponsiveAdvanced, updateCurrentPostId } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import useDevice from '../../helper/hooks/use-device'
import { Section, Sections } from '../../helper/Sections'
import { Resize } from './Resize'
import RowSettings from './Setting'
import { renderShapeIcons } from './shapeIcons'
const { Tooltip } = wp.components
const { serialize, parse } = wp.blocks;
const { getBlocksByClientId } = wp.data.select('core/block-editor');
const { removeBlocks, updateBlockAttributes, insertBlocks } = wp.data.dispatch( 'core/block-editor' )
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

const layoutPosition = [
    { lg: [100], sm: [100], xs: [100] },
    { lg: [ 50, 50 ], sm: [ 50, 50 ], xs: [100, 100] },
    { lg: [ 70, 30 ], sm: [ 50, 50 ], xs: [100, 100] },
    { lg: [ 30, 70 ], sm: [ 50, 50 ], xs: [100, 100] },
    { lg: [ 33.33, 33.33, 33.34 ], sm: [ 33.33, 33.33, 33.34 ], xs: [ 100, 100, 100 ] },
    { lg: [ 25, 25, 50 ], sm: [ 25, 25, 50 ], xs: [ 100, 100, 100 ] },
    { lg: [ 50, 25, 25 ], sm: [ 50, 25, 25 ], xs: [ 100, 100, 100 ] },
    { lg: [ 25, 25, 25, 25 ], sm: [ 50, 50, 50, 50 ], xs: [100, 100, 100, 100] },
    { lg: [ 20, 20, 20, 20, 20 ], sm: [ 20, 20, 20, 20, 20 ], xs: [100, 100, 100, 100, 100] },
    { lg: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ], sm: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ], xs: [100, 100, 100, 100, 100, 100] }
];

const rowColumnFields = [
    { lg: [100], sm: [100], xs: [100] },
    { lg: [ 50, 50 ], sm: [ 50, 50 ], xs: [100, 100] },
    { lg: [ 33.33, 33.33, 33.34 ], sm: [ 33.33, 33.33, 33.34 ], xs: [ 100, 100, 100 ] },
    { lg: [ 25, 25, 25, 25 ], sm: [ 50, 50, 50, 50 ], xs: [100, 100, 100, 100] },
    { lg: [ 20, 20, 20, 20, 20 ], sm: [ 20, 20, 20, 20, 20 ], xs: [100, 100, 100, 100, 100] },
    { lg: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ], sm: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ], xs: [100, 100, 100, 100, 100, 100]}
];

export default function Edit(props) {
    const {
        setAttributes,
        name,
        attributes,
        clientId,
        className,
        toggleSelection,
    } = props;

    const {
        blockId,
        currentPostId,
        advanceId,
        HtmlTag,
        rowBtmShape,
        rowTopShape,
        rowBgImg,
        rowOverlayBgImg,
        layout,
        rowTopGradientColor,
        rowBtmGradientColor,
        rowWrapPadding,
        align,

    } = attributes;

    useEffect(() => {
        const _client = clientId.substr(0, 6)
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId );

        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
        
    }, [clientId]);

    const store = {
        setAttributes, 
        name, 
        attributes, 
        clientId 
    }

    const [ device ] = useDevice();

    let __preview_css;
    if ( blockId ) {
        __preview_css = CssGenerator(attributes, 'ultimate-post/row', blockId, isInlineCSS());
    }

    const unit = rowWrapPadding[device]?.unit 
        ? rowWrapPadding[device]?.unit 
        :
         'px';

    if ( !Object.keys(layout).length ) {
        return (
            <div className="ultp-column-structure">
                <div className="ultp-column-structure__heading">Chose a Layout</div>
                <div className="ultp-column-structure__content">
                    { layoutPosition.map((data, i) => {
                        return (
                            <Tooltip key={i} delay={0} visible={true} position={'bottom'} text={`${data.lg.join(" : ")}`}>
                                <div onClick={()=> setAttributes({layout: data})}>
                                    { data.lg.map( (e, _i) => <div key={_i} style={{width: `calc(${e}% - 15px)`}}></div>) }
                                </div>
                            </Tooltip>
                        )})
                    }
                </div>
            </div>
        )
    }
    const rowBgPaddingClass = rowBgImg?.openColor ? 
        ( 
            ' ultpBgPadding' + 
            (!rowWrapPadding?.lg?.left ? ' lgL' : '') + 
            (!rowWrapPadding?.lg?.right ? ' lgR' : '') + 
            (!rowWrapPadding?.sm?.left ? ' smL' : '') + 
            (!rowWrapPadding?.sm?.right ? ' smR' : '') + 
            (!rowWrapPadding?.xs?.left ? ' xsL' : '') + 
            (!rowWrapPadding?.xs?.right ? ' xsR' : '') 
        ) 
        : 
        '';

    const getColumnLayout = (layout) => {
        return layout.lg.map((e, key) => {
            return [
                "ultimate-post/column", 
                { 
                    columnWidth: {
                        lg: layout.lg[key],
                        sm: layout.sm[key],
                        xs: layout.xs[key],
                        unit: '%'
                    }
                }
            ];
        });
    }
    
    const setPadding = (previousLength, height, position) => {
        const def = Object.assign({}, rowWrapPadding[device], {[position]: previousLength + parseInt(height)})
        setAttributes({
            rowWrapPadding: Object.assign(
                {}, 
                rowWrapPadding, 
                {
                    [device]: def 
                }
            )
        });
    }

    return (
        <Fragment>
            <InspectorControls>
            <Sections>
                <Section slug="setting" title={__('Style','ultimate-post')}>
                    <div className="ultp-field-wrap ultp-field-tag ultp-row-control">
                        <label>{__('Row Column','ultimate-post')}</label>
                        <div className="ultp-sub-field">
                            {rowColumnFields.map( (el, i) => {
                                return <span key={i} className={`ultp-tag-button` + (el.lg.length == layout.lg.length ? ` active`: '')} onClick={() => {
                                    setAttributes({layout: el})
                                    const { innerBlocks } = getBlocksByClientId(clientId)[0] || [];
                                    if (innerBlocks.length > el.lg.length) {
                                        Array(innerBlocks.length - el.lg.length).fill(1).forEach((e, __i) => {
                                            innerBlocks[innerBlocks.length - (__i + 1)].innerBlocks.forEach((elem, k) => {
                                                const serializedBlock = serialize(getBlocksByClientId( elem.clientId ));
                                                insertBlocks(parse(serializedBlock), 0, innerBlocks[el.lg.length - 1].clientId, false);
                                            });
                                            removeBlocks(innerBlocks[ innerBlocks.length - (__i+1) ].clientId, false);
                                        })
                                    }
                                    for (let _i = 0; _i < el.lg.length; _i++) {
                                        const width = {lg: el.lg[_i], sm: el.sm[_i], xs: el.xs[_i], unit: '%'}
                                        if (innerBlocks.length >= _i+1) {
                                            updateBlockAttributes(innerBlocks[_i].clientId, { columnWidth: width })
                                        } else {
                                            const newBlock  = wp.blocks.createBlock('ultimate-post/column', { columnWidth: width })
                                            insertBlocks(newBlock ,_i, clientId, false )
                                        }
                                    }
                                }}>{i+1}</span>
                            })}
                        </div>
                    </div><br/>
                    <RowSettings clientId={clientId} layout={layout} store={store} />
                </Section>    
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={__('General','ultimate-post')} include={[
                        {position: 1, data: { type:'text',key:'advanceId', label:__('ID','ultimate-post') }},
                        {position: 2, data: { type:'range',key:'advanceZindex',min:-100, max:10000, step:1, label:__('z-index','ultimate-post') }},
                        {
                            position: 3, data: { type:'select', key:'contentHeightType',label:__('Height Type','ultimate-post'), options:[
                                { value:'normal',label:__('Normal','ultimate-post') },
                                { value:'windowHeight',label:__('Window Height','ultimate-post') },
                                { value:'custom',label:__('Min Height','ultimate-post') },
                            ]} 
                        },
                        {
                            position: 4, data: { type:'range', key:'contentHeight', min: 0, max: 1500, step: 1, responsive: true, unit: true, label:__('Min Height','ultimate-post') } 
                        },
                        {
                            position: 5, data: { type:'group', key:'contentOverflow',justify:true,label:__('Overflow','ultimate-post'), options:[
                                { value:'visible',label:__('Visible','ultimate-post') },
                                { value:'hidden',label:__('Hidden','ultimate-post') },
                            ]} 
                        },
                        {
                            position: 6, data: { type:'select',key:'HtmlTag', block:'column', beside: true, label:__('Html Tag','ultimate-post'), options:[
                                { value:'div',label:__('div','ultimate-post') },
                                { value:'section',label:__('Section','ultimate-post') },
                                { value:'header',label:__('header','ultimate-post') },
                                { value:'footer',label:__('footer','ultimate-post') },
                                { value:'aside',label:__('aside','ultimate-post') },
                                { value:'main',label:__('main','ultimate-post') },
                                { value:'article',label:__('article','ultimate-post') },
                            ]} 
                        },
                        {
                            position: 7, data: { type:'toggle',key:'enableRowLink',label:__('Enable Wrapper Link','ultimate-post')} 
                        },
                        {
                            position: 8, data: { type:'text',key:'rowWrapperLink',label:__('Wrapper Link','ultimate-post')} 
                        },
                        {
                            position: 9, data: { type:'toggle',key:'enableNewTab',label:__('Enable Target Blank','ultimate-post')} 
                        },
                    ]}  initialOpen={true} store={store}/>
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            { blockSupportLink() }
            </InspectorControls>
            <HtmlTag {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className} ${align ? `align${align}` : ''} ${rowBgPaddingClass}`}>
                { __preview_css &&
                    <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                }
                <div className={`ultp-row-wrapper`}>
                    <Resize 
                        position="top"
                        setLengthFunc={setPadding }
                        unit={unit}
                        previousLength={parseInt(rowWrapPadding[device]?.top ? rowWrapPadding[device]?.top : 0)}
                        toggleSelection={toggleSelection}
                    />
                    { (Object.keys(rowBgImg).length > 0 && rowBgImg.openColor == 1 && rowBgImg.type == "video" && rowBgImg.video) &&
                        bgVideoBg(rowBgImg.video, rowBgImg.loop||0, rowBgImg.start||'', rowBgImg.end||'', rowBgImg.fallback||'')
                    }
                    { (rowTopShape && rowTopShape != 'empty') &&
                        <div className="ultp-row-shape ultp-shape-top">
                            {renderShapeIcons(rowTopShape, blockId, 'top', rowTopGradientColor)}
                        </div>
                    }
                    { rowOverlayBgImg.openColor == 1 &&
                        <div className="ultp-row-overlay"></div>
                    }
                    { (rowBtmShape && rowBtmShape != 'empty') &&
                        <div className="ultp-row-shape ultp-shape-bottom">
                            {renderShapeIcons(rowBtmShape, blockId, 'bottom', rowBtmGradientColor)}
                        </div>
                    }
                    <InnerBlocks
                        renderAppender={ false }
                        template={getColumnLayout(layout)}
                        // templateLock={'all'}
                        allowedBlocks={['ultimate-post/column']}
                    />
                    <Resize 
                        position="bottom"
                        setLengthFunc={setPadding }
                        unit={unit}
                        previousLength={parseInt(rowWrapPadding[device]?.bottom ? rowWrapPadding[device]?.bottom : 0)}
                        toggleSelection={toggleSelection}
                    />
                </div>

            </HtmlTag>
        </Fragment>
    )
}
