import { bgVideoBg } from "../../helper/CommonPanel";
import { renderShapeIcons } from "./shapeIcons";
const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { 
        blockId, 
        advanceId,
        HtmlTag,
        rowTopShape, 
        rowBtmShape, 
        enableRowLink, 
        rowWrapperLink, 
        enableNewTab, 
        rowBgImg, 
        rowOverlayBgImg, 
        rowTopGradientColor, 
        rowBtmGradientColor, 
        align, 
        rowWrapPadding,
        rowStickyPosition
    } = props.attributes;

    const rowBgPaddingClass = rowBgImg?.openColor ? 
        ( 
            ' ultpBgPadding' + 
            (!rowWrapPadding?.lg?.left ? ' lgL' : '') + 
            (!rowWrapPadding?.lg?.right ? ' lgR' : '') + 
            (!rowWrapPadding?.sm?.left ? ' smL' : '') + 
            (!rowWrapPadding?.sm?.right ? ' smR' : '') + 
            (!rowWrapPadding?.xs?.left ? ' xsL' : '') + 
            (!rowWrapPadding?.xs?.right ? ' xsR' : '') 
        ) 
        : 
        '';
    
    let rowStickyClass = "";
    if ( ['row_sticky', 'row_scrollToStickyTop'].includes(rowStickyPosition) ) {
        rowStickyClass = ` row_sticky_active ${rowStickyPosition}`;
    }

    return (
        <HtmlTag {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${align ? `align${align}` : ``} ${rowBgPaddingClass}${rowStickyClass}`}>
            <div className="ultp-row-wrapper">
                { (Object.keys(rowBgImg).length > 0 && rowBgImg.openColor == 1 && rowBgImg.type == "video" && rowBgImg.video) &&
                    bgVideoBg(rowBgImg.video, rowBgImg.loop||0, rowBgImg.start||'', rowBgImg.end||'', rowBgImg.fallback||'')
                }
                { (rowTopShape && rowTopShape != 'empty') &&
                    <div className="ultp-row-shape ultp-shape-top">
                        { renderShapeIcons(rowTopShape, blockId, 'top', rowTopGradientColor) }
                    </div>
                }
                { rowOverlayBgImg.openColor == 1 &&
                    <div className="ultp-row-overlay"></div>
                }
                { enableRowLink && 
                    <a href={ rowWrapperLink } target={ enableNewTab && '_blank' } className="ultp-rowwrap-url"></a>
                }
                { (rowBtmShape && rowBtmShape != "empty") &&
                    <div className="ultp-row-shape ultp-shape-bottom">
                        {renderShapeIcons(rowBtmShape, blockId, 'bottom', rowBtmGradientColor) }
                    </div>
                }
                <div className="ultp-row-content">
                    <InnerBlocks.Content />
                </div>
            </div>
        </HtmlTag>
    )
}