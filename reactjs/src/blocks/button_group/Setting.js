const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const ButtonsSetting = ({ store }) => {
    const { btnAnimation, btnVeticalPosition } = store.attributes;

    let helpMessage = '';
    switch (btnAnimation) {
        case 'style1':
            helpMessage = "Control The Background From Button Background";
            break;
        case 'style2':
            helpMessage = "Control The Background From Button Background";
            break;
        case 'style3':
                helpMessage = "Control The Box-shadow From Button Background";
            break;
        case 'style4':
            helpMessage = "Control The Box-shadow From Button Background";
            break;
    }

    let verticalIcon = ['juststart', 'justcenter', 'justend', 'justbetween', 'justaround', 'justevenly'];

    if(btnVeticalPosition){
        verticalIcon = ['juststart', 'justcenter', 'justend'];
    }
    
    return (
        <>
            <TemplateModal
                prev="https://www.wpxpo.com/postx/blocks/#demoid7952"  
                store={store}
            />
            <Sections>
                <Section slug="global" title={__('Global Style','ultimate-post')}>
                    <CommonSettings title={`inline`} include={[
                        {
                            position: 1, data: { type:'range',key:'btnItemSpace', min: 0, max: 300, step: 1, responsive: true, label:__('Button Gap','ultimate-post') } 
                        },
                        { 
                            position: 2, data: { type: 'alignment', key: 'btnJustifyAlignment', icons: verticalIcon, options:['flex-start', 'center', 'flex-end','space-between', 'space-around', 'space-evenly'], label: __('Horizontal Alignment', 'ultimate-post') } 
                        },
                        {
                            position: 3, data: { type: 'alignment', block: 'button', key: 'btnVeticalAlignment', disableJustify: true, icons: ['algnStart', 'algnCenter', 'algnEnd', 'stretch'], options:['flex-start', 'center', 'flex-end', 'stretch'], label: __('Vertical Alignment', 'ultimate-post') }
                        },
                        {
                            position: 4, data: { type:'toggle',key:'btnVeticalPosition',label:__('Vertical Alignment','ultimate-post')} 
                        },
                        {
                            position: 5, data: { type:'dimension', key:'btnWrapMargin',step:1 ,unit:true , responsive:true ,label:__('Margin','ultimate-post') }
                        },
                    ]} initialOpen={true}  store={store}/>
                    <CommonSettings title={__('Button Animation','ultimate-post')} include={[
                        {
                            position: 1, data: { type:'select',key:'btnAnimation',label:__('Button Aniation','ultimate-post'), options:[
                                {value:'none',label:__('None','ultimate-post')},
                                {value:'style1',label:__('Style 1','ultimate-post')},
                                {value:'style2',label:__('Style 2','ultimate-post')},
                                {value:'style3',label:__('Style 3','ultimate-post')},
                                {value:'style4',label:__('Style 4','ultimate-post')},
                            ], help: helpMessage}
                        },
                        {
                            position: 2, data: { type:'range', key:'btnTranslate', min: -20, max: 20, step: .5, responsive: true, label:__('Button Transform','ultimate-post'), unit: ['px'], help: 'Control Button Position'}
                        }
                    ]} initialOpen={false}  store={store}/>
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={'inline'} include={[
                        {
                            position: 1, data: { type:'text',key:'advanceId', label:__('ID','ultimate-post') }
                        },
                        {
                            position: 2, data: { type:'range',key:'advanceZindex',min:-100, max:10000, step:1, label:__('z-index','ultimate-post') },
                        }
                    ]} initialOpen={true}  store={store}/>
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            { blockSupportLink() }
        </>
    );
};

export default ButtonsSetting;