const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    currentPostId: { type: "string" , default: "" },
    
    /*==========================
        Global Style
    ==========================*/
    btnJustifyAlignment: {
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'btnVeticalPosition', condition:'==', value: false },
                ],
                selector:
                `{{ULTP}} .ultp-button-wrapper .block-editor-block-list__layout,
                {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { justify-content: {{btnJustifyAlignment}}; }`
            },
            {
                depends: [
                    { key:'btnVeticalPosition', condition:'==', value: true },
                ],
                selector:
                `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
                {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { align-items: {{btnJustifyAlignment}}; }`
            }
        ]
    },
    btnVeticalAlignment: {
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'btnVeticalPosition', condition:'==', value: false },
                ],
                selector:
                `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
                {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { align-items: {{btnVeticalAlignment}}; }
                {{ULTP}} .ultp-button-wrapper .wp-block-ultimate-post-button { height: 100%; }`
            }
        ]
    },
    btnVeticalPosition: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'btnVeticalPosition', condition:'==', value: true },
                ],
                selector:
                `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
                {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { flex-direction: column; }`
            },
            {
                depends: [
                    { key:'btnVeticalPosition', condition:'==', value: false },
                ],
                selector:
                `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
                {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { flex-direction: row; }`
            }
        ]
    },
    btnItemSpace: {
        type: 'object',
        default: {lg:'28', unit:'px'},
        style: [{
            selector:
            `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { gap:{{btnItemSpace}};  }`
        }]
    },
    btnWrapMargin:{
        type: 'object',
        default: {lg:{top: '',bottom: '',left: '', right: '', unit:'px'}},
        style: [{
            selector:
            `{{ULTP}} .ultp-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-button-wrapper.ultp-button-frontend { margin:{{btnWrapMargin}};  }`
        }]
    },

    /*==========================
        Title Animation
    ==========================*/
    btnAnimation:{
        type: 'string',
        default: 'none',
        style: [
            {
                depends: [
                    { key:'btnAnimation', condition:'!=', value: 'style3' },
                ],
            },
            {
                depends: [
                    { key:'btnAnimation', condition:'==', value: 'style3' },
                ],
                selector:'{{ULTP}} .ultp-button-wrapper.ultp-anim-style3 .wp-block-ultimate-post-button:hover { box-shadow: none; }'
            },
        ]
    },
    btnTranslate: {
        type: 'object',
        default: {lg:'-5', unit:'px'},
        style: [
            {
                depends: [
                    { key:'btnAnimation', condition:'==', value: 'style4' },
                ],
            selector:'{{ULTP}} .wp-block-ultimate-post-button:hover { transform: translate( {{btnTranslate}}, {{btnTranslate}});  }'
            },
        ]
    },
    
    /*==========================
        General Advance Settings
    ==========================*/
    advanceId:{
        type: 'string',
        default: '',
    },
    advanceZindex:{
        type: 'string',
        default: '',
        style:[{ selector: '{{ULTP}} .ultp-button-wrapper {z-index: {{advanceZindex}}; }' }],
    },
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{
            selector: ''}],
    }
}
export default attributes;