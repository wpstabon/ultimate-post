const { Component } = wp.element;
const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
	const { blockId, advanceId, btnAnimation } = props.attributes;
	return (
		<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId}`}>
			<div className={`ultp-button-wrapper ultp-button-frontend ultp-anim-${btnAnimation}`}>
				<InnerBlocks.Content />
			</div>
		</div>
	)
}