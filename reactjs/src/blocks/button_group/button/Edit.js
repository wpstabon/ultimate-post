const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element;
const { createBlock } = wp.blocks;
const { RichText } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator'
import SingleButtonSetting from './Setting';
import IconPack from '../../../helper/fields/tools/IconPack';
import ToolBarElement from '../../../helper/ToolBarElement'
import { isInlineCSS } from '../../../helper/CommonPanel'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, name, className, attributes, clientId, attributes: { blockId, advanceId, layout, btnIconEnable, btntextEnable, btnText, btnIcon, btnLink, dcEnabled } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/button', blockId, isInlineCSS());
	}

	return (
		<Fragment>
			<InspectorControls>
				<SingleButtonSetting store={store} />
			</InspectorControls>
			<ToolBarElement
				include={[
					{
						type: 'layout', block: 'button', key: 'layout', label: __('Style', 'ultimate-post'), options: [
							{ img: 'assets/img/layouts/button/layout1.svg', label: __('Layout 1', 'ultimate-post'), value: 'layout1' },
							{ img: 'assets/img/layouts/button/layout2.svg', label: __('Layout 2', 'ultimate-post'), value: 'layout2' },
							{ img: 'assets/img/layouts/button/layout3.svg', label: __('Layout 3', 'ultimate-post'), value: 'layout3' },
							{ img: 'assets/img/layouts/button/layout4.svg', label: __('Layout 4', 'ultimate-post'), value: 'layout4' },
						]
					},
					{
						type: 'linkbutton', key: 'btnLink', onlyLink: true, value: btnLink, placeholder: 'Enter Button URL', label: __('Button Link', 'ultimate-post')
					}
				]}
				store={store} />
			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className} ultp-button-${layout}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				{
					btnIconEnable &&
					<div className="ultp-btnIcon-wrap">
						{IconPack[btnIcon]}
					</div>
				}
				{
					btntextEnable &&
					<RichText
						key="editable"
						tagName={'div'}
						className={'ultp-button-text'}
						keepPlaceholderOnFocus
						allowedFormats={['ultimate-post/dynamic-content']}
						placeholder={__('Click Here...', 'ultimate-post')}
						onChange={value => setAttributes({ btnText: value })}
						onReplace={(blocks, indexToSelect, initialPosition) =>
							wp.data.dispatch('core/block-editor').replaceBlocks(clientId, blocks, indexToSelect, initialPosition)
						}
						onSplit={(value, isAfterOriginal) =>
							createBlock('ultimate-post/button', { ...attributes, btnText: value })
						}
						value={btnText} />
				}
			</div>
		</Fragment>
	)
}