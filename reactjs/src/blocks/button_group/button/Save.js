import { isDCActive } from '../../../helper/dynamic_content';
import IconPack from '../../../helper/fields/tools/IconPack';

export default function Save(props) {
	const {
		blockId,
		advanceId,
		layout,
		btnIconEnable,
		btntextEnable,
		btnText,
		btnIcon,
		btnLink,
		btnLinkTarget,
		btnLinkDownload,
		btnLinkNoFollow,
		btnLinkSponsored,
		dcEnabled,
	} = props.attributes;

	let linkRelation = 'noopener';
	linkRelation += btnLinkNoFollow ? ' nofollow' : '';
	linkRelation += btnLinkSponsored ? ' sponsored' : '';

	const anchorProps = {}

	if (isDCActive() && dcEnabled) {
		anchorProps['href'] = "#";
		anchorProps['target'] = btnLinkTarget;
	} else if (btnLink.url) {
		anchorProps['href'] = btnLink.url;
		anchorProps['target'] = btnLinkTarget;
	}

	return (
		<a
			{...(advanceId && { id: advanceId })}
			className={`ultp-block-${blockId} ultp-button-${layout}`}

			{...anchorProps}

			rel={linkRelation}
			download={btnLinkDownload && 'download'}
		>
			{btnIconEnable && (
				<div className="ultp-btnIcon-wrap">{IconPack[btnIcon]}</div>
			)}
			{btntextEnable && (
				<div
					className="ultp-button-text"
					dangerouslySetInnerHTML={{ __html: btnText }}
				/>
			)}
		</a>
	);
}