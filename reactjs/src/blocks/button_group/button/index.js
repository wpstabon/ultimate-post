const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import { dynamicRichTextAttributes } from '../../../helper/dynamic_content';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType(
    'ultimate-post/button', {
        title: __('Button','ultimate-post'),
        parent:  ["ultimate-post/button-group"],
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/button.svg'} alt="button svg"/>,
        category: 'ultimate-post',
        description: __('Create & customize button','ultimate-post'),
        keywords: [ 
            __('button','ultimate-post'),
            __('single button','ultimate-post'),
            __('btn','ultimate-post')
        ],
        attributes,
        supports: {
            reusable: false,
            html: false,
        },
        edit: Edit,
        save: Save,
    }
)