import { dynamicRichTextAttributes } from "../../../helper/dynamic_content";

const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    layout:{
        type: 'string',
        default: 'layout1',
    },

    /*==========================
        Text Content Settings
    ==========================*/
    btnText:{
        type: 'string',
        default: '',
    },
    btntextEnable:{
        type: 'boolean',
        default: true,
    },
    btnTextTypo: {
        type: 'object',
        default: {openTypography: 1, size: {lg:16, unit:'px'}, height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight: 500},
        style: [{ selector: '{{ULTP}} .ultp-button-text' }]
    },
    btnTextColor:{
        type: 'string',
        default: '',
        style: [{selector: '{{ULTP}} .ultp-button-text { color:{{btnTextColor}} } '}]
    },
    btnTextHover:{
        type: 'string',
        default: '',
        style: [{selector: '{{ULTP}}:hover .ultp-button-text { color:{{btnTextHover}} } '}]
    },
    btnLink:{
        type: 'object',
        default: '',
    },
    btnLinkTarget:{
        type: 'string',
        default: '_self',
    },
    btnLinkNoFollow:{
        type: 'boolean',
        default: false,
    },
    btnLinkSponsored:{
        type: 'boolean',
        default: false,
    },
    btnLinkDownload:{
        type: 'boolean',
        default: false,
    },
    
    /*==========================
        Background Settings
    ==========================*/
    btnSize:{
        type: 'string',
        default: 'custom',
        style: [
            {
                depends: [
                    { key:'btnSize', condition:'==', value: 'custom' },
                ],
            },
            {
                depends: [
                    { key:'btnSize', condition:'==', value: 'xl' },
                ],
                selector: '{{ULTP}} { padding: 20px 40px !important; }'
            },
            {
                depends: [
                    { key:'btnSize', condition:'==', value: 'lg' },
                ],
                selector: '{{ULTP}} { padding: 10px 20px !important; }'
            },
            {
                depends: [
                    { key:'btnSize', condition:'==', value: 'md' },
                ],
                selector: '{{ULTP}} { padding: 8px 16px !important; }'
            },
            {
                depends: [
                    { key:'btnSize', condition:'==', value: 'sm' },
                ],
                selector: '{{ULTP}} { padding: 6px 12px !important; }'
            },
        ]
    },
    btnBgCustomSize:{
        type: 'object',
        default: {lg:{top: '15',bottom: '15',left: '30', right: '30', unit:'px'}},
        style:[
            {   
                depends: [
                    { key:'btnSize', condition:'==', value: 'custom' },
                ],
                selector: '{{ULTP}} { padding:{{btnBgCustomSize}} !important; }' 
            }
        ],
    },
    btnMargin:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style:[{ selector: '{{ULTP}} { margin:{{btnMargin}}; }' }],
    },
    btnBgColor: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '', gradient: {}},
        style: [{
            selector:
            `.ultp-anim-none {{ULTP}}.wp-block-ultimate-post-button,
            .ultp-anim-style1 {{ULTP}}.wp-block-ultimate-post-button, 
            .ultp-anim-style2 {{ULTP}}.wp-block-ultimate-post-button:before, 
            .ultp-anim-style3 {{ULTP}}.wp-block-ultimate-post-button, 
            .ultp-anim-style4 {{ULTP}}.wp-block-ultimate-post-button`
        }]
    },
    btnBorder:{
        type: 'object',
        default: {openBorder: 0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper 
            {{ULTP}}.wp-block-ultimate-post-button` 
        }],
    },
    btnRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style:[{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper 
            {{ULTP}}.wp-block-ultimate-post-button { border-radius:{{btnRadius}}; }` 
        }],
    },
    btnShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 3, right: 3, bottom: 0, left: 0},color: '#FFB714'},
        style: [{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper 
            {{ULTP}}.wp-block-ultimate-post-button` 
        }],
    },

    //Hover
    btnBgHover: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '', gradient: {}},
        style: [{selector:
            `.ultp-anim-none {{ULTP}}.wp-block-ultimate-post-button:before, 
            .ultp-anim-style1 {{ULTP}}.wp-block-ultimate-post-button:before, 
            .ultp-anim-style2 {{ULTP}}.wp-block-ultimate-post-button, 
            .ultp-anim-style3 {{ULTP}}.wp-block-ultimate-post-button:before, 
            .ultp-anim-style4 {{ULTP}}.wp-block-ultimate-post-button:before`}]
    },
    btnHoverBorder:{
        type: 'object',
        default: {openBorder: 0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper 
            {{ULTP}}.wp-block-ultimate-post-button:hover` 
        }],
    },
    btnHoverRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style:[{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper {{ULTP}}.wp-block-ultimate-post-button:hover { border-radius:{{btnHoverRadius}}; }` 
        }],
    },
    btnHoverShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 3, right: 3, bottom: 0, left: 0},color: '#FFB714'},
        style:[{ 
            selector: 
            `.wp-block-ultimate-post-button-group .ultp-button-wrapper {{ULTP}}.wp-block-ultimate-post-button:hover` 
        }],
    },
    
    /*==========================
        Icon Settings
    ==========================*/
    btnIconEnable:{
        type: 'boolean',
        default: false,              
    },
    btnIcon: {
        type: 'string',
        default: 'arrow_right_circle_line'
    },
    btnIconPosition: {
        type: 'boolean',
        default: false,
        style:[
            { 
                depends: [
                    { key:'btnIconPosition', condition:'==', value: false },
                ],
                selector: '{{ULTP}} { flex-direction: row }' 
            },
            { 
                depends: [
                    { key:'btnIconPosition', condition:'==', value: true },
                ],
                selector: '{{ULTP}} { flex-direction: row-reverse }' 
            }
        ],
    },
    btnIconSize: {
        type: 'object',
        default: {lg:'17', unit:'px'},
        style:[{ 
            selector: '{{ULTP}} .ultp-btnIcon-wrap svg { height: {{btnIconSize}}; width: {{btnIconSize}}; }' 
        }], 
    },
    btnIconGap: {
        type: 'object',
        default: {lg:'12', unit:'px'},
        style:[{ selector: '{{ULTP}} { gap: {{btnIconGap}}; }' }], 
    },
    btnIconColor:{
        type: 'string',
        default: '#fff',
        style: [{ selector: '{{ULTP}} > .ultp-btnIcon-wrap svg { fill:{{btnIconColor}}; } ' }]
    },
    btnIconHoverColor:{
        type: 'string',
        default: '#f2f2f2',
        style: [{
            selector: '{{ULTP}}:hover > .ultp-btnIcon-wrap svg { fill:{{btnIconHoverColor}}; }'
        }]
    },

    /*==========================
        General Advance Settings
    ==========================*/
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has( > {{ULTP}}) {display:none;}` }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has( > {{ULTP}}) {display:none;}` }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has( > {{ULTP}}) {display:none;}` }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{selector: ''}],
    },

    /*==========================
        Dynamic Content
    ==========================*/
    ...dynamicRichTextAttributes,
}

export default attributes;