const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../../helper/CommonPanel';
import { Section, Sections } from '../../../helper/Sections';

const SingleButtonSetting = ({ store }) => {
    return (
        <Sections>
            <Section slug="button" title={__('Button','ultimate-post')}>
                <CommonSettings title={`inline`} include={[
                    { position: 1, data:{ type:'layout', block:'button', key:'layout', label:__('Style','ultimate-post'),options: [
                        { img: 'assets/img/layouts/button/layout1.svg', label: __('Layout 1','ultimate-post'), value: 'layout1' },
                        { img: 'assets/img/layouts/button/layout2.svg', label: __('Layout 2','ultimate-post'), value: 'layout2' },
                        { img: 'assets/img/layouts/button/layout3.svg', label: __('Layout 3','ultimate-post'), value: 'layout3' },
                        { img: 'assets/img/layouts/button/layout4.svg', label: __('Layout 4','ultimate-post'), value: 'layout4' },
                    ]}},
                    {
                        position: 2, data: { type:'select',key:'btnSize',label:__('Button Size','ultimate-post'), options:[
                            { value:'sm',label:__('Small','ultimate-post') },
                            { value:'md',label:__('Medium','ultimate-post') },
                            { value:'lg',label:__('Large','ultimate-post') },
                            { value:'xl',label:__('Extra Large','ultimate-post') },
                            { value:'custom',label:__('Custom','ultimate-post') },
                        ]}
                    },
                    {
                        position: 3, data: { type:'dimension', key:'btnBgCustomSize',step:1 ,unit:true , responsive:true ,label:__('Button Custom Size','ultimate-post') }
                    },
                    {
                        position: 4, data: { type:'linkbutton',key:'btnLink', onlyLink: true, placeholder: 'Enter Button URL', label:__('Button Link', 'ultimate-post') }
                    },
                ]} initialOpen={true}  store={store}/>
                <CommonSettings 
                    title={__('Text','ultimate-post')} 
                    depend="btntextEnable" 
                    include={[
                        {
                            position: 1, data: { type:'typography',key:'btnTextTypo',label:__('Text Typography','ultimate-post')}
                        },
                        {
                            position: 2, data: { type:'color', key:'btnTextColor',label:__('Text Color','ultimate-post')}
                        },
                        {
                            position: 3, data: { type:'color', key:'btnTextHover',label:__('Text Hover Color','ultimate-post')}
                        },
                    ]} initialOpen={false}  store={store}/>
                <CommonSettings title={__('Background','ultimate-post')} include={[
                    {
                        position: 1, data: { type:'dimension', key:'btnMargin',step:1 ,unit:true , responsive:true ,label:__('Margin','ultimate-post') }
                    },
                    {
                        position: 2, data: { type: 'tab', content: [
                            {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                {   
                                    type: 'color2', key: 'btnBgColor', label: __('Background Color', 'ultimate-post') 
                                },
                                {  
                                    type: 'border', key: 'btnBorder', label: __('Border', 'ultimate-post')
                                },
                                { 
                                    type:'dimension', key:'btnRadius', step:1 , unit:true , responsive:true , label:__('Border Radius','ultimate-post') 
                                },
                                { 
                                    type:'boxshadow', key:'btnShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post') 
                                },
                            ]
                        },
                        {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [ 
                            {   
                                type: 'color2', key: 'btnBgHover', label: __('Background Color', 'ultimate-post') 
                            },
                            {  
                                type: 'border', key: 'btnHoverBorder', label: __('Border', 'ultimate-post')
                            },
                            { 
                                type:'dimension', key:'btnHoverRadius', step:1 , unit:true , responsive:true , label:__('Border Radius','ultimate-post') 
                            },
                            { 
                                type:'boxshadow', key:'btnHoverShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post') 
                            },
                        ]}
                    ]}}
                ]} initialOpen={false}  store={store}/>
                <CommonSettings 
                    title={__('Icon','ultimate-post')}
                    depend="btnIconEnable" 
                    include={[
                        
                        {
                            position: 1, data: { type:'icon', key:'btnIcon',label:__('Icon Store','ultimate-post')} 
                        },
                        {
                            position: 2, data: { type:'toggle',key:'btnIconPosition',label:__('Icon After Text','ultimate-post')} 
                        },
                        {
                            position: 3, data: { type:'range',key:'btnIconSize', min: 0, max: 300, step: 1, responsive: true, label:__('Icon Size','ultimate-post') } 
                        },
                        {
                            position: 4, data: { type:'range', key:'btnIconGap', min: 0, max: 300, step: 1, responsive: true, label:__('Space Between Icon and Text','ultimate-post')}
                        },
                        {
                            position: 5, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                    {
                                        type:'color', key:'btnIconColor',label:__('Icon Color','ultimate-post')
                                    },
                                ]
                            },
                            {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [ 
                                {
                                    type:'color', key:'btnIconHoverColor',label:__('Icon Color','ultimate-post')
                                },
                            ]}
                        ]}},
                    ]} initialOpen={false}  store={store}/>
            </Section>
            <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                <ResponsiveAdvanced store={store}/>
                <CustomCssAdvanced store={store}/>
            </Section>
        </Sections>
    );
};

export default SingleButtonSetting;