const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/button-block/', 'block_docs');

registerBlockType(
    'ultimate-post/button-group', {
        title: __('Button Group','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/button-group.svg'} alt="button group"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Create & customize multiple button-styles','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('button','ultimate-post'),
            __('button group','ultimate-post'),
            __('btn','ultimate-post')
        ],
        supports: {
            align: ['wide', 'full'],
            html: false,
            reusable: false,
        },
        attributes,
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/button.svg',
            },
        },
        edit: Edit,
        save: Save,
    }
)