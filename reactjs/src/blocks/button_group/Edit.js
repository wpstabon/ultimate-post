const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useEffect, useState, Fragment } = wp.element
import { CssGenerator } from '../../helper/CssGenerator'
import ButtonsSetting from './Setting'
import ToolBarElement from '../../helper/ToolBarElement'
import { isInlineCSS, updateCurrentPostId } from '../../helper/CommonPanel'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

    const [section, setSection] = useState('Content');
    const { setAttributes, name, attributes, className, clientId, attributes: { blockId, currentPostId, advanceId, btnAnimation, previewImg } } = props

    useEffect(() => {    
        const _client = clientId.substr(0, 6)
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId );
        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
    }, [clientId]);

    const store = { setAttributes, name, attributes, setSection, section, clientId }
    
    let __preview_css;
    if (blockId) { 
        __preview_css = CssGenerator(attributes, 'ultimate-post/button-group', blockId, isInlineCSS());
    }

    if (previewImg) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }

    return (
        <Fragment>
            <InspectorControls>
                <ButtonsSetting store={store}/>
            </InspectorControls>
            <ToolBarElement
                include={[
                    { 
                        type:'template' 
                    }
                ]}
                store={store} />
            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className}`}>
                { __preview_css &&
                    <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                }
                <div className={`ultp-button-wrapper${btnAnimation ? ' ultp-anim-'+btnAnimation : ''}`}>
                    <InnerBlocks
                        template={[["ultimate-post/button", {}]]}
                        allowedBlocks={['ultimate-post/button']}
                    />
                </div>
            </div>
        </Fragment>
    )
}