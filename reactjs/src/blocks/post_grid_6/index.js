const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-grid-6/', 'block_docs');

registerBlockType(
    'ultimate-post/post-grid-6', {
        title: __('Post Grid #6','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-grid-6.svg'}/>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('Post Grid in gradient display with tile blocks.','ultimate-post')}
        //     <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [
            __('Post Grid','ultimate-post'),
            __('Grid View','ultimate-post'),
            __('Article','ultimate-post'),
            __('Post Listing','ultimate-post'),
            __('Grid','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/postgrid6.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)