const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType(
    'ultimate-post/heading', {
        title: __('Heading','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/heading.svg'} alt="Heading"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">{__('Display heading or title with the ultimate controls.','ultimate-post')}</span>,
        keywords: [ 
            __('heading','ultimate-post'),
            __('title','ultimate-post'),
            __('section','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {},
        edit: Edit,
        save() {
            return null;
        },
    }
)