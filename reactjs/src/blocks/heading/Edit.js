const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { useEffect, useState, Fragment } = wp.element
import { CssGenerator } from '../../helper/CssGenerator'
import { GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, HeadingContent, isInlineCSS, blockSupportLink, updateCurrentPostId } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'
import Heading from '../../helper/block_template/Heading'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {
    
    const [section, setSection] = useState('Content');
    const { setAttributes, name, attributes, clientId, className, attributes: { blockId, advanceId, headingText, headingStyle, headingAlign, headingURL, headingBtnText, subHeadingShow, subHeadingText, headingTag, dcEnabled, dcText, currentPostId } } = props

    useEffect(() => {
        const _client = clientId.substr(0, 6);
        const reference = getBlockAttributes(getBlockRootClientId(clientId));
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId);
        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
    }, [clientId]);

    const store = { setAttributes, name, attributes, setSection, section, clientId }

    let __preview_css;
    if (blockId) {
        __preview_css = CssGenerator(attributes, 'ultimate-post/heading', blockId, isInlineCSS());
    }

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                        <HeadingContent include={[
                            {
                                position: 6, data: { type: 'toggle', key: 'openInTab', label: __('Links Open in New Tabs', 'ultimate-post') },
                            }
                        ]} initialOpen={true} store={store} />
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced initialOpen={true} store={store} />
                        <ResponsiveAdvanced store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                {blockSupportLink()}
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                {__preview_css &&
                    <style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
                }
                <div className={`ultp-block-wrapper`}>
                    <Heading
                        props={
                            {
                                headingShow: true,
                                headingStyle,
                                headingAlign,
                                headingURL,
                                headingText,
                                setAttributes,
                                headingBtnText,
                                subHeadingShow,
                                subHeadingText,
                                headingTag,
                                dcEnabled,
                                dcText
                            }
                        }
                    />
                </div>
            </div>
        </Fragment>
    )
}