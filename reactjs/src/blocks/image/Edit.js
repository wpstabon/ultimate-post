const { __ } = wp.i18n
const { InspectorControls, RichText } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element
const { TextControl, PanelBody, SelectControl, Spinner } = wp.components
import { CssGenerator } from '../../helper/CssGenerator'
import { ImageStyle, ButtonStyle, GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, isInlineCSS, blockSupportLink, updateCurrentPostId } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'
import Alignment from '../../helper/fields/Alignment'
import Color from '../../helper/fields/Color'
import Dimension from '../../helper/fields/Dimension'
import Media from '../../helper/fields/Media'
import RadioImage from '../../helper/fields/RadioImage'
import Toggle from '../../helper/fields/Toggle'
import Typography from '../../helper/fields/Typography'
import { isDCActive } from '../../helper/dynamic_content'
import DynamicContent from '../../helper/dynamic_content/dynamic-content'
import TextField from '../../helper/fields/TextField'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

    const [section, setSection] = useState('Content');
    const [state, setState] = useState({ device: 'lg', setDevice: "", postsList: [], loading: false, error: false, section: 'Content' });
    const { setAttributes, name, clientId, attributes, className, attributes: { previewImg, blockId, advanceId, headingText, imageUpload, imgLink, imgAlt, imgOverlay, imgOverlayType, imgAnimation, headingColor, headingTypo, headingEnable, headingMargin, linkTarget, alignment, btnLink, btnText, btnTarget, btnPosition, linkType, darkImgEnable, darkImage, imgCrop, dcEnabled, currentPostId } } = props

    useEffect(() => {
        const _client = clientId.substr(0, 6);
        const reference = getBlockAttributes(getBlockRootClientId(clientId));
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
    }, [clientId]);

    const store = { setAttributes, name, attributes, setSection, section, clientId }

    // const imgUrl = imageUpload['url'] ? imageUpload['url'] : ultp_data.url + 'assets/img/ultp-placeholder.jpg';
    const imgUrl = imageUpload?.size && imageUpload?.size[imgCrop] ? (imageUpload?.size[imgCrop]?.url ? imageUpload?.size[imgCrop]?.url : imageUpload?.size[imgCrop]) : imageUpload['url'] ? imageUpload['url'] : ultp_data.url + 'assets/img/ultp-placeholder.jpg';

    let __preview_css;
    if (blockId) {
        __preview_css = CssGenerator(attributes, 'ultimate-post/image', blockId, isInlineCSS());
    }

    if (previewImg) {
        return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
    }

    const setDevice = (val) => {
        if (val) {
            setState({ device: val })
        }
    }

    const handleLoader = (val) => {
        setState({ loading: val })
    }

    const block = wp.data.select('core/block-editor').getBlock(clientId);

    return (
        <Fragment>
            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__('Setting', 'ultimate-post')}>
                        <PanelBody initialOpen={true} title={__('', 'ultimate-post')}>
                            <Media
                                label={__('Upload Image', 'ultimate-post')}
                                multiple={false}
                                block='image-block'
                                type={['image']}
                                panel={true}
                                value={imageUpload}
                                handleLoader={handleLoader}
                                dcEnabled={dcEnabled['imageUpload']}
                                DynamicContent={
                                    <DynamicContent
                                        headingBlock={block}
                                        attrKey={"imageUpload"}
                                        isActive={false}
                                        config={{
                                            disableAdv: true,
                                            disableLink: true,
                                            fieldType: 'image'
                                        }}

                                    />
                                }
                                onChange={(val) => {
                                    setAttributes({ imageUpload: val, imgAlt: val?.alt ? val.alt : 'Image Not Found', headingText: val?.caption ? val.caption : 'This is a Image Example' })
                                }} />
                            <TextControl
                                __nextHasNoMarginBottom={true}
                                label={__('Image Alt Text', 'ultimate-post')}
                                value={imgAlt}
                                onChange={val => setAttributes({ imgAlt: val })} />
                            <RadioImage
                                label={__('Link Type', 'ultimate-post')}
                                isText={true}
                                options={[
                                    { label: __('Link', 'ultimate-post'), value: 'link' },
                                    { label: __('Button', 'ultimate-post'), value: 'button' }
                                ]}
                                value={linkType}
                                onChange={val => setAttributes({ linkType: val })}
                            />
                            {(linkType == 'link') &&
                                <Fragment>
                                    <TextField
                                        value={imgLink}
                                        onChange={val => setAttributes({ imgLink: val })}
                                        isTextField={true}
                                        attr={{
                                            label: __('Link', 'ultimate-post'),
                                            disabled: dcEnabled.imgLink
                                        }}
                                        DC={
                                            isDCActive() ? (
                                                <DynamicContent
                                                    headingBlock={block}
                                                    isActive={false}
                                                    attrKey='imgLink'
                                                    config={{
                                                        linkOnly: true,
                                                        disableAdv: true,
                                                        fieldType: 'url'
                                                    }}
                                                />
                                            ) : null
                                        }
                                    />
                                    <SelectControl
                                        __nextHasNoMarginBottom={true}
                                        label={__('Link Target', 'ultimate-post')}
                                        value={linkTarget || ''}
                                        options={[
                                            { label: __('Self', 'ultimate-post'), value: '_self' },
                                            { label: __('Blank', 'ultimate-post'), value: '_blank' }
                                        ]}
                                        onChange={val => setAttributes({ linkTarget: val })}
                                    />
                                </Fragment>
                            }
                            {(linkType == 'button') &&
                                <Fragment>
                                    <TextControl
                                        __nextHasNoMarginBottom={true}
                                        label={__('Button Text', 'ultimate-post')}
                                        value={btnText}
                                        onChange={val => setAttributes({ btnText: val })} />

                                    <TextField
                                        value={btnLink}
                                        onChange={val => setAttributes({ btnLink: val })}
                                        isTextField={true}
                                        attr={{
                                            label: __('Button Link', 'ultimate-post'),
                                            disabled: dcEnabled.btnLink
                                        }}
                                        DC={
                                            isDCActive() ? (
                                                <DynamicContent
                                                    headingBlock={block}
                                                    isActive={false}
                                                    attrKey='btnLink'
                                                    config={{
                                                        linkOnly: true,
                                                        disableAdv: true,
                                                        fieldType: 'url'
                                                    }}
                                                />
                                            ) : null
                                        }
                                    />
                                    <SelectControl
                                        __nextHasNoMarginBottom={true}
                                        label={__('Button Link Target', 'ultimate-post')}
                                        value={btnTarget || ''}
                                        options={[
                                            { label: __('Self', 'ultimate-post'), value: '_self' },
                                            { label: __('Blank', 'ultimate-post'), value: '_blank' }
                                        ]}
                                        onChange={val => setAttributes({ btnTarget: val })}
                                    />
                                    <SelectControl
                                        __nextHasNoMarginBottom={true}
                                        label={__('Button Position', 'ultimate-post')}
                                        value={btnPosition || ''}
                                        options={[
                                            { label: __('Left Top', 'ultimate-post'), value: 'leftTop' },
                                            { label: __('Right Top', 'ultimate-post'), value: 'rightTop' },
                                            { label: __('Center Center', 'ultimate-post'), value: 'centerCenter' },
                                            { label: __('Bottom Left', 'ultimate-post'), value: 'bottomLeft' },
                                            { label: __('Bottom Right', 'ultimate-post'), value: 'bottomRight' },
                                        ]}
                                        onChange={val => setAttributes({ btnPosition: val })}
                                    />
                                </Fragment>
                            }
                            <Toggle
                                label={__('Enable Caption', 'ultimate-post')}
                                value={headingEnable}
                                onChange={val => setAttributes({ headingEnable: val })}
                            />
                        </PanelBody>
                        <PanelBody title={__('Dark Mode Image', 'ultimate-post')}>
                            <Toggle
                                label={__('Enable Dark Image', 'ultimate-post')}
                                value={darkImgEnable}
                                onChange={val => setAttributes({ darkImgEnable: val })}
                            />
                            {
                                darkImgEnable && (
                                    <Media
                                        label={__('Upload Dark Mode Image', 'ultimate-post')}
                                        multiple={false}
                                        type={['image']}
                                        panel={true}
                                        value={darkImage}
                                        dcEnabled={dcEnabled['darkImage']}
                                        DynamicContent={
                                            <DynamicContent
                                                headingBlock={block}
                                                attrKey={"darkImage"}
                                                isActive={false}
                                                config={{
                                                    disableAdv: true,
                                                    disableLink: true,
                                                    fieldType: 'image'
                                                }}

                                            />
                                        }
                                        onChange={(val) => {
                                            setAttributes({ darkImage: val })
                                        }}
                                    />
                                )
                            }
                        </PanelBody>
                    </Section>
                    <Section slug="style" title={__('Style', 'ultimate-post')}>
                        <ImageStyle
                            store={store}
                            initialOpen={true}
                            exclude={['imgCropSmall', 'imgAnimation']}
                            include={[
                                {
                                    position: 0, data: {
                                        type: 'select', key: 'imgAnimation', label: __('Hover Animation', 'ultimate-post'), options: [
                                            { value: 'none', label: __('No Animation', 'ultimate-post') },
                                            { value: 'zoomIn', label: __('Zoom In', 'ultimate-post') },
                                            { value: 'zoomOut', label: __('Zoom Out', 'ultimate-post') },
                                            { value: 'opacity', label: __('Opacity', 'ultimate-post') },
                                            { value: 'roateLeft', label: __('Rotate Left', 'ultimate-post') },
                                            { value: 'rotateRight', label: __('Rotate Right', 'ultimate-post') }
                                        ]
                                    }
                                },
                                {
                                    position: 1, data: { type: 'alignment', key: 'imgAlignment', label: __('Image Align', 'ultimate-post'), responsive: true, disableJustify: true },
                                }
                            ]} />
                        {headingEnable &&
                            <PanelBody title={__('Caption', 'ultimate-post')}>
                                <Alignment
                                    label={__('Alignment', 'ultimate-post')}
                                    value={alignment}
                                    onChange={val => setAttributes({ alignment: val })}
                                    disableJustify
                                    responsive
                                    device={state.device}
                                    setDevice={(v) => setDevice(v)}
                                    onDeviceChange={value => setState({ device: value })} />
                                <Color
                                    label={__('Caption Color', 'ultimate-post')}
                                    value={headingColor}
                                    onChange={val => setAttributes({ headingColor: val })} />
                                <Typography
                                    label={__('Caption Typography', 'ultimate-post')}
                                    value={headingTypo}
                                    onChange={val => setAttributes({ headingTypo: val })}
                                    device={state.device}
                                    setDevice={(v) => setDevice(v)}
                                    onDeviceChange={value => setState({ device: value })} />
                                <Dimension
                                    label={__('Caption Margin', 'ultimate-post')}
                                    value={headingMargin}
                                    min={0}
                                    max={100}
                                    unit={['px', 'em', 'rem', '%']}
                                    responsive
                                    device={state.device}
                                    setDevice={(v) => setDevice(v)}
                                    onDeviceChange={value => setState({ device: value })}
                                    onChange={val => setAttributes({ headingMargin: val })}
                                    clientId={props.clientId}
                                />
                            </PanelBody>
                        }
                        {(linkType == 'button') &&
                            <ButtonStyle store={store} />
                        }
                    </Section>
                    <Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
                        <GeneralAdvanced store={store} />
                        <ResponsiveAdvanced store={store} />
                        <CustomCssAdvanced store={store} />
                    </Section>
                </Sections>
                {blockSupportLink()}
            </InspectorControls>
            <div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
                {__preview_css &&
                    <style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
                }
                <div className={`ultp-block-wrapper`}>
                    <figure className={`ultp-image-block-wrapper`}>
                        <div className={`ultp-image-block ultp-image-block-${imgAnimation} ${(imgOverlay === true) ? 'ultp-image-block-overlay ultp-image-block-' + imgOverlayType : ''} ${state.loading ? ' ultp-loading-active' : ''}`}>
                            {imgUrl && !(state.loading) ?
                                (((linkType == 'link') && imgLink) ?
                                    <a><img className={`ultp-image`} src={imgUrl} alt={imgAlt ? imgAlt : 'Image'} /></a>
                                    :
                                    <img className={`ultp-image`} src={imgUrl} alt={imgAlt ? imgAlt : 'Image'} />
                                )
                                :
                                <>
                                    <img className={`ultp-image`} src={imgUrl} alt={imgAlt ? imgAlt : 'Image'} />
                                    <Spinner />
                                </>
                            }
                            {(linkType == 'button') && btnLink &&
                                <div className={`ultp-image-button ultp-image-button-${btnPosition}`} ><a href={"#"}>{btnText}</a></div>
                            }
                        </div>
                        {headingEnable && headingText &&
                            <RichText
                                key="editable"
                                tagName={'figcaption'}
                                className={`ultp-image-caption`}
                                keepPlaceholderOnFocus
                                placeholder={__('Add Text...', 'ultimate-post')}
                                onChange={value => setAttributes({ headingText: value })}
                                value={headingText}
                            />
                        }
                    </figure>
                </div>
            </div>
        </Fragment>
    )

}
