const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from "./attributes";


registerBlockType( 
    'ultimate-post/image', {
        title: __('Image','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/image.svg'} alt="Image"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">{__('Display images with the ultimate controls.','ultimate-post')}</span>,
        keywords: [
            __('Image','ultimate-post'),
            __('Media','ultimate-post'),
            __('Gallery','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/image.svg',
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)