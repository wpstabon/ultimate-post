import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { dynamicImageAttributes } from "../../helper/dynamic_content";

const attributes = {
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  /* =================================  
        Image Setting/Style
  ================================= */  
  imageUpload: {
    type: "object",
    default: {
      id: "999999",
      url: ultp_data.url+"assets/img/ultp-placeholder.jpg",
    },
  },
  // Dark Image
  darkImgEnable: { 
    type: "boolean",
    default: false 
  },
  darkImage: {
    type: "object",
    default: {
      url: ultp_data.url+"assets/img/ultp-placeholder.jpg",
    },
  },
  linkType: { type: "string", default: "link" },
  imgLink: { type: "string", default: "" },
  linkTarget: { type: "string", default: "_blank" },
  imgAlt: { type: "string", default: "Image" },
  imgAlignment: {
    type: "object",
    default: { lg: "left" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-block-wrapper {text-align: {{imgAlignment}};}",
      },
    ],
  },
  imgCrop: {
    type: "string",
    default: "full",
  },
  imgWidth: {
    type: "object",
    default: { lg: "", ulg: "px" },
    style: [
      { selector: "{{ULTP}} .ultp-image-block { max-width: {{imgWidth}}; }" },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-image-block {object-fit: cover; height: {{imgHeight}}; } 
          {{ULTP}} .ultp-image-block .ultp-image {height: 100%;}`,
      },
    ],
  },
  imageScale: {
    type: "string",
    default: "cover",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-block img {object-fit: {{imageScale}};}",
      },
    ],
  },
  imgAnimation: { type: "string", default: "none" },
  imgGrayScale: {
    type: "object",
    default: { lg: "0", ulg: "%", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image { filter: grayscale({{imgGrayScale}}); }",
      },
    ],
  },
  imgHoverGrayScale: {
    type: "object",
    default: { lg: "0", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-block:hover .ultp-image { filter: grayscale({{imgHoverGrayScale}}); }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector: "{{ULTP}} .ultp-image-block { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  imgHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-block:hover { border-radius:{{imgHoverRadius}}; }",
      },
    ],
  },
  imgShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-image-block" }],
  },
  imgHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        selector: "{{ULTP}} .ultp-image-block:hover",
      },
    ],
  },
  imgMargin: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-image-block { margin: {{imgMargin}}; }",
      },
    ],
  },
  imgOverlay: { type: "boolean", default: false },
  imgOverlayType: {
    type: "string",
    default: "default",
    style: [{ depends: [{ key: "imgOverlay", condition: "==", value: true }] }],
  },
  overlayColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#0e1523" },
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector: "{{ULTP}} .ultp-image-block::before",
      },
    ],
  },
  imgOpacity: {
    type: "string",
    default: 0.69999999999999996,
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector:
          "{{ULTP}} .ultp-image-block::before { opacity: {{imgOpacity}}; }",
      },
    ],
  },
  imgLazy: { type: "boolean", default: false },
  imgSrcset: { type: "boolean", default: false },

  /* =================================  
        Heading Setting/Style
  ================================= */  
  headingText: { type: "string", default: "This is a Image Example" },
  headingEnable: { type: "boolean", default: false },
  headingColor: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "headingEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-caption { color:{{headingColor}}; }",
      },
    ],
  },
  alignment: {
    type: "object",
    default: { lg: "left" },
    style: [
      {
        depends: [{ key: "headingEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-caption {text-align: {{alignment}};}",
      },
    ],
  },
  headingTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "14", unit: "px" },
      height: { lg: "", unit: "px" },
      decoration: "",
      transform: "",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "headingEnable", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-image-block-wrapper .ultp-image-caption",
      },
    ],
  },
  headingMargin: {
    type: "object",
    default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "headingEnable", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-image-caption { margin:{{headingMargin}}; }",
      },
    ],
  },

/* =================================  
        Button Setting/Style
  ================================= */
  buttonEnable: { type: "boolean", default: false },
  btnText: { type: "string", default: "Free Download" },
  btnLink: { type: "string", default: "#" },
  btnTarget: { type: "string", default: "_blank" },
  btnPosition: { type: "string", default: "centerCenter" },
  btnTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 20, unit: "px" },
      spacing: { lg: 0, unit: "px" },
      transform: "",
      weight: "",
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector: "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a",
      },
    ],
  },
  btnColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a { color:{{btnColor}}; }",
      },
    ],
  },
  btnBgColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#037fff" },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector: "{{ULTP}}  .ultp-image-button a",
      },
    ],
  },
  btnBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector: "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a",
      },
    ],
  },
  btnRadius: {
    type: "object",
    default: { lg: "2", unit: "px" },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-button a { border-radius:{{btnRadius}}; }",
      },
    ],
  },
  btnShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector: "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a",
      },
    ],
  },
  btnHoverColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a:hover { color:{{btnHoverColor}}; }",
      },
    ],
  },
  btnBgHoverColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#1239e2" },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-button a:hover",
      },
    ],
  },
  btnHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a:hover",
      },
    ],
  },
  btnHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-button a:hover { border-radius:{{btnHoverRadius}}; }",
      },
    ],
  },
  btnHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a:hover",
      },
    ],
  },
  btnSacing: {
    type: "object",
    default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: "px" } },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-block-wrapper .ultp-image-button a { margin:{{btnSacing}}; }",
      },
    ],
  },
  btnPadding: {
    type: "object",
    default: {
      lg: { top: "6", bottom: "6", left: "12", right: "12", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "linkType", condition: "==", value: "button" }],
        selector:
          "{{ULTP}} .ultp-image-button a { padding:{{btnPadding}}; }",
      },
    ],
  },
  /* =================================  
        Wrapper Settings
  ================================= */
  ...commonAttributes(['advanceAttr'], ['loadingColor']),

  ...dynamicImageAttributes,
  
};
export default attributes;
