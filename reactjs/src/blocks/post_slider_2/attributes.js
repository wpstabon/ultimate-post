import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { V4_1_0_CompCheck } from "../../helper/compatibility";
import { getDCBlockAttributes } from "../../helper/dynamic_content/attributes";

const attributes = {
  /*==========================
      General Settings
  ==========================*/
  blockPubDate: { type: "string", default: "empty" },
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  slidesToShow: {
    type: "object",
    default: { lg: "1", sm: "1", xs: "1" },
    style: [
      {
        depends: [
          { key: "fade", condition: "!=", value: true },
          { key: "layout", condition: "==", value: "slide2" },
        ],
      },
      { depends: [{ key: "layout", condition: "==", value: "slide3" }] },
    ],
  },
  autoPlay: { type: "boolean", default: true },
  height: {
    type: "object",
    default: { lg: "550", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-slider-wrap { height: {{height}}; }",
      },
    ],
  },
  slidesCenterPadding: {
    type: "object",
    default: { lg: "160", sm: "100", xs: "50" },
    style: [
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
          { key: "layout", condition: "!=", value: "slide5" },
          { key: "layout", condition: "!=", value: "slide6" },
          { key: "layout", condition: "!=", value: "slide8" },
        ],
      },
    ],
  },
  slidesTopPadding: {
    type: "string",
    default: "0",
    style: [
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide1" },
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
          { key: "layout", condition: "!=", value: "slide5" },
          { key: "layout", condition: "!=", value: "slide6" },
          { key: "layout", condition: "!=", value: "slide8" },
        ],
        selector:
          "{{ULTP}} .ultp-block-items-wrap .slick-list { padding-top: {{slidesTopPadding}}px !important; padding-bottom: {{slidesTopPadding}}px !important; }",
      },
    ],
  },
  allItemScale: {
    type: "object",
    default: { lg: ".9" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide4" }],
        selector:
          "{{ULTP}} .slick-slide { transition: .3s; transform: scale({{allItemScale}}) }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .slick-slide { transition: .3s; transform: scale({{allItemScale}}) }",
      },
    ],
  },
  centerItemScale: {
    type: "object",
    default: { lg: "1.12" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide4" }],
        selector:
          "{{ULTP}} .slick-center { transition: .3s; transform: scale({{centerItemScale}}) !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .slick-center { transition: .3s; transform: scale({{centerItemScale}}) !important; }",
      },
    ],
  },
  slideSpeed: {
    type: "string",
    default: "3000",
    style: [{ depends: [{ key: "autoPlay", condition: "==", value: true }] }],
  },
  sliderGap: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "layout", condition: "!=", value: "slide2" }],
        selector:
          `{{ULTP}} .ultp-block-items-wrap .slick-slide > div { padding: 0 {{sliderGap}}px; }
          {{ULTP}} .ultp-block-items-wrap .slick-list{ margin: 0 -{{sliderGap}}px; }`,
      },
    ],
  },
  dots: { type: "boolean", default: true },
  arrows: { type: "boolean", default: true },
  preLoader: { type: "boolean", default: false },
  fade: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "layout", condition: "==", value: "slide2" }] },
      { depends: [{ key: "layout", condition: "==", value: "slide5" }] },
      { depends: [{ key: "layout", condition: "==", value: "slide6" }] },
      { depends: [{ key: "layout", condition: "==", value: "slide8" }] },
    ],
  },
  headingShow: { type: "boolean", default: false },
  excerptShow: { type: "boolean", default: false },
  contentTag: { type: "string", default: "div" },
  openInTab: { type: "boolean", default: false },
  notFoundMessage: { type: "string", default: "No Post Found" },
  layout: { type: "string", default: "slide1" },

  /*==========================
      Title Settings
  ==========================*/
  titleShow: { type: "boolean", default: true },
  titleStyle: { type: "string", default: "none" },
  titleTag: { type: "string", default: "h3" },
  titlePosition: { type: "boolean", default: true },
  titleColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-title a { color:{{titleColor}} !important; }",
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "rgba(107,107,107,1)",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-title a:hover { color:{{titleHoverColor}} !important; }",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "24", unit: "px" },
      height: { lg: "36", unit: "px" },
      decoration: "none",
      family: "",
      weight: "300",
      transform: "uppercase",
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-item .ultp-block-content .ultp-block-title, 
          {{ULTP}} .ultp-block-item .ultp-block-content .ultp-block-title a`,
      },
    ],
  },
  titlePadding: {
    type: "object",
    default: { lg: { top: 0, bottom: 0, unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title { padding:{{titlePadding}}; }",
      },
    ],
  },
  titleLength: { type: "string", default: 0 },
  titleBackground: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title a { padding: 2px 7px; -webkit-box-decoration-break: clone; box-decoration-break: clone; background-color:{{titleBackground}}; }",
      },
    ],
  },
  titleAnimColor: {
    type: "string",
    default: "black",
    style: [
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style1" }],
        selector:
          "{{ULTP}} .ultp-title-style1 a {  cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.35s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style2" }],
        selector:
          "{{ULTP}} .ultp-title-style2 a:hover {  border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style3" }],
        selector:
          "{{ULTP}} .ultp-title-style3 a {  text-decoration: none; $thetransition: all 1s cubic-bezier(1,.25,0,.75) 0s; position: relative; transition: all 0.35s ease-out; padding-bottom: 3px; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style4" }],
        selector:
          "{{ULTP}} .ultp-title-style4 a { cursor: pointer; font-weight: 600; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a:hover { text-decoration: none; transition: all 0.35s ease-out; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-size:auto; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style6" }],
        selector:
          "{{ULTP}} .ultp-title-style6 a { background-image: linear-gradient(120deg, {{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 2px; background-position: 0 88%; transition: background-size 0.15s ease-in; padding: 5px 5px; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style7" }],
        selector:
          "{{ULTP}} .ultp-title-style7 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: right 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style8" }],
        selector:
          "{{ULTP}} .ultp-title-style8 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: center 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style9" }],
        selector:
          "{{ULTP}} .ultp-title-style9 a { background-image: linear-gradient(120deg,{{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 10px; background-position: 0 88%; transition: 0.3s ease-in; padding: 3px 5px; }",
      },
    ],
  },
  /*==========================
      Image Settings
  ==========================*/
  imageShow: { type: "boolean", default: true },
  imgCrop: { type: "string", default: "full" },
  imgOverlay: { type: "boolean", default: false },
  imgBgbrightness: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-image { filter: brightness({{imgBgbrightness}}); }",
      },
    ],
  },
  imgBgBlur: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-image > a { filter: blur({{imgBgBlur}}px);}",
      },
    ],
  },
  imgOverlayType: {
    type: "string",
    default: "default",
    style: [{ depends: [{ key: "imgOverlay", condition: "==", value: true }] }],
  },
  overlayColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#0e1523" },
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector: "{{ULTP}} .ultp-block-image-custom > a::before",
      },
    ],
  },
  imgOpacity: {
    type: "string",
    default: 0.69999999999999996,
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector:
          "{{ULTP}} .ultp-block-image-custom > a::before { opacity: {{imgOpacity}}; }",
      },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-image-inner { height:{{imgHeight}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide6" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-image-inner { height:{{imgHeight}} !important; }",
      },
    ],
  },
  imgWidth: {
    type: "object",
    default: { lg: "", unit: "%" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-image-inner { width:{{imgWidth}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-image-inner { width:{{imgWidth}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide6" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-image-inner { width:{{imgWidth}} !important; }",
      },
    ],
  },
  imgGrayScale: {
    type: "object",
    default: { lg: "0", ulg: "%", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-image img { filter: grayscale({{imgGrayScale}}); }",
      },
    ],
  },
  imgHoverGrayScale: {
    type: "object",
    default: { lg: "0", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image img { filter: grayscale({{imgHoverGrayScale}}); }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector: "{{ULTP}} .ultp-block-image { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  imgHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image { border-radius:{{imgHoverRadius}}; }",
      },
    ],
  },
  imgShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-image" }],
  },
  imgHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-item:hover .ultp-block-image" }],
  },
  fallbackEnable: {
    type: "boolean",
    default: true,
    style: [{ depends: [{ key: "imageShow", condition: "==", value: true }] }],
  },
  fallbackImg: {
    type: "object",
    default: "",
    style: [
      {
        depends: [
          { key: "imageShow", condition: "==", value: true },
          { key: "fallbackEnable", condition: "==", value: true },
        ],
      },
    ],
  },
  imgSrcset: { type: "boolean", default: false },
  imgLazy: { type: "boolean", default: false },

  /*==========================
      Excerpt Settings
  ==========================*/
  showSeoMeta: { type: "boolean", default: false },
  showFullExcerpt: { 
    type: "boolean", 
    default: false,
    style: [
      {
        depends: [{ key: "showSeoMeta", condition: "==", value: false }]
      }
    ]
  },
  excerptLimit: {
    type: "string",
    default: 40,
    style: [
      { depends: [{ key: "showFullExcerpt", condition: "==", value: false }] },
    ],
  },
  excerptColor: {
    type: "string",
    default: "#fff8",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: 
        `{{ULTP}} .ultp-block-excerpt, 
        {{ULTP}} .ultp-block-excerpt p { color:{{excerptColor}}; }`,
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 26, unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-excerpt, 
          {{ULTP}} .ultp-block-excerpt p`,
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: 10, bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt{ padding: {{excerptPadding}}; }",
      },
    ],
  },
  /*==========================
      Arrow Settings
  ==========================*/
  arrowStyle: {
    type: "string",
    default: "leftAngle2#rightAngle2",
    style: [{ depends: [{ key: "arrows", condition: "==", value: true }] }],
  },
  arrowSize: {
    type: "object",
    default: { lg: "80", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-next svg, 
          {{ULTP}} .slick-prev svg { width:{{arrowSize}}; }`,
      },
    ],
  },
  arrowWidth: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { width:{{arrowWidth}}; }",
      },
    ],
  },
  arrowHeight: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow { height:{{arrowHeight}}; } 
          {{ULTP}} .slick-arrow { line-height:{{arrowHeight}}; }`,
      },
    ],
  },
  arrowSpaceBetween:{
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "layout", condition: "==", value: "slide6" }] },
      { depends: [{ key: "layout", condition: "==", value: "slide5" }] },
      { depends: [{ key: "layout", condition: "==", value: "slide8" }] },
    ],
  },
  arrowPosBetween:{
    type: "object",
    default: { lg: "45", sm: "16", xs: "0", unit: "px" },
    style: [
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide6" },
          { key: "arrowSpaceBetween", condition: "==", value: true }
        ],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowPosBetween}}; } 
          {{ULTP}} .slick-prev { left:{{arrowPosBetween}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide5" },
          { key: "arrowSpaceBetween", condition: "==", value: true }
        ],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowPosBetween}}; } 
          {{ULTP}} .slick-prev { left:{{arrowPosBetween}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide8" },
          { key: "arrowSpaceBetween", condition: "==", value: true }
        ],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowPosBetween}}; } 
          {{ULTP}} .slick-prev { left:{{arrowPosBetween}}; }`,
      },
    ],
  },
  arrowPos: {
    type: "string",
    default: "left",
    style: [
        {
          depends: [
            { key: "layout", condition: "==", value: "slide6" },
            { key: "arrows", condition: "==", value: true },
            { key: "arrowSpaceBetween", condition: "==", value: false }
          ]
        },
        {
          depends: [
            { key: "layout", condition: "==", value: "slide5" },
            { key: "arrows", condition: "==", value: true },
            { key: "arrowSpaceBetween", condition: "==", value: false }
          ]        
        },
        {
          depends: [
            { key: "layout", condition: "==", value: "slide8" },
            { key: "arrows", condition: "==", value: true },
            { key: "arrowSpaceBetween", condition: "==", value: false }
          ]        
        },
    ],
  },
  arrowVartical: {
    type: "object",
    default: { lg: "45", sm: "16", xs: "0", unit: "px" },
    style: [
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "!=", value: "slide5" },
          { key: "layout", condition: "!=", value: "slide8" },
          { key: "layout", condition: "!=", value: "slide6" },
        ],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { left:{{arrowVartical}}; }`,
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: true },
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "!=", value: "slide5" },
          { key: "layout", condition: "!=", value: "slide8" },
          { key: "layout", condition: "!=", value: "slide6" },
        ],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { left:{{arrowVartical}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide5" },
        ],
        selector:
          `{{ULTP}} .slick-next { top:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { top:{{arrowVartical}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide6" },
        ],
        selector:
          `{{ULTP}} .slick-next { top:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { top:{{arrowVartical}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide7" },
        ],
        selector:
          `{{ULTP}} .slick-next { top:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { top:{{arrowVartical}}; }`,
      },
      {
        depends: [
          { key: "arrows", condition: "==", value: true },
          { key: "layout", condition: "==", value: "slide8" },
        ],
        selector:
          `{{ULTP}} .slick-next { top:{{arrowVartical}}; } 
          {{ULTP}} .slick-prev { top:{{arrowVartical}}; }`,
      },
    ],
  },
  prevArrowPos: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide5" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-prev { left: unset !important; right: {{prevArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide6" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-prev { left: unset !important; right: {{prevArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide8" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-prev { left: unset !important; right: {{prevArrowPos}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .slick-prev { right: unset !important; left: {{prevArrowPos}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .slick-prev { right: unset !important; left: {{prevArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "layout", condition: "==", value: "slide5" },
          { key: "arrowPos", condition: "==", value: "left" },
          { key: "arrowSpaceBetween", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .slick-prev { right: unset !important; left: {{prevArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "layout", condition: "==", value: "slide6" },
          { key: "arrowPos", condition: "==", value: "left" },
          { key: "arrowSpaceBetween", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .slick-prev { right: unset !important; left: {{prevArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "layout", condition: "==", value: "slide8" },
          { key: "arrowPos", condition: "==", value: "left" },
          { key: "arrowSpaceBetween", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .slick-prev { right: unset !important; left: {{prevArrowPos}} !important; }",
      },
    ],
  },
  nextArrowPos: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide5" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-next { left: unset !important; right: {{nextArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide6" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-next { left: unset !important; right: {{nextArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide8" },
          { key: "arrowPos", condition: "==", value: "right" },
        ],
        selector:
          "{{ULTP}} .slick-next { left: unset !important; right: {{nextArrowPos}} !important; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .slick-next { left: unset !important; right: {{nextArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide5" },
          { key: "arrowPos", condition: "==", value: "left" },
        ],
        selector:
          "{{ULTP}} .slick-next { right: unset !important; left: {{nextArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide6" },
          { key: "arrowPos", condition: "==", value: "left" },
        ],
        selector:
          "{{ULTP}} .slick-next { right: unset !important; left: {{nextArrowPos}} !important; }",
      },
      {
        depends: [
          { key: "arrowSpaceBetween", condition: "==", value: false },
          { key: "layout", condition: "==", value: "slide8" },
          { key: "arrowPos", condition: "==", value: "left" },
        ],
        selector:
          "{{ULTP}} .slick-next { right: unset !important; left: {{nextArrowPos}} !important; }",
      },
    ],
  },
  arrowColor: {
    type: "string",
    default: "#000",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow:before { color:{{arrowColor}}; }
          {{ULTP}} .slick-arrow svg { fill:{{arrowColor}}; }`,
      },
    ],
  },
  arrowHoverColor: {
    type: "string",
    default: "#000",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow:hover:before { color:{{arrowHoverColor}}; } 
          {{ULTP}} .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }`,
      },
    ],
  },
  arrowBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { background:{{arrowBg}}; }",
      },
    ],
  },
  arrowHoverBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-arrow:hover { background:{{arrowHoverBg}}; }",
      },
    ],
  },
  arrowBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow",
      },
    ],
  },
  arrowHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow:hover",
      },
    ],
  },
  arrowRadius: {
    type: "object",
    default: {
      lg: { top: "0", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { border-radius: {{arrowRadius}}; }",
      },
    ],
  },
  arrowHoverRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-arrow:hover{ border-radius: {{arrowHoverRadius}}; }",
      },
    ],
  },
  arrowShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow",
      },
    ],
  },
  arrowHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow:hover",
      },
    ],
  },

  /*==========================
      Dot Settings
  ==========================*/
  dotWidth: {
    type: "object",
    default: { lg: "5", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { width:{{dotWidth}}; }",
      },
    ],
  },
  dotHeight: {
    type: "object",
    default: { lg: "5", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { height:{{dotHeight}}; }",
      },
    ],
  },
  dotHoverWidth: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li.slick-active button { width:{{dotHoverWidth}}; }",
      },
    ],
  },
  dotHoverHeight: {
    type: "object",
    default: { lg: "8", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li.slick-active button { height:{{dotHoverHeight}}; }",
      },
    ],
  },
  dotSpace: {
    type: "object",
    default: { lg: "4", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots { padding: 0 {{dotSpace}}; } 
          {{ULTP}} .slick-dots li button { margin: 0 {{dotSpace}}; }`,
      },
    ],
  },
  dotVartical: {
    type: "object",
    default: { lg: "-20", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots { bottom:{{dotVartical}}; }",
      },
    ],
  },
  dotHorizontal: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots { left:{{dotHorizontal}}; }",
      },
    ],
  },
  dotBg: {
    type: "string",
    default: "#9b9b9b",
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { background:{{dotBg}}; }",
      },
    ],
  },
  dotHoverBg: {
    type: "string",
    default: "#9b9b9b",
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover, 
          {{ULTP}} .slick-dots li.slick-active button { background:{{dotHoverBg}}; }`,
      },
    ],
  },
  dotBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button",
      },
    ],
  },
  dotHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover, 
          {{ULTP}} .slick-dots li.slick-active button`,
      },
    ],
  },
  dotRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li button { border-radius: {{dotRadius}}; }",
      },
    ],
  },
  dotHoverRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover, 
          {{ULTP}} .slick-dots li.slick-active button { border-radius: {{dotHoverRadius}}; }`,
      },
    ],
  },
  dotShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button",
      },
    ],
  },
  dotHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover, 
          {{ULTP}} .slick-dots li.slick-active button`,
      },
    ],
  },

  /*==========================
      Content Settings
  ==========================*/
  contentVerticalPosition: {
    type: "string",
    default: "bottomPosition",
    style: [
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "topPosition",
          },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-topPosition { align-items:flex-start; }",
      },
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "middlePosition",
          },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-middlePosition { align-items:center; }",
      },
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "bottomPosition",
          },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-bottomPosition { align-items:flex-end; }",
      },
    ],
  },
  contentHorizontalPosition: {
    type: "string",
    default: "centerPosition",
    style: [
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "leftPosition",
          },
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-leftPosition { justify-content:flex-start; }",
      },
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "centerPosition",
          },
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-centerPosition { justify-content:center; }",
      },
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "rightPosition",
          },
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-rightPosition { justify-content:flex-end; }",
      },
    ],
  },
  contentAlign: {
    type: "string",
    default: "center",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: flex-start;}`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: center;}`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: flex-end;}`,
      },
    ],
  },
  contenWraptWidth: {
    type: "object",
    default: { lg: "100", unit: "%" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-content { width:{{contenWraptWidth}}; }",
      },
      /* {
        depends: [{ key: "layout", condition: "==", value: "slide3" }], // can not find any usecase slide 3
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-content { width:{{contenWraptWidth}}; }",
      }, */
      {
        depends: [{ key: "layout", condition: "!=", value: "slide2" }],
        depends: [{ key: "layout", condition: "!=", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-block-content .ultp-block-content-inner { width:{{contenWraptWidth}}; }",
      },
    ],
  },
  contenWraptHeight: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-content { height:{{contenWraptHeight}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-block-slider-wrap > .ultp-block-content { height:{{contenWraptHeight}} !important; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner { height:{{contenWraptHeight}} !important; }",
      },
    ],
  },
  slideBgBlur: {
    type: "string",
    default: "",
    style: [
      /* {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content { backdrop-filter: blur({{slideBgBlur}}px); }",
      }, */
      // {
      //   depends: [{ key: "layout", condition: "==", value: "slide3" }],
      //   selector:
      //     "{{ULTP}} .ultp-slide-slide3 .ultp-block-content { backdrop-filter: blur({{slideBgBlur}}px); }",
      // },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
          { key: "layout", condition: "!=", value: "slide6" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner { backdrop-filter: blur({{slideBgBlur}}px); }",
      },
    ],
  },
  contentWrapBg: {
    type: "string",
    default: "#00000069",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content { background:{{contentWrapBg}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-slide-slide3 .ultp-block-content { background:{{contentWrapBg}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner { background:{{contentWrapBg}}; }",
      },
    ],
  },
  contentWrapHoverBg: {
    type: "string",
    default: "#00000069",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content:hover { background:{{contentWrapHoverBg}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-slide-slide3 .ultp-block-content:hover { background:{{contentWrapHoverBg}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner:hover { background:{{contentWrapHoverBg}}; }",
      },
    ],
  },
  contentWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector: "{{ULTP}} .ultp-slide-slide2 .ultp-block-content",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector: "{{ULTP}} .ultp-slide-slide3 .ultp-block-content",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector: "{{ULTP}} .ultp-block-content-inner",
      },
    ],
  },
  contentWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector: "{{ULTP}} .ultp-slide-slide2 .ultp-block-content:hover",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector: "{{ULTP}} .ultp-slide-slide3 .ultp-block-content:hover",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector: "{{ULTP}} .ultp-block-content-inner:hover",
      },
    ],
  },
  contentWrapRadius: {
    type: "object",
    default: {
      lg: { top: "6", bottom: "6", left: "6", right: "6", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content { border-radius:{{contentWrapRadius}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-slide-slide3 .ultp-block-content { border-radius:{{contentWrapRadius}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner { border-radius: {{contentWrapRadius}}; }",
      },
    ],
  },
  contentWrapHoverRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content:hover { border-radius: {{contentWrapHoverRadius}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-slide-slide3 .ultp-block-content:hover { border-radius: {{contentWrapHoverRadius}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner:hover { border-radius: {{contentWrapHoverRadius}}; }",
      },
    ],
  },
  contentWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 0, right: 5, bottom: 15, left: 0 },
      color: "rgba(0,0,0,0.15)",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector: "{{ULTP}} .ultp-slide-slide2 .ultp-block-content",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector: "{{ULTP}} .ultp-slide-slide3 .ultp-block-content",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector: "{{ULTP}} .ultp-block-content-inner",
      },
    ],
  },
  contentWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 0, right: 10, bottom: 25, left: 0 },
      color: "rgba(0,0,0,0.25)",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector: "{{ULTP}} .ultp-slide-slide2 .ultp-block-content",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector: "{{ULTP}} .ultp-slide-slide3 .ultp-block-content",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector: "{{ULTP}} .ultp-block-content-inner:hover",
      },
    ],
  },
  contentWrapPadding: {
    type: "object",
    default: {
      lg: { top: "27", bottom: "27", left: "", right: "", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide2" }],
        selector:
          "{{ULTP}} .ultp-slide-slide2 .ultp-block-content { padding: {{contentWrapPadding}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide3" }],
        selector:
          "{{ULTP}} .ultp-slide-slide3 .ultp-block-content { padding: {{contentWrapPadding}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "!=", value: "slide2" },
          { key: "layout", condition: "!=", value: "slide3" },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-inner { padding: {{contentWrapPadding}}; }",
      },
    ],
  },
  slideWrapMargin: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: "slide6" }],
        selector:
          "{{ULTP}} .ultp-block-content { margin: {{slideWrapMargin}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide7" }],
        selector:
          "{{ULTP}} .ultp-block-content { margin: {{slideWrapMargin}}; }",
      },
      {
        depends: [{ key: "layout", condition: "==", value: "slide8" }],
        selector:
          "{{ULTP}} .ultp-block-content { margin: {{slideWrapMargin}}; }",
      },
    ],
  },
  /*==========================
      Heading Settings
  ==========================*/
  headingText: { type: "string", default: "Post Slider #2" },

  /* ========== Advanced Settings ========== */ 
  ...commonAttributes([
    'advanceAttr',
    'heading',
    'meta',
    'category',
    'readMore',
    'query'
  ], 
  [], 
  [ 
    // Post Slider 2
    {
      key: 'queryNumPosts',
      default: { lg: 5 }
    },
    {
      key: 'queryNumber',
      default: 5
    },
    // loadingColor
    {   
      key:'loadingColor', style: [{
        selector:
          `{{ULTP}} .ultp-loading .ultp-loading-blocks div , 
          {{ULTP}} .ultp-loading .ultp-loading-spinner div { --loading-block-color: {{loadingColor}}; }`,
        }] 
    }, 
    {
      key: 'metaStyle',
      default: 'noIcon'
    },
    {
      key: 'metaList',
      default: '["metaAuthor","metaDate"]'
    },
    {
      key: 'metaColor',
      default: '#F3F3F3'
    },
    {
      key: 'metaSeparatorColor',
      default: '#b3b3b3'
    },
    {
      key: 'metaSpacing', default: { lg: "10", unit: "px" },
    },
    // category 
    {
      key: 'catTypo',
      default: { 
        openTypography: 1, 
          size: { lg: 12, unit: "px" }, 
          height: { lg: 18, unit: "px" },
          spacing: {
            lg: 7.32002,
            unit: "px"
          },
          transform: "uppercase",
          weight: "300",
          decoration: "none",
          family: ""
        }
    },
    {
      key: "catSacing",
      default: { lg: { top: 0, bottom: 0, unit: "px" } }
    },
    // read More
    // {
    //   key: 'catHoverColor',
    //   default: 'var(--postx_preset_Secondary_color)' 
    // }
    // read More 
    {
      key: 'readMoreColor', default: "var(--postx_preset_Primary_color)",
    },
    {
      key: 'catBgHoverColor',
      default: { openColor: 0, type: "color", color:  '#b3b3b3' },
    },
    {
      key: 'readMoreBgColor',
      default: { openColor: 0, type: "color", color: "#037fff" },
    },
    {
      key: 'readMoreHoverColor',
      default: "var(--postx_preset_Secondary_color)",
    },
    {
      key: 'readMoreBgHoverColor',
      default: { openColor: 0, type: "color", color: "#0c32d8" },
    },
    {
      key: 'readMoreSacing',
      default: { lg: { top: 30, bottom: "", left: "", right: "", unit: "px" } },
    },
    {
      key: 'readMorePadding',
      default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    }
  ]),
  
  //compatibility
  V4_1_0_CompCheck,
  ...getDCBlockAttributes(),
  /*==========================
      Query Settings
  ==========================*/
  // post slider 2
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 5 }
  // },
  // queryNumber: {
  //   type: "string",
  //   default: 5,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // metaKey: {
  //   type: "string",
  //   default: "custom_meta_key",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrder: { type: "string", default: "desc" },
  // queryInclude: { type: "string", default: "" },
  // queryExclude: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },
  /*==========================
      Meta Settings
  ==========================*/
  // post slider 2
  // metaShow: { type: "boolean", default: true },
  // metaPosition: { type: "string", default: "top" },
  // metaStyle: { type: "string", default: "noIcon" },
  // authorLink: { type: "boolean", default: true },
  // metaSeparator: { type: "string", default: "dot" },
  // metaList: { type: "string", default: '["metaAuthor","metaDate"]' },
  // metaMinText: { type: "string", default: "min read" },
  // metaAuthorPrefix: { type: "string", default: "" },
  // metaDateFormat: { type: "string", default: "j M Y" },
  // metaTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     weight: "300",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element, 
  //         {{ULTP}} .ultp-block-item span.ultp-block-meta-element a`,
  //     },
  //   ],
  // },
  // metaColor: {
  //   type: "string",
  //   default: "#FFFFFFA8",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { color: {{metaColor}}; } 
  //         {{ULTP}} span.ultp-block-meta-element svg { fill: {{metaColor}}; } 
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element a { color: {{metaColor}}; }`,
  //     },
  //   ],
  // },
  // metaHoverColor: {
  //   type: "string",
  //   default: "#a5a5a5",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element:hover , 
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:hover a { color: {{metaHoverColor}}; } 
  //         {{ULTP}} span.ultp-block-meta-element:hover svg { fill: {{metaHoverColor}}; }`,
  //     },
  //   ],
  // },
  // metaSeparatorColor: {
  //   type: "string",
  //   default: "#b3b3b3",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: 
  //       `{{ULTP}} .ultp-block-meta-dot span:after { background:{{metaSeparatorColor}}; } 
  //       {{ULTP}} span.ultp-block-meta-element:after { color:{{metaSeparatorColor}}; }`,
  //     },
  //   ],
  // },
  // metaSpacing: {
  //   type: "object",
  //   default: { lg: "10", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { margin-right:{{metaSpacing}}; } 
  //         {{ULTP}} span.ultp-block-meta-element { padding-left: {{metaSpacing}}; }`,
  //     },
  //   ],
  // },
  // metaMargin: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { margin:{{metaMargin}}; }",
  //     },
  //   ],
  // },
  // metaPadding: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "5", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { padding:{{metaPadding}}; }",
  //     },
  //   ],
  // },
  // metaBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: "0", bottom: "0", left: "0" },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta",
  //     },
  //   ],
  // },
  // metaBg: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { background:{{metaBg}}; }",
  //     },
  //   ],
  // },

  /*==========================
      Category Settings
  ==========================*/
  // slider 2
  // catShow: { type: "boolean", default: true },
  // maxTaxonomy: { type: "string", default: "30" },
  // taxonomy: { type: "string", default: "category" },
  // catStyle: { type: "string", default: "classic" },
  // catPosition: { type: "string", default: "aboveTitle" },
  // customCatColor: { type: "boolean", default: false },
  // seperatorLink: {
  //   type: "string",
  //   default: ultp_data.category_url,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // onlyCatColor: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // catLineWidth: {
  //   type: "object",
  //   default: { lg: "20" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before { width:{{catLineWidth}}px; }`,
  //     },
  //   ],
  // },
  // catLineSpacing: {
  //   type: "object",
  //   default: { lg: "30" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth .ultp-category-in { padding-left: {{catLineSpacing}}px; padding-right: {{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in { padding-left: {{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderRight .ultp-category-in { padding-right:{{catLineSpacing}}px; }`,
  //     },
  //   ],
  // },
  // catLineColor: {
  //   type: "string",
  //   default: "#000",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before { background:{{catLineColor}}; }`,
  //     },
  //   ],
  // },
  // catLineHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderLeft:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderRight:hover .ultp-category-in:before { background:{{catLineHoverColor}}; }`,
  //     },
  //   ],
  // },
  // catTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: 15, unit: "px" },
  //     spacing: { lg: 7.3200000000000003, unit: "px" },
  //     transform: "uppercase",
  //     weight: "300",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //   ],
  // },
  // catBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catRadius: {
  //   type: "object",
  //   default: { lg: "2", unit: "px" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //   ],
  // },
  // catHoverColor: {
  //   type: "string",
  //   default: "#a5a5a5",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //   ],
  // },
  // catBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catSacing: {
  //   type: "object",
  //   default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-category-grid { margin:{{catSacing}}; }",
  //     },
  //   ],
  // },
  // catPadding: {
  //   type: "object",
  //   default: { lg: { top: 8, bottom: 6, left: 16, right: 16, unit: "px" } },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //   ],
  // },

  /*==========================
      ReadMore Settings
  ==========================*/
  // Slider 2
  // readMore: { type: "boolean", default: false },
  // readMoreText: { type: "string", default: "" },
  // readMoreIcon: { type: "string", default: "rightArrowLg" },
  // readMoreTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "500",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreIconSize: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore svg { width:{{readMoreIconSize}}; }",
  //     },
  //   ],
  // },
  // readMoreColor: {
  //   type: "string",
  //   default: "#000",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { color:{{readMoreColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a svg { fill:{{readMoreColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "#037fff" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreRadius: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { border-radius:{{readMoreRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a:hover { color:{{readMoreHoverColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a:hover svg { fill:{{readMoreHoverColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "#0c32d8" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverRadius: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreSacing: {
  //   type: "object",
  //   default: { lg: { top: 30, bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore { margin:{{readMoreSacing}}; }",
  //     },
  //   ],
  // },
  // readMorePadding: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a { padding:{{readMorePadding}}; }",
  //     },
  //   ],
  // },
  /*==========================
      Heading Settings
  ==========================*/
  // headingURL: { type: "string", default: "" },
  // headingBtnText: {
  //   type: "string",
  //   default: "View More",
  //   style: [
  //     { depends: [{ key: "headingStyle", condition: "==", value: "style11" }] },
  //   ],
  // },
  // headingStyle: { type: "string", default: "style1" },
  // headingTag: { type: "string", default: "h2" },
  // headingAlign: {
  //   type: "string",
  //   default: "left",
  //   style: [
  //     {
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner, 
  //         {{ULTP}} .ultp-sub-heading-inner { text-align:{{headingAlign}}; }`,
  //     },
  //   ],
  // },
  // headingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "20", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "700",
  //   },
  //   style: [{ selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-inner" }],
  // },
  // headingColor: {
  //   type: "string",
  //   default: "#0e1523",
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { color:{{headingColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor: {
  //   type: "string",
  //   default: "#0e1523",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before, 
  //         {{ULTP}} .ultp-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor2: {
  //   type: "string",
  //   default: "#e5e5e5",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //   ],
  // },
  // headingBg: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-style5 .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } 
  //         {{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span, 
  //         {{ULTP}} .ultp-heading-inner span:after { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } 
  //         {{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }`,
  //     },
  //   ],
  // },
  // headingBg2: {
  //   type: "string",
  //   default: "#e5e5e5",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg2}}; }",
  //     },
  //   ],
  // },
  // headingBtnTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "14", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-btn",
  //     },
  //   ],
  // },
  // headingBtnColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn { color:{{headingBtnColor}}; } 
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn svg { fill:{{headingBtnColor}}; }`,
  //     },
  //   ],
  // },
  // headingBtnHoverColor: {
  //   type: "string",
  //   default: "#0a31da",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover { color:{{headingBtnHoverColor}}; } 
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }`,
  //     },
  //   ],
  // },
  // headingBorder: {
  //   type: "string",
  //   default: "3",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before, 
  //         {{ULTP}} .ultp-heading-inner span:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //   ],
  // },
  // headingSpacing: {
  //   type: "object",
  //   default: { lg: 20, sm: 10, unit: "px" },
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-wrap { margin-top:0; margin-bottom:{{headingSpacing}}; }",
  //     },
  //   ],
  // },
  // headingRadius: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //   ],
  // },
  // headingPadding: {
  //   type: "object",
  //   default: { lg: { unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //   ],
  // },
  // subHeadingShow: { type: "boolean", default: false },
  // subHeadingText: {
  //   type: "string",
  //   default:
  //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.",
  //   style: [
  //     { depends: [{ key: "subHeadingShow", condition: "==", value: true }] },
  //   ],
  // },
  // subHeadingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "16", unit: "px" },
  //     spacing: { lg: "0", unit: "px" },
  //     height: { lg: "27", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "500",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-sub-heading div",
  //     },
  //   ],
  // },
  // subHeadingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading div { color:{{subHeadingColor}}; }",
  //     },
  //   ],
  // },
  // subHeadingSpacing: {
  //   type: "object",
  //   default: { lg: { top: "8", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading-inner { margin:{{subHeadingSpacing}}; }",
  //     },
  //   ],
  // },
  // enableWidth: {
  //   type: 'toggle',
  //   default: false,
  //   style: [{
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //   }],
  // },
  // customWidth: {
  //     type: "object",
  //     default: { lg: { top: "", unit: "px" }},
  //     style: [{
  //         depends: [
  //             { key: "subHeadingShow", condition: "==", value: true },
  //             { key: "enableWidth", condition: "==", value: true }
  //         ],
  //         selector: "{{ULTP}} .ultp-sub-heading-inner { max-width:{{customWidth}}; }",
  //     }],
  // },

};
export default attributes;
