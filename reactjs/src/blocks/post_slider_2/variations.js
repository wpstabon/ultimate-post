export const variation = {
    slide1: {
        /* == Slide Option ==*/
        autoPlay: true,
        excerptShow: false,
        readMore: false,
        imgOverlay: false,
        sliderGap: 0,
        height: { lg: 550, unit: "px" },
        slidesCenterPadding: {
            lg: "160",
            sm: "100",
            xs: "50",
        },
        imgBgBlur: "0",

        /* == Content Wrapper ==*/
        contentWrapBg: "#00000069",
        contentWrapHoverBg: "#00000069",
        slideBgBlur: "",
        contentAlign: "center",
        contentVerticalPosition: "bottomPosition",
        contenWraptHeight: {
            lg: "",
            ulg: "",
        },
        contentWrapRadius: { lg: "10" },
        contentWrapHoverRadius: {
            lg: "10",
        },
        contentWrapPadding: {
            lg: {
                top: "27",
                bottom: "27",
                left: "24",
                right: "24",
                unit: "px",
            },
            sm: {
                top: "27",
                bottom: "27",
                left: "16",
                right: "16",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleColor: "#fff",
        titleHoverColor: "rgba(107,107,107,1)",
        titleTypo: {
            decoration: "none",
            family: "",
            height: {
                lg: "36",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "24", unit: "px" },
            weight: "300",
            transform: "uppercase",
        },

        /* == Metalist ==*/
        metaColor: "#FFFFFFA8",
        metaHoverColor: "#a5a5a5",
        metaTypo: {
            decoration: "none",
            family: "",
            height: {
                lg: "20",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "300",
        },
        metaStyle: "noIcon",
        metaList: '["metaAuthor","metaDate"]',
        metaSpacing: {
            lg: 6,
            unit: "px",
            ulg: "px",
        },
        metaSeparator: "emptyspace",
        metaDateFormat: "j M Y",
        metaAuthorPrefix: "",
        contenWraptWidth: {
            lg: "100",
            unit: "%",
            ulg: "%",
        },
        metaColor: "#FFFFFFA8",
        metaSeparator: "dot",
        metaSpacing: {
            lg: "13",
            unit: "px",
            ulg: "px",
        },

        /* == Category ==*/
        catRadius: { lg: "0", unit: "px" },
        catBgColor: {
            color: "",
            openColor: 1,
            type: "color",
        },
        catBgHoverColor: {
            color: "",
            openColor: 1,
            type: "color",
        },
        catTypo: {
            decoration: "none",
            family: "",
            height: {
                lg: "15",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "300",
            transform: "uppercase",
            spacing: {
                lg: "7.32",
                unit: "px",
            },
        },

        /* == Dot ==*/
        dotWidth: { lg: "5", unit: "px" },
        dotHeight: { lg: "5", unit: "px" },
        dotHoverWidth: {
            lg: "8",
            unit: "px",
        },
        dotHoverHeight: {
            lg: "8",
            unit: "px",
        },
        dotVartical: {
            lg: "-20",
            unit: "px",
        },
        dotBg: "#9b9b9b",
        dotHoverBg: "#9b9b9b",
        dotRadius: {
            lg: {
                top: "30",
                bottom: "30",
                left: "30",
                right: "30",
                unit: "px",
            },
        },
        dotHoverRadius: {
            lg: {
                top: "30",
                bottom: "30",
                left: "30",
                right: "30",
                unit: "px",
            },
        },

        /* == Arrow ==*/
        arrowBg: "",
        arrowHoverBg: "",
        arrowColor: "#000",
        arrowHoverColor: "#000",
        arrowSize: { lg: "80", unit: "px" },
        arrowVartical: {
            lg: "45",
            sm: "16",
            xs: "0",
            unit: "px",
        },
        arrowStyle: "leftAngle2#rightAngle2",
        arrowBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(255,255,255,1)",
            openBorder: 1,
        },
        arrowHoverBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(67,67,67,1)",
            openBorder: 1,
        },
        wrapMargin: {
            lg: {
                top: "",
                bottom: "50",
                unit: "px",
                left: "",
                right: "",
            },
        },
    },
    slide2: {
        /* == Slide Control ==*/
        autoPlay: true,
        fade: true,
        slidesToShow: {
            lg: "1",
            sm: "1",
            xs: "1",
        },
        excerptShow: true,
        readMore: true,
        imgOverlay: false,
        sliderGap: 0,
        height: { lg: 550, unit: "px" },
        imgWidth: {
            xs: "20",
            ulg: "%",
            unit: "%",
        },

        /* == Content Wrapper ==*/
        contenWraptWidth: {
            lg: 71,
            unit: "%",
            ulg: "%",
            usm: "%",
            sm: 100,
            xs: 100,
        },
        contentWrapBg: "#644737",
        contentWrapHoverBg: "#644737",
        contentAlign: "left",
        contenWraptHeight: {
            lg: "100",
            ulg: "%",
        },
        contentWrapRadius: { lg: "0" },
        contentWrapHoverRadius: { lg: "0" },
        contentVerticalPosition: "middlePosition",
        contentWrapPadding: {
            lg: {
                top: "0",
                bottom: "0",
                left: "60",
                right: "30",
                unit: "px",
            },
            xs: {
                top: "24",
                bottom: "24",
                left: "24",
                right: "24",
                unit: "px",
            },
        },

        /* == Title ==*/
        titleLength: "12",
        titleColor: "#fff",
        titleHoverColor: "#fff6",
        titleTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            openTypography: 1,
            size: {
                lg: "28",
                xs: "22",
                unit: "px",
            },
            weight: "400",
        },
        titlePadding: {
            lg: {
                top: "15",
                bottom: "5",
                unit: "px",
            },
        },

        /* == Meta ==*/
        metaColor: "#FFFFFFA8",
        metaHoverColor: "#a5a5a5",
        metaTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            openTypography: 1,
            size: { lg: "16", unit: "px" },
            weight: "300",
        },
        catTypo: {
            decoration: "none",
            height: {
                lg: "12",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            spacing: { lg: "", unit: "px" },
            weight: "300",
            transform: "",
        },

        /* == Excerpt ==*/
        excerptColor: "#d7d7d8d8",
        excerptTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            openTypography: 1,
            size: { lg: "16", unit: "px" },
            weight: "300",
            transform: "",
        },

        /* == Category ==*/
        catRadius: { lg: "0", unit: "px" },
        catBgColor: {
            color: "#ffffff21",
            openColor: 1,
            type: "color",
        },
        catBgHoverColor: {
            color: "#ffffff21",
            openColor: 1,
            type: "color",
        },

        /* == Read More ==*/
        readMoreColor: "#fff",
        readMoreHoverColor: "#fff7",
        readMoreTypo: {
            decoration: "none",
            family: "",
            height: {
                lg: "12",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "300",
            transform: "uppercase",
        },

        /* == dot ==*/
        dotBg: "#644737",
        dotHoverBg: "#644737",
        dotWidth: { lg: "6", unit: "px" },
        dotHeight: { lg: "6", unit: "px" },
        dotHoverWidth: {
            lg: "10",
            unit: "px",
        },
        dotHoverHeight: {
            lg: "10",
            unit: "px",
        },
        dotVartical: {
            lg: "-27",
            unit: "px",
        },
        dotRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        dotHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        /* == Arrow ==*/
        metaSeparator: "dot",
        metaSpacing: {
            lg: "13",
            unit: "px",
            ulg: "px",
        },

        /* == Arrow ==*/
        arrowBg: "#503a2d",
        arrowHoverBg: "#614F44",
        arrowColor: "#fff",
        arrowHoverColor: "#fff",
        arrowWidth: {
            lg: "40",
            unit: "px",
        },
        arrowHeight: {
            lg: "40",
            unit: "px",
        },
        arrowSize: { lg: "20", unit: "px" },
        arrowVartical: {
            lg: "0",
            ulg: "px",
        },
        arrowRadius: { lg: "0", ulg: "px" },
        arrowHoverRadius: {
            lg: "0",
            ulg: "px",
        },
        arrowStyle: "leftAngle2#rightAngle2",
        arrowBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(255,255,255,1)",
            openBorder: 1,
        },
        arrowHoverBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(67,67,67,1)",
            openBorder: 1,
        },

        wrapMargin: {
            lg: {
                top: "",
                bottom: "50",
                unit: "px",
                left: "",
                right: "",
            },
        },
    },
    slide3: {
        /* == Slide Option ==*/
        autoPlay: true,
        excerptShow: false,
        readMore: false,
        height: { lg: 550, unit: "px" },
        contentAlign: "center",
        imgHeight: { lg: 50, unit: "%" },
        slidesToShow: {
            lg: "3",
            sm: "2",
            xs: "1",
        },
        fade: false,
        sliderGap: "10",
        dots: false,
        layout: "slide3",
        imgRadius: {
            lg: {
                top: "10",
                right: "10",
                bottom: "",
                left: "",
                unit: "px",
            },
            unit: "px",
        },
        imgOverlay: false,

        /* == Content Wrapper ==*/
        contentWrapBg: "rgba(241,241,241,1)",
        contentWrapHoverBg: "",
        contenWraptHeight: {
            lg: 45,
            unit: "%",
        },
        contentWrapHoverRadius: {
            lg: {
                top: "",
                bottom: "10",
                left: "10",
                right: "",
                unit: "px",
            },
        },
        contentWrapRadius: {
            lg: {
                top: "",
                bottom: "10",
                left: "10",
                right: "",
                unit: "px",
            },
        },
        contentWrapHoverRadius: {
            lg: {
                top: "",
                bottom: "10",
                left: "10",
                right: "",
                unit: "px",
            },
        },
        contentWrapPadding: {
            lg: {
                top: "16",
                bottom: "32",
                left: "16",
                right: "16",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleLength: "8",
        titleColor: "rgba(22,22,22,1)",
        titleHoverColor: "rgba(22,22,22, .4)",
        titleTypo: {
            openTypography: 1,
            size: { lg: "22", unit: "px" },
            height: {
                lg: "32",
                unit: "px",
            },
            decoration: "none",
            family: "",
            weight: "700",
            transform: "capitalize",
        },

        /* == Arrow Style ==*/
        arrowStyle: "leftArrowLg#rightArrowLg",
        arrowWidth: {
            lg: "56",
            unit: "px",
        },
        arrowHeight: {
            lg: "56",
            unit: "px",
        },
        arrowSize: {
            lg: "27",
            nit: "px",
            ulg: "px",
        },
        arrowVartical: {
            lg: "-32",
            unit: "px",
            ulg: "px",
        },
        arrowColor: "rgba(255,255,255,1)",
        arrowHoverColor: "rgba(67,67,67,1)",
        arrowBg: "rgba(67,67,67,1)",
        arrowHoverBg: "rgba(255,255,255,1)",
        arrowBorder: {
            width: {
                top: "2",
                right: "2",
                bottom: "2",
                left: "2",
                unit: "px",
            },
            type: "solid",
            color: "rgba(255,255,255,1)",
            openBorder: 1,
        },
        arrowHoverBorder: {
            width: {
                top: "2",
                right: "2",
                bottom: "2",
                left: "2",
                unit: "px",
            },
            type: "solid",
            color: "rgba(67,67,67,1)",
            openBorder: 1,
        },
        arrowRadius: {
            lg: {
                top: "51",
                bottom: "51",
                left: "51",
                right: "51",
                unit: "px",
            },
        },
        arrowHoverRadius: {
            lg: {
                top: "51",
                bottom: "51",
                left: "51",
                right: "51",
                unit: "px",
            },
        },

        /* == Category Style ==*/
        catSacing: {
            lg: {
                top: "5",
                bottom: "10",
                left: "",
                right: "",
                unit: "px",
            },
        },
        catPadding: {
            lg: {
                top: "4",
                bottom: "4",
                left: "9",
                right: "9",
                unit: "px",
            },
        },
        catTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            spacing: { lg: "", unit: "px" },
            transform: "uppercase",
            weight: "400",
            decoration: "none",
            family: "",
        },
        catBgColor: {
            openColor: 1,
            type: "color",
            color: "rgba(97,59,255,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catBgHoverColor: {
            openColor: 1,
            type: "color",
            color: "rgba(97,59,255,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        /* == Meta Style ==*/
        metaSeparator: "verticalbar",
        metaColor: "rgba(118,118,118,1)",
        metaHoverColor: "rgba(97,59,255,1)",
        metaSpacing: {
            lg: "10",
            unit: "px",
            ulg: "px",
        },
        metaTypo: {
            openTypography: 1,
            size: { lg: "11", unit: "px" },
            height: { lg: "", unit: "px" },
            weight: "400",
            decoration: "none",
            family: "",
            transform: "uppercase",
        },

        /* == Block Wrapper ==*/
        wrapMargin: {
            lg: {
                top: "",
                bottom: "0",
                unit: "px",
                left: "30",
                right: "30",
            },
        },
    },
    slide4: {
        /* == Slide Option ==*/
        autoPlay: true,
        sliderGap: 0,
        excerptShow: true,
        readMore: false,
        layout: "slide4",
        contentAlign: "left",
        centerItemScale: { lg: "1.08" },
        slidesToShow: {
            lg: "1",
            sm: "1",
            xs: "1",
        },
        height: {
            lg: 500,
            unit: "px",
            ulg: "px",
        },
        slidesCenterPadding: {
            lg: "180",
            xs: "30",
        },
        slidesTopPadding: "20",

        /* == Content Style ==*/
        slideBgBlur: "",
        contentWrapBg: "",
        contentWrapHoverBg: "",
        contentWrapRadius: { lg: "0" },
        contentWrapHoverRadius: { lg: "0" },
        contenWraptHeight: {
            lg: "",
            ulg: "%",
        },
        contenWraptWidth: {
            lg: "100",
            unit: "%",
            ulg: "%",
        },
        contentWrapPadding: {
            lg: {
                top: "",
                bottom: "50",
                left: "35",
                right: "35",
                unit: "px",
            },
        },

        /* == Content Style ==*/
        titleLength: "9",
        titleColor: "#fff",
        titleHoverColor: "#a5a5a5",
        titlePadding: {
            lg: {
                top: "15",
                bottom: "5",
                unit: "px",
            },
        },
        titleTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            openTypography: 1,
            size: {
                lg: "32",
                xs: "22",
                unit: "px",
            },
            weight: "700",
            style: "italic",
        },

        /* == Arrow Style ==*/
        arrowSize: {
            lg: "31",
            unit: "px",
            ulg: "px",
        },
        arrowWidth: {
            lg: "40",
            unit: "px",
        },
        arrowHeight: {
            lg: "80",
            unit: "px",
            ulg: "px",
        },
        arrowVartical: {
            lg: "100",
            sm: "24",
            xs: "16",
            ulg: "px",
            usm: "px",
            uxs: "px",
        },
        arrowColor: "#fff",
        arrowHoverColor: "#fff",
        arrowBg: "rgba(44,44,44,0.7)",
        arrowHoverBg: "#503a2d",
        arrowBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(255,255,255,1)",
            openBorder: 1,
        },
        arrowHoverBorder: {
            width: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            type: "solid",
            color: "rgba(67,67,67,1)",
            openBorder: 1,
        },
        arrowRadius: { lg: "0", ulg: "px" },
        arrowHoverRadius: {
            lg: "0",
            ulg: "px",
        },

        /* == Dot Style ==*/
        dotWidth: { lg: "15", unit: "px" },
        dotHeight: {
            lg: "2",
            unit: "px",
            ulg: "px",
        },
        dotHoverWidth: {
            lg: "22",
            unit: "px",
        },
        dotHoverHeight: {
            lg: "3",
            unit: "px",
            ulg: "px",
        },
        dotVartical: {
            lg: "15",
            unit: "px",
            ulg: "px",
        },
        dotBg: "rgba(255,255,255,1)",
        dotHoverBg: "rgba(255,255,255,1)",
        dotRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        dotHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        /* == Category Style ==*/
        catTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            spacing: { lg: "", unit: "px" },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "400",
            transform: "uppercase",
        },
        catColor: "rgba(96,96,96,1)",
        catHoverColor: "#a5a5a5",
        catBgColor: {
            openColor: 1,
            type: "color",
            color: "rgba(255,255,255,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catBgHoverColor: {
            openColor: 1,
            type: "color",
            color: "rgba(255,255,255,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catRadius: { lg: "0", unit: "px" },
        catSacing: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
        catPadding: {
            lg: {
                top: "5",
                bottom: "5",
                left: "8",
                right: "8",
                unit: "px",
            },
        },

        /* == Image Style ==*/
        imgOverlay: true,
        imgOverlayType: "custom",
        overlayColor: {
            openColor: 1,
            type: "gradient",
            color: "#0e1523",
            gradient: "linear-gradient(0deg,rgb(0,0,0) 0%,rgba(7,7,7,0) 100%)",
            clip: false,
        },
        imgOpacity: "0.61",
        imgHeight: { lg: "255", ulg: "px" },
        imgRadius: {
            lg: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            unit: "px",
        },

        /* == Readmore Style ==*/
        readMoreTypo: {
            decoration: "none",
            family: "",
            height: {
                lg: "12",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "300",
            transform: "uppercase",
        },
        readMoreColor: "#fff",
        readMoreHoverColor: "#a5a5a5",

        /* == Meta Style ==*/
        metaSeparator: "slash",
        metaAuthorPrefix: "By",
        metaTypo: {
            decoration: "none",
            height: { lg: "", unit: "px" },
            openTypography: 1,
            size: { lg: "12", unit: "px" },
            weight: "400",
        },
        metaColor: "rgba(255,255,255,1)",
        metaHoverColor: "#a5a5a5",
        metaSpacing: {
            lg: "6",
            unit: "px",
            ulg: "px",
        },

        /* == Excerpt Style ==*/
        excerptLimit: "23",
        excerptColor: "rgba(255,255,255,0.61)",
        excerptTypo: {
            decoration: "none",
            height: {
                lg: "28",
                xs: "16",
                unit: "px",
            },
            openTypography: 1,
            size: { lg: "16", unit: "px" },
            weight: "400",
            transform: "",
        },
        excerptPadding: {
            lg: {
                top: "10",
                bottom: "10",
                unit: "px",
            },
        },

        /* == Block Wrapper Style ==*/
        wrapRadius: {
            lg: {
                top: "23",
                right: "23",
                bottom: "23",
                left: "23",
                unit: "px",
            },
            unit: "px",
        },
        wrapMargin: {
            lg: {
                top: "",
                bottom: "",
                unit: "px",
                left: "",
                right: "",
            },
        },
    },
    slide5: {
        /* == Slider Style ==*/
        fade: true,
        autoPlay: true,
        readMore: false,
        excerptShow: false,
        imgOverlay: false,
        slideBgBlur: "6",
        contentAlign: "left",
        slidesToShow: {
            lg: "1",
            sm: "1",
            xs: "1",
        },
        layout: "slide5",
        imgWidth: { lg: "75", ulg: "%" },

        /* == Content Wrapper ==*/
        contentHorizontalPosition: "leftPosition",
        contentVerticalPosition: "bottomPosition",
        contenWraptWidth: {
            lg: "57",
            xs: "100",
            unit: "%",
            ulg: "%",
            uxs: "%",
        },
        contentWrapBg: "rgba(255,255,255,0.75)",
        contentWrapHoverBg: "rgba(255,255,255,0.75)",
        contentWrapRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapPadding: {
            lg: {
                top: "20",
                bottom: "20",
                left: "24",
                right: "24",
                unit: "px",
            },
        },
        slideWrapMargin: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleColor: "rgba(40,74,0,1)",
        titleHoverColor: "rgba(40,74,0, .6)",
        titleTypo: {
            openTypography: 1,
            size: { lg: "28", unit: "px" },
            height: { lg: "", unit: "px" },
            decoration: "none",
            family: "",
            weight: "700",
            transform: "capitalize",
        },
        titlePadding: {
            lg: {
                top: "10",
                bottom: "0",
                unit: "px",
            },
        },
        titleLength: "5",

        /* == Arrow Style ==*/
        arrowPos: "right",
        arrowSize: {
            lg: "20",
            unit: "px",
            ulg: "px",
        },
        arrowWidth: {
            lg: "38",
            unit: "px",
            ulg: "px",
        },
        arrowHeight: {
            lg: "38",
            unit: "px",
            ulg: "px",
        },
        arrowVartical: {
            lg: "278",
            unit: "px",
            ulg: "px",
        },
        prevArrowPos: {
            lg: "62",
            unit: "px",
        },
        nextArrowPos: {
            lg: "21",
            unit: "px",
        },
        arrowColor: "#284a00",
        arrowHoverColor: "#284a00",
        arrowBg: "rgba(255,255,255,1)",
        arrowHoverBg: "#e2e2e2",
        arrowRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        arrowHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        /* == Dot Style ==*/
        dotWidth: { lg: "6", unit: "px" },
        dotHeight: {
            lg: "6",
            unit: "px",
            ulg: "px",
        },
        dotHoverWidth: {
            lg: "10",
            unit: "px",
        },
        dotHoverHeight: {
            lg: "10",
            unit: "px",
            ulg: "px",
        },
        dotSpace: {
            lg: "4",
            unit: "px",
            ulg: "px",
        },
        dotVartical: {
            lg: "109",
            xs: "200",
            unit: "px",
            ulg: "px",
            uxs: "px",
        },
        dotHorizontal: {
            lg: "0",
            ulg: "px",
        },
        dotBg: "rgba(255,255,255,1)",
        dotHoverBg: "rgba(117,137,78,1)",

        /* == Category Style==*/
        catTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            spacing: { lg: "", unit: "px" },
            transform: "uppercase",
            weight: "500",
            decoration: "none",
            family: "",
        },
        catColor: "rgba(40,74,0,1)",
        catHoverColor: "rgba(40,74,0,.5)",
        catBgColor: {
            openColor: 1,
            type: "color",
            color: "rgba(222,197,93,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catBgHoverColor: {
            openColor: 1,
            type: "color",
            color: "rgba(222,197,93,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catSacing: {
            lg: {
                top: 0,
                bottom: "5",
                left: 0,
                right: 0,
                unit: "px",
            },
        },
        catPadding: {
            lg: {
                top: "4",
                bottom: "4",
                left: "8",
                right: "8",
                unit: "px",
            },
        },

        /* == Meta Style==*/
        metaList: '["metaDate"]',
        metaTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            weight: "400",
            decoration: "none",
            family: "",
        },
        metaColor: "rgba(40,74,0,1)",
        metaHoverColor: "rgba(40,74,0,.5)",
        metaMargin: {
            lg: {
                top: "15",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
        metaPadding: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
    },
    slide6: {
        /* == Slider Style ==*/
        autoPlay: true,
        dots: false,
        fade: false,
        imgOverlay: false,
        excerptShow: false,
        readMore: false,
        layout: "slide6",
        height: {
            lg: 480,
            unit: "px",
            ulg: "px",
        },

        /* == Content Wrapper Style ==*/
        contentVerticalPosition: "middlePosition",
        contentHorizontalPosition: "rightPosition",
        contentAlign: "left",
        contenWraptWidth: {
            lg: "482",
            sm: "70",
            unit: "%",
            ulg: "px",
            uxs: "%",
            xs: "90",
            usm: "%",
        },
        contenWraptHeight: {
            lg: "288",
            ulg: "px",
            uxs: "px",
            xs: "250",
            usm: "%",
            sm: "",
        },
        contentWrapBg: "rgba(255,255,255,1)",
        contentWrapHoverBg: "rgba(255,255,255,1)",
        contentWrapBorder: {
            width: {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1,
            },
            type: "solid",
            color: "rgba(112,112,112,1)",
            openBorder: 0,
        },
        contentWrapRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapPadding: {
            lg: {
                top: "35",
                bottom: "0",
                left: "35",
                right: "35",
                unit: "px",
            },
            xs: {
                top: "16",
                right: "16",
                bottom: "16",
                left: "16",
                unit: "px",
            },
        },
        contentWrapBorder: {
            width: {
                top: "1",
                right: "1",
                bottom: "1",
                left: "1",
                unit: "px",
            },
            type: "solid",
            color: "#707070",
            openBorder: 1,
        },
        slideWrapMargin: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleColor: "rgba(60,60,60,1)",
        titleHoverColor: "rgba(151,148,148,1)",
        titleTypo: {
            openTypography: 1,
            size: {
                lg: "26",
                xs: "20",
                unit: "px",
            },
            height: {
                lg: "36",
                unit: "px",
            },
            decoration: "none",
            family: "",
            weight: "500",
            transform: "",
        },
        titlePadding: {
            lg: {
                top: "20",
                bottom: "12",
                unit: "px",
                right: "0",
                left: "0",
            },
        },
        titleLength: "10",

        /* == Arrow Style ==*/
        arrowSize: {
            lg: "25",
            unit: "px",
            ulg: "px",
        },
        arrowWidth: {
            lg: "52",
            unit: "px",
            ulg: "px",
        },
        arrowHeight: {
            lg: "52",
            unit: "px",
            ulg: "px",
        },
        arrowPos: "right",
        arrowVartical: {
            lg: "358",
            unit: "px",
            ulg: "px",
            uxs: "px",
            xs: "55",
        },
        prevArrowPos: {
            lg: "429",
            unit: "px",
            xs: "53",
            xs: "",
        },
        nextArrowPos: {
            lg: "377",
            unit: "px",
            xs: "0",
            xs: "",
        },

        arrowColor: "rgba(255,255,255,1)",
        arrowHoverColor: "",
        arrowBg: "#9f9f9f",
        arrowHoverBg: "#848484",
        arrowRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        arrowHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        /* == Category Style ==*/
        catColor: "rgba(121,175,167,1)",
        catHoverColor: "rgba(121,176,168,0.5)",
        catTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            spacing: { lg: "", unit: "px" },
            transform: "capitalize",
            weight: "300",
            decoration: "none",
            family: "",
        },
        catBgColor: {
            openColor: 0,
            type: "openColor",
            color: "",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },
        catBgHoverColor: {
            openColor: 0,
            type: "openColor",
            color: "#037fff",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },
        catPadding: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        /* == Image Style ==*/
        imgHeight: {
            lg: "100",
            ulg: "%",
            xs: "200",
            ulg: "%",
            uxs: "px",
        },
        imgWidth: {
            lg: "70",
            ulg: "%",
            uxs: "%",
            xs: "100",
        },

        /* == Meta Style ==*/
        metaStyle: "style3",
        metaSeparator: "dot",
        metaColor: "rgba(112,112,112,1)",
        metaHoverColor: "rgba(1,1,1,1)",
        metaSpacing: {
            lg: "12",
            unit: "px",
            ulg: "px",
        },
        metaTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            weight: "400",
            decoration: "none",
            family: "",
        },

        /* == Block Wrapper Style ==*/
        wrapBg: {
            openColor: 0,
            type: "color",
            color: "rgba(237,237,237,1)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },
        wrapOuterPadding: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
    },
    slide7: {
        /* == Slider Option ===*/
        autoPlay: true,
        fade: false,
        dots: false,
        excerptShow: false,
        sliderGap: "0",
        layout: "slide7",
        slideBgBlur: 0,
        llItemScale: { lg: "0.93" },
        centerItemScale: { lg: "0.99" },
        height: { lg: "439", unit: "px" },
        slidesCenterPadding: {
            lg: "213",
            sm: "100",
            xs: "0",
            unit: "px",
        },

        /* == Content Wrapper ===*/
        contentWrapBg: "",
        contentWrapHoverBg: "",
        contentVerticalPosition: "middlePosition",
        contenWraptWidth: {
            lg: "",
            unit: "%",
            ulg: "%",
        },
        contentWrapPadding: {
            lg: {
                top: "",
                bottom: "",
                left: "24",
                right: "24",
                unit: "px",
            },
        },
        contentWrapRadius: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
        contentWrapHoverRadius: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
        slideWrapMargin: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleColor: "rgba(255,255,255,1)",
        titleHoverColor: "rgba(202,202,202,1)",
        titleTypo: {
            openTypography: 1,
            size: { lg: "24", unit: "px" },
            height: {
                lg: "36",
                unit: "px",
            },
            decoration: "none",
            family: "",
            weight: "700",
            transform: "capitalize",
        },
        titlePadding: {
            lg: {
                top: "10",
                bottom: "10",
                unit: "px",
                right: "20",
                left: "20",
            },
        },

        /* == Arrow Style ==*/
        arrowSize: {
            lg: "22",
            unit: "px",
            ulg: "px",
        },
        arrowWidth: {
            lg: "49",
            unit: "px",
            ulg: "px",
        },
        arrowHeight: {
            lg: "49",
            unit: "px",
            ulg: "px",
        },
        arrowVartical: {
            lg: "229",
            unit: "px",
        },
        arrowBg: "rgba(255,255,255,1)",
        arrowHoverBg: "rgba(255,255,255,1)",
        arrowRadius: {
            lg: {
                top: "5",
                bottom: "5",
                left: "5",
                right: "5",
                unit: "px",
            },
        },
        arrowHoverRadius: {
            lg: {
                top: "5",
                bottom: "5",
                left: "5",
                right: "5",
                unit: "px",
            },
        },
        arrowShadow: {
            inset: "",
            width: {
                top: "5",
                right: "6",
                bottom: "31",
                left: "0",
                unit: "px",
            },
            color: "rgba(0,0,0,0.32)",
            openShadow: 1,
        },
        catTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            spacing: {
                lg: "3.6",
                unit: "px",
            },
            transform: "uppercase",
            weight: "500",
            decoration: "none",
            family: "",
        },
        prevArrowPos: {
            lg: "200",
            sm: "80",
            xs: "0",
            unit: "px",
        },
        nextArrowPos: {
            lg: "200",
            sm: "80",
            xs: "0",
            unit: "px",
        },

        /* == Category Style ==*/
        catColor: "rgba(255,255,255,1)",
        catHoverColor: "rgba(255,255,255, .5)",
        catHoverColor: "rgba(207,207,207,1)",
        catBgColor: {
            openColor: 0,
            type: "openColor",
            color: "#000",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },
        catBgHoverColor: {
            openColor: 0,
            type: "openColor",
            color: "#037fff",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        /* == Image Style ==*/
        imgOverlay: true,
        imgOverlayType: "custom",
        overlayColor: {
            openColor: 1,
            type: "gradient",
            color: "#0e1523",
            gradient:
                "linear-gradient(359deg,rgb(0,0,0) 34%,rgba(30,30,30,0.64) 99%)",
            clip: false,
        },
        imgOpacity: "0.55",
        imgGrayScale: {
            lg: "0",
            unit: "%",
            ulg: "%",
        },
        imgRadius: {
            lg: {
                top: "24",
                right: "24",
                bottom: "24",
                left: "24",
                unit: "px",
            },
            unit: "px",
        },

        /* == Meta Style ==*/
        metaList: '["metaAuthor","metaView"]',
        metaStyle: "icon",
        metaSeparator: "dash",
        metaTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: {
                lg: "20",
                unit: "px",
            },
            weight: "400",
            decoration: "none",
            family: "",
        },
        metaColor: "rgba(255,255,255,1)",
        metaSpacing: {
            lg: "15",
            unit: "px",
            ulg: "px",
        },
        metaMargin: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
    },
    slide8: {
        /* == Slider Option ==*/
        autoPlay: true,
        slideBgBlur: 4,
        layout: "slide8",
        excerptShow: false,
        readMore: false,
        imgOverlay: false,

        /* == Content Wrapper ==*/
        contentWrapBg: "#00000059",
        contentWrapHoverBg: "#00000059",
        contentAlign: "left",
        contentWrapRadius: { lg: "0" },
        contentWrapHoverRadius: { lg: "0" },
        contentHorizontalPosition: "leftPosition",
        contenWraptHeight: {
            lg: 335,
            ulg: "px",
            usm: "%",
        },
        contenWraptWidth: {
            lg: 742,
            unit: "%",
            ulg: "px",
            usm: "%",
            sm: 100,
        },
        contentWrapRadiu: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        contentWrapPadding: {
            lg: {
                top: "67",
                bottom: "21",
                left: "67",
                right: "67",
                unit: "px",
            },
            xs: {
                top: "30",
                right: "16",
                bottom: "16",
                left: "16",
                unit: "px",
            },
        },
        slideWrapMargin: {
            lg: {
                top: "",
                bottom: "",
                left: "165",
                right: "",
                unit: "px",
            },
            sm: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
        },

        /* == Title Style ==*/
        titleHoverColor: "rgba(141,141,141,1)",
        titleTypo: {
            openTypography: 1,
            size: {
                lg: "38",
                xs: "25",
                unit: "px",
            },
            height: {
                lg: "1.3",
                xs: "34",
                ulg: "rem",
                uxs: "px",
            },
            decoration: "none",
            family: "",
            weight: "600",
            transform: "capitalize",
        },
        titlePadding: {
            lg: {
                top: "10",
                bottom: "10",
                unit: "px",
            },
        },
        titleLength: "9",

        /* == Arrow Style ==*/
        arrowSize: {
            lg: "21",
            unit: "px",
            ulg: "px",
        },
        arrowWidth: {
            lg: "50",
            unit: "px",
            ulg: "px",
        },
        arrowHeight: {
            lg: "50",
            unit: "px",
            ulg: "px",
        },
        arrowVartical: {
            lg: "190",
            xs: "190",
            unit: "px",
        },
        prevArrowPos: {
            lg: "165",
            unit: "px",
            sm: "0",
        },
        nextArrowPos: {
            lg: "218",
            unit: "px",
            sm: "55",
        },
        arrowColor: "rgba(255,255,255,1)",
        arrowHoverColor: "rgba(255,255,255,1)",
        arrowBg: "rgba(56,56,56,1)",
        arrowHoverBg: "rgba(0,0,0,1)",
        arrowRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        arrowHoverRadius: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },

        dotWidth: { lg: "4", unit: "px" },
        dotHeight: {
            lg: "4",
            unit: "px",
            ulg: "px",
        },
        dotHoverWidth: {
            lg: "8",
            unit: "px",
        },
        dotHoverHeight: {
            lg: "8",
            unit: "px",
            ulg: "px",
        },
        dotVartical: {
            lg: "21",
            unit: "px",
            ulg: "px",
        },
        dotBg: "rgba(255,255,255,1)",
        dotHoverBg: "rgba(255,255,255,1)",

        /* == Category Style ==*/
        catTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: "", unit: "px" },
            spacing: {
                lg: "7.8",
                unit: "px",
            },
            transform: "uppercase",
            weight: "400",
            decoration: "none",
            family: "",
        },
        catBgColor: {
            openColor: 1,
            type: "color",
            color: "rgba(255,255,255,0.29)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catBgHoverColor: {
            openColor: 1,
            type: "color",
            color: "rgba(255,255,255,0.29)",
            gradient:
                "linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)",
            clip: false,
        },

        catRadius: {
            lg: {
                0: "2",
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                unit: "px",
            },
            unit: "px",
        },
        catSacing: {
            lg: {
                top: "0",
                bottom: "0",
                left: "0",
                right: "0",
                unit: "px",
            },
        },
        catPadding: {
            lg: {
                top: "8",
                bottom: "8",
                left: "10",
                right: "10",
                unit: "px",
            },
        },

        /* == Meta Style ==*/
        metaSeparator: "dash",
        metaTypo: {
            openTypography: 1,
            size: { lg: 12, unit: "px" },
            height: { lg: 20, unit: "px" },
            weight: "300",
            decoration: "none",
            family: "",
        },
        metaColor: "rgba(255,255,255,1)",
        metaSpacing: {
            lg: "14",
            unit: "px",
            ulg: "px",
        },
    },
};
