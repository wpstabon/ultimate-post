const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-slider-2/', 'block_docs');

registerBlockType(
    'ultimate-post/post-slider-2', {
        title: __('Post Slider #2','ultimate-post'),
        icon: <div className={`ultp-block-inserter-icon-section`}>
                    <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-slider-2.svg'}/>
                    {!ultp_data.active &&
                        <span className={`ultp-pro-block`}>Pro</span>
                    }
                </div>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('An advanced & highly customizable Post Slider ','ultimate-post')}
        //     <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [ 
            __('Post Slider','ultimate-post'),
            __('Post Carousel','ultimate-post'),
            __('Slide','ultimate-post'),
            __('Slider','ultimate-post'),
            __('Feature','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/post-slider2.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)