const { __ } = wp.i18n;
import {
	arrowSpacing,
	dotSpacing,
	getArrowToolbarSettings,
	getDotsToolbarSettings,
	styleIcon,
} from '../../helper/CommonPanel';
import {
	ArrowContent,
	DotStyle,
	WrapStyle,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	HeadingContent,
	ImageStyle,
	ReadMoreStyle,
	TitleStyle,
	CategoryStyle,
	ExcerptStyle,
	MetaStyle,
	blockSupportLink,
	getTitleToolbarSettings,
	getExcerptToolbarSettings,
	getMetaToolbarSettings,
	getReadMoreToolbarSettings,
	getCategoryToolbarSettings,
	TypographyTB,
	ToolbarSettingsAndStyles,
	imageSettingsKeys,
	imageStyleKeys,
	ImageStyleArg,
	readMoreSettings,
	readMoreStyle,
	ReadMoreStyleArg,
	categorySettings,
	categoryStyle,
	CategoryStyleArg,
	metaSettings,
	metaStyle,
	MetaStyleArg,
	metaText,
	headingSettings,
	headingStyle,
	HeadingContentArg,
	spaceingIcon as spacingIcon,
	colorIcon,
	settingsIcon,
	metaTextIcon,
	typoIcon,
	GeneralContentWithQuery,
	videoColors,
} from '../../helper/CommonPanel';
import DeperciatedSettings from '../../helper/Components/DepreciatedSettings';
import ExtraToolbarSettings from '../../helper/Components/ExtraToolbarSettings';
import UltpToolbarGroup from '../../helper/Components/UltpToolbarGroup';
import { isDCActive } from '../../helper/dynamic_content';
import DCToolbar from '../../helper/dynamic_content/DCToolbar';
import { Sections, Section } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import { variation } from './variations';

export const MAX_CUSTOM_META_GROUPS = 8;

export default function Settings({ store }) {
	const { section } = store;
	const {
		V4_1_0_CompCheck: { runComp },
	} = store.attributes;

	return (
		<>
			<TemplateModal
				prev="https://www.wpxpo.com/postx/blocks/#demoid7487"
				store={store}
			/>
			<Sections>
				<Section slug="setting" title={__('Setting', 'ultimate-post')}>
					<GeneralContentWithQuery
						initialOpen={section['general']}
						store={store}
						include={[
							{
								position: 0,
								data: {
									type: isDCActive() ? 'toggle' : '',
									label: __(
										'Enable Dynamic Content',
										'ultimate-post'
									),
									key: 'dcEnabled',
									help: __(
										'Insert dynamic data & custom fields that update automatically.',
										'ultimate-post'
									),
								},
							},
							{
								position: 1,
								data: {
									type: 'layout',
									block: 'post-slider-2',
									key: 'layout',
									exclude: ['blockPubDate'],
									label: __('Slider Layout', 'ultimate-post'),
									options: [
										{
											img: 'assets/img/layouts/slider2/layout1.png',
											label: __(
												'Slide 1',
												'ultimate-post'
											),
											value: 'slide1',
										},
										{
											img: 'assets/img/layouts/slider2/layout2.png',
											label: __(
												'Slide 2',
												'ultimate-post'
											),
											value: 'slide2',
										},
										{
											img: 'assets/img/layouts/slider2/layout3.png',
											label: __(
												'Slide 3',
												'ultimate-post'
											),
											value: 'slide3',
										},
										{
											img: 'assets/img/layouts/slider2/layout4.png',
											label: __(
												'Slide 4',
												'ultimate-post'
											),
											value: 'slide4',
										},
										{
											img: 'assets/img/layouts/slider2/layout5.png',
											label: __(
												'Slide 5',
												'ultimate-post'
											),
											value: 'slide5',
										},
										{
											img: 'assets/img/layouts/slider2/layout6.png',
											label: __(
												'Slide 6',
												'ultimate-post'
											),
											value: 'slide6',
										},
										{
											img: 'assets/img/layouts/slider2/layout7.png',
											label: __(
												'Slide 7',
												'ultimate-post'
											),
											value: 'slide7',
										},
										{
											img: 'assets/img/layouts/slider2/layout8.png',
											label: __(
												'Slide 8',
												'ultimate-post'
											),
											value: 'slide8',
										},
									],
									variation,
								},
							},
							{
								position: 2,
								data: {
									type: 'range',
									key: 'slidesToShow',
									min: 1,
									max: 8,
									step: 1,
									responsive: true,
									label: __(
										'Number of Slide',
										'ultimate-post'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'range',
									key: 'height',
									label: __('Height', 'ultimate-post'),
									min: 0,
									max: 1000,
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								position: 4,
								data: {
									type: 'range',
									key: 'slidesCenterPadding',
									min: 1,
									max: 400,
									step: 1,
									responsive: true,
									label: __(
										'Padding ( Center Mode )',
										'ultimate-post'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'range',
									key: 'slidesTopPadding',
									min: 0,
									max: 150,
									step: 1,
									label: __(
										'Padding ( Top & Bottom )',
										'ultimate-post'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'range',
									key: 'allItemScale',
									min: 0.0,
									max: 2.5,
									step: 0.01,
									responsive: true,
									unit: false,
									label: __(
										'All Item Scale ( Center Mode )',
										'ultimate-post'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'range',
									key: 'centerItemScale',
									min: 0.0,
									max: 2.5,
									step: 0.01,
									responsive: true,
									unit: false,
									label: __(
										'Center Item Scale ( Center Mode )',
										'ultimate-post'
									),
								},
							},
							{
								position: 8,
								data: {
									type: 'range',
									key: 'slideSpeed',
									min: 0,
									max: 10000,
									step: 100,
									label: __('Slide Speed', 'ultimate-post'),
								},
							},
							{
								position: 9,
								data: {
									type: 'range',
									key: 'sliderGap',
									min: -5,
									max: 100,
									label: __('Slider Gap', 'ultimate-post'),
								},
							},
							{
								position: 10,
								data: {
									type: 'toggle',
									key: 'fade',
									label: __(
										'Animation Fade',
										'ultimate-post'
									),
								},
							},
							{
								position: 11,
								data: {
									type: 'toggle',
									key: 'autoPlay',
									label: __('Auto Play', 'ultimate-post'),
								},
							},
							{
								position: 12,
								data: {
									type: 'toggle',
									key: 'dots',
									label: __('Dots', 'ultimate-post'),
								},
							},
							{
								position: 13,
								data: {
									type: 'toggle',
									key: 'arrows',
									label: __('Arrows', 'ultimate-post'),
								},
							},
							{
								position: 14,
								data: {
									type: 'toggle',
									key: 'preLoader',
									label: __('Pre Loader', 'ultimate-post'),
								},
							},
							{
								position: 15,
								data: {
									type: 'separator',
								},
							},
							{
								data: {
									type: 'text',
									key: 'notFoundMessage',
									label: __(
										'No result found Text',
										'ultimate-post'
									),
								},
							},
						]}
						exclude={['columns', 'columnGridGap']}
					/>

					<TitleStyle
						store={store}
						depend="titleShow"
						initialOpen={section['title']}
						hrIdx={[4, 9]}
					/>

					<ImageStyle
						isTab={true}
						store={store}
						initialOpen={section['image']}
						include={[
							{
								position: 3,
								data: {
									type: 'range',
									key: 'imgBgBlur',
									min: 0,
									max: 20,
									step: 0.5,
									responsive: false,
									label: __(
										'Background Blur',
										'ultimate-post'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'range',
									key: 'imgBgbrightness',
									min: 0,
									max: 50,
									step: 0.5,
									responsive: false,
									label: __(
										'Background brightness',
										'ultimate-post'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'fallbackEnable',
									label: __(
										'Fallback Image Enable',
										'ultimate-post'
									),
								},
							},
							{
								data: {
									type: 'media',
									key: 'fallbackImg',
									label: __(
										'Fallback Image',
										'ultimate-post'
									),
								},
							},
						]}
						exclude={[
							'imageScale',
							'imgMargin',
							'imgSeparator',
							'imgCropSmall',
							'imgAnimation',
						]}
						hrIdx={[
							{
								tab: 'settings',
								hr: [1, 9],
							},
							// {
							//     tab: "style",
							//     hr: [4],
							// },
						]}
					/>

					<MetaStyle
						isTab={true}
						store={store}
						depend={'metaShow'}
						initialOpen={section['meta']}
						exclude={['metaListSmall']}
						hrIdx={[
							{
								tab: 'settings',
								hr: [4],
							},
							{
								tab: 'style',
								hr: [7],
							},
						]}
					/>

					<CategoryStyle
						isTab={true}
						initialOpen={section['taxonomy-/-category']}
						depend="catShow"
						store={store}
						exclude={['catPosition']}
						hrIdx={[
							{
								tab: 'style',
								hr: [8],
							},
						]}
					/>

					<ReadMoreStyle
						isTab={true}
						store={store}
						initialOpen={section['read-more']}
						depend="readMore"
						hrIdx={[
							{
								tab: 'style',
								hr: [3],
							},
						]}
					/>

					<ExcerptStyle
						depend="excerptShow"
						initialOpen={section['excerpt']}
						store={store}
						hrIdx={[3, 6]}
					/>

					<ArrowContent
						depend="arrows"
						store={store}
						initialOpen={section['arrow']}
						include={[
							{
								position: 5,
								data: {
									type: 'separator',
									key: 'separatorStyle',
									label: __(
										'Arrow Position',
										'ultimate-post'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'toggle',
									key: 'arrowSpaceBetween',
									label: __(
										'Arrow Space Between',
										'ultimate-post'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'range',
									key: 'arrowPosBetween',
									min: 1,
									max: 1500,
									step: 1,
									responsive: true,
									label: __(
										'Arrow Position',
										'ultimate-post'
									),
								},
							},
							{
								position: 8,
								data: {
									type: 'tag',
									key: 'arrowPos',
									label: 'Arrow Possition',
									disabled: true,
									options: [
										{
											value: 'left',
											label: __('Left', 'ultimate-post'),
										},
										{
											value: 'right',
											label: __('Right', 'ultimate-post'),
										},
									],
								},
							},
							{
								position: 9,
								data: {
									type: 'range',
									key: 'prevArrowPos',
									min: 1,
									max: 1500,
									step: 1,
									responsive: true,
									label: __(
										'Previous Arrow Position',
										'ultimate-post'
									),
								},
							},
							{
								position: 10,
								data: {
									type: 'range',
									key: 'nextArrowPos',
									min: 1,
									max: 1500,
									step: 1,
									responsive: true,
									label: __(
										'Next Arrow Position',
										'ultimate-post'
									),
								},
							},
						]}
					/>

					<DotStyle
						depend="dots"
						store={store}
						initialOpen={section['dot']}
					/>

					<WrapStyle
						store={store}
						exclude={['contentWrapInnerPadding']}
						include={[
							{
								position: 0,
								data: {
									type: 'tag',
									key: 'contentVerticalPosition',
									label: 'Vertical Position',
									disabled: true,
									options: [
										{
											value: 'topPosition',
											label: __('Top', 'ultimate-post'),
										},
										{
											value: 'middlePosition',
											label: __(
												'Middle',
												'ultimate-post'
											),
										},
										{
											value: 'bottomPosition',
											label: __(
												'Bottom',
												'ultimate-post'
											),
										},
									],
								},
							},
							{
								position: 1,
								data: {
									type: 'tag',
									key: 'contentHorizontalPosition',
									label: __(
										'Horizontal Position',
										'ultimate-post'
									),
									disabled: true,
									options: [
										{
											value: 'leftPosition',
											label: __('Left', 'ultimate-post'),
										},
										{
											value: 'centerPosition',
											label: __(
												'Center',
												'ultimate-post'
											),
										},
										{
											value: 'rightPosition',
											label: __('Right', 'ultimate-post'),
										},
									],
								},
							},
							{
								position: 4,
								data: {
									type: 'range',
									key: 'slideBgBlur',
									min: 0,
									max: 20,
									step: 1,
									responsive: false,
									label: __(
										'Background Blur',
										'ultimate-post'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'dimension',
									key: 'slideWrapMargin',
									label: __('Margin', 'ultimate-post'),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						]}
					/>

					{!runComp && (
						<DeperciatedSettings open={section['heading']}>
							<HeadingContent
								isTab={true}
								store={store}
								initialOpen={section['heading']}
								depend="headingShow"
							/>
						</DeperciatedSettings>
					)}
				</Section>
				<Section
					slug="advanced"
					title={__('Advanced', 'ultimate-post')}
				>
					<GeneralAdvanced
						initialOpen={true}
						store={store}
						include={[
							{
								position: 2,
								data: {
									type: 'color',
									key: 'loadingColor',
									label: __('Loading Color', 'ultimate-post'),
									pro: true,
								},
							},
						]}
					/>
					<ResponsiveAdvanced store={store} />
					<CustomCssAdvanced store={store} />
				</Section>
			</Sections>
			{blockSupportLink()}
		</>
	);
}

export function AddSettingsToToolbar({ selected, store }) {
	const {
		V4_1_0_CompCheck: { runComp },
	} = store.attributes;

	const {
		metaShow,
		titleShow,
		titlePosition,
		excerptShow,
		readMore,
		metaPosition,
		catShow,
	} = store.attributes;

	const layoutContext = [
		metaShow && metaPosition == 'bottom',
		readMore,
		excerptShow,
		titleShow && titlePosition == false,
		metaShow && metaPosition == 'top',
		titleShow && titlePosition == true,
		catShow,
	];

	if (isDCActive() && selected.startsWith('dc_')) {
		return (
			<DCToolbar
				store={store}
				selected={selected}
				layoutContext={layoutContext}
			/>
		);
	}

	switch (selected) {
		case 'heading':
			if (runComp) return null;
			return (
				<UltpToolbarGroup text={'Heading'}>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Heading Settings', 'ultimate-post')}
						styleTitle={__('Heading Style', 'ultimate-post')}
						settingsKeys={headingSettings}
						styleKeys={headingStyle}
						oArgs={HeadingContentArg}
					/>
				</UltpToolbarGroup>
			);
		case 'arrow':
			return (
				<UltpToolbarGroup text={'Arrow'}>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getArrowToolbarSettings({
							include: arrowSpacing,
							exclude: '__all',
							title: __('Arrow Dimension', 'ultimate-post'),
						})}
						store={store}
						label={__('Arrow Dimension', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={styleIcon}
						include={getArrowToolbarSettings({
							title: __('Arrow Style', 'ultimate-post'),
							exclude: ['separatorStyle', ...arrowSpacing],
							include: [
								{
									data: {
										type: 'tag',
										key: 'arrowPos',
										label: 'Arrow Possition',
										disabled: true,
										options: [
											{
												value: 'left',
												label: __(
													'Left',
													'ultimate-post'
												),
											},
											{
												value: 'right',
												label: __(
													'Right',
													'ultimate-post'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'range',
										key: 'prevArrowPos',
										min: 1,
										max: 1500,
										step: 1,
										responsive: true,
										label: __(
											'Previous Arrow Position',
											'ultimate-post'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'nextArrowPos',
										min: 1,
										max: 1500,
										step: 1,
										responsive: true,
										label: __(
											'Next Arrow Position',
											'ultimate-post'
										),
									},
								},
							],
						})}
						store={store}
						label={__('Arrow Style', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'dot':
			return (
				<UltpToolbarGroup text={'Dots'}>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getDotsToolbarSettings({
							include: dotSpacing,
							exclude: '__all',
							title: __('Dots Dimension', 'ultimate-post'),
						})}
						store={store}
						label={__('Dots Dimension', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={styleIcon}
						include={getDotsToolbarSettings({
							title: __('Dots Settings', 'ultimate-post'),
							exclude: ['separatorStyle', ...dotSpacing],
						})}
						store={store}
						label={__('Dots Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'title':
			return (
				<UltpToolbarGroup text={'Title'}>
					<TypographyTB
						store={store}
						attrKey="titleTypo"
						label={__('Title Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={colorIcon}
						include={getTitleToolbarSettings({
							include: [
								'titleColor',
								'titleHoverColor',
								'titleBackground',
							],
							exclude: '__all',
							title: __('Title Color', 'ultimate-post'),
						})}
						store={store}
						label={__('Title Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={settingsIcon}
						include={getTitleToolbarSettings({
							exclude: [
								'titleBackground',
								'titleTypo',
								'titleColor',
								'titleHoverColor',
							],
							title: __('Title Settings', 'ultimate-post'),
						})}
						store={store}
						label={__('Title Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'excerpt':
			return (
				<UltpToolbarGroup text={'Excerpt'}>
					<TypographyTB
						store={store}
						attrKey="excerptTypo"
						label={__('Excerpt Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={colorIcon}
						include={getExcerptToolbarSettings({
							include: ['excerptColor'],
							exclude: '__all',
							title: __('Excerpt Color', 'ultimate-post'),
						})}
						store={store}
						label={__('Excerpt Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={settingsIcon}
						include={getExcerptToolbarSettings({
							exclude: ['excerptTypo', 'excerptColor'],
							title: __('Excerpt Settings', 'ultimate-post'),
						})}
						store={store}
						label={__('Excerpt Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'meta':
			return (
				<UltpToolbarGroup text={'Meta'}>
					<TypographyTB
						store={store}
						attrKey="metaTypo"
						label={__('Meta Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getMetaToolbarSettings({
							include: [
								'metaSpacing',
								'metaMargin',
								'metaPadding',
							],
							exclude: '__all',
							title: __('Meta Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Meta Spacing', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={metaTextIcon}
						include={getMetaToolbarSettings({
							include: metaText,
							exclude: '__all',
							title: __('Meta Texts', 'ultimate-post'),
						})}
						store={store}
						label={__('Meta Texts', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Meta Settings', 'ultimate-post')}
						styleTitle={__('Meta Style', 'ultimate-post')}
						settingsKeys={metaSettings}
						styleKeys={metaStyle}
						oArgs={MetaStyleArg}
						exSettings={['metaListSmall']}
						exStyle={[
							'metaTypo',
							'metaMargin',
							'metaPadding',
							'metaSpacing',
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'read-more':
			return (
				<UltpToolbarGroup text={'Read More'}>
					<TypographyTB
						store={store}
						attrKey="readMoreTypo"
						label={__('Read More Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getReadMoreToolbarSettings({
							include: [
								'readMoreSacing', // 💩 yes, it's a typo
								'readMorePadding',
							],
							exclude: '__all',
							title: __('Read More Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Read More Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__(
							'Read More Settings',
							'ultimate-post'
						)}
						styleTitle={__('Read More Style', 'ultimate-post')}
						settingsKeys={readMoreSettings}
						styleKeys={readMoreStyle}
						oArgs={ReadMoreStyleArg}
						exStyle={[
							'readMoreTypo',
							'readMoreSacing',
							'readMorePadding',
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'cat':
			return (
				<UltpToolbarGroup text={'Taxonomy/Category'}>
					<TypographyTB
						store={store}
						attrKey="catTypo"
						label={__('Category Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getCategoryToolbarSettings({
							include: ['catSacing', 'catPadding'],
							exclude: '__all',
							title: __('Category Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Category Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Category Settings', 'ultimate-post')}
						styleTitle={__('Category Style', 'ultimate-post')}
						settingsKeys={categorySettings}
						styleKeys={categoryStyle}
						oArgs={CategoryStyleArg}
						exSettings={['catPosition']}
						exStyle={['catTypo', 'catSacing', 'catPadding']}
					/>
				</UltpToolbarGroup>
			);
		case 'image':
			return (
				<UltpToolbarGroup text={'Image'}>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Image Settings', 'ultimate-post')}
						styleTitle={__('Image Style', 'ultimate-post')}
						settingsKeys={imageSettingsKeys}
						styleKeys={imageStyleKeys}
						oArgs={ImageStyleArg}
						exSettings={[
							'imageScale',
							'imgMargin',
							'imgSeparator',
							'imgCropSmall',
							'imgAnimation',
						]}
						exStyle={[
							'imageScale',
							'imgMargin',
							'imgSeparator',
							'imgCropSmall',
							'imgAnimation',
						]}
						incStyle={[
							{
								position: 0,
								data: {
									type: 'range',
									key: 'imgBgBlur',
									min: 0,
									max: 20,
									step: 0.5,
									responsive: false,
									label: __(
										'Background Blur',
										'ultimate-post'
									),
								},
							},
							{
								position: 1,
								data: {
									type: 'range',
									key: 'imgBgbrightness',
									min: 0,
									max: 50,
									step: 0.5,
									responsive: false,
									label: __(
										'Background brightness',
										'ultimate-post'
									),
								},
							},
						]}
						incSettings={[
							{
								data: {
									type: 'toggle',
									key: 'fallbackEnable',
									label: __(
										'Fallback Image Enable',
										'ultimate-post'
									),
								},
							},
							{
								data: {
									type: 'media',
									key: 'fallbackImg',
									label: __(
										'Fallback Image',
										'ultimate-post'
									),
								},
							},
						]}
					/>
				</UltpToolbarGroup>
			);
		default:
			return null;
	}
}
