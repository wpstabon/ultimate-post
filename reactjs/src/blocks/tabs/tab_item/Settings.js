import { CommonSettings, CustomCssAdvanced, ResponsiveAdvanced } from "../../../helper/CommonPanel"
import { Section, Sections } from "../../../helper/Sections"

const { __ } = wp.i18n

export const TabItemSettings = ({ store }) => {
    return (
        <>
            <Sections>
                <Section slug="global" title={__('Global Style','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 5,  data:{ type:'text', key:'tabItemText', label:__('Item Label','ultimate-post') } },
                            { position: 30,  data: { type:'icon',    key:'tabItemIcon',label:__('Item Icon','ultimate-post')}},
                        ]}
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Content','ultimate-post')} 
                        include={[

                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Badge','ultimate-post')} 
                        depend="enableBadge"
                        include={[
                            
                        ]} 
                        initialOpen={false}  store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={'inline'} 
                        include={[
                            { position: 1, data: { type:'text',     key:'advanceId',        label:__('ID','ultimate-post') } },
                            { position: 2, data: { type:'range',    key:'advanceZindex',    label:__('z-index','ultimate-post'), min:-100, max:10000, step:1 } }
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    )
}