const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import { commonAttribute } from '../../../helper/CommonAttribute';
import UltpLinkGenerator from '../../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/menu/', 'block_docs');

registerBlockType(
    'ultimate-post/tab-item', {
        title: __('Tab Item','ultimate-post'),
        parent:  ["ultimate-post/tabs"],
        icon: <img src={ultp_data.url + 'assets/img/blocks/advanced-list.svg'} alt="List PostX"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Create & customize bullets and numbered lists.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('tab','ultimate-post'),
            __('item','ultimate-post'),
            __('tab item','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/list.svg',
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            blockId: {
                type: 'string',
                default: '',
            },
            previewImg: {
                type: 'string',
                default: '',
            },

            // dependencies start
            parentTabsInline: {     // Menu block inline or not
                type: 'boolean',
                default: true,
            },
            defaultOpen: {     // Menu block inline or not
                type: 'boolean',
                default: false,
            },
            blockPosition: {
                type: 'string',
                default: '0'
            },
            defaultOpenTab: {
                type: 'string',
                default: '0',
            },
            // dependencies end

            tabItemText: {
                type: 'string',
                default: 'New Menu Item'
            },
            iconAfterText: {     // Menu block inline or not
                type: 'boolean',
                default: true,
            },
            tabItemIcon: {
                type: 'string',
                default: ''
            },
            advanceId: {
                type: 'string',
                default: ''
            },
            advanceId: { 
                type: "string", 
                default: "" 
            },
            advanceZindex: {
                // type number
                type: "string",
                default: "",
                style: [
                    { selector: "{{ULTP}} .ultp-block-wrapper{z-index:{{advanceZindex}};}" },
                ],
            },

            /*==========================
                General Advance Settings
            ==========================*/
        }
    }
)