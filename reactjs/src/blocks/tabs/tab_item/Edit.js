const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const {  Fragment, useState, useEffect, useRef } = wp.element
import { CssGenerator } from '../../../helper/CssGenerator';
import { isInlineCSS, updateChildAttr } from '../../../helper/CommonPanel';
import { TabItemSettings } from './Settings';
const { 
    getBlockAttributes,
    getBlockIndex,
    getBlockParents,
    getBlockCount,
    hasSelectedInnerBlock
} = wp.data.select('core/block-editor');
const { updateBlockAttributes } = wp.data.dispatch( 'core/block-editor' )

export default function Edit(props) {
    const prevPropsRef = useRef(null);
    const [section, setSection] = useState('Content');
    const { 
        setAttributes,
        name,
        className,
        attributes,
        clientId,
        attributes: { 
            previewImg,
            blockId,
            advanceId,
        } 
    } = props;
    const store = {
        setAttributes,
        name,
        attributes,
        setSection: setSection,
        section: section,
        clientId 
    };
    useEffect(() => {
        const _client = clientId.substr(0, 6)
        
        if ( !blockId ) {
            setAttributes({ blockId: _client });
        } else if ( blockId && blockId != _client ) {
            setAttributes({ blockId: _client });
        }
	}, [clientId]);

    useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if ( !prevAttributes ) {
			prevPropsRef.current = attributes;
		}
        const currentPos = getBlockIndex(clientId).toLocaleString();
        
        if( prevAttributes?.blockPosition != currentPos ) {
            setAttributes({ blockPosition: currentPos  });
        }
        prevPropsRef.current = attributes;

	}, [attributes]);
    
    let __preview_css;
    if ( blockId ) { 
        __preview_css = CssGenerator(attributes, 'ultimate-post/tab-item', blockId, isInlineCSS());
    }

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }

    const parentIDs = getBlockParents( clientId );
    const { editorSelectedTab, defaultOpenTab } = getBlockAttributes(parentIDs[parentIDs.length - 1]) || {};
    const currentBlockIndex = getBlockIndex(clientId);
    
    if ( ( props.isSelected || hasSelectedInnerBlock(clientId) ) &&  editorSelectedTab != currentBlockIndex ) {
        updateBlockAttributes(parentIDs[parentIDs.length - 1], { editorSelectedTab: currentBlockIndex.toLocaleString() })
        updateChildAttr(parentIDs[parentIDs.length - 1]);
    }
    if( defaultOpenTab != attributes.defaultOpenTab ) {
        setAttributes({ defaultOpenTab: defaultOpenTab });
    }
    const hasInnerBlocks = getBlockCount( clientId );
    return (
        <Fragment>
            <InspectorControls>
                <TabItemSettings store={store} />
            </InspectorControls>
            <div 
                {...(advanceId && {id:advanceId})}
                className={`ultp-block-${blockId} ${className} ${currentBlockIndex == editorSelectedTab ? 'pt-active' : ''} ultp-tab-${blockId}`}
            >
                { __preview_css &&
                    <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                }
                <div className={`ultp-tab-item-wrapper`}>
                    <div className={`ultp-tab-item-content `}>
                        <InnerBlocks
                            renderAppender={hasInnerBlocks ? undefined : () => <InnerBlocks.ButtonBlockAppender />}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}