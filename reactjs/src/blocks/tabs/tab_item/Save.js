const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { blockId, advanceId, defaultOpenTab, blockPosition } = props.attributes;
    
    return (
        <div 
            { ...( advanceId && { id: advanceId } ) }
            className={`ultp-block-${blockId} ultp-tab-${blockId} ${defaultOpenTab == blockPosition ? 'pt-active' : ''}`}
        >
            <div className={`ultp-tab-item-wrapper`}>
                <div className={`ultp-tab-item-content`}>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    )
}