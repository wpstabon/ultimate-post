import { CommonSettings, CustomCssAdvanced, ResponsiveAdvanced } from "../../helper/CommonPanel"
import { Section, Sections } from '../../helper/Sections';
import Select from "../../helper/fields/Select";

const { __ } = wp.i18n

export const TabsSettings = ({ store, defaultOpenTab, tabOptions, setAttributes }) => {
    return (
        <>
            <Sections>
                <Section slug="global" title={__('Style','ultimate-post')}>
                    <div className="ultp-section-accordion">
                        <div className="ultp-section-show">
                            <Select 
                                label={'Default Open Tab'}
                                options={ tabOptions }
                                value={defaultOpenTab}
                                onChange={ v => {
                                    setAttributes({defaultOpenTab: v.toLocaleString()});
                                } }
                            />
                        </div>
                    </div>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 5,  data: { type: 'toggle',      key: 'tabInline', label:__('Inline view','ultimate-post')} },
                            { position: 20, data: { type: 'range',       key: 'tabContentGap', min: 0, max: 200, step: 1, responsive: false, unit: false, label:__('Space between Nav and Content','ultimate-post') } },
                            { position: 20, data: { type: 'range',       key: 'tabNavWidth', min: 0, max: 800, step: 1, responsive: true, unit: ['px', '%'], label:__('Tab Nav Width','ultimate-post') } },
                            
                            { position: 5,  data: { type: 'toggle',      key: 'tabThemeWidth', label:__('Inherit theme width','ultimate-post')} },
                            { position: 20, data: { type: 'range',       key: 'tabWidth', min: 0, max: 1600, step: 1, responsive: true, unit: ['px', '%'], label:__('Width','ultimate-post') } },
                            { position: 10, data: { type: 'alignment',   key: 'tabAlign', block: 'nabs',  disableJustify: true, icons: ['left', 'center', 'right'], options:['left', 'center', 'right'], label: __('Alignment', 'ultimate-post') } },
                            { position: 20, data: { type: 'range',       key: 'tabBottomSpace', min: 0, max: 800, step: 1, responsive: true, unit: ['px'], label:__('Bottom Space','ultimate-post') } },
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Tab Nav Wrapper','ultimate-post')} 
                        include={[
                            { position: 20, data: { type: 'range',        key: 'tavNavItemGap', min: 0, max: 200, step: 1, responsive: true, unit: ['px', '%'], label:__('Nav item gap','ultimate-post') } },
                            { position: 10, data: { type: 'alignment',    key: 'tavNavAlign', block: 'nabs',  disableJustify: true, icons:  ['juststart', 'justcenter', 'justend', 'justbetween'], options: ['flex-start', 'center', 'flex-end', 'space-between'], label: __('Nav Alignment', 'ultimate-post') } },
                            { position: 30, data: { type: 'color2',       key: 'tabNavBg', image: true, label: __('Background', 'ultimate-post') } },
                            { position: 40, data: { type: 'border',       key: 'tabNavBorder', label: __('Border', 'ultimate-post') } },
                            { position: 50, data: { type: 'boxshadow',    key: 'tabNavShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post')} },
                            { position: 60, data: { type: 'dimension',    key: 'tabNavRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                            { position: 70, data: { type: 'dimension',    key: 'tabNavPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Tab Nav Item','ultimate-post')} 
                        include={[
                            { position: 10, data: { type: 'alignment',   key: 'navItemAlign', block: 'nabs',  disableJustify: true, icons: ['left', 'center', 'right'], options:['start', 'center', 'end'], label: __('Label Alignment', 'ultimate-post') } },
                            { position: 10, data: { type: 'dimension',    key: 'tabItemRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') }},
                            { position: 10, data: { type: 'dimension',    key: 'tabItemPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                            {
                                position: 10, data: { type: 'tab', content: 
                                [
                                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                            { type: 'color',        key: 'tabItemColor', label: __('Color', 'ultimate-post') },
                                            { type: 'color2',        key: 'tabItemBg', label:__('Background','ultimate-post') },
                                            { type: 'border',       key: 'tabItemBorder', label:__('Border','ultimate-post') },
                                        ]
                                    },
                                    {   name: 'hover', title: __('Active', 'ultimate-post'), options: [
                                            { type: 'color',        key: 'tabItemColorHvr', label: __('Color', 'ultimate-post') },
                                            { type: 'color2',        key: 'tabItemBgHvr', label:__('Background','ultimate-post') },
                                            { type: 'border',       key: 'tabItemBorderHvr', label:__('Border','ultimate-post') },
                                        ]
                                    }
                                ]}
                            },
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Tab Content','ultimate-post')} 
                        include={[
                            { position: 20, data: { type: 'range',        key: 'tabContentMinHeight', min: 0, max: 1200, step: 1, responsive: true, unit: ['px'], label:__('Content Min Height','ultimate-post') } },
                            { position: 30, data: { type: 'color2',       key: 'tabContentBg', image: true, label: __('Background', 'ultimate-post') } },
                            { position: 40, data: { type: 'border',       key: 'tabContentBorder', label: __('Border', 'ultimate-post') } },
                            { position: 50, data: { type: 'boxshadow',    key: 'tabContentShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post')} },
                            { position: 60, data: { type: 'dimension',    key: 'tabContentRadius', step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                            { position: 70, data: { type: 'dimension',    key: 'tabContentPadding', step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]}
                        initialOpen={false}  store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 5,  data: { type:'text',key:'advanceId', label:__('ID','ultimate-post') } },
                            { position: 20, data: { type:'range',key:'advanceZindex',min:-100, max:10000, step:1, label:__('z-index','ultimate-post') } },
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    )
}