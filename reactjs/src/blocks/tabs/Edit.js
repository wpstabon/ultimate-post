const { __ } = wp.i18n
const { InspectorControls, InnerBlocks, RichText } = wp.blockEditor
const { useState, useEffect, useRef, Fragment } = wp.element
import { isInlineCSS, updateChildAttr } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator'
import IconPack from '../../helper/fields/tools/IconPack';
import icons from '../../helper/icons';
import { TabsSettings } from './Settings';
const { getBlockAttributes, getBlockRootClientId, getBlockOrder, hasSelectedInnerBlock } = wp.data.select('core/block-editor');
const { 
    removeBlock,
    updateBlockAttributes,
    insertBlocks,
    moveBlockToPosition,
    selectBlock,
    clearSelectedBlock
} = wp.data.dispatch( 'core/block-editor' );

export default function Edit(props) {
    const prevPropsRef = useRef(null);
    const loaderRef = useRef(true);
	const [section, setSection] = useState('Content');
    const { 
        setAttributes,
        name,
        className,
        attributes,
        clientId,
        attributes: {
            previewImg,
            blockId,
            advanceId,
            dummyAttr,
            defaultOpenTab,
            editorSelectedTab,
            tabInline
        } 
    } = props;
    const store = { 
        setAttributes,
        name,
        attributes,
        setSection: setSection,
        section: section,
        clientId
    }

    useEffect(() => {
        const _client = clientId.substr(0, 6)
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );
        loaderRef.current = false;
        
        if ( !blockId ) {
            setAttributes({ blockId: _client, editorSelectedTab: '0' });
        } else if ( blockId && blockId != _client ) {
            if ( !reference?.hasOwnProperty('ref') ) {
                if ( !reference?.hasOwnProperty('theme') ) {
                    setAttributes({ blockId: _client, editorSelectedTab: '0' });
                }
            }
        }
	}, [clientId]);

    function checkO(obj1, obj2) {
        const keys1 = Object.keys(obj1);
        const keys2 = Object.keys(obj2);
    
        if (keys1.length !== keys2.length) {
            return false;
        }
    
        for (const key of keys1) {
            if (!keys2.includes(key) || obj1[key] !== obj2[key]) {
                return false;
            }
        }
    
        return true;
    }

    function isEqualObject( obj1, obj2 ) {
        const keys1 = Object.keys(obj1);
        const keys2 = Object.keys(obj2);

        if ( keys1.length !== keys2.length ) {
            return false;
        }

        for ( const key of keys1 ) {
            if ( !keys2.includes(key) || obj1[key] !== obj2[key] ) {
                return false;
            }
        }
        return true;
    }

    useEffect(() => {
        const prevAttributes = prevPropsRef.current;
        const childIds  = getBlockOrder( clientId );
        const childBlockAttributes = childIds?.map((childCID) => {
            const { 
                tabItemText,
                iconAfterText,
                enableIcon,
                tabItemIcon,
                blockId,
                blockPosition
            } = getBlockAttributes(childCID) || {};
            return {
                tabItemText,
                // enableIcon,
                iconAfterText,
                tabItemIcon,
                tabItemCid: childCID,
                blockId,
                blockPosition: blockPosition
            }
        });

        if ( prevAttributes?.defaultOpenTab != defaultOpenTab ) {
            updateChildAttr(clientId);
        }
        let toUpdate = {};
        if ( !prevAttributes ) {
			prevPropsRef.current = attributes;
            toUpdate = { ...toUpdate, tabItems: JSON.stringify(childBlockAttributes) }
		} else {
            let updateTabItems = false;
            const prevTabItems = JSON.parse(prevAttributes.tabItems || '[]') || [];
            if ( prevTabItems.length !== childBlockAttributes.length ) {
                updateTabItems = true;
            } else {
                for ( let i = 0; i < childBlockAttributes.length; i++ ) {
                    if ( !isEqualObject( prevTabItems[i], childBlockAttributes[i] ) ) {
                        updateTabItems = true;
                        break;
                    }
                }
            }

            if ( updateTabItems ) {
                toUpdate = { ...toUpdate, tabItems: JSON.stringify(childBlockAttributes) }
            }
            prevPropsRef.current = attributes;
        }
        
        setTimeout(()=> {
            if( dummyAttr == undefined ) {
                setAttributes({ dummyAttr: true });
            }
        }, 0);

        if ( defaultOpenTab == '' || childBlockAttributes.length <= defaultOpenTab ) {
            toUpdate = { ...toUpdate, defaultOpenTab: childBlockAttributes[0]?.blockPosition || 0 };
            updateChildAttr(clientId);
        }

        if ( Object.keys(toUpdate).length > 0 ) {
            setAttributes(toUpdate);
        }

    }, [attributes]);

    let __preview_css;
    if ( blockId ) { 
        __preview_css = CssGenerator(attributes, 'ultimate-post/tabs', blockId, isInlineCSS());
    }

    const tabsTemplate = [
        [ "ultimate-post/tab-item", { parentTabsInline: true, tabItemText: 'Tab 1' } ],
        [ "ultimate-post/tab-item", { parentTabsInline: true, tabItemText: 'Tab 2' } ],
        [ "ultimate-post/tab-item", { parentTabsInline: true, tabItemText: 'Tab 3' } ],
    ]

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }

    const tabItems = JSON.parse( attributes.tabItems || '[]' );

    const tabOptions = tabItems.map((item)=> {
        return {
            label: item.tabItemText.slice(0, 16),
            value: item.blockPosition
        }
    });

    return (
        <Fragment>
            <InspectorControls>
                <TabsSettings setAttributes={setAttributes} defaultOpenTab={defaultOpenTab} tabOptions={tabOptions}  store={store}/>
            </InspectorControls>
            <div 
                {...(advanceId && { id: advanceId } ) }
                className={`ultp-block-${blockId} ${className}`}
            >
                { __preview_css &&
                    <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                }
                <div className={`ultp-tabs-wrapper`}>
                    <div className={`ultp-tabs-content`}>
                        <div className="ultp-tabs-items-wrap">
                            <div className="ultp-tab-items">
                                {
                                    tabItems?.map((tabItem, k) => {
                                        const {tabItemCid, tabItemText, enableIcon, iconAfterText, tabItemIcon } = tabItem;
                                        const childLength = tabItems.length;
                                        return <div key={k} className="ultp-tab-item-label-container" onClick={()=> {
                                            setAttributes({ editorSelectedTab: k.toLocaleString() })
                                            updateChildAttr(clientId);
                                        }}>
                                            <div className={`ultp-tab-item-label-actions ${ k == editorSelectedTab ? 'pt-active' : ''}`}>
                                                {
                                                    childLength > 1 && k > 0 && k <= childLength-1 && <div onClick={(e)=> {
                                                        e.stopPropagation();
                                                        moveBlockToPosition(tabItemCid, clientId, clientId, k-1);
                                                        if ( editorSelectedTab == k ) {
                                                            setAttributes({ editorSelectedTab: (k-1).toLocaleString() });
                                                        } else if ( editorSelectedTab == k-1 ) {
                                                            setAttributes({ editorSelectedTab: (k).toLocaleString() });
                                                        } else {
                                                            setAttributes({ dummyAttr: dummyAttr ? false : true });
                                                        }
                                                        updateChildAttr(clientId);
                                                    }}>
                                                        {IconPack[ tabInline ? 'arrowUp2' : 'leftAngle2' ]}
                                                    </div>
                                                }
                                                {
                                                    childLength > 1 && k >= 0 && k < childLength-1 && <div onClick={(e)=> {
                                                        e.stopPropagation();
                                                        moveBlockToPosition(tabItemCid, clientId, clientId, k+1);
                                                        if ( editorSelectedTab == k ) {
                                                            setAttributes({ editorSelectedTab: (k+1).toLocaleString() });
                                                        } else if ( editorSelectedTab == k+1 ) {
                                                            setAttributes({ editorSelectedTab: (k).toLocaleString() });
                                                        } else {
                                                            setAttributes({ dummyAttr: dummyAttr ? false : true });
                                                        }
                                                        updateChildAttr(clientId);
                                                    }}>
                                                        {IconPack[ tabInline ? 'collapse_bottom_line' : 'rightAngle2' ]}
                                                    </div>
                                                }
                                                <div onClick={(e)=> {
                                                    e.stopPropagation();
                                                    if ( editorSelectedTab == k && k > 0 ) {
                                                        console.log(editorSelectedTab, '<<test 1--', k);
                                                        setAttributes({ editorSelectedTab: (k - 1).toLocaleString() });
                                                    } else if ( editorSelectedTab > k ) {
                                                        console.log(editorSelectedTab, '<<test 2--', k);
                                                        setAttributes({ editorSelectedTab: (parseInt(editorSelectedTab) - 1).toLocaleString() });
                                                    } else {
                                                        setAttributes({ dummyAttr: dummyAttr ? false : true });
                                                    }
                                                    removeBlock(tabItemCid, false);
                                                    updateChildAttr(clientId);
                                                }}>
                                                    {icons.delete}
                                                </div>
                                            </div>
                                            {
                                                enableIcon && !iconAfterText && <div className='ultp-menu-item-icon'>{IconPack[tabItemIcon]}</div>
                                            }
                                            <RichText
                                                data-tabid= {`ultp-block-${tabItem.blockId}`}
                                                key="editable"
                                                tagName={'div'}
                                                className={`ultp-tab-item-label ${ k == editorSelectedTab ? 'pt-active' : ''}`}
                                                keepPlaceholderOnFocus
                                                placeholder={__('Tab Text...','ultimate-post')}
                                                onChange={(value) => { 
                                                    updateBlockAttributes(tabItemCid, { tabItemText: value }); 
                                                    setAttributes({ dummyAttr: dummyAttr ? false : true });
                                                }}
                                                value={tabItemText}
                                            />
                                            {
                                                enableIcon && iconAfterText && <div className='ultp-menu-item-icon'>{IconPack[tabItemIcon]}</div>
                                            }
                                        </div>
                                    })
                                }
                                <div className="ultp-tabs-add" onClick={() => {
                                    const currentLength =  tabItems.length || 0;
                                    const newBlock  = wp.blocks.createBlock('ultimate-post/tab-item', { parentTabsInline: true, tabItemText: `Tab ${currentLength + 1}` })
                                    insertBlocks(newBlock, currentLength + 1, clientId, false )
                                    setAttributes({ dummyAttr: dummyAttr ? false : true });
                                }}> Add new</div>
                            </div>
                        </div>
                        <div className="ultp-tabs-content-warp">
                            <InnerBlocks
                                template={tabsTemplate}
                                renderAppender={ false }
                                allowedBlocks={['ultimate-post/tab-item']}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}