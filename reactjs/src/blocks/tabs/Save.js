import IconPack from "../../helper/fields/tools/IconPack";
const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { 
        blockId,
        advanceId,
        defaultOpenTab,
        tabItems
    } = props.attributes;
    const parsedTabItems = JSON.parse( tabItems || '[]' );
    const showType = 'click';
    
    return (
        <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId}`}>
            <div className={`ultp-tabs-wrapper`}>
                <div className={`ultp-tabs-content`} data-show={showType}>
                    <div className="ultp-tabs-items-wrap">
                        <div className="ultp-tab-items">
                            {
                                parsedTabItems?.map((tabItem, k) => (
                                    <div key={k} className={`ultp-tab-item-label-container`}>
                                        {
                                            tabItem.enableIcon && !tabItem.iconAfterText && <div className='ultp-menu-item-icon'>{IconPack[tabItem.tabItemIcon]}</div>
                                        }
                                        <div data-tabid= {`ultp-block-${tabItem.blockId}`} className={`ultp-tab-item-label ${ k == defaultOpenTab ? 'pt-active' : ''}`} >{tabItem.tabItemText}</div>
                                        {
                                            tabItem.enableIcon && tabItem.iconAfterText && <div className='ultp-menu-item-icon'>{IconPack[tabItem.tabItemIcon]}</div>
                                        }
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className="ultp-tabs-content-warp">
                        <InnerBlocks.Content />
                    </div>
                </div>
            </div>
        </div>
    )
}