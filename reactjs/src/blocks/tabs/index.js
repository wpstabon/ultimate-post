const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/menu/', 'block_docs');

registerBlockType(
    'ultimate-post/tabs', {
        title: __('Tabs - PostX','ultimate-post'),
        icon: <img src={ultp_data.url + 'assets/img/blocks/advanced-list.svg'} alt="List PostX"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Create & customize bullets and numbered lists.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('tabs','ultimate-post'),
            __('tab','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/list.svg',
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            /*==========================
                    Needed
            ==========================*/
            blockId: {
                type: 'string',
                default: '',
            },
            previewImg: {
                type: 'string',
                default: '',
            },
            editorSelectedTab: {
                type: 'string',
                default: '0',
            },
            tabItems: {
                type: 'string',
                default: '[]',
            },
            defaultOpenTab: {
                type: 'string',
                default: '0',
            },


            /*==========================
                Inline
            ==========================*/
            tabInline: {
                type: 'boolean',
                default: false,
                style: [
                    {
                        depends: [
                            { key:'tabInline', condition:'==', value: false },
                        ],
                    },
                    {
                        depends: [
                            { key:'tabInline', condition:'==', value: true },
                        ],
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content { flex-direction: row; } 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-items { flex-direction: column; }
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-items .ultp-tab-item-label-container { flex: 1 1 auto; }`
                    }
                ]
            },
            tabContentGap: {
                type: 'string',
                default: '16',
                style:[{
                        selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content { gap: {{tabContentGap}}px;}' 
                    },
                ],
            },
            tabNavWidth: {
                type: 'object',
                default: { lg:'25', ulg:'%'},
                anotherKey: 'tabContentGap',
                style:[
                    {
                        depends: [
                            { key:'tabInline', condition:'==', value: true },
                        ],
                        // selector: 
                        // `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap { flex-basis: calc( {{tabNavWidth}} - calc( {{tabContentGap}}px / 2 ));} 
                        // {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp { flex-basis: calc( 100% - {{tabNavWidth}} - {{tabContentGap}}px / 2 );}`
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap { flex-basis: {{tabNavWidth}};} 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp { flex-basis: calc( 100% - {{tabNavWidth}} - {{tabContentGap}}px );}`
                    },
                ],
            },
            tabThemeWidth: {
                type: 'boolean',
                default: true,
            },
            tabWidth: {
                type: 'object',
                default: { lg:'980', ulg:'px'},
                style:[
                    {
                        depends: [
                            { key:'tabThemeWidth', condition:'==', value: false },
                        ],
                        selector: '{{ULTP}} > .ultp-tabs-wrapper { max-width: {{tabWidth}} }'
                    },
                ],
            },
            tabAlign: {
                type: 'string',
                default: 'center',
                style: [
                    {
                        depends: [
                            { key:'tabAlign', condition:'==', value: 'left' },
                            { key:'tabThemeWidth', condition:'==', value: false },
                        ],
                        selector: '{{ULTP}} > .ultp-tabs-wrapper { margin-right: auto !important; }' 
                    },
                    {
                        depends: [
                            { key:'tabAlign', condition:'==', value: 'center' },
                            { key:'tabThemeWidth', condition:'==', value: false },
                        ],
                        selector: '{{ULTP}} > .ultp-tabs-wrapper { margin-left: auto !important; margin-right: auto !important; }' 
                    },
                    {
                        depends: [
                            { key:'tabAlign', condition:'==', value: 'right' },
                            { key:'tabThemeWidth', condition:'==', value: false },
                        ],
                        selector: '{{ULTP}} > .ultp-tabs-wrapper { margin-left: auto !important; }' 
                    },
                ],
            },
            tabBottomSpace: {
                type: 'object',
                default: { lg:'10', ulg:'px'},
                style:[
                    {
                        selector: '{{ULTP}} > .ultp-tabs-wrapper { margin-bottom: {{tabBottomSpace}}; }'
                    },
                ],
            },

            /*==========================
                Tab Nav Wrapper Settings
            ==========================*/
            tavNavItemGap: {
                type: 'object',
                default: { lg:'16', ulg:'px'},
                style:[{
                        selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-items { gap: {{tavNavItemGap}};}' 
                    },
                ],

            },
            tavNavAlign: {
                type: 'string',
                default: 'center',
                style: [
                    {
                        selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-items { justify-content: {{tavNavAlign}}; }' 
                    },
                ],

            },
            tabNavBg: {
                type: 'object',
                default: {openColor: 1, type: 'color', color: '#10b981', size: 'cover', repeat: 'no-repeat', loop: true},
                style:[{
                    selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap'
                }],

            },
            tabNavBorder: {
                type: 'object',
                default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
                style: [{
                    selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap'
                }]

            },
            tabNavShadow: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [{ selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap' }],

            },
            tabNavRadius: {
                type: 'object',
                default: {lg:{top: '4', bottom: '4', left: '4', right: '4'}, unit:'px'},
                style: [{
                    selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap { border-radius: {{tabNavRadius}};}',
                }]

            },
            tabNavPadding: {
                type: 'object',
                default: {lg:{top: '8',bottom: '8', left: '6', right: '6', unit:'px'}},
                style:[
                    { selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap { padding:{{tabNavPadding}}; }' }
                ],
            },


            /*==========================
                Tab Nav Item
            ==========================*/
            navItemAlign: {
                type: 'string',
                default: 'center',
                style: [
                    {
                        selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label { text-align: {{navItemAlign}}; }'
                    }
                ],
            },
            tabItemColor: {
                type: 'string',
                default: '#ff8787',
                style: [
                    {
                        selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label { color: {{tabItemColor}}; }'
                    }
                ],
            },
            tabItemBg: {
                type: 'object',
                default: {openColor: 1, type: 'color', color: '#317760', size: 'cover', repeat: 'no-repeat', loop: true},
                style:[{
                    selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label'
                }],

            },
            tabItemBorder: {
                type: 'object',
                default: { openBorder: 1, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
                style: [{
                    selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label'
                }]
            },
            tabItemRadius: {
                type: 'object',
                default: {lg:'', unit:'px'},
                style: [{
                    selector: '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label { border-radius: {{tabItemRadius}};}',
                }]
            },
            tabItemPadding: {
                type: 'object',
                default: {lg:{top: '',bottom: '', unit:'px'}},
                style:[
                    { selector:'{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label { padding:{{tabItemPadding}}; }' }
                ],
            },

            tabItemColorHvr: {
                type: 'string',
                default: '#317760',
                style: [
                    {
                        selector:
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label:hover, 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label.pt-active  { color: {{tabItemColorHvr}}; }`
                    }
                ],
            },
            tabItemBgHvr: {
                type: 'object',
                default: {openColor: 1, type: 'color', color: '#ff8787', size: 'cover', repeat: 'no-repeat', loop: true},
                style:[{
                    selector: 
                    `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label:hover, 
                    {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label.pt-active`
                }],

            },
            tabItemBorderHvr: {
                type: 'object',
                default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
                style: [{
                    selector:   '{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-items-wrap .ultp-tab-item-label.pt-active'
                }]
            },

            /*==========================
                Tab Content Settings
            ==========================*/
            tabContentMinHeight: {
                type: 'object',
                default: { lg:'150', ulg:'px' },
                style:[
                    {
                        depends: [
                            { key:'tabInline', condition:'==', value: false },
                        ],
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item, 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item { min-height: {{tabContentMinHeight}};}`
                    },
                ],
            },
            tabContentBg: {
                type: 'object',
                default: { openColor: 1, type: 'color', color: '#10b981', size: 'cover', repeat: 'no-repeat', loop: true },
                style:[
                    {
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item, 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item`
                    }
                ], 

            },
            tabContentBorder: {
                type: 'object',
                default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
                style: [
                    {
                        selector:
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item, 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item`
                    }
                ]
            },
            tabContentShadow: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [
                    { 
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item, 
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item`
                    }
                ],
            },
            tabContentRadius: {
                type: 'object',
                default: {lg:'', unit:'px'},
                style: [
                    {
                        selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item,
                        {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item { border-radius: {{tabContentRadius}};}`
                    }
                ]
            },
            tabContentPadding: {
                type: 'object',
                default: {lg:{top: '10',bottom: '10', unit:'px'}},
                style:[
                    { selector: 
                        `{{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-tab-item > .ultp-tab-item-wrapper, {{ULTP}} > .ultp-tabs-wrapper > .ultp-tabs-content > .ultp-tabs-content-warp > .wp-block-ultimate-post-tab-item > .ultp-tab-item-wrapper { padding:{{tabContentPadding}}; }`
                    }
                ],

            },
            

            /*==========================
                General Advance Settings
            ==========================*/
            advanceId:{
                type: 'string',
                default: '',
            },
            advanceZindex:{
                type: 'string',
                default: '',
                style:[{ selector: '{{ULTP}} .ultp-tabs-wrapper {z-index:{{advanceZindex}};}' }],
            },
            hideExtraLarge: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideTablet: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideMobile: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            advanceCss: {
                type: 'string',
                default: '',
                style: [{selector: ''}],
            }
        }
    }
)