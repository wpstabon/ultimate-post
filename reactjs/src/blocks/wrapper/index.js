const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType(
    'ultimate-post/wrapper', {
        title: __('Wrapper','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/wrapper.svg'}/>,
        category: 'ultimate-post',
        description: __('Wrapper block for Gutenberg.','ultimate-post'),
        keywords: [ 
            __('wrapper','ultimate-post'),
            __('wrap','ultimate-post'),
            __('column','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/wrapper.svg',
            },
        },
        edit: Edit,
        save: Save,
    }
)