const { Component } = wp.element;
const { InnerBlocks } = wp.blockEditor

export default function Save(props) {
    const { blockId, advanceId } = props.attributes
    return (
        <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId}`}>
            <div className={`ultp-wrapper-block`}>
                <InnerBlocks.Content />
            </div>
        </div>
    )
}