const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element
import { CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink, isInlineCSS, updateCurrentPostId } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, className, clientId, name, attributes, attributes: { blockId, currentPostId, previewImg, advanceId } } = props;

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/wrapper', blockId, isInlineCSS());
	}

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<InspectorControls>
				<Sections>
					<Section slug="setting" title={__('Style', 'ultimate-post')}>
						<GeneralAdvanced store={store} />
					</Section>
					<Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
						<ResponsiveAdvanced store={store} />
						<CustomCssAdvanced store={store} />
					</Section>
				</Sections>
				{blockSupportLink()}
			</InspectorControls>
			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className={`ultp-wrapper-block`}>
					<InnerBlocks templateLock={false} />
				</div>
			</div>
		</Fragment>
	)
}
