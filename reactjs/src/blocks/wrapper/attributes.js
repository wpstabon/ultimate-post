import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    currentPostId: { type: "string" , default: "" },
    //--------------------------
    //  Wrapper Style
    //--------------------------
    advanceId:{
        type: 'string',
        default: '',
    },
    ...commonAttributes(['advanceAttr'], ['loadingColor', 'advanceId'], [
        { key: 'wrapBg', default: {openColor: 1, type: 'color', color: 'var(--postx_preset_Base_2_color)'}, style: [{selector:'{{ULTP}} .ultp-wrapper-block'}] }, 
        { key: 'wrapBorder', style: [{ selector:'{{ULTP}} .ultp-wrapper-block'}] }, 
        { key: 'wrapShadow', style: [{ selector: '{{ULTP}} .ultp-wrapper-block' }] }, 
        { key: 'wrapRadius', style: [{ selector: '{{ULTP}} .ultp-wrapper-block { border-radius:{{wrapRadius}}; }' }] }, 
        { key: 'wrapHoverBackground', style:[{ selector: '{{ULTP}} .ultp-wrapper-block:hover' }]  }, 
        { key: 'wrapHoverBorder', style: [{ selector: '{{ULTP}} .ultp-wrapper-block:hover' }] }, 
        { key: 'wrapHoverRadius', style:[{ selector: '{{ULTP}} .ultp-wrapper-block:hover { border-radius:{{wrapHoverRadius}}; }' }]  }, 
        { key: 'wrapHoverShadow', style: [{ selector: '{{ULTP}} .ultp-wrapper-block:hover' }] }, 
        { key: 'wrapMargin', style: [{ selector:'{{ULTP}} .ultp-wrapper-block { margin:{{wrapMargin}}; }' }] }, 
        { key: 'wrapOuterPadding', default: {lg:{top: '30',bottom: '30',left: '30', right: '30', unit:'px'}}, style: [ {selector: '{{ULTP}} .ultp-wrapper-block { padding:{{wrapOuterPadding}}; }'} ] }, 
        { key: 'advanceZindex', style: [ {selector: '{{ULTP}} .ultp-wrapper-block { padding:{{wrapOuterPadding}}; }'} ] }
    ]),
    
}
export default attributes;