const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/taxonomy-1/', 'block_docs');

registerBlockType(
    'ultimate-post/ultp-taxonomy', {
        title: __('Taxonomy','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/ultp-taxonomy.svg'}/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Listing your Taxonomy in grid / list view.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [
            __('Taxonomy','ultimate-post'),
            __('Category','ultimate-post'),
            __('Category List','ultimate-post')
        ],
        attributes,
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/taxonomy.svg',
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)