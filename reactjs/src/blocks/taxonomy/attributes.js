import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { V4_1_0_CompCheck } from "../../helper/compatibility";

const attributes = {
  /*============================
      General Setting
  ============================*/
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  layout: { type: "string", default: "1" },
  taxGridEn: { type: "boolean", default: true },
  columns: {
    type: "object",
    default: { lg: "1" },
    style: [
      {
        depends: [{ key: "taxGridEn", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-taxonomy-items { grid-template-columns: repeat({{columns}}, 1fr); }",
      },
    ],
  },
  columnGridGap: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        depends: [{ key: "taxGridEn", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-taxonomy-items { grid-column-gap: {{columnGridGap}}; } 
          {{ULTP}} .ultp-taxonomy-item { margin-bottom: 0; }`,
      },
    ],
  },
  rowGap: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        depends: [{ key: "taxGridEn", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-taxonomy-items { grid-row-gap: {{rowGap}}; }",
      },
    ],
  },
  titleShow: { type: "boolean", default: true },
  headingShow: { type: "boolean", default: true },
  excerptShow: { type: "boolean", default: false },
  countShow: { type: "boolean", default: true },
  openInTab: { type: "boolean", default: false },
  notFoundMessage: { type: "string", default: "No Taxonomy Found." },

  /*============================
      Query Settings
  ============================*/
  taxType: { type: "string", default: "regular" },
  taxSlug: { type: "string", default: "category" },
  taxValue: {
    type: "string",
    default: "[]",
    style: [
      {
        depends: [
          { key: "taxType", condition: "!=", value: ["parent", "regular"] },
        ],
      },
    ],
  },
  queryNumber: {
    type: "string",
    default: 6,
    style: [
      { depends: [{ key: "taxType", condition: "!=", value: "custom" }] },
    ],
  },
  /*============================
      Image Settings
  ============================*/
  imgCrop: { type: "string", default: (ultp_data.disable_image_size == 'yes' ? "full" : "ultp_layout_landscape") },
  imgWidth: {
    type: "object",
    default: { lg: "", ulg: "%" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-4 a img, 
          {{ULTP}} .ultp-taxonomy-layout-5 a img { max-width: {{imgWidth}}; }`,
      },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-4 a img,  
          {{ULTP}} .ultp-taxonomy-layout-5 a img {object-fit: cover; height: {{imgHeight}}; }`,
      },
    ],
  },
  imgSpacing: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-4 a img,  
          {{ULTP}} .ultp-taxonomy-layout-5 a img { margin-bottom: {{imgSpacing}}px; }`,
      },
    ],
  },
  contentAlign: {
    type: "string",
    default: "center",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-5 li a,  
          {{ULTP}} .ultp-taxonomy-layout-4 li a { text-align:{{contentAlign}}; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-5 li a,  
          {{ULTP}} .ultp-taxonomy-layout-4 li a { text-align:{{contentAlign}}; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-5 li a,  
          {{ULTP}} .ultp-taxonomy-layout-4 li a { text-align:{{contentAlign}}; }`,
      },
    ],
  },

  /*============================
      Content Settings
  ============================*/
  contentWrapBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-item a .ultp-taxonomy-lt5-content { background:{{contentWrapBg}}; }`,
      },
    ],
  },
  contentWrapHoverBg: {
    type: "string",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-item a:hover .ultp-taxonomy-lt5-content { background:{{contentWrapHoverBg}}; }`,
      },
    ],
  },
  contentWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a`,
      },
    ],
  },
  contentWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a:hover`,
      },
    ],
  },
  contentWrapRadius: {
    type: "object",
    default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a { border-radius: {{contentWrapRadius}}; }`,
      },
    ],
  },
  contentWrapHoverRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a:hover { border-radius: {{contentWrapHoverRadius}}; }`,
      },
    ],
  },
  contentWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a`,
      },
    ],
  },
  contentWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a:hover,  
          {{ULTP}} .ultp-taxonomy-layout-5 .ultp-taxonomy-item a:hover`,
      },
    ],
  },
  contentWrapPadding: {
    type: "object",
    default: { lg: { unit: "px", top: "6", right: "12", bottom: "6", left: "12" } },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["1", "4", "5"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-1 .ultp-taxonomy-item a,
          {{ULTP}} .ultp-taxonomy-layout-4 .ultp-taxonomy-item a,
          {{ULTP}} .ultp-taxonomy-item a .ultp-taxonomy-lt5-content { padding: {{contentWrapPadding}}; }`,
      },
    ],
  },

  /*============================
      Taxonomy Wrapper Settings
  ============================*/
  customTaxColor: { type: "boolean", default: false },
  seperatorTaxLink: {
    type: "string",
    default: ultp_data.category_url,
    style: [
      { depends: [{ key: "customTaxColor", condition: "==", value: true }] },
    ],
  },
  TaxAnimation: { type: "string", default: "none" },
  TaxWrapBg: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "customTaxColor", condition: "!=", value: true }],
        selector:
          `{{ULTP}} .ultp-taxonomy-items li a .ultp-taxonomy-lt2-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a .ultp-taxonomy-lt3-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a .ultp-taxonomy-lt6-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a .ultp-taxonomy-lt7-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a .ultp-taxonomy-lt8-overlay { background:{{TaxWrapBg}}; }`,
      },
    ],
  },
  TaxWrapHoverBg: {
    type: "string",
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
          { key: "customTaxColor", condition: "!=", value: true },
        ],
        selector:
          `{{ULTP}} .ultp-taxonomy-items a:hover .ultp-taxonomy-lt2-overlay,
          {{ULTP}} .ultp-taxonomy-items a:hover .ultp-taxonomy-lt3-overlay, 
          {{ULTP}} .ultp-taxonomy-items a:hover .ultp-taxonomy-lt6-overlay,  
          {{ULTP}} .ultp-taxonomy-items a:hover .ultp-taxonomy-lt7-overlay,  
          {{ULTP}} .ultp-taxonomy-items a:hover .ultp-taxonomy-lt8-overlay { background:{{TaxWrapHoverBg}}; }`,
      },
    ],
  },
  TaxWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector: "{{ULTP}} .ultp-taxonomy-item a",
      },
    ],
  },
  TaxWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector: "{{ULTP}} .ultp-taxonomy-item a:hover",
      },
    ],
  },
  TaxWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector: "{{ULTP}} .ultp-taxonomy-item a",
      },
    ],
  },
  TaxWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector: "{{ULTP}} .ultp-taxonomy-item a:hover",
      },
    ],
  },
  TaxWrapRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector:
          "{{ULTP}} .ultp-taxonomy-item a { border-radius: {{TaxWrapRadius}}; }",
      },
    ],
  },
  TaxWrapHoverRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector:
          "{{ULTP}} .ultp-taxonomy-item a:hover { border-radius: {{TaxWrapHoverRadius}}; }",
      },
    ],
  },
  customOpacityTax: {
    type: "string",
    default: 0.59999999999999998,
    style: [
      {
        selector:
          `{{ULTP}} .ultp-taxonomy-lt2-overlay,  
          {{ULTP}} .ultp-taxonomy-lt3-overlay,  
          {{ULTP}} .ultp-taxonomy-lt6-overlay,  
          {{ULTP}} .ultp-taxonomy-lt7-overlay,  
          {{ULTP}} .ultp-taxonomy-lt8-overlay { opacity: {{customOpacityTax}}; }`,
      },
    ],
  },
  customTaxOpacityHover: {
    type: "string",
    default: 0.90000000000000002,
    style: [
      {
        selector:
          `{{ULTP}} .ultp-taxonomy-items li a:hover .ultp-taxonomy-lt2-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a:hover .ultp-taxonomy-lt3-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a:hover .ultp-taxonomy-lt6-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a:hover .ultp-taxonomy-lt7-overlay,  
          {{ULTP}} .ultp-taxonomy-items li a:hover .ultp-taxonomy-lt8-overlay { opacity: {{customTaxOpacityHover}}; }`,
      },
    ],
  },
  TaxWrapPadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6", "7", "8"] },
        ],
        selector:
          "{{ULTP}} .ultp-taxonomy-item a { padding: {{TaxWrapPadding}}; }",
      },
    ],
  },

  /*============================
      Title Settings
  ============================*/
  titleTag: { type: "string", default: "span" },
  contentTitleAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: "1" },
          { key: "countShow", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .ultp-block-items-wrap ul li a { justify-content: {{contentTitleAlign}};}",
      },
    ],
  },
  titlePosition: { type: "boolean", default: true },
  customTaxTitleColor: {
    type: "boolean",
    default: false,
    style: [
      { depends: [{ key: "layout", condition: "!=", value: ["2", "3", "6"] }] },
    ],
  },
  seperatorTaxTitleLink: {
    type: "string",
    default: ultp_data.category_url,
    style: [
      {
        depends: [
          { key: "customTaxTitleColor", condition: "==", value: true },
          { key: "layout", condition: "!=", value: ["2", "3", "6"] },
        ],
      },
    ],
  },
  titleColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [
          { key: "customTaxTitleColor", condition: "==", value: false },
        ],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-name { color:{{titleColor}}; }",
      },
      {
        depends: [
          { key: "layout", condition: "==", value: ["2", "3", "6"] },
        ],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-name { color:{{titleColor}}; }",
      },
    ],
  },
  TitleBgColor: {
    type: "string",
    default: "var(--postx_preset_Base_3_color)",
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["7", "8"] },
          { key: "customTaxTitleColor", condition: "==", value: false },
        ],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-7 .ultp-taxonomy-name,  
          {{ULTP}} .ultp-taxonomy-layout-8 .ultp-taxonomy-name { background:{{TitleBgColor}}; }`,
      },
    ],
  },
  TitleBgHoverColor: {
    type: "string",
    default: "var(--postx_preset_Base_3_color)",
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["7", "8"] },
          { key: "customTaxTitleColor", condition: "==", value: false },
        ],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-7 li a:hover .ultp-taxonomy-name,  
          {{ULTP}} .ultp-taxonomy-layout-8 li a:hover .ultp-taxonomy-name { background:{{TitleBgHoverColor}}; }`,
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-item a:hover .ultp-taxonomy-name,  
          {{ULTP}} .ultp-block-item a:hover .ultp-taxonomy-count { color:{{titleHoverColor}}!important; }`,
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "14", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "22", unit: "px" },
      transform: "",
      decoration: "none",
      family: "",
      weight: "",
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-item .ultp-taxonomy-name",
      },
    ],
  },
  titleDotColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2", "3"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-2 .ultp-taxonomy-bar,  
          {{ULTP}} .ultp-taxonomy-layout-3 .ultp-taxonomy-bar { border-bottom-color:{{titleDotColor}}; }`,
      },
    ],
  },
  titleDotHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2", "3"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-2 li:hover a .ultp-taxonomy-bar,  
          {{ULTP}} .ultp-taxonomy-layout-3 li:hover a .ultp-taxonomy-bar { border-bottom-color:{{titleDotHoverColor}}; }`,
      },
    ],
  },
  titleDotSize: {
    type: "string",
    default: "solid",
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2", "3"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-2 .ultp-taxonomy-bar,  
          {{ULTP}} .ultp-taxonomy-layout-3 .ultp-taxonomy-bar { border-bottom-width:{{titleDotSize}}px; }`,
      },
    ],
  },
  titleDotStyle: {
    type: "string",
    default: { lg: "1" },
    style: [
      {
        depends: [{ key: "layout", condition: "==", value: ["2", "3"] }],
        selector:
          `{{ULTP}} .ultp-taxonomy-layout-2 .ultp-taxonomy-bar,  
          {{ULTP}} .ultp-taxonomy-layout-3 .ultp-taxonomy-bar { border-bottom-style: {{titleDotStyle}}; }`,
      },
    ],
  },
  titlePadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-name { padding:{{titlePadding}}; }",
      },
    ],
  },
  titleRadius: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      {
        depends: [
          { key: "titleShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: ["7", "8"] },
        ],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-name { border-radius:{{titleRadius}}; }",
      },
    ],
  },

  /*============================
      Excerpt Settings
  ============================*/
  excerptLimit: { type: "string", default: 30 },
  excerptColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_3_color)",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-desc { color:{{excerptColor}}; }",
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: "22", unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-taxonomy-desc",
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: "0", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-taxonomy-desc { padding: {{excerptPadding}}; }",
      },
    ],
  },

  /*============================
      Counter Settings
  ============================*/
  counterTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: "22", unit: "px" },
      spacing: { lg: 0, unit: "px" },
      transform: "",
      weight: "400",
      decoration: "none",
      family: "",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-item .ultp-taxonomy-count" }],
  },
  counterColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-count { color:{{counterColor}}; }",
      },
    ],
  },
  counterBgColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Base_1_color)" },
    style: [{ selector: "{{ULTP}} .ultp-block-item .ultp-taxonomy-count" }],
  },
  counterWidth: {
    type: "string",
    default: "24",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-count { max-width:{{counterWidth}}px; width: 100% }",
      },
    ],
  },
  counterHeight: {
    type: "string",
    default: "24",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-count { height:{{counterHeight}}px; line-height:{{counterHeight}}px !important; }",
      },
    ],
  },
  counterBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-item .ultp-taxonomy-count" }],
  },
  counterRadius: {
    type: "object",
    default: { lg: { top: '22', right: '22', bottom: '22', left: '22', unit: 'px' }, unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item .ultp-taxonomy-count { border-radius:{{counterRadius}}; }",
      },
    ],
  },

  /*============================
      Separator Settings
  ============================*/
  separatorShow: {
    type: "boolean",
    default: false,
    depends: [{ key: "taxGridEn", condition: "!=", value: true }],
  },
  septColor: {
    type: "string",
    default: "var(--postx_preset_Base_3_color)",
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:not(:last-of-type) { border-bottom-color:{{septColor}}; }",
      },
    ],
  },
  septStyle: {
    type: "string",
    default: "solid",
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:not(:last-of-type) { border-bottom-style:{{septStyle}}; }",
      },
    ],
  },
  septSize: {
    type: "string",
    default: { lg: "1" },
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:not(:last-of-type) { border-bottom-width: {{septSize}}px; }",
      },
    ],
  },
  septSpace: {
    type: "object",
    default: { lg: "5" },
    style: [
      {
        depends: [{ key: "taxGridEn", condition: "!=", value: true }],
        selector:
          `{{ULTP}} .ultp-block-item:not(:last-of-type) { padding-bottom: {{septSpace}}px; }  
          {{ULTP}} .ultp-block-item:not(:last-of-type) { margin-bottom: {{septSpace}}px; }`,
      },
    ],
  },
/*============================
      Heading Settings
  ============================*/
  headingText: { type: "string", default: "Post Taxonomy" },
  // headingURL: { type: "string", default: "" },
  // headingBtnText: {
  //   type: "string",
  //   default: "View More",
  //   style: [
  //     { depends: [{ key: "headingStyle", condition: "==", value: "style11" }] },
  //   ],
  // },
  // headingStyle: { type: "string", default: "style9" },
  // headingTag: { type: "string", default: "h2" },
  // headingAlign: {
  //   type: "string",
  //   default: "left",
  //   style: [
  //     {
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner,  
  //         {{ULTP}} .ultp-sub-heading-inner { text-align:{{headingAlign}}; }`,
  //     },
  //   ],
  // },
  // headingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "20", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "700",
  //   },
  //   style: [{ selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-inner" }],
  // },
  // headingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_1_color)",
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { color:{{headingColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before,  
  //         {{ULTP}} .ultp-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor2: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_3_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //   ],
  // },
  // headingBg: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_3_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-style5 .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; }  
  //         {{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span,  
  //         {{ULTP}} .ultp-heading-inner span:after { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; }  
  //         {{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }`,
  //     },
  //   ],
  // },
  // headingBg2: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg2}}; }",
  //     },
  //   ],
  // },
  // headingBtnTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "14", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-wrap .ultp-heading-btn",
  //     },
  //   ],
  // },
  // headingBtnColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn { color:{{headingBtnColor}}; }  
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn svg { fill:{{headingBtnColor}}; }`,
  //     },
  //   ],
  // },
  // headingBtnHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Secondary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover { color:{{headingBtnHoverColor}}; }  
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }`,
  //     },
  //   ],
  // },
  // headingBorder: {
  //   type: "string",
  //   default: "3",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before, 
  //         {{ULTP}} .ultp-heading-inner span:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //   ],
  // },
  // headingSpacing: {
  //   type: "object",
  //   default: { lg: 20, sm: 10, unit: "px" },
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-wrap { margin-top:0; margin-bottom:{{headingSpacing}}; }",
  //     },
  //   ],
  // },
  // headingRadius: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //   ],
  // },
  // headingPadding: {
  //   type: "object",
  //   default: { lg: { unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //   ],
  // },
  // subHeadingShow: { type: "boolean", default: false },
  // subHeadingText: {
  //   type: "string",
  //   default:
  //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.",
  //   style: [
  //     { depends: [{ key: "subHeadingShow", condition: "==", value: true }] },
  //   ],
  // },
  // subHeadingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "16", unit: "px" },
  //     spacing: { lg: "0", unit: "px" },
  //     height: { lg: "27", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "500",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-sub-heading div",
  //     },
  //   ],
  // },
  // subHeadingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading div { color:{{subHeadingColor}}; }",
  //     },
  //   ],
  // },
  // subHeadingSpacing: {
  //   type: "object",
  //   default: { lg: { top: "8", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading-inner { margin:{{subHeadingSpacing}}; }",
  //     },
  //   ],
  // },
  // enableWidth: {
  //   type: 'toggle',
  //   default: false,
  //   style: [{
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //   }],
  // },
  // customWidth: {
  //     type: "object",
  //     default: { lg: { top: "", unit: "px" }},
  //     style: [{
  //         depends: [
  //             { key: "subHeadingShow", condition: "==", value: true },
  //             { key: "enableWidth", condition: "==", value: true }
  //         ],
  //         selector: "{{ULTP}} .ultp-sub-heading-inner { max-width:{{customWidth}}; }",
  //     }],
  // },

  /* ========= Advanced Settings ========= */
  ...commonAttributes(['advanceAttr', 'heading'], ['loadingColor']),

  //Compatibility
  V4_1_0_CompCheck,
};
export default attributes;
