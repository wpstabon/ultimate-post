const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { useEffect, useState, useRef, Fragment } = wp.element
const { Spinner, Placeholder } = wp.components
import { blockSupportLink, CounterStyle, CustomCssAdvanced, ExcerptStyle, GeneralAdvanced, GeneralContent, HeadingContent, ImageStyle, isInlineCSS, isReload, ResponsiveAdvanced, SeparatorStyle, TaxQuery, TaxWrapStyle, TitleStyle, updateCurrentPostId, WrapStyle } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import { Section, Sections } from '../../helper/Sections'
import TemplateModal from '../../helper/TemplateModal'
import ToolBarElement from '../../helper/ToolBarElement'
import Heading from '../../helper/block_template/Heading'
import { handleCompatibility } from '../../helper/compatibility'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
	const [state, setState] = useState({ postsList: [], loading: true, error: false });
	const { setAttributes, name, clientId, className, attributes, attributes: { blockId, advanceId, taxGridEn, headingText, headingStyle, headingShow, headingAlign, headingURL, headingBtnText, subHeadingShow, subHeadingText, taxType, previewImg, layout, headingTag, countShow, titleShow, excerptShow, TaxAnimation, columns, customTaxTitleColor, customTaxColor, imgCrop, titleTag, notFoundMessage, V4_1_0_CompCheck: { runComp }, currentPostId, taxSlug, taxValue, queryNumber } } = props

	useEffect(() => {
		fetchTaxonomy();
	}, []);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (!prevAttributes) {
			prevPropsRef.current = attributes;
		} else if (isReload(prevAttributes, attributes)) {
			fetchTaxonomy();
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	useEffect(() => {
		const _client = clientId.substr(0, 6);
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		handleCompatibility(props);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	function fetchTaxonomy() {
		if (state.error) {
			setState({ ...state, error: false });
		}
		if (!state.loading) {
			setState({ ...state, loading: true });
		}
		wp.apiFetch({
			path: '/ultp/specific_taxonomy',
			method: 'POST',
			data: { taxValue, queryNumber, taxType, taxSlug, wpnonce: ultp_data.security, archiveBuilder: ultp_data.archive == 'archive' ? ultp_data.archive : '' }
		}).then((obj) => {
			setState({ ...state, postsList: obj, loading: false })
		}).catch((error) => {
			setState({ ...state, loading: false, error: true })
		})
	}

	function renderContent() {
		const CustomTitleTag = titleTag;

		if (previewImg) {
			return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
		}

		return (
			!state.error ? !state.loading ? state.postsList.length > 0 ?
				<div className={`ultp-block-items-wrap`}>
					<ul className={`ultp-taxonomy-items ${(TaxAnimation != 'none') ? 'ultp-taxonomy-animation-' + TaxAnimation : ''} ultp-taxonomy-column-${columns.lg} ultp-taxonomy-layout-${layout}`}>
						{(state.postsList.map((post, idx) => {
							const styleCSS = post.image ? { backgroundImage: `url(${post.image[imgCrop]})` } : { background: `${post.color}` };
							return (
								<li key={idx} className={`ultp-block-item ultp-taxonomy-item`}>

									{layout == 1 &&
										<Fragment>
											<a href="#">
												{titleShow && <CustomTitleTag className="ultp-taxonomy-name" style={customTaxTitleColor ? { color: post.color } : {}}>{post.name}</CustomTitleTag>}
												{countShow && <span className="ultp-taxonomy-count" style={customTaxTitleColor ? { color: post.color } : {}}>{post.count}</span>}
												{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
											</a>

										</Fragment>
									}
									{layout == 2 &&
										<Fragment>
											<a href="#" style={styleCSS}>
												<div className="ultp-taxonomy-lt2-overlay" style={customTaxColor ? { backgroundColor: post.color } : {}}></div>
												<div className="ultp-taxonomy-lt2-content">
													{titleShow &&
														<Fragment>
															<CustomTitleTag className="ultp-taxonomy-name">{post.name}</CustomTitleTag>
															<span className="ultp-taxonomy-bar"></span>
														</Fragment>
													}
													{countShow && <span className="ultp-taxonomy-count">{post.count}</span>}
												</div>
												{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
											</a>
										</Fragment>
									}
									{layout == 3 &&
										<a href="#">
											<div className="ultp-taxonomy-lt3-img" style={styleCSS}></div>
											<div className="ultp-taxonomy-lt3-overlay" style={customTaxColor ? { backgroundColor: post.color } : {}}></div>
											<div className="ultp-taxonomy-lt3-content">
												{titleShow &&
													<Fragment>
														<CustomTitleTag className="ultp-taxonomy-name">{post.name}</CustomTitleTag>
														<span className="ultp-taxonomy-bar"></span>
													</Fragment>
												}
												{countShow && <span className="ultp-taxonomy-count">{post.count}</span>}
											</div>
											{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
										</a>
									}
									{layout == 4 &&
										<a href="#">
											{(post.image && post.image.full) &&
												<img src={post?.image[imgCrop]} alt={post.name} />
											}
											<div className="ultp-taxonomy-lt4-content">
												{titleShow && <CustomTitleTag className="ultp-taxonomy-name" style={customTaxTitleColor ? { color: post.color } : {}}>{post.name}</CustomTitleTag>}
												{countShow && <span className="ultp-taxonomy-count" style={customTaxTitleColor ? { color: post.color } : {}}>{post.count}</span>}
											</div>
											{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
										</a>
									}
									{layout == 5 &&
										<a href="#">
											{(post.image && post.image.full) &&
												<img src={post?.image[imgCrop]} alt={post.name} />
											}
											<span className="ultp-taxonomy-lt5-content">
												{titleShow && <CustomTitleTag className="ultp-taxonomy-name" style={customTaxTitleColor ? { color: post.color } : {}}>{post.name}</CustomTitleTag>}
												{countShow && <span className="ultp-taxonomy-count" style={customTaxTitleColor ? { color: post.color } : {}}>{post.count}</span>}
												{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
											</span>
										</a>
									}
									{layout == 6 &&
										<a href="#" style={styleCSS}>
											<div className="ultp-taxonomy-lt6-overlay" style={customTaxColor ? { backgroundColor: post.color } : {}}></div>
											{titleShow && <CustomTitleTag className="ultp-taxonomy-name">{post.name}</CustomTitleTag>}
											{countShow && <span className="ultp-taxonomy-count">{post.count}</span>}
											{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
										</a>
									}
									{layout == 7 &&
										<a href="#" style={styleCSS}>
											<div className="ultp-taxonomy-lt7-overlay" style={customTaxColor ? { backgroundColor: post.color } : {}}></div>
											{titleShow &&
												<CustomTitleTag className="ultp-taxonomy-name" style={customTaxTitleColor ? { backgroundColor: post.color } : {}}>{post.name}
													{countShow && <span className="ultp-taxonomy-count">{post.count}</span>}
												</CustomTitleTag>
											}
											{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
										</a>
									}
									{layout == 8 &&
										<a href="#" style={styleCSS}>
											<div className="ultp-taxonomy-lt8-overlay" style={customTaxColor ? { backgroundColor: post.color } : {}}></div>
											{titleShow &&
												<CustomTitleTag className="ultp-taxonomy-name" style={customTaxTitleColor ? { backgroundColor: post.color } : {}}>{post.name}
													{countShow && <span className="ultp-taxonomy-count">{post.count}</span>}
												</CustomTitleTag>
											}
											{excerptShow && <div className="ultp-taxonomy-desc">{post.desc}</div>}
										</a>
									}
								</li>
							)
						}))}
					</ul>
				</div>
				: (<Placeholder label={notFoundMessage}></Placeholder>)
				: (<Placeholder label={__('Loading...', 'ultimate-post')}><Spinner /></Placeholder>)
				: (<Placeholder label={__('Posts are not available.', 'ultimate-post')} ><div style={{ marginBottom: 15 }}>{__('Make sure Add Post.', 'ultimate-post')}</div></Placeholder>)
		)
	}

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/ultp-taxonomy', blockId, isInlineCSS());
	}

	return (
		<Fragment>
			<InspectorControls>
				<TemplateModal
					prev="https://www.wpxpo.com/postx/blocks/#demoid6841"
					store={store}
				/>
				<Sections>
					<Section slug="setting" title={__('Setting', 'ultimate-post')}>
						<GeneralContent store={store} initialOpen={true}
							exclude={['contentAlign', 'openInTab', 'contentTag']}
							include={[
								{
									position: 0, data: {
										type: 'layout', block: 'ultp-taxonomy', key: 'layout', label: __('Layout', 'ultimate-post'),
										options: [
											{ img: 'assets/img/layouts/taxonomy/l1.png', label: __('Layout 1', 'ultimate-post'), value: '1', pro: false },
											{ img: 'assets/img/layouts/taxonomy/l2.png', label: __('Layout 2', 'ultimate-post'), value: '2', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l3.png', label: __('Layout 3', 'ultimate-post'), value: '3', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l4.png', label: __('Layout 4', 'ultimate-post'), value: '4', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l5.png', label: __('Layout 5', 'ultimate-post'), value: '5', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l6.png', label: __('Layout 6', 'ultimate-post'), value: '6', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l7.png', label: __('Layout 7', 'ultimate-post'), value: '7', pro: true },
											{ img: 'assets/img/layouts/taxonomy/l8.png', label: __('Layout 8', 'ultimate-post'), value: '8', pro: true },
										]
									}
								},
								{
									position: 1, data: { type: 'toggle', key: 'taxGridEn', label: __('Grid View', 'ultimate-post'), pro: true }
								},
								{
									position: 4, data: { type: 'range', key: 'rowGap', min: 0, max: 120, step: 1, responsive: true, unit: ['px', 'em', 'rem'], label: __('Row Gap', 'ultimate-post') },
								},
								{
									data: { type: 'text', key: 'notFoundMessage', label: __('No result found Text', 'ultimate-post') }
								},
							]}
						/>
						<TaxQuery store={store} />
						{
							!runComp && (
								<HeadingContent
									store={store}
									depend="headingShow" />
							)
						}
						{
							(layout == '4' || layout == '5') &&
							<ImageStyle
								store={store}
								exclude={
									['imgMargin', 'imgCropSmall', 'imgAnimation', 'imgOverlay', 'imgOpacity', 'overlayColor', 'imgOverlayType', 'imgGrayScale', 'imgHoverGrayScale', 'imgShadow', 'imgHoverShadow', 'imgTab', 'imgHoverRadius', 'imgRadius', 'imgSrcset', 'imgLazy', 'imageScale']
								}
								include={[
									{
										position: 3, data: { type: 'alignment', key: 'contentAlign', responsive: false, label: __('Alignment', 'ultimate-post'), disableJustify: true }
									},
									{
										position: 2, data: { type: 'range', key: 'imgSpacing', label: __('Img Spacing', 'ultimate-post'), min: 0, max: 100, step: 1, responsive: true }
									}
								]}
							/>}
						{
							(layout == '1' || layout == '4' || layout == '5') &&
							<WrapStyle
								store={store}
								exclude={
									['contenWraptWidth', 'contenWraptHeight', 'contentWrapInnerPadding']
								}
								include={[
									{
										position: 0, data: {
											type: 'select', key: 'TaxAnimation', label: __('Hover Animation', 'ultimate-post'),
											options: [
												{ value: 'none', label: __('No Animation', 'ultimate-post') },
												{ value: 'zoomIn', label: __('Zoom In', 'ultimate-post') },
												{ value: 'zoomOut', label: __('Zoom Out', 'ultimate-post') },
												{ value: 'opacity', label: __('Opacity', 'ultimate-post') },
												{ value: 'slideLeft', label: __('Slide Left', 'ultimate-post') },
												{ value: 'slideRight', label: __('Slide Right', 'ultimate-post') },
											]
										}
									},
								]}
							/>
						}
						{
							(layout == '2' || layout == '3' || layout == '6' || layout == '7' || layout == '8') &&
							<TaxWrapStyle store={store} />
						}
						<TitleStyle
							depend="titleShow"
							store={store}
							exclude={
								['titlePosition', 'titleLength', 'titleBackground', 'titleStyle', 'titleAnimColor']
							}
							include={[
								{
									position: 0, data: { type: 'toggle', key: 'customTaxTitleColor', label: __('Specific Color', 'ultimate-post'), pro: true }
								},
								{
									position: 1, data: { type: 'alignment', key: 'contentTitleAlign', responsive: false, label: __('Title Align', 'ultimate-post'), disableJustify: true }
								},
								{
									position: 2, data: { type: 'linkbutton', key: 'seperatorTaxTitleLink', placeholder: __('Choose Color', 'ultimate-post'), label: __('Taxonomy Specific (Pro)', 'ultimate-post'), text: 'Choose Color' }
								},
								{
									position: 5, data: { type: 'color', key: 'TitleBgColor', label: __('Background Color', 'ultimate-post') }
								},
								{
									position: 6, data: { type: 'color', key: 'TitleBgHoverColor', label: __('Hover Background Color', 'ultimate-post') }
								},
								{
									position: 7, data: { type: 'color', key: 'titleDotColor', label: __('Border Color', 'ultimate-post') }
								},
								{
									position: 8, data: { type: 'color', key: 'titleDotHoverColor', label: __('Border Hover Color', 'ultimate-post') }
								},
								{
									position: 9, data: { type: 'range', key: 'titleDotSize', label: __('Border Size', 'ultimate-post'), min: 0, max: 5, step: 1 }
								},
								{
									position: 10, data: {
										type: 'select', key: 'titleDotStyle', label: __('Border Style', 'ultimate-post'),
										options: [
											{ value: 'none', label: __('None', 'ultimate-post') },
											{ value: 'solid', label: __('Solid', 'ultimate-post') },
											{ value: 'dashed', label: __('Dashed', 'ultimate-post') },
											{ value: 'dotted', label: __('Dotted', 'ultimate-post') },
											{ value: 'double', label: __('Double', 'ultimate-post') },
										]
									}
								},
								{
									position: 11, data: { type: 'range', key: 'titleRadius', min: 0, max: 300, step: 1, responsive: true, unit: ['px', 'em', 'rem', '%'], label: __('Border Radius', 'ultimate-post') }
								}
							]}
						/>
						<ExcerptStyle store={store} depend="excerptShow" exclude={['showSeoMeta', 'excerptLimit', 'showFullExcerpt']} />
						<CounterStyle store={store} depend="countShow" />

						{!taxGridEn && <SeparatorStyle depend='separatorShow' store={store} />}
					</Section>
					<Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
						<GeneralAdvanced initialOpen={true} store={store} />
						<ResponsiveAdvanced store={store} />
						<CustomCssAdvanced store={store} />
					</Section>
				</Sections>
				{blockSupportLink()}
			</InspectorControls>

			<ToolBarElement
				include={[
					{
						type: 'query',
						taxQuery: true
					},
					{
						type: 'template'
					},
					{
						type: 'layout', block: 'ultp-taxonomy', key: 'layout',
						options: [
							{ img: 'assets/img/layouts/taxonomy/l1.png', label: __('Layout 1', 'ultimate-post'), value: '1', pro: false },
							{ img: 'assets/img/layouts/taxonomy/l2.png', label: __('Layout 2', 'ultimate-post'), value: '2', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l3.png', label: __('Layout 3', 'ultimate-post'), value: '3', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l4.png', label: __('Layout 4', 'ultimate-post'), value: '4', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l5.png', label: __('Layout 5', 'ultimate-post'), value: '5', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l6.png', label: __('Layout 6', 'ultimate-post'), value: '6', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l7.png', label: __('Layout 7', 'ultimate-post'), value: '7', pro: true },
							{ img: 'assets/img/layouts/taxonomy/l8.png', label: __('Layout 8', 'ultimate-post'), value: '8', pro: true },
						]
					}
				]} store={store} />

			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className={`ultp-block-wrapper`}>
					{headingShow &&
						<div className={`ultp-heading-filter`}>
							<div className={`ultp-heading-filter-in`}>
								<Heading
									props={
										{
											headingShow,
											headingStyle,
											headingAlign,
											headingURL,
											headingText,
											setAttributes,
											headingBtnText,
											subHeadingShow,
											subHeadingText,
											headingTag
										}
									}
								/>
							</div>
						</div>
					}
					{renderContent()}
				</div>
			</div>
		</Fragment>
	)
}
