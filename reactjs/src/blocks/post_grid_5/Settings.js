const { __ } = wp.i18n;

import {
	GeneralContentWithQuery,
	PaginationContent,
	FilterContent,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	HeadingContent,
	ImageStyle,
	ReadMoreStyle,
	TitleStyle,
	CategoryStyle,
	ExcerptStyle,
	MetaStyle,
	VideoStyle,
	blockSupportLink,
	getTitleToolbarSettings,
	getExcerptToolbarSettings,
	getMetaToolbarSettings,
	getReadMoreToolbarSettings,
	getCategoryToolbarSettings,
	TypographyTB,
	ToolbarSettingsAndStyles,
	imageSettingsKeys,
	imageStyleKeys,
	ImageStyleArg,
	getVideoToolbarSettings,
	readMoreSettings,
	readMoreStyle,
	ReadMoreStyleArg,
	categorySettings,
	categoryStyle,
	CategoryStyleArg,
	metaSettings,
	metaStyle,
	MetaStyleArg,
	metaText,
	pagiSettings,
	pagiStyles,
	PaginationContentArg,
	getPagiToolbarSettings,
	headingSettings,
	headingStyle,
	HeadingContentArg,
	filterSettings,
	filterStyle,
	FilterContentArg,
	getFilterToolbarSettings,
	spaceingIcon as spacingIcon,
	colorIcon,
	settingsIcon,
	metaTextIcon,
	metaStylePro,
	metaSeparatorPro,
	metaListPro,
	metaListSmallPro,
	catPositionPro,
	OverlayStyle,
	filterTypePro,
	filterValuePro,
	navPositionPro,
	typoIcon,
} from '../../helper/CommonPanel';

import DeperciatedSettings from '../../helper/Components/DepreciatedSettings';
import ExtraToolbarSettings from '../../helper/Components/ExtraToolbarSettings';
import UltpToolbarGroup from '../../helper/Components/UltpToolbarGroup';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import { isDCActive } from '../../helper/dynamic_content';
import DCToolbar from '../../helper/dynamic_content/DCToolbar';
import { handleAdvOptions } from '../../helper/gridFunctions';
const { useEffect } = wp.element;

export const MAX_CUSTOM_META_GROUPS = 8;

export default function PG5_Settings({ store }) {
	const {
		queryType,
		titleAnimationArg,
		advFilterEnable,
		advPaginationEnable,
		V4_1_0_CompCheck: { runComp },
	} = store.attributes;
	const { context, section } = store;

	useEffect(() => {
		handleAdvOptions(context, advFilterEnable, store, advPaginationEnable);
	}, [advFilterEnable, advPaginationEnable, store.clientId]);

	return (
		<>
			<TemplateModal
				prev="https://www.wpxpo.com/postx/blocks/#demoid6833"
				store={store}
			/>
			<Sections>
				<Section slug="setting" title={__('Setting', 'ultimate-post')}>
					<GeneralContentWithQuery
						store={store}
						initialOpen={true}
						exclude={['columns', 'queryNumber', 'queryNumPosts']}
						include={[
							{
								position: 0,
								data: {
									type: isDCActive() ? 'toggle' : '',
									label: __(
										'Enable Dynamic Content',
										'ultimate-post'
									),
									key: 'dcEnabled',
									help: __(
										'Insert dynamic data & custom fields that update automatically.',
										'ultimate-post'
									),
								},
							},
							{
								position: 1,
								data: {
									type: 'advFilterEnable',
									key: 'advFilterEnable',
								},
							},
							{
								position: 2,
								data: {
									type: 'advPagiEnable',
									key: 'advPaginationEnable',
								},
							},

							{
								position: 3,
								data: { type: 'separator' },
							},
							{
								position: 4,
								data: {
									type: 'layout',
									block: 'post-grid-5',
									key: 'layout',
									label: __('Layout', 'ultimate-post'),
									options: [
										{
											img: 'assets/img/layouts/pg5/l1.png',
											label: __(
												'Layout 1',
												'ultimate-post'
											),
											value: 'layout1',
											pro: false,
										},
										{
											img: 'assets/img/layouts/pg5/l2.png',
											label: __(
												'Layout 2',
												'ultimate-post'
											),
											value: 'layout2',
											pro: true,
										},
										{
											img: 'assets/img/layouts/pg5/l3.svg',
											label: __(
												'Layout 3',
												'ultimate-post'
											),
											value: 'layout3',
											pro: true,
										},
									],
								},
							},

							{
								position: 5,
								data: { type: 'separator' },
							},

							{
								position: 6,
								data: {
									type: 'range',
									_inline: true,
									key: 'overlayHeight',
									min: 0,
									max: 800,
									step: 1,
									unit: true,
									responsive: true,
									label: __('Height', 'ultimate-post'),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'columnFlip',
									pro: true,
									label: __('Flip Layout', 'ultimate-post'),
								},
							},
							{
								data: {
									type: 'text',
									key: 'notFoundMessage',
									label: __(
										'No result found Text',
										'ultimate-post'
									),
								},
							},
						]}
					/>

					<TitleStyle
						store={store}
						depend="titleShow"
						initialOpen={section['title']}
						include={[
							{
								position: 5,
								data: {
									type: 'typography',
									key: 'titleLgTypo',
									label: __(
										'Typography Large Title',
										'ultimate-post'
									),
								},
							},
						]}
						hrIdx={[4, 10]}
					/>

					<ImageStyle
						isTab={true}
						store={store}
						initialOpen={section['image']}
						depend="showImage"
						include={[
							{
								data: {
									type: 'toggle',
									key: 'fallbackEnable',
									label: __(
										'Fallback Image Enable',
										'ultimate-post'
									),
								},
							},
							{
								data: {
									type: 'media',
									key: 'fallbackImg',
									label: __(
										'Fallback Image',
										'ultimate-post'
									),
								},
							},
						]}
						exclude={[
							'imgMargin',
							'imgWidth',
							'imageScale',
							'imgHeight',
						]}
						hrIdx={[
							{
								tab: 'settings',
								hr: [3, 11],
							},
							{
								tab: 'style',
								hr: [4],
							},
						]}
					/>

					<MetaStyle
						pro={true}
						isTab={true}
						store={store}
						initialOpen={section['meta']}
						depend="metaShow"
						exclude={[
							'metaSeparator',
							'metaStyle',
							'metaList',
							'metaListSmall',
						]}
						include={[
							{
								position: 0,
								data: {
									tab: 'settings',
									type: 'toggle',
									key: 'showSmallMeta',
									label: __(
										'Meta Small Item',
										'ultimate-post'
									),
								},
							},
							{ position: 1, data: metaStylePro },
							{ position: 3, data: metaSeparatorPro },
							{ position: 4, data: metaListPro },
							{ position: 7, data: metaListSmallPro },
						]}
						hrIdx={[
							{
								tab: 'style',
								hr: [7],
							},
						]}
					/>

					<CategoryStyle
						pro={true}
						isTab={true}
						initialOpen={section['taxonomy-/-category']}
						depend="catShow"
						store={store}
						exclude={['catPosition']}
						include={[
							{
								position: 0,
								data: {
									tab: 'settings',
									type: 'toggle',
									key: 'showSmallCat',
									label: __(
										'Small Item Category',
										'ultimate-post'
									),
								},
							},
							{ position: 1, data: catPositionPro },
						]}
						hrIdx={[
							{
								tab: 'style',
								hr: [8],
							},
						]}
					/>

					<ExcerptStyle
						depend="excerptShow"
						initialOpen={section['excerpt']}
						store={store}
						include={[
							{
								position: 0,
								data: {
									tab: 'settings',
									type: 'toggle',
									key: 'showSmallExcerpt',
									label: __(
										'Small Item Excerpt',
										'ultimate-post'
									),
								},
							},
						]}
						hrIdx={[4, 7]}
					/>

					<ReadMoreStyle
						isTab={true}
						store={store}
						initialOpen={section['read-more']}
						depend="readMore"
						include={[
							{
								position: 0,
								data: {
									tab: 'settings',
									type: 'toggle',
									key: 'showSmallBtn',
									label: __(
										'Small Item Read More',
										'ultimate-post'
									),
								},
							},
						]}
						hrIdx={[
							{
								tab: 'style',
								hr: [3],
							},
						]}
					/>

					<VideoStyle
						initialOpen={section['video']}
						depend="vidIconEnable"
						store={store}
						hrIdx={[5]}
					/>

					<OverlayStyle store={store} include={titleAnimationArg} />

					{!runComp && (
						<DeperciatedSettings
							open={
								section['filter'] ||
								section['pagination'] ||
								section['heading']
							}
						>
							<HeadingContent
								isTab={true}
								store={store}
								initialOpen={section['heading']}
								depend="headingShow"
								// hrIdx={[
								//     {
								//         tab: "settings",
								//         hr: [
								//             {
								//                 idx: 1,
								//                 label: __(
								//                     "Heading Settings",
								//                     "ultimate-posts"
								//                 ),
								//             },
								//             {
								//                 idx: 8,
								//                 label: __(
								//                     "Subheading Settings",
								//                     "ultimate-posts"
								//                 ),
								//             },
								//         ],
								//     },
								//     {
								//         tab: "style",
								//         hr: [
								//             {
								//                 idx: 1,
								//                 label: __(
								//                     "Heading Style",
								//                     "ultimate-posts"
								//                 ),
								//             },
								//             {
								//                 idx: 12,
								//                 label: __(
								//                     "Subheading Style",
								//                     "ultimate-posts"
								//                 ),
								//             },
								//         ],
								//     },
								// ]}
							/>

							{queryType != 'posts' &&
								queryType != 'customPosts' && (
									<FilterContent
										pro={true}
										isTab={true}
										store={store}
										initialOpen={section['filter']}
										depend="filterShow"
										exclude={['filterType', 'filterValue']}
										include={[
											{
												position: 1,
												data: filterTypePro,
											},
											{
												position: 2,
												data: filterValuePro,
											},
										]}
										hrIdx={[
											{
												tab: 'settings',
												hr: [5],
											},
											{
												tab: 'style',
												hr: [4, 9],
											},
										]}
									/>
								)}

							<PaginationContent
								pro={true}
								isTab={true}
								store={store}
								initialOpen={section['pagination']}
								depend="paginationShow"
								include={[
									{
										position: 1,
										data: {
											tab: 'settings',
											type: 'select',
											key: 'paginationType',
											label: __(
												'Pagination Type',
												'ultimate-post'
											),
											pro: true,
											options: [
												{
													value: 'none',
													label: __(
														'None',
														'ultimate-post'
													),
												},
												{
													value: 'navigation',
													label: __(
														'Navigation',
														'ultimate-post'
													),
												},
											],
										},
									},
									{ position: 2, data: navPositionPro },
								]}
								exclude={[
									'paginationType',
									'paginationAjax',
									'loadMoreText',
									'paginationText',
									'pagiMargin',
									'navPosition',
								]}
								hrIdx={[
									{
										tab: 'style',
										hr: [4],
									},
								]}
							/>
						</DeperciatedSettings>
					)}
				</Section>
				<Section
					slug="advanced"
					title={__('Advanced', 'ultimate-post')}
				>
					<GeneralAdvanced
						initialOpen={true}
						store={store}
						include={[
							{
								position: 2,
								data: {
									type: 'color',
									key: 'loadingColor',
									label: __('Loading Color', 'ultimate-post'),
									pro: true,
								},
							},
						]}
					/>
					<ResponsiveAdvanced store={store} />
					<CustomCssAdvanced store={store} />
				</Section>
			</Sections>
			{blockSupportLink()}
		</>
	);
}

export function AddSettingsToToolbar({ selected, store }) {
	const {
		V4_1_0_CompCheck: { runComp },
	} = store.attributes;

	const {
		metaShow,
		showImage,
		titleShow,
		catPosition,
		titlePosition,
		excerptShow,
		readMore,
		metaPosition,
		layout,
		fallbackEnable,
		catShow,
	} = store.attributes;

	const layoutContext = [
		metaShow && metaPosition == 'bottom',
		readMore,
		excerptShow,
		titleShow && titlePosition == false,
		metaShow && metaPosition == 'top',
		titleShow && titlePosition == true,
		catShow && catPosition == 'aboveTitle',
	];

	if (isDCActive() && selected.startsWith('dc_')) {
		return (
			<DCToolbar
				store={store}
				selected={selected}
				layoutContext={layoutContext}
			/>
		);
	}

	switch (selected) {
		case 'filter':
			if (runComp) return null;
			return (
				<UltpToolbarGroup text={'Filter'} pro>
					<TypographyTB
						store={store}
						attrKey="fliterTypo"
						label={__('Filter Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getFilterToolbarSettings({
							include: ['fliterSpacing', 'fliterPadding'],
							exclude: '__all',
							title: __('Filter Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Filter Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Filter Settings', 'ultimate-post')}
						styleTitle={__('Filter Style', 'ultimate-post')}
						settingsKeys={filterSettings}
						styleKeys={filterStyle}
						oArgs={FilterContentArg}
						exStyle={[
							'filterTypo',
							'fliterSpacing',
							'fliterPadding',
						]}
						exSettings={['filterValue', 'filterType']}
						incSettings={[
							{ position: 1, data: filterTypePro },
							{ position: 2, data: filterValuePro },
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'heading':
			if (runComp) return null;
			return (
				<UltpToolbarGroup text={'Heading'}>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Heading Settings', 'ultimate-post')}
						styleTitle={__('Heading Style', 'ultimate-post')}
						settingsKeys={headingSettings}
						styleKeys={headingStyle}
						oArgs={HeadingContentArg}
					/>
				</UltpToolbarGroup>
			);
		case 'pagination':
			if (runComp) return null;
			return (
				<UltpToolbarGroup text={'Pagination'} pro>
					<TypographyTB
						store={store}
						attrKey="pagiTypo"
						label={__('Pagination Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getPagiToolbarSettings({
							include: ['pagiPadding', 'navMargin'],
							exclude: '__all',
							title: __('Pagination Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Pagination Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__(
							'Pagination Settings',
							'ultimate-post'
						)}
						styleTitle={__('Pagination Style', 'ultimate-post')}
						settingsKeys={pagiSettings}
						styleKeys={pagiStyles}
						oArgs={PaginationContentArg}
						exStyle={[
							'pagiTypo',
							'pagiMargin',
							'pagiPadding',
							'navMargin',
						]}
						exSettings={[
							'paginationType',
							'paginationAjax',
							'loadMoreText',
							'paginationText',
							'navPosition',
						]}
						incSettings={[
							{
								position: 1,
								data: {
									type: 'select',
									key: 'paginationType',
									label: __(
										'Pagination Type',
										'ultimate-post'
									),
									pro: true,
									options: [
										{
											value: 'none',
											label: __('None', 'ultimate-post'),
										},
										{
											value: 'navigation',
											label: __(
												'Navigation',
												'ultimate-post'
											),
										},
									],
								},
							},
							{ position: 2, data: navPositionPro },
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'video':
			return (
				<UltpToolbarGroup text={'Video'}>
					<ExtraToolbarSettings
						buttonContent={colorIcon}
						include={getVideoToolbarSettings({
							include: [
								'popupIconColor',
								'popupHovColor',
								'popupTitleColor',
								'closeIconColor',
								'closeHovColor',
							],
							exclude: '__all',
							title: __('Video Icon Color', 'ultimate-post'),
						})}
						store={store}
						label={__('Video Icon Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={settingsIcon}
						include={getVideoToolbarSettings({
							exclude: [
								'popupIconColor',
								'popupHovColor',
								'popupTitleColor',
								'closeIconColor',
								'closeHovColor',
							],
						})}
						store={store}
						label={__('Video Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'title':
			return (
				<UltpToolbarGroup text={'Title'}>
					<ExtraToolbarSettings
						buttonContent={typoIcon}
						include={getTitleToolbarSettings({
							include: [
								'titleTypo',
								{
									data: {
										type: 'typography',
										key: 'titleLgTypo',
										label: __(
											'Typography Large Title',
											'ultimate-post'
										),
									},
								},
							],
							exclude: '__all',
							title: __('Title Typography', 'ultimate-post'),
						})}
						store={store}
						label={__('Title Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={colorIcon}
						include={getTitleToolbarSettings({
							include: [
								'titleColor',
								'titleBackground',
								'titleHoverColor',
								'titleAnimColor',
							],
							exclude: '__all',
							title: __('Title Color', 'ultimate-post'),
						})}
						store={store}
						label={__('Title Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={settingsIcon}
						include={getTitleToolbarSettings({
							exclude: [
								'titleBackground',
								'titleTypo',
								'titleColor',
								'titleHoverColor',
							],
							title: __('Title Settings', 'ultimate-post'),
						})}
						store={store}
						label={__('Title Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'excerpt':
			return (
				<UltpToolbarGroup text={'Excerpt'}>
					<TypographyTB
						store={store}
						attrKey="excerptTypo"
						label={__('Excerpt Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={colorIcon}
						include={getExcerptToolbarSettings({
							include: ['excerptColor'],
							exclude: '__all',
							title: __('Excerpt Color', 'ultimate-post'),
						})}
						store={store}
						label={__('Excerpt Color', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={settingsIcon}
						include={getExcerptToolbarSettings({
							exclude: ['excerptTypo', 'excerptColor'],
							title: __('Excerpt Settings', 'ultimate-post'),
							include: [
								{
									position: 0,
									data: {
										type: 'toggle',
										key: 'showSmallExcerpt',
										label: __(
											'Small Item Excerpt',
											'ultimate-post'
										),
									},
								},
							],
						})}
						store={store}
						label={__('Excerpt Settings', 'ultimate-post')}
					/>
				</UltpToolbarGroup>
			);
		case 'meta':
			return (
				<UltpToolbarGroup text={'Meta'} pro>
					<TypographyTB
						store={store}
						attrKey="metaTypo"
						label={__('Meta Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getMetaToolbarSettings({
							include: [
								'metaSpacing',
								'metaMargin',
								'metaPadding',
							],
							exclude: '__all',
							title: __('Meta Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Meta Spacing', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={metaTextIcon}
						include={getMetaToolbarSettings({
							include: metaText,
							exclude: '__all',
							title: __('Meta Texts', 'ultimate-post'),
						})}
						store={store}
						label={__('Meta Texts', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Meta Settings', 'ultimate-post')}
						styleTitle={__('Meta Style', 'ultimate-post')}
						settingsKeys={metaSettings}
						styleKeys={metaStyle}
						oArgs={MetaStyleArg}
						exSettings={[
							'metaSeparator',
							'metaStyle',
							'metaList',
							'metaListSmall',
						]}
						exStyle={[
							'metaTypo',
							'metaMargin',
							'metaPadding',
							'metaSpacing',
							'metaSeparator',
							'metaStyle',
							'metaList',
						]}
						incSettings={[
							{
								position: 0,
								data: {
									tab: 'settings',
									type: 'toggle',
									key: 'showSmallMeta',
									label: __(
										'Meta Small Item',
										'ultimate-post'
									),
								},
							},
							{ position: 1, data: metaStylePro },
							{ position: 3, data: metaSeparatorPro },
							{ position: 4, data: metaListPro },
							{ position: 7, data: metaListSmallPro },
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'read-more':
			return (
				<UltpToolbarGroup text={'Read More'}>
					<TypographyTB
						store={store}
						attrKey="readMoreTypo"
						label={__('Read More Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getReadMoreToolbarSettings({
							include: [
								'readMoreSacing', // 💩 yes, it's a typo
								'readMorePadding',
							],
							exclude: '__all',
							title: __('Read More Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Read More Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__(
							'Read More Settings',
							'ultimate-post'
						)}
						styleTitle={__('Read More Style', 'ultimate-post')}
						settingsKeys={readMoreSettings}
						styleKeys={readMoreStyle}
						oArgs={ReadMoreStyleArg}
						exStyle={[
							'readMoreTypo',
							'readMoreSacing',
							'readMorePadding',
						]}
						incSettings={[
							{
								position: 0,
								data: {
									type: 'toggle',
									key: 'showSmallBtn',
									label: __(
										'Small Item Read More',
										'ultimate-post'
									),
								},
							},
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'cat':
			return (
				<UltpToolbarGroup text={'Taxonomy/Category'} pro>
					<TypographyTB
						store={store}
						attrKey="catTypo"
						label={__('Category Typography', 'ultimate-post')}
					/>
					<ExtraToolbarSettings
						buttonContent={spacingIcon}
						include={getCategoryToolbarSettings({
							include: ['catSacing', 'catPadding'],
							exclude: '__all',
							title: __('Category Spacing', 'ultimate-post'),
						})}
						store={store}
						label={__('Category Spacing', 'ultimate-post')}
					/>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Category Settings', 'ultimate-post')}
						styleTitle={__('Category Style', 'ultimate-post')}
						settingsKeys={categorySettings}
						styleKeys={categoryStyle}
						oArgs={CategoryStyleArg}
						exStyle={['catTypo', 'catSacing', 'catPadding']}
						exSettings={['catPosition']}
						incSettings={[
							{
								position: 0,
								data: {
									type: 'toggle',
									key: 'showSmallCat',
									label: __(
										'Small Item Category',
										'ultimate-post'
									),
								},
							},
							{ position: 1, data: catPositionPro },
						]}
					/>
				</UltpToolbarGroup>
			);
		case 'image':
			return (
				<UltpToolbarGroup text={'Image'}>
					<ToolbarSettingsAndStyles
						store={store}
						settingsTitle={__('Image Settings', 'ultimate-post')}
						styleTitle={__('Image Style', 'ultimate-post')}
						settingsKeys={imageSettingsKeys}
						styleKeys={imageStyleKeys}
						oArgs={ImageStyleArg}
						exSettings={[
							'imgMargin',
							'imgWidth',
							'imageScale',
							'imgHeight',
						]}
						exStyle={[
							'imgMargin',
							'imgWidth',
							'imageScale',
							'imgHeight',
						]}
						incSettings={[
							{
								data: {
									type: 'toggle',
									key: 'fallbackEnable',
									label: __(
										'Fallback Image Enable',
										'ultimate-post'
									),
								},
							},
							{
								data: {
									type: 'media',
									key: 'fallbackImg',
									label: __(
										'Fallback Image',
										'ultimate-post'
									),
								},
							},
						]}
					/>
				</UltpToolbarGroup>
			);
		default:
			return null;
	}
}
