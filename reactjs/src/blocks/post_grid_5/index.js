const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-grid-5/', 'block_docs');

registerBlockType(
    'ultimate-post/post-grid-5', {
        title: __('Post Grid #5','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-grid-5.svg'}/>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('Post Grid with big posts on left in tiles overlay style.','ultimate-post')}
        //     <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [
            __('Post Grid','ultimate-post'),
            __('Grid View','ultimate-post'),
            __('Article','ultimate-post'),
            __('Post Listing','ultimate-post'),
            __('Grid','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/postgrid5.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)