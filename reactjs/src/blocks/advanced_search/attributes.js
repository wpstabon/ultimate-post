const attributes = {
    blockId: { 
        type: 'string', 
        default: '' 
    },
    blockPubDate: { type: "string", default: "empty" },
    previewImg: {
        type: 'string', 
        default: '' 
    },
    currentPostId: { type: "string" , default: "" },
    
    /* =================================  
        General Content Setting
    ================================= */  
    searchAjaxEnable:{
        type: 'boolean', 
        default: true 
    },
    searchFormStyle:{
        type: 'string', 
        default: 'input1',
        style: [
            {
                depends: [
                    { key:'searchBtnReverse', condition:'==', value: false },
                ], 
                selector: '{{ULTP}} .ultp-searchres-input { padding-left: 16px; }'
            },
            {
                depends: [
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
            },
        ]
    },
    searchnoresult:{
        type: 'string', 
        default: 'No Results Found',
        style: [
            {
                depends: [
                    { key:'searchAjaxEnable', condition:'==', value: true },
                ], 
            },
        ]
    },
    searchPopup:{
        type: 'boolean', 
        default: false, 
    },
    searchPopupIconStyle:{
        type: 'string', 
        default: 'popup-icon1',
        style: [
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchPopupIconStyle', condition:'==', value: 'popup-icon1' }
                ], 
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchPopupIconStyle', condition:'==', value: 'popup-icon2' }
                ], 
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchPopupIconStyle', condition:'==', value: 'popup-icon3' }
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon { flex-direction: row-reverse }' 
            },
        ]
    },  
    searchPostType: {
        type: 'string',
        default: '',
    },
    
    searchIconAlignment: {
        type: 'string',
        default: 'left',
        style: [
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchIconAlignment', condition:'==', value: 'center' }
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon { margin: 0 auto; }' 
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchIconAlignment', condition:'==', value: 'left' }
                ], 
                selector:
                `{{ULTP}} .ultp-searchpopup-icon,  
                {{ULTP}} .ultp-search-animation-popup .ultp-search-canvas { margin-right: auto; }` 
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'searchIconAlignment', condition:'==', value: 'right' }
                ], 
                selector:
                `{{ULTP}} .ultp-searchpopup-icon, 
                {{ULTP}} .ultp-search-animation-popup .ultp-search-canvas { margin-left: auto; }` 
            },
        ]
    },
    
    /* =================================  
        Popup Icon Style
    ================================= */  
    popupIconGap:{
        type: 'object',
        default: {lg:'17', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchPopupIconStyle', condition:'!=', value: 'popup-icon1' },
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon { gap:{{popupIconGap}}; }' 
            },
        ]
    },
    popupIconSize:{
        type: 'object',
        default: {lg:'17', unit: 'px'},
        style: [
            {
                selector:'{{ULTP}} .ultp-searchpopup-icon svg { height:{{popupIconSize}}; width:{{popupIconSize}}; }' 
            },
        ]
    },
    popupIconTextTypo:{
        type: 'object',
        default: {openTypography: 0,size: {lg:16, unit:'px'},height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [
            {   
                depends: [
                    { key:'searchPopupIconStyle', condition:'!=', value: 'popup-icon1' },
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon .ultp-search-button__text'
            }
        ]
    },
    popupIconColor:{
        type: 'string',
        default: 'var(--postx_preset_Base_1_color)',
        style: [
            {
                selector:'{{ULTP}} .ultp-searchpopup-icon svg { fill:{{popupIconColor}}; }' 
            },
        ],
    },
    popupIconTextColor:{
        type: 'string',
        default: 'var(--postx_preset_Base_1_color)',
        style: [
            { 
                depends: [
                    { key:'searchPopupIconStyle', condition:'!=', value: 'popup-icon1' },
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon .ultp-search-button__text { color: {{popupIconTextColor}}; }'
            },
        ],
    },
    popupIconBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: 'var(--postx_preset_Contrast_2_color)', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                selector:'{{ULTP}} .ultp-searchpopup-icon'
            }
        ],
    },
    
    // Hover 
    popupIconHoverColor:{
        type: 'string',
        default: '',
        style: [
            { 
                selector:'{{ULTP}} .ultp-searchpopup-icon:hover svg { fill: {{popupIconHoverColor}}; }' 
            },
        ],
    },
    popupIconTextHoverColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'searchPopupIconStyle', condition:'!=', value: 'popup-icon1' },
                ], 
                selector:'{{ULTP}} .ultp-searchpopup-icon:hover .ultp-search-button__text { color: {{popupIconTextHoverColor}}; }' 
            },
        ],
    },
    popupIconHoverBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                selector:'{{ULTP}} .ultp-searchpopup-icon:hover' 
            }
        ],
    },
    // hover end
    popupIconPadding:{
        type: 'object',
        default: {lg:{top: '', bottom: '', left: '', right: '', unit:'px'}},
        style:[
            { 
                selector:'{{ULTP}} .ultp-searchpopup-icon { padding: {{popupIconPadding}}; }'
            }
        ],
    },
    popupIconBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#000' },
        style: [
            {
                selector:'{{ULTP}}  .ultp-searchpopup-icon'
            },
        ],
    },
    IconHoverBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#000' },
        style: [
            {
                selector:'{{ULTP}}  .ultp-searchpopup-icon:hover'
            },
        ],
    },
    popupIconRadius:{
        type: 'object',
        default: {lg:{top: '', bottom: '', left: '', right: '', unit:'px'}},
        style: [
            { 
                selector:'{{ULTP}} .ultp-searchpopup-icon { border-radius: {{popupIconRadius}}; }'
            }
        ]
    },
    
    /* =================================  
        Search Button Style
    ================================= */  
    searchBtnEnable:{
        type: 'boolean', 
        default: false,
    },
    btnNewTab:{
        type: 'boolean', 
        default: false,
    },
    enableSearchPage:{
        type: 'boolean', 
        default: true, 
        style: [
            {
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
            },
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true }
                ], 
            },
        ]
    },
    searchBtnText:{
        type: 'boolean', 
        default: true, 
    },
    searchBtnIcon:{
        type: 'boolean', 
        default: true,
    },
    searchIconAfterText:{
        type: 'boolean', 
        default: false,
        style: [
            {
                depends: [
                    { key:'searchIconAfterText', condition:'==', value: false },
                    { key:'searchBtnIcon', condition:'==', value: true },
                    { key:'searchBtnText', condition:'==', value: true }
                ], 
            },
            {
                depends: [
                    { key:'searchIconAfterText', condition:'==', value: true },
                    { key:'searchBtnIcon', condition:'==', value: true },
                    { key:'searchBtnText', condition:'==', value: true }
                ], 
                selector:'{{ULTP}} .ultp-search-button { flex-direction: row-reverse }' 
            },
        ]
    },
    searchButtonText:{
        type: 'text', 
        default: 'Search',
    },
    searchButtonPosition:{
        type: 'object',
        default: {lg:'7', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input1' },
                ], 
                selector:'{{ULTP}} .ultp-searchform-content.ultp-searchform-input1 { gap:{{searchButtonPosition}}; }'
            },
            {
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input2' },
                    { key:'searchBtnReverse', condition:'==', value: false },
                ], 
                selector:'{{ULTP}} .ultp-search-button { right: {{searchButtonPosition}}; left: auto; }' 
            },
            {
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input2' },
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button { left: {{searchButtonPosition}}; right: auto;}'
            },
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input1' },
                ], 
                selector:'{{ULTP}} .ultp-searchform-content.ultp-searchform-input1 { gap:{{searchButtonPosition}}; }'
            },
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input2' },
                    { key:'searchBtnReverse', condition:'==', value: false },
                ], 
                selector:'{{ULTP}} .ultp-search-button { right: {{searchButtonPosition}}; left: auto; }' 
            },
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                    { key:'searchFormStyle', condition:'==', value: 'input2' },
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button { left: {{searchButtonPosition}}; right: auto;}'
            },
        ]
    },
    searchTextGap:{
        type: 'object',
        default: {lg:'8', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button { gap: {{searchTextGap}}; }' 
            },
        ]
    },
    searchBtnIconSize:{
        type: 'object',
        default: {lg:'17', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button svg { height:{{searchBtnIconSize}}; width:{{searchBtnIconSize}}; }' 
            },
        ]
    },
    searchBtnTextTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg: 14, unit:'px'},height: {lg:22, unit:'px'}, decoration: 'none',family: '', weight: 400},
        style: [
            {   
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button .ultp-search-button__text'
            }
        ]
    },
    searchBtnIconColor:{
        type: 'string',
        default: '#fff',
        style: [
            { 
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button svg { fill:{{searchBtnIconColor}}; }'
            },
        ],
    },
    searchBtnTextColor:{
        type: 'string',
        default: '#fff',
        style: [
            {
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button .ultp-search-button__text { color: {{searchBtnTextColor}}; }'
            },
        ],
    },
    searchBtnBg:{
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#212121', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button'
            },
            { 
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button'
            }
        ],
    },
    btnBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 0, right: 0, bottom: 0, left: 0}, color: '#000' },
        style:[{ 
            selector:'{{ULTP}} .ultp-search-button'
        }],
    },
    // Hover
    searchBtnIconHoverColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-button:hover svg { fill:{{searchBtnIconHoverColor}}; }'
            },
        ],
    },
    searchBtnTextHoverColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button:hover .ultp-search-button__text { color: {{searchBtnTextHoverColor}}; }'
            },
        ],
    },
    searchBtnHoverBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button:hover'
            },
            { 
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button:hover'
            }
        ],
    },
    btnHoverBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 0, right: 0, bottom: 0, left: 0}, color: '#000' },
        style:[{ 
                selector:'{{ULTP}} .ultp-search-button:hover'
            }
        ],
    },
    searchBtnPadding:{
        type: 'object',
        default: {lg:{top: '13', bottom: '13', left: '13', right: '13', unit:'px'}},
        style:[
            { 
                depends: [
                    { key:'searchBtnText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button { padding: {{searchBtnPadding}}; }'
            },
            { 
                depends: [
                    { key:'searchBtnIcon', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-button { padding: {{searchBtnPadding}}; }'
            },
        ],
    },
    searchBtnRadius:{
        type: 'object',
        default: {lg:{top: '5', bottom: '5', left: '5', right: '5', unit:'px'}},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-button { border-radius: {{searchBtnRadius}}; }'
            }
        ],
    },
    searchClear:{
        type: 'object',
        default: {lg:'', unit: 'px'},
        style: [
            {
                selector:'{{ULTP}} .ultp-search-clear { right: {{searchClear}} !important; }'
            },
        ]
    },
    
    /* =================================  
        Popup Canvas
    ================================= */  
    popupAnimation:{
        type: 'string', 
        default: 'popup',
    },
    popupCloseIconSeparator:{
        type: 'string', 
        default: 'Close Icon Style',
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'!=', value: 'popup' },
                ], 
            }
        ]
    },
    popupCloseSize:{
        type: 'object',
        default: {lg:'20', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'!=', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-popupclose-icon svg { height:{{popupCloseSize}}; width:{{popupCloseSize}}; }'
            }
        ]
    },
    popupCloseColor:{
        type: 'string',
        default: '#000',
        style: [
            { 
                depends: [
                    { key:'popupAnimation', condition:'!=', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-popupclose-icon svg { fill:{{popupCloseColor}}; }'
            },
        ],
    },
    popupCloseHoverColor:{
        type: 'string',
        default: '#7777',
        style: [
            { 
                depends: [
                    { key:'popupAnimation', condition:'!=', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-popupclose-icon:hover svg { fill:{{popupCloseHoverColor}}; }'
            },
        ],
    },
    windowpopupHeading:{
        type: 'boolean', 
        default: true, 
    },
    windowpopupText:{
        type: 'string',
        default: 'Search The Query',
    },
    windowpopupHeadingAlignment:{
        type: 'string',
        default: 'center',
        style: [
            {   
                depends: [
                    { key:'windowpopupHeading', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-popup-heading { text-align:{{windowpopupHeadingAlignment}}; }'
            }
        ]
    },
    windowpopupHeadingTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg: 16, unit:'px'},height: {lg:24, unit:'px'}, decoration: 'none',family: '', weight: 600},
        style: [
            {   
                depends: [
                    { key:'windowpopupHeading', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-popup-heading'
            }
        ]
    },
    windowpopupHeadingColor:{
        type: 'string',
        default: '#000',
        style: [
            { 
                depends: [
                    { key:'windowpopupHeading', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-popup-heading { color:{{windowpopupHeadingColor}}; }'
            },
        ],
    },
    popupHeadingSpacing:{
        type: 'object',
        default: {lg:'12'},
        style: [
            {
                depends: [
                    { key:'windowpopupHeading', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-popup-heading { margin-bottom: {{popupHeadingSpacing}}px;}'
            },
        ]
    },
    searchPopupPosition:{
        type: 'string',
        default: 'right',
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                ], 
            },
        ]
    },
    popupBG:{
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#FCFCFC'},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-canvas'
            }
        ],
    },
    canvasWidth:{
        type: 'object',
        default: {lg:'600', xs:'100', ulg: 'px', unit: '%'},
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-search-canvas { width: {{canvasWidth}};}'
            },
            {
                depends: [
                    { key:'popupAnimation', condition:'!=', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-search-canvas > div { max-width: {{canvasWidth}} !important; width: 100% !important; }'
            },
        ]
    },
    popupPadding:{
        type: 'object',
        default: {lg:{top: '40', bottom: '40', left: '40', right: '40', unit:'px'}},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-canvas { padding: {{popupPadding}}; }'
            }
        ],
    },
    popupRadius:{
        type: 'object',
        default: {lg:{top: '', bottom: '', left: '', right: '', unit:'px'}},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-canvas { border-radius: {{popupRadius}}; }'
            }
        ],
    },
    popupShadow:{
        type: "object",
        default: {
            openShadow: 1,
            width: { top: 0, right: 3, bottom: 6, left: 0},
            color: "rgba(0, 0, 0, 0.16)",
        },
        style: [{ 
            depends: [
                { key:'searchPopup', condition:'==', value: true },
            ], 
            selector:'{{ULTP}} .ultp-search-canvas'
        }]
    },
    popupPositionX:{
        type: 'object',
        default: {lg:'', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                    { key:'searchPopupPosition', condition:'==', value: 'left' },
                ], 
                selector:
                `{{ULTP}} .ultp-search-popup .ultp-search-canvas {right:{{popupPositionX}}; left: auto; } 
                body > {{ULTP}} { transform: translateX(-{{popupPositionX}}) }`
            },
            {
                depends: [
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                    { key:'searchPopupPosition', condition:'==', value: 'right' },
                ], 
                selector:
                `{{ULTP}} .ultp-search-popup .ultp-search-canvas {  left:{{popupPositionX}}; right: auto; } 
                body > {{ULTP}}   { transform: translateX({{popupPositionX}}) }`
            },
        ]
    },
    popupPositionY:{
        type: 'object',
        default: {lg:'10', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                ],    
                selector: 
                `{{ULTP}} .ultp-search-popup .ultp-search-canvas { top:{{popupPositionY}}; } 
                body > {{ULTP}}.result-data.ultp-search-animation-popup { translate: 0px {{popupPositionY}} }`
            },
        ]
    },
    
    /* =================================  
            Search Form Style
    ================================= */  
    searchBtnReverse:{
        type: 'boolean', 
        default: false, 
        style: [
            
            {
                depends: [
                    { key:'searchFormStyle', condition:'==', value: 'input1' },
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
                selector: 
                `{{ULTP}} .ultp-searchform-content.ultp-searchform-input1 { flex-direction: row-reverse; } 
                {{ULTP}} .ultp-searchres-input {  padding-left: 16px; padding-right: 40px; }`
            },
            {
                depends: [
                    { key:'searchFormStyle', condition:'==', value: 'input2' },
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-button { right: auto; } 
                {{ULTP}} .ultp-search-clear { right: 16px;  } 
                {{ULTP}} .ultp-searchres-input {  padding-left: 120px;  padding-right: 40px;}`
            },
            {
                depends: [
                    { key:'searchFormStyle', condition:'==', value: 'input3' },
                    { key:'searchBtnReverse', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-button {  right: auto; left: 0px;} 
                {{ULTP}} .ultp-search-clear { right: 16px } 
                {{ULTP}} .ultp-searchres-input {  padding-left: 120px; padding-right: 40px;}`
            },
            {
                depends: [
                    { key:'searchBtnReverse', condition:'==', value: false },
                ], 
            },
        ]
    },
    searchInputPlaceholder: {
        type: 'string', 
        default: 'Search...', 
    },
    inputTypo:{
        type: 'object',
        default: {openTypography: 0,size: {lg:16, unit:'px'},height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [
            {   
                selector:'{{ULTP}} .ultp-search-inputwrap input.ultp-searchres-input'
            }
        ]
    },
    searchFormWidth:{
        type: 'object',
        default: {lg:'', sm:'100',unit: '%'},
        style: [
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-searchform-content, 
                {{ULTP}} .ultp-search-result { width: {{searchFormWidth}} !important; }`
            },
        ]
    },  
    inputHeight:{
        type: 'object',
        default: {lg:'', unit: 'px'},
        style: [
            {
                selector: '{{ULTP}}  input.ultp-searchres-input { height: {{inputHeight}}; }'
            },
        ]
    },  
    inputColor:{
        type: 'string',
        default: '#000',
        style: [
            {   
                selector: '{{ULTP}} .ultp-search-inputwrap input.ultp-searchres-input { color: {{inputColor}}; }'
            }
        ],
    },
    inputBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                selector: '{{ULTP}} input.ultp-searchres-input'
            }
        ],
    },
    inputFocusBorder:{
        type: 'object',
        default: { openBorder: 1, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#000' },
        style: [
            {
                selector: '{{ULTP}} .ultp-search-inputwrap input.ultp-searchres-input:focus'
            },
        ],
    },
    inputBorder:{
        type: 'object',
        default: { openBorder: 1, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#989898' },
        style: [
            { 
                selector: '{{ULTP}} .ultp-search-inputwrap input.ultp-searchres-input'
            }
        ]
    },
    inputPadding:{
        type: 'object',
        default: {lg:{}},
        style:[
            { 
                selector: '{{ULTP}} .ultp-search-inputwrap input.ultp-searchres-input { padding:{{inputPadding}}; }'
            }
        ],
    },
    inputRadius:{
        type: 'object',
        default: {lg:{top: '5', bottom: '5', left: '5', right: '5', unit:'px'}},
        style: [
            { 
                selector: '{{ULTP}} input.ultp-searchres-input { border-radius:{{inputRadius}}; }'
            }
        ]
    },
    
    /* =================================  
            Search Result Settings
    ================================= */  
    resColumn:{
        type: 'object',
        default: {lg:'1'},
        style: [
            {
                selector: '{{ULTP}} .ultp-result-data { display: grid; grid-template-columns: repeat( {{resColumn}} , auto) } ' 
            },
        ]
    },
    resultGap:{
        type: 'object',
        default: {lg:'10'},
        style: [
            {
                depends: [
                    { key:'resultSeparatorEnable', condition:'!=', value: true },
                    { key:'moreResultsbtn', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-result-data { gap:{{resultGap}}px; } ' 
            },
            {
                depends: [
                    { key:'resMoreResultDevider', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-result-data { gap:{{resultGap}}px; } ' 
            },
            {
                depends: [
                    { key:'moreResultsbtn', condition:'==', value: false },
                    { key:'resultSeparatorEnable', condition:'==', value: false },
                ],
                selector:'{{ULTP}} .ultp-result-data { gap:{{resultGap}}px; } ' 
            },
        ]
    },
    resExcerptEnable:{
        type: 'boolean', 
        default: true, 
    },
    resCatEnable:{
        type: 'boolean', 
        default: true, 
    },
    resAuthEnable:{
        type: 'boolean', 
        default: true, 
    },
    resDateEnable:{
        type: 'boolean', 
        default: true, 
    },
    resImageEnable:{
        type: 'boolean', 
        default: true, 
    },
    resImageSize:{
        type: 'object',
        default: {lg:'70', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'resImageEnable', condition:'==', value: true },
                ],  
                selector: '{{ULTP}}  img.ultp-searchresult-image { height:{{resImageSize}}; width:{{resImageSize}}; } ' 
            },
        ]
    },
    resImageRadius:{
        type: 'object',
        default: {lg:{top: '0', bottom: '0', left: '0', right: '0', unit:'px'}},
        style:[
            { 
                depends: [
                    { key:'resImageEnable', condition:'==', value: true },
                ],  
                selector:'{{ULTP}} img.ultp-searchresult-image { border-radius: {{resImageRadius}}; }' 
            }
        ],
    },
    resImageSpace:{
        type: 'object',
        default: {lg:'20', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'resImageEnable', condition:'==', value: true },
                ],  
                selector:'{{ULTP}} .ultp-search-result__item { column-gap:{{resImageSpace}}; } ' 
            },
        ]
    },
    resTitleColor:{
        type: 'string',
        default: '#101010',
        style: [
            {
                selector:'{{ULTP}} .ultp-search-result .ultp-searchresult-title { color:{{resTitleColor}}; }' 
            },
        ],
    },
    resExcerptColor:{
        type: 'string',
        default: '#000',
        style: [
            {
                depends: [
                    { key:'resExcerptEnable', condition:'==', value: true },
                ],  
                selector:'{{ULTP}} .ultp-searchresult-excerpt { color:{{resExcerptColor}}; }' 
            },
        ],        
    },
    resMetaColor: {
        type: 'string',
        default: '#000',
        style: [
            {
                selector:
                `{{ULTP}}  .ultp-searchresult-author, 
                {{ULTP}}  .ultp-searchresult-publishdate, 
                {{ULTP}} .ultp-searchresult-category a { color:{{resMetaColor}}; }` 
            },
        ],        
    },
    // Hover
    resTitleHoverColor:{
        type: 'string',
        default: '#037fff',
        style: [
            {
                selector:'{{ULTP}} .ultp-search-result .ultp-searchresult-title:hover { color:{{resTitleHoverColor}}; }'
            },
        ],
    },
    resMetaHoverColor:{
        type: 'string',
        default: '#037fff',
        style: [
            {
                selector:
                `{{ULTP}} .ultp-searchresult-author:hover, 
                {{ULTP}} .ultp-searchresult-publishdate:hover, 
                {{ULTP}} .ultp-searchresult-category a:hover { color:{{resMetaHoverColor}}; }` 
            },
        ],
    },
    // Hover End
    resTitleTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:14, unit:'px'},height: {lg:23, unit:'px'}, decoration: 'none',family: '', weight:500},
        style: [
            {   
                selector:'{{ULTP}} .ultp-search-result  a.ultp-searchresult-title'
            }
        ]
    },
    excerptTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:14, unit:'px'},height: {lg:22, unit:'px'}, decoration: 'none',family: '', weight:300},
        style: [
            {   
                depends: [
                    { key:'resExcerptEnable', condition:'==', value: true },
                ],  
                selector:'{{ULTP}} .ultp-searchresult-excerpt'
            }
        ]
    },
    resMetaTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:12, unit:'px'},height: {lg:22, unit:'px'}, decoration: 'none',family: '', weight:300},
        style: [
            {   
                selector:
                `{{ULTP}} .ultp-search-result .ultp-searchresult-author, 
                {{ULTP}} .ultp-search-result .ultp-searchresult-publishdate, 
                {{ULTP}}  .ultp-searchresult-category a` 
            }
        ]
    },
    resExcerptLimit:{
        type: 'string',
        default: '25',
        style: [
            {   
                depends: [
                    { key:'resExcerptEnable', condition:'==', value: true },
                ],  
            }
        ]
    },
    // Meta Separator Style
    resultMetaSpace:{
        type: 'object',
        default: {lg:'8', unit: 'px'},
        style: [
            {
                selector:
                `{{ULTP}} .ultp-rescontent-meta > div::after, 
                {{ULTP}} .ultp-rescontent-meta > .ultp-searchresult-author:after { margin: 0px {{resultMetaSpace}} }`
            },
        ]
    },
    resultMetaSeparatorSize:{
        type: 'object',
        default: {lg:'5', unit: 'px'},
        style: [
            {
                selector:
                `{{ULTP}} .ultp-rescontent-meta > div::after, 
                {{ULTP}} .ultp-rescontent-meta > .ultp-searchresult-author:after { height:{{resultMetaSeparatorSize}}; width:{{resultMetaSeparatorSize}}; }`
            },
        ]
    },
    resMetaSeparatorColor:{
        type: 'string',
        default: '#4A4A4A',
        style: [
            {
                selector:
                `{{ULTP}} .ultp-rescontent-meta > div::after, 
                {{ULTP}} .ultp-rescontent-meta > .ultp-searchresult-author:after { background-color:{{resMetaSeparatorColor}}; }` 
            },
        ],
    },
    //
    resultHighlighter: {
        type:"boolean",
        default: true,
        style:[
            { 
                depends: [
                    { key:'resultHighlighter', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-highlight { font-weight: bold; text-decoration: underline } '
            },
            { 
                depends: [
                    { key:'resultHighlighter', condition:'==', value: false },
                ],
            },
        ],
    },
    resultHighlighterColor: {
        type: 'string',
        default: '#777777',
        style:[
            { 
                depends: [
                    { key:'resultHighlighter', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-search-highlight { color: {{resultHighlighterColor}}; }'
            }
        ],
    },
    // dropdown wrapper
    resultBg:{
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#FCFCFC'},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-result' 
            }
        ],
    },
    // resultWidth:{
    //     type: 'object',
    //     default: {lg:'', unit: 'px'},
    //     style: [
    //         {
    //             depends: [
    //                 { key:'searchPopup', condition:'!=', value: true },
    //             ],
    //             selector:'{{ULTP}} .ultp-search-result { max-width:{{resultWidth}}; width: 100%; } ' 
    //         },
    //         {
    //             depends: [
    //                 { key:'searchPopup', condition:'!=', value: true },
    //             ],
    //             selector:'{{ULTP}} .ultp-search-result, {{ULTP}}  .ultp-searchform-content  { max-width:{{resultWidth}}; width: 100%; } ' 
    //         },
    //     ]
    // },
    resultMaxHeight:{
        type: 'object',
        default: {lg:'300', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'popupAnimation', condition:'==', value: 'fullwidth' },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result {  max-height:{{resultMaxHeight}} !important; }  
                {{ULTP}} .ultp-search-canvas:has(.ultp-search-clear.active) .ultp-search-result { min-height:{{resultMaxHeight}} !important; }` 
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result { max-height:{{resultMaxHeight}}; }  
                {{ULTP}} .ultp-result-data { max-height:calc({{resultMaxHeight}} - 50px); }`  
            },
            {
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result { max-height:{{resultMaxHeight}}; } 
                {{ULTP}} .ultp-result-data { max-height:calc({{resultMaxHeight}} - 50px); }` 
            },
        ]
    },
    // Enable Separator resultSeparatorEnable
    // More Result Separator - resMoreResultDevider
    // More result - moreResultsbtn 
    resultPadding:{
        type: 'object',
        default: {lg:{top: '15', bottom: '15', left: '15', right: '15', unit:'px'}},
        style:[
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-dropdown .ultp-result-data.ultp-result-show, 
                {{ULTP}}.popup-active .ultp-result-data.ultp-result-show { padding:{{resultPadding}}; } 
                {{ULTP}} .ultp-search-result .ultp-result-data:has( > div) { padding:{{resultPadding}}; }  
                {{ULTP}} .ultp-search-noresult { padding: {{resultPadding}} !important;}` 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'==', value: false },
                ], 
                selector:'{{ULTP}} .ultp-search-result__item { padding:{{resultPadding}};} ' 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: false },
                    { key:'resMoreResultDevider', condition:'==', value: false },
                ], 
                selector:'{{ULTP}} .ultp-search-result__item { padding:{{resultPadding}}; } ' 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: false },
                    { key:'moreResultsbtn', condition:'==', value: false },
                    { key:'resMoreResultDevider', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}}.popup-active .ultp-result-data.ultp-result-show, 
                {{ULTP}} .ultp-search-noresult { padding: {{resultPadding}} !important;} 
                {{ULTP}} .ultp-result-data:has( > div) { padding:{{resultPadding}}; }`  
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: false },
                    { key:'resMoreResultDevider', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-result__item { padding:{{resultPadding}}; } ' 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: false },
                    { key:'moreResultsbtn', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}}.popup-active .ultp-result-data.ultp-result-show,  
                {{ULTP}} .ultp-search-noresult { padding: {{resultPadding}} !important;} 
                {{ULTP}} .ultp-result-data:has( > div) { padding:{{resultPadding}}; }` 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: false },
                    { key:'moreResultsbtn', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}}.popup-active .ultp-result-data.ultp-result-show,  
                {{ULTP}} .ultp-search-noresult {  padding: {{resultPadding}} !important;} 
                {{ULTP}} .ultp-result-data:has( > div) { padding:{{resultPadding}}; }` 
            },
            { 
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: false },
                    { key:'moreResultsbtn', condition:'==', value: false },
                    { key:'resMoreResultDevider', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}}.popup-active .ultp-result-data.ultp-result-show,  
                {{ULTP}} .ultp-search-noresult { color: green; padding: {{resultPadding}} !important;} 
                {{ULTP}} .ultp-result-data:has( > div) { padding:{{resultPadding}}; color: red; }` 
            },
            
            
            // { 
            //     depends: [
            //         { key:'resultSeparatorEnable', condition:'==', value: true },
            //         { key:'resMoreResultDevider', condition:'==', value: false },
            //         { key:'moreResultsbtn ', condition:'==', value: false },
            //     ], 
            //     selector:'{{ULTP}} .ultp-search-result .ultp-search-result__item { padding:{{resultPadding}}; } ' 
            // },
            // { 
            //     depends: [
            //         { key:'resultSeparatorEnable', condition:'==', value: true },
            //         { key:'resMoreResultDevider', condition:'==', value: true },
            //         { key:'moreResultsbtn ', condition:'==', value: false },
            //     ], 
            //     selector:'{{ULTP}} .ultp-search-result .ultp-search-result__item { padding:{{resultPadding}}; } ' 
            // },
            // { 
            //     depends: [
            //         { key:'resultSeparatorEnable', condition:'==', value: false },
            //         { key:'resMoreResultDevider', condition:'==', value: false },
            //         { key:'moreResultsbtn ', condition:'==', value: false },
            //     ], 
            //     selector:`{{ULTP}} .ultp-search-result .ultp-result-data:has( > div) { padding:{{resultPadding}}; }  {{ULTP}} .ultp-search-noresult { padding: {{resultContainerPadding}} !important;}` 
            // },
        ],
    },
    resultBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                selector:'{{ULTP}} .ultp-search-result' 
            },
        ],
    },
    resultBorderRadius:{
        type: 'object',
        default: {lg:{top: '', bottom: '', left: '', right: '', unit:'px'}},
        style:[
            { 
                selector:'{{ULTP}} .ultp-search-result { border-radius: {{resultBorderRadius}}; }' 
            }
        ],
    },
    resultShadow:{
        type: "object",
        default: {
            openShadow: 0,
            width: { top: 0, right: 3, bottom: 6, left: 0},
            color: "rgba(0, 0, 0, 0.16)",
        },
        style: [
            { 
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'popupAnimation', condition:'==', value: 'popup' },
                ], 
                selector:'{{ULTP}} .ultp-search-canvas:has(.ultp-search-clear.active) .ultp-search-result'
            },
            { 
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'popupAnimation', condition:'==', value: 'top' },
                ], 
                selector:'{{ULTP}} .ultp-search-result:has(.ultp-result-data > div)'
            },
            { 
                depends: [
                    { key:'searchPopup', condition:'==', value: true },
                    { key:'popupAnimation', condition:'==', value: 'fullwidth' },
                ], 
                selector:'{{ULTP}} .ultp-search-canvas:has(.ultp-search-clear.active) .ultp-search-result'
            },
            { 
                depends: [
                    { key:'searchPopup', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result:has(.active), 
                {{ULTP}} .ultp-search-result:has( .ultp-result-data > .ultp-search-result__item )`
            },
        ],
    },
    resultSpacingX:{
        type: 'object',
        default: {lg:'0', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'searchPopup', condition:'!=', value: true},
                ], 
                selector:'{{ULTP}} .ultp-search-result { left:{{resultSpacingX}}; }'
            },
        ]
    },
    resultSpacingY:{
        type: 'object',
        default: {lg:'0', unit: 'px'},
        style: [
            {
                selector:'{{ULTP}} .ultp-search-result { top:{{resultSpacingY}}; }'
            },
        ]
    },
    resultSeparatorEnable:{
        type:"boolean",
        default: true,
    },
    resultSeparatorColor:{
        type: 'string',
        default: '#DEDEDE',
        style: [
            {
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-search-result__item { border-color: {{resultSeparatorColor}} !important; }' 
            },
        ],
    },
    resultSeparatorWidth:{
        type: 'object',
        default: {lg:'1', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'moreResultsbtn', condition:'==', value: true },
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'==', value: true },
                ], 
                selector:'{{ULTP}} .ultp-viewall-results { border-top: {{resultSeparatorWidth}} solid !important; }' 
            },
            {
                depends: [
                    { key:'moreResultsbtn', condition:'==', value: true },
                    { key:'resMoreResultDevider', condition:'!=', value: true },
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result__item { border-top: {{resultSeparatorWidth}} solid; } 
                {{ULTP}} .ultp-search-result__item { margin-top: -{{resultSeparatorWidth}}; }` 
            },
            {
                depends: [
                    { key:'moreResultsbtn', condition:'!=', value: true },
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-search-result__item { border-top: {{resultSeparatorWidth}} solid; } 
                {{ULTP}} .ultp-search-result__item { margin-top: -{{resultSeparatorWidth}}; }` 
            },
        ]
    },
    resMoreResultDevider: {
        type:"boolean",
        default: true,
        style: [
            {
                depends: [
                    { key:'resultSeparatorEnable', condition:'==', value: true },
                    { key:'moreResultsbtn', condition:'==', value: true },
                ], 
            },
        ]
    },
    // resultSeparatorSpace:{
    //     type: 'object',
    //     default: {lg:'10', unit: 'px'},
    //     style: [
    //         {
    //             selector:'{{ULTP}} .ultp-search-result .ultp-search-result__item { padding-top:{{resultSeparatorSpace}}; margin-bottom:{{resultSeparatorSpace}} }' 
    //         },
    //     ]
    // },
    
    /* =================================  
            Search Result Settings
    ================================= */  
    moreResultsbtn:{
        type:"boolean",
        default: true,
    },
    viewMoreResultText: {
        type: 'string', 
        default: 'View More Results', 
    },
    moreResultPosts: {
        type: 'string', 
        default: 3, 
        style: [
            {
                depends: [
                    { key:'moreResultsbtn', condition:'==', value: true },
                ], 
            },
        ]
        
    },
    moreResultsText:{
        type: 'string', 
        default: 'View More Results' 
    },
    moreResultsTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:14, unit:'px'},height: {lg:23, unit:'px'}, decoration: 'none',family: '', weight:500},
        style: [
            {   
                selector:'{{ULTP}} .ultp-viewall-results'
            }
        ]
    },
    moreResultsColor:{
        type: 'string',
        default: '#646464',
        style: [
            {
                selector:'{{ULTP}} .ultp-viewall-results { color:{{moreResultsColor}}; }' 
            },
        ],
    },
    moreResultsHoverColor:{
        type: 'string',
        default: '#000',
        style: [
            {
                selector:'{{ULTP}} .ultp-viewall-results:hover { color:{{moreResultsHoverColor}}; }'  
            },
        ],
    },
  /* ========  Advanced Settings  ======== */
    loadingColor: {
        type: "string",
        default: "#000",
        style: [{
            selector: 
            `{{ULTP}} .ultp-result-loader.active:before, 
            {{ULTP}} .ultp-viewmore-loader.viewmore-active { border-color: {{loadingColor}} {{loadingColor}}  {{loadingColor}} transparent !important; }`,
        }],
    },
    advanceId: { 
        type: "string", 
        default: "" 
    },
    advanceZindex: {
        // type number
        type: "string",
        default: "",
        style: [
            { selector: "{{ULTP}} .ultp-block-wrapper { z-index:{{advanceZindex}};}" },
        ],
    },
    /* ====== General Advnace Settings End ====== */
    hideExtraLarge: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    hideTablet: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    hideMobile: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    advanceCss: { 
        type: "string", 
        default: "", 
        style: [{ selector: "" }],
    },
    
};

export default attributes;
