const { __ } = wp.i18n
const { InspectorControls, RichText } = wp.blockEditor
const { useEffect, useState, useRef, Fragment } = wp.element
import { isInlineCSS, updateCurrentPostId } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import IconPack from '../../helper/fields/tools/IconPack'
import ToolBarElement from '../../helper/ToolBarElement'
import UltpLinkGenerator from '../../helper/UltpLinkGenerator'
import AdvancedSearch from './Settings'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

    const prevPropsRef = useRef(null);
    const [section, setSection] = useState('Content');
    const [state, setState] = useState({ 
        postsList: '', 
        emptyList: false, 
        viewAll: false, 
        loading: false, 
        error: false,  
        searchVal: '', 
        searchNavPopup: false, 
        loadmore: 1,
        totalPost: null
    });

    const { setAttributes, className, name, clientId, attributes, attributes: { blockId, currentPostId, searchAjaxEnable, moreResultsbtn, resImageEnable, resAuthEnable, resDateEnable, resCatEnable, resExcerptEnable, resExcerptLimit, moreResultPosts, searchPostType, searchButtonText, searchInputPlaceholder, previewImg, advanceId, searchFormStyle, searchPopupIconStyle, searchnoresult, searchPopup, searchBtnText, searchBtnIcon, popupAnimation, moreResultsText, windowpopupHeading, windowpopupText, blockPubDate } } = props

    useEffect(() => {
        const _client = clientId.substr(0, 6)
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId );
        if (!blockId) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
                if (!reference?.hasOwnProperty('theme')) {
                    setAttributes({ blockId: _client });
                }
            }
        }
        // blockPubDate
		if(blockPubDate == "empty") {
			setAttributes({ blockPubDate: new Date().toISOString() });
		}

    }, [clientId]);

    useEffect(() => {
		const prevAttributes = prevPropsRef.current;
        if (!prevAttributes) {
            prevPropsRef.current = attributes;
        } else {
            const postnumberCondition = prevAttributes.moreResultPosts !== attributes.moreResultPosts || attributes.moreResultsbtn !== prevAttributes.moreResultsbtn || prevAttributes.resExcerptLimit != attributes.resExcerptLimit || attributes.resExcerptEnable != prevAttributes.resExcerptEnable || prevAttributes.searchPostType != attributes.searchPostType;
            if ( prevAttributes.searchVal !== state.searchVal || postnumberCondition ) {
                handleSearchValue(state.searchVal, true);
            }
            if ( prevAttributes.loadmore != state.loadmore ) {
                handleSearchValue(state.searchVal, false, state.loadmore);
            }
        }
    }, [attributes]);

    function handleSearchValue(search, newblock = false, viewMoreVal = 1){
        let postNumber =  moreResultPosts;
        if(!moreResultsbtn) {
            postNumber = 10;
            viewMoreVal = 1;
            setState({ ...state, postsList: ''})
        }

        setState({ ...state, postsList: newblock ? '' : state.postsList, loading: true, emptyList: false })
        if(search && search.length > 2  && searchAjaxEnable  ){
            wp.apiFetch({
                path: '/ultp/ultp_search_data',
                method: 'POST',
                data: { 
                    searchText: search, 
                    postPerPage: postNumber,
                    paged: viewMoreVal,
                    image: resImageEnable, 
                    author: resAuthEnable, 
                    category: resCatEnable,
                    date: resDateEnable, 
                    excerpt: resExcerptEnable, 
                    excerptLimit: resExcerptLimit,
                    exclude: searchPostType.length > 0 && JSON.parse(searchPostType),
                }
            }).then(res => {
                if(res.post_data.length > 0){
                    setState({ ...state, postsList: res.post_data, loading: false, error: false, emptyList: false, totalPost: res.post_count})
                    if(newblock){
                        setState({ ...state, postsList: res.post_data, loadmore: 1})
                    } else {
                        setState({ ...state, postsList: state.postsList + res.post_data  })
                    }
                } else if(res.post_data.length < 1){
                    setState({ ...state, loading: false, error: false, loadmore: 1, emptyList: true });
                }
            }).catch( (error) => {
                console.log("error", error.message);
                setState({ ...state, loading: true, error: true })
            })
        } else {
            setState({ ...state, loading: false, error: false, viewAll: false, loadmore: 1  })
        }
    };

    function searchButton( style, textEnable = true, iconEnable = true) {
        const textShow = textEnable && style != "popup-icon1";
        return (
            <div className={`${style ? 'ultp-searchpopup-icon ultp-searchbtn-'+style : 'ultp-search-button'}`} >
                { iconEnable && IconPack.search_line }
                { textShow && 
                    <RichText
                        key="editable"
                        tagName={'span'}
                        className={'ultp-search-button__text'}
                        keepPlaceholderOnFocus
                        placeholder={__('Change Text...', 'ultimate-post')}
                        onChange={value => setAttributes({ searchButtonText: value })}
                        value={searchButtonText}
                    />
                }
            </div>
        )
    }

    function renderSearchResult(postData,  moreResultsbtn,  moreResultsText, searchnoresult) {
        const show = postData?.length > 0 || state.loading && state.totalPost != state.postsList.length || state.emptyList && state.searchVal != 0 || moreResultsbtn && state.postsList.length > 0 && state.searchVal.length != 0 && !(state.totalPost < (state.loadmore * moreResultPosts));
        return (
            <div className={`ultp-search-result ${show ? 'ultp-result-show' : '' }`}>
                <div className={`ultp-result-data  ${postData?.length > 0 ? 'ultp-result-show' : '' }`} dangerouslySetInnerHTML={{__html: postData}} />
                { state.loading && state.totalPost != state.postsList.length &&
                    <div className="ultp-search-result__item ultp-result-loader active"></div>
                }
                { state.emptyList && state.searchVal != 0 &&
                    <div className="ultp-search-result__item ultp-search-noresult active">
                        <RichText
                            key="editable"
                            tagName={'span'}
                            className={'ultp-search-button__text'}
                            keepPlaceholderOnFocus
                            placeholder={__('Change Text...', 'ultimate-post')}
                            onChange={value => setAttributes({ searchnoresult: value })}
                            value={searchnoresult}
                        />
                    </div>
                }
                { moreResultsbtn && state.postsList.length > 0 && state.searchVal.length != 0 && !(state.totalPost < (state.loadmore * moreResultPosts)) && state.totalPost - (state.loadmore * moreResultPosts) != 0 &&
                    <div className="ultp-backend-viewmore ultp-search-result__item ultp-viewall-results active" onClick={()=> setState({...state, viewAll: !state.viewAll, loadmore: Number(state.loadmore) + 1, loading: true})} >
                        <RichText
                            key="editable"
                            tagName={'span'}
                            className={''}
                            keepPlaceholderOnFocus
                            placeholder={__('Change Text...', 'ultimate-post')}
                            onChange={value => setAttributes({ moreResultsText: value })}
                            value={moreResultsText}
                        />
                        ({state.totalPost - (state.loadmore * moreResultPosts)})
                    </div>
                }
            </div>
        )
    }

    function searchInputForm(searchFormStyle, searchBtnText, searchBtnIcon) {
        return (
            <div className={`ultp-searchform-content ultp-searchform-${searchFormStyle}`}>
                <div className="ultp-search-inputwrap">
                    <input type="text" className="ultp-searchres-input" value={state.searchVal} placeholder={searchInputPlaceholder} onChange={e => setState({...state, searchVal : e.target.value})} />
                    { state.searchVal.length > 2 && 
                        <span className="ultp-search-clear active" data-blockid={blockId} onClick={()=> setState({...state, searchVal: '', postsList: '', loadmore: 1})}>{IconPack.close_line}</span>
                    }
                </div>
                { searchButton( false, searchBtnText, searchBtnIcon) }
            </div>
        )
    }

        const store = {setAttributes, name, attributes, setSection, section, clientId }  

        let __preview_css;
        if (blockId) { 
            __preview_css = CssGenerator(attributes, 'ultimate-post/advanced-search', blockId, isInlineCSS());
        }
        if (previewImg) {
            return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
        }
        const postData =  state.searchVal.length == 0 ? [] : state?.postsList;
        const upgradePro = UltpLinkGenerator('', 'advanced_search', ultp_data.affiliate_id);

        return (
            <Fragment>
                <InspectorControls>
                    <AdvancedSearch store={store}/>
                </InspectorControls>
                <ToolBarElement
                    include={[ { 
                            type:'template' 
                        }]}
                    store={store} />
                <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className}`}>
                    { __preview_css &&
                        <style dangerouslySetInnerHTML={{__html: __preview_css}}></style>
                    }
                    {
                        !ultp_data.active && 
                        <div className="ultp-pro-helper">
                            <div className="ultp-pro-helper__upgrade"> 
                                <span>To Unlock Search - PostX Block</span>
                                <a className="ultp-upgrade-pro" href={upgradePro} target="_blank"> Upgrade to Pro </a>
                                <a href={`https://www.wpxpo.com/postx/blocks/#demoid8233`} target="_blank"> View Demo</a>
                            </div>
                        </div>
                    }
                    <div className={`ultp-block-wrapper ${!ultp_data.active ? ' ultp-wrapper-pro' : ''}`}>
                        <div className={`ultp-search-container ${searchPopup ? ' popup-active ultp-search-animation-'+popupAnimation : ''}`} >
                            {   searchPopup &&
                                <div onClick={()=> setState({...state, searchNavPopup: !state.searchNavPopup, postsList: '', loading: false})}>
                                    {searchButton( searchPopupIconStyle)}
                                </div>
                            }
                            {
                                searchPopup && 
                                <div className="ultp-search-popup">
                                    <div className={`ultp-canvas-wrapper ${state.searchNavPopup ? 'canvas-popup-active' : '' }`}>
                                        <div className="ultp-search-canvas" >
                                            {
                                                windowpopupHeading &&
                                                <RichText
                                                    key="editable"
                                                    tagName={'div'}
                                                    className={'ultp-search-popup-heading'}
                                                    keepPlaceholderOnFocus
                                                    placeholder={__('Change Text...', 'ultimate-post')}
                                                    onChange={value => setAttributes({ windowpopupText: value })}
                                                    value={windowpopupText}
                                                />
                                            }
                                            { searchInputForm(searchFormStyle, searchBtnText, searchBtnIcon) }
                                            { searchAjaxEnable && 
                                                renderSearchResult(postData, moreResultsbtn, moreResultsText, searchnoresult)
                                            }
                                        </div>
                                    </div>
                                    { searchPopup && popupAnimation != "popup" && state.searchNavPopup &&
                                        <div className="ultp-popupclose-icon" onClick={()=> setState({ ...state, searchVal: '', searchNavPopup: false, postsList: '', loading: false})}>{ IconPack.close_line }</div>
                                    }
                                </div>
                            }
                            { ! searchPopup &&  
                                searchInputForm(searchFormStyle, searchBtnText, searchBtnIcon)
                            }
                        </div>
                        {
                            searchAjaxEnable && !searchPopup && 
                            <div className="ultp-search-dropdown">
                                { renderSearchResult(postData, moreResultsbtn, moreResultsText, searchnoresult) }
                            </div>
                        }
                    </div>
                </div>
            </Fragment>
        )
    }