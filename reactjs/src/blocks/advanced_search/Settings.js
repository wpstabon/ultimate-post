const { __ } = wp.i18n
import { GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, GeneralContent, CommonSettings, blockSupportLink } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'
import TemplateModal from '../../helper/TemplateModal'

const AdvancedSearch = (props) => {
    const {store} = props
    const { searchPopup, searchAjaxEnable, moreResultsbtn } = store.attributes;

    return (
        <>
            <TemplateModal 
                prev="https://www.wpxpo.com/postx/blocks/#demoid8233"  
                store={store}
            />
            <Sections>
                <Section slug="setting" title={__('Setting','ultimate-post')}>
                    <CommonSettings store={store} initialOpen={true}
                        title={__('Structure Setting','ultimate-post')} 
                        include={[
                            {
                                position: 1, data:{ type:'toggle',key:'searchAjaxEnable', label:__('Ajax Search','ultimate-post') } 
                            },
                            {
                                position: 2, data:{ type:'select', key:'searchFormStyle', image: true, defaultMedia: true, label:__('Choose Search Form Style', 'ultimate-post'),
                                    options:[
                                        {  label: 'Input Style 1', value: 'input1', img:'assets/img/layouts/search/inputform/input-form1.svg'},
                                        {  label: 'Input Style 2', value: 'input2', img: 'assets/img/layouts/search/inputform/input-form2.svg'}, 
                                        {  label: 'Input Style 3', value: 'input3', img: 'assets/img/layouts/search/inputform/input-form3.svg'},
                                    ],
                                }
                            },
                            {
                                position: 3, data:{ type:'text',key:'searchnoresult', label:__('No Result Found Text','ultimate-post') } 
                            },
                            {
                                position: 4, data:{ type:'toggle',key:'searchPopup', label:__('Enable Search Popup','ultimate-post') } 
                            },
                            {
                                position: 5, data:{ type:'select', key:'searchPopupIconStyle', defaultMedia: true, image: true,label:__('Choose Popup Icon Style', 'ultimate-post'),
                                    options:[
                                        {  label: 'Popup Style 1', value: 'popup-icon1', img: 'assets/img/layouts/search/popupicon/search.svg'},
                                        {  label: 'Popup Style 2', value: 'popup-icon2', img: 'assets/img/layouts/search/popupicon/leftsearch.svg'},
                                        {  label: 'Popup Style 3', value: 'popup-icon3', img: 'assets/img/layouts/search/popupicon/rightsearch.svg'},
                                    ],
                                }
                            },
                            {
                                position: 6, data: { type:'search', key:'searchPostType', label:__('Post Type Exclude','ultimate-post'), multiple:true, search:'allPostType', pro: true }
                            },
                            {
                                position: 6, data:{ type:'alignment', key: 'searchIconAlignment', label:__('Popup Icon Alignment','ultimate-post'), disableJustify:true } 
                            }
                        ]} />
                    {
                        searchPopup &&
                        <CommonSettings store={store} initialOpen={false}
                            title={__('Icon','ultimate-post')} 
                            include={[
                                {
                                    position: 1, data: { type:'range', key:'popupIconGap', min: 0, max: 300, step: 1, responsive: true, label:__('Gap Between Text and Icon','ultimate-post') } 
                                },
                                {
                                    position: 2, data: { type:'range', key:'popupIconSize', min: 0, max: 300, step: 1, responsive: true, label:__('Icon Size','ultimate-post') } 
                                },
                                {   
                                    position: 3, data: { type:'typography',key:'popupIconTextTypo',label:__('Search Text Typography','ultimate-post') } 
                                },
                                {   position: 4, data: { type: 'tab', content: [
                                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                        { 
                                            type:'color', key:'popupIconColor', label:__('Color','ultimate-post') 
                                        },
                                        { 
                                            type:'color', key:'popupIconTextColor', label:__('Text Color','ultimate-post') 
                                        },
                                        { 
                                            type:'color2', key:'popupIconBg', label:__('Background Color','ultimate-post') 
                                        },
                                        { 
                                            type:'border', key:'popupIconBorder', label:__('Border','ultimate-post') 
                                        }
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                    { 
                                        type:'color', key:'popupIconHoverColor', label:__('Hover Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color', key:'popupIconTextHoverColor', label:__('Text Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color2', key:'popupIconHoverBg', label:__('Hover Background Color','ultimate-post') 
                                    },
                                    { 
                                        type:'border', key:'IconHoverBorder', label:__('Hover Border','ultimate-post') 
                                    }

                                ]}
                            ]}},
                            {
                                position: 5, data:  {  type:'dimension', key:'popupIconPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') },
                            },
                            {
                                position: 7, data:  {  type:'dimension', key:'popupIconRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')  },
                            }
                            ]} />
                    }
                    <CommonSettings store={store} initialOpen={false}
                        title={__('Search Button','ultimate-post')} 
                        include={[
                            {
                                position: 1, data:{ type:'toggle', key:'enableSearchPage', label:__('Click to go Search Result Page','ultimate-post') } 
                            },
                            {
                                position: 2, data:{ type:'toggle', key:'btnNewTab', label:__('Open In New Tab','ultimate-post') } 
                            },
                            {
                                position: 3, data:{ type:'toggle', key:'searchBtnText', label:__('Enable Text','ultimate-post') } 
                            },
                            {
                                position: 4, data:{ type:'toggle', key:'searchBtnIcon', label:__('Enable Icon','ultimate-post') } 
                            },
                            {
                                position: 5, data:{ type:'toggle', key:'searchIconAfterText', label:__('Icon After Text','ultimate-post') } 
                            },
                            {
                                position: 6, data:{ type:'toggle', key:'searchBtnReverse', label:__('Search Button Reverse','ultimate-post') } 
                            },
                            {
                                position: 7, data: { type:'text', key:'searchButtonText', label:__('Search Button Text','ultimate-post') } 
                            },
                            {
                                position: 8, data: { type:'range', key:'searchButtonPosition', min: 0, max: 300, step: 1, responsive: true, label:__('Button Gap','ultimate-post') } 
                            },
                            {
                                position: 9, data: { type:'range', key:'searchTextGap', min: 0, max: 300, step: 1, responsive: true, label:__('Search Text Gap','ultimate-post') } 
                            },
                            {
                                position: 10, data: { type:'range', key:'searchBtnIconSize', min: 0, max: 300, step: 1, responsive: true, label:__('Icon Size','ultimate-post') } 
                            },
                            {   
                                position: 11, data: { type:'typography', key:'searchBtnTextTypo',label:__('Search Text Typography','ultimate-post') } 
                            },
                            {   position: 12, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                    { 
                                        type:'color', key:'searchBtnIconColor', label:__('Icon Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color', key:'searchBtnTextColor', label:__('Text Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color2', key:'searchBtnBg', label:__('Background Color','ultimate-post') 
                                    },
                                    {
                                        type:'border', key:'btnBorder', label:__('Border','ultimate-post')
                                    },
                                ]
                            },
                            {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                { 
                                    type:'color', key:'searchBtnIconHoverColor', label:__('Icon Hover Color','ultimate-post') 
                                },
                                { 
                                    type:'color', key:'searchBtnTextHoverColor', label:__('Text Color','ultimate-post') 
                                },
                                { 
                                    type:'color2', key:'searchBtnHoverBg', label:__('Hover Background Color','ultimate-post') 
                                },
                                {
                                    type:'border', key:'btnHoverBorder', label:__('Hover Border','ultimate-post')
                                },
                            ]},
                            
                        ]}},
                        {
                            position: 13, data: {  type:'dimension', key:'searchBtnPadding', step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post')  },
                        },
                        {
                            position: 14, data: {  type:'dimension', key:'searchBtnRadius', step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')   },
                        }
                        ]} />
                    {   
                        searchPopup &&
                        <CommonSettings store={store} initialOpen={false}
                        title={__('Popup Canvas','ultimate-post')} 
                        include={[
                            {
                                position: 1, data:{ type:'select', key:'popupAnimation', image: true, 
                                defaultMedia: true,label:__('Popup  Type', 'ultimate-post'),
                                    options:[
                                        {  label: 'Popup', value: 'popup', img:'assets/img/layouts/search/popuptype/popup.svg'},
                                        {  label: 'Top Window', value: 'top', img: 'assets/img/layouts/search/popuptype/topView.svg'},
                                        {  label: 'Full Window', value: 'fullwidth', img: 'assets/img/layouts/search/popuptype/fullWindow.svg'},
                                    ],
                                }
                            },
                            {
                                position: 2, data: { type:'separator', label:__('Search Popup Heading','ultimate-post') } 
                            },
                            {
                                position: 3, data:{ type:'toggle', key:'windowpopupHeading', label:__('Search Heading','ultimate-post') } 
                            },
                            {
                                position: 4, data:{ type:'alignment', key:'windowpopupHeadingAlignment', disableJustify: true, label:__('Heading Alignment','ultimate-post') } 
                            },
                            {
                                position: 5, data:{ type: 'text', key:'windowpopupText', label:__('Heading Text','ultimate-post') }
                            },
                            {   
                                position: 6, data: { type:'typography', key:'windowpopupHeadingTypo',label:__('Search Heading Typography','ultimate-post') } 
                            },
                            {
                                position: 7, data: { type:'color', key:'windowpopupHeadingColor', label:__('Search Heading Color','ultimate-post') },
                            },
                            {
                                position: 8, data: { type:'range', key:'popupHeadingSpacing', min: 0, max: 300, step: 1, responsive: true, label:__('Heading Spacing','ultimate-post') } 
                            },
                            {
                                position: 9, data: { type:'separator', key:"popupCloseIconSeparator" ,label:__('Close Icon Style','ultimate-post') } 
                            },

                            {
                                position: 10, data: { type:'range', key:'popupCloseSize', min: 0, max: 300, step: 1, responsive: true, label:__('Close Icon Size','ultimate-post') } 
                            },
                            {
                                position: 11, data: { type:'color', key:'popupCloseColor', label:__('Close Icon Color','ultimate-post') },
                            },
                            {
                                position: 12, data: { type:'color', key:'popupCloseHoverColor', label:__('Close Icon Hover Color','ultimate-post') },
                            },
                            {
                                position: 13, data: { type:'separator', label:__('Canvas Wrapper Style','ultimate-post') } 
                            },
                            {
                                position: 14, data: {  type:'color2', key:'popupBG', label:__('Canvas Background','ultimate-post')}
                            },
                            {
                                position: 15, data: { type:'range', key:'canvasWidth', min: 0, max: 1000, step: 1, unit:true, responsive: true, label:__('Canvas Width','ultimate-post') } 
                            },
                            {
                                position: 16, data:  {  type:'dimension', key:'popupPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') },
                            },
                            {
                                position: 17, data:  {  type:'dimension', key:'popupRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')  },
                            },
                            { 
                                position: 18, data: { type:'boxshadow', key:'popupShadow', step:1 , unit:true , responsive:true , label:__('Box Shadow','ultimate-post')  }
                            },
                            {
                                position: 19, data: { type:'tag',key:'searchPopupPosition', label:__('Position Left/Right','ultimate-post'), options:[
                                    { value:'left',label:__('Start Left','ultimate-post') },
                                    { value:'right',label:__('Start Right','ultimate-post') }] } 
                            },
                            {
                                position: 20, data: { type:'range', key:'popupPositionX', min: 0, max: 300, step: 1, responsive: true, label:__('Dropdown Position X','ultimate-post') } 
                            },
                            {
                                position: 21, data: { type:'range', key:'popupPositionY', min: 0, max: 300, step: 1, responsive: true, label:__('Dropdown Position Y','ultimate-post') } 
                            },
                        ]} />
                    }
                    <CommonSettings store={store} initialOpen={false}
                        title={__('Search Form','ultimate-post')} 
                        include={[
                            {
                                position: 1, data:{ type:'text',key:'searchInputPlaceholder', label:__('Input Placeholder','ultimate-post') } 
                            },
                            {   
                                position: 2, data: { type:'typography', key:'inputTypo', label:__('Search Input Typography','ultimate-post') } 
                            },
                            {
                                position: 3, data: { type:'range', key:'searchFormWidth', min: 0, max: 900, step: 1, unit: true, responsive: true, label:__('Search Form Width','ultimate-post') } 
                            },
                            {
                                position: 4, data: { type:'range', key:'inputHeight', min: 0, max: 300, step: 1, responsive: true, label:__('Input Height','ultimate-post') } 
                            },
                            {
                                position: 5, data: { type:'color', key:'inputColor', label:__('Input Color','ultimate-post') },
                            },
                            {
                                position: 6, data: {  type:'color2', key:'inputBg', label:__('Input Background','ultimate-post')}
                            },
                            {
                                position: 7, data: { type:'border', key:'inputFocusBorder', label:__('Input Focus Border','ultimate-post') },
                            },
                            {
                                position: 8, data: { type:'border', key:'inputBorder', label:__('Input Border','ultimate-post') },
                            },
                            {
                                position: 9, data:  {  type:'dimension', key:'inputPadding',step:1 ,unit:true , responsive:true ,label:__('Input Padding','ultimate-post') },
                            },
                            {
                                position: 10, data:  {  type:'dimension', key:'inputRadius',step:1 ,unit:true , responsive:true ,label:__('Input Radius','ultimate-post') },
                            },
                            {
                                position: 11, data: { type:'range', key:'searchClear', min: 0, max: 300, step: 1, responsive: true, label:__('Search Clear Icon Position','ultimate-post') } 
                            },
                        ]} />
                    {
                        searchAjaxEnable && 
                        <CommonSettings store={store} initialOpen={false}
                        title={__('Search Result','ultimate-post')} 
                        include={[
                            {
                                position: 1, data: { type:'range', key:'resColumn', min: 0, max: 3, step: 1, responsive: true, label:__('Result Column','ultimate-post') } 
                            },
                            {
                                position: 2, data: { type:'range', key:'resultGap', min: 0, max: 300, step: 1, responsive: true, label:__('Gap','ultimate-post') } 
                            },
                            {
                                position: 3, data:{ type:'toggle', key:'resExcerptEnable', label:__('Excerpt Enable','ultimate-post') } 
                            },
                            {
                                position: 4, data:{ type:'toggle', key:'resCatEnable', label:__('Category Enable','ultimate-post') } 
                            },
                            {
                                position: 5, data:{ type:'toggle', key:'resAuthEnable', label:__('Author Enable','ultimate-post') } 
                            },
                            {
                                position: 6, data:{ type:'toggle', key:'resDateEnable', label:__('Publish Date Enable','ultimate-post') } 
                            },
                            {
                                position: 7, data:{ type:'toggle', key:'resImageEnable', label:__('Image Enable','ultimate-post') } 
                            },
                            {
                                position: 8, data: { type:'range', key:'resImageSize', min: 0, max: 300, step: 1, responsive: true, label:__('Image Size','ultimate-post') } 
                            },
                            {
                                position: 9, data:  {  type:'dimension', key:'resImageRadius',step:1 ,unit:true , responsive:true ,label:__('Image Radius','ultimate-post')  },
                            },
                            {
                                position: 10, data: { type:'range', key:'resImageSpace', min: 0, max: 300, step: 1, responsive: true, label:__('Image Gap','ultimate-post') } 
                            },
                            {   position: 11, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                    { 
                                        type:'color', key:'resTitleColor', label:__('Title Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color', key:'resExcerptColor', label:__('Excerpt Color','ultimate-post') 
                                    },
                                    { 
                                        type:'color', key:'resMetaColor', label:__('Meta  Color','ultimate-post') 
                                    },
                                ]
                            },
                            {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                { 
                                    type:'color', key:'resTitleHoverColor', label:__('Title Hover Color','ultimate-post') 
                                },
                                { 
                                    type:'color', key:'resMetaHoverColor', label:__('Meta Hover Color','ultimate-post') 
                                }
                            ]},
                            
                        ]}},
                        {   
                            position: 12, data: { type:'typography', key:'resTitleTypo', label:__('Title Typography','ultimate-post') } 
                        },
                        {   
                            position: 13, data: { type:'typography', key:'resMetaTypo', label:__('Meta Typography','ultimate-post') } 
                        },
                        {   
                            position: 14, data: { type:'typography', key:'excerptTypo', label:__('Excerpt Typography','ultimate-post') } 
                        },
                        {
                            position: 15, data: { type:'range', key:'resExcerptLimit', min: 0, max: 400, step: 1, label:__('Excerpt Limit','ultimate-post') } 
                        },
                        {
                            position: 16, data: { type:'separator',  label:__('Meta Separator Style','ultimate-post') } 
                        },
                        {
                            position: 17, data: { type:'range', key:'resultMetaSpace', min: 0, max: 300, step: 1, responsive: true, label:__('Meta Spacing','ultimate-post') } 
                        },
                        {
                            position: 18, data: { type:'range', key:'resultMetaSeparatorSize', min: 0, max: 300, step: 1, responsive: true, label:__('Meta Separator Size','ultimate-post') } 
                        },
                        {
                            position: 19, data: { type:'color', key:'resMetaSeparatorColor', label:__('Meta Separator Color','ultimate-post') },
                        },
                        {
                            position: 20, data:{ type:'toggle', key:'resultHighlighter', label:__('Enable Highlighter','ultimate-post') } 
                        },
                        {
                            position: 21, data: { type:'color', key:'resultHighlighterColor', label:__('Highlighter Color','ultimate-post') },
                        },
                        {
                            position: 22, data: { type:'separator',  label:__('Wrapper Style','ultimate-post') } 
                        },
                        {
                            position: 23, data: {  type:'color2', key:'resultBg', label:__('Background Color','ultimate-post')}
                        },
                        // {
                        //     position: 24, data: { type:'range', key:'resultWidth', min: 0, max: 1000, step: 1, unit:true, responsive: true, label:__('Result Box Width','ultimate-post') } 
                        // },
                        {
                            position: 24, data: { type:'range', key:'resultMaxHeight', min: 0, max: 900, step: 1, responsive: true, label:__('Result Box Height','ultimate-post') } 
                        },
                        {
                            position: 25, data:  {  type:'dimension', key:'resultPadding', unit:true , responsive:true ,label:__('Item Padding','ultimate-post') },
                        },
                        // {
                        //     position: 27, data:  {  type:'dimension', key:'resultContainerPadding', unit:true , responsive:true ,label:__('Container Padding','ultimate-post') },
                        // },
                        {
                            position: 26, data: { type:'border', key:'resultBorder', label:__('Border','ultimate-post') },
                        },
                        {
                            position: 27, data:  {  type:'dimension', key:'resultBorderRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') },
                        },
                        { 
                            position: 28, data: { type:'boxshadow', key:'resultShadow', step:1 , unit:true , responsive:true , label:__('Box Shadow','ultimate-post')  }
                        },
                        {
                            position: 29, data: { type:'range', key:'resultSpacingX', min: 0, max: 300, step: 1, responsive: true, label:__('Horizontal Position','ultimate-post') } 
                        },
                        {
                            position: 30, data: { type:'range', key:'resultSpacingY', min: 0, max: 300, step: 1, responsive: true, label:__('Vertical Position','ultimate-post') } 
                        },
                        {
                            position: 31, data:{ type:'toggle', key:'resultSeparatorEnable', label:__("Enable Separator",'ultimate-post') } 
                        },
                        {
                            position: 32, data: { type:'color', key:'resultSeparatorColor', label:__('Separator Color','ultimate-post') },
                        },
                        {
                            position: 33, data: { type:'range', key:'resultSeparatorWidth', min: 0, max: 30, step: 1, responsive: true, label:__('Separator Width','ultimate-post') } 
                        },
                        {
                            position: 34, data:{ type:'toggle', key:'resMoreResultDevider', label:__("Disable Result Item Separator",'ultimate-post') } 
                        },
                        ]} />
                    }
                    {
                        searchAjaxEnable &&
                            <CommonSettings store={store} initialOpen={false}
                            title={__("More Result's",'ultimate-post')} 
                            depend="moreResultsbtn"
                            include={[
                                {
                                    position: 1, data: { type:'range', key:'moreResultPosts', min: 0, max: 100, step: 1, label:__('Show Initial Post','ultimate-post') } 
                                },
                                {
                                    position: 2, data:{ type:'text',key:'moreResultsText', label:__('View More Results','ultimate-post') } 
                                },
                                {   
                                    position: 3, data: { type:'typography', key:'moreResultsTypo', label:__('Typography','ultimate-post') } 
                                },
                                {
                                    position: 4, data: { type:'color', key:'moreResultsColor', label:__('Color','ultimate-post') },
                                },
                                {
                                    position: 5, data: { type:'color', key:'moreResultsHoverColor', label:__('Hover Color','ultimate-post') },
                                },
                            ]} />
                    }
                    
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings store={store} initialOpen={true}
                        title={__("Adavnced Settings",'ultimate-post')} 
                        include={[
                            {
                                position: 1, data: { type:'text',key:'advanceId', label:__('ID','ultimate-post') },
                            },
                            { 
                                position: 2, data:{ type:'color',key:'loadingColor', label:__('Loading Color','ultimate-post'), pro: true } 
                            },
                            {
                                position: 3, data: { type:'range',key:'advanceZindex',min:-100, max:10000, step:1, label:__('z-index','ultimate-post') },
                            }
                        ]} />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            { blockSupportLink() }
        </>
    )
}

export default AdvancedSearch;