const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/search-block/', 'block_docs');

registerBlockType(
    'ultimate-post/advanced-search', {
        title: __('Search - PostX','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/advanced-search.svg'}/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Ensure effortless content searching.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [
            __('Advanced Search','ultimate-post'),
            __('Search Block','ultimate-post'),
            __('Search Result','ultimate-post'),
            __('Search Form','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/search.svg',
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)