const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-List-1/', 'block_docs');

registerBlockType(
    'ultimate-post/post-list-1', {
        title: __('Post List #1','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-list-1.svg'}/>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('Listing your posts in the classic style.','ultimate-post')}
        //     <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [
            __('Post Listing','ultimate-post'),
            __('Listing','ultimate-post'),
            __('Article','ultimate-post'),
            __('List View','ultimate-post'),
            __('List','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/postlist1.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)