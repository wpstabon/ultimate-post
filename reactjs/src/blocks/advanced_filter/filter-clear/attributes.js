const attributes = {
    blockId: { type: "string", default: "" },
    previewImg: { type: "string", default: "" },
    clearButtonText: { type: "string", default: "Clear Filter" },
    clearButtonPosition: { 
        type: "string", 
        default: "auto",
        style: [
            {
                selector: "{{ULTP}}-wrapper { flex-basis: {{clearButtonPosition}} ; }"
            }
        ]
    },
    cAlignEnable: {
        type: "boolean",
        default: false
    },
    cAlign: {
        type: "string",
        default: {
            lg: "start",
            sm: "start",
            xs: "start"
        },
        style: [
            {
                selector: "{{ULTP}}-wrapper { justify-content:{{cAlign}} ; display:flex;flex-wrap:wrap; }",
            },
            {
                depends:[{key: "cAlignEnable",condition:"==",value:true}],
                selector: "{{ULTP}}-wrapper { justify-content:{{cAlign}} ; display:flex;flex-wrap:wrap;flex-grow:1; }",
            },
        ],
    },
    gap: {
        type: "object",
        default: { lg: "10", unit: "px" },
        style: [
            {
                selector: "{{ULTP}}-wrapper { gap: {{gap}}; }",
            },
        ],
    },

    /*============================
        Clear Button Settings
    ============================*/
    cbTypo: {
        type: "object",
        default: {
            openTypography: 1,
            size: { lg: "14", unit: "px" },
            height: { lg: "20", unit: "px" },
            decoration: "none",
            transform: "",
            family: "",
            weight: "400",
        },
        style: [{ selector: "{{ULTP}} .ultp-clear-button" }],
    },
    cbColor: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)",
        style: [
            {
                selector: "{{ULTP}} .ultp-clear-button { color:{{cbColor}}; } ",
            },
        ],
    },
    cbColorHover: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-clear-button:hover { color:{{cbColorHover}}; } ",
            },
        ],
    },
    cbBgColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-clear-button { background-color:{{cbBgColor}}; } ",
            },
        ],
    },
    cbBgHoverColor: {
        type: "string",
        default: "var(--postx_preset_Secondary_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-clear-button:hover { background-color:{{cbBgHoverColor}}; } ",
            },
        ],
    },
    cbBorder: {
        type: "object",
        default: {
            openBorder: 1,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "var(--postx_preset_Contrast_1_color)",
            type: "solid",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-clear-button",
            },
        ],
    },
    cbBorderRadius: {
        type: "object",
        default: { lg: { top: "3", bottom: "3", left: "3", right: "3", unit: "px" } },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-clear-button { border-radius: {{cbBorderRadius}}; }",
            },
        ],
    },
    cbBoxShadow: {
        type: "object",
        default: {
            openShadow: 0,
            width: { top: 0, right: 0, bottom: 0, left: 0 },
            color: "var(--postx_preset_Secondary_color)",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-clear-button",
            },
        ],
    },
    cbPadding: {
        type: "object",
        default: {
            lg: { top: "8", bottom: "8", left: "10", right: "10", unit: "px" },
        },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-clear-button { padding:{{cbPadding}}; }",
            },
        ],
    },

    /*============================
        Selected Filters Settings
    ============================*/
    sfTypo: {
        type: "object",
        default: {
            openTypography: 1,
            size: { lg: "14", unit: "px" },
            height: { lg: "20", unit: "px" },
            decoration: "none",
            transform: "",
            family: "",
            weight: "400",
        },
        style: [{ selector: "{{ULTP}} .ultp-selected-filter-text" }],
    },
    sfIconSize: {
        type: "object",
        default: { lg: "13", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-selected-filter-icon svg { width: {{sfIconSize}}; height:{{sfIconSize}} }",
            },
        ],
    },
    sfColor: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)",
        style: [
            {
                selector:
                    `{{ULTP}} .ultp-selected-filter-text { color:{{sfColor}}; }  
                    {{ULTP}} .ultp-selected-filter-icon svg { fill:{{sfColor}}; }`,
            },
        ],
    },
    sfColorHover: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)", 
        style: [
            {
                selector:
                    `{{ULTP}} .ultp-selected-filter:hover .ultp-selected-filter-text { color:{{sfColorHover}}; }  
                    {{ULTP}} .ultp-selected-filter:hover .ultp-selected-filter-icon svg { fill:{{sfColorHover}}; }`,
            },
        ],
    },
    sfBgColor: {
        type: "string",
        default: "var(--postx_preset_Secondary_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-selected-filter { background-color:{{sfBgColor}}; } ",
            },
        ],
    },
    sfBgHoverColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-selected-filter:hover { background-color:{{sfBgHoverColor}}; } ",
            },
        ],
    },
    sfBorder: {
        type: "object",
        default: {
            openBorder: 1,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "var(--postx_preset_Contrast_1_color)",
            type: "solid",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-selected-filter",
            },
        ],
    },
    sfBorderRadius: {
        type: "object",
        default: { lg: "3", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-selected-filter { border-radius:{{sfBorderRadius}}; }",
            },
        ],
    },
    sfBoxShadow: {
        type: "object",
        default: {
            openShadow: 1,
            width: { top: 0, right: 0, bottom: 0, left: 0 },
            color: "var(--postx_preset_Secondary_color)",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-selected-filter",
            },
        ],
    },
    sfPadding: {
        type: "object",
        default: {
            lg: { top: "5", bottom: "5", left: "6", right: "6", unit: "px" },
        },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-selected-filter { padding:{{sfPadding}}; }",
            },
        ],
    },
};

export default attributes;
