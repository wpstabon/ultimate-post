import IconPack from "../../../helper/fields/tools/IconPack";

export default function SelectedFilter({ text, clientId, onChange }) {
    return (
        <div className="ultp-selected-filter">
            <span
                className="ultp-selected-filter-icon"
                role="button"
                onClick={() => onChange(clientId)}
            >
                {IconPack["close_line"]}
            </span>
            <span className="ultp-selected-filter-text">{text}</span>
        </div>
    );
}
