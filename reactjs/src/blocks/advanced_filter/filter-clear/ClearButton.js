export default function ClearButton({ attributes, onChange }) {
    const { clearButtonText } = attributes;

    return <button className="ultp-clear-button" onClick={onChange}>{clearButtonText}</button>;
}
