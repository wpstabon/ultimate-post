import { blockSupportLink, CommonSettings, ToolbarSettings } from "../../../helper/CommonPanel";
import icons from "../../../helper/icons";
import { Section, Sections } from "../../../helper/Sections";
import ToolBarElement from "../../../helper/ToolBarElement";

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;

const alignmentOptions = [
    {
        icon: (<span className="dashicons dashicons-editor-alignleft"></span>),
        value: "start",
        label: __("Left", "ultimate-post"),
    },
    {
        icon: (<span className="dashicons dashicons-editor-justify"></span>),
        value: "center",
        label: __("Normal", "ultimate-post"),
    },
    {
        icon: (<span className="dashicons dashicons-editor-alignright"></span>),
        value: "end",
        label: __("Right", "ultimate-post"),
    },
];

const clearSettings = [
    {
        data: {
            type: "toggle",
            key: "cAlignEnable",
            label: __('Enable Alignement Controls', 'ultimate-post'),
        }
    },
    {
        data: {
            type: "alignment",
            key: "cAlign",
            disableIf: (attr) => {
                return !attr["cAlignEnable"];
            },
            responsive: true,
            label: __("Alignment", "ultimate-post"),
            options: alignmentOptions.map(item => item.value)
        },
    },
    {
        data: {
            type: "tag",
            key: "clearButtonPosition",
            label: __("Position", "ultimate-post"),
            options: [
                {
                    value: "auto",
                    label: __("Inline", "ultimate-post"),
                },
                {
                    value: "100%",
                    label: __("Block", "ultimate-post"),
                },
            ]
        },
    },
    {
        data: {
            type: "text",
            key: "clearButtonText",
            label: __("Text", "ultimate-post"),
        },
    },
];

const clearStyles = [
    {
        data: {
            type: "typography",
            key: "cbTypo",
            label: __("Typography", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "cbColor",
            label: __("Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "cbColorHover",
            label: __("Hover Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "cbBgColor",
            label: __(
                "Background Color",
                "ultimate-post"
            ),
        },
    },
    {
        data: {
            type: "color",
            key: "cbBgHoverColor",
            label: __(
                "Background Hover Color",
                "ultimate-post"
            ),
        },
    },
    {
        data: {
            type: "border",
            key: "cbBorder",
            label: __("Border", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "cbBorderRadius",
            label: __("Border Radius", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true
        },
    },
    {
        data: {
            type: "boxshadow",
            key: "cbBoxShadow",
            label: __("BoxShadow", "ultimate-post"),
        },
    },
    {
        data: {
            type: "range",
            key: "gap",
            min: 0,
            max: 500,
            step: 1,
            responsive: true,
            unit: ["px", "em", "rem"],
            label: __("Gap Between Items", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "cbPadding",
            label: __("Padding", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
];

const selectedStyles = [
    {
        data: {
            type: "typography",
            key: "sfTypo",
            label: __("Typography", "ultimate-post"),
        },
    },
    {
        data: {
            type: "range",
            key: "sfIconSize",
            min: 0,
            max: 120,
            step: 1,
            responsive: true,
            unit: ["px", "em", "rem"],
            label: __("Icon Size", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sfColor",
            label: __("Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sfColorHover",
            label: __("Hover Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sfBgColor",
            label: __(
                "Background Color",
                "ultimate-post"
            ),
        },
    },
    {
        data: {
            type: "color",
            key: "sfBgHoverColor",
            label: __(
                "Background Hover Color",
                "ultimate-post"
            ),
        },
    },
    {
        data: {
            type: "border",
            key: "sfBorder",
            label: __("Border", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "sfBorderRadius",
            label: __("Border Radius", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true
        },
    },
    {
        data: {
            type: "boxshadow",
            key: "sfBoxShadow",
            label: __("BoxShadow", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "sfPadding",
            label: __("Padding", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
];

const clearButtonSettings = () => {
    return [
        ...clearSettings,
        ...clearStyles
    ];
}

const toolbarSettingsConverter = (settings) => {
    return settings.map((setting) => setting.data);
};

const getToolbarSettings = () => {
    return [
        {
            isToolbar: true,
            data: {
                type: "tab_toolbar",
                content: [
                    {
                        name: "clear_button_set",
                        title: __("Clear Button Settings", "ultimate-post"),
                        options: toolbarSettingsConverter(
                            clearSettings
                        ),
                    },
                ],
            },
        },
    ];
};

const getToolbarStyle = () => {
    return [
        {
            isToolbar: true,
            data: {
                type: "tab_toolbar",
                content: [
                    {
                        name: "clear_button_sty",
                        title: __("Clear Button Styles", "ultimate-post"),
                        options: toolbarSettingsConverter(clearStyles),
                    },
                    {
                        name: "selected_filter_sty",
                        title: __("Selected Filter Styles", "ultimate-post"),
                        options: toolbarSettingsConverter(selectedStyles),
                    },
                ],
            },
        },
    ];
};

export default function SidebarSettings({
    attributes,
    setAttributes,
    name,
    clientId,
}) {
    const store = { setAttributes, name, attributes, clientId };

    return (
        <>
            <ToolBarElement
                store={store}
                include={[
                    {
                        type: "custom",
                        icon: icons.setting,
                        label: __("Clear Filter Settings", "ultimate-post"),
                    },
                ]}
            >
                <ToolbarSettings
                    store={store}
                    include={getToolbarSettings()}
                />
            </ToolBarElement>

            <ToolBarElement
                store={store}
                include={[
                    {
                        type: "custom",
                        icon: icons.styleIcon,
                        label: __("Clear Filter Styles", "ultimate-post"),
                    },
                ]}
            >
                <ToolbarSettings
                    store={store}
                    include={getToolbarStyle()}
                />
            </ToolBarElement>

            <InspectorControls>
                <Sections>
                    <Section slug="setting" title={__("Settings", "ultimate-post")}>
                        <CommonSettings
                            title={"inline"}
                            store={store}
                            include={clearSettings}
                        />
                    </Section>
                    <Section slug="style" title={__("Styles", "ultimate-post")}>
                        <CommonSettings
                            title={__("Clear Button Style", "ultimate-post")}
                            initialOpen={true}
                            store={store}
                            include={clearStyles}
                        />

                        <CommonSettings
                            title={__("Selected Filter Style", "ultimate-post")}
                            store={store}
                            include={selectedStyles}
                            initialOpen={true}
                        />
                    </Section>
                </Sections>
                { blockSupportLink() }
            </InspectorControls>
        </>
    );
}
