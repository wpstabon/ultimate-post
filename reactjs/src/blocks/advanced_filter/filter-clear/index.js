const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from "./Edit";
import UltpLinkGenerator from "../../../helper/UltpLinkGenerator";
const docsUrl = UltpLinkGenerator(
    "https://wpxpo.com/docs/postx/postx-features/advanced-post-filter/",
    "block_docs"
);
import attributes from "./attributes";

const clearIcon   = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/clear-filter.svg'}/>;

registerBlockType("ultimate-post/filter-clear", {
    apiVersion: 2,
    title: __("Clear Filter", "ultimate-post"),
    icon: clearIcon,
    category: "ultimate-post",
    description: (
        <span className="ultp-block-info">
            {__("Clears all the selected filter", "ultimate-post")}
            <a target="_blank" href={docsUrl}>
                {__("Documentation", "ultimate-post")}
            </a>
        </span>
    ),
    keywords: [
        __("Filter", "ultimate-post"),
        __("Post Filter", "ultimate-post"),
    ],
    supports: {},
    usesContext: ["post-grid-parent/postBlockClientId", "advanced-filter/cId"],
    edit: Edit,
    ancestor: ["ultimate-post/advanced-filter"],
    attributes,
});
