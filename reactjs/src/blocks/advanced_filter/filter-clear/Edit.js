const { __ } = wp.i18n;
import { CssGenerator } from "../../../helper/CssGenerator";
import { isInlineCSS } from "../../../helper/CommonPanel";
import SidebarSettings from "./SidebarSettings";
import ClearButton from "./ClearButton";
import {
    clearFilter,
    getAllInnerBlocks,
    getSelectedFilters,
} from "../../../helper/gridFunctions";
import SelectedFilter from "./SelectedFilter";
const { useEffect } = wp.element;
const { useBlockProps } = wp.blockEditor;
const { useSelect } = wp.data;
const {selectBlock} = wp.data.dispatch("core/block-editor");

const name = "ultimate-post/filter-clear";

export default function Edit({ attributes, setAttributes, clientId, context, isSelected }) {
    const { blockId } = attributes;

    const filterParent = context["advanced-filter/cId"];

    const blocks = useSelect(
        (select) => {
            const parentBlocks =
                select("core/block-editor").getBlocks(filterParent);
            const allBlocks = getAllInnerBlocks(parentBlocks);
            return allBlocks.filter(
                (block) => block.name === "ultimate-post/filter-select"
            );
        },
        [filterParent]
    );

    const selectedFilterLabels = getSelectedFilters(blocks) || [];

    useEffect(() => {
        setAttributes({
            blockId: clientId.slice(-6),
        });
    }, [clientId]);

    const blockProps = useBlockProps({
        className: `ultp-block-${blockId} ultp-filter-clear`,
    });

    let __preview_css;
    if (blockId) {
        __preview_css = CssGenerator(attributes, name, blockId, isInlineCSS());
    }

    const content = () => {
        return (
            <>
                {selectedFilterLabels.map((val, idx) => {
                    return Object.keys(val).map((clientId) => {
                        return (
                            <div {...blockProps} key={clientId}>
                                <SelectedFilter
                                    text={val[clientId]}
                                    clientId={clientId}
                                    onChange={(cId) => clearFilter(cId)}
                                />
                            </div>
                        );
                    });
                })}

                <div {...blockProps}>
                    <ClearButton
                        attributes={attributes}
                        onChange={() => {
                            selectedFilterLabels.forEach((label) => {
                                Object.keys(label).forEach((clientId) => {
                                    clearFilter(clientId);
                                });
                            });
                            selectBlock(clientId);
                        }}
                    />
                </div>
            </>
        );
    };

    return (
        <>
            <SidebarSettings
                name={name}
                attributes={attributes}
                setAttributes={setAttributes}
                clientId={clientId}
            />

            {__preview_css && (
                <style
                    dangerouslySetInnerHTML={{ __html: __preview_css }}
                ></style>
            )}

            <div className={`ultp-block-${blockId}-wrapper`}>
                {content()}
            </div>
        </>
    );
}
