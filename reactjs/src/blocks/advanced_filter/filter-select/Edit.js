const { __ } = wp.i18n;
import { isInlineCSS } from '../../../helper/CommonPanel';
import { CssGenerator } from '../../../helper/CssGenerator';
import { getOptions, isCompatibleBlock } from '../../../helper/gridFunctions';
import Button from './Button';
import Select from './Select';
import Settings from './Settings';
const { useEffect, useMemo, useState } = wp.element;
const { useSelect, useDispatch } = wp.data;

export default function Edit({
	attributes,
	setAttributes,
	clientId,
	context,
	isSelected,
}) {
	const {
		blockId,
		type,
		allText,
		filterStyle,
		filterValues,
		selectedValue,
		selectedLabel,
		dropdownOptionsType,
		dropdownOptions,
	} = attributes;

	useEffect(() => {
		setAttributes({
			blockId: clientId.slice(-6),
		});
	}, [clientId]);

	const postBlockClientId = context['post-grid-parent/postBlockClientId'];

	const postTypes = useSelect(
		(select) => {
			if (postBlockClientId) {
				const childBlocks =
					select('core/block-editor').getBlocks(postBlockClientId);
				const blocks = childBlocks.filter((child) =>
					isCompatibleBlock(child.name)
				);

				return JSON.stringify(
					blocks
						.filter((block) => block.attributes.queryType)
						.map((block) => block.attributes.queryType)
				);
			}

			return [];
		},
		[postBlockClientId]
	);

	const [taxOptions, setTaxOptions] = useState(new Map());
	const [taxStatus, setTaxStatus] = useState('loading');

	useEffect(() => {
		if (type !== 'custom_tax' || !postTypes) {
			return;
		}

		setTaxStatus('loading');

		wp.apiFetch({
			path: 'ultp/v2/custom_tax',
			method: 'POST',
			data: {
				postTypes: JSON.parse(postTypes),
			},
		})
			.then((response) => {
				if (response.length === 0) {
					setTaxStatus('none');
					return;
				}

				const newTaxOptions = new Map();
				response.forEach((tax) => {
					newTaxOptions.set(tax.id, {
						label: tax.name,
					});
				});
				setAttributes({
					postTypes: postTypes,
				});
				setTaxOptions(newTaxOptions);
				setTaxStatus('success');
			})
			.catch((error) => {
				console.log(error);
				setTaxStatus('error');
			});
	}, [postTypes]);

	const [status, options] = getOptions(type, allText, attributes);

	useEffect(() => {
		setAttributes({
			selectedValue: '_all',
			selectedLabel: '',
		});
	}, []);

	let inlineOptions = useMemo(() => {
		if (
			filterStyle === 'inline' ||
			(filterStyle === 'dropdown' && dropdownOptionsType === 'specific')
		) {
			return Array.from(
				(type === 'custom_tax'
					? taxStatus === 'success'
						? taxOptions
						: new Map()
					: status === 'success'
						? options
						: new Map()
				).entries()
			).map(([key, value]) => {
				return { value: key, label: value.label };
			});
		}

		return [];
	}, [
		taxStatus,
		filterStyle,
		dropdownOptionsType,
		status,
		taxOptions,
		options,
	]);

	const selectOptions = useMemo(() => {
		if (filterStyle === 'dropdown' && dropdownOptionsType === 'specific') {
			const opts = JSON.parse(dropdownOptions);

			const filteredOpts = new Map();

			const currOptions =
				type === 'custom_tax'
					? taxStatus === 'success'
						? taxOptions
						: new Map()
					: status === 'success'
						? options
						: new Map();

			opts.forEach((value) => {
				if (currOptions.has(value)) {
					const opt = currOptions.get(value);
					filteredOpts.set(value, { ...opt });
				}
			});

			return filteredOpts;
		}

		return type === 'custom_tax' ? taxOptions : options;
	}, [
		dropdownOptionsType,
		dropdownOptions,
		options,
		taxOptions,
		type,
		taxStatus,
		status,
		filterStyle,
	]);

	const inlineValues = useMemo(() => {
		return JSON.parse(filterValues);
	}, [filterValues]);

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(
			attributes,
			'ultimate-post/filter-select',
			blockId,
			isInlineCSS()
		);
	}

	return (
		<>
			<Settings
				name={'ultimate-post/filter-select'}
				attributes={attributes}
				setAttributes={setAttributes}
				clientId={clientId}
				inlineOptions={inlineOptions}
			/>

			{__preview_css && (
				<style
					dangerouslySetInnerHTML={{ __html: __preview_css }}
				></style>
			)}

			{filterStyle === 'dropdown' && (
				<Select
					name={type}
					options={selectOptions}
					status={type === 'custom_tax' ? taxStatus : status}
					blockId={blockId}
					clientId={clientId}
					value={selectedValue}
					openDropdown={isSelected}
					onChange={(value, label) => {
						setAttributes({
							selectedValue: String(value),
							selectedLabel: label,
						});
					}}
					isSearchEnabled={attributes.searchEnabled}
					searchPlaceholder={attributes.sPlaceholderText}
				/>
			)}

			{filterStyle === 'inline' && (
				<div className={`ultp-block-${blockId}-wrapper`}>
					{inlineValues.map((val) => {
						let text = inlineOptions
							.filter((item) => item.value === val)
							.map((item) => item.label);

						if (text.length === 0) {
							if (inlineOptions[0] && inlineOptions[0].value) {
								setAttributes({
									filterValues: JSON.stringify([
										inlineOptions[0].value,
									]),
								});
							}
						}

						return (
							<Button
								key={val}
								text={text[0] || 'Select a Value'}
								status={
									type === 'custom_tax' ? taxStatus : status
								}
								blockId={blockId}
							/>
						);
					})}
				</div>
			)}
		</>
	);
}
