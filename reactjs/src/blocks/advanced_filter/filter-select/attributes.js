const attributes = {
	blockId: { type: 'string', default: '' },
	previewImg: { type: 'string', default: '' },
	type: { type: 'string', default: '' },
	dropdownOptionsType: { type: 'string', default: 'all' },
	dropdownOptions: { type: 'string', default: '["_all"]' },
	filterStyle: { type: 'string', default: 'dropdown' },
	filterValues: { type: 'string', default: '["_all"]' },
	allText: { type: 'string', default: 'All' },
	selectedValue: { type: 'string', default: '_all' },
	selectedLabel: { type: 'string', default: '' },
	postTypes: { type: 'string', default: '[]' },
	searchEnabled: { type: 'boolean', default: false },

	queryOrder: {
		type: 'string',
		default: 'desc',
	},

	align: {
		type: 'object',
		default: {
			lg: 'margin-left:0px !important;margin-right:0px !important',
		},
		style: [
			{
				selector:
					'{{ULTP}}.ultp-filter-select { {{align}} ; } {{ULTP}}-wrapper { {{align}}; display:flex; flex-wrap:wrap; }',
			},
		],
	},

	iGap: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				// depends:[{key:"type", condition:"==", value:"inline"}],
				selector: '{{ULTP}}-wrapper  { gap: {{iGap}}; }',
			},
		],
	},

	// Field Style
	// ----------------
	pTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '400',
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent-inner, {{ULTP}}.ultp-filter-button',
			},
		],
	},

	pIconSize: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'filterStyle', condition: '==', value: 'dropdown' },
				],
				selector:
					'{{ULTP}} .ultp-filter-select-field-icon svg { width: {{pIconSize}}; height:{{pIconSize}} }',
			},
		],
	},

	pColor: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_1_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent-inner,{{ULTP}}.ultp-filter-button { color:{{pColor}}; } {{ULTP}} .ultp-filter-select-field-icon { fill:{{pColor}}; } ',
			},
		],
	},

	pColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Over_Primary_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent:hover .ultp-filter-select__parent-inner { color:{{pColorHover}}; } {{ULTP}} .ultp-filter-select__parent:hover .ultp-filter-select-field-icon svg { fill:{{pColorHover}}; } {{ULTP}}.ultp-filter-button:hover{ color:{{pColorHover}}; } {{ULTP}}.ultp-filter-button-active{ color:{{pColorHover}}; }',
			},
		],
	},

	pBgColor: {
		type: 'string',
		default: 'var(--postx_preset_Base_2_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent, {{ULTP}}.ultp-filter-button { background-color:{{pBgColor}}; } ',
			},
		],
	},

	pBgColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Primary_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent:hover,{{ULTP}}.ultp-filter-button:hover { background-color:{{pBgColorHover}}; } {{ULTP}}.ultp-filter-button-active { background-color:{{pBgColorHover}}; }',
			},
		],
	},

	pBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: 'var(--postx_preset_Contrast_1_color)',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent, {{ULTP}}.ultp-filter-button',
			},
		],
	},

	pBorderRadius: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent,{{ULTP}}.ultp-filter-button { border-radius:{{pBorderRadius}}; }',
			},
		],
	},

	pBoxShadow: {
		type: 'object',
		default: {
			openShadow: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: 'var(--postx_preset_Secondary_color)',
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent, {{ULTP}}.ultp-filter-button',
			},
		],
	},

	pPadding: {
		type: 'object',
		default: {
			lg: { top: '8', bottom: '8', left: '14', right: '14', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__parent, {{ULTP}}.ultp-filter-button { padding:{{pPadding}}; }',
			},
		],
	},

	// Dropdown
	// ----------------

	dAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown-inner{ text-align:{{dAlign}}; }',
			},
		],
	},

	dWidthType: {
		type: 'string',
		default: 'auto',
	},

	dWidth: {
		type: 'object',
		default: {lg: "200", unit: 'px'},
        style: [
			{
				depends: [
					{ key: 'dWidthType', condition: '==', value: 'fixed' },
				],
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown { width: {{dWidth}}; }',
			},
        ]
	},

	dTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '400',
		},
		style: [{ selector: '{{ULTP}} .ultp-filter-select__dropdown-inner' }],
	},

	dColor: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_1_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown-inner { color:{{dColor}}; }  {{ULTP}} .ultp-filter-select-options::-webkit-scrollbar-thumb{ background: {{dColor}}; }',
			},
		],
	},

	dColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Over_Primary_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown-inner:hover { color:{{dColorHover}}; }',
			},
		],
	},

	dBgColor: {
		type: 'string',
		default: 'var(--postx_preset_Base_2_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown { background-color:{{dBgColor}}; } ',
			},
		],
	},

	dBgColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Primary_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown-inner:hover { background-color:{{dBgColorHover}}; } ',
			},
		],
	},

	dBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: 'var(--postx_preset_Contrast_1_color)',
			type: 'solid',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select__dropdown',
			},
		],
	},

	dBorderRadius: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown { border-radius:{{dBorderRadius}}; }',
			},
		],
	},

	dBoxShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: 'var(--postx_preset_Secondary_color)',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select__dropdown',
			},
		],
	},

	dSpace: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown-inner { margin-bottom: {{dSpace}}; }',
			},
		],
	},

	dPadding: {
		type: 'object',
		default: {
			lg: { top: '5', bottom: '5', left: '5', right: '5', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select__dropdown { padding:{{dPadding}}; }',
			},
		],
	},

	// Search Input
	sTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '400',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select-search',
			},
		],
	},

	sPlaceholderText: {
		type: 'string',
		default: 'Search...',
	},

	sColor: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_1_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search { color: {{sColor}}; } }',
			},
		],
	},

	sColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_1_color)',
		style: [
			{
				selector: `
                    {{ULTP}} .ultp-filter-select-search:hover,
                    {{ULTP}} .ultp-filter-select-search:focus { 
                        color:{{sColorHover}}; 
                    }`,
			},
		],
	},

	sPlaceholderColor: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_3_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search::placeholder { color: {{sPlaceholderColor}}; } }',
			},
		],
	},

	sPlaceholderColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Contrast_3_color)',
		style: [
			{
				selector: `
                    {{ULTP}} .ultp-filter-select-search::placeholder:hover {
                        color:{{sPlaceholderColorHover}}; 
                    }`,
			},
			{
				selector: `
                    {{ULTP}} .ultp-filter-select-search::placeholder:focus { 
                        color:{{sPlaceholderColorHover}}; 
                    }`,
			},
		],
	},

	sBgColor: {
		type: 'string',
		default: 'var(--postx_preset_Base_2_color)',
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search { background-color:{{sBgColor}}; } ',
			},
		],
	},

	sBgColorHover: {
		type: 'string',
		default: 'var(--postx_preset_Base_2_color)',
		style: [
			{
				selector: `
                    {{ULTP}} .ultp-filter-select-search:hover,
                    {{ULTP}} .ultp-filter-select-search:focus { 
                        background-color: {{sBgColorHover}}; 
                    }`,
			},
		],
	},

	sBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: 'var(--postx_preset_Contrast_1_color)',
			type: 'solid',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select-search',
			},
		],
	},

	sBorderHover: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: 'var(--postx_preset_Primary_color)',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search:hover, {{ULTP}} .ultp-filter-select-search:focus',
			},
		],
	},

	sBorderRadius: {
		type: 'object',
		default: {
			lg: { top: '2', bottom: '2', left: '2', right: '2', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search { border-radius: {{sBorderRadius}}; }',
			},
		],
	},

	sBorderRadiusHover: {
		type: 'object',
		default: {
			lg: { top: '2', bottom: '2', left: '2', right: '2', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search:focus { border-radius: {{sBorderRadiusHover}}; }',
			},
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search:hover { border-radius: {{sBorderRadiusHover}}; }',
			},
		],
	},

	sBoxShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: 'var(--postx_preset_Primary_color)',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select-search',
			},
		],
	},

	sBoxShadowHover: {
		type: 'object',
		default: {
			openShadow: 1,
			width: { top: 0, right: 0, bottom: 0, left: 1 },
			color: 'var(--postx_preset_Primary_color)',
		},
		style: [
			{
				selector: '{{ULTP}} .ultp-filter-select-search:focus',
			},
		],
	},

	sPadding: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '8', right: '8', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search { padding:{{sPadding}}; }',
			},
		],
	},

	sMargin: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '5', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{ULTP}} .ultp-filter-select-search { margin:{{sMargin}}; }',
			},
		],
	},
};

export default attributes;
