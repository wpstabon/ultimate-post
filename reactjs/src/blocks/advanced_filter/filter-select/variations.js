const { __ } = wp.i18n;

const authorIcon  = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/author-filter.svg'}/>;
const tagsIcon    = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/tags-filter.svg'}/>;
export const catIcon     = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/category-filter.svg'}/>;
const oByIcon     = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/order-by-filter.svg'}/>;
const oIcon       = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/order-filter.svg'}/>;

const advSortIcon = (
    <div className={`ultp-block-inserter-icon-section`}>
        <img src={ultp_data.url + 'assets/img/blocks/adv-filter/advanced-sort-filter.svg'}/>
        {!ultp_data.active &&
            <span className={`ultp-pro-block`}>Pro</span>
        }
    </div>
);

const taxIcon = (
    <div className={`ultp-block-inserter-icon-section`}>
        <img src={ultp_data.url + 'assets/img/blocks/adv-filter/taxonomy-sort-filter.svg'}/>
        {!ultp_data.active &&
            <span className={`ultp-pro-block`}>Pro</span>
        }
    </div>
);

export const variations = [
    {
        name: "tags",
        icon: tagsIcon,
        title: __("Tags Filter", "ultimate-post"),
        description: __("Filter posts by Tags", "ultimate-post"),
        attributes: {
            type: "tags",
            allText: "All Tags",
        },
        isActive: ["type"],
    },
    {
        name: "category",
        icon: catIcon,
        title: __("Category Filter", "ultimate-post"),
        description: __("Filter posts by Category", "ultimate-post"),
        isDefault: true,
        attributes: {
            type: "category",
            allText: "All Categories",
        },
        isActive: ["type"],
    },
    {
        name: "author",
        title: __("Author Filter", "ultimate-post"),
        icon: authorIcon,
        description: __("Filter posts by Author", "ultimate-post"),
        attributes: {
            type: "author",
            allText: "All Authors",
        },
        isActive: ["type"],
    },
    {
        name: "order",
        icon: oIcon,
        title: __("Order Filter (Asc/Desc)", "ultimate-post"),
        description: __("Orders posts", "ultimate-post"),
        attributes: {
            type: "order",
        },
        isActive: ["type"],
    },
    {
        name: "order_by",
        icon: oByIcon,
        title: __("Order By Filter", "ultimate-post"),
        description: __("Orders posts by a Attribute", "ultimate-post"),
        attributes: {
            type: "order_by",
        },
        isActive: ["type"],
    },
    {
        name: "adv_sort",
        icon: advSortIcon,
        title: __("Advanced Sort Filter", "ultimate-post"),
        description: __("Orders posts (Advanced)", "ultimate-post"),
        attributes: {
            type: "adv_sort",
            allText: "Advanced Sort",
        },
        isActive: ["type"],
    },
    {
        name: "custom_tax",
        icon: taxIcon,
        title: __("Custom Taxonomy", "ultimate-post"),
        description: __("Filter posts by Custom Taxonomy", "ultimate-post"),
        attributes: {
            type: "custom_tax",
        },
        isActive: ["type"],
    },
];

export const filterSelectVariations = variations.map(v => "filter-select/" + v.name);