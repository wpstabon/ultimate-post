import {
	blockSupportLink,
	CommonSettings,
	ToolbarSettings,
} from '../../../helper/CommonPanel';
import icons from '../../../helper/icons';
import { Section, Sections } from '../../../helper/Sections';
import ToolBarElement from '../../../helper/ToolBarElement';

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;

const alignmentOptions = [
	{
		icon: <span className="dashicons dashicons-editor-alignleft"></span>,
		value: 'margin-right: auto !important;margin-left:0px !important',
		label: __('Left', 'ultimate-post'),
	},
	{
		icon: <span className="dashicons dashicons-editor-justify"></span>,
		value: 'margin-left:0px !important;margin-right:0px !important',
		label: __('Normal', 'ultimate-post'),
	},
	{
		icon: <span className="dashicons dashicons-editor-alignright"></span>,
		value: 'margin-left: auto !important;margin-right:0px !important',
		label: __('Right', 'ultimate-post'),
	},
];

const getGeneralSettings = (attributes, inlineOptions) => {
	const { filterStyle, type, dropdownOptionsType } = attributes;

	const showSpecific =
		filterStyle === 'dropdown' &&
		['category', 'tags', 'author', 'custom_tax'].includes(type);

	const showSpecificSelect =
		showSpecific && dropdownOptionsType === 'specific';

	const hasSearchCapability = [
		'category',
		'tags',
		'author',
		'custom_tax',
	].includes(type);

	const settings = [
		{
			data: {
				type: 'alignment',
				key: 'align',
				responsive: true,
				label: __('Alignment', 'ultimate-post'),
				options: alignmentOptions.map((item) => item.value),
				icons: ['left', 'flow', 'right'],
			},
		},
		{
			data: {
				type: 'tag',
				key: 'filterStyle',
				label: __('Filter Style', 'ultimate-post'),
				options: [
					{
						value: 'dropdown',
						label: 'Dropdown',
					},
					{ value: 'inline', label: 'Inline' },
				],
			},
		},

		{
			data: {
				disableIf: (attr) => attr['filterStyle'] !== 'inline',
				type: 'select',
				key: 'filterValues',
				multiple: true,
				label: __('Filter Values', 'ultimate-post'),
				options: inlineOptions,
			},
		},

		{
			data: showSpecific
				? {
						type: 'tag',
						key: 'dropdownOptionsType',
						label: __('Available Options', 'ultimate-post'),
						options: [
							{
								value: 'all',
								label: __('All', 'ultimate-post'),
							},
							{
								value: 'specific',
								label: __('Specific', 'ultimate-post'),
							},
						],
					}
				: {},
		},

		{
			data: showSpecificSelect
				? {
						type: 'select',
						key: 'dropdownOptions',
						multiple: true,
						label: __('Select Specific Options', 'ultimate-post'),
						options: inlineOptions,
						help: __(
							'First selected option will be the default value.',
							'ultimate-post'
						),
					}
				: {},
		},

		{
			data: {
				type: 'text',
				key: 'allText',
				label: __('All Content Text', 'ultimate-post'),
			},
		},

		{
			data: hasSearchCapability
				? {
						type: 'toggle',
						key: 'searchEnabled',
						label: __(
							'Enable Searching in Dropdown',
							'ultimate-post'
						),
					}
				: {},
		},
	];

	return settings;
};

const getFieldSettings = (attributes) => {
	const { filterStyle } = attributes;
	return [
		{
			data: {
				type: 'typography',
				key: 'pTypo',
				label: __('Typography', 'ultimate-post'),
			},
		},
		{
			depend: filterStyle === 'dropdown',
			data: {
				type: 'range',
				key: 'pIconSize',
				min: 0,
				max: 120,
				step: 1,
				responsive: true,
				unit: ['px', 'em', 'rem'],
				label: __('Icon Size', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'pColor',
				label: __('Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'pColorHover',
				label: __('Hover Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'pBgColor',
				label: __('Background Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'pBgColorHover',
				label: __('Background Hover Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'border',
				key: 'pBorder',
				label: __('Border', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'dimension',
				key: 'pBorderRadius',
				label: __('Border Radius', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
		{
			data: {
				type: 'boxshadow',
				key: 'pBoxShadow',
				label: __('BoxShadow', 'ultimate-post'),
			},
		},
		{
			data: {
				disableIf: (attr) => attr['filterStyle'] !== 'inline',
				type: 'range',
				key: 'iGap',
				min: 0,
				max: 120,
				step: 1,
				responsive: true,
				unit: ['px', 'em', 'rem'],
				label: __('Gap Between Inline Items', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'dimension',
				key: 'pPadding',
				label: __('Padding', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
	];
};

const getDropdownSettings = () => {
	return [
		{
			data: {
				type: 'alignment',
				key: 'dAlign',
				responsive: true,
				label: __('Alignment', 'ultimate-post'),
				disableJustify: true,
			},
		},
		{
			data: {
				type: 'tag',
				key: 'dWidthType',
				label: __('Width', 'ultimate-post'),
				options: [
					{
						value: 'auto',
						label: __('Auto', 'ultimate-post'),
					},
					{ value: 'fixed', label: __('Fixed', 'ultimate-post') },
				],
			},
		},

		{
			data: {
				disableIf: (attr) => attr['dWidthType'] !== 'fixed',
				type: 'range',
				key: 'dWidth',
				min: 0,
				max: 500,
				step: 1,
				responsive: true,
				unit: ['px', 'em', 'rem'],
				label: __('Fixed Width', 'ultimate-post'),
			},
		},

		{
			data: {
				type: 'typography',
				key: 'dTypo',
				label: __('Typography', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'dColor',
				label: __('Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'dColorHover',
				label: __('Hover Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'dBgColor',
				label: __('Background Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'color',
				key: 'dBgColorHover',
				label: __('Background Hover Color', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'border',
				key: 'dBorder',
				label: __('Border', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'dimension',
				key: 'dBorderRadius',
				label: __('Border Radius', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
		{
			data: {
				type: 'boxshadow',
				key: 'dBoxShadow',
				label: __('BoxShadow', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'range',
				key: 'dSpace',
				min: 0,
				max: 120,
				step: 1,
				responsive: true,
				unit: ['px', 'em', 'rem'],
				label: __('Space between Menu', 'ultimate-post'),
			},
		},
		{
			data: {
				type: 'dimension',
				key: 'dPadding',
				label: __('Padding', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
	];
};

const getSearchSettings = (attributes) => {
	return [
		{
			data: {
				type: 'typography',
				key: 'sTypo',
				label: __('Typography', 'ultimate-post'),
			},
		},

		{
			data: {
				type: 'text',
				key: 'sPlaceholderText',
				label: __('Placeholder Text', 'ultimate-post'),
			},
		},

		{
			data: {
				type: 'tab',
				content: [
					{
						name: 'normal',
						title: __('Normal', 'ultimate-post'),
						options: [
							{
								type: 'color',
								key: 'sColor',
								label: __('Color', 'ultimate-post'),
							},
							{
								type: 'color',
								key: 'sBgColor',
								label: __('Background Color', 'ultimate-post'),
							},
							{
								type: 'color',
								key: 'sPlaceholderColor',
								label: __('Placeholder Color', 'ultimate-post'),
							},

							{
								type: 'border',
								key: 'sBorder',
								label: __('Border', 'ultimate-post'),
							},

							{
								type: 'dimension',
								key: 'sBorderRadius',
								label: __('Border Radius', 'ultimate-post'),
								step: 1,
								unit: true,
								responsive: true,
							},

							{
								type: 'boxshadow',
								key: 'sBoxShadow',
								label: __('Box Shadow', 'ultimate-post'),
							},
						],
					},
					{
						name: 'hover',
						title: __('Hover/Active', 'ultimate-post'),
						options: [
							{
								type: 'color',
								key: 'sColorHover',
								label: __('Color', 'ultimate-post'),
							},
							{
								type: 'color',
								key: 'sBgColorHover',
								label: __('Background Color', 'ultimate-post'),
							},
							{
								type: 'color',
								key: 'sPlaceholderColorHover',
								label: __('Placeholder Color', 'ultimate-post'),
							},

							{
								type: 'border',
								key: 'sBorderHover',
								label: __('Border', 'ultimate-post'),
							},

							{
								type: 'dimension',
								key: 'sBorderRadiusHover',
								label: __('Border Radius', 'ultimate-post'),
								step: 1,
								unit: true,
								responsive: true,
							},

							{
								type: 'boxshadow',
								key: 'sBoxShadowHover',
								label: __('Box Shadow', 'ultimate-post'),
							},
						],
					},
				],
			},
		},

		{
			data: {
				type: 'dimension',
				key: 'sPadding',
				label: __('Padding', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},

		{
			data: {
				type: 'dimension',
				key: 'sMargin',
				label: __('Margin', 'ultimate-post'),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
	];
};

const toolbarSettingsConverter = (settings) => {
	return settings.map((setting) => setting.data);
};

const getToolbarSettings = (attributes, inlineOptions) => {
	const content = [
		{
			name: 'general',
			title: __('Select Filter Settings', 'ultimate-post'),
			options: toolbarSettingsConverter(
				getGeneralSettings(attributes, inlineOptions)
			),
		},
	];

	return [
		{
			isToolbar: true,
			data: {
				type: 'tab_toolbar',
				content,
			},
		},
	];
};

const getToolbarStyle = (attributes, inlineOptions) => {
	const { filterStyle, type } = attributes;

	const content = [
		{
			name: 'field_style',
			title:
				filterStyle === 'dropdown'
					? __('Field Style', 'ultimate-post')
					: __('Style', 'ultimate-post'),
			options: toolbarSettingsConverter(getFieldSettings(attributes)),
		},
	];

	if (filterStyle === 'dropdown') {
		content.push({
			name: 'dropdown_style',
			title: __('Dropdown Style', 'ultimate-post'),
			options: toolbarSettingsConverter(getDropdownSettings()),
		});
	}

	// if (['category', 'tags', 'author', 'custom_tax'].includes(type)) {
	// 	content.push({
	// 		name: 'dropdown_search_style',
	// 		title: __('Search Input Style', 'ultimate-post'),
	// 		options: toolbarSettingsConverter(getSearchSettings(attributes)),
	// 	});
	// }

	return [
		{
			isToolbar: true,
			data: {
				type: 'tab_toolbar',
				content,
			},
		},
	];
};

export default function Settings({
	attributes,
	setAttributes,
	name,
	clientId,
	inlineOptions,
}) {
	const store = { setAttributes, name, attributes, clientId };
	const { filterStyle, type } = attributes;

	return (
		<>
			<ToolBarElement
				store={store}
				include={[
					{
						type: 'custom',
						icon: icons.setting,
						label: __('Select Filter Settings', 'ultimate-post'),
					},
				]}
			>
				<ToolbarSettings
					store={store}
					include={getToolbarSettings(attributes, inlineOptions)}
				/>
			</ToolBarElement>

			<ToolBarElement
				store={store}
				include={[
					{
						type: 'custom',
						icon: icons.styleIcon,
						label: __('Select Filter Style', 'ultimate-post'),
					},
				]}
			>
				<ToolbarSettings
					store={store}
					include={getToolbarStyle(attributes, inlineOptions)}
				/>
			</ToolBarElement>

			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={__('Settings', 'ultimate-post')}
					>
						<CommonSettings
							title={'inline'}
							store={store}
							include={getGeneralSettings(
								attributes,
								inlineOptions
							)}
						/>
					</Section>
					<Section slug="style" title={__('Styles', 'ultimate-post')}>
						<CommonSettings
							title={
								filterStyle === 'dropdown'
									? __('Field Style', 'ultimate-post')
									: 'inline'
							}
							store={store}
							include={getFieldSettings(attributes)}
							initialOpen
						/>

						{filterStyle === 'dropdown' && (
							<CommonSettings
								title={__('Dropdown Style', 'ultimate-post')}
								store={store}
								include={getDropdownSettings()}
							/>
						)}

						{['category', 'tags', 'author', 'custom_tax'].includes(
							type
						) && (
							<CommonSettings
								title={__(
									'Search Input Style',
									'ultimate-post'
								)}
								store={store}
								include={getSearchSettings(attributes)}
							/>
						)}
					</Section>
				</Sections>
				{blockSupportLink()}
			</InspectorControls>
		</>
	);
}
