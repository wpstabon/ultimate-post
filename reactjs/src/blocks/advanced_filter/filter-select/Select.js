const { useBlockProps } = wp.blockEditor;
const { useEffect } = wp.element;
import IconPack from '../../../helper/fields/tools/IconPack';
import { getTitleCase } from '../../../helper/gridFunctions';

/**
 * @typedef {Object} SelectProps
 * @property {string} name
 * @property {string} value
 * @property {Map} options
 * @property {string} status
 * @property {string} blockId
 * @property {string} clientId
 * @property {Function} onChange
 * @property {boolean} openDropdown
 * @property {boolean} [isSearchEnabled]
 * @property {string} [searchPlaceholder]
 */

/**
 * @param {SelectProps} props
 */
export default function Select({
	name,
	value,
	options,
	status,
	blockId,
	clientId,
	onChange,
	openDropdown,
	isSearchEnabled = false,
	searchPlaceholder = '',
}) {
	const shouldSetDefValue =
		!options.has(value) &&
		options.size > 0 &&
		status === 'success' &&
		value;

	useEffect(() => {
		if (shouldSetDefValue) {
			const _val = options.keys().next().value;
			onChange(_val, '');
		}
	}, [shouldSetDefValue, onChange, options]);

	const blockProps = useBlockProps({
		className: `ultp-block-${blockId} ultp-filter-select`,
	});

	return (
		<>
			<div {...blockProps} aria-expanded={openDropdown}>
				<div className="ultp-filter-select-field ultp-filter-select__parent">
					<span
						className="ultp-filter-select-field-selected ultp-filter-select__parent-inner"
						aria-label={name}
					>
						{status === 'loading' && 'Loading...'}
						{status === 'none' && 'None'}
						{status === 'error' && 'Error'}
						{status === 'success' &&
							(options.has(value)
								? options.get(value).label
								: '...')}
					</span>
					<span
						className={`ultp-filter-select-field-icon ${
							openDropdown ? 'ultp-dropdown-icon-rotate' : ''
						}`}
					>
						{IconPack['collapse_bottom_line']}
					</span>
				</div>

				{openDropdown && (
					<ul className="ultp-filter-select-options ultp-filter-select__dropdown">
						{isSearchEnabled && (
							<input
								className="ultp-filter-select-search"
								type="search"
								placeholder={searchPlaceholder}
							/>
						)}
						{status === 'success' &&
							Array.from(options.entries()).map(([key, val]) => (
								<li
									className={
										'ultp-filter-select__dropdown-inner ' +
										(val.disabled ? 'disabled' : '')
									}
									key={key}
									onClick={(e) => {
										if (val.disabled) return;
										const label = `${getTitleCase(name)}: ${
											val.label
										}`;
										onChange(
											key,
											key === '_all' ? '' : label
										);
									}}
								>
									{val.label}
								</li>
							))}
					</ul>
				)}
			</div>
		</>
	);
}
