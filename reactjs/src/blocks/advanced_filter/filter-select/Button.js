const { useBlockProps } = wp.blockEditor;
const { useState } = wp.element;

/**
 * @typedef {Object} SelectProps
 * @property {string} text
 * @property {string} blockId
 * @property {'success' | 'loading' | 'error' | 'none'} status
 */

/**
 * @param {SelectProps} props
 */
export default function Button({ text, blockId, status }) {
    const [isActive, setIsActive] = useState(false);

    const blockProps = useBlockProps({
        className: `ultp-block-${blockId} ultp-filter-button ${isActive ? "ultp-filter-button-active" : ""}`,
        role: "button"
    });

    return (
        <div {...blockProps} onClick={() => setIsActive(prev => !prev)}>
            {status === "loading" && "Loading..."}
            {status === "none" && "None"}
            {status === "error" && "Error"}
            {status === "success" && text}
        </div>
    );
}
