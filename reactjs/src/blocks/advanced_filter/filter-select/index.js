const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from "./Edit";
import attributes from "./attributes";
import UltpLinkGenerator from "../../../helper/UltpLinkGenerator";
const docsUrl = UltpLinkGenerator(
    "https://wpxpo.com/docs/postx/postx-features/advanced-post-filter/",
    "block_docs"
);
import { catIcon, variations } from "./variations";

registerBlockType("ultimate-post/filter-select", {
    apiVersion: 2,
    title: __("Select Filter", "ultimate-post"),
    icon: catIcon,
    category: "ultimate-post",
    description: (
        <span className="ultp-block-info">
            {__("Post Block from PostX", "ultimate-post")}
            <a target="_blank" href={docsUrl}>
                {__("Documentation", "ultimate-post")}
            </a>
        </span>
    ),
    keywords: [__("Post Filter", "ultimate-post")],
    attributes,
    variations,
    isDefault: false,
    usesContext: ["post-grid-parent/postBlockClientId"],
    ancestor: ["ultimate-post/advanced-filter"],
    supports: {},
    edit: Edit,
});
