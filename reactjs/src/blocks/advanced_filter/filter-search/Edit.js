const { __ } = wp.i18n;
import { CssGenerator } from "../../../helper/CssGenerator";
import { isInlineCSS } from "../../../helper/CommonPanel";
import SidebarSettings from "./SidebarSettings";
import IconPack from "../../../helper/fields/tools/IconPack";
const { useEffect } = wp.element;
const { useBlockProps } = wp.blockEditor;

export default function Edit({ attributes, setAttributes, clientId, context }) {
    const { blockId } = attributes;

    useEffect(() => {
        setAttributes({
            blockId: clientId.slice(-6),
        });
    }, [clientId]);

    const blockProps = useBlockProps({
        className: `ultp-block-${blockId} ultp-filter-search`,
    });

    let __preview_css = "";
    if (blockId) {
        __preview_css = CssGenerator(
            attributes,
            "ultimate-post/filter-search-adv",
            blockId,
            isInlineCSS()
        );
    }

    return (
        <>
            <SidebarSettings
                name={"ultimate-post/filter-search-adv"}
                attributes={attributes}
                setAttributes={setAttributes}
                clientId={clientId}
            />

            {__preview_css && (
                <style
                    dangerouslySetInnerHTML={{ __html: __preview_css }}
                ></style>
            )}

            <div {...blockProps}>
                <div className="ultp-filter-search-input">
                    <input
                        aria-label="search"
                        type="search"
                        placeholder={attributes.placeholder}
                    />
                    <span className="ultp-filter-search-input-icon">
                        {IconPack["search_line"]}
                    </span>
                </div>
            </div>
        </>
    );
}
