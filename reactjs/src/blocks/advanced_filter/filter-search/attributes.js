const attributes = {
    blockId: { type: "string", default: "" },
    previewImg: { type: "string", default: "" },
    placeholder: { type: "string", default: "Search..." },

    searchAlign: {
        type: "object",
        default: {
            lg: "margin-left:0px !important;margin-right:0px !important",
        },
        style: [
            {
                selector: "{{ULTP}} { {{searchAlign}} ; }",
            },
        ],
    },

    sWidthType: {
        type: "string",
        default: "auto",
        style: [
            {
                selector: "{{ULTP}} { width: {{sWidthType}} ; }",
            },
        ],
    },

    sWidth: {
        type: "object",
        default: { lg: "200", unit: "px" },
        style: [
          {
            depends: [{ key: "sWidthType", condition: "==", value: "fit-content" }],
            selector:
            "{{ULTP}} .ultp-filter-search-input input {width:{{sWidth}} !important;}",
          },
        ],
      },

    sTypo: {
        type: "object",
        default: {
            openTypography: 1,
            size: { lg: "14", unit: "px" },
            height: { lg: "20", unit: "px" },
            decoration: "none",
            transform: "",
            family: "",
            weight: "400",
        },
        style: [{ selector: "{{ULTP}} .ultp-filter-search-input input" }],
    },

    sIconSize: {
        type: "object",
        default: { lg: "10", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input-icon svg { width: {{sIconSize}}; height:{{sIconSize}} }",
            },
        ],
    },

    sColor: {
        type: "string",
        default: "var(--postx_preset_Contrast_1_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input input, {{ULTP}} .ultp-filter-search-input input::placeholder { color:{{sColor}}; } {{ULTP}} .ultp-filter-search-input-icon svg { fill:{{sColor}}; } ",
            },
        ],
    },

    sColorHover: {
        type: "string",
        default: "var(--postx_preset_Contrast_1_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input:hover input { color:{{sColorHover}}; } {{ULTP}} .ultp-filter-search-input:hover .ultp-filter-search-input-icon svg { fill:{{sColorHover}}; } ",
            },
        ],
    },

    sBgColor: {
        type: "string",
        default: "var(--postx_preset_Base_1_color)",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input { background-color:{{sBgColor}}; } ",
            },
        ],
    },

    sBorder: {
        type: "object",
        default: {
            openBorder: 1,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "var(--postx_preset_Contrast_1_color)",
            type: "solid",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-filter-search-input",
            },
        ],
    },

    sBorderRadius: {
        type: "object",
        default: { lg: "2", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input { border-radius:{{sBorderRadius}}; }",
            },
        ],
    },

    sBoxShadow: {
        type: "object",
        default: {
            openShadow: 0,
            width: { top: 0, right: 0, bottom: 0, left: 0 },
            color: "var(--postx_preset_Secondary_color)",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-filter-search-input",
            },
        ],
    },

    sPadding: {
        type: "object",
        default: {
            lg: { top: "3", bottom: "3", left: "4", right: "14", unit: "px" },
        },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-filter-search-input { padding:{{sPadding}}; }",
            },
        ],
    },
};

export default attributes;
