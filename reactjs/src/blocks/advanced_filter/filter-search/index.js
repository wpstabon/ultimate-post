const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from "./Edit";
import UltpLinkGenerator from "../../../helper/UltpLinkGenerator";
const docsUrl = UltpLinkGenerator(
    "https://wpxpo.com/docs/postx/all-blocks/adv-filter/",
    "block_docs"
);
import attributes from "./attributes";
const searchIcon = (
    <div className={`ultp-block-inserter-icon-section`}>
        <img src={ultp_data.url + 'assets/img/blocks/adv-filter/search-filter.svg'}/>
        {!ultp_data.active &&
            <span className={`ultp-pro-block`}>Pro</span>
        }
    </div>
);

registerBlockType("ultimate-post/filter-search-adv", {
    apiVersion: 2,
    title: __("Search Filter", "ultimate-post"),
    icon: searchIcon,
    category: "ultimate-post",
    description: (
        <span className="ultp-block-info">
            {__("Search Filter", "ultimate-post")}
            <a target="_blank" href={docsUrl}>
                {__("Documentation", "ultimate-post")}
            </a>
        </span>
    ),
    keywords: [
        __("Filter", "ultimate-post"),
        __("Post Filter", "ultimate-post"),
    ],
    supports: {},
    usesContext: ["post-grid-parent/postBlockClientId"],
    edit: Edit,
    ancestor: ["ultimate-post/advanced-filter"],
    attributes,
});
