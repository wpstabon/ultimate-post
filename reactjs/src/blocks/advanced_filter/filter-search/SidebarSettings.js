import {
    blockSupportLink,
    CommonSettings,
    ToolbarSettings,
} from "../../../helper/CommonPanel";
import icons from "../../../helper/icons";
import { Section, Sections, SectionsNoTab } from "../../../helper/Sections";
import ToolBarElement from "../../../helper/ToolBarElement";

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;

const alignmentOptions = [
    {
        icon: <span className="dashicons dashicons-editor-alignleft"></span>,
        value: "margin-right: auto !important;margin-left:0px !important",
        label: __("Left", "ultimate-post"),
    },
    {
        icon: <span className="dashicons dashicons-editor-justify"></span>,
        value: "margin-left:0px !important;margin-right:0px !important",
        label: __("Normal", "ultimate-post"),
    },
    {
        icon: <span className="dashicons dashicons-editor-alignright"></span>,
        value: "margin-left: auto !important;margin-right:0px !important",
        label: __("Right", "ultimate-post"),
    },
];

const settings = [
    {
        data: {
            type: "alignment",
            key: "searchAlign",
            responsive: true,
            label: __("Alignment", "ultimate-post"),
            options: alignmentOptions.map((item) => item.value),
            icons: ["left", "flow", "right"],
        },
    },
    {
        data: {
            type: "tag",
            key: "sWidthType",
            label: __("Input Width", "ultimate-post"),
            options: [
                {
                    value: "auto",
                    label: __("Auto", "ultimate-post"),
                },
                {
                    value: "fit-content",
                    label: __("Fixed", "ultimate-post"),
                },
                {
                    value: "100%",
                    label: __("Full", "ultimate-post"),
                },
            ],
        },
    },
    {
        data: {
            type: "range",
            key: "sWidth",
            min: 0,
            max: 500,
            step: 1,
            responsive: true,
            unit: ["px", "em", "rem"],
            label: __("Max Width", "ultimate-post"),
        },
    },
    {
        data: {
            type: "text",
            key: "placeholder",
            label: __("Placeholder Text", "ultimate-post"),
        },
    },
];

const style = [
    {
        data: {
            type: "typography",
            key: "sTypo",
            label: __("Typography", "ultimate-post"),
        },
    },
    {
        data: {
            type: "range",
            key: "sIconSize",
            min: 0,
            max: 120,
            step: 1,
            responsive: true,
            unit: ["px", "em", "rem"],
            label: __("Icon Size", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sColor",
            label: __("Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sColorHover",
            label: __("Hover Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "color",
            key: "sBgColor",
            label: __("Background Color", "ultimate-post"),
        },
    },
    {
        data: {
            type: "border",
            key: "sBorder",
            label: __("Border", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "sBorderRadius",
            label: __("Border Radius", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true
        },
    },
    {
        data: {
            type: "boxshadow",
            key: "sBoxShadow",
            label: __("BoxShadow", "ultimate-post"),
        },
    },
    {
        data: {
            type: "dimension",
            key: "sPadding",
            label: __("Padding", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
];

const allSettings =  [
    ...settings,
    ...style,
];

const toolbarSettingsConverter = (settings) => {
    return settings.map((setting) => setting.data);
};

const getToolbarSettings = () => {
    return [
        {
            isToolbar: true,
            data: {
                type: "tab_toolbar",
                content: [
                    {
                        name: "settings",
                        title: __("Search Filter Settings", "ultimate-post"),
                        options: toolbarSettingsConverter(settings),
                    },
                ],
            },
        },
    ];
};

const getToolbarStyle = () => {
    return [
        {
            isToolbar: true,
            data: {
                type: "tab_toolbar",
                content: [
                    {
                        name: "style",
                        title: __("Search Filter Style", "ultimate-post"),
                        options: toolbarSettingsConverter(style),
                    },
                ],
            },
        },
    ];
};

export default function SidebarSettings({
    attributes,
    setAttributes,
    name,
    clientId,
}) {
    const store = { setAttributes, name, attributes, clientId };

    return (
        <>
            <ToolBarElement
                store={store}
                include={[
                    {
                        type: "custom",
                        label: __("Search Filter Settings", "ultimate-post"),
                        icon: icons.setting,
                    },
                ]}
            >
                <ToolbarSettings store={store} include={getToolbarSettings()} />
            </ToolBarElement>

            <ToolBarElement
                store={store}
                include={[
                    {
                        type: "custom",
                        label: __("Search Filter Style", "ultimate-post"),
                        icon: icons.styleIcon,
                    },
                ]}
            >
                <ToolbarSettings store={store} include={getToolbarStyle()} />
            </ToolBarElement>

            <InspectorControls>
                <Sections>
                    <Section
                        slug="setting"
                        title={__("Settings", "ultimate-post")}
                    >
                        <CommonSettings
                            title={"inline"}
                            store={store}
                            include={settings}
                        />
                    </Section>
                    <Section
                        slug="style"
                        title={__("Styles", "ultimate-post")}
                    >
                        <CommonSettings
                            title={"inline"}
                            store={store}
                            include={style}
                        />
                    </Section>
                </Sections>
                {blockSupportLink()}
            </InspectorControls>
        </>
    );
}
