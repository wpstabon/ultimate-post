import { isInlineCSS, updateCurrentPostId } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator';
import Settings from './Settings';

const { useBlockProps, useInnerBlocksProps } = wp.blockEditor;
const { useEffect } = wp.element;
const { useSelect } = wp.data;

const TEMPLATE = [
	[
		'ultimate-post/filter-select',
		{
			type: 'tags',
			allText: 'All Tags',
		},
		[],
	],
	[
		'ultimate-post/filter-select',
		{
			type: 'category',
			allText: 'All Categories',
		},
		[],
	],
	[
		'ultimate-post/filter-select',
		{
			type: 'order',
		},
		[],
	],
];

const ALLOWED_BLOCKS = [
	'ultimate-post/filter-select',
	'ultimate-post/filter-clear',
	'ultimate-post/filter-search-adv',
];

const PRIORITY = [
	'ultimate-post/filter-select/tags',
	'ultimate-post/filter-select/categories',
	'ultimate-post/filter-select/order',
	'ultimate-post/filter-clear',
	'ultimate-post/filter-select/search',
	'ultimate-post/filter-select/adv_sort',
];

const prefix = 'editor-block-list-item-ultimate-post-';

// Only one block from this list is allowed to be in the innerblocks at a time
const noDupList = new Set(
	[
		'filter-select/adv_sort',
		'filter-select/order_by',
		'filter-select/order',
		'filter-select/author',
		'filter-clear',
		'filter-search-adv',
	].map((b) => prefix + b)
);

export default function Edit({
	attributes,
	setAttributes,
	clientId,
	context,
	isSelected,
}) {
	const { blockId, currentPostId } = attributes;

	useEffect(() => {
		updateCurrentPostId(setAttributes, '', currentPostId, clientId);
		setAttributes({
			blockId: clientId.slice(-6),
			clientId: clientId,
		});
	}, [clientId]);

	// Prevents duplicates
	const blocks = useSelect((select) => {
		return select('core/block-editor').getBlocks(clientId) || [];
	});

	// Update selection hack to stop isSelected from being false
	useEffect(() => {
		const { selectBlock } = wp.data.dispatch('core/block-editor');
		selectBlock(clientId);
	}, [blocks.length, clientId]);

	useEffect(() => {
		if (!isSelected) return;
		const unsub = wp.data.subscribe(() => {
			wp.data
				.select('core/block-editor')
				.getBlocks(clientId)
				.map(
					(b) =>
						'editor-block-list-item-' +
						b.name.replace('/', '-') +
						(b.name.includes('filter-select')
							? '/' + b.attributes.type
							: '')
				)
				.forEach((blockName) => {
					if (!noDupList.has(blockName)) {
						return;
					}

					Array.from(
						document.getElementsByClassName(blockName)
					).forEach(
						/**
						 *
						 * @param {HTMLElement} button
						 */
						(button) => {
							button.ariaDisabled = 'true';
							button.style.pointerEvents = 'none';
						}
					);
				});
		});

		return () => unsub();
	}, [isSelected]);

	const blockProps = useInnerBlocksProps(
		useBlockProps({
			className: `ultp-block-${blockId} ultp-filter-block`,
			'data-postid': context.postId,
		}),
		{
			orientation: 'horizontal',
			template: TEMPLATE,
			templateLock: false,
			allowedBlocks: ALLOWED_BLOCKS,
			prioritizedInserterBlocks: PRIORITY,
			templateInsertUpdatesSelection: false,
		}
	);

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(
			attributes,
			'ultimate-post/advanced-filter',
			blockId,
			isInlineCSS()
		);
	}

	return (
		<>
			<Settings
				attributes={attributes}
				setAttributes={setAttributes}
				name={'ultimate-post/advanced-filter'}
				clientId={clientId}
			/>
			{__preview_css && (
				<style
					dangerouslySetInnerHTML={{ __html: __preview_css }}
				></style>
			)}
			<div {...blockProps} />
		</>
	);
}