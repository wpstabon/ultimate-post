const { useBlockProps, useInnerBlocksProps } = wp.blockEditor;

export default function Save({ attributes }) {
    const { blockId } = attributes;

    const blockProps = useInnerBlocksProps.save(
        useBlockProps.save({
            className: `ultp-block-${blockId} ultp-filter-block`,
        })
    );

    return <div {...blockProps} />;
}
