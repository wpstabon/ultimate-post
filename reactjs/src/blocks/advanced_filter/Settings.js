import { AdvFilterArgs, blockSupportLink, CommonSettings } from "../../helper/CommonPanel";
import { Section, Sections, SectionsNoTab } from "../../helper/Sections";
import ToolBarElement from "../../helper/ToolBarElement";

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;

export default function Settings({
    name,
    attributes,
    setAttributes,
    clientId,
}) {
    const store = { setAttributes, name, attributes, clientId };

    return (
        <>
            <ToolBarElement 
                    store={store}
                    include={[
                        {
                            type: "adv_filter"
                        },
                        {
                            type: "inserter",
                            label: __('Add Filters', 'ultimate-post'),
                            clientId: clientId
                        }
                    ]}
            />

            <InspectorControls>
                <SectionsNoTab>
                    <Section slug="setting" title={__("Setting", "ultimate-post")}>
                        <CommonSettings
                            title={"inline"}
                            include={AdvFilterArgs}
                            store={store}
                            initialOpen={true}
                        />
                    </Section>
                </SectionsNoTab>
                { blockSupportLink() }
            </InspectorControls>
        </>
    );
}
