const attributes = {
    blockId: { type: "string", default: "" },
    previewImg: { type: "string", default: "" },
    currentPostId: { type: "string" , default: "" },
    relation: { type: "string", default: "AND" },
    align: {
        type: "object",
        default: {lg: "flex-start"},
        style: [
            {
                selector: "{{ULTP}} { justify-content: {{align}}; }",
            },
        ],
    },
    gapChild: {
        type: "object",
        default: { lg: "10", unit: "px" },
        style: [
            {
                selector: "{{ULTP}} { gap: {{gapChild}}; }",
            },
        ],
    },
    spacingTop: {
        type: "object",
        default: { lg: "10", unit: "px" },
        style: [
            {
                selector: "{{ULTP}} { margin-top: {{spacingTop}} !important; }",
            },
        ],
    },
    spacingBottom: {
        type: "object",
        default: { lg: "10", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} { margin-bottom: {{spacingBottom}} !important; }",
            },
        ],
    },
};

export default attributes;
