const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from "./Edit";
import Save from "./Save";
import attributes from "./attributes";
import UltpLinkGenerator from "../../helper/UltpLinkGenerator";
const docsUrl = UltpLinkGenerator(
    "https://wpxpo.com/docs/postx/postx-features/advanced-post-filter/",
    "block_docs"
);

const filterIcon  = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/filter-icon.svg'}/>;

registerBlockType("ultimate-post/advanced-filter", {
    apiVersion: 2,
    title: __("Advanced Post Filter", "ultimate-post"),
    icon: filterIcon,
    category: "ultimate-post",
    description: (
        <span className="ultp-block-info">
            {__("Allow users to find specific posts using filters", "ultimate-post")}
            <a target="_blank" href={docsUrl}>
                {__("Documentation", "ultimate-post")}
            </a>
        </span>
    ),
    keywords: [
        __("Post Grid", "ultimate-post"),
        __("Grid View", "ultimate-post"),
        __("Article", "ultimate-post"),
        __("Post Listing", "ultimate-post"),
        __("Grid", "ultimate-post"),
    ],
    attributes,
    providesContext: {
        "advanced-filter/cId": "clientId",
    },
    usesContext: ["postId"],
    supports: {
        inserter: false,
    },
    edit: Edit,
    save: Save,
});
