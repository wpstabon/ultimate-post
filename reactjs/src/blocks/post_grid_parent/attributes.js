const attributes = {
  blockId: { type: "string", default: "" },
  cId: { type: "string", default: "" },
  postId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  previewImg: { type: "string", default: "" },
  advFilterEnable: {type: "boolean", default: false},
  grids: { type: "string", default: "" },
  pagi: { type: "string", default: "" },
};

export default attributes;
