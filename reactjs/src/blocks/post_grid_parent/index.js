const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit'
import Save from './Save';
import attributes from "./attributes";
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-grid-1/', 'block_docs');

const postGridParentIcon = <img src={ultp_data.url + 'assets/img/blocks/adv-filter/post-block.svg'}/>;

registerBlockType(
    'ultimate-post/post-grid-parent', {
        apiVersion: 2,
        title: __('Post Block','ultimate-post'),
        icon: postGridParentIcon,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Post Block from PostX','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [
            __('Post Grid','ultimate-post'),
            __('Grid View','ultimate-post'),
            __('Article','ultimate-post'),
            __('Post Listing','ultimate-post'),
            __('Grid','ultimate-post'),
        ],
        providesContext: {
            'post-grid-parent/postBlockClientId': 'cId',
            'post-grid-parent/pagi' : 'pagi'
        },
        usesContext: ['postId'],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
            inserter: false,
            reusable: false,
        },
        edit: Edit,
        save: Save
    }
)