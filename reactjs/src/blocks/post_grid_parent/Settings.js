const { __ } = wp.i18n
import { GeneralContent, QueryContent, PaginationContent, SeparatorStyle, FilterContent, GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, WrapStyle, HeadingContent, ImageStyle, ReadMoreStyle, TitleStyle, CategoryStyle, ExcerptStyle, MetaStyle, VideoStyle, blockSupportLink, CommonSettings } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'
import TemplateModal from '../../helper/TemplateModal'

const getFilterBlock = (clientId) => {
    const { getBlock, getPreviousBlockClientId } = wp.data.select('core/block-editor');

    const previousBlockClientId = getPreviousBlockClientId(clientId);

    if (previousBlockClientId) {
        const previousBlock = getBlock(previousBlockClientId);
        if( previousBlock && previousBlock.name === "core/paragraph" && previousBlock.attributes.content === "Filter block" ) {
            return previousBlock
        }
    }

    return null;
}

const insertFilterBlock = (clientId) => {
    const { insertBlock } = wp.data.dispatch('core/block-editor');
    const { createBlock } = wp.blocks;

    const filterBlock = getFilterBlock(clientId);

    if (!filterBlock) {
        insertBlock(
            createBlock("core/paragraph", {content: "Filter block"}),
            clientId,
        );
    }
}

const removeFilterBlock = (clientId) => {
    const { removeBlock } = wp.data.dispatch('core/block-editor');

    const filterBlock = getFilterBlock(clientId);

    if (filterBlock) {
        removeBlock(filterBlock.clientId);
    }
}

const {useEffect} = wp.element;

const PG1_Settings = (props) => {
    const {store} = props
    const {layout, queryType, advFilterEnable} = store.attributes;

    useEffect(() => {
        advFilterEnable ?
            insertFilterBlock(store.clientId) :
            removeFilterBlock(store.clientId);
    }, [advFilterEnable, store.clientId])

    return (
        <>
            <TemplateModal 
                prev="https://www.wpxpo.com/postx/blocks/#demoid6829"
                store={store}
            />
            <Sections>
                <Section slug="setting" title={__('Setting','ultimate-post')}>
                    <GeneralContent initialOpen={true} store={store}
                        include={[
                            {
                                position: 0,
                                data: {
                                    type: "advFilterEnable",
                                    key: "advFilterEnable",
                                }
                            },
                            {
                                position: 1, data:{ type:'layout', block:'post-grid-1', key:'layout', label:__('Layout','ultimate-post'),
                                    options:[
                                        { img: 'assets/img/layouts/pg1/l1.png', label: 'Layout 1', value: 'layout1', pro: false /*, demoImg: 'https://ultp.wpxpo.com/wp-content/uploads/2020/09/post-grid-1-style2.jpg', demoUrl: 'https://www.wpxpo.com/postx/patterns/#demoid4389'*/ },
                                        { img: 'assets/img/layouts/pg1/l2.png', label: 'Layout 2', value: 'layout2', pro: true },
                                        { img: 'assets/img/layouts/pg1/l3.png', label: 'Layout 3', value: 'layout3', pro: true },
                                        { img: 'assets/img/layouts/pg1/l4.png', label: 'Layout 4', value: 'layout4', pro: true },
                                        { img: 'assets/img/layouts/pg1/l5.png', label: 'Layout 5', value: 'layout5', pro: true },
                                    ]
                                }
                            },
                            {
                                position: 2, data:{ type:'layout', key:'gridStyle', pro: true, tab: true, label:__('Select Advanced Style','ultimate-post'), block:'post-grid-1',
                                    options:[
                                        { img:'assets/img/layouts/pg1/s1.png', label:__('Style 1','ultimate-post'),value: 'style1', pro: false },
                                        { img:'assets/img/layouts/pg1/s2.png', label:__('Style 2','ultimate-post'),value: 'style2', pro: true },
                                        { img:'assets/img/layouts/pg1/s3.png', label:__('Style 3','ultimate-post'),value: 'style3', pro: true },
                                        { img:'assets/img/layouts/pg1/s4.png', label:__('Style 4','ultimate-post'),value: 'style4', pro: true }
                                    ]
                                }
                            },
                            {
                                position: 3, data: { type:'range',key:'rowSpace',min:0, max:80, step:1, responsive:true, label:__('Row Gap','ultimate-post') }, 
                            },
                            {
                                data:{ type:'toggle',key:'equalHeight', label:__('Equal Height','ultimate-post'), pro: true, customClass: 'ultp-pro-field' } 
                            },
                            {
                                data:{ type:'text',key:'notFoundMessage', label:__('No result found Text','ultimate-post') } 
                            },
                            
                        ]}/>
                    <QueryContent store={store} />
                    <HeadingContent 
                        store={store} 
                        depend="headingShow"/>
                    <TitleStyle 
                        store={store} 
                        depend="titleShow" 
                        exclude={['titleBackground']} />
                    <MetaStyle 
                        store={store} 
                        depend="metaShow" 
                        exclude={['metaListSmall']} />
                    <CategoryStyle 
                        depend="catShow" 
                        store={store} />
                    { 
                        ( layout != 'layout2') && 
                        <ImageStyle 
                            store={store} 
                            depend="showImage" 
                            exclude={['imgMargin']} 
                            include={[
                                { 
                                    position: 3, data:{ type:'range',key:'imgSpacing', label:__('Img Spacing','ultimate-post'),min:0, max:100, step:1, responsive:true } 
                                },
                                {   
                                    position: 8, data:{ type:'toggle',key:'fallbackEnable', label:__('Fallback Image Enable','ultimate-post')},
                                },
                                {   
                                    position: 9, data:{ type:'media',key:'fallbackImg', label:__('Fallback Image','ultimate-post')},
                                },
                            ]}/> 
                    }
                    {
                        ( layout != 'layout2' )  &&
                        <VideoStyle
                            depend="vidIconEnable"
                            store={store}/>
                    }
                    { 
                        <WrapStyle 
                            store={store} 
                            exclude={['contenWraptWidth','contenWraptHeight']} 
                            include={[
                                { position: 6, data:{ type:'color',key:'innerBgColor', label:__('Inner Bg Color','ultimate-post') } },
                            ]} /> 
                    }
                    <ExcerptStyle 
                        depend="excerptShow" 
                        store={store} />
                    { 
                        (queryType != 'posts' && queryType != 'customPosts') && 
                        <FilterContent 
                            store={store} 
                            depend="filterShow"
                        />
                    }
                    <PaginationContent 
                        store={store} 
                        depend="paginationShow"/>
                    <ReadMoreStyle 
                        store={store} 
                        depend="readMore"/>
                    <SeparatorStyle
                        depend='separatorShow'
                        exclude={["septSpace"]} 
                        store={store} />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <GeneralAdvanced initialOpen={true} store={store}
                    include={[
                        { position: 2, data:{ type:'color',key:'loadingColor', label:__('Loading Color','ultimate-post'), pro: true } },
                    ]}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            { blockSupportLink() }
        </>
    )
}
export default PG1_Settings;


