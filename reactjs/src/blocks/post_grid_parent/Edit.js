import { updateCurrentPostId } from '../../helper/CommonPanel';
import {
    getAllInnerBlocks,
    isCompatibleBlock,
} from '../../helper/gridFunctions';
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

const { useBlockProps, useInnerBlocksProps } = wp.blockEditor;
const { useEffect, useMemo } = wp.element;
const { useSelect, useDispatch } = wp.data;

export default function Edit({
	attributes,
	setAttributes,
	clientId,
	context,
	isSelected,
}) {
	const [blocks, pagi, advFilter, allowedBlocks] = useSelect(
		(select) => {
			const parentBlocks =
				select('core/block-editor').getBlocks(clientId);
			const allBlocks = getAllInnerBlocks(parentBlocks);

			const _blocks = allBlocks
				.filter((block) => isCompatibleBlock(block.name))
				.map((block) => ({
					blockId: block.attributes.blockId,
					name: block.name.replace('/', '_'),
				}));

			const _pagi = allBlocks
				.filter((block) =>
					block.name.includes('ultimate-post/post-pagination')
				)
				.map((block) => 'ultp-block-' + block.attributes.blockId);

			const _advFilter = allBlocks
				.filter((block) => block.name.includes('advanced-filter'))
				.map((block) => 'ultp-block-' + block.attributes.blockId);

			// If a compatible block is present, prevent other compatible blocks
			// to be in it (prevents multiple grid blocks)
			const allowedBlocks =
				_blocks.length > 0
					? [
							'ultimate-post/advanced-filter',
							'ultimate-post/post-pagination',
							..._blocks.map((b) => b.name.replace('_', '/')),
						]
					: true;

			return [
				JSON.stringify(_blocks),
				JSON.stringify(_pagi),
				JSON.stringify(_advFilter),
				allowedBlocks,
			];
		},
		[clientId]
	);

	// Hides block appender
	useEffect(() => {
		if (isSelected) {
			try {
				// @ts-ignore
				document.querySelector('.block-list-appender').style.display =
					'none';
			} catch {}
		}
	}, [isSelected]);

	const blockProps = useInnerBlocksProps(
		useBlockProps({
			className: 'ultp-post-grid-parent',
		}),
		{
			allowedBlocks,
		}
	);

	useEffect(() => {
		const { currentPostId } = attributes
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);
		setAttributes({
			cId: clientId,
			grids: blocks,
			pagi: pagi,
			postId: context.postId ? String(context.postId) : '',
			currentPostId: context.postId ? String(context.postId) : '',
		});
	}, [clientId, blocks, pagi, context.postId]);

	// Sync filter attributes with grid blocks
	useSyncAdvFilterDefaultValues(clientId);

	return <div {...blockProps} />;
}

function useSyncAdvFilterDefaultValues(clientId) {
	const { updateBlockAttributes } = useDispatch('core/block-editor');

	const parentBlocks = useSelect(
		(select) => select('core/block-editor').getBlocks(clientId),
		[]
	);

	const allBlocks = useMemo(
		() => getAllInnerBlocks(parentBlocks),
		[parentBlocks]
	);

	const filterBlocks = useMemo(() => {
		return allBlocks.filter(
			(block) =>
				block.name === 'ultimate-post/filter-select' &&
				['category', 'tags', 'custom_tax', 'author'].includes(
					block.attributes.type
				)
		);
	}, [allBlocks]);

	const gridBlocks = useMemo(() => {
		return allBlocks.filter((block) => isCompatibleBlock(block.name));
	}, [allBlocks]);

    // Filter Block Select dropdown Default values
	useEffect(() => {
		const newDefQueryTax = {};

		filterBlocks.forEach((filterBlock) => {
			const { dropdownOptionsType, dropdownOptions, type, filterStyle } =
				filterBlock.attributes;

			const opts = JSON.parse(dropdownOptions);
			const defValue = opts[0];

			if (
				filterStyle === 'dropdown' &&
				dropdownOptionsType === 'specific' &&
				defValue
			) {
				newDefQueryTax[filterBlock.clientId] = `${type}###${defValue}`;
			}
		});

		gridBlocks.forEach((gridBlock) => {
			const {
				attributes: { defQueryTax: currDefQueryTax },
			} = gridBlock;

			if (Object.keys(newDefQueryTax).length > 0) {
				let needsUpdate = false;

				for (const key of Object.keys(newDefQueryTax)) {
					if (
						!(key in currDefQueryTax) ||
						currDefQueryTax[key] !== newDefQueryTax[key]
					) {
						needsUpdate = true;
						break;
					}
				}

				if (needsUpdate) {
					updateBlockAttributes(gridBlock.clientId, {
						defQueryTax: newDefQueryTax,
					});
				}
			} else if (Object.keys(currDefQueryTax).length !== 0) {
				updateBlockAttributes(gridBlock.clientId, {
					defQueryTax: {},
				});
			}
		});
	}, [filterBlocks, gridBlocks]);



    // Filter Block Parent
	const filterParentBlock = useMemo(
		() =>
			allBlocks.find(
				(block) => block.name === 'ultimate-post/advanced-filter'
			),
		[allBlocks]
	);

	const { relation  } = filterParentBlock?.attributes ?? {};

	useEffect(() => {
		if (relation) {
			gridBlocks.forEach((gridBlock) => {
				if (gridBlock.attributes.advRelation !== relation) {
					updateBlockAttributes(gridBlock.clientId, {
						advRelation: relation,
					});
				}
			});
		}
	}, [gridBlocks, relation]);
}