const { useBlockProps, useInnerBlocksProps } = wp.blockEditor;

export default function Save({attributes}) {
    const blockProps = useInnerBlocksProps.save(
        useBlockProps.save({
            className: "ultp-post-grid-parent",
        })
    );

    return (
        <div
            {...blockProps}
            data-postid={attributes.currentPostId}
            data-grids={attributes.grids}
            data-pagi={attributes.pagi}
        />
    );
}
