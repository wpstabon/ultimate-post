const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { useEffect, useState, useRef, Fragment } = wp.element;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import {
    FeatureToggleArgsDep,
    FeatureToggleArgsNew,
    SliderFeatureToggleArgs,
    SliderSetting,
    attrBuild,
    isInlineCSS,
    isReload,
    updateCurrentPostId,
} from '../../helper/CommonPanel';
import Category from '../../helper/Components/grid-category';
import Excerpt from '../../helper/Components/grid-excerpt';
import { ImageSlider, SliderContent } from '../../helper/Components/grid-image';
import Meta from '../../helper/Components/grid-meta';
import ReadMore from '../../helper/Components/grid-readmore';
import Title from '../../helper/Components/grid-title';
import { CssGenerator } from '../../helper/CssGenerator';
import ToolBarElement from '../../helper/ToolBarElement';
import Heading from '../../helper/block_template/Heading';
import { handleCompatibility } from '../../helper/compatibility';
import { isDCActive } from '../../helper/dynamic_content';
import AddDCButton from '../../helper/dynamic_content/AddDCButton';
import MetaGroup from '../../helper/dynamic_content/MetaGroup';
import IconPack from '../../helper/fields/tools/IconPack';
import {
	stateObj,
	SETTING_SECTIONS,
    resetState,
    restoreState,
    saveSelectedSection,
    saveToolbar,
    scrollSidebarSettings,
} from '../../helper/gridFunctions';
import Settings, {
    AddSettingsToToolbar,
    MAX_CUSTOM_META_GROUPS,
} from './Settings';
const { getBlockAttributes, getBlockRootClientId } =
	wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [state, setState] = useState(stateObj)

	const { setAttributes, clientId, className, name, isSelected, attributes, context, attributes: { blockId, imageShow, imgCrop, readMoreIcon, arrowStyle, arrows, fade, dots, slideSpeed, autoPlay, readMore, readMoreText, excerptLimit, metaStyle, catShow, metaSeparator, titleShow, catStyle, catPosition, titlePosition, excerptShow, metaList, metaShow, headingShow, headingStyle, headingAlign, headingURL, headingText, headingBtnText, subHeadingShow, subHeadingText, metaPosition, imgOverlay, imgOverlayType, contentVerticalPosition, contentHorizontalPosition, showFullExcerpt, customCatColor, onlyCatColor, contentTag, titleTag, showSeoMeta, titleLength, metaMinText, metaAuthorPrefix, titleStyle, metaDateFormat, slidesToShow, authorLink, fallbackEnable, headingTag, notFoundMessage, dcEnabled, dcFields, V4_1_0_CompCheck: { runComp }, advanceId, previewImg, currentPostId } } = props;	

	useEffect(() => {
		resetState();
		fetchProducts();
	}, []);

	function setSelectedDc(selectedDC) {
		setState({ ...state, selectedDC });
	}

	function selectParentMetaGroup(groupIdx) {
		setSelectedDc(groupIdx);
		setToolbarSettings('dc_group');
	}

	function startMetaFieldOnboarding() {
		setState({
			...state,
			selectedDC: '0,0,1',
			toolbarSettings: 'dc_field',
		});
	}

	function setSection(title) {
		setState({
			...state,
			section: {
				...SETTING_SECTIONS,
				[title]: true,
			},
		});
		scrollSidebarSettings(title);
		saveSelectedSection(title);
	}

	function setToolbarSettings(title) {
		setState(prev => ({ ...prev, toolbarSettings: title }));
		saveToolbar(title);
	}

	useEffect(() => {
		const _client = clientId.substr(0, 6);
		const reference = getBlockAttributes(getBlockRootClientId(clientId));

		handleCompatibility(props);
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId );

		if (!blockId) {
			setAttributes({ blockId: _client });
			if (ultp_data.archive && ultp_data.archive == 'archive') {
				setAttributes({ queryType: 'archiveBuilder' });
			}
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (isDCActive() && attributes.dcFields.length === 0) {
			setAttributes({
				dcFields: Array(MAX_CUSTOM_META_GROUPS).fill(undefined),
			});
		}
		if (prevAttributes) {
			if (isReload(prevAttributes, attributes)) {
				fetchProducts();
				prevPropsRef.current = attributes;
			}
			if (prevAttributes.isSelected !== isSelected && isSelected) {
				restoreState(setSection, setToolbarSettings);
			}
		} else {
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	function fetchProducts() {
		if (state.error) {
			setState({ ...state, error: false });
		}
		if (!state.loading) {
			setState({ ...state, loading: true });
		}
		wp.apiFetch({
			path: '/ultp/fetch_posts',
			method: 'POST',
			data: attrBuild(attributes),
		})
		.then((obj) => {
			setState({ ...state, postsList: obj, loading: false });
		})
		.catch((error) => {
			setState({ ...state, loading: false, error: true });
		});
	}

	function arrowFunc(e, onClick) {
		e.stopPropagation();
		setSection('arrow');
		setToolbarSettings('arrow');
		onClick();
	}

	function dotFunc(e) {
		e.stopPropagation();
		setSection('dot');
		setToolbarSettings('dot');
	}

	function renderContent() {

		const CustomTag = `${contentTag}`;

		const NextArrow = (props) => {
			const { className, onClick } = props;
			const data = arrowStyle.split('#');
			return (
				<div
					className={className}
					onClick={(e) => {
						arrowFunc(e, onClick);
					}}
				>
					{IconPack[data[1]]}
				</div>
			);
		};

		const PrevArrow = (props) => {
			const { className, onClick } = props;
			const data = arrowStyle.split('#');
			return (
				<div
					className={className}
					onClick={(e) => {
						arrowFunc(e, onClick);
					}}
				>
					{IconPack[data[0]]}
				</div>
			);
		};

		let attrSettings = {
			arrows: arrows,
			dots: dots,
			autoplay: autoPlay,
			autoplaySpeed: slideSpeed,
			nextArrow: <NextArrow />,
			prevArrow: <PrevArrow />,
		};

		if (
			typeof slidesToShow.lg != 'undefined' &&
			parseInt(slidesToShow.lg) < 2
		) {
			attrSettings.fade = fade;
			attrSettings.slidesToShow = 1;
		} else {
			attrSettings.slidesToShow = parseInt(slidesToShow.lg);
			attrSettings.responsive = [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: parseInt(slidesToShow.sm) || 1,
						slidesToScroll: 1,
					},
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: parseInt(slidesToShow.xs) || 1,
						slidesToScroll: 1,
					},
				},
			];
		}

		attrSettings.appendDots = (dots) => {
			return (
				<div onClick={(e) => dotFunc(e)}>
					<ul> {dots} </ul>
				</div>
			);
		};

		const settings = SliderSetting(attrSettings);

		const customMeta = (idx, postId) => {
			return (
				isDCActive() &&
				dcEnabled && (
					<MetaGroup
						idx={idx}
						postId={postId}
						fields={dcFields}
						settingsOnClick={(e, name) => {
							e?.stopPropagation();
							setToolbarSettings(name);
						}}
						selectedDC={state.selectedDC}
						setSelectedDc={setSelectedDc}
						setAttributes={setAttributes}
						dcFields={dcFields}
					/>
				)
			);
		};

		return !state.error ? (
			!state.loading ? (
				state.postsList.length > 0 ? (
					<div className={`ultp-block-items-wrap`}>
						<div
							onClick={(e) => {
								e.preventDefault();
								setSection('heading');
								setToolbarSettings('heading');
							}}
						>
							<Heading
								props={{
									headingShow,
									headingStyle,
									headingAlign,
									headingURL,
									headingText,
									setAttributes,
									headingBtnText,
									subHeadingShow,
									subHeadingText,
									headingTag,
								}}
							/>
						</div>

						<Slider {...settings}>
							{state.postsList.map((post, idx) => {
								const meta = JSON.parse(
									metaList.replaceAll('u0022', '"')
								);
								return (
									<CustomTag
										key={idx}
										className={`ultp-block-item post-id-${post.ID}`}
									>
										<div
											className={`ultp-block-slider-wrap`}
										>
											<div
												className={`ultp-block-image-inner`}
											>
												{((post.image &&
													!post.is_fallback) ||
													fallbackEnable) && (
													<ImageSlider
														imgOverlay={imgOverlay}
														imgOverlayType={
															imgOverlayType
														}
														post={post}
														fallbackEnable={
															fallbackEnable
														}
														idx={idx}
														imgCrop={imgCrop}
														onClick={() => {}}
													/>
												)}
											</div>

											{/* Image onClick must be provided here because the zIndex of this div is higher than the actual image component */}
											<SliderContent
												contentHorizontalPosition={
													contentHorizontalPosition
												}
												contentVerticalPosition={
													contentVerticalPosition
												}
												onClick={(e) => {
													e.stopPropagation();
													setSection('image');
													setToolbarSettings('image');
												}}
											>
												<div
													className={`ultp-block-content-inner`}
												>
													<Category
														post={post}
														catShow={catShow}
														catStyle={catStyle}
														catPosition={
															catPosition
														}
														customCatColor={
															customCatColor
														}
														onlyCatColor={
															onlyCatColor
														}
														onClick={(e) => {
															setSection('taxonomy-/-category');
															setToolbarSettings('cat');
														}}
													/>

													{customMeta(6, post.ID)}

													{post.title &&
														titleShow &&
														titlePosition ==
															true && (
															<Title
																title={
																	post.title
																}
																headingTag={
																	titleTag
																}
																titleLength={
																	titleLength
																}
																titleStyle={
																	titleStyle
																}
																onClick={(
																	e
																) => {
																	setSection('title');
																	setToolbarSettings('title');
																}}
															/>
														)}
													{customMeta(5, post.ID)}
													{metaShow &&
														metaPosition ==
															'top' && (
															<Meta
																meta={meta}
																post={post}
																metaSeparator={
																	metaSeparator
																}
																metaStyle={
																	metaStyle
																}
																metaMinText={
																	metaMinText
																}
																metaAuthorPrefix={
																	metaAuthorPrefix
																}
																metaDateFormat={
																	metaDateFormat
																}
																authorLink={
																	authorLink
																}
																onClick={(
																	e
																) => {
																	setSection('meta');
																	setToolbarSettings('meta');
																}}
															/>
														)}
													{customMeta(4, post.ID)}
													{post.title &&
														titleShow &&
														titlePosition ==
															false && (
															<Title
																title={
																	post.title
																}
																headingTag={
																	titleTag
																}
																titleLength={
																	titleLength
																}
																titleStyle={
																	titleStyle
																}
																onClick={(
																	e
																) => {
																	setSection('title');
																	setToolbarSettings('title');
																}}
															/>
														)}
													{customMeta(3, post.ID)}
													{excerptShow && (
														<div
															className={`ultp-block-excerpt`}
														>
															<Excerpt
																excerpt={
																	post.excerpt
																}
																excerpt_full={
																	post.excerpt_full
																}
																seo_meta={
																	post.seo_meta
																}
																excerptLimit={
																	excerptLimit
																}
																showFullExcerpt={
																	showFullExcerpt
																}
																showSeoMeta={
																	showSeoMeta
																}
																onClick={(
																	e
																) => {
																	setSection('excerpt');
																	setToolbarSettings('excerpt');
																}}
															/>
														</div>
													)}
													{customMeta(2, post.ID)}
													{readMore && (
														<ReadMore
															readMoreText={
																readMoreText
															}
															readMoreIcon={
																readMoreIcon
															}
															titleLabel={
																post.title
															}
															onClick={() => {
																setSection('read-more');
																setToolbarSettings('read-more');
															}}
														/>
													)}
													{customMeta(1, post.ID)}
													{metaShow &&
														metaPosition ==
															'bottom' && (
															<Meta
																meta={meta}
																post={post}
																metaSeparator={
																	metaSeparator
																}
																metaStyle={
																	metaStyle
																}
																metaMinText={
																	metaMinText
																}
																metaAuthorPrefix={
																	metaAuthorPrefix
																}
																metaDateFormat={
																	metaDateFormat
																}
																authorLink={
																	authorLink
																}
																onClick={(
																	e
																) => {
																	setSection('meta');
																	setToolbarSettings('meta');
																}}
															/>
														)}
													{customMeta(0, post.ID)}

													{isDCActive() &&
														dcEnabled && (
															<AddDCButton
																dcFields={dcFields}
																setAttributes={setAttributes}
																startOnboarding={startMetaFieldOnboarding}
															/>
														)}
												</div>
											</SliderContent>
										</div>
									</CustomTag>
								);
							})}
						</Slider>
					</div>
				) : (
					<Placeholder
						className={`ultp-backend-block-loading`}
						label={notFoundMessage}
					></Placeholder>
				)
			) : (
				<Placeholder
					className={`ultp-backend-block-loading`}
					label={__('Loading...', 'ultimate-post')}
				>
					<Spinner />
				</Placeholder>
			)
		) : (
			<Placeholder
				label={__('Posts are not available.', 'ultimate-post')}
			>
				<div style={{ marginBottom: 15 }}>
					{__('Make sure Add Post.', 'ultimate-post')}
				</div>
			</Placeholder>
		);
	}

	
		const store = {
			setAttributes,
			name,
			attributes,
			setSection,
			section: state.section,
			clientId,
			context,
			setSelectedDc,
			selectedDC: state.selectedDC,
			selectParentMetaGroup
		};
		

		let __preview_css;
		if (blockId) {
			__preview_css = CssGenerator(
				attributes,
				'ultimate-post/post-slider-1',
				blockId,
				isInlineCSS()
			);
		}

		if (previewImg) {
			return (
				<img
					style={{ marginTop: '0px', width: '420px' }}
					src={previewImg}
				/>
			);
		}

		return (
			<Fragment>
				<AddSettingsToToolbar
					store={store}
					selected={state.toolbarSettings}
				/>
				<ToolBarElement
					include={[
						{
							type: 'query',
						},
						{
							type: 'template',
						},
						{
							type: 'grid_align',
							key: 'contentAlign',
						},
						{
							type: 'feat_toggle',
							label: __('Slider Features', 'ultimate-post'),
							data: SliderFeatureToggleArgs,
							new: FeatureToggleArgsNew,
							[runComp ? 'exclude' : 'dep']: FeatureToggleArgsDep,
						},
					]}
					store={store}
				/>

				<InspectorControls>
					<Settings store={store} />
				</InspectorControls>

				<div
					{...(advanceId && { id: advanceId })}
					className={`ultp-block-${blockId} ${className}`}
					onClick={(e) => {
						e.stopPropagation();
						setSection('general');
						setToolbarSettings('');
					}}
				>
					{__preview_css && (
						<style
							dangerouslySetInnerHTML={{ __html: __preview_css }}
						></style>
					)}

					<div className={`ultp-block-wrapper`}>
						{renderContent()}
					</div>
				</div>
			</Fragment>
		);
	}

