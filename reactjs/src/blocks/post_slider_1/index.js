const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/post-slider-1/', 'block_docs');

registerBlockType(
    'ultimate-post/post-slider-1', {
        title: __('Post Slider #1','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/post-slider-1.svg'}/>,
        category: 'ultimate-post',
        // description: <span className="ultp-block-info">
        //     {__('Dynamic post slider with lots of settings.','ultimate-post')}
        //     <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        // </span>,
        keywords: [ 
            __('Post Slider','ultimate-post'),
            __('Post Carousel','ultimate-post'),
            __('Slide','ultimate-post'),
            __('Slider','ultimate-post'),
            __('Feature','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/postslider1.svg',
            },
        },
        usesContext: [
            'post-grid-parent/postBlockClientId'
        ],
        edit: Edit,
        save() {
            return null;
        },
    }
)