import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { V4_1_0_CompCheck } from "../../helper/compatibility";
import { getDCBlockAttributes } from "../../helper/dynamic_content/attributes";

const attributes = {
  /*==========================`
      General Settings
  ==========================*/
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  slidesToShow: { type: "object", default: { lg: "1", sm: "1", xs: "1" } },
  autoPlay: { type: "boolean", default: true },
  height: {
    type: "object",
    default: { lg: "550", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-image,  
          {{ULTP}} .ultp-block-slider-wrap { height: {{height}}; }`,
      },
    ],
  },
  slideSpeed: {
    type: "string",
    default: "3000",
    style: [{ depends: [{ key: "autoPlay", condition: "==", value: true }] }],
  },
  sliderGap: {
    type: "string",
    default: "10",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-items-wrap .slick-slide > div { padding: 0 {{sliderGap}}px; line-height: 0px; }
          {{ULTP}} .ultp-block-items-wrap .slick-list { margin: 0 -{{sliderGap}}px; }`,
      },
    ],
  },
  dots: { type: "boolean", default: true },
  arrows: { type: "boolean", default: true },
  preLoader: { type: "boolean", default: false },
  fade: { type: "boolean", default: true },
  excerptShow: { type: "boolean", default: true },
  contentTag: { type: "string", default: "div" },
  openInTab: { type: "boolean", default: false },
  notFoundMessage: { type: "string", default: "No Post Found" },
  
  /*==========================`
      Title Settings
  ==========================*/
  titleShow: { type: "boolean", default: true },
  titleStyle: { type: "string", default: "none" },
  titleAnimColor: {
    type: "string",
    default: "black",
    style: [
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style1" }],
        selector:
          "{{ULTP}} .ultp-title-style1 a {  cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.35s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style2" }],
        selector:
          "{{ULTP}} .ultp-title-style2 a:hover {  border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style3" }],
        selector:
          "{{ULTP}} .ultp-title-style3 a {  text-decoration: none; $thetransition: all 1s cubic-bezier(1,.25,0,.75) 0s; position: relative; transition: all 0.35s ease-out; padding-bottom: 3px; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style4" }],
        selector:
          "{{ULTP}} .ultp-title-style4 a { cursor: pointer; font-weight: 600; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a:hover{ text-decoration: none; transition: all 0.35s ease-out; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-size:auto; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style6" }],
        selector:
          "{{ULTP}} .ultp-title-style6 a{ background-image: linear-gradient(120deg, {{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 2px; background-position: 0 88%; transition: background-size 0.15s ease-in; padding: 5px 5px; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style7" }],
        selector:
          "{{ULTP}} .ultp-title-style7 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: right 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style8" }],
        selector:
          "{{ULTP}} .ultp-title-style8 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: center 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style9" }],
        selector:
          "{{ULTP}} .ultp-title-style9 a { background-image: linear-gradient(120deg,{{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 10px; background-position: 0 88%; transition: 0.3s ease-in; padding: 3px 5px; } ",
      },
    ],
  },
  titleTag: { type: "string", default: "h3" },
  titlePosition: { type: "boolean", default: true },
  titleColor: {
    type: "string",
    default: "#0e1523",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-title a { color:{{titleColor}} !important; }",
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "#037fff",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-title a:hover { color:{{titleHoverColor}} !important; }",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "28", unit: "px" },
      height: { lg: "36", unit: "px" },
      decoration: "none",
      family: "",
      weight: "500",
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-item .ultp-block-content .ultp-block-title,  
          {{ULTP}} .ultp-block-item .ultp-block-content .ultp-block-title a`,
      },
    ],
  },
  titlePadding: {
    type: "object",
    default: { lg: { top: 25, bottom: 12, unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title { padding:{{titlePadding}}; }",
      },
    ],
  },
  titleLength: { type: "string", default: 0 },
  titleBackground: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title a { padding: 2px 7px; -webkit-box-decoration-break: clone; box-decoration-break: clone; background-color:{{titleBackground}}; }",
      },
    ],
  },

  /*==========================`
      Image Settings
  ==========================*/
  imageShow: { type: "boolean", default: true },
  imgCrop: { type: "string", default: "full" },
  imgOverlay: { type: "boolean", default: false },
  imgOverlayType: {
    type: "string",
    default: "default",
    style: [{ depends: [{ key: "imgOverlay", condition: "==", value: true }] }],
  },
  overlayColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#0e1523" },
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector: "{{ULTP}} .ultp-block-image-custom > a::before",
      },
    ],
  },
  imgOpacity: {
    type: "string",
    default: 0.69999999999999996,
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector:
          "{{ULTP}} .ultp-block-image-custom > a::before { opacity: {{imgOpacity}}; }",
      },
    ],
  },
  imgGrayScale: {
    type: "object",
    default: { lg: "0", ulg: "%", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-image img { filter: grayscale({{imgGrayScale}}); }",
      },
    ],
  },
  imgHoverGrayScale: {
    type: "object",
    default: { lg: "0", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image img { filter: grayscale({{imgHoverGrayScale}}); }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector: "{{ULTP}} .ultp-block-image { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  imgHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image { border-radius:{{imgHoverRadius}}; }",
      },
    ],
  },
  imgShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-image" }],
  },
  imgHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-item:hover .ultp-block-image" }],
  },
  fallbackEnable: {
    type: "boolean",
    default: true,
    style: [{ depends: [{ key: "imageShow", condition: "==", value: true }] }],
  },
  fallbackImg: {
    type: "object",
    default: "",
    style: [
      {
        depends: [
          { key: "imageShow", condition: "==", value: true },
          { key: "fallbackEnable", condition: "==", value: true },
        ],
      },
    ],
  },
  imgSrcset: { type: "boolean", default: false },
  imgLazy: { type: "boolean", default: false },


  /*==========================`
      Excerpt Settings
  ==========================*/
  showSeoMeta: { type: "boolean", default: false },
  showFullExcerpt: { 
    type: "boolean", 
    default: false,
    style: [
      {
        depends: [{ key: "showSeoMeta", condition: "==", value: false }]
      }
    ]
  },
  excerptLimit: {
    type: "string",
    default: 40,
    style: [
      { depends: [{ key: "showFullExcerpt", condition: "==", value: false }] },
    ],
  },
  excerptColor: {
    type: "string",
    default: "#777",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: 
        `{{ULTP}} .ultp-block-excerpt,  
        {{ULTP}} .ultp-block-excerpt p { color:{{excerptColor}}; }`,
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 26, unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-excerpt,  
          {{ULTP}} .ultp-block-excerpt p`,
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: 10, bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt { padding: {{excerptPadding}}; }",
      },
    ],
  },
  /*==========================`
      Arrow Settings
  ==========================*/
  arrowStyle: {
    type: "string",
    default: "leftAngle2#rightAngle2",
    style: [{ depends: [{ key: "arrows", condition: "==", value: true }] }],
  },
  arrowSize: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-next svg,  
          {{ULTP}} .slick-prev svg { width:{{arrowSize}}; }`,
      },
    ],
  },
  arrowWidth: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { width:{{arrowWidth}}; }",
      },
    ],
  },
  arrowHeight: {
    type: "object",
    default: { lg: "60", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow { height:{{arrowHeight}}; }  
          {{ULTP}} .slick-arrow { line-height:{{arrowHeight}}; }`,
      },
    ],
  },
  arrowVartical: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-next { right:{{arrowVartical}}; }  
          {{ULTP}} .slick-prev { left:{{arrowVartical}}; }`,
      },
    ],
  },
  arrowColor: {
    type: "string",
    default: "#037fff",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow:before { color:{{arrowColor}}; }  
          {{ULTP}} .slick-arrow svg { fill:{{arrowColor}}; }`,
      },
    ],
  },
  arrowHoverColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-arrow:hover:before { color:{{arrowHoverColor}}; }  
          {{ULTP}} .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }`,
      },
    ],
  },
  arrowBg: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { background:{{arrowBg}}; }",
      },
    ],
  },
  arrowHoverBg: {
    type: "string",
    default: "#037fff",
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-arrow:hover { background:{{arrowHoverBg}}; }",
      },
    ],
  },
  arrowBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow",
      },
    ],
  },
  arrowHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow:hover",
      },
    ],
  },
  arrowRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow { border-radius: {{arrowRadius}}; }",
      },
    ],
  },
  arrowHoverRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-arrow:hover { border-radius: {{arrowHoverRadius}}; }",
      },
    ],
  },
  arrowShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow",
      },
    ],
  },
  arrowHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "arrows", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-arrow:hover",
      },
    ],
  },

  /*==========================`
      Dot Settings
  ==========================*/
  dotWidth: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { width:{{dotWidth}}; }",
      },
    ],
  },
  dotHeight: {
    type: "object",
    default: { lg: "10", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { height:{{dotHeight}}; }",
      },
    ],
  },
  dotHoverWidth: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li.slick-active button { width:{{dotHoverWidth}}; }",
      },
    ],
  },
  dotHoverHeight: {
    type: "object",
    default: { lg: "16", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li.slick-active button { height:{{dotHoverHeight}}; }",
      },
    ],
  },
  dotSpace: {
    type: "object",
    default: { lg: "4", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots { padding: 0 {{dotSpace}}; }  
          {{ULTP}} .slick-dots li button { margin: 0 {{dotSpace}}; }`,
      },
    ],
  },
  dotVartical: {
    type: "object",
    default: { lg: "40", unit: "px" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots { bottom:{{dotVartical}}; }",
      },
    ],
  },
  dotHorizontal: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots { left:{{dotHorizontal}}; }",
      },
    ],
  },
  dotBg: {
    type: "string",
    default: "#f5f5f5",
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button { background:{{dotBg}}; }",
      },
    ],
  },
  dotHoverBg: {
    type: "string",
    default: "#000",
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover,  
          {{ULTP}} .slick-dots li.slick-active button { background:{{dotHoverBg}}; }`,
      },
    ],
  },
  dotBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button",
      },
    ],
  },
  dotHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover,  
          {{ULTP}} .slick-dots li.slick-active button`,
      },
    ],
  },
  dotRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          "{{ULTP}} .slick-dots li button { border-radius: {{dotRadius}}; }",
      },
    ],
  },
  dotHoverRadius: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover,  
          {{ULTP}} .slick-dots li.slick-active button { border-radius: {{dotHoverRadius}}; }`,
      },
    ],
  },
  dotShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector: "{{ULTP}} .slick-dots li button",
      },
    ],
  },
  dotHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "dots", condition: "==", value: true }],
        selector:
          `{{ULTP}} .slick-dots li button:hover,  
          {{ULTP}} .slick-dots li.slick-active button`,
      },
    ],
  },

  /*==========================`
      Content Settings
  ==========================*/
  contentVerticalPosition: {
    type: "string",
    default: "middlePosition",
    style: [
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "topPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-topPosition { align-items:flex-start; }",
      },
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "middlePosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-middlePosition { align-items:center; }",
      },
      {
        depends: [
          {
            key: "contentVerticalPosition",
            condition: "==",
            value: "bottomPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-bottomPosition { align-items:flex-end; }",
      },
    ],
  },
  contentHorizontalPosition: {
    type: "string",
    default: "centerPosition",
    style: [
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "leftPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-leftPosition { justify-content:flex-start; }",
      },
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "centerPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-centerPosition { justify-content:center; }",
      },
      {
        depends: [
          {
            key: "contentHorizontalPosition",
            condition: "==",
            value: "rightPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-rightPosition { justify-content:flex-end; }",
      },
    ],
  },
  contentAlign: {
    type: "string",
    default: "center",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; }  
          {{ULTP}} .ultp-block-meta { justify-content: flex-start; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; }  
          {{ULTP}} .ultp-block-meta { justify-content: center; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-block-content-inner { text-align:{{contentAlign}}; }  
          {{ULTP}} .ultp-block-meta { justify-content: flex-end; }`,
      },
    ],
  },
  contenWraptWidth: {
    type: "object",
    default: { lg: "60", unit: "%" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner { width:{{contenWraptWidth}}; }",
      },
    ],
  },
  contenWraptHeight: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner { height:{{contenWraptHeight}}; }",
      },
    ],
  },
  contentWrapBg: {
    type: "string",
    default: "#fff",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner { background:{{contentWrapBg}}; }",
      },
    ],
  },
  contentWrapHoverBg: {
    type: "string",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner:hover { background:{{contentWrapHoverBg}}; }",
      },
    ],
  },
  contentWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-inner" }],
  },
  contentWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-inner:hover" }],
  },
  contentWrapRadius: {
    type: "object",
    default: {
      lg: { top: "6", bottom: "6", left: "6", right: "6", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner{ border-radius: {{contentWrapRadius}}; }",
      },
    ],
  },
  contentWrapHoverRadius: {
    type: "object",
    default: {
      lg: { top: "10", bottom: "10", left: "10", right: "10", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner:hover{ border-radius: {{contentWrapHoverRadius}}; }",
      },
    ],
  },
  contentWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 0, right: 5, bottom: 15, left: 0 },
      color: "rgba(0,0,0,0.15)",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-inner" }],
  },
  contentWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 0, right: 10, bottom: 25, left: 0 },
      color: "rgba(0,0,0,0.25)",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-inner:hover" }],
  },
  contentWrapPadding: {
    type: "object",
    default: {
      lg: { top: "50", bottom: "50", left: "50", right: "50", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner { padding: {{contentWrapPadding}}; }",
      },
    ],
  },
  contentMargin: {
    type: "object",
    default: {
      lg: { top: "", bottom: "", left: "", right: "", unit: "px" },
    },
    style: [
      {
        selector: 
          "{{ULTP}} .ultp-block-content-inner { margin: {{contentMargin}}; }",
      },
    ],
  },

  /*==========================`
      Heading Settings
  ==========================*/
  headingShow: { type: "boolean", default: false },
  headingText: { type: "string", default: "Post Slider #1" },
  
  /* ========= Advanced Settings ========= */
  ...commonAttributes([
    'advanceAttr', 
    'heading', 
    'meta', 
    'category', 
    'readMore', 
    'query'
  ],
  [],
  [
    // Query 
    {
      key: 'queryNumPosts',
      default: { lg: 5 }
    },
    {
      key: 'queryNumber',
      default: 5
    },
    // Meta
    {
      key: 'metaSeparator',
      default: "dash"
    },
    {
      key: 'metaColor',
      default: '#989898'
    }, 
    {
      key: 'metaSeparatorColor',
      default: '#b3b3b3'
    },
    // Category
    {
      key: "catSacing",
      default: { lg: { top: -65, bottom: 5, left: 0, right: 0, unit: "px" } }
    },
    {
      key: 'catPadding',
      default: { lg: { top: 8, bottom: 6, left: 16, right: 16, unit: 'px'} }
    },
    // read More 
    { key: 'readMore', default: true },
    {
      key: 'readMoreColor', default: '#000'
    },
    {
      key: 'readMoreBgColor',
      default: { openColor: 0, type: "color", color: "#037fff" },
    },
    {
      key: 'readMoreHoverColor',
      default: "#037fff"
    },
    {
      key: 'readMoreBgHoverColor',
      default: { openColor: 0, type: "color", color: "#0c32d8" },
    },
    {
      key: 'readMoreSacing',
      default: { lg: { top: 30, bottom: "", left: "", right: "", unit: "px" } },
    },
    {
      key: 'readMorePadding',
      default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
    }
  ]),

  //Compatibility
  V4_1_0_CompCheck,
  ...getDCBlockAttributes(),
  /*==========================`
      Query Settings
  ==========================*/
  // Post Slider 1
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 5 }
  // },
  // queryNumber: {
  //   type: "string",
  //   default: 5,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // metaKey: {
  //   type: "string",
  //   default: "custom_meta_key",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrder: { type: "string", default: "desc" },
  // queryInclude: { type: "string", default: "" },
  // queryExclude: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },
  
  /*==========================`
      Meta Settings
  ==========================*/
  // // Post Slider 1 
  // metaShow: { type: "boolean", default: true },
  // metaPosition: { type: "string", default: "top" },
  // metaStyle: { type: "string", default: "icon" },
  // authorLink: { type: "boolean", default: true },
  // metaSeparator: { type: "string", default: "dash" },
  // metaList: { type: "string", default: '["metaAuthor","metaDate","metaRead"]' },
  // metaMinText: { type: "string", default: "min read" },
  // metaAuthorPrefix: { type: "string", default: "By" },
  // metaDateFormat: { type: "string", default: "M j, Y" },
  // metaTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element,  
  //         {{ULTP}} .ultp-block-item span.ultp-block-meta-element a`,
  //     },
  //   ],
  // },
  // metaColor: {
  //   type: "string",
  //   default: "#989898",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { color: {{metaColor}}; }  
  //         {{ULTP}} span.ultp-block-meta-element svg { fill: {{metaColor}}; }  
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element a { color: {{metaColor}}; }`,
  //     },
  //   ],
  // },
  // metaHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element:hover ,  
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:hover a { color: {{metaHoverColor}}; }  
  //         {{ULTP}} span.ultp-block-meta-element:hover svg { fill: {{metaHoverColor}}; }`,
  //     },
  //   ],
  // },
  // metaSeparatorColor: {
  //   type: "string",
  //   default: "#b3b3b3",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: 
  //       `{{ULTP}} .ultp-block-meta-dot span:after { background:{{metaSeparatorColor}}; }  
  //       {{ULTP}} span.ultp-block-meta-element:after { color:{{metaSeparatorColor}}; }`,
  //     },
  //   ],
  // },
  // metaSpacing: {
  //   type: "object",
  //   default: { lg: "15", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { margin-right:{{metaSpacing}}; }  
  //         {{ULTP}} span.ultp-block-meta-element { padding-left: {{metaSpacing}}; }`,
  //     },
  //   ],
  // },
  // metaMargin: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { margin:{{metaMargin}}; }",
  //     },
  //   ],
  // },
  // metaPadding: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "5", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { padding:{{metaPadding}}; }",
  //     },
  //   ],
  // },
  // metaBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: "0", bottom: "0", left: "0" },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta",
  //     },
  //   ],
  // },
  // metaBg: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { background:{{metaBg}}; }",
  //     },
  //   ],
  // },
  
  /*==========================`
      Category Settings
  ==========================*/
  // Post Slider 1
  // catShow: { type: "boolean", default: true },
  // maxTaxonomy: { type: "string", default: "30" },
  // taxonomy: { type: "string", default: "category" },
  // catStyle: { type: "string", default: "classic" },
  // catPosition: { type: "string", default: "aboveTitle" },
  // customCatColor: { type: "boolean", default: false },
  // seperatorLink: {
  //   type: "string",
  //   default: ultp_data.category_url,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // onlyCatColor: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // catLineWidth: {
  //   type: "object",
  //   default: { lg: "20" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after,  
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before { width:{{catLineWidth}}px; }`,
  //     },
  //   ],
  // },
  // catLineSpacing: {
  //   type: "object",
  //   default: { lg: "30" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth .ultp-category-in { padding-left: {{catLineSpacing}}px; padding-right: {{catLineSpacing}}px; }  
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in { padding-left: {{catLineSpacing}}px; }  
  //         {{ULTP}} .ultp-category-borderRight .ultp-category-in { padding-right:{{catLineSpacing}}px; }`,
  //     },
  //   ],
  // },
  // catLineColor: {
  //   type: "string",
  //   default: "#000",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after,  
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before { background:{{catLineColor}}; }`,
  //     },
  //   ],
  // },
  // catLineHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:after,  
  //         {{ULTP}} .ultp-category-borderLeft:hover .ultp-category-in:before,  
  //         {{ULTP}} .ultp-category-borderRight:hover .ultp-category-in:before { background:{{catLineHoverColor}}; }`,
  //     },
  //   ],
  // },
  // catTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: 15, unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "500",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //   ],
  // },
  // catBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "#000" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catRadius: {
  //   type: "object",
  //   default: { lg: "2", unit: "px" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //   ],
  // },
  // catHoverColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //   ],
  // },
  // catBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "#037fff" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catSacing: {
  //   type: "object",
  //   default: { lg: { top: -65, bottom: 5, left: 0, right: 0, unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-category-grid { margin:{{catSacing}}; }",
  //     },
  //   ],
  // },
  // catPadding: {
  //   type: "object",
  //   default: { lg: { top: 8, bottom: 6, left: 16, right: 16, unit: "px" } },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //   ],
  // },
  
  /*==========================`
      ReadMore Settings
  ==========================*/
  // Post Slider 1
  // readMore: { type: "boolean", default: true },
  // readMoreText: { type: "string", default: "" },
  // readMoreIcon: { type: "string", default: "rightArrowLg" },
  // readMoreTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "500",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreIconSize: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore svg{ width:{{readMoreIconSize}}; }",
  //     },
  //   ],
  // },
  // readMoreColor: {
  //   type: "string",
  //   default: "#000",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { color:{{readMoreColor}}; }  
  //         {{ULTP}} .ultp-block-readmore a svg { fill:{{readMoreColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "#037fff" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreRadius: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { border-radius:{{readMoreRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a:hover { color:{{readMoreHoverColor}}; }  
  //         {{ULTP}} .ultp-block-readmore a:hover svg { fill:{{readMoreHoverColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "#0c32d8" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverRadius: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreSacing: {
  //   type: "object",
  //   default: { lg: { top: 30, bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore { margin:{{readMoreSacing}}; }",
  //     },
  //   ],
  // },
  // readMorePadding: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a { padding:{{readMorePadding}}; }",
  //     },
  //   ],
  // },
  /*==========================`
      Heading Settings
  ==========================*/
  // headingURL: { type: "string", default: "" },
  // headingBtnText: {
  //   type: "string",
  //   default: "View More",
  //   style: [
  //     { depends: [{ key: "headingStyle", condition: "==", value: "style11" }] },
  //   ],
  // },
  // headingStyle: { type: "string", default: "style1" },
  // headingTag: { type: "string", default: "h2" },
  // headingAlign: {
  //   type: "string",
  //   default: "left",
  //   style: [
  //     {
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner,  
  //         {{ULTP}} .ultp-sub-heading-inner { text-align:{{headingAlign}}; }`,
  //     },
  //   ],
  // },
  // headingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "20", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "700",
  //   },
  //   style: [{ selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-inner" }],
  // },
  // headingColor: {
  //   type: "string",
  //   default: "#0e1523",
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { color:{{headingColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor: {
  //   type: "string",
  //   default: "#0e1523",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before,  
  //         {{ULTP}} .ultp-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor2: {
  //   type: "string",
  //   default: "#e5e5e5",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //   ],
  // },
  // headingBg: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-style5 .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; }  
  //         {{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span,  
  //         {{ULTP}} .ultp-heading-inner span:after { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; }
  //         {{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }`,
  //     },
  //   ],
  // },
  // headingBg2: {
  //   type: "string",
  //   default: "#e5e5e5",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg2}}; }",
  //     },
  //   ],
  // },
  // headingBtnTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "14", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-btn",
  //     },
  //   ],
  // },
  // headingBtnColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn { color:{{headingBtnColor}}; }  
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn svg { fill:{{headingBtnColor}}; }`,
  //     },
  //   ],
  // },
  // headingBtnHoverColor: {
  //   type: "string",
  //   default: "#0a31da",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover { color:{{headingBtnHoverColor}}; }  
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }`,
  //     },
  //   ],
  // },
  // headingBorder: {
  //   type: "string",
  //   default: "3",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before,  
  //         {{ULTP}} .ultp-heading-inner span:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before,  
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before,  
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before,  
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //   ],
  // },
  // headingSpacing: {
  //   type: "object",
  //   default: { lg: 20, sm: 10, unit: "px" },
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}}; }",
  //     },
  //   ],
  // },
  // headingRadius: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //   ],
  // },
  // headingPadding: {
  //   type: "object",
  //   default: { lg: { unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //   ],
  // },
  // subHeadingShow: { type: "boolean", default: false },
  // subHeadingText: {
  //   type: "string",
  //   default:
  //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.",
  //   style: [
  //     { depends: [{ key: "subHeadingShow", condition: "==", value: true }] },
  //   ],
  // },
  // subHeadingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "16", unit: "px" },
  //     spacing: { lg: "0", unit: "px" },
  //     height: { lg: "27", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "500",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-sub-heading div",
  //     },
  //   ],
  // },
  // subHeadingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading div { color:{{subHeadingColor}}; }",
  //     },
  //   ],
  // },
  // subHeadingSpacing: {
  //   type: "object",
  //   default: { lg: { top: "8", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading-inner { margin:{{subHeadingSpacing}}; }",
  //     },
  //   ],
  // },
  // enableWidth: {
  //   type: 'toggle',
  //   default: false,
  //   style: [{
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //   }],
  // },
  // customWidth: {
  //     type: "object",
  //     default: { lg: { top: "", unit: "px" }},
  //     style: [{
  //         depends: [
  //             { key: "subHeadingShow", condition: "==", value: true },
  //             { key: "enableWidth", condition: "==", value: true }
  //         ],
  //         selector: "{{ULTP}} .ultp-sub-heading-inner { max-width:{{customWidth}}; }",
  //     }],
  // },

};
export default attributes;
