const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { useEffect, useState, useRef, Fragment } = wp.element;
const { Spinner, Placeholder } = wp.components;
import {
	attrBuild,
	FeatureToggleArgsDep,
	FeatureToggleArgsNew,
	isInlineCSS,
	isReload,
	updateCurrentPostId
} from '../../helper/CommonPanel';
import Category from '../../helper/Components/grid-category';
import Excerpt from '../../helper/Components/grid-excerpt';
import { GHeading2 } from '../../helper/Components/grid-heading';
import { Image8, Video } from '../../helper/Components/grid-image';
import LoadMore from '../../helper/Components/grid-loadmore';
import Meta from '../../helper/Components/grid-meta';
import Navigation from '../../helper/Components/grid-nav';
import Pagination from '../../helper/Components/grid-pagination';
import ReadMore from '../../helper/Components/grid-readmore';
import Title from '../../helper/Components/grid-title';
import { CssGenerator } from '../../helper/CssGenerator';
import ToolBarElement from '../../helper/ToolBarElement';
import { handleCompatibility } from '../../helper/compatibility';
import { isDCActive } from '../../helper/dynamic_content';
import AddDCButton from '../../helper/dynamic_content/AddDCButton';
import MetaGroup from '../../helper/dynamic_content/MetaGroup';
import {
	stateObj,
	SETTING_SECTIONS,
    resetState,
    restoreState,
    saveSelectedSection,
    saveToolbar,
    scrollSidebarSettings,
} from '../../helper/gridFunctions';
import PL3_Settings, { AddSettingsToToolbar, MAX_CUSTOM_META_GROUPS } from './Settings';
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [state, setState] = useState(stateObj);

	const { setAttributes, name, clientId, className, isSelected, context, attributes, attributes: { blockId, previewImg, readMoreIcon, imgCrop, excerptLimit, showFullExcerpt, columns, metaStyle, metaShow, catShow, readMore, readMoreText, showImage, metaSeparator, titleShow, catStyle, catPosition, titlePosition, excerptShow, imgAnimation, imgOverlayType, imgOverlay, metaList, varticalAlign, imgFlip, metaPosition, layout, customCatColor, onlyCatColor, contentTag, titleTag, showSeoMeta, titleLength, metaMinText, metaAuthorPrefix, titleStyle, metaDateFormat, authorLink, fallbackEnable, vidIconEnable, notFoundMessage, dcEnabled, dcFields, advanceId, paginationShow, paginationAjax, headingShow, filterShow, paginationType, navPosition, paginationNav, loadMoreText, paginationText, V4_1_0_CompCheck: { runComp }, currentPostId } } = props

	useEffect(() => {
		resetState();
		fetchProducts();
	}, []);

	function setSelectedDc(selectedDC) {
		setState({ ...state, selectedDC });
	}

	function selectParentMetaGroup(groupIdx) {
		setSelectedDc(groupIdx);
		setToolbarSettings('dc_group');
	}

	function startMetaFieldOnboarding() {
		setState({
			...state,
			selectedDC: '0,0,1',
			toolbarSettings: 'dc_field',
		});
	}

	function setSection(title) {
		setState({
			...state,
			section: {
				...SETTING_SECTIONS,
				[title]: true,
			},
		});
		scrollSidebarSettings(title);
		saveSelectedSection(title);
	}

	function setToolbarSettings(title) {
		setState(prev => ({ ...prev, toolbarSettings: title }));
		saveToolbar(title);
	}

    useEffect(() => {
        const _client = clientId.substr(0, 6);
        const reference = getBlockAttributes( getBlockRootClientId(clientId) );
        updateCurrentPostId(setAttributes, reference, currentPostId, clientId );

		handleCompatibility(props);

		if (!blockId) {
			setAttributes({ blockId: _client });
			if (ultp_data.archive && ultp_data.archive == 'archive') {
				setAttributes({ queryType: 'archiveBuilder' });
			}
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [blockId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (isDCActive() && attributes.dcFields.length === 0) {
			setAttributes({
				dcFields: Array(MAX_CUSTOM_META_GROUPS).fill(undefined),
			});
		}
		if (prevAttributes) {
			if (isReload(prevAttributes, attributes)) {
				fetchProducts();
				prevPropsRef.current = attributes;
			}
			if (prevAttributes.isSelected !== isSelected && isSelected) {
				restoreState(setSection, setToolbarSettings);
			}
		} else {
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	function fetchProducts() {
		if (state.error) {
			setState({ ...state, error: false });
		}
		if (!state.loading) {
			setState({ ...state, loading: true });
		}
		wp.apiFetch({
			path: '/ultp/fetch_posts',
			method: 'POST',
			data: attrBuild(attributes),
		})
		.then((obj) => {
			setState({ ...state, postsList: obj, loading: false });
		})
		.catch((error) => {
			setState({ ...state, loading: false, error: true });
		});
	}

	function renderContent() {
		const CustomTag = `${contentTag}`;
        const customMeta = (idx, postId) => {
			return (
				isDCActive() &&
				dcEnabled && (
					<MetaGroup
						idx={idx}
						postId={postId}
						fields={dcFields}
						settingsOnClick={(e, name) => {
							e?.stopPropagation();
							setToolbarSettings(name);
						}}
						selectedDC={state.selectedDC}
						setSelectedDc={setSelectedDc}
						setAttributes={setAttributes}
						dcFields={dcFields}
					/>
				)
			);
		};

		if (previewImg) {
			return (
				<img
					style={{ marginTop: '0px', width: '420px' }}
					src={previewImg}
				/>
			);
		}

		return !state.error ? (
			!state.loading ? (
				state.postsList.length > 0 ? (
					<div
						className={`ultp-block-items-wrap ultp-block-row ultp-block-column-${columns.lg} ultp-block-content-${varticalAlign} ultp-block-content-${imgFlip} ultp-${layout}`}
					>
						{state.postsList.map((post, idx) => {
							const meta = JSON.parse(
								metaList.replaceAll('u0022', '"')
							);
							return (
								<CustomTag
									key={idx}
									className={`ultp-block-item ultp-block-media post-id-${post.ID}`}
								>
									<div className={`ultp-block-content-wrap`}>
										<Image8
											imgOverlay={imgOverlay}
											imgOverlayType={imgOverlayType}
											imgAnimation={imgAnimation}
											post={post}
											fallbackEnable={fallbackEnable}
											vidIconEnable={vidIconEnable}
											showImage={showImage}
											idx={idx}
											imgCrop={imgCrop}
											onClick={() => {
												setSection('image');
												setToolbarSettings('image');
											}}
											Video={
												vidIconEnable &&
												post.has_video ? (
													<Video
														onClick={() => {
															setSection('video');
															setToolbarSettings('video');
														}}
													/>
												) : null
											}
											Category={
												catPosition != 'aboveTitle' ? (
													<div
														className={`ultp-category-img-grid`}
													>
														<Category
															post={post}
															catShow={catShow}
															catStyle={catStyle}
															catPosition={
																catPosition
															}
															customCatColor={
																customCatColor
															}
															onlyCatColor={
																onlyCatColor
															}
															onClick={(e) => {
																setSection('taxonomy-/-category');
																setToolbarSettings('cat');
															}}
														/>
													</div>
												) : null
											}
										/>

										<div className={`ultp-block-content`}>

                                            {customMeta(7, post.ID)}

											{catPosition == 'aboveTitle' && (
												<Category
													post={post}
													catShow={catShow}
													catStyle={catStyle}
													catPosition={catPosition}
													customCatColor={
														customCatColor
													}
													onlyCatColor={onlyCatColor}
													onClick={(e) => {
														setSection('taxonomy-/-category');
														setToolbarSettings('cat');
													}}
												/>
											)}

                                            {customMeta(6, post.ID)}

											{post.title &&
												titleShow &&
												titlePosition == true && (
													<Title
														title={post.title}
														headingTag={titleTag}
														titleLength={
															titleLength
														}
														titleStyle={titleStyle}
														onClick={(e) => {
															setSection('title');
															setToolbarSettings('title');
														}}
													/>
												)}
                                            {customMeta(5, post.ID)}
											{metaShow &&
												metaPosition == 'top' && (
													<Meta
														meta={meta}
														post={post}
														metaSeparator={
															metaSeparator
														}
														metaStyle={metaStyle}
														metaMinText={
															metaMinText
														}
														metaAuthorPrefix={
															metaAuthorPrefix
														}
														metaDateFormat={
															metaDateFormat
														}
														authorLink={authorLink}
														onClick={(e) => {
															setSection('meta');
															setToolbarSettings('meta');
														}}
													/>
												)}
                                            {customMeta(4, post.ID)}
											{post.title &&
												titleShow &&
												titlePosition == false && (
													<Title
														title={post.title}
														headingTag={titleTag}
														titleLength={
															titleLength
														}
														titleStyle={titleStyle}
														onClick={(e) => {
															setSection('title');
															setToolbarSettings('title');
														}}
													/>
												)}
                                            {customMeta(3, post.ID)}
											{excerptShow && (
												<div
													className={`ultp-block-excerpt`}
												>
													<Excerpt
														excerpt={post.excerpt}
														excerpt_full={
															post.excerpt_full
														}
														seo_meta={post.seo_meta}
														excerptLimit={
															excerptLimit
														}
														showFullExcerpt={
															showFullExcerpt
														}
														showSeoMeta={
															showSeoMeta
														}
														onClick={(e) => {
															setSection('excerpt');
															setToolbarSettings('excerpt');
														}}
													/>
												</div>
											)}
                                            {customMeta(2, post.ID)}
											{readMore && (
												<ReadMore
													readMoreText={readMoreText}
													readMoreIcon={readMoreIcon}
													titleLabel={post.title}
													onClick={() => {
														setSection('read-more');
														setToolbarSettings('read-more');
													}}
												/>
											)}
                                            {customMeta(1, post.ID)}
											{metaShow &&
												metaPosition == 'bottom' && (
													<Meta
														meta={meta}
														post={post}
														metaSeparator={
															metaSeparator
														}
														metaStyle={metaStyle}
														metaMinText={
															metaMinText
														}
														metaAuthorPrefix={
															metaAuthorPrefix
														}
														metaDateFormat={
															metaDateFormat
														}
														authorLink={authorLink}
														onClick={(e) => {
															setSection('meta');
															setToolbarSettings('meta');
														}}
													/>
												)}
                                            {customMeta(0, post.ID)}

                                            {isDCActive() && dcEnabled && (
												<AddDCButton
													dcFields={dcFields}
													setAttributes={setAttributes}
													startOnboarding={startMetaFieldOnboarding}
												/>
											)}
										</div>
									</div>
								</CustomTag>
							);
						})}
						{/* <span className={`ultp-loadmore-insert-before`}></span> */}
					</div>
				) : (
					<Placeholder
						className={`ultp-backend-block-loading`}
						label={notFoundMessage}
					></Placeholder>
				)
			) : (
				<Placeholder
					className={`ultp-backend-block-loading`}
					label={__('Loading....', 'ultimate-post')}
				>
					<Spinner />
				</Placeholder>
			)
		) : (
			<Placeholder
				label={__('Posts are not available.', 'ultimate-post')}
			>
				<div style={{ marginBottom: 15 }}>
					{__('Make sure Add Post.', 'ultimate-post')}
				</div>
			</Placeholder>
		);
	}

		const store = {
			setAttributes,
			name,
			attributes,
			setSection,
			section: state.section,
			clientId,
			context,
            setSelectedDc,
			selectedDC: state.selectedDC,
			selectParentMetaGroup
		};

		let __preview_css;
		if (blockId) {
			__preview_css = CssGenerator(
				attributes,
				'ultimate-post/post-list-3',
				blockId,
				isInlineCSS()
			);
		}

		return (
			<Fragment>
				<AddSettingsToToolbar
					store={store}
					selected={state.toolbarSettings}
				/>
				<ToolBarElement
					include={[
						{
							type: 'query',
						},
						{
							type: 'template',
						},
						{
							type: 'grid_align',
							key: 'contentAlign',
						},
						{
							type: 'grid_spacing',
							include: [
								{
									position: 1,
									data: {
										type: 'range',
										key: 'rowSpace',
										min: 0,
										max: 80,
										step: 1,
										responsive: true,
										label: __('Row Gap', 'ultimate-post'),
									},
								},
							],
							exclude: [
								'spaceSep',
								'wrapOuterPadding',
								'wrapMargin',
							],
						},
						{
							type: 'layout',
							block: 'post-list-3',
							key: 'layout',
							options: [
								{
									img: 'assets/img/layouts/pl3/l1.png',
									label: __('Layout 1', 'ultimate-post'),
									value: 'layout1',
									pro: false,
								},
								{
									img: 'assets/img/layouts/pl3/l2.png',
									label: __('Layout 2', 'ultimate-post'),
									value: 'layout2',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pl3/l3.png',
									label: __('Layout 3', 'ultimate-post'),
									value: 'layout3',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pl3/l4.png',
									label: __('Layout 4', 'ultimate-post'),
									value: 'layout4',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pl3/l5.png',
									label: __('Layout 5', 'ultimate-post'),
									value: 'layout5',
									pro: true,
								},
							],
						},
						{
							type: 'feat_toggle',
							label: __('Post List Features', 'ultimate-post'),
							new: FeatureToggleArgsNew,
							[runComp ? 'exclude' : 'dep']: FeatureToggleArgsDep,
						},
					]}
					store={store}
				/>

				<InspectorControls>
					<PL3_Settings store={store} />
				</InspectorControls>

				<div
					{...(advanceId && { id: advanceId })}
					className={`ultp-block-${blockId} ${className}`}
					onClick={(e) => {
						e.stopPropagation();
						setSection('general');
						setToolbarSettings('');
					}}
				>
					{__preview_css && (
						<style
							dangerouslySetInnerHTML={{ __html: __preview_css }}
						></style>
					)}

					<div className={`ultp-block-wrapper`}>
						{(headingShow || filterShow || paginationShow) && (
							<GHeading2
								attributes={attributes}
								setAttributes={setAttributes}
								onClick={() => {
									setSection('heading');
									setToolbarSettings('heading');
								}}
								setSection={setSection}
								setToolbarSettings={setToolbarSettings}
							/>
						)}

						{renderContent()}

						{paginationShow && paginationType == 'loadMore' && (
							<LoadMore
								loadMoreText={loadMoreText}
								onClick={(e) => {
									setSection('pagination');
									setToolbarSettings('pagination');
								}}
							/>
						)}
						{paginationShow &&
							paginationType == 'navigation' &&
							navPosition != 'topRight' && (
								<Navigation
									onClick={() => {
										setSection('pagination');
										setToolbarSettings('pagination');
									}}
								/>
							)}
						{paginationShow && paginationType == 'pagination' && (
							<Pagination
								paginationNav={paginationNav}
								paginationAjax={paginationAjax}
								paginationText={paginationText}
								onClick={(e) => {
									setSection('pagination');
									setToolbarSettings('pagination');
								}}
							/>
						)}
					</div>
				</div>
			</Fragment>
		);
	}