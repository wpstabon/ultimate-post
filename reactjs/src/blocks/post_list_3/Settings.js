const { __ } = wp.i18n;
import {
    WrapStyle,
    SeparatorStyle,
    CounterStyle,
    PaginationContent,
    FilterContent,
    GeneralAdvanced,
    CustomCssAdvanced,
    ResponsiveAdvanced,
    HeadingContent,
    ImageStyle,
    ReadMoreStyle,
    TitleStyle,
    CategoryStyle,
    ExcerptStyle,
    MetaStyle,
    VideoStyle,
    blockSupportLink,
    getTitleToolbarSettings,
    getExcerptToolbarSettings,
    getMetaToolbarSettings,
    getReadMoreToolbarSettings,
    getCategoryToolbarSettings,
    TypographyTB,
    ToolbarSettingsAndStyles,
    imageSettingsKeys,
    imageStyleKeys,
    ImageStyleArg,
    getVideoToolbarSettings,
    readMoreSettings,
    readMoreStyle,
    ReadMoreStyleArg,
    categorySettings,
    categoryStyle,
    CategoryStyleArg,
    metaSettings,
    metaStyle,
    MetaStyleArg,
    metaText,
    pagiSettings,
    pagiStyles,
    PaginationContentArg,
    getPagiToolbarSettings,
    headingSettings,
    headingStyle,
    HeadingContentArg,
    filterSettings,
    filterStyle,
    FilterContentArg,
    getFilterToolbarSettings,
    spaceingIcon as spacingIcon,
    colorIcon,
    settingsIcon,
    metaTextIcon,
    typoIcon,
    GeneralContentWithQuery,
    videoColors,
} from "../../helper/CommonPanel";
import DeperciatedSettings from "../../helper/Components/DepreciatedSettings";
import ExtraToolbarSettings from "../../helper/Components/ExtraToolbarSettings";
import UltpToolbarGroup from "../../helper/Components/UltpToolbarGroup";
import { Sections, Section } from "../../helper/Sections";
import TemplateModal from "../../helper/TemplateModal";
import { isDCActive } from "../../helper/dynamic_content";
import DCToolbar from "../../helper/dynamic_content/DCToolbar";
import { handleAdvOptions } from "../../helper/gridFunctions";
const { useEffect } = wp.element;

export const MAX_CUSTOM_META_GROUPS = 8;

const PL3_Settings = (props) => {
    const { store } = props;
    const {
        layout,
        queryType,
        advFilterEnable,
        advPaginationEnable,
        V4_1_0_CompCheck: { runComp },
    } = store.attributes;
    const { context, section } = store;

    useEffect(() => {
        handleAdvOptions(context, advFilterEnable, store, advPaginationEnable);
    }, [advFilterEnable, advPaginationEnable, store.clientId]);

    return (
        <>
            <TemplateModal
                prev="https://www.wpxpo.com/postx/blocks/#demoid6838"
                store={store}
            />
            <Sections>
                <Section slug="setting" title={__("Setting", "ultimate-post")}>
                    <GeneralContentWithQuery
                        store={store}
                        initialOpen={true}
                        include={[
                            {
								position: 0,
								data: {
									type: isDCActive() ? 'toggle' : '',
									label: __(
										'Enable Dynamic Content',
										'ultimate-post'
									),
									key: 'dcEnabled',
									help: __(
										'Insert dynamic data & custom fields that update automatically.',
										'ultimate-post'
									),
								},
							},
                            {
                                position: 1,
                                data: {
                                    type: "advFilterEnable",
                                    key: "advFilterEnable",
                                },
                            },

                            {
                                position: 2,
                                data: {
                                    type: "advPagiEnable",
                                    key: "advPaginationEnable",
                                },
                            },

                            {
                                position: 3,
                                data: { type: "separator" },
                            },

                            {
                                position: 4,
                                data: {
                                    type: "layout",
                                    block: "post-list-3",
                                    key: "layout",
                                    label: __("Layout", "ultimate-post"),
                                    options: [
                                        {
                                            img: "assets/img/layouts/pl3/l1.png",
                                            label: __(
                                                "Layout 1",
                                                "ultimate-post"
                                            ),
                                            value: "layout1",
                                            pro: false,
                                        },
                                        {
                                            img: "assets/img/layouts/pl3/l2.png",
                                            label: __(
                                                "Layout 2",
                                                "ultimate-post"
                                            ),
                                            value: "layout2",
                                            pro: true,
                                        },
                                        {
                                            img: "assets/img/layouts/pl3/l3.png",
                                            label: __(
                                                "Layout 3",
                                                "ultimate-post"
                                            ),
                                            value: "layout3",
                                            pro: true,
                                        },
                                        {
                                            img: "assets/img/layouts/pl3/l4.png",
                                            label: __(
                                                "Layout 4",
                                                "ultimate-post"
                                            ),
                                            value: "layout4",
                                            pro: true,
                                        },
                                        {
                                            img: "assets/img/layouts/pl3/l5.png",
                                            label: __(
                                                "Layout 5",
                                                "ultimate-post"
                                            ),
                                            value: "layout5",
                                            pro: true,
                                        },
                                    ],
                                    variation: {
                                        layout1: {
                                            imgWidth: { lg: "45", ulg: "%" },
                                            catSacing: {
                                                lg: {
                                                    top: "0",
                                                    bottom: "10",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "15",
                                                    bottom: "5",
                                                    unit: "px",
                                                },
                                            },
                                            contentWrapBorder: {
                                                width: {
                                                    top: "0.5",
                                                    right: "0.5",
                                                    bottom: "0.5",
                                                    left: "0.5",
                                                    unit: "px",
                                                },
                                                type: "solid",
                                                color: "#969696",
                                                openBorder: 0,
                                            },
                                            contentWrapInnerPadding: {
                                                lg: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                            },
                                        },
                                        layout2: {
                                            imgWidth: { lg: "50", ulg: "%" },
                                            catSacing: {
                                                lg: {
                                                    top: "0",
                                                    bottom: "10",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "15",
                                                    bottom: "5",
                                                    unit: "px",
                                                },
                                            },
                                            contentWrapBorder: {
                                                width: {
                                                    top: "0.5",
                                                    right: "0.5",
                                                    bottom: "0.5",
                                                    left: "0.5",
                                                    unit: "px",
                                                },
                                                type: "solid",
                                                color: "#969696",
                                                openBorder: 0,
                                            },
                                            contentWrapInnerPadding: {
                                                lg: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                            },
                                        },
                                        layout3: {
                                            imgWidth: { lg: "50", ulg: "%" },
                                            catSacing: {
                                                lg: {
                                                    top: "0",
                                                    bottom: "10",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "0",
                                                    bottom: "5",
                                                    unit: "px",
                                                },
                                            },
                                            contentWrapBorder: {
                                                width: {
                                                    top: "0.5",
                                                    right: "0.5",
                                                    bottom: "0.5",
                                                    left: "0.5",
                                                    unit: "px",
                                                },
                                                type: "solid",
                                                color: "#969696",
                                                openBorder: 1,
                                            },
                                            contentWrapInnerPadding: {
                                                lg: {
                                                    top: "40",
                                                    bottom: "40",
                                                    left: "30",
                                                    right: "30",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "21",
                                                    bottom: "21",
                                                    left: "21",
                                                    right: "21",
                                                    unit: "px",
                                                },
                                            },
                                        },
                                        layout4: {
                                            imgWidth: { lg: "50", ulg: "%" },
                                            catSacing: {
                                                lg: {
                                                    top: "0",
                                                    bottom: "10",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "15",
                                                    bottom: "5",
                                                    unit: "px",
                                                },
                                            },
                                            contentWrapBorder: {
                                                width: {
                                                    top: "0.5",
                                                    right: "0.5",
                                                    bottom: "0.5",
                                                    left: "0.5",
                                                    unit: "px",
                                                },
                                                type: "solid",
                                                color: "#969696",
                                                openBorder: 0,
                                            },
                                            contentWrapInnerPadding: {
                                                lg: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "",
                                                    bottom: "",
                                                    left: "",
                                                    right: "",
                                                    unit: "px",
                                                },
                                            },
                                        },
                                        layout5: {
                                            imgWidth: { lg: "50", ulg: "%" },
                                            catSacing: {
                                                lg: {
                                                    top: "0",
                                                    bottom: "10",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "0",
                                                    bottom: "5",
                                                    unit: "px",
                                                },
                                            },
                                            contentWrapBorder: {
                                                width: {
                                                    top: "0.5",
                                                    right: "0.5",
                                                    bottom: "0.5",
                                                    left: "0.5",
                                                    unit: "px",
                                                },
                                                type: "solid",
                                                color: "#969696",
                                                openBorder: 1,
                                            },
                                            contentWrapInnerPadding: {
                                                lg: {
                                                    top: "40",
                                                    bottom: "40",
                                                    left: "30",
                                                    right: "30",
                                                    unit: "px",
                                                },
                                                xs: {
                                                    top: "21",
                                                    bottom: "21",
                                                    left: "21",
                                                    right: "21",
                                                    unit: "px",
                                                },
                                            },
                                        },
                                    },
                                },
                            },

                            {
                                position: 5,
                                data: { type: "separator" },
                            },

                            {
                                position: 7,
                                data: {
                                    type: "range",
                                    _inline: true,
                                    key: "rowSpace",
                                    min: 0,
                                    max: 80,
                                    step: 1,
                                    responsive: true,
                                    label: __("Row Gap", "ultimate-post"),
                                },
                            },
                            {
                                data: {
                                    type: "toggle",
                                    key: "imgFlip",
                                    label: __("Image Flip", "ultimate-post"),
                                },
                            },
                            {
                                data: {
                                    type: "toggle",
                                    key: "mobileImageTop",
                                    label: __(
                                        "Stack on Mobile",
                                        "ultimate-post"
                                    ),
                                },
                            },
                            {
                                data: {
                                    type: "tag",
                                    key: "varticalAlign",
                                    label: __(
                                        "Vertical Align",
                                        "ultimate-post"
                                    ),
                                    options: [
                                        {
                                            value: "top",
                                            label: __("Top", "ultimate-post"),
                                        },
                                        {
                                            value: "middle",
                                            label: __(
                                                "Middle",
                                                "ultimate-post"
                                            ),
                                        },
                                        {
                                            value: "bottom",
                                            label: __(
                                                "Bottom",
                                                "ultimate-post"
                                            ),
                                        },
                                    ],
                                },
                            },
                            {
                                data: {
                                    type: "text",
                                    key: "notFoundMessage",
                                    label: __(
                                        "No result found Text",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                    />

                    <TitleStyle
                        store={store}
                        depend="titleShow"
                        initialOpen={section["title"]}
                        exclude={["titleBackground"]}
                        hrIdx={[4, 8]}
                    />

                    <ImageStyle
                        isTab={true}
                        store={store}
                        initialOpen={section["image"]}
                        include={[
                            {
                                position: 3,
                                data: {
                                    type: "range",
                                    key: "imgSpacing",
                                    label: __("Img Spacing", "ultimate-post"),
                                    min: 0,
                                    max: 100,
                                    step: 1,
                                    responsive: true,
                                },
                            },
                            {
                                data: {
                                    type: "toggle",
                                    key: "fallbackEnable",
                                    label: __(
                                        "Fallback Image Enable",
                                        "ultimate-post"
                                    ),
                                },
                            },
                            {
                                data: {
                                    type: "media",
                                    key: "fallbackImg",
                                    label: __(
                                        "Fallback Image",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                        exclude={["imgMargin", "imgCropSmall"]}
                        hrIdx={[
                            {
                                tab: "settings",
                                hr: [2, 10],
                            },
                            {
                                tab: "style",
                                hr: [4],
                            },
                        ]}
                    />

                    <MetaStyle
                        isTab={true}
                        store={store}
                        depend={"metaShow"}
                        initialOpen={section["meta"]}
                        exclude={["metaListSmall"]}
                        hrIdx={[
                            {
                                tab: "settings",
                                hr: [4],
                            },
                            {
                                tab: "style",
                                hr: [7],
                            },
                        ]}
                    />

                    <CategoryStyle
                        isTab={true}
                        initialOpen={section["taxonomy-/-category"]}
                        depend="catShow"
                        store={store}
                        hrIdx={[
                            {
                                tab: "style",
                                hr: [8],
                            },
                        ]}
                    />

                    <ExcerptStyle
                        depend="excerptShow"
                        initialOpen={section["excerpt"]}
                        store={store}
                        hrIdx={[3, 6]}
                    />

                    <ReadMoreStyle
                        isTab={true}
                        store={store}
                        initialOpen={section["read-more"]}
                        depend="readMore"
                        hrIdx={[
                            {
                                tab: "style",
                                hr: [3],
                            },
                        ]}
                    />

                    <VideoStyle
                        initialOpen={section["video"]}
                        depend="vidIconEnable"
                        store={store}
                        hrIdx={[5]}
                    />

                    <SeparatorStyle
                        depend="separatorShow"
                        exclude={["septSpace"]}
                        store={store}
                    />

                    <WrapStyle
                        store={store}
                        exclude={["contenWraptWidth", "contenWraptHeight"]}
                    />

                    {layout === "layout2" && <CounterStyle store={store} />}

                    {!runComp && (
                        <DeperciatedSettings
                            open={
                                section["filter"] ||
                                section["pagination"] ||
                                section["heading"]
                            }
                        >
                            <HeadingContent
                                isTab={true}
                                store={store}
                                initialOpen={section["heading"]}
                                depend="headingShow"
                            />

                            {queryType != "posts" &&
                                queryType != "customPosts" && (
                                    <FilterContent
                                        isTab={true}
                                        store={store}
                                        initialOpen={section["filter"]}
                                        depend="filterShow"
                                        hrIdx={[
                                            {
                                                tab: "settings",
                                                hr: [5],
                                            },
                                            {
                                                tab: "style",
                                                hr: [4, 9],
                                            },
                                        ]}
                                    />
                                )}

                            <PaginationContent
                                isTab={true}
                                initialOpen={section["pagination"]}
                                depend="paginationShow"
                                store={store}
                            />
                        </DeperciatedSettings>
                    )}
                </Section>
                <Section
                    slug="advanced"
                    title={__("Advanced", "ultimate-post")}
                >
                    <GeneralAdvanced
                        initialOpen={true}
                        store={store}
                        include={[
                            {
                                position: 2,
                                data: {
                                    type: "color",
                                    key: "loadingColor",
                                    label: __("Loading Color", "ultimate-post"),
                                    pro: true,
                                },
                            },
                        ]}
                    />
                    <ResponsiveAdvanced store={store} />
                    <CustomCssAdvanced store={store} />
                </Section>
            </Sections>
            {blockSupportLink()}
        </>
    );
};

export default PL3_Settings;

export function AddSettingsToToolbar({ selected, store }) {
    const {
        V4_1_0_CompCheck: { runComp },
    } = store.attributes;

    const {
		metaShow,
		showImage,
		titleShow,
		catPosition,
		titlePosition,
		excerptShow,
		readMore,
		metaPosition,
		layout,
		fallbackEnable,
		catShow,
	} = store.attributes;

	const layoutContext = [
		metaShow && metaPosition == 'bottom',
		readMore,
		excerptShow,
		titleShow && titlePosition == false,
		metaShow && metaPosition == 'top',
		titleShow && titlePosition == true,
		catShow && catPosition == 'aboveTitle',
		fallbackEnable && showImage,
	];

	if (isDCActive() && selected.startsWith('dc_')) {
		return (
			<DCToolbar
				store={store}
				selected={selected}
				layoutContext={layoutContext}
			/>
		);
	}

    switch (selected) {
        case "filter":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Filter"}>
                    <TypographyTB
                        store={store}
                        attrKey="fliterTypo"
                        label={__("Filter Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getFilterToolbarSettings({
                            include: ["fliterSpacing", "fliterPadding"],
                            exclude: "__all",
                            title: __("Filter Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Filter Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Filter Settings", "ultimate-post")}
                        styleTitle={__("Filter Style", "ultimate-post")}
                        settingsKeys={filterSettings}
                        styleKeys={filterStyle}
                        oArgs={FilterContentArg}
                        exStyle={[
                            "fliterTypo",
                            "fliterSpacing",
                            "fliterPadding",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "heading":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Heading"}>
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Heading Settings", "ultimate-post")}
                        styleTitle={__("Heading Style", "ultimate-post")}
                        settingsKeys={headingSettings}
                        styleKeys={headingStyle}
                        oArgs={HeadingContentArg}
                    />
                </UltpToolbarGroup>
            );
        case "pagination":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Pagination"}>
                    <TypographyTB
                        store={store}
                        attrKey="pagiTypo"
                        label={__("Pagination Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getPagiToolbarSettings({
                            include: ["pagiMargin", "pagiPadding"],
                            exclude: "__all",
                            title: __("Pagination Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Pagination Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__(
                            "Pagination Settings",
                            "ultimate-post"
                        )}
                        styleTitle={__("Pagination Style", "ultimate-post")}
                        settingsKeys={pagiSettings}
                        styleKeys={pagiStyles}
                        oArgs={PaginationContentArg}
                        exStyle={["pagiTypo", "pagiMargin", "pagiPadding"]}
                    />
                </UltpToolbarGroup>
            );
        case "video":
            return (
                <UltpToolbarGroup text={"Video"}>
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getVideoToolbarSettings({
                            include: videoColors,
                            exclude: "__all",
                            title: __("Video Icon Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Video Icon Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getVideoToolbarSettings({
                            exclude: videoColors,
                        })}
                        store={store}
                        label={__("Video Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "title":
            return (
                <UltpToolbarGroup text={"Title"}>
                    <ExtraToolbarSettings
                        buttonContent={typoIcon}
                        include={getTitleToolbarSettings({
                            include: ["titleTypo"],
                            exclude: "__all",
                            title: __("Title Typography", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Title Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getTitleToolbarSettings({
                            include: ["titleColor", "titleHoverColor"],
                            exclude: "__all",
                            title: __("Title Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Title Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getTitleToolbarSettings({
                            exclude: [
                                "titleBackground",
                                "titleTypo",
                                "titleColor",
                                "titleHoverColor",
                            ],
                            title: __("Title Settings", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Title Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "excerpt":
            return (
                <UltpToolbarGroup text={"Excerpt"}>
                    <TypographyTB
                        store={store}
                        attrKey="excerptTypo"
                        label={__("Excerpt Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getExcerptToolbarSettings({
                            include: ["excerptColor"],
                            exclude: "__all",
                            title: __("Excerpt Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Excerpt Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getExcerptToolbarSettings({
                            exclude: ["excerptTypo", "excerptColor"],
                            title: __("Excerpt Settings", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Excerpt Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "meta":
            return (
                <UltpToolbarGroup text={"Meta"}>
                    <TypographyTB
                        store={store}
                        attrKey="metaTypo"
                        label={__("Meta Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getMetaToolbarSettings({
                            include: [
                                "metaSpacing",
                                "metaMargin",
                                "metaPadding",
                            ],
                            exclude: "__all",
                            title: __("Meta Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Meta Spacing", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={metaTextIcon}
                        include={getMetaToolbarSettings({
                            include: metaText,
                            exclude: "__all",
                            title: __("Meta Texts", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Meta Texts", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Meta Settings", "ultimate-post")}
                        styleTitle={__("Meta Style", "ultimate-post")}
                        settingsKeys={metaSettings}
                        styleKeys={metaStyle}
                        oArgs={MetaStyleArg}
                        exSettings={["metaListSmall"]}
                        exStyle={[
                            "metaTypo",
                            "metaMargin",
                            "metaPadding",
                            "metaSpacing",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "read-more":
            return (
                <UltpToolbarGroup text={"Read More"}>
                    <TypographyTB
                        store={store}
                        attrKey="readMoreTypo"
                        label={__("Read More Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getReadMoreToolbarSettings({
                            include: [
                                "readMoreSacing", // 💩 yes, it's a typo
                                "readMorePadding",
                            ],
                            exclude: "__all",
                            title: __("Read More Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Read More Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__(
                            "Read More Settings",
                            "ultimate-post"
                        )}
                        styleTitle={__("Read More Style", "ultimate-post")}
                        settingsKeys={readMoreSettings}
                        styleKeys={readMoreStyle}
                        oArgs={ReadMoreStyleArg}
                        exStyle={[
                            "readMoreTypo",
                            "readMoreSacing",
                            "readMorePadding",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "cat":
            return (
                <UltpToolbarGroup text={"Taxonomy/Category"}>
                    <TypographyTB
                        store={store}
                        attrKey="catTypo"
                        label={__("Category Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getCategoryToolbarSettings({
                            include: ["catSacing", "catPadding"],
                            exclude: "__all",
                            title: __("Category Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Category Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Category Settings", "ultimate-post")}
                        styleTitle={__("Category Style", "ultimate-post")}
                        settingsKeys={categorySettings}
                        styleKeys={categoryStyle}
                        oArgs={CategoryStyleArg}
                        exStyle={["catTypo", "catSacing", "catPadding"]}
                    />
                </UltpToolbarGroup>
            );
        case "image":
            return (
                <UltpToolbarGroup text={"Image"}>
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Image Settings", "ultimate-post")}
                        styleTitle={__("Image Style", "ultimate-post")}
                        settingsKeys={imageSettingsKeys}
                        styleKeys={imageStyleKeys}
                        oArgs={ImageStyleArg}
                        incSettings={[
                            {
                                data: {
                                    type: "toggle",
                                    key: "fallbackEnable",
                                    label: __(
                                        "Fallback Image Enable",
                                        "ultimate-post"
                                    ),
                                },
                            },
                            {
                                data: {
                                    type: "media",
                                    key: "fallbackImg",
                                    label: __(
                                        "Fallback Image",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                        incStyle={[
                            {
                                position: 0,
                                data: {
                                    type: "range",
                                    key: "imgSpacing",
                                    label: __("Img Spacing", "ultimate-post"),
                                    min: 0,
                                    max: 100,
                                    step: 1,
                                    responsive: true,
                                },
                            },
                        ]}
                        exSettings={["imgCropSmall"]}
                        exStyle={["imgMargin"]}
                    />
                </UltpToolbarGroup>
            );
        default:
            return null;
    }
}
