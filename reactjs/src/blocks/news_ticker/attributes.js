import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
  /*============================
    General Settings
  ============================*/
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  tickerType: { type: "string", default: "vertical" },
  tickerPositionEna: { type: "boolean", default: false },
  tickerPosition: {
    type: "string",
    default: "up",
    style: [
      {
        depends: [
          { key: "tickerPositionEna", condition: "==", value: true },
          { key: "tickerPosition", condition: "==", value: "up" },
        ],
        selector:
          `{{ULTP}} .ultp-news-sticky .ultp-newsTicker-wrap { position: fixed;width: 100%;top: 0;z-index: 101; left: 0; } 
          .admin-bar .ultp-news-sticky .ultp-newsTicker-wrap { top: 32px !important; }`,
      },
      {
        depends: [
          { key: "tickerPositionEna", condition: "==", value: true },
          { key: "tickerPosition", condition: "==", value: "down" },
        ],
        selector:
          "{{ULTP}} .ultp-news-sticky .ultp-newsTicker-wrap { position: fixed;width: 100%;bottom: 0; z-index: 9999999;left: 0; }",
      },
    ],
  },
  tickerHeading: { type: "boolean", default: true },
  tickTimeShow: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [{ key: "tickerType", condition: "!=", value: "typewriter" }],
      },
    ],
  },
  tickImageShow: { type: "boolean", default: false },
  navControlToggle: { type: "boolean", default: true },
  controlToggle: {
    type: "boolean",
    default: true,
    style: [
      {
        depends: [
          { key: "navControlToggle", condition: "==", value: true },
          { key: "TickNavStyle", condition: "!=", value: "nav2" },
        ],
      },
    ],
  },
  pauseOnHover: { type: "boolean", default: true },
  tickerDirectionVer: {
    type: "string",
    default: "up",
    style: [
      {
        depends: [
          { key: "tickerType", condition: "==", value: "vertical" },
          { key: "tickerAnimation", condition: "==", value: "slide" },
        ],
      },
    ],
  },
  tickerDirectionHorizon: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [
          { key: "tickerType", condition: "==", value: "horizontal" },
          { key: "tickerAnimation", condition: "==", value: "slide" },
        ],
      },
      { depends: [{ key: "tickerType", condition: "==", value: "marquee" }] },
    ],
  },
  tickerSpeed: {
    type: "string",
    default: "4000",
    style: [
      { depends: [{ key: "tickerType", condition: "!=", value: "marquee" }] },
    ],
  },
  marqueSpeed: {
    type: "string",
    default: "10",
    style: [
      { depends: [{ key: "tickerType", condition: "==", value: "marquee" }] },
    ],
  },
  tickerSpeedTypewriter: {
    type: "string",
    default: "50",
    style: [
      {
        depends: [{ key: "tickerType", condition: "==", value: "typewriter" }],
      },
    ],
  },
  tickerAnimation: {
    type: "string",
    default: "slide",
    style: [
      {
        depends: [
          { key: "tickerType", condition: "!=", value: "marquee" },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
      },
    ],
  },
  typeAnimation: {
    type: "string",
    default: "fadein",
    style: [
      {
        depends: [{ key: "tickerType", condition: "==", value: "typewriter" }],
      },
    ],
  },
  openInTab: { type: "boolean", default: false },
  headingText: { type: "string", default: "News Ticker" },

  /*============================
      Ticker Navigator Settings
  ============================*/
  TickNavStyle: {
    type: "string",
    default: "nav1",
    style: [
      { depends: [{ key: "navControlToggle", condition: "==", value: true }] },
    ],
  },
  TickNavIconStyle: {
    type: "string",
    default: "Angle2",
    style: [
      { depends: [{ key: "navControlToggle", condition: "==", value: true }] },
    ],
  },
  tickNavSize:{
    type: "object",
    default: { lg: { top: 6, bottom: 6, left: 6, right: 6, unit: "px" } },
    style: [
        { 
          depends: [
            { key: "TickNavStyle", condition: "==", value: "nav2" },
          ], 
          selector: "{{ULTP}} .ultp-news-ticker-controls .ultp-news-ticker-arrow { padding:{{tickNavSize}} !important; height: auto !important; width: auto !important; }"
      },
    ],
  },
  TickNavColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:after { border-color:{{TickNavColor}}}
          {{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow svg { fill:{{TickNavColor}}}`,
      },
      {
        depends: [{ key: "TickNavIconStyle", condition: "==", value: "icon2" }],
        selector:
          `{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:after ,
          {{ULTP}} button.ultp-news-ticker-arrow:before { border-right-color:{{TickNavColor}}; border-left-color: {{TickNavColor}}; } `,
      },
    ],
  },
  TickNavBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
    style: [
      {
        depends: [{ key: "TickNavStyle", condition: "==", value: "nav1" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow",
      },
      {
        depends: [{ key: "TickNavStyle", condition: "==", value: "nav2" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow",
      },
      {
        depends: [
          { key: "TickNavStyle", condition: "!=", value: "nav1" },
          { key: "TickNavStyle", condition: "!=", value: "nav2" },
        ],
        selector: "{{ULTP}} .ultp-news-ticker-controls",
      },
    ],
  },
  tickNavBorder: {
    type: "object",
    default: {
      openBorder: 0,
      disableColor: true,
      width: { top: 0, right: 0, bottom: 0, left: 0 },
      type: "solid",
    },
    style: [
      {
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav2" },
        ], 
        selector: "{{ULTP}} .ultp-news-ticker-controls .ultp-news-ticker-arrow" 
      },
    ],
  },
  TickNavHovColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:hover:after { border-color:{{TickNavHovColor}}}
          {{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow svg:hover { fill:{{TickNavHovColor}}}`,
      },
      {
        depends: [{ key: "TickNavIconStyle", condition: "==", value: "icon2" }],
        selector:
          `{{ULTP}} button.ultp-news-ticker-arrow:hover:after ,
          {{ULTP}} button.ultp-news-ticker-arrow:hover:before{ border-right-color:{{TickNavHovColor}}; border-left-color:{{TickNavHovColor}}; } `,
      },
    ],
  },
  TickNavHovBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)" },
    style: [
      {
        depends: [{ key: "TickNavStyle", condition: "==", value: "nav1" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:hover",
      },
      {
        depends: [{ key: "TickNavStyle", condition: "==", value: "nav2" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:hover",
      },
      {
        depends: [{ key: "TickNavStyle", condition: "==", value: "nav3" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-arrow:hover",
      },
      {
        depends: [
          { key: "TickNavStyle", condition: "!=", value: "nav1" },
          { key: "TickNavStyle", condition: "!=", value: "nav2" },
          { key: "TickNavStyle", condition: "!=", value: "nav3" },
        ],
        selector: "{{ULTP}} .ultp-news-ticker-controls:hover",
      },
    ],
  },
  tickNavHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      disableColor: true,
      width: { top: 0, right: 0, bottom: 0, left: 0 },
      type: "solid",
    },
    style: [
      { 
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav2" },
        ], 
        selector: "{{ULTP}} .ultp-news-ticker-controls .ultp-news-ticker-arrow:hover"  
      },
    ],
  },
  TickNavPause: {
    type: "string",
    default: "Pause Icon Style",
    style: [
      {
        depends: [
          { key: "TickNavStyle", condition: "!=", value: "nav2" },
          { key: "controlToggle", condition: "==", value: true },
        ],
      },
    ],
  },
  PauseColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [
          { key: "TickNavStyle", condition: "!=", value: "nav2" },
          { key: "controlToggle", condition: "==", value: true },
        ],
        selector:
          `{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause:before,
          {{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause:before,
          {{ULTP}} .ultp-news-ticker-nav4 button.ultp-news-ticker-pause:before { border-color:{{PauseColor}};}`,
      },
    ],
  },
  PauseHovColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [
          { key: "TickNavIconStyle", condition: "!=", value: "icon2" },
          { key: "controlToggle", condition: "==", value: true },
          { key: "TickNavStyle", condition: "==", value: "nav1" },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause:hover:before { border-color:{{PauseHovColor}};}",
      },
      {
        depends: [
          { key: "TickNavIconStyle", condition: "!=", value: "icon2" },
          { key: "controlToggle", condition: "==", value: true },
          { key: "TickNavStyle", condition: "==", value: "nav3" },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause:hover:before { border-color:{{PauseHovColor}};}",
      },
      {
        depends: [
          { key: "TickNavIconStyle", condition: "!=", value: "icon2" },
          { key: "controlToggle", condition: "==", value: true },
          { key: "TickNavStyle", condition: "==", value: "nav4" },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause:hover:before { border-color:{{PauseHovColor}};}",
      },
    ],
  },
  PauseBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Base_3_color)" },
    style: [
      {
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav1" },
          { key: "controlToggle", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause",
      },
      {
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav3" },
          { key: "controlToggle", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls button.ultp-news-ticker-pause",
      },
    ],
  },
  PauseHovBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Base_2_color)" },
    style: [
      {
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav1" },
          { key: "controlToggle", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls .ultp-news-ticker-pause:hover",
      },
      {
        depends: [
          { key: "TickNavStyle", condition: "==", value: "nav3" },
          { key: "controlToggle", condition: "==", value: true },
        ],
        selector:
          "{{ULTP}} .ultp-news-ticker-controls .ultp-news-ticker-pause:hover",
      },
    ],
  },

  /*============================
      Ticker Label Settings
  ============================*/
  tickLabelColor: {
    type: "string",
    default: "var(--postx_preset_Over_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-news-ticker-label { color:{{tickLabelColor}}; } ",
      },
    ],
  },
  tickLabelBg: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-news-ticker-label{background-color:{{tickLabelBg}};}",
      },
      {
        depends: [{ key: "tickShapeStyle", condition: "!=", value: "normal" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-label::after{ border-color:transparent transparent transparent {{tickLabelBg}} !important; }",
      },
    ],
  },
  tickLabelTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "16", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "27", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "500",
    },
    style: [{ selector: "{{ULTP}} .ultp-news-ticker-label" }],
  },
  tickLabelPadding: {
    type: "string",
    default: "15",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-news-ticker-label { padding:0px {{tickLabelPadding}}px; }",
      },
    ],
  },
  tickLabelSpace: {
    type: "object",
    default: { lg: "160" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker-box { padding-left:{{tickLabelSpace}}px; } 
          .rtl {{ULTP}} .ultp-news-ticker-box { padding-right:{{tickLabelSpace}}px !important; }`,
      },
    ],
  },
  tickShapeStyle: {
    type: "string",
    default: "normal",
    style: [
      {
        depends: [{ key: "tickShapeStyle", condition: "==", value: "large" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-label::after {border-top: 23px solid; border-left: 24px solid; border-bottom: 23px solid; right: -24px;border-color:transparent; }",
      },
      {
        depends: [{ key: "tickShapeStyle", condition: "==", value: "medium" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-label::after { border-top: 17px solid; border-left: 20px solid;border-bottom: 17px solid;border-color:transparent;right:-20px;}",
      },
      {
        depends: [{ key: "tickShapeStyle", condition: "==", value: "small" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-label::after { border-top: 12px solid;border-left: 15px solid; border-bottom: 12px solid;border-color: transparent;right: -14px; }",
      },
      {
        depends: [{ key: "tickShapeStyle", condition: "==", value: "normal" }],
        selector:
          "{{ULTP}} .ultp-news-ticker-label::after {display:none !important;}",
      },
    ],
  },

/*============================
      Ticker Body Settings
  ============================*/
  tickerContentHeight: {
    type: "string",
    default: "45",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker li,
          .editor-styles-wrapper {{ULTP}} .ultp-newsTicker-wrap ul li,
          {{ULTP}} .ultp-news-ticker-label,
          {{ULTP}} .ultp-newsTicker-wrap,
          {{ULTP}} .ultp-news-ticker-controls,
          {{ULTP}} .ultp-news-ticker-controls button { height:{{tickerContentHeight}}px; }`,
      },
    ],
  },
  tickBodyColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker li a,
          {{ULTP}} .ultp-news-ticker li div a,
          {{ULTP}} .ultp-news-ticker li div a span { color:{{tickBodyColor}}; }`,
      },
    ],
  },
  tickBodyHovColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker li a:hover,
          {{ULTP}} .ultp-news-ticker li div:hover,
          {{ULTP}} .ultp-news-ticker li div a:hover span { color:{{tickBodyHovColor}}; }`,
      },
    ],
  },
  tickBodyListColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "tickTxtStyle", condition: "==", value: "box" }],
        selector:
          "{{ULTP}} .ultp-list-box::before { background-color:{{tickBodyListColor}};}",
      },
      {
        depends: [{ key: "tickTxtStyle", condition: "==", value: "circle" }],
        selector:
          "{{ULTP}} .ultp-list-circle::before { background-color:{{tickBodyListColor}};}",
      },
      {
        depends: [{ key: "tickTxtStyle", condition: "==", value: "hand" }],
        selector:
          "{{ULTP}} .ultp-list-hand::before { color:{{tickBodyListColor}};}",
      },
      {
        depends: [
          { key: "tickTxtStyle", condition: "==", value: "right-sign" },
        ],
        selector:
          "{{ULTP}} .ultp-list-right-sign::before { color:{{tickBodyListColor}};}",
      },
      {
        depends: [
          { key: "tickTxtStyle", condition: "==", value: "right-bold" },
        ],
        selector:
          "{{ULTP}} .ultp-list-right-bold::before { color:{{tickBodyListColor}};}",
      },
    ],
  },
  tickerBodyBg: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        selector:
          `{{ULTP}} .ultp-news-ticker-box,
          {{ULTP}} .ultp-news-ticker-controls { background-color:{{tickerBodyBg}} }`,
      },
    ],
  },
  tickBodyTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "16", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "16", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "500",
    },
    style: [{ selector: "{{ULTP}} .ultp-news-ticker li a" }],
  },
  tickBodyBorderColor: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-newsTicker-wrap { background-color:{{tickBodyBorderColor}};border-color:{{tickBodyBorderColor}} }",  // its given intentionally please don't remove // given for border-radius compatibility
      },
    ],
  },
  tickBodyBorder: {
    type: "object",
    default: {
      openBorder: 0,
      disableColor: true,
      width: { top: 0, right: 0, bottom: 0, left: 0 },
      type: "solid",
    },
    style: [
      { selector: "{{ULTP}} .ultp-newsTicker-wrap" },
    ],
  },
  tickBodyRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-newsTicker-wrap,
          {{ULTP}} .ultp-news-ticker-label,
          {{ULTP}} .ultp-news-ticker-box { border-radius:{{tickBodyRadius}}; }
          {{ULTP}} .ultp-news-ticker-prev { border-top-right-radius:0px !important; border-bottom-right-radius:0px !important; border-radius: {{tickBodyRadius}}; }`,
      },
    ],
  },
  tickTxtStyle: {
    type: "string",
    default: "normal",
    style: [
      { selector: "{{ULTP}} .ultp-news-ticker li { list-style-type:none; }" },
    ],
  },
  tickImgWidth: {
    type: "string",
    default: "30",
    style: [
      {
        depends: [{ key: "tickImageShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-news-ticker li div img {width:{{tickImgWidth}}px}",
      },
    ],
  },
  tickImgSpace: {
    type: "string",
    default: "10",
    style: [
      {
        depends: [{ key: "tickImageShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-news-ticker li div img { margin-right: {{tickImgSpace}}px; } 
          .rtl {{ULTP}} .ultp-news-ticker li div img { margin-left: {{tickImgSpace}}px !important; }`,
      },
    ],
  },
  tickImgRadius: {
    type: "object",
    default: {
      lg: { top: "30", bottom: "30", left: "30", right: "30", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-news-ticker li div img { border-radius:{{tickImgRadius}}; }",
      },
    ],
  },
  tickBodySpace: {
    type: "object",
    default: { lg: "21" },
    style: [
      {
        depends: [{ key: "tickerType", condition: "==", value: "marquee" }],
        selector: "{{ULTP}} .ultp-news-ticker { gap:{{tickBodySpace}}px; }",
      },
    ],
  },

  /*============================
      Ticker Time Badge Settings
  ============================*/
  timeBadgeType: {
    type: "string",
    default: "days_ago",
  },
  timeBadgeDateFormat: { 
    type: "string", 
    default: "M j, Y",
    style: [
      {
        depends: [
          { key: "timeBadgeType", condition: "==", value: "date" },
        ],
      },
    ],
  },
  tickTimeBadge: {
    type: "string",
    default: "Time Badge",
    style: [
      {
        depends: [
          { key: "tickTimeShow", condition: "==", value: true },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
      },
    ],
  },
  timeBadgeColor: {
    type: "string",
    default: "var(--postx_preset_Base_1_color)",
    style: [
      {
        depends: [
          { key: "tickTimeShow", condition: "==", value: true },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
        selector:
          "{{ULTP}} .ultp-ticker-timebadge { color:{{timeBadgeColor}}; }",
      },
    ],
  },
  timeBadgeBg: {
    type: "object",
    default: { openColor: 1, type: "color", color: "var(--postx_preset_Contrast_1_color)" },
    style: [
      {
        depends: [
          { key: "tickTimeShow", condition: "==", value: true },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
        selector: "{{ULTP}} .ultp-ticker-timebadge",
      },
    ],
  },
  timeBadgeTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "12", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "16", unit: "px" },
      decoration: "none",
      transform: "",
      family: "",
      weight: "500",
    },
    style: [
      {
        depends: [
          { key: "tickTimeShow", condition: "==", value: true },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
        selector: "{{ULTP}} .ultp-news-ticker li .ultp-ticker-timebadge",
      },
    ],
  },
  timeBadgeRadius: {
    type: "object",
    default: {
      lg: { top: "100", bottom: "100", left: "100", right: "100", unit: "px" },
    },
    style: [
      {
        depends: [
          { key: "tickTimeShow", condition: "==", value: true },
          { key: "tickerType", condition: "!=", value: "typewriter" },
        ],
        selector:
          "{{ULTP}} .ultp-ticker-timebadge { border-radius:{{timeBadgeRadius}}; }",
      },
    ],
  },
  timeBadgePadding: {
    type: "object",
    default: { lg: { top: 3, bottom: 3, left: 6, right: 6, unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-ticker-timebadge { padding: {{timeBadgePadding}} }",
      },
    ],
  },

  /*=========== Advanced Settings =============*/
  ...commonAttributes(['advanceAttr', 'query'], ['loadingColor', 'queryInclude'], [
    {
      key: 'queryNumPosts',
      default: { lg: 4 }
    }
  ]),
  /*============================
      Query Settings
  ============================*/
  // News Ticker
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 4 }
  // },
  // queryNumber: {
  //   type: "string",
  //   default: 4,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // metaKey: {
  //   type: "string",
  //   default: "custom_meta_key",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrder: { type: "string", default: "desc" },
  // queryExclude: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },

  
};
export default attributes;
