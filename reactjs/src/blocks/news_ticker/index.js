const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import attributes from "./attributes";
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/news-ticker-block/', 'block_docs');

registerBlockType(
    'ultimate-post/news-ticker', {
        title: __('News Ticker','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/news-ticker.svg'}/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('News Ticker in the classic style.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [
            __('News Ticker','ultimate-post'),
            __('Post News','ultimate-post'),
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/newsticker.svg',
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)