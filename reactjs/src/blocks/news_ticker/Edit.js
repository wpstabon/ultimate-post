const { __ } = wp.i18n
const { dateI18n } = wp.date;
const { InspectorControls } = wp.blockEditor
const { useEffect, useState, useRef, Fragment } = wp.element
const { Spinner, Placeholder } = wp.components
import { CssGenerator } from '../../helper/CssGenerator'
import { CommonSettings, isReload, attrBuild, QueryContent, GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, isInlineCSS, blockSupportLink, updateCurrentPostId, _dateFormat } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections';
import IconPack from "../../helper/fields/tools/IconPack";
import TemplateModal from '../../helper/TemplateModal'
import ToolBarElement from '../../helper/ToolBarElement'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
	const [state, setState] = useState({ postsList: [], loading: true, error: false, section: 'Content' });
	const { setAttributes, name, className, attributes, clientId, attributes: { blockId, previewImg, advanceId, tickerHeading, tickImageShow, navControlToggle, controlToggle, tickerType, headingText, tickTimeShow, TickNavStyle, tickTxtStyle, TickNavIconStyle, timeBadgeType, timeBadgeDateFormat, currentPostId } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6);
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
			if (ultp_data.archive && ultp_data.archive == 'archive') {
				setAttributes({ queryType: 'archiveBuilder' });
			}
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (!prevAttributes) {
			prevPropsRef.current = attributes;
		} else if (isReload(prevAttributes, attributes)) {
			fetchProducts();
			prevPropsRef.current = attributes;
		}

	}, [attributes]);

	useEffect(() => {
		fetchProducts();
	}, []);

	function fetchProducts() {
		if (state.error) {
			setState({ ...state, error: false });
		}
		if (!state.loading) {
			setState({ ...state, loading: true });
		}
		wp.apiFetch({
			path: '/ultp/fetch_posts',
			method: 'POST',
			data: attrBuild(props.attributes)
		})
			.then((obj) => {
				setState({ ...state, postsList: obj, loading: false })
			}).catch((error) => {
				setState({ ...state, loading: false, error: true })
			})
	}

	function renderContent() {

		const navPauseBtn = <button aria-label="Pause Current Post" className="ultp-news-ticker-pause"></button>;
		const leftIcon = IconPack[`left${TickNavIconStyle}`];
		const rightIcon = IconPack[`right${TickNavIconStyle}`];

		return (
			!state.error ? !state.loading ? state.postsList.length > 0 ?
				<div className={`ultp-block-items-wrap`}>
					<div className={`ultp-news-ticker-${TickNavStyle} ultp-nav-${TickNavIconStyle} ultp-newsTicker-wrap ultp-newstick-${tickerType}`}>
						{(tickerHeading && headingText) &&
							<div className={`ultp-news-ticker-label`}>{headingText} <span></span> </div>
						}
						<div className="ultp-news-ticker-box">
							<ul className="ultp-news-ticker">
								{(state.postsList.map((post, idx) => {
									return (
										<li key={idx} >
											<div className={`ultp-list-${tickTxtStyle}`}>
												<a>
													{(tickImageShow && post.image ? post.image.thumbnail : undefined) &&
														<img src={post.image.thumbnail} alt={post.title} />
													}
													{post.title.replaceAll('&#038;', '').replaceAll('&#8217;', "'").replaceAll('&#8221;', '"')}
												</a>
												{tickTimeShow && (tickerType != 'typewriter') &&
													<span className="ultp-ticker-timebadge">
														{
															timeBadgeType == "date" ?
																dateI18n(_dateFormat(timeBadgeDateFormat), post.time)
																:
																post.post_time + " ago"
														}
													</span>
												}
											</div>
										</li>
									)
								}))}
							</ul>
						</div>
						{navControlToggle &&
							<div className={`ultp-news-ticker-controls  ultp-news-ticker-vertical-controls`}>
								<button aria-label="Show Previous Post" className={`ultp-news-ticker-arrow ultp-news-ticker-prev`}>{leftIcon}</button>
								{(TickNavStyle == "nav1" || TickNavStyle == "nav3" || TickNavStyle == "nav4") && controlToggle &&
									navPauseBtn
								}
								<button aria-label="Show Next Post" className={`ultp-news-ticker-arrow ultp-news-ticker-next`}>{rightIcon}</button>
							</div>
						}
					</div>
				</div>
				: (<Placeholder className={`ultp-backend-block-loading`} label={__('No Posts found', 'ultimate-post')}><Spinner /></Placeholder>)
				: (<Placeholder className={`ultp-backend-block-loading`} label={__('Loading...', 'ultimate-post')}> <Spinner /></Placeholder>)
				: (<Placeholder label={__('Posts are not available', 'ultimate-post')}><div style={{ marginBottom: 15 }}>{__('Make sure Add Post.', 'ultimate-post')}</div></Placeholder>)
		)
	}

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/news-ticker', blockId, isInlineCSS());
	}

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<InspectorControls>
				<TemplateModal
					prev="https://www.wpxpo.com/postx/blocks/#demoid6845"
					store={store}
				/>
				<Sections>
					<Section slug="setting" title={__('Setting', 'ultimate-post')}>
						<CommonSettings initialOpen={true} store={store} title={__('General', 'ultimate-post')}
							include={[
								{
									position: 1, data: {
										type: 'select', key: 'tickerType', label: __('Ticker Type', 'ultimate-post'), options: [
											{ value: 'vertical', label: __('Vertical', 'ultimate-post') },
											{ value: 'horizontal', label: __('Horizontal', 'ultimate-post') },
											{ pro: true, value: 'marquee', label: __('Marquee', 'ultimate-post') },
											{ pro: true, value: 'typewriter', label: __('Typewriter', 'ultimate-post') },
										]
									}
								},
								{
									position: 2, data: {
										type: 'select', key: 'tickerDirectionVer', label: __('Direction', 'ultimate-post'), options: [
											{ value: 'up', label: __('Up', 'ultimate-post') },
											{ value: 'down', label: __('Down', 'ultimate-post') },
										]
									}
								},
								{
									position: 3, data: {
										type: 'select', key: 'tickerDirectionHorizon', label: __('Direction', 'ultimate-post'), options: [
											{ value: 'left', label: __('Left', 'ultimate-post') },
											{ value: 'right', label: __('Right', 'ultimate-post') },
										]
									}
								},
								{ position: 4, data: { type: 'range', key: 'tickerSpeed', min: 1000, max: 10000, step: 100, label: __('Speed', 'ultimate-post') } },
								{ position: 5, data: { type: 'range', key: 'marqueSpeed', min: 1, max: 10, step: 1, label: __('Speed (Fast to Slow)', 'ultimate-post') } },
								{
									position: 6, data: {
										type: 'select', key: 'tickerAnimation', label: __('Animation', 'ultimate-post'), options: [
											{ value: 'slide', label: __('Slide', 'ultimate-post') },
											{ value: 'fadein', label: __('Fade In', 'ultimate-post') },
											{ value: 'fadeout', label: __('Fade Out', 'ultimate-post') },
										]
									}
								},
								{
									position: 7, data: {
										type: 'select', key: 'typeAnimation', label: __('Animation', 'ultimate-post'), options: [
											{ value: 'normal', label: __('Normal', 'ultimate-post') },
											{ value: 'fadein', label: __('Fade In', 'ultimate-post') },
											{ value: 'fadeout', label: __('Fade Out', 'ultimate-post') },
										]
									}
								},
								{ position: 8, data: { type: 'toggle', key: 'tickerPositionEna', pro: true, label: __('Position sticky', 'ultimate-post') } },
								{
									position: 9, data: {
										type: 'select', key: 'tickerPosition', label: __('Select Position', 'ultimate-post'), options: [
											{ value: 'up', label: __('Up', 'ultimate-post') },
											{ value: 'down', label: __('Down', 'ultimate-post') },
										]
									}
								},
								{ position: 10, data: { type: 'toggle', key: 'pauseOnHover', label: __('Pause On Hover', 'ultimate-post') } },
								{ position: 11, data: { type: 'toggle', key: 'openInTab', label: __('Open In New Tab', 'ultimate-post') } },
							]}
						/>
						<QueryContent store={store} />
						<CommonSettings depend="navControlToggle" store={store} title={__('Ticker Navigator', 'ultimate-post')}
							include={[
								{
									position: 1, data: { type: 'toggle', key: 'controlToggle', label: __('Show Toggle', 'ultimate-post') }
								},
								{
									position: 2, data: {
										type: 'select', key: 'TickNavStyle', label: __('Ticker Navigator Layout', 'ultimate-post'), options: [
											{ value: 'nav1', label: __('Style 1', 'ultimate-post') },
											{ value: 'nav2', label: __('Style 2', 'ultimate-post') },
											{ value: 'nav3', label: __('Style 3', 'ultimate-post') },
											{ value: 'nav4', label: __('Style 4', 'ultimate-post') },
										]
									}
								},
								{
									position: 3, data: {
										type: 'select', key: 'TickNavIconStyle', label: __('Ticker Icon', 'ultimate-post'), options: [
											{ value: 'Angle2', label: __('Icon 1', 'ultimate-post') },
											{ value: 'Angle', label: __('Icon 2', 'ultimate-post') },
											{ value: 'ArrowLg', label: __('Icon 3', 'ultimate-post') },
										]
									}
								},
								{
									position: 4, data: { type: 'dimension', key: 'tickNavSize', step: 1, unit: true, responsive: true, label: __('Icon Padding', 'ultimate-post') }
								},
								{
									position: 5, data: {
										type: 'tab', content: [{
											name: 'normal', title: __('Normal', 'ultimate-post'), options: [
												{
													type: 'color', key: 'TickNavColor', label: __('Icon Color', 'ultimate-post')
												},
												{
													type: 'color2', key: 'TickNavBg', label: __('Icon Bg Color', 'ultimate-post')
												},
												{
													type: 'border', key: 'tickNavBorder', label: __('Border', 'ultimate-post'),
												},
												{
													type: 'separator', key: 'TickNavPause', label: __('Pause Arrow Icon', 'ultimate-post')
												},
												{
													type: 'color', key: 'PauseColor', label: __('Pause Color', 'ultimate-post')
												},
												{
													type: 'color2', key: 'PauseBg', label: __('Pause Bg Color', 'ultimate-post')
												},
											]
										},
										{
											name: 'hover', title: __('Hover', 'ultimate-post'), options: [
												{
													type: 'color', key: 'TickNavHovColor', label: __('Hover Color', 'ultimate-post')
												},
												{
													type: 'color2', key: 'TickNavHovBg', label: __('Hover Background', 'ultimate-post')
												},
												{
													type: 'border', key: 'tickNavHoverBorder', label: __('Border', 'ultimate-post'),
												},
												{
													type: 'separator', key: 'TickNavPause', label: __('Pause Arrow Icon', 'ultimate-post')
												},
												{
													type: 'color', key: 'PauseHovColor', label: __('Pause Hover Color', 'ultimate-post')
												},
												{
													type: 'color2', key: 'PauseHovBg', label: __('Pause Hover Bg Color', 'ultimate-post')
												},
											]
										},
										]
									},
								},
							]}
						/>
						<CommonSettings depend="tickerHeading" store={store} title={__('Ticker Label', 'ultimate-post')}
							include={[
								{
									position: 1, data: {
										type: 'select', key: 'tickShapeStyle', label: __('Ticker Shape', 'ultimate-post'), options: [
											{ value: 'normal', label: __('Normal', 'ultimate-post') },
											{ pro: true, value: 'small', label: __('Small Shape', 'ultimate-post') },
											{ pro: true, value: 'medium', label: __('Medium Shape', 'ultimate-post') },
											{ pro: true, value: 'large', label: __('Large Shape', 'ultimate-post') },
										]
									}
								},
								{ position: 2, data: { type: 'text', key: 'headingText', label: __('Heading', 'ultimate-post') } },
								{ position: 3, data: { type: 'color', key: 'tickLabelColor', label: __('Color', 'ultimate-post') } },
								{ position: 4, data: { type: 'color', key: 'tickLabelBg', label: __('Label Background Color', 'ultimate-post') } },
								{ position: 5, data: { type: 'typography', key: 'tickLabelTypo', label: __('Typography', 'ultimate-post') } },
								{ position: 6, data: { type: 'range', key: 'tickLabelPadding', min: 10, max: 100, step: 1, label: __('Padding', 'ultimate-post') } },
								{ position: 7, data: { type: 'range', key: 'tickLabelSpace', min: 10, max: 300, step: 1, responsive: true, label: __('Left Content Space', 'ultimate-post') } },
							]}
						/>
						<CommonSettings store={store} title={__('Ticker Body', 'ultimate-post')}
							include={[
								{ position: 1, data: { type: 'range', key: 'tickerContentHeight', min: 10, max: 100, step: 1, label: __('Content Height', 'ultimate-post') } },
								{ position: 2, data: { type: 'color', key: 'tickBodyColor', label: __('List Text Color', 'ultimate-post') } },
								{ position: 3, data: { type: 'color', key: 'tickBodyHovColor', label: __('List Text Hover Color', 'ultimate-post') } },
								{ position: 4, data: { type: 'color', key: 'tickerBodyBg', label: __('Background Color', 'ultimate-post') } },
								{ position: 5, data: { type: 'typography', key: 'tickBodyTypo', label: __('Body Text Typography', 'ultimate-post') } },
								{ position: 6, data: { type: 'color', key: 'tickBodyBorderColor', label: __(' Border Prefix Color', 'ultimate-post') } },
								{ position: 7, data: { type: 'border', key: 'tickBodyBorder', label: __('Border', 'ultimate-post') }, },
								{ position: 8, data: { type: 'dimension', key: 'tickBodyRadius', step: 1, unit: true, responsive: true, label: __('Border Radius', 'ultimate-post') } },
								{ position: 9, data: { type: 'range', key: 'tickBodySpace', step: 1, min: 10, max: 100, responsive: true, step: 1, label: __('List Space Between', 'ultimate-post') } },
								{
									position: 10, data: {
										type: 'select', key: 'tickTxtStyle', label: __('Ticker List Style', 'ultimate-post'), options: [
											{ value: 'normal', label: __('- None -', 'ultimate-post') },
											{ value: 'circle', label: __('Circle', 'ultimate-post') },
											{ value: 'box', label: __('Box', 'ultimate-post') },
											{ value: 'hand', label: __('Hand', 'ultimate-post') },
											{ value: 'right-sign', label: __('Right Sign Icon', 'ultimate-post') },
											{ value: 'right-bold', label: __('Right Bold Sign Icon', 'ultimate-post') },
										]
									}
								},
								{ position: 11, data: { type: 'color', key: 'tickBodyListColor', label: __('List Style Color', 'ultimate-post') } },
							]}
						/>
						{(tickerType != 'typewriter') && <CommonSettings depend="tickTimeShow" store={store} title={__('Time Badge', 'ultimate-post')}
							include={[
								{
									position: 1,
									data: {
										type: 'tag',
										key: 'timeBadgeType',
										label: __('Badge Type', 'ultimate-post'),
										options: [
											{ value: 'days_ago', label: 'Days Ago' },
											{ value: 'date', label: 'Date' },
										],
									},
								},
								{
									position: 5,
									data: {
										type: 'select',
										key: 'timeBadgeDateFormat',
										label: __('Date/Time Format', 'ultimate-post'),
										options: [
											{ value: 'M j, Y', label: 'Feb 7, 2024' },
											{ value: 'default_date', label: 'WordPress Default Date Format', pro: true },
											{ value: 'default_date_time', label: 'WordPress Default Date & Time Format', pro: true },
											{ value: 'g:i A', label: '1:12 PM', pro: true },
											{ value: 'F j, Y', label: 'February 7, 2024', pro: true },
											{ value: 'F j, Y g:i A', label: 'February 7, 2024 1:12 PM', pro: true },
											{ value: 'M j, Y g:i A', label: 'Feb 7, 2022 1:12 PM', pro: true },
											{ value: 'j M Y', label: '7 Feb 2024', pro: true },
											{ value: 'j M Y g:i A', label: '7 Feb 2022 1:12 PM', pro: true },
											{ value: 'j F Y', label: '7 February 2022', pro: true },
											{ value: 'j F Y g:i A', label: '7 February 2022 1:12 PM', pro: true },
											{ value: 'j. M Y', label: '7. Feb 2022', pro: true },
											{ value: 'j. M Y | H:i', label: '7. Feb 2022 | 1:12', pro: true },
											{ value: 'j. F Y', label: '7. February 2022', pro: true },
											{ value: 'j.m.Y', label: '7.02.2022', pro: true },
											{ value: 'j.m.Y g:i A', label: '7.02.2022 1:12 PM', pro: true },
										],
									},
								},
								{ position: 10, data: { type: 'separator', key: 'tickTimeBadge', label: __('Time Style', 'ultimate-post') }, },
								{ position: 20, data: { type: 'color', key: 'timeBadgeColor', label: __('Time Badge Color', 'ultimate-post') }, },
								{ position: 30, data: { type: 'color2', key: 'timeBadgeBg', label: __('Time Bg Color', 'ultimate-post') } },
								{ position: 40, data: { type: 'typography', key: 'timeBadgeTypo', label: __('Time Badge  Typography', 'ultimate-post') } },
								{ position: 50, data: { type: 'dimension', key: 'timeBadgeRadius', step: 1, unit: true, responsive: true, label: __('Time Badge Radius', 'ultimate-post') } },
								{ position: 60, data: { type: 'dimension', key: 'timeBadgePadding', step: 1, unit: true, responsive: true, label: __('Padding', 'ultimate-post') } },
							]}
						/>}
						<CommonSettings depend="tickImageShow" store={store} title={__('Image', 'ultimate-post')}
							include={[
								{ position: 1, data: { type: 'range', key: 'tickImgWidth', min: 10, max: 100, step: 1, label: __('image width', 'ultimate-post') } },
								{ position: 2, data: { type: 'range', key: 'tickImgSpace', min: 10, max: 100, step: 1, label: __('image Space', 'ultimate-post') } },
								{ position: 3, data: { type: 'dimension', key: 'tickImgRadius', step: 1, unit: true, responsive: true, label: __('Image Radius', 'ultimate-post') } },
							]}
						/>
					</Section>
					<Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
						<GeneralAdvanced initialOpen={true} store={store} />
						<ResponsiveAdvanced pro={true} store={store} />
						<CustomCssAdvanced store={store} />
					</Section>
				</Sections>
				{blockSupportLink()}
			</InspectorControls>

			<ToolBarElement
				include={[
					{
						type: 'query'
					},
					{
						type: 'template'
					}
				]}
				store={store} />

			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className={`ultp-block-wrapper`}>
					{renderContent()}
				</div>
			</div>
		</Fragment>
	)
}