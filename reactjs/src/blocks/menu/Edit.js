const { __ } = wp.i18n
const { InspectorControls, InnerBlocks, BlockControls } = wp.blockEditor
const { Fragment, useState, useEffect, useRef, useMemo } = wp.element
import { updateChildAttr, updateCurrentPostId } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator'
import icons from '../../helper/icons';
import { FetchSuggestions } from './helper/FetchSuggestions';
import { MobileMenu } from './helper/MobileMenu';
import { MenuSettings, MenuToolbar } from './Settings';
const { Spinner, Placeholder, Dropdown, ToolbarButton } = wp.components;
const {
    getBlockParentsByBlockName,
} = wp.data.select('core/block-editor');

export default function Edit(props) {
    const prevPropsRef = useRef(null);
    const loaderRef = useRef(false);
    const hasRootMenuRef = useRef('');
    const [listTemplate, setListTemplate] = useState();
    const [toggleRender, setToggleRender] = useState(false);
	const [section, setSection] = useState('Content');
    const { 
        setAttributes,
        name,
        className,
        attributes,
        clientId,
        attributes: {
            previewImg,
            blockId,
            advanceId,
            hasRootMenu,
            previewMobileView,
            menuAlign,
            menuMvInheritCss,
            mvTransformToHam,
            menuInline,
            currentPostId,
            menuHamAppear
        }
    } = props;
    
    useEffect(() => {
        const _client = clientId.substr(0, 6)
        updateCurrentPostId(setAttributes, '', currentPostId, clientId );
        
        if ( !blockId ) {
            loaderRef.current = true;
            wp.apiFetch({
                path: '/wp/v2/pages?per_page=4&parent=0'
            }).then((response) => {
                let templates = [];
                if ( response && response.length > 0 && hasRootMenuRef.current != 'hasRootMenu') {
                    templates = response.map((page) => {
                        return [
                            "ultimate-post/menu-item",
                            {  
                                parentMenuInline: menuInline,
                                menuItemText: page.title?.rendered || 'No Title',
                                menuItemLink: page.link,
                                contentHorizontalPosition: { lg: 1, ulg: 'px' },
                                contentVerticalPosition: { lg: 12, ulg: 'px' } 
                            }
                        ];
                    });
                }
                loaderRef.current = false;
                setListTemplate(templates);
            }).catch((error) => {
                loaderRef.current = false;
                setListTemplate([]);
                console.error(error);
            });

            setAttributes({ blockId: _client });
        } else if ( blockId && blockId != _client ) {
            loaderRef.current = true;
            setAttributes({ blockId: _client });
            setTimeout(() => {
                setToggleRender(!toggleRender);
                loaderRef.current = false;
            }, 0)
            // document.getElementById('ultp-block-' +blockId)?.remove();
        }
	}, [clientId]);

    const needsUpdateChild = ( preAttr ) => {
        const { menuInline, dropIcon, iconAfterText } = attributes;
        if (
            preAttr.menuInline != menuInline ||
            preAttr.iconAfterText != iconAfterText ||
            preAttr.dropIcon != dropIcon
        ) {
            return true;
        }
    }
    useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if ( !prevAttributes ) {
			prevPropsRef.current = attributes;
		} else if ( needsUpdateChild(prevAttributes) ) {
			updateChildAttr(clientId);
		}

        const hasRootM = getBlockParentsByBlockName(clientId, ['ultimate-post/menu'])?.length > 0;
        if ( hasRootM && hasRootMenu != "hasRootMenu" ) {
            setAttributes({ hasRootMenu: "hasRootMenu" });
            hasRootMenuRef.current = 'hasRootMenu';
        }
        if (
            prevAttributes?.hasRootMenu != hasRootMenu ||
            prevAttributes?.mvTransformToHam != mvTransformToHam ||
            prevAttributes?.menuHamAppear != menuHamAppear 
        ) {
            let theCSS = ''
            if ( menuHamAppear && mvTransformToHam > 0  && hasRootMenu != "hasRootMenu" ) {
                theCSS = `
                    @media (max-width: ${mvTransformToHam}px) {
                        .postx-page {{ULTP}}[data-mv="enable"] > .ultp-menu-wrapper {
                            display: none;
                        }
                        .postx-page {{ULTP}}[data-mv="enable"] > .ultp-mv-ham-icon.ultp-active {
                            display: block;
                        }
                    }
                `;
            }
            setAttributes({ menuMvResCss: theCSS });
        }

        prevPropsRef.current = attributes;
	}, [attributes]);

    const store = {
        setAttributes,
        name,
        attributes,
        setSection: setSection,
        section: section,
        clientId
    };

    const blockCss = useMemo((e) => {
        return CssGenerator(attributes, 'ultimate-post/menu', blockId, false );
    }, [attributes, clientId]);

    const listTemp = [
        [ "ultimate-post/menu-item", { parentMenuInline: true, menuItemText: 'Menu #1', contentHorizontalPosition: { lg: 1, ulg: 'px' }, contentVerticalPosition: { lg: 12, ulg: 'px' } }],
        [ "ultimate-post/menu-item", { parentMenuInline: true, menuItemText: 'Menu #2', contentHorizontalPosition: { lg: 1, ulg: 'px' }, contentVerticalPosition: { lg: 12, ulg: 'px' } }],
        [ "ultimate-post/menu-item", { parentMenuInline: true, menuItemText: 'Menu #3', contentHorizontalPosition: { lg: 1, ulg: 'px' }, contentVerticalPosition: { lg: 12, ulg: 'px' } }],
    ]

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }
    const defaultBlock = { 
        name: 'ultimate-post/menu-item', 
        attributes: { 
            parentMenuInline: menuInline, 
            menuItemText: `Menu Item`, 
            contentHorizontalPosition: { lg: 1, ulg: 'px' }, 
            contentVerticalPosition: { lg: 12, ulg: 'px' } 
        }
    };

    const menuCssClass = (hasRootMenu != "hasRootMenu" && previewMobileView && !menuMvInheritCss) ? '' : 'ultpMenuCss';

    return (
        <Fragment>
            <BlockControls>
                <MenuToolbar
                    isSelected={props.isSelected}
                    menuInline={menuInline}
                    clientId={clientId}
                    menuAlign={menuAlign}
                    store={store}
                />
            </BlockControls>
            <InspectorControls>
                <MenuSettings
                    store={store}
                    hasRootMenu={hasRootMenu}
                    menuInline={menuInline}
                    menuHamAppear={menuHamAppear}
                />
            </InspectorControls>
            {
                loaderRef.current ? <Placeholder
                    className={`ultp-backend-block-loading ultp-menu-block`}
                    label={__('Loading...', 'ultimate-post')}
                >
                    <Spinner />
                </Placeholder>
                :
                <div 
                    {...(advanceId && {id: advanceId})}
                    data-menuinline={menuInline}
                    className={`ultp-block-${blockId} ${className} ${menuCssClass}`}
                >
                    { blockCss &&
                        <style dangerouslySetInnerHTML={{__html: blockCss}}></style>
                    }
                    {
                        ( 
                            hasRootMenu != "hasRootMenu" &&
                            previewMobileView
                        ) ? 
                        <MobileMenu
                            attributes={attributes}
                        />
                        :
                        <div className={`ultp-menu-wrapper _${menuAlign}`}>
                            <div className={`ultp-menu-content`}>
                                <InnerBlocks
                                    template={listTemplate?.length ? listTemplate : listTemp}
                                    defaultBlock={defaultBlock}
                                    allowedBlocks={['ultimate-post/menu-item']}
                                    directInsert={true}
                                    renderAppender={ props.isSelected ? () => <div className='ultp-menu-block-appender'>
                                        <Dropdown
                                            contentClassName="ultp-menu-toolbar-drop"
                                            renderToggle={({ onToggle }) => (
                                                <ToolbarButton
                                                    label="Add Item"
                                                    icon={icons.plus}
                                                    onClick={() => onToggle()} 
                                                />
                                            )}
                                            renderContent={({onClose}) => (
                                                <FetchSuggestions
                                                    hasStep={true}
                                                    callback={(data) => {
                                                        const { _title, _url, _target } = data;
                                                        onClose();
                                                        wp.data.dispatch( 'core/block-editor' ).insertBlock(wp.blocks.createBlock("ultimate-post/menu-item", { 
                                                            parentMenuInline: menuInline, 
                                                            menuItemText: _title,
                                                            menuItemLink: _url,
                                                            menuLinkTarget: _target,
                                                            contentHorizontalPosition: { lg: 1, ulg: 'px' }, 
                                                            contentVerticalPosition: { lg: 12, ulg: 'px' } 
                                                        }), 999, clientId, false)
                                                    }}
                                                />
                                            )}
                                        />
                                    </div> : false }
                                />
                            </div>
                        </div>
                    }
                </div>
            }
        </Fragment>
    )
}
