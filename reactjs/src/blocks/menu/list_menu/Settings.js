import { CommonSettings, CustomCssAdvanced, menuDropIcons, ResponsiveAdvanced, ToolbarCommonSettings } from "../../../helper/CommonPanel"
import icons from "../../../helper/icons";
import { Section, Sections } from '../../../helper/Sections';
const { Toolbar, ToolbarButton, Dropdown } = wp.components;

const { __ } = wp.i18n

export const ListToolbar = (props) => {
    const { clientId, isSelected, store } = props;
    
    return (
        <>
            { isSelected && 
                <Toolbar>
                    <ToolbarButton
                        label="Add Item"
                        icon={icons.plus}
                        onClick={() => {
                            wp.data.dispatch( 'core/block-editor' ).insertBlock(wp.blocks.createBlock("ultimate-post/menu-item", {}), 999, clientId, false)
                        }}
                    />
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        focusOnMount={true}
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Style"
                                icon={icons.styleIcon}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                                    
                                    { position: 10, data: { type: 'tab', content: [
                                        {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                                { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                                { type:'boxshadow', key:'itemShadow', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                                { type:'border',    key:'itemBorder',    label: __('Border', 'ultimate-post') },
                                            ]
                                        },
                                        {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                { type:'toggle',    key:'innerCurrItemStyle',label:__('User Hover as Active Color','ultimate-post')},
                                                { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                                { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                                { type:'boxshadow', key:'itemShadowHvr', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                                { type:'border',    key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') },
                                            ]
                                        }
                                    ]}},
                                    { position: 30, data: { type:'dimension',   key:'itemRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                                    { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                                ]} 
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        focusOnMount={true}
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Spacing"
                                icon={icons.spacing}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 20, data: { type:'range',        key:'listItemGap', min: 0, max: 100, step: 1, responsive: true, unit: ['px'], label:__('Item Gap','ultimate-post') } },
                                ]}
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />
                </Toolbar>
            }
            <Toolbar>
                <ToolbarButton
                    label="Delete List Menu"
                    icon={icons.delete}
                    onClick={() => {
                        wp.data.dispatch( 'core/block-editor' ).removeBlock(clientId, true)
                    }}
                />
            </Toolbar>
        </>
    )
}
export const ListMenuSettings = ({ store }) => {
    return (
        <>
            <Sections>
                <Section slug="global" title={__('Style','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 10, data: { type:'alignment', block: 'list-menu', key: 'listMenuAlignItems', disableJustify: true, icons: ['left_new', 'center_new', 'right_new', 'alignStretch'], options:['flex-start', 'center', 'flex-end', 'stretch'], label: __('Items Alignment', 'ultimate-post') } },
                            { position: 20, data: { type:'range',        key:'listItemGap', min: 0, max: 100, step: 1, responsive: true, unit: ['px'], label:__('Item Gap','ultimate-post') } },
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('List Item','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                            
                            { position: 10, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                        { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadow', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorder',    label: __('Border', 'ultimate-post') },
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                        { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadowHvr', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') },
                                    ]
                                }
                            ]}},
                            { position: 30, data: { type:'dimension',   key:'itemRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                            { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Item Icon','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'toggle',  key:'iconAfterText',label:__('Icon After Text','ultimate-post')} },
                            { position: 10, data: { type:'range',   key:'iconSize', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Icon Size','ultimate-post') } },
                            { position: 20, data: { type:'range',   key:'iconSpacing', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Spacing','ultimate-post') } },
                            { position: 30, data:  { type:'color',  key:'iconColor',label:__('Color','ultimate-post')}},
                            { position: 40, data:  { type:'color',  key:'iconColorHvr',label:__('Hover Color','ultimate-post')}},
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Dropdown Icon','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'icon',    key:'dropIcon', selection: menuDropIcons, label:__('Choose Icon','ultimate-post')}},
                            { position: 10, data: { type:'color',   key:'dropColor',label:__('Icon Color','ultimate-post')}},
                            { position: 10, data: { type:'color',   key:'dropColorHvr',label:__('Icon Hover Color','ultimate-post')}},
                            { position: 15, data: { type:'range',   key:'dropSize', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Icon Size','ultimate-post') } },
                            { position: 20, data: { type:'range',   key:'dropSpacing', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Text to Icon Spacing','ultimate-post') } },
                        ]}
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Background Wrapper','ultimate-post')} 
                        include={[
                            { position: 30, data: { type:'color2',       key:'listMenuBg', image: false, label: __('Background', 'ultimate-post') } },
                            { position: 40, data: { type:'border',       key:'listMenuBorder', label: __('Border', 'ultimate-post') } },
                            { position: 50, data: { type:'boxshadow',   key:'listMenuShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post')} },
                            { position: 60, data: { type:'dimension',    key:'listMenuRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') }},
                            { position: 70, data: { type:'dimension',    key:'listMenuPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]} 
                        initialOpen={true}  store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={'inline'} 
                        include={[
                            { position: 1, data: { type:'text',     key:'advanceId',        label:__('ID','ultimate-post') } },
                            { position: 2, data: { type:'range',    key:'advanceZindex',    label:__('z-index','ultimate-post'), min:-100, max:10000, step:1 } }
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    )
}