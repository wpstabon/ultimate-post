const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { blockId, advanceId } = props.attributes;
    const extras = {}
    const extrasLength = Object.keys(extras);
        
    return (
        <div 
            {...(advanceId && {id:advanceId})}
            data-bid={blockId}
            className={`ultp-block-${blockId} ultpMenuCss`}
            { ...( extrasLength && extras )}
        >
            <div className={`ultp-list-menu-wrapper`}>
                <div className={`ultp-list-menu-content`}>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    )
}