const { __ } = wp.i18n
import { updateChildAttr } from '../../../helper/CommonPanel'
import { CssGenerator } from '../../../helper/CssGenerator'
import { ListMenuSettings, ListToolbar } from './Settings'
const { InspectorControls, InnerBlocks, BlockControls } = wp.blockEditor;
const { 
    useState,
    useEffect,
    useRef,
    Fragment,
    useMemo
} = wp.element

export default function Edit(props) {
    const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
    const { 
        setAttributes, 
        name, 
        className, 
        attributes, 
        clientId, 
        attributes: { 
            previewImg, 
            blockId, 
            advanceId,
            dropIcon,
            iconAfterText
        } 
        
    } = props;

    const store = {
        setAttributes,
        name,
        attributes,
        setSection: setSection,
        section: section, 
        clientId
    };

    useEffect(() => {
        const _client = clientId.substr(0, 6);
        
        if ( !blockId ) {
            setAttributes({ blockId: _client });
        } else if ( blockId && blockId != _client ) {
            setAttributes({ blockId: _client });
            // document.getElementById('ultp-block-' +blockId)?.remove();
        }
	}, [clientId]);

    useEffect(() => {
		const prevAttributes = prevPropsRef.current;

		if ( !prevAttributes ) {
			prevPropsRef.current = attributes;
		} else if (
            prevAttributes.iconAfterText != iconAfterText ||
            prevAttributes.dropIcon != dropIcon
        ) {
			updateChildAttr(clientId);
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

    const blockCss = useMemo(() => {
        return CssGenerator(attributes, 'ultimate-post/list-menu', blockId, false );
    }, [attributes, clientId]);

    const listTemplate = [
        [ "ultimate-post/menu-item", { menuItemText: 'List Item' } ],
        [ "ultimate-post/menu-item", { menuItemText: 'List Item' } ],
        [ "ultimate-post/menu-item", { menuItemText: 'List Item' } ]
    ]

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }
    
    return (
        <Fragment>
            <BlockControls>
                <ListToolbar
                    isSelected={props.isSelected}
                    clientId={clientId}
                    store={store}
                />
            </BlockControls>
            <InspectorControls>
                <ListMenuSettings store={store} />
            </InspectorControls>
            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className} ultpMenuCss`}>
                { blockCss &&
                    <style dangerouslySetInnerHTML={{__html:blockCss}}></style>
                }
                <div className={`ultp-list-menu-wrapper`}>
                    <div className={`ultp-list-menu-content`}>
                        <InnerBlocks
                            template={listTemplate}
                            allowedBlocks={['ultimate-post/menu-item']}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}