const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/menu/', 'block_docs');

registerBlockType(
    'ultimate-post/list-menu', {
        title: __('List Menu','ultimate-post'),
        parent:  ["ultimate-post/menu-item"],
        icon: <img src={ultp_data.url + 'assets/img/blocks/menu/list_menu.svg'} alt="List Menu"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Manage and Customize Menus in a List Format.','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [ 
            __('menu','ultimate-post'),
            __('list','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
        },
        example: {
            attributes: {
                previewImg: false,
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            /*==========================
                    Needed
            ==========================*/
            blockId: {
                type: 'string',
                default: '',
            },
            previewImg: {
                type: 'string',
                default: '',
            },

            /*==========================
                Inline
            ==========================*/
            listItemGap: {
                type: 'object',
                default: { lg:'0', ulg:'px' },
                style:[
                    {
                        selector: 
                         `{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                        .postx-page {{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content { gap: {{listItemGap}};}` 
                    }
                ],
            },
            listMenuAlignItems: {
                type: 'string',
                default: 'stretch',
                style: [
                    {
                        selector: `{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content > .block-editor-inner-blocks > .block-editor-block-list__layout, 
                        .postx-page {{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content { align-items: {{listMenuAlignItems}}; }`
                    }
                ],
            },
            listMenuBg: {
                type: 'object',
                default: { openColor: 1, type: 'color', color: '#F5F5F5', size: 'cover', repeat: 'no-repeat', loop: true },
                style:[{
                    selector: '{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content'
                }],
            },
            listMenuBorder: {
                type: 'object',
                default: { openBorder: 1, width:{top: 1, right: 1, bottom: 1, left: 1 },color: '#D2D2D2' },
                style: [{
                    selector:'{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content'
                }]
            },
            listMenuShadow: {
                type: 'object',
                default: {openShadow: 0, width: { top: 1, right: 1, bottom: 1, left: 1 },color: '#037fff'},
                style: [{ selector: '{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content' }],
            },
            listMenuRadius: {
                type: 'object',
                default: { lg: { top: 4, right: 4, bottom: 4, left: 4 , unit:'px' } },
                style:[{ selector:'{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content { border-radius:{{listMenuRadius}}; }' }],
            },
            listMenuPadding: {
                type: 'object',
                default: { lg: { top: '12', bottom: '12',left: '8', right: '8', unit:'px' } },
                style:[{ selector:'{{ULTP}} > .ultp-list-menu-wrapper > .ultp-list-menu-content { padding:{{listMenuPadding}}; }' }],
            },

            /*==========================
                    Dropdown
            ==========================*/
            dropIcon: {
                type: 'string',
                default: 'rightAngle2'
            },
            dropColor: {
                type: 'string',
                default: '#2E2E2E',
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { fill: {{dropColor}}; }'
                    },
                ]
            },
            dropColorHvr: {
                type: 'string',
                default: '#2E2E2E',
                style: [
                    {
                        selector:'.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { fill: {{dropColorHvr}}; }'
                    },
                ]
            },
            dropSize: {
                type: 'object',
                default: { lg:'14', ulg:'px' },
                style: [
                    {
                        selector:'.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { height: {{dropSize}}; width: {{dropSize}}; }'
                    }
                ],
            },
            dropSpacing: {
                type: 'object',
                default: { lg:'64', ulg:'px' },
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { gap: {{dropSpacing}}; }'
                    },
                ]
            },

            /*==========================
                Menu Item
            ==========================*/
            itemTypo: {
                type: 'object',
                default: { openTypography: 1 ,size: { lg:16, unit:'px'},height: {lg:'20', unit:'px'}, decoration: 'none',family: '', weight: 400, transform: "capitalize"},
                style: [{selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text'}]
            },
            itemColor: {
                type: 'string',
                default: '#2E2E2E',
                style: [{selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColor}}; }'}]
            },
            itemBg: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: ''},
                style:[{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }]
            },
            itemShadow: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }],
            },
            
            // hover
            innerCurrItemStyle:{
                type: 'toggle',
                default: false,

            },
            itemColorHvr: {
                type: 'string',
                default: '#6E6E6E',
                style: [{
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: false },
                    ],
                    selector:'.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColorHvr}}; }'
                },
                {
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: true },
                    ],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColorHvr}}; }`
                }]
            },
            itemBgHvr: {
                type: 'object',
                default: {openColor: 1, type: 'color', color: '#FFFFFF'},
                style:[{ 
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: false },
                    ],
                    selector: 
                    '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' 
                },
                {
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: true },
                    ],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container`
                }
                ]
            },
            itemShadowHvr: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [{ 
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: false },
                    ],
                    selector: 
                    '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' 
                },
                {
                    depends: [
                        { key:'innerCurrItemStyle', condition:'==', value: true },
                    ],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container`
                }
            ],
            },
            itemBorder: {
                type: 'object',
                default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
                style:[{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }],
            },
            itemBorderHvr: {
                type: 'object',
                default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
                style:[{ selector: '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }],
            },
            itemRadius: {
                type: 'object',
                default: { top: 4, right: 4, bottom: 4, left: 4 , unit:'px'},
                style: [{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { border-radius:{{itemRadius}}; }' }],
            },
            itemPadding: {
                type: 'object',
                default: { lg: { top: 12, right: 12, bottom: 12, left: 20, unit:'px'} },
                style:[ {selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { padding:{{itemPadding}}; }'} ],
            },
            
            /*==========================
                Item Icon
            ==========================*/
            iconAfterText: {
                type: 'boolean',
                default: false,
            },
            iconSize: {
                type: 'object',
                default: { lg:'12', ulg:'px' },
                style: [
                    {
                        selector: `
                        .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg,
                        .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon img { height: {{iconSize}}; width: {{iconSize}}; }`
                    }
                ],
            },
            iconSpacing: {
                type: 'object',
                default: { lg:'10', ulg:'px' },
                style: [
                    {
                        selector: '.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label { gap: {{iconSpacing}}; }' 
                    },
                    
                ],
            },
            iconColor: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg { fill:{{iconColor}} }'
                    },
                ]
            },
            iconColorHvr: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        selector:'.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg { fill:{{iconColorHvr}} }'
                    },
                ]
            },
            
            
            /*==========================
                General Advance Settings
            ==========================*/
            advanceId:{
                type: 'string',
                default: '',
            },
            advanceZindex:{
                type: 'string',
                default: '',
                style:[{ selector: '{{ULTP}} .ultp-list-menu-wrapper {z-index:{{advanceZindex}};}' }],
            },
            hideExtraLarge: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideTablet: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideMobile: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            advanceCss: {
                type: 'string',
                default: '',
                style: [{selector: ''}],
            }
        }
    }
)