import { CommonSettings, menuDropIcons, ToolbarCommonSettings } from "../../helper/CommonPanel"
import IconPack from "../../helper/fields/tools/IconPack";
import icons from "../../helper/icons";
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from "../../helper/TemplateModal";
const { __ } = wp.i18n;
const { Toolbar, ToolbarButton, Dropdown } = wp.components;
import { FetchSuggestions } from './helper/FetchSuggestions';

export const MenuToolbar = (props) => {
    const { menuInline, clientId, isSelected, menuAlign, store } = props;
    return (
        <>
            {
                isSelected && 
                <Toolbar className="ultp-menu-toolbar-group">
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        renderToggle={({ onToggle }) => (
                            <div
                                className={"ultp-menu-toolbar-add-item"}
                                onClick={() => onToggle()} 
                            >
                                {icons.plus}
                                <div className="__label">{__('Add Item', 'ultimate-post')}</div>
                            </div>
                        )}
                        renderContent={({onClose}) => (
                            <FetchSuggestions
                                hasStep={true}
                                callback={(data) => {
                                    const { _title, _url, _target } = data;
                                    onClose();
                                    wp.data.dispatch( 'core/block-editor' ).insertBlock(wp.blocks.createBlock("ultimate-post/menu-item", { 
                                        parentMenuInline: menuInline, 
                                        menuItemText: _title,
                                        menuItemLink: _url,
                                        menuLinkTarget: _target,
                                        contentHorizontalPosition: { lg: 1, ulg: 'px' }, 
                                        contentVerticalPosition: { lg: 12, ulg: 'px' } 
                                    }), 999, clientId, false)
                                }}
                            />
                        )}
                    />
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        focusOnMount={true}
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Alignment"
                                icon={icons[menuAlign]}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 10, data: { type:'alignment',   key:'menuAlign', block: 'menu', inline: true, disableJustify: true, icons: ['left', 'center', 'right'], options:['left', 'center', 'right'], label: __('Alignment', 'ultimate-post') } },
                                ]} 
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        focusOnMount={true}
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Style"
                                icon={icons.styleIcon}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                                    
                                    { position: 10, data: { type: 'tab', content: [
                                        {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                                { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                                { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                                { type:'boxshadow', key:'itemShadow', step:1,unit:true, responsive:true,label:__('Box shadow','ultimate-post')},
                                                { position: 20, data: { type:'border',      key:'itemBorder',    label: __('Border', 'ultimate-post') } },
                                            ]
                                        },
                                        {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                                { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                                { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                                { type:'boxshadow', key:'itemShadowHvr', step:1,unit:true, responsive:true,label:__('Box shadow','ultimate-post')},
                                                { position: 20, data: { type:'border',      key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') } },
                                            ]
                                        }
                                    ]}},
                                    { position: 30, data: { type:'dimension',   key:'itemRadius',step:1,unit:true, responsive:true,label:__('Border Radius','ultimate-post')} },
                                    { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1,unit:true, responsive:true,label:__('Padding','ultimate-post') }},
                                ]}
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        focusOnMount={true}
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Spacing"
                                icon={icons.spacing}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 20, data: { type:'range',       key:'menuItemGap', min: 0, max: 200, step: 1, responsive: true, unit: ['px', '%'], label:__('Menu item gap','ultimate-post') } }
                                ]}
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />
                </Toolbar>
            }
        </>
    )
}

export const MenuSettings = ({ store, hasRootMenu, menuInline, menuHamAppear }) => {
    return (
        <>
            <TemplateModal
                designLibrary={false}
                prev="https://www.wpxpo.com/postx/blocks/#demoid8819"  
                store={store}
            />
            <Sections 
                callback={(data) => {
                    if ( data.slug == 'hamburger' ) {
                        store.setAttributes({ previewMobileView: true });
                    } else {
                        store.setAttributes({ previewMobileView: false, previewHamIcon: false });
                    }
                }}
                classes={" ultp-menu-side-settings"}
            >
                <Section slug="global" title={__('Normal View','ultimate-post')} icon={false}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 5,  data: { type:'toggle',      key:'menuInline',label:__('Inline','ultimate-post')} },
                            { position: 20, data: { type:'range',       key:'menuItemGap', min: 0, max: 200, step: 1, responsive: true, unit: ['px', '%'], label:__('Menu item gap','ultimate-post') } },
                            { position: 10, data: { type:'alignment',   key:'menuAlign', block: 'menu',  disableJustify: true, icons: ['left', 'center', 'right'], options:['left', 'center', 'right'], label: __('Alignment', 'ultimate-post') } },
                            { position: 11, data: { type:'alignment',   key:'menuAlignItems', block: 'menu',  disableJustify: true, icons: menuInline ? ['alignStartR', 'alignCenterR', 'alignEndR', 'alignStretchR'] : ['left_new', 'center_new', 'right_new', 'alignStretch'], options:['flex-start', 'center', 'flex-end', 'stretch'], label: __('Items Alignment', 'ultimate-post') } },
                        ]}
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Menu Items','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                            
                            { position: 10, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                        { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadow', step:1,unit:true, responsive:true,label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorder',    label: __('Border', 'ultimate-post') },
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                        { type:'toggle',    key:'currentItemStyle',label:__('User Hover as Active Color','ultimate-post')},
                                        { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadowHvr', step:1,unit:true, responsive:true, label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') },
                                    ]
                                }
                            ]}},
                            { position: 30, data: { type:'dimension',   key:'itemRadius',step:1,unit:true, responsive:true,label:__('Border Radius','ultimate-post')} },
                            { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1,unit:true, responsive:true,label:__('Padding','ultimate-post') }},
                        ]}
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Item Icon','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'toggle',  key:'iconAfterText',label:__('Icon After Text','ultimate-post')} },
                            { position: 10, data: { type:'range',   key:'iconSize', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Icon Size','ultimate-post') } },
                            { position: 20, data: { type:'range',   key:'iconSpacing', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Spacing','ultimate-post') } },
                            { position: 30, data:  { type:'color',  key:'iconColor',label:__('Color','ultimate-post')}},
                            { position: 40, data:  { type:'color',  key:'iconColorHvr',label:__('Hover Color','ultimate-post')}},
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Dropdown Icon','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'icon',    key:'dropIcon', selection: menuDropIcons, label:__('Choose Icon','ultimate-post')}},
                            { position: 10, data: { type:'color',   key:'dropColor',label:__('Icon Color','ultimate-post')}},
                            { position: 10, data: { type:'color',   key:'dropColorHvr',label:__('Icon Hover Color','ultimate-post')}},
                            { position: 15, data: { type:'range',   key:'dropSize', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Icon Size','ultimate-post') } },
                            { position: 20, data: { type:'range',   key:'dropSpacing', min: 0, max: 200, step: 1, responsive: true, unit: ['px'], label:__('Text to Icon Spacing','ultimate-post') } },
                        ]}
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Background Wrapper','ultimate-post')}
                        include={[
                            { position: 30, data: { type:'color2',      key:'menuBg', image: true, label: __('Background', 'ultimate-post') } },
                            { position: 40, data: { type:'border',      key:'menuBorder', label: __('Border', 'ultimate-post') } },
                            { position: 50, data: { type:'boxshadow',   key:'menuShadow', step:1, unit:true, responsive:true, label:__('Box shadow','ultimate-post')} },
                            { position: 60, data: { type:'dimension',   key:'menuRadius',step:1,unit:true, responsive:true,label:__('Border Radius','ultimate-post')} },
                            { position: 65, data: { type:'dimension',   key:'menuMargin',step:1,unit:true, responsive:true,label:__('Margin','ultimate-post') }},
                            { position: 70, data: { type:'dimension',   key:'menuPadding',step:1,unit:true, responsive:true,label:__('Padding','ultimate-post') }},
                            { position: 80, data: { type:'toggle',      key:'inheritThemeWidth', label:__('Inherit Theme Width (Content)','ultimate-post')} },
                            { position: 90, data: { type:'range',       key:'menuContentWidth', min: 0, max: 1700, step: 1, responsive: true, unit: ['px', '%'], label:__('Content Max-Width','ultimate-post') } },
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Advanced Style','ultimate-post')}
                        include={[
                            { position: 1,  data: { type: 'text',     key:'advanceId',        label:__('ID','ultimate-post') } },
                            { position: 5,  data: { type: 'range',    key:'advanceZindex',    label:__('z-index','ultimate-post'), min:-100, max:10000, step:1 } },
                            { position: 30, data: { type: 'textarea', key: 'advanceCss',      label:__('Custom CSS','ultimate-post'), placeholder: __('Add {{ULTP}} before the selector to wrap element.', 'ultimate-post') } },
                            { position: 15, data: { type: 'toggle',   key: 'hideExtraLarge',  pro: true, label: __('Hide On Extra Large Display', 'ultimate-post') } },
                            { position: 20, data: { type: 'toggle',   key: 'hideTablet',      pro: true, label: __('Hide On Tablet', 'ultimate-post') } },
                            { position: 25, data: { type: 'toggle',   key: 'hideMobile',      pro: true, label: __('Hide On Mobile', 'ultimate-post') } },
                        ]} 
                        initialOpen={false}  store={store}
                    />
                </Section>
                { hasRootMenu != "hasRootMenu" && 
                    <Section slug="hamburger" title={__('Hamburger View','ultimate-post')} icon={IconPack['hamicon_1']}>
                        <CommonSettings title={`inline`} 
                            include={[
                                { position: 1, data: { type: 'toggle',      key: 'menuHamAppear', label:__('Enable Hamburger Menu Conversion','ultimate-post')} },
                                { position: 4, data: { type: 'range',       key: 'mvTransformToHam',   min: 0, max: 1400, step: 1, unit: ['px'], responsive: false, label:__('Hamburger Breakpoint','ultimate-post') } },
                                menuHamAppear && { position: 9, data: { type: 'range',       key: 'mvPopWidth',    min: 0, max: 1400, step: 1, responsive: false, unit: ['px', '%'], label:__('Drawer Width','ultimate-post') } },
                                menuHamAppear && {
                                    position: 20, data: { type:'select', key:'menuResStructure', inline: true, justify: true, pro: true, options: [
                                        { value: 'mv_dissolve', label: __('Dissolve', 'ultimate-post') },
                                        { value: 'mv_slide', label: __('Slide', 'ultimate-post') },
                                        { value: 'mv_accordian', label: __('Accordian', 'ultimate-post') },
                                    ], label:__('Navigation Effect','ultimate-post') }
                                },
                                menuHamAppear && { position: 22, data: { type: 'dimension',    key: 'hamExpandedPadding',        label:__('Expanded Container Padding','ultimate-post'),      step:1, unit:['px'],  responsive: false } },
                                menuHamAppear && { position: 30, data: { type: 'range',       key: 'mvAnimationDuration',    min: 0, max: 2000, step: 1, responsive: false, unit: false, label:__('Animation Duration(ms)','ultimate-post') } },
                            ]} 
                            initialOpen={false} store={store}
                        />
                        {
                            menuHamAppear &&  <>
                                <CommonSettings title={__('Hamburger Icon','ultimate-post')}
                                    include={[
                                        { position: 1,  data: { type: 'toggle',     key: 'previewHamIcon', label:__('Preview','ultimate-post')} },
                                        { position: 5,  data: { type: 'icon',       key: 'mvHamIcon', hideFilter: true,         label:__('Choose Ham Icon','ultimate-post'), selection: [ "hamicon_1", "hamicon_2", "hamicon_3", "hamicon_4", "hamicon_5", "hamicon_6" ] } },
                                        { position: 10, data: { type: 'range',      key: 'mvHamIconSize',        min: 6, max: 200, step: 1, responsive: false, label:__('Icon Size','ultimate-post') } },
                                        {
                                            position: 20, 
                                            data: { 
                                                type: 'tab', 
                                                content: [
                                                    {   
                                                        name: 'normal', title: __('Normal', 'ultimate-post'), 
                                                        options: [
                                                            { type: 'color',        key: 'hamVClr',            label:__('Color','ultimate-post') },
                                                            { type: 'color2',       key: 'hamVBg',             label:__('Background','ultimate-post' ) },
                                                            { type: 'border',       key: 'hamVBorder',         label:__('Border','ultimate-post') } ,
                                                            { type: 'dimension',    key: 'hamVRadius',         label:__('Border Radius','ultimate-post'),      step:1, unit:['px'],  responsive: true } ,
                                                            { type: 'boxshadow',    key: 'hamVShadow',         label:__('Box shadow','ultimate-post') },
                                                        ]
                                                    },
                                                    {   
                                                        name: 'hover', title: __('Hover', 'ultimate-post'), 
                                                        options: [
                                                            { type: 'color',        key: 'hamVClrHvr',            label:__('Color','ultimate-post') },
                                                            { type: 'color2',       key: 'hamVBgHvr',             label:__('Background','ultimate-post' ) },
                                                            { type: 'border',       key: 'hamVBorderHvr',         label:__('Border','ultimate-post') } ,
                                                            { type: 'dimension',    key: 'hamVRadiusHvr',         label:__('Border Radius','ultimate-post'),      step:1, unit:['px'],  responsive: true } ,
                                                            { type: 'boxshadow',    key: 'hamVShadowHvr',         label:__('Box shadow','ultimate-post') },
                                                        ]
                                                    }
                                                ]
                                            }
                                        },
                                        { position: 10, data: { type: 'dimension',    key: 'hamVPadding',        label:__('Padding','ultimate-post'),      step:1, unit:['px'],  responsive: true } },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                                <CommonSettings title={__('Navigation Icon','ultimate-post')}
                                    include={[
                                        { position: 5,  data: { type: 'icon',         key: 'naviIcon',           selection: menuDropIcons, label:__('choose Icon','ultimate-post')} },
                                        { position: 5,  data: { type: 'icon',         key: 'naviExpIcon',           label:__('Expanded Icon','ultimate-post')} },
                                        { position: 10, data: { type: 'range',        key: 'naviIconSize',           min: 0, max: 200, step: 1, responsive: false, label:__('Icon Size','ultimate-post') } },
                                        { position: 10, data: { type: 'color',        key: 'naviIconClr',            label:__('Color','ultimate-post') }, },
                                        { position: 10, data: { type: 'color',        key: 'naviIconClrHvr',            label:__('Hover Color','ultimate-post') }, },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                                <CommonSettings title={__('Close Icon','ultimate-post')}
                                    include={[
                                        { position: 5,  data: { type: 'icon',         key: 'closeIcon',           selection: ["close_line", "close_circle_line"], hideFilter: true, label:__('Choose Close Icon','ultimate-post')} },
                                        { position: 10, data: { type: 'range',        key: 'closeSize',           min: 0, max: 200, step: 1, responsive: false, label:__('Icon Size','ultimate-post') } },
                                        {
                                            position: 20, 
                                            data: { 
                                                type: 'tab', 
                                                content: [
                                                    {   
                                                        name: 'normal', title: __('Normal', 'ultimate-post'), 
                                                        options: [
                                                            { type: 'color',        key: 'closeClr',            label:__('Icon Color','ultimate-post') },
                                                            { type: 'color',        key: 'closeWrapBg',         label:__('Wrap BG','ultimate-post') },
                                        
                                                        ]
                                                    },
                                                    {   
                                                        name: 'hover', title: __('Hover', 'ultimate-post'), 
                                                        options: [
                                                            { type: 'color',        key: 'closeClrHvr',         label:__('Color','ultimate-post') },
                                                            { type: 'color',        key: 'closeWrapBgHvr',      label:__('Wrap BG Hover','ultimate-post')},
                                                        ]
                                                    }
                                                ]
                                            }
                                        },
                                        { position: 40, data: { type: 'dimension',    key: 'closeSpace',          response: false, label:__('Spacing', 'ultimate-post'),      step:1, unit:['px'],  responsive: false } },
                                        { position: 45, data: { type: 'dimension',    key: 'closePadding',        label:__('Padding','ultimate-post'),      step:1, unit:['px'],  responsive: false } },
                                        { position: 50, data: { type: 'dimension',    key: 'closeRadius',         label:__('Border Radius','ultimate-post'),      step:1, unit:['px'],  responsive: false } },
                                        { position: 55, data: { type: 'border',       key: 'closeBorder',         label:__('Border','ultimate-post'), step:1, unit:['px'],  responsive: false } },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                                <CommonSettings title={__('Menu Items','ultimate-post')}
                                    include={[
                                        { position: 1,  data: { type: 'toggle',           key: 'menuMvInheritCss', label:__('Inherit Parent Block CSS','ultimate-post')} },
                                        { position: 40, data: { type: 'color',            key: 'mvItemColor',             label: __( 'Color','ultimate-post' )} },
                                        { position: 40, data: { type: 'color2',           key: 'mvItemBg',                label: __( 'Background','ultimate-post' )} },
                                        { position: 40, data: { type: 'color',            key: 'mvItemColorHvr',          label: __( 'Hover Color','ultimate-post' )} },
                                        { position: 40, data: { type: 'color2',           key: 'mvItemBgHvr',             label: __( 'Hover Background','ultimate-post' )} },
                                        { position: 40, data: { type: 'range',            key: 'mvItemSpace',             label: __( 'Spacing Between','ultimate-post' ),  min: 0, max: 200, step: 1, responsive: false } },
                                        { position: 40, data: { type: 'typography',       key: 'mvItemTypo',              label: __( 'Typography', 'ultimate-post' ) } },
                                        { position: 40, data: { type: 'border',           key: 'mvItemBorder',            label: __( 'Border', 'ultimate-post' ) } },
                                        { position: 40, data: { type: 'dimension',        key: 'mvItemRadius',            label: __( 'Border Radius', 'ultimate-post' ), step: 1, unit: true, responsive: true } },
                                        { position: 40, data: { type: 'dimension',        key: 'mvItemPadding',           label: __( 'Padding', 'ultimate-post' ), step: 1, unit: true, responsive: true } },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                                <CommonSettings title={__('Menu Header','ultimate-post')}
                                    include={[
                                        { position: 40, data: { type: 'color2',       key: 'mvHeadBg',            image: false, label: __('Header BG', 'ultimate-post') } },
                                        { position: 40, data: { type: 'dimension',    key: 'mvHeadPadding',       step:1, unit:['px', '%', 'em'], responsive:true,label:__('Header Padding','ultimate-post') } },
                                        { position: 40, data: { type: 'border',       key: 'mvHeadBorder',        label:__('Header Border','ultimate-post'),      step:1, unit:['px'],  responsive: false } },
                                        { position: 40,  data: { type: 'text',         key: 'mvHeadtext',           label:__('Menu Label','ultimate-post') } },
                                        { position: 40, data: { type: 'color',        key: 'backClr',             label:__('Label Color','ultimate-post')} },
                                        { position: 40, data: { type: 'color',        key: 'backClrHvr',          label:__('Label Hover Color','ultimate-post')} },
                                        { position: 40, data: { type: 'typography',   key: 'backTypo',            label:__('Label Typography','ultimate-post') } },
                                        { position: 40, data: { type: 'icon',         key: 'backIcon',            label:__('Choose Back Icon','ultimate-post')} },
                                        { position: 40, data: { type: 'range',        key: 'backIconSize',        min: 6, max: 200, step: 1, responsive: false, label:__('Back Icon Size','ultimate-post') } },
                                        { position: 40, data: { type: 'range',        key: 'backIconSpace',       min: 0, max: 200, step: 1, responsive: false, label:__('Text to Icon Gap','ultimate-post') } },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                                <CommonSettings title={__('Container','ultimate-post')}
                                    include={[
                                        { position: 10, data: { type: 'color2',           key: 'mvBodyBg',                label: __( 'Background', 'ultimate-post' ),   image: false } },
                                        { position: 20, data: { type: 'dimension',        key: 'mvBodyPadding',           label: __( 'Padding','ultimate-post' ),    step:1,unit:true, responsive:true } },
                                        { position: 30, data: { type: 'border',           key: 'mvBodyBorder',            label:__('Border','ultimate-post') } },
                                        { position: 40, data: { type: 'boxshadow',        key: 'mvBodyShadow',            label:__('Box shadow','ultimate-post') } },
                                        { position: 50, data: { type: 'color',            key: 'mvOverlay',               label: __( 'Background Overlay', 'ultimate-post' ),   image: false } },
                                    ]}
                                    initialOpen={false}  store={store}
                                />
                            </>
                        }
                    </Section>
                }
            </Sections>
        </>
    )
}