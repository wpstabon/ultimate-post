const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/postx-menu/', 'block_docs');

registerBlockType(
    'ultimate-post/menu', {
        title: __('Menu - PostX','ultimate-post'),
        icon: <img src={ultp_data.url + 'assets/img/blocks/menu/menu.svg'} alt="Menu PostX"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__("Set Up and Organize Your Site's Navigation",'ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('menu','ultimate-post'),
            __('mega','ultimate-post'),
            __('mega menu','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: false,
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            /*==========================
                    Needed
            ==========================*/
            blockId: {
                type: 'string',
                default: '',
            },
            currentPostId: { type: "string" , default: "" },
            previewImg: {
                type: 'string',
                default: '',
            },
            menuMvResCss:{
                type: 'string',
                default: '',
                style: [
                    { selector: '' }
                ],
            },

            /*==========================
                Inline
            ==========================*/
            menuInline: {
                type: 'boolean',
                default: true,
                style: [
                    {
                        depends: [
                            { key:'menuInline', condition:'==', value: false },
                        ],
                    },
                    {
                        depends: [
                            { key:'menuInline', condition:'==', value: true },
                        ],
                        selector: 
                            `{{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content > .block-editor-inner-blocks > .block-editor-block-list__layout,
                            .postx-page {{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content { flex-direction: row; }`
                    }
                ]
            },
            menuAlign: {
                type: 'string',
                default: 'center',
                // style: [
                //     {
                //         depends: [
                //             { key:'menuAlign', condition:'==', value: 'left' },
                //         ], 
                //         selector: '{{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content { margin-right: auto !important; }' 
                //     },
                //     {
                //         depends: [
                //             { key:'menuAlign', condition:'==', value: 'center' },
                //         ], 
                //         selector: '{{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content { margin-left: auto !important; margin-right: auto !important; }' 
                //     },
                //     {
                //         depends: [
                //             { key:'menuAlign', condition:'==', value: 'right' },
                //         ], 
                //         selector: '{{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content { margin-left: auto !important; }' 
                //     },
                // ],
            },
            menuAlignItems: {
                type: 'string',
                default: 'stretch',
                style: [
                    {
                        selector: `{{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content > .block-editor-inner-blocks > .block-editor-block-list__layout,
                            .postx-page {{ULTP}} > .ultp-menu-wrapper > .ultp-menu-content { align-items: {{menuAlignItems}}; }`
                    }
                ],
            },
            menuItemGap: {
                type: 'object',
                default: { lg: '16', ulg: 'px'},
                style:[{
                        selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper > .ultp-menu-content > .block-editor-inner-blocks > .block-editor-block-list__layout, .postx-page {{ULTP}}.ultpMenuCss > .ultp-menu-wrapper > .ultp-menu-content { gap: {{menuItemGap}};}' 
                    },
                ],
            },
            menuBg: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: '', size: 'cover', repeat: 'no-repeat', loop: true},
                style:[{
                    selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper'
                }],
            },
            menuBorder: {
                type: 'object',
                default: { openBorder: 0, width: {top: 0, right: 0, bottom: 0, left: 0}, color: '#dfdfdf' },
                style: [{
                    selector:'{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper'
                }]
            },
            menuShadow: {
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff'},
                style: [{ selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper' }],
            },
            menuRadius: {
                type: 'object',
                default: { lg: { top: '',bottom: '', unit:'px' } },
                style: [{
                    selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper { border-radius: {{menuRadius}};}',
                }]
            },
            menuMargin: {
                type: 'object',
                default: { lg: { top: '',bottom: '', unit:'px' } },
                style:[{ selector:'{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper { margin:{{menuMargin}}; }' }],
            },
            menuPadding: {
                type: 'object',
                default: { lg: {top: '',bottom: '', unit:'px'}},
                style:[{ selector:'{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper { padding:{{menuPadding}}; }' }],
            },
            inheritThemeWidth: {
                type: 'boolean',
                default: true,
                style: [
                    {
                        selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper > .ultp-menu-content { max-width: 100%; }' 
                    },
                ],
            },
            menuContentWidth: {
                type: 'object',
                default: { lg: '1140', ulg: 'px' },
                style: [
                    {
                        depends: [
                            { key:'inheritThemeWidth', condition:'==', value: false },
                        ],
                        selector: '{{ULTP}}.ultpMenuCss > .ultp-menu-wrapper > .ultp-menu-content { max-width: {{menuContentWidth}};}' 
                    },
                ],
            },

            /*==========================
                    Dropdown
            ==========================*/
            dropIcon: {
                type: 'string',
                default: 'collapse_bottom_line'
            },
            dropColor: {
                type: 'string',
                default: '#2E2E2E',
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { fill: {{dropColor}}; }'
                    },
                ]
            },
            dropColorHvr: {
                type: 'string',
                default: '#2E2E2E',
                style: [
                    {
                        selector:'.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { fill: {{dropColorHvr}}; }'
                    },
                ]
            },
            dropSize: {
                type: 'object',
                default: { lg:'12', ulg:'px' },
                style: [
                    {
                        selector:'.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { height: {{dropSize}}; width: {{dropSize}}; }'
                    }
                ],
            },
            dropSpacing: {
                type: 'object',
                default: { lg:'8', ulg:'px' },
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { gap: {{dropSpacing}}; }'
                    },
                ]
            },

            /*==========================
                Menu Item
            ==========================*/
            itemTypo: {
                type: 'object',
                default: { openTypography: 1, size: { lg: 16, unit:'px'}, height: { lg: '20', unit:'px'}, decoration: 'none',family: '', weight: 400, transform: "capitalize" },
                style: [ { selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text'}]
            },
            itemColor: {
                type: 'string',
                default: '#2E2E2E',
                style: [ { selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColor}}; }'}]
            },
            itemBg: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: ''},
                style:[{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }]
            },
            itemShadow: {
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff'},
                style: [{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }],
            },

            // Hover
            currentItemStyle:{
                type: 'boolean',
                default: true,
            },
            itemColorHvr: {
                type: 'string',
                default: '#037fff',
                style: [{
                    depends: [
                        { key:'currentItemStyle', condition:'==', value: false },
                    ], 
                    selector: '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColorHvr}}; }'
                },
                {
                    depends: [
                        { key:'currentItemStyle', condition:'==', value: true },
                    ], 
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColorHvr}}; }`
                },
                ]
            },
            itemBgHvr: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: '#6ee7b7'},
                style:[{ 
                    depends: [
                        { key:'currentItemStyle', condition:'==', value: false },
                    ],
                    selector: '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' 
                },
                {
                    depends: [
                        { key:'currentItemStyle', condition:'==', value: true },
                    ],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container` 
                }]
            },
            itemShadowHvr: {
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#6ee7b7'},
                style: [{ 
                    depends: [ { key:'currentItemStyle', condition:'==', value: false }],
                    selector: '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' 
                },
                {
                    depends: [ { key:'currentItemStyle', condition:'==', value: true }],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container
                    ` 
                }],
            },
            itemBorderHvr: {
                type: 'object',
                default: {openBorder:0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff',type: 'solid'},
                style:[{ 
                    depends: [ { key:'currentItemStyle', condition:'==', value: false }],
                    selector: '.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' 
                },
                {
                    depends: [ { key:'currentItemStyle', condition:'==', value: true }],
                    selector: 
                    `.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container,
                    .ultpMenuCss .wp-block-ultimate-post-menu-item > .ultp-current-link.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container` 
                }
                ],
            },

            itemBorder: {
                type: 'object',
                default: {openBorder:0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff',type: 'solid'},
                style:[{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container' }],
            },
            itemRadius: {
                type: 'object',
                default: { lg: '', unit:'px'},
                style: [{ selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { border-radius:{{itemRadius}}; }' }],
            },
            itemPadding: {
                type: 'object',
                default: { lg: {top: '4',bottom: '4', left: '4', right: '4', unit:'px'}},
                style:[ {selector: '.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container { padding:{{itemPadding}}; }'} ],
            },

            /*==========================
                Item Icon
            ==========================*/
            iconAfterText: {
                type: 'boolean',
                default: false,
            },
            iconSize: {
                type: 'object',
                default: { lg:'16', ulg:'px' },
                style: [
                    {
                        selector: `
                        .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg,
                        .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon img { height: {{iconSize}}; width: {{iconSize}}; }`
                    }
                ],
            },
            iconSpacing: {
                type: 'object',
                default: { lg:'10', ulg:'px' },
                style: [
                    {
                        
                        selector: '.ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-label { gap: {{iconSpacing}}; }' 
                    },
                ],
            },
            iconColor: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        selector:'.ultpMenuCss .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg { fill:{{iconColor}}; stroke:{{iconColor}}; }'
                    },
                ]
            },
            // hover
            iconColorHvr: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        selector:'.ultpMenuCss .wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper[data-parentbid="{{ULTP}}"] > .ultp-menu-item-label-container .ultp-menu-item-icon svg { fill:{{iconColorHvr}}; stroke:{{iconColorHvr}};}'
                    },
                ]
            },

            /*==========================
                    Mobile View
            ==========================*/
            hasRootMenu: {
                type: 'string',
                default: '',
            },
            menuHamAppear: {
                // type: 'string',
                // default: 'custom',
                type: 'boolean',
                default: true
            },
            mvTransformToHam: {
                type: 'string',
                default: '800',
                style: [
                    {
                        depends: [
                            { key:'menuHamAppear', condition: '==', value: true },
                        ]
                    }
                ],
            },
            previewMobileView: {
                type: 'string',
                default: '',
            },
            mvPopWidth: {
                type: 'string',
                default: { _value: '84', unit: '%', onlyUnit: true },
                style: [
                    {   
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper { width: {{mvPopWidth}}; }'
                    }
                ],
            },
            menuResStructure: {
                type: 'string',
                default: 'mv_slide'
            },
            hamExpandedPadding: {
                type: 'object',
                default: { top: "10", right: "0", bottom: "10", left: "15", unit: "px"},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                            { key:'menuResStructure', condition: '==', value: 'mv_accordian' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-container .ultp-mobile-view-wrapper .ultp-mobile-view-body .wp-block-ultimate-post-menu-item .ultp-menu-item-content { padding: {{hamAccorPadding}}; }'
                    }
                ]
            },
            mvAnimationDuration: {
                type: 'string',
                default: '400'
            },
            mvDrawerPosition: {
                type: 'string',
                default: 'left'
            },
            /*==========================
                Hamburger Icon
            ==========================*/
            previewHamIcon: {
                type: 'string',
                default: '',
            },
            mvHamIcon: {
                type: 'string',
                default: 'hamicon_3',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ]
                    },
                ]
            },
            mvHamIconSize: {
                type: 'string',
                default: '24',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mv-ham-icon svg { height: {{mvHamIconSize}}px; width: {{mvHamIconSize}}px; }`
                    },
                ]
            },
            hamVClr: {
                type: 'string',
                default: '#070707',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mv-ham-icon svg { fill: {{hamVClr}}; }`
                    },
                ]
            },
            hamVBg: {
                type: 'object',
                default: { openColor: 0, type: 'color', color: '' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon'
                    }
                ],
            },
            hamVBorder: {
                type: 'object',
                default: { openBorder: 0, width: { top: 0, right: 0, bottom: 0, left: 0},color: '#dfdfdf' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon'
                    }
                ]
            },
            hamVRadius: {
                type: 'object',
                default: { lg:'', unit:'px' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon { border-radius: {{hamVRadius}}; }'
                    }
                ]
            },
            hamVShadow:{
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff'},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon'
                    }
                ]
            },
            hamVPadding: {
                type: 'object',
                default: { lg: {top: '',bottom: '', unit:'px'}},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon { padding: {{hamVPadding}}; }'
                    }
                ]
            },
            // Hover
            hamVClrHvr: {
                type: 'string',
                default: '#070707',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mv-ham-icon:hover svg { fill: {{hamVClrHvr}}; }`
                    },
                ]
            },
            hamVBgHvr: {
                type: 'object',
                default: { openColor: 0, type: 'color', color: '' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon:hover'
                    }
                ],
            },
            hamVBorderHvr: {
                type: 'object',
                default: { openBorder: 0, width: { top: 0, right: 0, bottom: 0, left: 0},color: '#dfdfdf' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon:hover'
                    }
                ]
            },
            hamVRadiusHvr: {
                type: 'object',
                default: { lg:'', unit:'px' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon:hover { border-radius: {{hamVRadiusHvr}}; }'
                    }
                ]
            },
            hamVShadowHvr: {
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff'},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mv-ham-icon:hover'
                    }
                ]
            },
            /*==========================
                    Navigation Icon
            ==========================*/
            naviIcon: {
                type: 'string',
                default: 'rightAngle2',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ]
                    },
                ]
            },
            naviExpIcon: {
                type: 'string',
                default: 'arrowUp2',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                            { key:'menuResStructure', condition: '==', value: 'mv_accordian' },
                        ]
                    },
                ]
            },
            naviIconSize: {
                type: 'string',
                default: '18',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { height: {{naviIconSize}}px; width: {{naviIconSize}}px; }`
                    },
                ]
            },
            naviIconClr: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container .ultp-menu-item-dropdown svg { fill: {{naviIconClr}}; }`
                    },
                ]
            },
            naviIconClrHvr: {
                type: 'string',
                default: '#000',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-hammenu-accordian-active > .ultp-menu-item-wrapper >  .ultp-menu-item-label-container > .ultp-menu-item-dropdown svg,
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container .ultp-menu-item-dropdown:hover svg { fill: {{naviIconClrHvr}}; }`
                    },
                ]
            },

            /*==========================
                    Close Icon
            ==========================*/
            closeIcon: {
                type: 'string',
                default: 'close_line',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ]
                    },
                ]
            },
            closeSize: {
                type: 'string',
                default: '18',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close svg { height: {{closeSize}}px; width: {{closeSize}}px; }`
                    },
                ]
            },
            closeClr: {
                type: 'string',
                default: '#fff',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close svg { fill: {{closeClr}}; }`
                    },
                ]
            },
            closeClrHvr: {
                type: 'string',
                default: '#fff',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close:hover svg { fill: {{closeClrHvr}}; }`
                    },
                ]
            },
            closeWrapBg: {
                type: 'string',
                default: '#999999',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close  { background-color: {{closeWrapBg}}; }`
                    },
                ]
            },
            closeWrapBgHvr: {
                type: 'string',
                default: '#ED2A2A',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close:hover  { background-color: {{closeWrapBgHvr}}; }`
                    },
                ]
            },
            closeSpace: {
                type: 'object',
                default: { left: 0, unit:"px", right: -44 },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close { margin: {{closeSpace}}; }'
                    }
                ]
            },
            closePadding: {
                type: 'object',
                default: { unit: "px", top: "8", right: "8", bottom: "8", left: "8" },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close { padding: {{closePadding}}; }'
                    }
                ]
            },
            closeRadius: {
                type: 'object',
                default: { top: "26", right: "26", bottom: "26", left: "26", unit:'px' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close { border-radius: {{closeRadius}}; }'
                    }
                ]
            },
            closeBorder: {
                type: 'object',
                default: { openBorder: 0, width: { top: 0, right: 0, bottom: 0, left: 0},color: '#dfdfdf' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-close'
                    }
                ]
            },
            /*==========================
                    Menu Items
            ==========================*/
            menuMvInheritCss: {
                type: 'boolean',
                default: false,
            },
            mvItemColor: {
                type: 'string',
                default: '#070707',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: `{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-text { color: {{mvItemColor}}; }
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container svg { fill: {{mvItemColor}}; } `
                    },
                ]
            },
            mvItemBg: {
                type: 'object',
                default: { openColor: 0, type: 'color', color: '' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container'
                    }
                ],
            },
            mvItemColorHvr: {
                type: 'string',
                default: '#6044FF',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: `{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container:hover .ultp-menu-item-label-text { color: {{mvItemColorHvr}}; }
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container:hover svg { fill: {{mvItemColorHvr}}; } `
                    },
                ]
            },
            mvItemBgHvr: {
                type: 'object',
                default: { openColor: 0, type: 'color', color: '#fff', size: 'cover', repeat: 'no-repeat', loop: true},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container:hover'
                    }
                ],
            },
            mvItemSpace: {
                type: 'string',
                default: '0',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: `{{ULTP}} .ultp-mobile-view-body .ultp-menu-content, 
                        {{ULTP}} .ultp-mobile-view-body { gap: {{mvItemSpace}}px; }`
                    }
                ]
            },
            mvItemTypo: {
                type: 'object',
                default: { openTypography: 1, size: { lg:16, unit:'px' }, height: { lg:'20', unit:'px' }, decoration: 'none',family: '', weight: 400, transform: "capitalize" },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-text'
                    }
                ]
            },
            mvItemBorder: {
                type: 'object',
                default: { openBorder: 1, width: { top: 0, right: 0, bottom: 1, left: 0 }, color: '#E6E6E6',type: 'solid' },
                style:[
                    { 
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container' 
                    }
                ],
            },
            mvItemRadius: {
                type: 'object',
                default: { lg: '', unit:'px'},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container { border-radius:{{mvItemRadius}}; }' 
                    }
                ],
            },
            mvItemPadding: {
                type: 'object',
                default: { lg: { top: '20', bottom: '20', left: '', right: '', unit:'px'}},
                style:[
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                            { key:'menuMvInheritCss', condition: '==', value: false },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-wrapper .ultp-menu-item-label-container { padding:{{mvItemPadding}}; }'
                    } 
                ],
            },
            /*==========================
                    Menu Header
            ==========================*/
            mvHeadtext: {
                type: 'string',
                default: 'Menu'
            },
            mvHeadBg: {
                type: 'object',
                default: { openColor: 1, type: 'color', color: '#F1F1F1', size: 'cover', repeat: 'no-repeat', loop: true},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-head'
                    }
                ],
            },
            mvHeadPadding: {
                type: 'object',
                default: { lg: {top: '18', bottom: '18', left: '24', right: '4', unit:'px' } },
                style:[
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-head { padding: {{mvHeadPadding}}; }'
                    }
                ],
            },
            mvHeadBorder: {
                type: 'object',
                default: { openBorder: 0, width: { top: 0, right: 0, bottom: 2, left: 0 },color: '#dfdfdf' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-head'
                    }
                ]
            },
            backClr: {
                type: 'string',
                default: '#070707',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label { color: {{backClr}}; }
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label-con svg { fill: {{backClr}}; }`
                    },
                ]
            },
            backClrHvr: {
                type: 'string',
                default: '#070707',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label-con:hover .ultp-mv-back-label { color: {{backClrHvr}}; }
                        {{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label-con:hover svg { fill: {{backClrHvr}}; }`
                    },
                ]
            },
            backTypo: {
                type: "object",
                default: {
                  openTypography: 1,
                  size: { lg: "16", unit: "px" },
                  height: { lg: "20", unit: "px" },
                  decoration: "none",
                  transform: "capitalize",
                  family: "",
                  weight: "400",
                },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: "{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label-con .ultp-mv-back-label"
                    }
                ],
            },
            backIcon: {
                type: 'string',
                default: 'leftArrowLg',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ]
                    },
                ]
            },
            backIconSize: {
                type: 'string',
                default: '20',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mv-back-label-con svg { height: {{backIconSize}}px; width: {{backIconSize}}px; }`
                    },
                ]
            },
            backIconSpace: {
                type: 'string',
                default: '10',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:`{{ULTP}} .ultp-mobile-view-wrapper .ultp-mv-back-label-con { gap: {{backIconSpace}}px; }`
                    },
                ]
            },
            /*==========================
                    Background
            ==========================*/
            mvBodyBg: {
                type: 'object',
                default: { openColor: 1, type: 'color', color: '#ffffff', size: 'cover', repeat: 'no-repeat', loop: true},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-body'
                    }
                ],
            },
            mvOverlay: {
                type: 'string',
                default: 'rgba(0,10,20,.3411764706)',
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-container { background: {{mvOverlay}}; }'
                    }
                ],
            },
            mvBodyPadding: {
                type: 'object',
                default: { lg: { top: '16', bottom: '24', left: '16', right: '16', unit:'px' } },
                style:[
                    {
                        depends: [
                            { key:'hasRootMenu', condition:'!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-body { padding: {{mvBodyPadding}}; }'
                    }
                ],
            },
            mvBodyBorder: {
                type: 'object',
                default: { openBorder: 0, width: { top: 0, right: 0, bottom: 2, left: 0 },color: '#dfdfdf' },
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector:'{{ULTP}} .ultp-mobile-view-body'
                    }
                ]
            },
            mvBodyShadow: {
                type: 'object',
                default: { openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#037fff'},
                style: [
                    {
                        depends: [
                            { key:'hasRootMenu', condition: '!=', value: 'hasRootMenu' },
                        ],
                        selector: '{{ULTP}} .ultp-mobile-view-body'
                    }
                ]
            },

            /*==========================
                General Advance Settings
            ==========================*/
            advanceId:{
                type: 'string',
                default: '',
            },
            advanceZindex:{
                type: 'string',
                default: '',
                style:[{ selector: '{{ULTP}} > .ultp-menu-wrapper { z-index: {{advanceZindex}}; }' }],
            },
            hideExtraLarge: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideTablet: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideMobile: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            advanceCss: {
                type: 'string',
                default: '',
                style: [{selector: ''}],
            }
        }
    }
)