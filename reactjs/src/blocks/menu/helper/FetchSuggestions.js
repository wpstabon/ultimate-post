import Select from "../../../helper/fields/Select";
import IconPack from "../../../helper/fields/tools/IconPack";
const { __ } = wp.i18n;

const { useState, useEffect } = wp.element;
const { TextControl, Spinner, Placeholder } = wp.components;

export const FetchSuggestions = (props) => {
    const { callback, params, hasStep } = props;
    const [ query, setQuery ] = useState('');
    const [ loading, setLoading ] = useState(true);
    const [ addStep, setAddStep ] = useState(1);
    const [ extraData, setExtraData ] = useState({
        _title: '',
        _url: '',
        _target: '_self',
    });
    const [ suggestions, setSuggestions ] = useState([]);
    const [toggle, setToggle ] = useState(false);

    const handleSearch = (params) => {
        const param = params ? params : query == '' ? { isInitialSuggestions: true, initialSuggestionsSearchOptions: { perPage: '6' } } : {};
        wp?.coreData?.__experimentalFetchLinkSuggestions(query, param)
        .then((res) => {
            setSuggestions(res);
            loading && setLoading(false);
        })
        .catch((error) => { setSuggestions([]); console.error('Error fetching link suggestions:', error) });
    };
    useEffect(() => {
        handleSearch(params);
    }, [query]);

    const handleUrl = (title, url) => {
        if ( hasStep ) {
            setAddStep(2);
            setExtraData({
                ...extraData, 
                _title: title || '',
                _url: url || ''
            });
        } else {
            callback({
                ...extraData, 
                _title: title || '',
                _url: url || ''
            });
        }
    }
    const handleChange = ( value, type ) => {
        setExtraData({...extraData, [type]: value});
    }

    return (
        addStep == 1 ?
        <div className="ultp-menu-searchdropdown-con">
            <div className="ultp-menu-search-text">
                <input type="text" placeholder="Search or Type Url" onChange={(e)=> setQuery(e.target.value)} />
                <div className={`ultp-menu-search-add ${query ? '__active' : ''}`}
                    onClick={() => query && handleUrl('', query)}
                >
                    {IconPack['top_left_angle_line']}
                </div>
            </div>
            {
                loading ? <Placeholder
                    className={`ultp-backend-block-loading ultp-menu-block`}
                    label={__('Fetching...', 'ultimate-post')}
                >
                    <Spinner />
                </Placeholder>
                :
                <div className="ultp-menu-search-items">
                    { suggestions.map( (s, key) => {
                        return (
                            <div onClick={() => handleUrl(s.title, s.url)} key={key} >
                                <div>{s.title}</div>
                                <div className="__type">{s.type}</div>
                            </div>
                        );
                    })}
                </div>
            }
        </div>
        :
        <div className="ultp-section-accordion ultp-section-fetchurl" id="ultp-sidebar-inline">
            <div className="ultp-section-show ultp-toolbar-section-show">
                <TextControl
                    __nextHasNoMarginBottom={true}
                    value={extraData._title}
                    placeholder={'Type Label'}
                    label={'Label'}
                    onChange={ v => handleChange(v, '_title')}
                />
                <div className={`ultp-field-selflink`}>
                    <label>Link</label>
                    <div>
                        <span className={`ultp-link-section`}>
                            <span className="ultp-link-input">
                                <input type="text" onChange={(v) => handleChange(v.target.value, '_url')}  value={extraData._url} placeholder={'Type Url'}/>
                                {   extraData._url &&
                                    <span className="ultp-image-close-icon dashicons dashicons-no-alt" onClick={() => handleChange('', '_url')}/>
                                }
                            </span>
                            <span className={`ultp-link-options`}>
                                <span className={`ultp-collapse-section`} onClick={()=>{ setToggle(!toggle)}}>
                                    <span className={`ultp-short-collapse ${toggle && ' active'}`}/>
                                </span>
                            </span>
                        </span>
                        { toggle &&
                            <div className={`ultp-short-content active`}>
                                <Select
                                    value={extraData._target}
                                    label={"Link Target"}
                                    options={[
                                        { value:'_self',label: __('Same Tab','ultimate-post') },
                                        { value:'_blank',label: __('New Tab','ultimate-post') },
                                    ]}
                                    onChange={(v) => handleChange(v, '_target')}
                                />
                            </div>
                        }
                    </div>
                </div>
                <div className="ultp-section-fetchurl-add" onClick={()=> callback(extraData)}>{__('Save','ultimate-post') }</div>
            </div>
        </div>
    );
}