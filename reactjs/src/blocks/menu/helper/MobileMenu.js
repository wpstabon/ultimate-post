import IconPack from "../../../helper/fields/tools/IconPack";
export const MobileMenu = (props) => {
    const {
        blockId,
        backIcon,
        closeIcon,
        mvHamIcon,
        naviIcon,
        mvHeadtext,
        previewHamIcon,
        menuAlign
    } = props.attributes;
    const margin = menuAlign == 'right' ? { "margin-left": "auto" } : menuAlign == 'left' ? { "margin-right": "auto" } : { "margin": "auto" };
    return (
        previewHamIcon ? <div style={{ display: 'block', width: 'fit-content', ...margin}} className="ultp-mv-ham-icon ultp-active">{IconPack[mvHamIcon]}</div>
        :
        <div className={`ultp-mobile-view-container-preview`} style={{backgroundImage: `url(${ultp_data.url}assets/img/blocks/menu/mobile_preview.png)`}}>
            <div className={`ultp-mobile-view-container ultp-editor`}>
                <div className="ultp-mobile-view-wrapper">
                    <div className="ultp-mobile-view-head">
                        <div className="ultp-mv-back-label-con">
                            {IconPack[backIcon]}
                            <div className="ultp-mv-back-label">{mvHeadtext}</div>
                        </div>
                        <div className="ultp-mv-close">{IconPack[closeIcon]}</div>
                    </div>
                    <div className="ultp-mobile-view-body">
                        <div data-bid="" className="wp-block-ultimate-post-menu-item">
                            <div data-parentbid={`.ultp-block-${blockId || 'null'}`} className="ultp-menu-item-wrapper">
                                <div className="ultp-menu-item-label-container">
                                    <a className="ultp-menu-item-label ultp-menuitem-pos-aft">
                                        <div className="ultp-menu-item-label-text">Home</div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div data-bid="" className="wp-block-ultimate-post-menu-item ">
                            <div data-parentbid={`.ultp-block-${blockId || 'null'}`} className="ultp-menu-item-wrapper">
                                <div className="ultp-menu-item-label-container">
                                    <a className="ultp-menu-item-label ultp-menuitem-pos-aft">
                                        <div className="ultp-menu-item-label-text">Blog</div>
                                    </a>
                                    <div className="ultp-menu-item-dropdown">
                                        {IconPack[naviIcon]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-bid="" className="wp-block-ultimate-post-menu-item ">
                            <div data-parentbid={`.ultp-block-${blockId || 'null'}`} className="ultp-menu-item-wrapper">
                                <div className="ultp-menu-item-label-container">
                                    <a className="ultp-menu-item-label ultp-menuitem-pos-aft">
                                        <div className="ultp-menu-item-label-text">Features</div>
                                        {/* <div className="ultp-menu-item-icon">
                                            <svg></svg>
                                        </div> */}
                                        {/* <div data-currentbid=".ultp-block-4acba9" className="ultp-menu-editor ultp-menu-item-badge">hot</div> */}
                                    </a>
                                    <div className="ultp-menu-item-dropdown">
                                        {IconPack[naviIcon]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-bid="" className="wp-block-ultimate-post-menu-item">
                            <div data-parentbid={`.ultp-block-${blockId || 'null'}`} className="ultp-menu-item-wrapper">
                                <div className="ultp-menu-item-label-container">
                                    <a className="ultp-menu-item-label ultp-menuitem-pos-aft">
                                        <div className="ultp-menu-item-label-text">Support</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div data-bid="" className="wp-block-ultimate-post-menu-item ">
                            <div data-parentbid={`.ultp-block-${blockId || 'null'}`} className="ultp-menu-item-wrapper">
                                <div className="ultp-menu-item-label-container">
                                    <a className="ultp-menu-item-label ultp-menuitem-pos-aft">
                                        <div className="ultp-menu-item-label-text">About Us</div>
                                        {/* <div className="ultp-menu-item-icon">
                                            <svg></svg>
                                        </div> */}
                                        {/* <div data-currentbid=".ultp-block-4acba9" className="ultp-menu-editor ultp-menu-item-badge">hot</div> */}
                                    </a>
                                    <div className="ultp-menu-item-dropdown">
                                        {IconPack[naviIcon]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};