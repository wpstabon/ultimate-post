const { __ } = wp.i18n
const { InspectorControls, InnerBlocks, BlockControls } = wp.blockEditor
const { Toolbar, ToolbarButton } = wp.components;
const { useState, useEffect, useRef, Fragment, useMemo } = wp.element
import { CssGenerator } from '../../../helper/CssGenerator';
import icons from '../../../helper/icons';
import { MegaMenuSettings } from './Settings';
const { dispatch } = wp.data;
const { 
    getBlockAttributes,
    getBlockParents,
    getBlockCount
} = wp.data.select('core/block-editor');

export default function Edit(props) {
    const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
    const {
        setAttributes, 
        name, 
        className, 
        attributes, 
        clientId, 
        attributes: {
            blockId, 
            advanceId,
            megaWidthType,
            megaParentBlock,
            megaAlign
        }
        
    } = props;
    const store = {
        setAttributes, 
        name,
        attributes, 
        setSection: setSection,
        section: section,
        clientId
    }

    useEffect(() => {
        const _client = clientId.substr(0, 6)
        if ( !blockId ) {
            setAttributes({ blockId: _client });
        } else if ( blockId && blockId != _client ) {
            setAttributes({ blockId: _client });
            // document.getElementById('ultp-block-' +blockId)?.remove();
        }

        handleMegaMenuWidth('setTimeout');
        setTimeout(()=> {
            handleMegaMenuWidth('setTimeout');
            const mainElement = document.querySelector('.editor-styles-wrapper');
            const resizeObserver = new ResizeObserver((e) => {
                handleMegaMenuWidth('resizeObserver');
            });
            if ( mainElement ) {
                resizeObserver.observe(mainElement);
            }
        }, 1000);

        document.addEventListener('click', function(e) { 
            handleMegaMenuWidth('click'); 
        });
	}, [clientId]);

    const handleMegaMenuWidth = (t) => {
        const hasMegaMenu = document.querySelectorAll('.wp-block-ultimate-post-menu-item.hasMegaMenuChild > .ultp-menu-item-wrapper > .ultp-menu-item-content');
        if ( hasMegaMenu.length > 0 ) {
            hasMegaMenu.forEach(function (item) {
                if (item?.classList.contains('ultpMegaWindowWidth')) {
                    const editorMainWrapper = document.querySelector('.editor-styles-wrapper');
                    // @ts-ignore
                    const editorMainWrapperWidth = editorMainWrapper ? editorMainWrapper.offsetWidth : 800;
                    const editorMainWrapperLeft = editorMainWrapper ? editorMainWrapper.getBoundingClientRect().left : 0;
                    const contentLeft = item.getBoundingClientRect().left;

                    const megaMenuWrapper = item.querySelector(' .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-mega-menu > .ultp-mega-menu-wrapper');
                    if ( megaMenuWrapper ) {
                        // @ts-ignore
                        megaMenuWrapper.style.maxWidth = `${editorMainWrapperWidth}px`;
                        // @ts-ignore
                        megaMenuWrapper.style.boxSizing = 'border-box';
                    }
                    const innerBlocks = item.querySelector(' .block-editor-inner-blocks');
                    if ( innerBlocks ) {
                        // @ts-ignore
                        innerBlocks.style.left = `-${contentLeft - editorMainWrapperLeft}px`;
                    }

                } else if ( item?.classList.contains('ultpMegaMenuWidth') ) {
                    const closetMenu = item.closest('.wp-block-ultimate-post-menu');
                    // @ts-ignore
                    const closetMenuWidth = closetMenu ? closetMenu.offsetWidth : 800;
                    const closetMenuLeft = closetMenu ? closetMenu.getBoundingClientRect().left : 0;
                    const contentLeft = item.getBoundingClientRect().left;

                    const megaMenuWrapper = item.querySelector(' .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-mega-menu > .ultp-mega-menu-wrapper');
                    if ( megaMenuWrapper ) {
                        // @ts-ignore
                        megaMenuWrapper.style.maxWidth = `${closetMenuWidth}px`;
                        // @ts-ignore
                        megaMenuWrapper.style.boxSizing = 'border-box';
                    }
                    const innerBlocks = item.querySelector('.block-editor-inner-blocks');
                    if ( innerBlocks ) {
                        // @ts-ignore
                        innerBlocks.style.left = `-${contentLeft - closetMenuLeft}px`;
                    }

                } else {
                    const megaMenuWrapper = item?.querySelector('.block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block > .wp-block-ultimate-post-mega-menu > .ultp-mega-menu-wrapper');
                    if ( megaMenuWrapper ) {
                        // @ts-ignore
                        megaMenuWrapper.style.maxWidth = '';
                        // @ts-ignore
                        megaMenuWrapper.style.boxSizing = '';
                    }
                    const innerBlocks = item?.querySelector('.block-editor-inner-blocks');
                    if ( innerBlocks ) {
                        // @ts-ignore
                        innerBlocks.style.left = '';
                    }
                }
            });
        }
    }

    useEffect(() => {
		const prevAttributes = prevPropsRef.current;
        const parentIDs = getBlockParents( clientId );

		if ( !prevAttributes ) {
			prevPropsRef.current = attributes;
		} else if ( prevPropsRef.megaWidthType != megaWidthType ) {
            dispatch( 'core/block-editor' ).updateBlockAttributes(parentIDs[parentIDs.length - 1], { megaWidthType: megaWidthType } );
            prevPropsRef.current = attributes;
        }

        const { parentBlock } = getBlockAttributes(parentIDs[parentIDs.length - 1]) || {};
        if ( megaParentBlock != parentBlock ) { // check if it is inside a menu/list menu block
            setAttributes({megaParentBlock: parentBlock});
        }
	}, [attributes]);

    const blockCss = useMemo(() => {
        return CssGenerator(attributes, 'ultimate-post/mega-menu', blockId, false );
    }, [attributes, clientId]);

    const hasInnerBlocks = getBlockCount( clientId );

    return (
        <Fragment>
            <BlockControls>
                <Toolbar>
                    <ToolbarButton
                        label="Delete Mega Menu"
                        icon={icons.delete}
                        onClick={() => {
                            wp.data.dispatch( 'core/block-editor' ).removeBlock(clientId, true)
                        }}
                    />
                </Toolbar>
            </BlockControls>
            <InspectorControls>
                <MegaMenuSettings store={store} />
            </InspectorControls>
            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className} ultpMenuCss`}>
                { blockCss &&
                    <style dangerouslySetInnerHTML={{__html: blockCss}}></style>
                }
                <div className={`ultp-mega-menu-wrapper`}>
                    <div className={`ultp-mega-menu-content _${megaAlign}`}>
                        <InnerBlocks
                            templateLock={ false }
                            renderAppender={hasInnerBlocks ? undefined : () => <InnerBlocks.ButtonBlockAppender />}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}