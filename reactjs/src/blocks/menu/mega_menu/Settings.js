import { CommonSettings, CustomCssAdvanced, ResponsiveAdvanced } from "../../../helper/CommonPanel"
import { Section, Sections } from "../../../helper/Sections"
const { __ } = wp.i18n

export const MegaMenuSettings = ({ store }) => {
    return (
        <>
            <Sections>
                <Section slug="global" title={__('Style','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 1, data: { type:'alignment',   key:'megaAlign', block: 'mega-menu',  disableJustify: true, icons: ['left', 'center', 'right'], options:['left', 'center', 'right'], label: __('Alignment', 'ultimate-post') } },
                            {
                                position: 10, data: { type:'select', key:'megaWidthType', label:__('Width Type','ultimate-post'), options:[
                                    { value:'windowWidth',label:__('Window Width','ultimate-post') },
                                    { value:'custom',label:__('Custom','ultimate-post') },
                                    { value:'parentMenuWidth',label:__('Parent Menu Width','ultimate-post') },
                                ]}
                            },
                            { position: 20, data: { type:'range',       key:'megaWidth', min: 0, max: 1800, step: 1, responsive: true, unit: ['px'], label:__('Width','ultimate-post') } },
                            { position: 30, data: { type:'range',       key:'megaContentWidth', min: 0, max: 1800, step: 1, responsive: true, unit: ['px'], label:__('Content Max Width','ultimate-post') } },
                            { position: 40, data: { type:'color2',      key:'megaBg', image: true, label: __('Background', 'ultimate-post') } },
                            { position: 50, data: { type:'border',      key:'megaBorder', label: __('Border', 'ultimate-post') } },
                            { position: 55, data: { type:'boxshadow',   key:'megaShadow', step:1 , unit:true , responsive:true , label:__('Box shadow','ultimate-post')} },
                            { position: 60, data: { type:'dimension',   key:'megaRadius',step:1 ,unit:true , responsive:true ,label:__('Radius','ultimate-post') }},
                            { position: 70, data: { type:'dimension',   key:'megaPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]} 
                        initialOpen={true}  store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={'inline'} 
                        include={[
                            { position: 1, data: { type: 'text',     key:'advanceId',        label:__('ID','ultimate-post') } },
                            { position: 2, data: { type: 'range',    key:'advanceZindex',    label:__('z-index','ultimate-post'), min:-100, max:10000, step:1 } }
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    )
}