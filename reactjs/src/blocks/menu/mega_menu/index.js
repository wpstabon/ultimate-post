const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/menu/', 'block_docs');

registerBlockType(
    'ultimate-post/mega-menu', {
        title: __('Mega Menu','ultimate-post'),
        parent:  ["ultimate-post/menu-item"],
        icon: <div className={`ultp-block-inserter-icon-section ultp-megamenu-pro-icon`}>
            <img src={ultp_data.url + 'assets/img/blocks/menu/mega_menu.svg'} alt="Mega Menu"/>
            {
                !ultp_data.active &&
                <span className={`ultp-pro-block`}>Pro</span>
            }
        </div>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Build Advanced and Engaging Multi-Level Menu.','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [ 
            __('menu','ultimate-post'),
            __('mega','ultimate-post'),
            __('mega menu','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
            align: []
        },
        example: {
            attributes: {
                previewImg: false,
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            blockId: {
                type: 'string',
                default: '',
            },
            previewImg: {
                type: 'string',
                default: '',
            },
            megaParentBlock: {
                type: 'string',
                default: '',
            },
            megaAlign: {
                type: 'string',
                default: 'center',
            },
            megaWidthType: {
                type: 'string',
                default: 'custom',
                style: [
                    {
                        depends: [
                            { key:'megaParentBlock', condition:'==', value: 'hasMenuParent' },
                        ],
                    },
                    {
                        depends: [
                            { key:'megaWidthType', condition:'==', value: 'windowWidth' },
                        ],
                        selector: '{{ULTP}} > .ultp-mega-menu-wrapper { width: 100vw; }' 
                    },
                    {
                        depends: [
                            { key:'megaWidthType', condition:'==', value: 'parentMenuWidth' },
                        ],
                        selector: '{{ULTP}} > .ultp-mega-menu-wrapper { width: 100vw; }' 
                    }
                ],
            },
            megaWidth: {
                type: 'object',
                default: { lg:'400', ulg:'px' },
                style: [
                    {
                        depends: [
                            { key:'megaWidthType', condition:'==', value: 'custom' },
                        ],
                        selector: '{{ULTP}} > .ultp-mega-menu-wrapper { width: {{megaWidth}}; }' 
                    },
                ],
            },
            megaContentWidth: {
                type: 'object',
                default: { lg:'400', ulg:'px' },
                style: [
                    {
                        selector: '{{ULTP}} > .ultp-mega-menu-wrapper > .ultp-mega-menu-content { max-width: {{megaContentWidth}}; }' 
                    },
                ],
            },
            megaBg: {
                type: 'object',
                default: { openColor: 1, type: 'color', color: '#F5F5F5', size: 'cover', repeat: 'no-repeat', loop: true },
                style:[{
                    selector: '{{ULTP}} > .ultp-mega-menu-wrapper'
                }],
            },
            megaBorder: {
                type: 'object',
                default: { openBorder: 1, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#D2D2D2' },
                
                style:[{
                    selector: '{{ULTP}} > .ultp-mega-menu-wrapper'
                }],
            },
            megaShadow: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style:[{
                    selector: '{{ULTP}} > .ultp-mega-menu-wrapper'
                }],
            },
            megaPadding: {
                type: 'object',
                default: { lg: { top: 16, bottom: 16,left: 16, right: 16, unit:'px' } },
                style:[{
                    selector: '{{ULTP}} > .ultp-mega-menu-wrapper { padding:{{megaPadding}}; }'
                }],
            },
            megaRadius: {
                type: 'object',
                default: { lg: { top: 4, bottom: 4,left: 4, right: 4, unit:'px' } },
                style:[{
                    selector: '{{ULTP}} > .ultp-mega-menu-wrapper { border-radius:{{megaRadius}}; }'
                }],
            },
            
            /*==========================
                General Advance Settings
            ==========================*/
            advanceId:{
                type: 'string',
                default: '',
            },
            advanceZindex:{
                type: 'string',
                default: '',
                style:[{ selector: '{{ULTP}} > .ultp-mega-menu-wrapper { z-index: {{advanceZindex}}; }' }],
            },
            hideExtraLarge: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideTablet: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            hideMobile: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} {display:none;}' }],
            },
            advanceCss: {
                type: 'string',
                default: '',
                style: [{selector: ''}],
            }
        }
    }
)