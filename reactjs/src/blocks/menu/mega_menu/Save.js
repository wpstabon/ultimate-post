const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { blockId, advanceId, megaAlign } = props.attributes;
    const extras = {}
    const extrasLength = Object.keys(extras);
        
    return (
        <div 
            {...(advanceId && {id:advanceId})}
            data-bid={blockId} 
            className={`ultp-block-${blockId} ultpMenuCss`}
            { ...( extrasLength && extras )}
        >
            <div className={`ultp-mega-menu-wrapper`}>
                <div className={`ultp-mega-menu-content _${megaAlign}`}>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    )
}