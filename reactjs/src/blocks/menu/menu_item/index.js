const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../../helper/UltpLinkGenerator';
import Edit from './Edit'
import Save from './Save'
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/menu/', 'block_docs');

registerBlockType(
    'ultimate-post/menu-item', {
        title: __('Menu Item','ultimate-post'),
        parent:  ["ultimate-post/menu", "ultimate-post/list-menu"],
        icon: <img src={ultp_data.url + 'assets/img/blocks/menu/menu_item.svg'} alt="Menu Item"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Edit and Optimize Individual Menu Items.','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [ 
            __('menu','ultimate-post'),
            __('item','ultimate-post')
        ],
        supports: {
            html: false,
            reusable: false,
        },
        example: {
            attributes: {
                previewImg: false,
            },
        },
        edit: Edit,
        save: Save,
        attributes: {
            blockId: {
                type: 'string',
                default: '',
            },
            previewImg: {
                type: 'string',
                default: '',
            },

            // dependencies start
            parentMenuInline: {     // Menu block inline or not
                type: 'boolean',
                default: true,
            },
            hasChildBlock: {         // has child block block inline or not
                type: 'boolean',
                default: false,
            },
            parentBlock: {
                type: 'string',
                default: '',
            },
            childBlock: {
                type: 'string',
                default: '',
            },
            childMegaWidth: {
                type: 'string',
                default: '',
            },
            parentDropIcon: {     // Parent Block(menu/list-menu) dropIcon
                type: 'string',
                default: 'collapse_bottom_line',
            },
            iconAfterText: {
                type: 'boolean',
                default: false,
            },
            parentBid: {
                type: 'string',
                default: '',
            },

            // dependencies end

            /*==========================
                InLine
            ==========================*/ 
            menuItemText: {
                type: 'string',
                default: 'List Item'
            },
            menuItemLink: {
                type: 'string',
                default: ''
            },
            menuLinkTarget: {
                type: 'string',
                default: '_self'
            },
            menuIconType: {
                type: 'string',
                default: 'icon'
            },
            menuItemIcon: {
                type: 'string',
                default: '',
                style: [
                    { 
                        depends: [
                            { key:'menuIconType', condition:'==', value: "icon" },
                        ], 
                    }
                ],
            },
            menuItemImg:{
                type: 'object',
                default: '',
                style: [
                    { 
                        depends: [
                            { key:'menuIconType', condition:'==', value: "img" },
                        ], 
                    }
                ],
            },
            menuItemSvg: {
                type: 'string',
                default: '',
                style: [
                    { 
                        depends: [
                            { key:'menuIconType', condition:'==', value: "svg" },
                        ], 
                    }
                ],
            },
            menuItemImgScale: {
                type: "string",
                default: "cover",
                style: [
                    {
                        depends: [
                            { key:'menuIconType', condition:'==', value: "img" },
                        ],
                        selector: "{{ULTP}} > .ultp-menu-item-wrapper .ultp-menu-item-label-container .ultp-menu-item-icon img {object-fit: {{menuItemImgScale}};}",
                    },
                ],
            },
            menuItemImgRadius: {
                type: "object",
                default: { lg: "", unit: "px" },
                style: [
                    {
                        depends: [
                            { key:'menuIconType', condition:'==', value: "img" },
                        ],
                        selector: "{{ULTP}} > .ultp-menu-item-wrapper .ultp-menu-item-label-container .ultp-menu-item-icon img {border-radius: {{menuItemImgRadius}};}",
                    },
                ],
            },
            contentHorizontalPositionType: {
                type: 'string',
                default: 'toRight',
                style: [
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: true },
                        ]
                    },
                    {
                        depends: [
                            { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                        ]
                    },
                ]
            },
            contentHorizontalPosition: {
                type: 'object',
                default: { lg:'16', ulg:'px' },
                style:[
                    
                    // Menu Inline
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: true },
                            { key:'contentHorizontalPositionType', condition:'==', value: 'toRight' },
                        ],
                        selector: `
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                        .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 0% + {{contentHorizontalPosition}} );} 
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 100% + {{contentHorizontalPosition}} ); }`
                    },
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: true },
                            { key:'contentHorizontalPositionType', condition:'==', value: 'toLeft' },
                        ],
                        selector: `{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                        .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 100% - {{contentHorizontalPosition}} );} 
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% + 0px ); width: calc( 0% + {{contentHorizontalPosition}} ); }`
                    },
                    // Menu List View
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: false },
                            { key:'parentBlock', condition:'!=', value: 'hasListMenuParent' },
                        ],
                        selector: `
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                        .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 100% + {{contentHorizontalPosition}} );} 
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 100% + {{contentHorizontalPosition}} ); }`
                    },
                    // List
                    {
                        depends: [
                            { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                            { key:'contentHorizontalPositionType', condition:'==', value: 'toRight' },
                        ],
                        selector: `
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks,
                        .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 100% + {{contentHorizontalPosition}} );} 
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 100% + {{contentHorizontalPosition}} ); }`
                    },
                    {
                        depends: [
                            { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                            { key:'contentHorizontalPositionType', condition:'==', value: 'toLeft' },
                        ],
                        selector: `
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                        .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { right: calc( 100% + {{contentHorizontalPosition}} );} 
                        {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% + 0px ); width: calc( 100% + {{contentHorizontalPosition}} ); }`
                    }
                ],
            },
            // contentHorizontalPosition2: {
            //     type: 'object',
            //     default: { lg:'0', ulg:'px' },
                // style:[
                //     {
                //         depends: [
                //             { key:'parentMenuInline', condition:'==', value: true },
                //             { key:'contentHorizontalPositionType', condition:'==', value: 'toRight' },
                //         ],
                //         selector: `
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                //         .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 0% + {{contentHorizontalPosition}} );} 
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 0% + {{contentHorizontalPosition}} ); }`
                //     },
                //     {
                //         depends: [
                //             { key:'parentMenuInline', condition:'==', value: true },
                //             { key:'contentHorizontalPositionType', condition:'==', value: 'toLeft' },
                //         ],
                //         selector: `{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                //         .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 0% - {{contentHorizontalPosition}} );} 
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% + 0px ); width: calc( 100% + {{contentHorizontalPosition}} ); }`
                //     },
                //     {
                //         depends: [
                //             { key:'parentMenuInline', condition:'==', value: false },
                //             { key:'parentBlock', condition:'!=', value: 'hasListMenuParent' },
                //         ],
                //         selector: `
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                //         .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 100% + {{contentHorizontalPosition}} );} 
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 0% + {{contentHorizontalPosition}} ); }`
                //     },
                //     {
                //         depends: [
                //             { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                //             { key:'contentHorizontalPositionType', condition:'==', value: 'toRight' },
                //         ],
                //         selector: `
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks,
                //         .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { left: calc( 100% + {{contentHorizontalPosition}} );} 
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { left: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 0% + {{contentHorizontalPosition}} ); }`
                //     },
                //     {
                //         depends: [
                //             { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                //             { key:'contentHorizontalPositionType', condition:'==', value: 'toLeft' },
                //             { key:'parentBlock', condition:'==', value: 'hasListMenuParent' },
                //         ],
                //         selector: `
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                //         .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { right: calc( 100% + {{contentHorizontalPosition}} );} 
                //         {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { right: calc( 0% - {{contentHorizontalPosition}} ); width: calc( 0% + {{contentHorizontalPosition}} ); }`
                //     }
                // ],
            // },
            contentVerticalPositionType: {
                type: 'string',
                default: 'toBottom',
                style: [
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: false },
                        ]
                    },
                ]
            },
            contentVerticalPosition: {
                type: 'object',
                default: { lg:'0', ulg:'px' },
                style: [
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: true }
                        ],
                        selector: `
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                            .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { top: calc( 100% + {{contentVerticalPosition}} );} 
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: calc( 0% - {{contentVerticalPosition}} ); height: calc( 100% + {{contentVerticalPosition}} ); }`
                    },
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: false },
                            { key:'contentVerticalPositionType', condition:'==', value: 'toBottom' },
                        ],
                        selector: `
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                            .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { top: calc( 0% + {{contentVerticalPosition}} );} 
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: calc( 0% - {{contentVerticalPosition}} ); height: calc( 100% + {{contentVerticalPosition}} ); }` 
                    },
                    {
                        depends: [
                            { key:'parentMenuInline', condition:'==', value: false },
                            { key:'contentVerticalPositionType', condition:'==', value: 'toTop' },
                        ],
                        selector: `
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
                            .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { top: calc( 100% - {{contentVerticalPosition}} );} 
                            {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: 0; height: calc( 0% + {{contentVerticalPosition}} ); }`
                    },
                ],
            },
            // contentVerticalPosition: {
            //     type: 'object',
            //     default: { lg:'0', ulg:'px' },
            //     style: [
            //         {
            //             depends: [
            //                 { key:'parentMenuInline', condition:'==', value: true }
            //             ],
            //             selector: `
            //                 {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
            //                 .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { top: calc( 100% + {{contentVerticalPosition}} );} 
            //                 {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: calc( 0% - {{contentVerticalPosition}} ); height: calc( 100% + {{contentVerticalPosition}} ); }`
            //         },
            //         {
            //             depends: [
            //                 { key:'parentMenuInline', condition:'==', value: false },
            //                 { key:'contentVerticalPositionType', condition:'==', value: 'toBottom' },
            //             ],
            //             selector: `
            //             {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
            //             .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { top: calc( 0% + {{contentVerticalPosition}} );} {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: calc( 0% - {{contentVerticalPosition}} ); height: calc( 100% + {{contentVerticalPosition}} ); }` 
            //         },
            //         {
            //             depends: [
            //                 { key:'parentMenuInline', condition:'==', value: false },
            //                 { key:'contentVerticalPositionType', condition:'==', value: 'toTop' },
            //             ],
            //             selector: `
            //             {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content > .block-editor-inner-blocks, 
            //             .postx-page {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content { bottom: calc( 100% + {{contentVerticalPosition}} );} {{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-content::before { top: 0; height: calc( 100% + {{contentVerticalPosition}} ); }`
            //         },
            //     ],
            // },
            
            /*==========================
                Badge
            ==========================*/
            enableBadge: {
                type: 'boolean',
                default: false,
            },
            badgeText: {
                type: 'string',
                default: 'BADGE',
            },
            badgeTypo: {
                type: 'object',
                default: {openTypography: 1, size: { lg: 12, unit:'px' }, height: {lg:16, unit:'px'}, transform: "uppercase", decoration: 'none',family: '', weight: 500},
                style: [
                    { 
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"]' 
                    }
                ]
            },
            badgeColor: {
                type: 'string',
                default: '#fff',
                style: [
                    { 
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { color:{{badgeColor}} }' 
                    }
                ]
            },
            badgeBg: {
                type: 'object',
                default: {openColor: 1, type: 'color', color: '#037FFF', gradient: {}},
                style: [
                    {
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"]' 
                    }
                ]
            },
            badgeBorder: {
                type: 'object',
                default: {openBorder:0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
                style: [
                    { 
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"]' 
                    }
                ]
            },
            badgeRadius: {
                type: 'object',
                default: {lg:{top: '12',bottom: '12',left: '12', right: '12', unit:'px'}},
                style: [
                    { 
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { border-radius:{{badgeRadius}}; }' 
                    }
                ]
            },
            badgePadding: {
                type: 'object',
                default: {lg:{top: '4',bottom: '4',left: '12', right: '12', unit:'px'}},
                style: [
                    { 
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { padding:{{badgePadding}}; }' 
                    }
                ]
            },
            badgePosition: {
                type: 'string',
                default: 'top',
            },
            badgeSpacing: {
                type: 'object',
                default: { lg:'8', ulg:'px' },
                style: [
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'top' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { bottom: calc( 100% + {{badgeSpacing}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'right' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { left: calc( 100% + {{badgeSpacing}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'bottom' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { top: calc( 100% + {{badgeSpacing}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'left' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { right: calc( 100% + {{badgeSpacing}} ); }'
                    },
                ]
            },
            badgePositionValue: {
                type: 'object',
                default: { lg:'10', ulg:'px' },
                style: [
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'top' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { left: calc( 0% + {{badgePositionValue}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'right' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { top: calc( 0% + {{badgePositionValue}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'bottom' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { left: calc( 0% + {{badgePositionValue}} ); }'
                    },
                    {
                        depends: [
                            { key:'badgePosition', condition:'==', value: 'left' },
                        ],
                        selector:'.ultp-menu-item-badge[data-currentbid="{{ULTP}}"] { top: calc( 0% + {{badgePositionValue}} ); }'
                    },
                ]
            },
            /*==========================
                Item Style
            ==========================*/
            itemTypo: {
                type: 'object',
                default: { openTypography: 0 ,size: { lg:16, unit:'px'},height: {lg:'', unit:'px'}, decoration: 'none',family: '', weight: 400, transform: "capitalize"},
                style: [{selector:'.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container .ultp-menu-item-label-text'}]
            },
            itemColor: {
                type: 'string',
                default: '',
                style: [{selector:'.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColor}}; }'}]
            },
            itemBg: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: ''},
                style:[{ selector: '.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }]
            },
            itemShadow: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [{ selector: '.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }],
            },
            itemColorHvr: {
                type: 'string',
                default: '',
                style: [{selector:'.ultpMenuCss {{ULTP}}.wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper > .ultp-menu-item-label-container .ultp-menu-item-label-text { color: {{itemColorHvr}}; }'}]
            },
            itemBgHvr: {
                type: 'object',
                default: {openColor: 0, type: 'color', color: '#FFFFFF'},
                style:[{ selector: '.ultpMenuCss {{ULTP}}.wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }]
            },
            itemShadowHvr: {
                type: 'object',
                default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
                style: [{ selector: '.ultpMenuCss {{ULTP}}.wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }],
            },
            itemBorderHvr: {
                type: 'object',
                default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
                style:[{ selector: '.ultpMenuCss {{ULTP}}.wp-block-ultimate-post-menu-item:hover > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }],
            },
            itemBorder: {
                type: 'object',
                default: {openBorder: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
                style:[{ selector: '.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container' }],
            },
            itemRadius: {
                type: 'object',
                default: { top: '', right: '', bottom: '', left: '' , unit:'px'},
                style: [{ selector: '.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container { border-radius:{{itemRadius}}; }' }],
            },
            itemPadding: {
                type: 'object',
                default: { lg: { top: '', right: '', bottom: '', left: '', unit:'px'} },
                style:[ {selector: '.ultpMenuCss{{ULTP}} > .ultp-menu-item-wrapper > .ultp-menu-item-label-container { padding:{{itemPadding}}; }'} ],
            },

            /*==========================
                General Advance Settings
            ==========================*/
            advanceId:{
                type: 'string',
                default: '',
            },
            advanceZindex:{
                type: 'string',
                default: '',
                style:[{ selector: '{{ULTP}} > .ultp-menu-item-wrapper { z-index: {{advanceZindex}}; }' }],
            },
            hideExtraLarge: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} { display:none; }' }],
            },
            hideTablet: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} { display:none; }' }],
            },
            hideMobile: {
                type: 'boolean',
                default: false,
                style: [{ selector: '{{ULTP}} { display:none; }' }],
            },
            advanceCss: {
                type: 'string',
                default: '',
                style: [{selector: ''}],
            }
        }
    }
)