import { CommonSettings, CustomCssAdvanced, ResponsiveAdvanced, ToolbarCommonSettings } from "../../../helper/CommonPanel"
import icons from "../../../helper/icons";
import { Section, Sections } from "../../../helper/Sections"
const { __ } = wp.i18n;
const { Dropdown, Toolbar, ToolbarButton, } = wp.components;

export const MenuItemToolbar = (props) => {
    const { clientId, isSelected, store } = props;
    
    return (
        <>
            {
                isSelected && 
                <Toolbar className="ultp-menu-toolbar-group">
                    <Dropdown
                        contentClassName="ultp-menu-toolbar-drop"
                        renderToggle={({ onToggle }) => (
                            <ToolbarButton
                                label="Update Link"
                                icon={icons.updateLink}
                                onClick={() => onToggle()} 
                            />
                        )}
                        renderContent={({onClose}) => (
                            <ToolbarCommonSettings title={false}
                                include={[
                                    { position: 5,  data:{ type:'text', key:'menuItemText', label: 'Label' } },
                                    { position: 10, data:{ 
                                        type:'linkbutton',
                                        key:'menuItemLink',
                                        onlyLink: true,
                                        extraBtnAttr: {
                                            target: {
                                                key: 'menuLinkTarget',
                                                options: [
                                                    { value:'_self',label:__('Same Tab','ultimate-post') },
                                                    { value:'_blank',label:__('New Tab','ultimate-post') },
                                                ],
                                                label:__('Link Target To','ultimate-post')
                                            },
                                            follow: false,
                                            sponsored: false,
                                            download: false,
                                        },
                                        label:__('Link','ultimate-post') 
                                    }}
                                ]}
                                initialOpen={true}  
                                store={store}
                            />
                        )}
                    />

                </Toolbar>
            }
            <Toolbar>
                <Dropdown
                    contentClassName="ultp-menu-toolbar-drop"
                    focusOnMount={true}
                    renderToggle={({ onToggle }) => (
                        <ToolbarButton
                            label="Style"
                            icon={icons.styleIcon}
                            onClick={() => onToggle()} 
                        />
                    )}
                    renderContent={({onClose}) => (
                        <ToolbarCommonSettings title={false}
                            include={[
                                { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                                
                                { position: 10, data: { type: 'tab', content: [
                                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                            { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                            { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                            { type:'boxshadow', key:'itemShadow', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                            { type:'border',    key:'itemBorder',    label: __('Border', 'ultimate-post') },
                                        ]
                                    },
                                    {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                            { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                            { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                            { type:'boxshadow', key:'itemShadowHvr', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                            { type:'border',    key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') },
                                        ]
                                    }
                                ]}},
                                { position: 30, data: { type:'dimension',   key:'itemRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                                { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                            ]}
                            initialOpen={true}  
                            store={store}
                        />
                    )}
                />
            </Toolbar>
            <Toolbar>
                <ToolbarButton
                    label="Duplicate"
                    icon={icons.duplicate}
                    onClick={() => {
                        
                        wp.data.dispatch( 'core/block-editor' ).duplicateBlocks([clientId], true)
                    }}
                />
            </Toolbar>
            <Toolbar>
                <ToolbarButton
                    label="Delete Item"
                    icon={icons.delete}
                    onClick={() => {
                        wp.data.dispatch( 'core/block-editor' ).removeBlock(clientId, true)
                    }}
                />
            </Toolbar>
        </>
    )
}
export const MenuItemSettings = ({ store, clientId }) => {

    return (
        <>
            <Sections>
                <Section slug="global" title={__('Global Style','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            { position: 5,  data:{ type:'text', key:'menuItemText', label: 'Label' } },
                            { 
                                position: 10, 
                                data: { 
                                    type:'linkbutton',
                                    key:'menuItemLink',
                                    onlyLink: true,
                                    extraBtnAttr: {
                                        target: {
                                            key: 'menuLinkTarget',
                                            options: [
                                                { value:'_self',label:__('Same Tab','ultimate-post') },
                                                { value:'_blank',label:__('New Tab','ultimate-post') },
                                            ],
                                            label:__('Link Target To','ultimate-post')
                                        },
                                        follow: false,
                                        sponsored: false,
                                        download: false,
                                    },
                                    label:__('Link','ultimate-post') 
                                } 
                            },
                            {
                                position: 40, data: { type:'tag', key:'menuIconType', label:__('Icon Type','ultimate-post'), options:[
                                    { value:'icon', label: __('Icon','ultimate-post') },
                                    { value:'img',  label: __('Image','ultimate-post') },
                                    { value:'svg',  label: __('Custom svg','ultimate-post') },
                                ]}
                            },
                            { position: 50, data: { type:'icon',    key:'menuItemIcon',label:__('Icon','ultimate-post')}},
                            { position: 60, data: { type:'media',   key:'menuItemImg', label:__('Image','ultimate-post')} },
                            { position: 70, data: { type:'textarea',   key:'menuItemSvg', label:__('SVG code','ultimate-post')} },
                            {
                                position: 80, data: {
                                    type: 'tag',
                                    key: 'menuItemImgScale',
                                    label: __('Image scale', 'ultimate-post'),
                                    options: [
                                        { value: '', label: __('None', 'ultimate-post') },
                                        { value: 'cover', label: __('Cover', 'ultimate-post') },
                                        { value: 'contain', label: __('Contain', 'ultimate-post') },
                                        { value: 'fill', label: __('Fill', 'ultimate-post') },
                                    ],
                                },
                            },
                            {
                                position: 90, data: { type:'dimension', key:'menuItemImgRadius',step:1 ,unit:true , responsive:true ,label:__('Image Radius','ultimate-post')}
                            },
                        ]}
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Badge','ultimate-post')} 
                        depend="enableBadge"
                        include={[
                            { position: 5,  data: { type:'text',        key:'badgeText',        label:__('Badge Label','ultimate-post') } },
                            { position: 10,  data: { type:'typography',  key:'badgeTypo',        label:__('Typography','ultimate-post') } },
                            { position: 20, data: { type:'color',       key:'badgeColor',       label:__('Color','ultimate-post')}},
                            { position: 30, data: { type:'color2',      key:'badgeBg',          label:__('Background','ultimate-post')}},
                            { position: 40, data: { type:'border',      key:'badgeBorder',      label:__('Border', 'ultimate-post') } },
                            { position: 50, data: { type:'dimension',   key:'badgeRadius',      label:__('Border Radius','ultimate-post'),      step:1, unit:true,  responsive:true } },
                            { position: 60, data: { type:'dimension',   key:'badgePadding',     label:__('Padding','ultimate-post'),            step:1, unit:true,  responsive:true } },
                            {
                                position: 70, data: { type:'tag', key:'badgePosition', label:__('Badge Position','ultimate-post'), options:[
                                    { value:'top',      label:__('Top','ultimate-post') },
                                    { value:'right',    label:__('Right','ultimate-post') },
                                    { value:'bottom',   label:__('Bottom','ultimate-post') },
                                    { value:'left',     label:__('Left','ultimate-post') },
                                ]}
                            },
                            { position: 80, data: { type:'range',    key:'badgePositionValue',    label:__('Position Adjustment','ultimate-post'), step:1, unit: ['px', '%'],  responsive:true, min:-200, max:200 } },
                            { position: 90, data: { type:'range',    key:'badgeSpacing',    label:__('Gap Between Label and Badge','ultimate-post'), step:1, unit: ['px', '%'],  responsive:true, min:0, max:200 } }
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Item Style','ultimate-post')}
                        include={[
                            { position: 5,  data: { type:'typography',  key:'itemTypo', label:__('Typography','ultimate-post') } },
                            
                            { position: 10, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                        { type:'color',     key:'itemColor',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBg',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadow', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorder',    label: __('Border', 'ultimate-post') },
                                        
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                        { type:'color',     key:'itemColorHvr',label:__('Color','ultimate-post')},
                                        { type:'color2',    key:'itemBgHvr',label:__('Background','ultimate-post')},
                                        { type:'boxshadow', key:'itemShadowHvr', step:1 ,unit:true , responsive:true ,label:__('Box shadow','ultimate-post')},
                                        { type:'border',    key:'itemBorderHvr',    label: __('Hover Border', 'ultimate-post') },
                                    ]
                                }
                            ]}},
                            { position: 30, data: { type:'dimension',   key:'itemRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post')} },
                            { position: 40, data: { type:'dimension',   key:'itemPadding',  step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }},
                        ]}
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Content Position','ultimate-post')} 
                        include={[
                            {
                                position: 40, data: { type:'tag', key:'contentHorizontalPositionType', label:__('Horizontal Position','ultimate-post'), options:[
                                    { value:'toLeft',label:__('To Left','ultimate-post') },
                                    { value:'toRight',label:__('To Right','ultimate-post') },
                                ]}
                            },
                            { position: 50, data: { type:'range',       key:'contentHorizontalPosition', min: 0, max: 1200, step: 1, responsive: true, unit: ['px'], label:__('Horizontal Position Value','ultimate-post') } },
                            {
                                position: 60, data: { type:'tag', key:'contentVerticalPositionType', label:__('Vertical Position','ultimate-post'), options:[
                                    { value:'toTop',label:__('To Top','ultimate-post') },
                                    { value:'toBottom',label:__('To Bottom','ultimate-post') },
                                ]}
                            },
                            { position: 70, data: { type:'range',  key: 'contentVerticalPosition', min: 0, max: 1200, step: 1, responsive: true, unit: ['px'], label:__('Vertical Position Value','ultimate-post') } },
                        ]} 
                        initialOpen={false} store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <CommonSettings title={'inline'} 
                        include={[
                            { position: 1, data: { type:'text',     key:'advanceId',        label:__('ID','ultimate-post') } },
                            { position: 2, data: { type:'range',    key:'advanceZindex',    label:__('z-index','ultimate-post'), min:-100, max:10000, step:1 } }
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    )
}