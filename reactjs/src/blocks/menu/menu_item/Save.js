const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { blockId,
        advanceId,
        menuItemText,
        hasChildBlock,
        parentBlock,
        childBlock,
        childMegaWidth,
        iconAfterText,
        parentDropIcon,
        menuItemLink,
        menuLinkTarget,
        enableBadge,
        badgeText,
        parentBid,
        menuIconType,
        menuItemIcon,
        menuItemImg,
        menuItemSvg,
    } = props.attributes;

    let itemIcon;
    if ( menuIconType == 'img' ) {
        itemIcon = <img src={menuItemImg?.url || "#" } alt="Menu Item Image" />;
    } else if ( menuIconType == 'icon' ) {
        itemIcon =  menuItemIcon ? '_ultp_mi_ic_'+ menuItemIcon + '_ultp_mi_ic_end_' : '';
    } else if ( menuIconType == 'svg' ) {
        itemIcon = menuItemSvg;
    }
    const extras = {}
    const extrasLength = Object.keys(extras);
    return (
        <div 
            {...(advanceId && {id:advanceId})} 
            data-bid={blockId} 
            className={`ultp-block-${blockId} ${parentBlock} ${childBlock} ultpMenuCss`}
            { ...( extrasLength && extras )}
        >
            <div data-parentbid={parentBid} className={`ultp-menu-item-wrapper`}>
                <div className='ultp-menu-item-label-container'>
                    <a 
                        className={`ultp-menu-item-label ultp-menuitem-pos-${!iconAfterText ? "bef" : "aft"}`}
                        {...(menuItemLink && {href: menuItemLink, target: menuLinkTarget, rel: "noopener" })}
                    >
                        { menuItemText && <div className="ultp-menu-item-label-text">{menuItemText}</div> }
                        { 
                            itemIcon && (
                                menuIconType == 'svg' ? <div className='ultp-menu-item-icon' dangerouslySetInnerHTML={{ __html: itemIcon }} ></div>:
                                <div className='ultp-menu-item-icon' >
                                    {itemIcon}
                                </div> 
                            )
                        }
                        {
                            enableBadge &&
                            <div data-currentbid={`.ultp-block-${blockId}`} className="ultp-menu-item-badge">{badgeText}</div>
                        }
                    </a>
                    {
                        parentDropIcon &&
                        <div className='ultp-menu-item-dropdown'>{ hasChildBlock && '_ultp_mi_ic_'+ parentDropIcon + '_ultp_mi_ic_end_'}</div>
                    }
                </div>
                <div className={`ultp-menu-item-content ${childMegaWidth}`}>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    )
}