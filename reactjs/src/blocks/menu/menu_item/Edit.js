const { __ } = wp.i18n
const { InspectorControls, InnerBlocks, RichText, BlockControls } = wp.blockEditor
const {  Fragment, useState, useEffect, useMemo } = wp.element
import { CssGenerator } from '../../../helper/CssGenerator';
import { MenuItemSettings, MenuItemToolbar } from './Settings';
import IconPack from '../../../helper/fields/tools/IconPack';
import icons from '../../../helper/icons';
const { Dropdown } = wp.components;

const { 
    getBlockAttributes,
    getBlockParents,
    getBlockCount,
    getBlocks,
    getBlockName
} = wp.data.select('core/block-editor');

export default function Edit(props) {
	const [section, setSection] = useState('Content');
    const {
        setAttributes,
        name,
        className,
        attributes,
        clientId,
        attributes: { 
            previewImg, 
            blockId,
            advanceId,
            menuItemText,
            hasChildBlock,
            childBlock,
            parentBlock,
            childMegaWidth,
            parentDropIcon,
            iconAfterText,
            enableBadge,
            badgeText,
            parentBid,
            menuIconType,
            menuItemIcon,
            menuItemImg,
            menuItemSvg,

            parentMenuInline,
        } 
    } = props;
    const store = {
        setAttributes,
        name,
        attributes,
        setSection: setSection,
        section: section,
        clientId
    };
    useEffect(() => {
        const _client = clientId.substr(0, 6);
        if ( !blockId ) {
            setAttributes({ blockId: _client });
        } else if ( blockId && blockId != _client ) {
            setAttributes({ blockId: _client });
            // document.getElementById('ultp-block-' +blockId)?.remove();
        }
	}, [clientId]);

    useEffect(() => {
        
        const parentIDs = getBlockParents( clientId );
        const immediateParentClientId = parentIDs[parentIDs.length - 1] 
        
        const childs  = getBlocks( clientId );
        const childBlockName = childs[0]?.name || '';
        const { megaWidthType } =  childs[0] ? childs[0].attributes : {};
        
        const parentBlockName = immediateParentClientId ? getBlockName(immediateParentClientId) : '';
        const { menuInline, dropIcon, iconAfterText, blockId } = getBlockAttributes(parentIDs[parentIDs.length - 1]) || {};

        const __parentBlock = parentBlockName == 'ultimate-post/menu' ? 'hasMenuParent' : parentBlockName == 'ultimate-post/list-menu' ? 'hasListMenuParent' : '';
        const __childBlock = childBlockName == 'ultimate-post/mega-menu' ? 'hasMegaMenuChild' : childBlockName == 'ultimate-post/list-menu' ? 'hasListMenuChild' : '';
        const __childMegaWidth = megaWidthType == 'windowWidth' ? 'ultpMegaWindowWidth' : megaWidthType == 'parentMenuWidth' ? 'ultpMegaMenuWidth' : 'ultpMegaCustomWidth';
        const __hasChildBlock = childs.length > 0;
        const __parentMenuInline = menuInline == undefined ? false : menuInline;
        const __dropIcon = dropIcon;
        const __iconAfterText = iconAfterText;
        const __parentBid = `.ultp-block-${blockId || 'null'}`;

        const toUpdate = {
            ...( parentBlock != __parentBlock && { parentBlock: __parentBlock } ),
            ...( childBlock != __childBlock && { childBlock: __childBlock } ),
            ...( childMegaWidth != __childMegaWidth && { childMegaWidth: __childMegaWidth } ),
            ...( hasChildBlock != __hasChildBlock && { hasChildBlock: __hasChildBlock } ),
            ...( parentMenuInline != __parentMenuInline && { parentMenuInline: __parentMenuInline } ),
            ...( parentDropIcon != __dropIcon && { parentDropIcon: __dropIcon } ),
            ...( attributes.iconAfterText != __iconAfterText && { iconAfterText: __iconAfterText } ),
            ...( parentBid != __parentBid && { parentBid: __parentBid } ),
        };

        if ( Object.keys(toUpdate).length > 0 ) {
            setAttributes(toUpdate);
        }
	}, [attributes]);
    
    const blockCss = useMemo(() => {
        return CssGenerator(attributes, 'ultimate-post/menu-item', blockId, false );
    }, [attributes, clientId]);

    if ( previewImg ) {
        return <img style={{marginTop: '0px', width: '420px'}} src={previewImg} />
    }
    
    const childIds  = getBlockCount( clientId );
    const __hasChildBlock = childIds > 0;
    if ( __hasChildBlock != hasChildBlock ) {
        setAttributes({hasChildBlock : __hasChildBlock});
    }

    let itemIcon;
    if ( menuIconType == 'img' ) {
        itemIcon = <img src={menuItemImg?.url || "#" } alt="Menu Item Image" />;
    } else if ( menuIconType == 'icon' ) {
        itemIcon = IconPack[menuItemIcon];
    } else if ( menuIconType == 'svg' ) {
        itemIcon = menuItemSvg;
    }
    
    return (
        <Fragment>
            <BlockControls>
                <MenuItemToolbar
                    isSelected={props.isSelected}
                    clientId={clientId}
                    store={store}
                    __hasChildBlock={__hasChildBlock}
                />
            </BlockControls>
            <InspectorControls>
                <MenuItemSettings clientId={clientId} store={store} />
            </InspectorControls>
            <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId} ${className} ${parentBlock} ${childBlock} ultpMenuCss`}>
                { blockCss &&
                    <style dangerouslySetInnerHTML={{__html: blockCss}}></style>
                }
                <div data-parentbid={parentBid} className={`ultp-menu-item-wrapper`}>
                    <div className='ultp-menu-item-label-container'>
                        <a style={{pointerEvents: "none"}} className={`ultp-menu-item-label ultp-menuitem-pos-${ !iconAfterText ? "bef" : "aft"}`}>
                            {
                                ( props.isSelected || menuItemText == '' ) ? <RichText
                                    key="editable"
                                    isSelected={false}
                                    tagName={'div'}
                                    className={'ultp-menu-item-label-text'}
                                    keepPlaceholderOnFocus
                                    placeholder={__('Item Text...','ultimate-post')}
                                    onChange={value => setAttributes({ menuItemText: value })}
                                    value={menuItemText} 
                                />
                                :
                                <div className="ultp-menu-item-label-text">{menuItemText}</div>
                            }
                            { 
                                itemIcon && (
                                    menuIconType == 'svg' ? <div className='ultp-menu-item-icon' dangerouslySetInnerHTML={{ __html: itemIcon }} ></div>:
                                    <div className='ultp-menu-item-icon' >
                                        {itemIcon}
                                    </div> 

                                )
                            }
                            { enableBadge && <div data-currentbid={`.ultp-block-${blockId}`} className="ultp-menu-item-badge">{badgeText}</div> }
                        </a>
                        {
                            parentDropIcon && <div className='ultp-menu-item-dropdown'>{ __hasChildBlock && IconPack[parentDropIcon]}</div>
                        }
                    </div>
                    <div className={`ultp-menu-item-content ${childMegaWidth}`}>
                        <InnerBlocks
                            renderAppender={ 
                                __hasChildBlock ? false : props.isSelected ? () => <Dropdown
                                    contentClassName="ultp-menu-add-toolbar"
                                    focusOnMount={true}
                                    renderToggle={({ onToggle }) => (
                                        <div
                                            className={"ultp-listmenu-add-item"}
                                            onClick={() => onToggle()} 
                                        >
                                            {icons.plus}
                                            <div className="__label">{__('Sub Menu', 'ultimate-post')}</div>
                                        </div>
                                    )}
                                    renderContent={({onClose}) => (
                                        <div className={`ultp-toolbar-menu-block-items`}>
                                            <div className="ultp-toolbar-menu-block-item"
                                                onClick={()=> {
                                                    wp.data.dispatch( 'core/block-editor' ).insertBlock(wp.blocks.createBlock("ultimate-post/list-menu", {}), 0, clientId);
                                                    onClose();
                                                }}
                                            >
                                                <div className="ultp-toolbar-menu-block-img">
                                                    <img src={ultp_data.url + 'assets/img/blocks/menu/list_menu.svg'} alt="List Menu"/>
                                                </div>
                                                <div className="ultp-toolbar-menu-block-label">{__('List Menu','ultimate-post')}</div>
                                            </div>
                                            <div className="ultp-toolbar-menu-block-item"
                                                onClick={()=> {
                                                    wp.data.dispatch( 'core/block-editor' ).insertBlock(wp.blocks.createBlock("ultimate-post/mega-menu", {}), 0, clientId);
                                                    onClose();
                                                }}
                                            >
                                                <div className="ultp-toolbar-menu-block-img ultp-megamenu-pro-icon">
                                                    <img src={ultp_data.url + 'assets/img/blocks/menu/mega_menu.svg'} alt="Mega Menu"/>
                                                    {
                                                        !ultp_data.active &&
                                                        <span className={`ultp-pro-block`}>Pro</span>
                                                    }
                                                </div>
                                                <div className="ultp-toolbar-menu-block-label">{__('Mega Menu','ultimate-post')}</div>
                                            </div>
                                        </div>
                                    )}
                                /> : false 
                            }
                            allowedBlocks={['ultimate-post/list-menu', 'ultimate-post/mega-menu' ]}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
