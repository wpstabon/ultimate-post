const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    const { 
        blockId,
        advanceId,
        menuMvInheritCss,
        menuResStructure,
        hasRootMenu,
        menuAlign,
        mvTransformToHam,
        backIcon,
        closeIcon,
        mvHamIcon,
        naviExpIcon,
        naviIcon,
        mvHeadtext,
        menuHamAppear,
        mvAnimationDuration,
        mvDrawerPosition,
        menuInline
    } = props.attributes;
    const extras = menuHamAppear && hasRootMenu != "hasRootMenu" ? {
        "data-mvtoham": mvTransformToHam,
        "data-rcsstype": menuMvInheritCss ? 'own' : 'custom',
        "data-rstr": menuResStructure,
        "data-mv": hasRootMenu != "hasRootMenu" ? "enable" : "disable"
    } 
    : 
    { 
        "data-hasrootmenu": hasRootMenu
    };

    const extrasLength = Object.keys(extras);

    const mvExtras = {
        "data-animationduration": mvAnimationDuration,
        "data-headtext": mvHeadtext,
    }
    return (
        <div 
            {...(advanceId && {id:advanceId})} 
            data-bid={blockId}
            data-menuinline={menuInline}
            className={`ultp-block-${blockId} ultpMenuCss`}
            { ...( extrasLength && extras )}
        >
            {
                menuHamAppear && hasRootMenu != "hasRootMenu" && <>
                    <div
                        { ...mvExtras}
                        className="ultp-mv-ham-icon ultp-active"
                    >
                        {'_ultp_mn_ic_' + (mvHamIcon || 'hamicon_3' ) + '_ultp_mn_ic_end_'}
                    </div>
                    <div 
                        data-drawerpos={mvDrawerPosition}
                        className="ultp-mobile-view-container ultp-mv-trigger"
                    >
                        <div className="ultp-mobile-view-wrapper">
                            <div className="ultp-mobile-view-head">
                                <div className="ultp-mv-back-label-con ultpmenu-dnone">
                                    {'_ultp_mn_ic_' + ( backIcon || 'leftAngle2' ) + '_ultp_mn_ic_end_'}
                                    <div className="ultp-mv-back-label">{mvHeadtext}</div>
                                </div>
                                <div className="ultp-mv-close">{'_ultp_mn_ic_' + (closeIcon || 'close_line') + '_ultp_mn_ic_end_'}</div>
                            </div>
                            <div className="ultp-mobile-view-body">

                            </div>
                        </div>
                        <div className="ultp-mv-icons">
                            <div className="ultp-mv-label-icon">{'_ultp_mn_ic_' + (naviIcon || 'rightAngle2') + '_ultp_mn_ic_end_'}</div>
                            <div className="ultp-mv-label-icon-expand">{'_ultp_mn_ic_' + (naviExpIcon || 'arrowUp2') + '_ultp_mn_ic_end_'}</div>
                        </div>
                    </div>
                </>
            }
            <div className={`ultp-menu-wrapper _${menuAlign}`}>
                <div className={`ultp-menu-content`}>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    )
}