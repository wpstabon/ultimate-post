import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { V4_1_0_CompCheck } from "../../helper/compatibility";
import { getDCBlockAttributes } from "../../helper/dynamic_content/attributes";

const attributes = {
  /*============================
      General Settings
  ============================*/
  blockId: { type: "string", default: "" },
  previewImg: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  layout: { type: "string", default: "layout1" },
  headingShow: { type: "boolean", default: true },
  excerptShow: { type: "boolean", default: false },
  showImage: { type: "boolean", default: true },
  filterShow: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [
          {
            key: "queryType",
            condition: "!=",
            value: ["posts", "customPosts"],
          },
        ],
      },
    ],
  },
  paginationShow: { type: "boolean", default: false },
  contentTag: { type: "string", default: "div" },
  openInTab: { type: "boolean", default: false },
  notFoundMessage: { type: "string", default: "No Post Found" },
  /*============================
      Query Settings
  ============================*/
  metaKey: {
    type: "string",
    default: "custom_meta_key",
    style: [
      {
        depends: [
          { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
        ],
        0: {
          key: "queryType", condition: "!=", value: ["customPosts", "posts", "archiveBuilder"],
        },
      },
    ],
  },
  queryNumber: {
    type: "string",
    default: 5,
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["layout1", "layout2"] },
          {
            key: "queryType",
            condition: "!=",
            value: ["customPosts", "posts", "archiveBuilder"],
          },
        ],
      },
    ],
  },

  /*============================
      Title Settings
  ============================*/
  titleShow: { type: "boolean", default: true },
  titleStyle: { type: "string", default: "none" },
  titleAnimColor: {
    type: "string",
    default: "black",
    style: [
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style1" }],
        selector:
          `{{ULTP}} .ultp-title-style1 a {  cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.35s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: left 100%; }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style2" }],
        selector:
          `{{ULTP}} .ultp-title-style2 a:hover {  border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style3" }],
        selector:
          "{{ULTP}} .ultp-title-style3 a {  text-decoration: none; $thetransition: all 1s cubic-bezier(1,.25,0,.75) 0s; position: relative; transition: all 0.35s ease-out; padding-bottom: 3px; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style4" }],
        selector:
          "{{ULTP}} .ultp-title-style4 a { cursor: pointer; font-weight: 600; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a:hover{ text-decoration: none; transition: all 0.35s ease-out; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-size:auto; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          "{{ULTP}} .ultp-title-style5 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style6" }],
        selector:
          "{{ULTP}} .ultp-title-style6 a{ background-image: linear-gradient(120deg, {{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 2px; background-position: 0 88%; transition: background-size 0.15s ease-in; padding: 5px 5px; }",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style7" }],
        selector:
          "{{ULTP}} .ultp-title-style7 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: right 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style8" }],
        selector:
          "{{ULTP}} .ultp-title-style8 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: center 100%; } ",
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style9" }],
        selector:
          "{{ULTP}} .ultp-title-style9 a { background-image: linear-gradient(120deg,{{titleAnimColor}} 0%, {{titleAnimColor}} 100%); background-repeat: no-repeat; background-size: 100% 10px; background-position: 0 88%; transition: 0.3s ease-in; padding: 3px 5px; } ",
      },
    ],
  },
   // title
  titleTag: { type: "string", default: "h3" },
  titleAnimation: { type: "string", default: "" },
  titlePosition: { type: "boolean", default: true },
  titleColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-content .ultp-block-title a { color:{{titleColor}} !important; }",
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "rgba(255,255,255,0.70)",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-content .ultp-block-title a:hover { color:{{titleHoverColor}} !important; }",
      },
    ],
  },
  titleLgTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "30", unit: "px" },
      height: { lg: "36", unit: "px" },
      decoration: "none",
      family: "",
      weight: "700",
    },
    style: [
      {
        depends: [
          { key: "titleShow", condition: "==", value: true },
          { key: "layout", condition: "!=", value: 'layout5' }
        ],
        selector:
          `{{ULTP}} .ultp-block-item:first-child .ultp-block-title, 
          {{ULTP}} .ultp-block-item:first-child .ultp-block-title a`,
      },
      {
        depends: [
          { key: "titleShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: 'layout5' }
        ],
        selector:
          `{{ULTP}} .ultp-block-item:first-child .ultp-block-title, 
          {{ULTP}} .ultp-block-item:first-child .ultp-block-title a, 
          {{ULTP}} .ultp-block-item:nth-child(2) .ultp-block-title, 
          {{ULTP}} .ultp-block-item:nth-child(2) .ultp-block-title a`,
      }
    ]
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "20", unit: "px" },
      height: { lg: "26", unit: "px" },
      decoration: "none",
      family: "",
      weight: "600",
    },
    style: [
      {
        depends: [
          { key: "titleShow", condition: "==", value: true },
          { key: "layout", condition: "!=", value: 'layout5' }
        ],
        selector:
          `{{ULTP}} .ultp-block-item:not(:first-child) .ultp-block-title, 
          {{ULTP}} .ultp-block-item:not(:first-child) .ultp-block-title a`,
      },
      {
        depends: [
          { key: "titleShow", condition: "==", value: true },
          { key: "layout", condition: "==", value: 'layout5' }
        ],
        selector:
          `{{ULTP}} .ultp-block-item:not(:first-child):not(:nth-child(2)) .ultp-block-title, 
          {{ULTP}} .ultp-block-item:not(:first-child):not(:nth-child(2)) .ultp-block-title a`
      }
    ]
  },
  titlePadding: {
    type: "object",
    default: { lg: { top: 10, bottom: 5, unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title { padding:{{titlePadding}}; }",
      },
    ],
  },
  titleLength: { type: "string", default: 0 },
  titleBackground: {
    type: "string",
    default: "",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
        `{{ULTP}} .ultp-block-title a { padding: 2px 7px; -webkit-box-decoration-break: clone; box-decoration-break: clone; background-color:{{titleBackground}}; }`,
      },
    ],
  },
  /*============================
      Image Settings
  ============================*/
  // Post grid 3
  imgCrop: {
    type: "string",
    default: (ultp_data.disable_image_size == 'yes' ? "full" : "ultp_layout_landscape"),
    depends: [{ key: "showImage", condition: "==", value: true }],
  },
  imgCropSmall: { type: "string",     default: (ultp_data.disable_image_size == 'yes' ? "full" : "ultp_layout_square"),},
  imgAnimation: { type: "string", default: "roateLeft" },
  imgGrayScale: {
    type: "object",
    default: { lg: "0", ulg: "%", unit: "%" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-image img { filter: grayscale({{imgGrayScale}}); }",
      },
    ],
  },
  imgHoverGrayScale: {
    type: "object",
    default: { lg: "0", unit: "%" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image img { filter: grayscale({{imgHoverGrayScale}}); }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-content-wrap, 
          {{ULTP}} .ultp-block-image { border-radius:{{imgRadius}}; }`,
      },
    ],
  },
  imgHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-content-wrap:hover, 
          {{ULTP}} .ultp-block-content-wrap:hover .ultp-block-image { border-radius:{{imgHoverRadius}}; }`,
      },
    ],
  },
  imgShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-content-overlay",
      },
    ],
  },
  imgHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-item:hover .ultp-block-content-overlay",
      },
    ],
  },
  imgOverlay: { type: "boolean", default: true },
  imgOverlayType: {
    type: "string",
    default: "flat",
    style: [{ depends: [{ key: "imgOverlay", condition: "==", value: true }] }],
  },
  overlayColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#0e1523" },
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector: "{{ULTP}} .ultp-block-image-custom > a::before",
      },
    ],
  },
  imgOpacity: {
    type: "string",
    default: 0.69999999999999996,
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector:
          "{{ULTP}} .ultp-block-image-custom > a::before { opacity: {{imgOpacity}}; }",
      },
    ],
  },
  fallbackEnable: {
    type: "boolean",
    default: true,
    style: [{ depends: [{ key: "showImage", condition: "==", value: true }] }],
  },
  fallbackImg: {
    type: "object",
    default: "",
    style: [
      {
        depends: [
          { key: "showImage", condition: "==", value: true },
          { key: "fallbackEnable", condition: "==", value: true },
        ],
      },
    ],
  },
  imgSrcset: { type: "boolean", default: false },
  imgLazy: { type: "boolean", default: false },
  /*============================
    Meta Settings
  ============================*/
  showSmallMeta: { type: "boolean", default: true },
  metaListSmall: { type: "string", default: '["metaAuthor","metaDate"]' },
  showSmallCat: { type: "boolean", default: true },

  /*============================
      Category Settings
  ============================*/


  /*============================
    Excerpt Settings
  ============================*/
  showSeoMeta: { type: "boolean", default: false },
  showSmallExcerpt: { type: "boolean", default: false },
  showFullExcerpt: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [{ key: "showSeoMeta", condition: "==", value: false }]
      }
    ]
  },
  excerptLimit: {
    type: "string",
    default: 10,
    style: [
      { depends: [{ key: "showFullExcerpt", condition: "==", value: false }] },
    ],
  },
  excerptColor: {
    type: "string",
    default: "#fff",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-excerpt, {{ULTP}} .ultp-block-excerpt p { color:{{excerptColor}}; }",
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 20, unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt, {{ULTP}} .ultp-block-excerpt p",
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: 15, bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt{ padding: {{excerptPadding}}; }",
      },
    ],
  },
  
  showSmallBtn: { type: "boolean", default: false },
  /*============================
      Content Settings
  ============================*/
  contentAlign: {
    type: "string",
    default: "left",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta { justify-content: flex-start; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta { justify-content: center; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta { justify-content: flex-end; } 
          .rtl {{ULTP}} .ultp-block-meta { justify-content: start; }`,
      },
      {
        depends: [
          { key: "contentAlign", condition: "==", value: "right" },
          { key: "readMore", condition: "==", value: true },
        ],
        selector:
          `.rtl {{ULTP}} .ultp-block-readmore a {display:flex; flex-direction:row-reverse;justify-content: flex-end;}`,
      },
    ],
  },
  column: {
    type: "string",
    default: "2",
    style: [
      {
        depends: [
          { key: "layout", condition: "==", value: ["layout1", "layout2"] },
        ],
        selector:
          "{{ULTP}} .ultp-block-row { grid-template-columns: repeat({{column}}, 1fr); }",
      },
      {
        selector:
          "{{ULTP}} .ultp-block-row { grid-template-columns: repeat({{column}}, 1fr); }",
      },
    ],
  },
  columnGridGap: {
    type: "object",
    default: { lg: "20", unit: "px" },
    style: [
      { selector: "{{ULTP}} .ultp-block-row { grid-gap: {{columnGridGap}}; }" },
    ],
  },
  overlayHeight: {
    type: "object",
    default: { lg: "450", unit: "px" },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-item .ultp-block-image img, 
          {{ULTP}} .ultp-block-empty-image { width: 100%; object-fit: cover; height: 100%; } 
          {{ULTP}} .ultp-layout5 .ultp-block-item .ultp-block-content-overlay, 
          {{ULTP}} .ultp-layout3 .ultp-block-item .ultp-block-content-overlay, 
          {{ULTP}} .ultp-layout4 .ultp-block-item .ultp-block-content-overlay {height: calc({{overlayHeight}}/2);} 
          {{ULTP}} .ultp-layout1 .ultp-block-item .ultp-block-content-overlay, 
          {{ULTP}} .ultp-layout2 .ultp-block-item .ultp-block-content-overlay {height: calc({{overlayHeight}}/2.5);} 
          {{ULTP}} .ultp-layout1 .ultp-block-item:first-child .ultp-block-content-overlay, 
          {{ULTP}} .ultp-layout2 .ultp-block-item:first-child .ultp-block-content-overlay {height: calc({{overlayHeight}}/1.5);} 
          {{ULTP}} .ultp-block-row { min-height: {{overlayHeight}}; } 
          {{ULTP}} .ultp-block-row .ultp-block-item:first-child .ultp-block-image img, 
          {{ULTP}} .ultp-block-row .ultp-block-item:first-child .ultp-block-empty-image { width: 100%; object-fit: cover; height: 100%; } 
          @media (max-width: 768px) {
            {{ULTP}} .ultp-block-item .ultp-block-content-overlay .ultp-block-image img, 
            {{ULTP}} .ultp-block-content-overlay .ultp-block-empty-image { height: calc( {{overlayHeight}}/2 ); min-height: inherit; }
          }`,
      },
    ],
  },
  
  overlayContentPosition: {
    type: "string",
    default: "bottomPosition",
    style: [
      {
        depends: [
          {
            key: "overlayContentPosition",
            condition: "==",
            value: "topPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-topPosition { align-items:flex-start; }",
      },
      {
        depends: [
          {
            key: "overlayContentPosition",
            condition: "==",
            value: "middlePosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-middlePosition { align-items:center; }",
      },
      {
        depends: [
          {
            key: "overlayContentPosition",
            condition: "==",
            value: "bottomPosition",
          },
        ],
        selector:
          "{{ULTP}} .ultp-block-content-bottomPosition { align-items:flex-end; }",
      },
    ],
  },
  overlayBgColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "" },
    style: [{ selector: "{{ULTP}} .ultp-block-content-inner" }],
  },
  overlayWrapPadding: {
    type: "object",
    default: {
      lg: { top: "20", bottom: "20", left: "20", right: "20", unit: "px" },
    },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-inner { padding: {{overlayWrapPadding}}; }",
      },
    ],
  },
  /*============================
      Filter Settings
  ============================*/
  filterBelowTitle: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-navigation { position: relative; display: block; margin: auto 0 0 0; height: auto; }",
      },
    ],
  },
  filterAlign: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "filterBelowTitle", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-navigation { text-align:{{filterAlign}}; }",
      },
    ],
  },
  filterType: { type: "string", default: "category" },
  filterText: { type: "string", default: "all" },
  filterValue: {
    type: "string",
    default: "[]",
    style: [{ depends: [{ key: "filterType", condition: "!=", value: "" }] }],
  },
  fliterTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 22, unit: "px" },
      decoration: "none",
      family: "",
      weight: 500,
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a",
      },
    ],
  },
  filterColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a { color:{{filterColor}}; }
          {{ULTP}} .flexMenu-viewMore a:before { border-color:{{filterColor}}}`,
      },
    ],
  },
  filterHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a:hover, 
          {{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a.filter-active { color:{{filterHoverColor}} !important; } 
          {{ULTP}} .ultp-flex-menu .flexMenu-viewMore a:hover::before { border-color:{{filterHoverColor}}}`,
      },
    ],
  },
  filterBgColor: {
    type: "string",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a { background:{{filterBgColor}}; }",
      },
    ],
  },
  filterHoverBgColor: {
    type: "string",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
        `{{ULTP}} .ultp-filter-wrap ul li.filter-item > a:hover, 
          {{ULTP}} .ultp-filter-wrap ul li.filter-item > a.filter-active, 
          {{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore > a:hover { background:{{filterHoverBgColor}}; }`,
      },
    ],
  },
  filterBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a",
      },
    ],
  },
  filterHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a:hover",
      },
    ],
  },
  filterRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a { border-radius:{{filterRadius}}; }",
      },
    ],
  },
  fliterSpacing: {
    type: "object",
    default: { lg: { top: "", bottom: "", right: "", left: "20", unit: "px" } },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li { margin:{{fliterSpacing}}; }",
      },
    ],
  },
  fliterPadding: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-wrap ul li.filter-item > a, 
          {{ULTP}} .ultp-filter-wrap .flexMenu-viewMore > a { padding:{{fliterPadding}}; }`,
      },
    ],
  },
  filterDropdownColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a { color:{{filterDropdownColor}}; }",
      },
    ],
  },
  filterDropdownHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a:hover { color:{{filterDropdownHoverColor}}; }",
      },
    ],
  },
  filterDropdownBg: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { background:{{filterDropdownBg}}; }",
      },
    ],
  },
  filterDropdownRadius: {
    type: "object",
    default: { lg: "0" },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { border-radius:{{filterDropdownRadius}}; }",
      },
    ],
  },
  filterDropdownPadding: {
    type: "object",
    default: {
      lg: { top: "15", bottom: "15", left: "20", right: "20", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { padding:{{filterDropdownPadding}}; }",
      },
    ],
  },
  filterMobile: { type: "boolean", default: true },
  filterMobileText: {
    type: "string",
    default: "More",
    style: [
      { depends: [{ key: "filterMobile", condition: "==", value: true }] },
    ],
  },
  /*============================
      Pagination Settings
  ============================*/
  paginationType: { type: "string", default: "loadMore" },

  ...commonAttributes([
    'advFilter',
    'heading',
    'advanceAttr',
    'pagination',
    'video',
    'meta',
    'category',
    'readMore',
    'query' 
  ], ['metaKey', 'queryNumber'], [
    // query
    {
      key: 'queryNumPosts',
      default: { lg: 5 }
    },
    // pagination
    {
      key: 'pagiAlign',
      default: { lg: "left" }
    },
    {
      key: 'metaColor',
      default: '#F3F3F3'
    },
    {
      key: 'metaSeparatorColor',
      default: '#b3b3b3'
    },
    // Read More 
    {
      key: 'readMoreColor',
      default: '#fff'
    }
  ]),
    // Compatibility
    V4_1_0_CompCheck,
  /* ======== Advance Settings ========*/
  ...getDCBlockAttributes({defColor: "#fff"}),
  
  /*============================
      Video Settings
  ============================*/
  // vidIconEnable: { type: "boolean", default: true },
  // popupAutoPlay: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     { depends: [{ key: "enablePopup", condition: "==", value: true }] },
  //   ],
  // },
  // vidIconPosition: {
  //   type: "string",
  //   default: "topRight",
  //   style: [
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "bottomRight" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; right: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "center" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon {  margin: 0 auto; position: absolute; top: 50%; left: 50%; transform: translate(-50%,-60%); -o-transform: translate(-50%,-60%); -ms-transform: translate(-50%,-60%); -moz-transform: translate(-50%,-60%); -webkit-transform: translate(-50%,-50%); z-index: 998;}`,
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "bottomLeft" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; left: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "topRight" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { top: 20px; right: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "topLeft" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { top: 20px; left: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "rightMiddle" },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-video-icon { display: flex; justify-content: flex-end; align-items: center; height: 100%; width: 100%; top:0px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "leftMiddle" },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-video-icon { display: flex; justify-content: flex-start; align-items: center; height: 100%; width: 100%; top: 0px; }",
  //     },
  //   ],
  // },
  // popupIconColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon svg { fill: {{popupIconColor}}; } 
  //         {{ULTP}} .ultp-video-icon svg circle { stroke: {{popupIconColor}}; }`,
  //     },
  //   ],
  // },
  // popupHovColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon svg:hover { fill: {{popupHovColor}}; } 
  //         {{ULTP}} .ultp-video-icon svg:hover circle { stroke: {{popupHovColor}};}`,
  //     },
  //   ],
  // },
  // iconSize: {
  //   type: "object",
  //   default: { lg: "40", sm: "30", xs: "30", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-video-icon svg { height:{{iconSize}}; width: {{iconSize}};}",
  //     },
  //   ],
  // },
  // enablePopup: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "vidIconEnable", condition: "==", value: true }] },
  //   ],
  // },
  // popupWidth: {
  //   type: "object",
  //   default: { lg: "70" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "enablePopup", condition: "==", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}}  .ultp-video-modal__Wrapper {width:{{popupWidth}}% !important;}",
  //     },
  //   ],
  // },
  // enablePopupTitle: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         { key: "enablePopup", condition: "==", value: true },
  //       ],
  //     },
  //   ],
  // },
  // popupTitleColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "enablePopupTitle", condition: "==", value: true },
  //         { key: "enablePopup", condition: "==", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-video-modal__Wrapper a { color:{{popupTitleColor}} !important; }",
  //     },
  //   ],
  // },
  // closeIconSep: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     { depends: [{ key: "enablePopup", condition: "==", value: true }] },
  //   ],
  // },
  // closeIconColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-video-close { color:{{closeIconColor}}; }",
  //     },
  //   ],
  // },
  // closeHovColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-video-close:hover { color:{{closeHovColor}}; }",
  //     },
  //   ],
  // },
  // closeSize: {
  //   type: "object",
  //   default: { lg: "70", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-video-close { font-size:{{closeSize}}; }",
  //     },
  //   ],
  // },
  
  /*============================
      Pagination Settings
  ============================*/
    // loadMoreText: {
  //   type: "string",
  //   default: "Load More",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "loadMore" },
  //       ],
  //     },
  //   ],
  // },
  // paginationText: {
  //   type: "string",
  //   default: "Previous|Next",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // paginationNav: {
  //   type: "string",
  //   default: "textArrow",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // paginationAjax: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // navPosition: {
  //   type: "string",
  //   default: "topRight",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //     },
  //   ],
  // },
  // pagiTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 14, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiArrowSize: {
  //   type: "object",
  //   default: { lg: "14" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-next-prev-wrap ul li a svg { width:{{pagiArrowSize}}px; }",
  //     },
  //   ],
  // },
  // pagiColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action { color:{{pagiColor}}; } 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a svg { fill:{{pagiColor}}; } 
  //         {{ULTP}} .ultp-pagination svg, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action svg { fill:{{pagiColor}}; }`,
  //       },
  //   ],
  // },
  // pagiBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a,
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiShadow: {
  //   type: "object",
  //   default: {
  //     openShadow: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiRadius: {
  //   type: "object",
  //   default: {
  //     lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action { border-radius:{{pagiRadius}}; }`,
  //     },
  //   ],
  // },
  // pagiHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore-action:hover { color:{{pagiHoverColor}}; } 
  //         {{ULTP}} .ultp-pagination li a:hover svg, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover svg,
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action:hover svg { fill:{{pagiHoverColor}}; } 
  //         @media (min-width: 768px) { 
  //           {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover { color:{{pagiHoverColor}};}
  //         }`,
  //     },
  //   ],
  // },
  // pagiHoverbg: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)", replace: 1 },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:focus, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover{ {{pagiHoverbg}} }`,
  //     },
  //   ],
  // },
  // pagiHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover`,
  //     },
  //   ],
  // },
  // pagiHoverShadow: {
  //   type: "object",
  //   default: {
  //     openShadow: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //       `{{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover`,
  //     },
  //   ],
  // },
  // pagiHoverRadius: {
  //   type: "object",
  //   default: {
  //     lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover { border-radius:{{pagiHoverRadius}}; }`,
  //     },
  //   ],
  // },
  // pagiPadding: {
  //   type: "object",
  //   default: {
  //     lg: { top: "8", bottom: "8", left: "14", right: "14", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action { padding:{{pagiPadding}}; }`,
  //     },
  //   ],
  // },
  // navMargin: {
  //   type: "object",
  //   default: {
  //     lg: { top: "0", right: "0", bottom: "0", left: "0", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //       selector: "{{ULTP}} .ultp-next-prev-wrap ul { margin:{{navMargin}}; }",
  //     },
  //   ],
  // },
  // pagiMargin: {
  //   type: "object",
  //   default: {
  //     lg: { top: "30", right: "0", bottom: "0", left: "0", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "!=", value: "navigation" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination, 
  //         {{ULTP}} .ultp-loadmore { margin:{{pagiMargin}}; }`,
  //     },
  //   ],
  // },
  // pagiAlign: {
  //   type: "object",
  //   default: { lg: "left" },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-loadmore, 
  //         {{ULTP}} .ultp-next-prev-wrap ul, 
  //         {{ULTP}} .ultp-pagination, 
  //         {{ULTP}} .ultp-pagination-wrap { text-align:{{pagiAlign}}; }`,
  //     },
  //   ],
  // },
  /*============================
    Meta Settings
  ============================*/
  // metaShow: { type: "boolean", default: true },s
  
  // metaShow: { type: "boolean", default: true },
  // metaPosition: { type: "string", default: "top" },
  // metaStyle: { type: "string", default: "icon" },
  // authorLink: { type: "boolean", default: true },
  // metaSeparator: { type: "string", default: "dot" },
  // metaList: { type: "string", default: '["metaAuthor","metaDate","metaRead"]' },
  // metaMinText: { type: "string", default: "min read" },
  // metaAuthorPrefix: { type: "string", default: "By" },
  // metaDateFormat: { type: "string", default: "M j, Y" },
  // metaTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 13, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element, 
  //         {{ULTP}} .ultp-block-item span.ultp-block-meta-element a`,
  //     },
  //   ],
  // },
  // metaColor: {
  //   type: "string",
  //   default: "rgba(255,255,255,0.90)",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { color: {{metaColor}}; } 
  //         {{ULTP}}  span.ultp-block-meta-element svg { fill: {{metaColor}}; } 
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element a { color: {{metaColor}}; }`,
  //     },
  //   ],
  // },
  // metaHoverColor: {
  //   type: "string",
  //   default: "#037fff",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element:hover , 
  //         {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:hover a { color: {{metaHoverColor}}; } 
  //         {{ULTP}} span.ultp-block-meta-element:hover svg { fill: {{metaHoverColor}}; }`,
  //     },
  //   ],
  // },
  // metaSeparatorColor: {
  //   type: "string",
  //   default: "#b3b3b3",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: 
  //       `{{ULTP}} .ultp-block-meta-dot span:after { background:{{metaSeparatorColor}}; } 
  //       {{ULTP}} span.ultp-block-meta-element:after { color:{{metaSeparatorColor}}; }`,
  //     },
  //   ],
  // },
  // metaSpacing: {
  //   type: "object",
  //   default: { lg: "15", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} span.ultp-block-meta-element { margin-right:{{metaSpacing}}; } 
  //         {{ULTP}} span.ultp-block-meta-element { padding-left: {{metaSpacing}}; } 
  //         .rtl {{ULTP}} span.ultp-block-meta-element {margin-right:0; margin-left:{{metaSpacing}}; } 
  //         .rtl {{ULTP}} span.ultp-block-meta-element { padding-left:0; padding-right: {{metaSpacing}}; }`,
  //     },
  //     {
  //       depends: [
  //         { key: "metaShow", condition: "==", value: true },
  //         { key: "contentAlign", condition: "==", value: "right" },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:last-child{margin-right:0px;}",
  //     },
  //   ],
  // },
  // metaMargin: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { margin:{{metaMargin}}; }",
  //     },
  //   ],
  // },
  // metaPadding: {
  //   type: "object",
  //   default: { lg: { top: "5", bottom: "5", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { padding:{{metaPadding}}; }",
  //     },
  //   ],
  // },
  // metaBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: "0", bottom: "0", left: "0" },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta",
  //     },
  //   ],
  // },
  // metaBg: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [{ key: "metaShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-meta { background:{{metaBg}}; }",
  //     },
  //   ],
  // },
  /*============================
        ReadMore Settings
  ============================*/
  // readMore: { type: "boolean", default: false },
  // readMoreText: { type: "string", default: "" },
  // readMoreIcon: { type: "string", default: "rightArrowLg" },
  // readMoreTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "400",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreIconSize: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore svg { width:{{readMoreIconSize}}; }",
  //     },
  //   ],
  // },
  // readMoreColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { color:{{readMoreColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a svg { fill:{{readMoreColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "#000" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreRadius: {
  //   type: "object",
  //   default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a { border-radius:{{readMoreRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreHoverColor: {
  //   type: "string",
  //   default: "rgba(255,255,255,0.80)",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a:hover { color:{{readMoreHoverColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a:hover svg { fill:{{readMoreHoverColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "var(--postx_preset_Primary_color)" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverRadius: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreSacing: {
  //   type: "object",
  //   default: { lg: { top: 15, bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore { margin:{{readMoreSacing}}; }",
  //     },
  //   ],
  // },
  // readMorePadding: {
  //   type: "object",
  //   default: { lg: { top: "2", bottom: "2", left: "6", right: "6", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a { padding:{{readMorePadding}}; }",
  //     },
  //   ],
  // },
  
  /*============================
      Heading Settings
  ============================*/
  headingText: { type: "string", default: "Post Grid #3" },
  // headingURL: { type: "string", default: "" },
  // headingBtnText: {
  //   type: "string",
  //   default: "View More",
  //   style: [
  //     { depends: [{ key: "headingStyle", condition: "==", value: "style11" }] },
  //   ],
  // },
  // headingStyle: { type: "string", default: "style1" },
  // headingTag: { type: "string", default: "h2" },
  // headingAlign: {
  //   type: "string",
  //   default: "left",
  //   style: [
  //     {
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner, 
  //         {{ULTP}} .ultp-sub-heading-inner { text-align:{{headingAlign}}; }`,
  //     },
  //   ],
  // },
  // headingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "20", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "700",
  //   },
  //   style: [{ selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-inner" }],
  // },
  // headingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_1_color)",
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { color:{{headingColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before, 
  //         {{ULTP}} .ultp-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { border-color:{{headingBorderBottomColor}}; }",
  //     },
  //   ],
  // },
  // headingBorderBottomColor2: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_3_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
  //     },
  //   ],
  // },
  // headingBg: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_3_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-style5 .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; }
  //         {{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span, {{ULTP}} .ultp-heading-inner span:after { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } 
  //         {{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }`,
  //     },
  //   ],
  // },
  // headingBg2: {
  //   type: "string",
  //   default: "var(--postx_preset_Base_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg2}}; }",
  //     },
  //   ],
  // },
  // headingBtnTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "14", unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-btn",
  //     },
  //   ],
  // },
  // headingBtnColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn { color:{{headingBtnColor}}; } 
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn svg { fill:{{headingBtnColor}}; }`,
  //     },
  //   ],
  // },
  // headingBtnHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Secondary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover { color:{{headingBtnHoverColor}}; } 
  //         {{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }`,
  //     },
  //   ],
  // },
  // headingBorder: {
  //   type: "string",
  //   default: "3",
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-bottom-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner span:before, 
  //         {{ULTP}} .ultp-heading-inner span:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
  //       selector:
  //         `{{ULTP}} .ultp-heading-inner:before, 
  //         {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
  //     },
  //   ],
  // },
  // headingSpacing: {
  //   type: "object",
  //   default: { lg: 20, sm: 10, unit: "px" },
  //   style: [
  //     {
  //       selector:
  //         "{{ULTP}} .ultp-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}}; }",
  //     },
  //   ],
  // },
  // headingRadius: {
  //   type: "object",
  //   default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
  //     },
  //   ],
  // },
  // headingPadding: {
  //   type: "object",
  //   default: { lg: { unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //     {
  //       depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
  //       selector:
  //         "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
  //     },
  //   ],
  // },
  // subHeadingShow: { type: "boolean", default: false },
  // subHeadingText: {
  //   type: "string",
  //   default:
  //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.",
  //   style: [
  //     { depends: [{ key: "subHeadingShow", condition: "==", value: true }] },
  //   ],
  // },
  // subHeadingTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: "16", unit: "px" },
  //     spacing: { lg: "0", unit: "px" },
  //     height: { lg: "27", unit: "px" },
  //     decoration: "none",
  //     transform: "",
  //     family: "",
  //     weight: "500",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-sub-heading div",
  //     },
  //   ],
  // },
  // subHeadingColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Contrast_2_color)",
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading div { color:{{subHeadingColor}}; }",
  //     },
  //   ],
  // },
  // subHeadingSpacing: {
  //   type: "object",
  //   default: { lg: { top: "8", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading-inner { margin:{{subHeadingSpacing}}; }",
  //     },
  //   ],
  // },
  // enableWidth: {
  //   type: 'toggle',
  //   default: false,
  //   style: [
  //     {
  //       depends: [{ key: "subHeadingShow", condition: "==", value: true }],
  //     },
  //   ],
  // },
  // customWidth: {
  //   type: "object",
  //   default: { lg: { top: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [
  //         { key: "subHeadingShow", condition: "==", value: true },
  //         { key: "enableWidth", condition: "==", value: true }
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-sub-heading-inner { max-width:{{customWidth}}; }",
  //     },
  //   ],
  // },
  
  /*============================
      Category Settings
  ============================*/
  
  // maxTaxonomy: { type: "string", default: "30" },
  // taxonomy: { type: "string", default: "category" },
  // catStyle: { type: "string", default: "classic" },
  // catPosition: { type: "string", default: "aboveTitle" },
  // customCatColor: { type: "boolean", default: false },
  // seperatorLink: {
  //   type: "string",
  //   default: ultp_data.category_url,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // onlyCatColor: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // catLineWidth: {
  //   type: "object",
  //   default: { lg: "20" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before { width:{{catLineWidth}}px; }`,
  //     },
  //   ],
  // },
  // catLineSpacing: {
  //   type: "object",
  //   default: { lg: "30" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth .ultp-category-in { padding-left: {{catLineSpacing}}px; padding-right: {{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in { padding-left: {{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderRight .ultp-category-in { padding-right:{{catLineSpacing}}px; }`,
  //     },
  //   ],
  // },
  // catLineColor: {
  //   type: "string",
  //   default: "#000",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before { background:{{catLineColor}}; }`,
  //     },
  //   ],
  // },
  // catLineHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:after,
  //         {{ULTP}} .ultp-category-borderLeft:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderRight:hover .ultp-category-in:before { background:{{catLineHoverColor}}; }`,
  //     },
  //   ],
  // },
  // catTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 11, unit: "px" },
  //     height: { lg: 15, unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "500",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //   ],
  // },
  // catBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "#000" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catRadius: {
  //   type: "object",
  //   default: { lg: "2", unit: "px" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //   ],
  // },
  // catHoverColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //   ],
  // },
  // catBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catSacing: {
  //   type: "object",
  //   default: { lg: { top: 5, bottom: 5, left: 0, right: 0, unit: "px" } },
  //   style: [
  //     {
  //       depends: [
  //         // { key: "catShow", condition: "==", value: true }
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid { margin:{{catSacing}}; }",
  //     },
  //   ],
  // },
  // catPadding: {
  //   type: "object",
  //   default: { lg: { top: 3, bottom: 3, left: 7, right: 7, unit: "px" } },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //   ],
  // },
  /*============================
      Query Settings
  ============================*/
   // Post grid 3
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 5 }
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // queryOrder: { type: "string", default: "desc" },
  // queryInclude: { type: "string", default: "" },
  // queryExclude: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },
};
export default attributes;
