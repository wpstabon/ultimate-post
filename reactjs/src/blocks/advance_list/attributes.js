const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    previewImg: {
        type: 'string',
        default: '',
    },
    currentPostId: { type: "string" , default: "" },

    /*==========================
        Global Style
    ==========================*/
    layout: {
        type: 'string',
        default: 'layout1',
        style: [
            {
                depends: [ 
                    // { key:'listGroupBelowIcon', condition:'==', value: false }, 
                    { key:'layout', condition:'==', value: 'layout1' } 
                ], 
            },
            {
                depends: [  
                    { key:'layout', condition:'==', value: 'layout2' },
                    // { key:'listGroupBelowIcon', condition:'==', value: false }, 
                ], 
            },
            {
                depends: [ 
                    { key:'listGroupSubtextEnable', condition:'==', value: false },
                    { key:'layout', condition:'==', value: 'layout3' },
                    // { key:'listGroupBelowIcon', condition:'==', value: false }, 
                ], 
                selector: '{{ULTP}} li .ultp-list-content { display: block !important; }'
            },
            {
                depends: [ 
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'layout', condition:'==', value: 'layout3' },
                    // { key:'listGroupBelowIcon', condition:'==', value: false }, 
                ], 
            },
            // below icon false
            // {
            //     depends: [ 
            //         { key:'layout', condition:'==', value: 'layout1' },
            //         { key:'listGroupBelowIcon', condition:'==', value: true }, 
            //     ], 
            //     selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            // },
            // {
            //     depends: [  
            //         { key:'layout', condition:'==', value: 'layout2' },
            //         { key:'listGroupBelowIcon', condition:'==', value: true },
            //     ], 
            //     selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            // },
            // {
            //     depends: [ 
            //         { key:'listGroupSubtextEnable', condition:'==', value: false },
            //         { key:'layout', condition:'==', value: 'layout3' },
            //         { key:'listGroupBelowIcon', condition:'==', value: true },
            //     ], 
            //     selector: '{{ULTP}}  li  .ultp-list-content { display: block !important; }'
            // },
            // {
            //     depends: [ 
            //         { key:'listGroupSubtextEnable', condition:'==', value: true },
            //         { key:'layout', condition:'==', value: 'layout3' },
            //         { key:'listGroupBelowIcon', condition:'==', value: true },
            //     ], 
            //     selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            // },
        ]
    },
    listLayout:{
        type: 'string',
        default: 'number',
        style: [
            {
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}}  ul,
                {{ULTP}}  ul li { list-style-type: none; }`
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                    { key:'listLayout', condition:'==', value: 'roman' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: roman-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: roman-counter; }
                {{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon:before { content: counter(roman-counter, upper-roman) "."; display: flex; align-items: center; justify-content: center; transition: .3s; box-sizing: border-box;}`
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                    { key:'listLayout', condition:'==', value: 'number' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: number-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: number-counter; }
                {{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon:before { content: counter(number-counter) "."; display: flex; align-items: center; justify-content: center; transition: .3s; box-sizing: border-box; }`
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'abc' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: alpha-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: alpha-counter; }
                {{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon:before { content: counter(alpha-counter, lower-alpha) "."; display: flex; align-items: center; justify-content: center; transition: .3s; box-sizing: border-box;}`
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: alpha-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative;  }
                {{ULTP}} .ultp-list-texticon { line-height: 0px; }
                {{ULTP}} .ultp-list-texticon:before {   content: ""; display: inline-block; width: 10px; height: 10px; border-radius: 50%; background-color: black; transition: .3s; box-sizing: border-box; }`
            },
            // Below Subtext
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listLayout', condition:'==', value: 'roman' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: roman-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: roman-counter; }
                {{ULTP}} .ultp-list-texticon:before { content: counter(roman-counter, upper-roman) "."; display: flex; align-items: center; justify-content: center; transition: .3s; box-sizing: border-box;}`
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listLayout', condition:'==', value: 'number' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: number-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: number-counter; }
                {{ULTP}} .ultp-list-texticon:before { content: counter(number-counter) "."; display: flex; align-items: center;  justify-content: center; transition: .3s; box-sizing: border-box;}`
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listLayout', condition:'==', value: 'abc' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: alpha-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center;  position: relative; counter-increment: alpha-counter; }
                {{ULTP}} .ultp-list-texticon:before { content: counter(alpha-counter, lower-alpha) "."; display: flex; align-items: center; justify-content: center; transition: .3s; box-sizing: border-box; }`
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper { counter-reset: alpha-counter; }
                {{ULTP}} .wp-block-ultimate-post-list { display: flex; align-items: center; position: relative; counter-increment: alpha-counter; }
                {{ULTP}} .ultp-list-texticon { line-height: 0px; }
                {{ULTP}} .ultp-list-texticon:before {   content: ""; display: inline-block; width: 10px; height: 10px; border-radius: 50%; background-color: black; transition: .3s; box-sizing: border-box; }`
            },
        ]
    },
    listInline:{
        type: 'boolean',
        default: false,
        style: [
            {
            depends: [
                { key:'listInline', condition:'==', value: true },
            ], 
            selector:
            `{{ULTP}} .ultp-list-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list) { display: flex;}
            {{ULTP}} .ultp-list-wrapper > li:last-child { padding-right: 0px; margin-right: 0px; }
            {{ULTP}} .block-editor-block-list__layout > div,
            {{ULTP}} .wp-block-ultimate-post-list { width: auto !important; }`
        },
        {
            depends: [
                { key:'listInline', condition:'==', value: false },
            ], 
            selector:
            `{{ULTP}} .ultp-list-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list) { display: block;} 
            {{ULTP}} .block-editor-block-list__layout > div,
            {{ULTP}} .wp-block-ultimate-post-list { width: 100%; }`
        },
        {
            depends: [
                { key:'layout', condition:'==', value: 'layout2' },
                { key:'enableSeparator', condition:'==', value: true },
                { key:'listInline', condition:'==', value: true },
            ], 
            selector:
            `{{ULTP}} .ultp-list-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list) { display: flex;}
            {{ULTP}} .block-editor-block-list__layout>div:last-child li{ padding-right: 0px; margin-right: 0px;} 
            {{ULTP}} .block-editor-block-list__layout > div,
            {{ULTP}} .wp-block-ultimate-post-list { width: auto !important; }`
        },
        {
            depends: [
                { key:'layout', condition:'==', value: 'layout2' },
                { key:'enableSeparator', condition:'==', value: true },
                { key:'listInline', condition:'==', value: false },
            ], 
            selector:
            `{{ULTP}} .ultp-list-wrapper .block-editor-inner-blocks .block-editor-block-list__layout,
            {{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list) { display: block;} 
            {{ULTP}} .block-editor-block-list__layout > div,
            {{ULTP}} .wp-block-ultimate-post-list{ width: 100%; }`
        }
    ]
    },
    listAlignment:{
        type: 'string',
        default: 'left',
        style: [
            {
                depends: [
                    { key:'listAlignment', condition:'==', value: 'left' },
                    { key:'listInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-right:  auto; display: flex; flex-direction: column; align-items: flex-start;}`
            },
            {
                depends: [
                    { key:'listAlignment', condition:'==', value: 'right' },
                    { key:'listInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-left: auto; display: flex; flex-direction: column; align-items: flex-end;}`
            },
            {
                depends: [
                    { key:'listInline', condition:'==', value: false },
                    { key:'listAlignment', condition:'==', value: 'center' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin: auto auto; display: flex; flex-direction: column; align-items: center;}`
            },
            {
                depends: [
                    { key:'listInline', condition:'==', value: true },
                    { key:'listAlignment', condition:'==', value: 'center' },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin: auto auto; display: flex; flex-wrap: wrap; justify-content: center;}
                {{ULTP}} .ultp-list-wrapper .ultp-list-content { justify-content: center; }`
            },
            {
                depends: [
                    { key:'listAlignment', condition:'==', value: 'left' },
                    { key:'listInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-right:  auto; display: flex; justify-content: flex-start; flex-wrap: wrap;}`
            },
            {
                depends: [
                    { key:'listAlignment', condition:'==', value: 'right' },
                    { key:'listInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list ),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-left:  auto; display: flex; justify-content: flex-end; flex-wrap: wrap;}`
            },
        ]
    },
    listSpaceBetween:{
        type: 'object',
        default: {lg:'17', unit: 'px'},
        style: [
            {
                depends: [
                    { key:'enableSeparator', condition:'==', value: true },
                    { key:'listInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .wp-block-ultimate-post-list { margin-right:calc({{listSpaceBetween}}/ 2); padding-right:calc({{listSpaceBetween}}/ 2);}
                {{ULTP}} .wp-block-ultimate-post-list{ margin-top: 0px !important; margin-bottom: 0px !important;}` 
            },
            {
                depends: [
                    
                    { key:'listInline', condition:'==', value: true },
                    { key:'enableSeparator', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-list-wrapper:has( > .wp-block-ultimate-post-list),
                {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { gap:{{listSpaceBetween}}; }
                {{ULTP}} .wp-block-ultimate-post-list{ margin: 0px !important; }`
            },
            
            {
                depends: [
                    { key:'listInline', condition:'==', value: false },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list { margin-bottom: calc({{listSpaceBetween}}/ 2); padding-bottom:calc({{listSpaceBetween}}/ 2);}'
            }
        ]
    },
    listSpaceIconText:{
        type: 'object',
        default: {lg:'12', ulg:'px'},
        style: [
            {
                depends: [
                    { key:'layout', condition:'!=', value: 'layout3' },
                    { key:'listGroupBelowIcon', condition:'==', value:  false },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { column-gap:{{listSpaceIconText}}; }'
            },
            {
                depends: [
                    { key:'layout', condition:'!=', value: 'layout3' },
                    { key:'listGroupBelowIcon', condition:'==', value:  true },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-heading { column-gap:{{listSpaceIconText}}; }'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout3' },
                ],
                selector: 
                `{{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg,
                {{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon { margin-bottom:{{listSpaceIconText}}; }`
            },
        ]
    },
    
    /*==========================
        Icon/Image
    ==========================*/
    enableIcon:{
        type: 'boolean',
        default: true,
    },
    listPosition: {
        type: 'string',
        default: 'center',
        style: [
            { 
                depends: [
                    { key:'enableIcon', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { align-items:{{listPosition}}; }'
            }
        ],
    },
    listGroupIconType:{
        type: 'string',
        default: 'icon',
        style: [
            { 
                depends: [
                    { key:'enableIcon', condition:'==', value: true },
                ], 
            }
        ],
    },
    listCustomIcon: {
        type: 'string',
        default: 'right_circle_line',
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: "icon" },
                ], 
            }
        ],
    },
    listGroupCustomImg: {
        type: 'object',
        default: '',
        style: [
            { 
                depends: [
                    { key:'enableIcon', condition:'==', value: true },
                    { key:'listGroupIconType', condition:'==', value: "image" },
                ], 
            }
        ],
    },
    listImgRadius: {
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [
            {
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'image' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list img { border-radius: {{listImgRadius}}; }'
            }
        ]
    },
    listGroupIconSize: {
        type: 'string',
        default: '16',
        style: [{
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector:
                `{{ULTP}} .wp-block-ultimate-post-list svg,
                {{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg img { height:{{listGroupIconSize}}px; width:{{listGroupIconSize}}px; }`
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                ],
                selector: '{{ULTP}} .ultp-list-texticon:before { height:{{listGroupIconSize}}px; width:{{listGroupIconSize}}px; }'
            },
        ]
    },
    listIconTypo: {
        type: 'object',
        default: {openTypography: 0,size: {lg:16, unit:'px'},height: {lg:20, unit:'px'}, decoration: 'none',family: '', weight:700},
        style: [
            {   
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .ultp-list-texticon:before '
            }
        ]
    },
    listGroupBgSize: {
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .ultp-list-texticon:before { height:{{listGroupBgSize}}px; width:{{listGroupBgSize}}px; }'
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon { height:{{listGroupBgSize}}px; width:{{listGroupBgSize}}px; }'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg { height:{{listGroupBgSize}}px; width:{{listGroupBgSize}}px; min-width:{{listGroupBgSize}}px; }'
            },
        ]
    },
    // color and others
    listGroupIconColor:{
        type: 'string',
        default: 'var(--postx_preset_Contrast_1_color)',
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                ],
                selector: '{{ULTP}} .ultp-list-texticon:before { color:{{listGroupIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                    { key:'listLayout', condition:'==', value: 'bullet' },
                ],
                selector: '{{ULTP}} .ultp-list-texticon:before { background-color:{{listGroupIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                    { key:'listGroupIconType', condition:'!=', value: "image" },
                ],
                selector: '{{ULTP}} .ultp-listicon-bg svg { fill:{{listGroupIconColor}};}'
            }
        ],
    },
    listGroupIconbg:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .ultp-list-texticon:before { background: {{listGroupIconbg}};}'
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon { background: {{listGroupIconbg}};}'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg { background: {{listGroupIconbg}};}'
            }
        ],
    },
    listGroupIconBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                
                selector: '{{ULTP}} .ultp-list-texticon:before'
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg'
            },
        ]
    },
    listGroupIconRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .ultp-list-texticon:before { border-radius: {{listGroupIconRadius}}; }'
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-texticon { border-radius: {{listGroupIconRadius}}; }'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-listicon-bg { border-radius: {{listGroupIconRadius}}; }'
            }
        ]
    },

    // Hover
    listGroupHoverIconColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-list-texticon:before  { color:{{listGroupHoverIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-list-texticon:before  { background-color:{{listGroupHoverIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                    { key:'listGroupIconType', condition:'!=', value: "image" },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-listicon-bg svg { fill:{{listGroupHoverIconColor}};}'
            }
        ],
    },
    listGroupHoverIconbg:{
        type: 'string',
        default: '',
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `{{ULTP}} .block-editor-block-list__block:hover .wp-block-ultimate-post-list .ultp-list-texticon:before ,
                {{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-texticon:before  { background: {{listGroupHoverIconbg}}; }`
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-list-texticon { background: {{listGroupHoverIconbg}}; }'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-listicon-bg { background: {{listGroupHoverIconbg}}; }'
            }
        ],
    },
    listGroupHoverIconBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `{{ULTP}} .block-editor-block-list__block:hover .wp-block-ultimate-post-list .ultp-list-texticon:before, 
                {{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-texticon:before `
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-list-texticon'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-listicon-bg'
            },
        ]
    },
    listGroupHoverIconRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `{{ULTP}} .block-editor-block-list__block:hover .wp-block-ultimate-post-list .ultp-list-texticon:before , 
                {{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-texticon:before  { border-radius: {{listGroupHoverIconRadius}}; }`
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-list-texticon { border-radius: {{listGroupHoverIconRadius}}; }'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .wp-block-ultimate-post-list:hover .ultp-listicon-bg { border-radius: {{listGroupHoverIconRadius}}; }'
            }
        ]
    },

    /*==========================
        Content
    ==========================*/
    listDisableText:{
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'listDisableText', condition:'==', value: false },
                ], 
            },
            {
                depends: [
                    { key:'listDisableText', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} .ultp-list-content { display: block !important; }'
            },
        ]
    },
    listTextTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:16, unit:'px'},height: {lg:'24', unit:'px'}, decoration: 'none',family: '', weight: 400},
        style: [{
            selector:
            `{{ULTP}}  .ultp-list-title,
            {{ULTP}}  .ultp-list-title a`
        }]
    },
    listGroupTitleColor:{
        type: 'string',
        default: 'var(--postx_preset_Contrast_1_color)',
        style: [{
            selector:
            `{{ULTP}} .ultp-list-title,
            {{ULTP}} .ultp-list-title a { color: {{listGroupTitleColor}}; }`
        }]
    },
    listGroupTitleHoverColor:{
        type: 'string',
        default: 'var(--postx_preset_Primary_color)',
        style: [{
            selector:
            `{{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-title,
            {{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-title a,
            {{ULTP}} .block-editor-block-list__block:hover > .wp-block-ultimate-post-list .ultp-list-title,
            {{ULTP}} .block-editor-block-list__block:hover > .wp-block-ultimate-post-list .ultp-list-title a { color: {{listGroupTitleHoverColor}}; }`}]
    },
    listGroupSubtextEnable:{
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'layout', condition:'==', value: "layout1" },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: grid; grid-template-areas: "a b" "a c"; align-items: center; }'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: "layout2" },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: grid; grid-template-areas: "b a" "c a"; align-items: center; justify-content: flex-end; }'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: "layout3" },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            }
        ]
    },
    listGroupSubtextTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:14, unit:'px'},height: {lg:'20', unit:'px'}, decoration: 'none',family: '', weight: 400},
        style: [
            {
                depends: [
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} .ultp-list-subtext'}
            ]
    },
    listGroupSubtextSpace:{
        type: 'object',
        default: {lg:'5', ulg:'px'},
        style: [
            {
                depends: [
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'layout', condition:'==', value: "layout3" },
                ], 
                selector: '{{ULTP}} .ultp-list-subtext { margin-top:{{listGroupSubtextSpace}}; }'
            },
            {
                depends: [
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'layout', condition:'!=', value: "layout3" },
                ], 
                selector: '{{ULTP}} .ultp-list-subtext { margin-top:{{listGroupSubtextSpace}}; }'
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                    { key:'layout', condition:'!=', value: "layout3" },
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-list-content { row-gap:{{listGroupSubtextSpace}}; }
                {{ULTP}} .ultp-list-title,
                {{ULTP}} .ultp-list-content a { align-self: self-end; }
                {{ULTP}} .ultp-list-subtext { align-self: self-start; }`
            }
        ]
    },
    listGroupSubtextColor:{
        type: 'string',
        default: 'var(--postx_preset_Contrast_2_color)',
        style: [{
            depends: [
                { key:'listGroupSubtextEnable', condition:'==', value: true },
            ], 
            selector: '{{ULTP}} .ultp-list-subtext { color:{{listGroupSubtextColor}}; }'}
        ]
    },
    listGroupSubtextHoverColor:{
        type: 'string',
        default: '',
        style: [{
            depends: [
                { key:'listGroupSubtextEnable', condition:'==', value: true },
            ], 
            selector:
            `{{ULTP}} .ultp-list-wrapper  > .wp-block-ultimate-post-list:hover .ultp-list-subtext, 
            {{ULTP}} .block-editor-block-list__block:hover > .wp-block-ultimate-post-list .ultp-list-subtext  { color:{{listGroupSubtextHoverColor}}; }`}
        ]
    },
    listGroupBelowIcon:{
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'layout', condition:'==', value: "layout3" },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            },
            {
                depends: [
                    { key:'listGroupBelowIcon', condition:'==', value: true },
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'layout', condition:'!=', value: "layout3" },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { display: block !important; }'
            },
            {
                depends: [
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                    { key:'listGroupBelowIcon', condition:'==', value: false },
                    { key:'layout', condition:'!=', value: "layout3" },
                ], 
            },
        ]
    },

    /*==========================
        Content Wrap
    ==========================*/
    listGroupBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: 'var(--postx_preset_Base_2_color)', size: 'cover', repeat: 'no-repeat'},
        style:[{ 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content'
            }
        ],
    },
    listGroupborder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [{ 
            depends: [
                { key:'listGroupIconType', condition:'==', value: 'default' },
            ], 
            selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content'
        },
        { 
            depends: [
                { key:'listGroupIconType', condition:'!=', value: 'default' },
            ], 
            selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content'
        }]
    },
    listGroupRadius:{
        type: 'object',
        default: {lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit:'px'},
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { border-radius: {{listGroupRadius}};}'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { border-radius: {{listGroupRadius}};}'
            }]
    },
    listGroupPadding:{
        type: 'object',
        default: {lg:{top: '2', bottom: '2', left: '6', right: '6', unit:'px'}},
        style:[
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list  .ultp-list-content { padding: {{listGroupPadding}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content { padding: {{listGroupPadding}}; }'
            }
        ],
    },
    // Hover
    listGroupHoverBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover'
            }],
    },
    listGroupHovborder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover'
            }]
    },
    listGroupHovRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover { border-radius: {{listGroupHovRadius}};}'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ], 
                selector: '{{ULTP}} .wp-block-ultimate-post-list .ultp-list-content:hover { border-radius: {{listGroupHovRadius}};}'
            }]
    },
    
    /*==========================
        List Separator
    ==========================*/
    enableSeparator:{
        type: 'boolean',
        default: false,
    },
    listGroupSepColor:{
        type: 'string',
        default: 'var(--postx_preset_Base_3_color)',
        style: [
            {
                depends: [
                    { key:'enableSeparator', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} ul li { border-color: {{listGroupSepColor}}; }'
            }
        ]
    },
    listGroupSepSize:{
        type: 'string',
        default: '',
        style: [{
            depends: [
                { key:'listInline', condition:'==', value: true },
                { key:'enableSeparator', condition:'==', value: true },
            ], 
            selector: '{{ULTP}} ul  li { border-right-width: {{listGroupSepSize}}px; }'
        },
        {
            depends: [
                { key:'enableSeparator', condition:'==', value: true },
                { key:'listInline', condition:'==', value: false },
            ], 
            selector: '{{ULTP}} ul  li { border-bottom-width: {{listGroupSepSize}}px; }'
        }
    ]
    },
    listGroupSepStyle:{
        type: 'string',
        default: 'solid',
        style: [
            {
                depends: [
                    { key:'listInline', condition:'==', value: true },
                    { key:'enableSeparator', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} ul li { border-right-style: {{listGroupSepStyle}}; }'
            },
            {
                depends: [
                    { key:'listInline', condition:'==', value: false },
                    { key:'enableSeparator', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} ul li { border-bottom-style: {{listGroupSepStyle}}; }'
            }
        ]
    },
    
    /*==========================
        General Advance Settings
    ==========================*/
    wrapBg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5'},
        style: [{
            selector:
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout`
        }]
    },
    wrapBorder:{
        type: 'object',
        default: {openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout` 
        }],
    },
    wrapShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout` 
        }],
    },
    wrapRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style:[{ 
            selector:
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { border-radius:{{wrapRadius}}; }` 
        }],
    },
    wrapHoverBackground:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#037fff'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list):hover,
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover`
        }]
    },
    wrapHoverBorder:{
        type: 'object',
        default: {openBorder:0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list):hover,
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover` 
        }],
    },
    wrapHoverRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list):hover,
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover { border-radius:{{wrapHoverRadius}}; }` 
        }],
    },
    wrapHoverShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list):hover,
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover` 
        }],
    },
    wrapMargin:{
        type: 'object',
        default: {lg:{top: '',bottom: '', unit:'px'}},
        style:[{ selector:
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin:{{wrapMargin}}; }` 
        }],
    },
    wrapOuterPadding:{
        type: 'object',
        default: {lg:{top: '',bottom: '',left: '', right: '', unit:'px'}},
        style:[{
            selector: 
            `{{ULTP}} .ultp-list-wrapper:has(> .wp-block-ultimate-post-list),
            {{ULTP}} .ultp-list-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { padding:{{wrapOuterPadding}}; }`
        }],
    },
    advanceId:{
        type: 'string',
        default: '',
    },
    advanceZindex:{
        type: 'string',
        default: '',
        style:[{ selector: '{{ULTP}} .ultp-list-wrapper { z-index:{{advanceZindex}}; }' }],
    },
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{
            selector: ''}],
    }
}
export default attributes;
