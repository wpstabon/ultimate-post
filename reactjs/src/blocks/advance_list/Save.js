const { Component } = wp.element;
const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
	const { blockId, advanceId, layout } = props.attributes;
	return (
		<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId}`}>
			<ul className={`ultp-list-wrapper ultp-list-${layout}`}>
				<InnerBlocks.Content />
			</ul>
		</div>
	)
}