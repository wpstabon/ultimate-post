const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useState, useEffect, useRef, Fragment } = wp.element
import { updateChildAttr, isInlineCSS, updateCurrentPostId } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import ListsGroupSettings from './Settings'
import ToolBarElement from '../../helper/ToolBarElement'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
	const { setAttributes, name, className, attributes, clientId, attributes: { previewImg, blockId, advanceId, layout, listGroupIconType, listCustomIcon, listGroupCustomImg, listDisableText, listGroupSubtextEnable, listGroupBelowIcon, enableIcon, listLayout, currentPostId } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	function needsUpdate(prevProps) {
		return (
			prevProps.listGroupIconType != listGroupIconType ||
			prevProps.listCustomIcon != listCustomIcon ||
			prevProps.listGroupBelowIcon != listGroupBelowIcon ||
			prevProps.listGroupCustomImg != listGroupCustomImg ||
			prevProps.listDisableText != listDisableText ||
			prevProps.listGroupSubtextEnable != listGroupSubtextEnable ||
			prevProps.enableIcon != enableIcon ||
			prevProps.listLayout != listLayout ||
			prevProps.layout != layout
		)
	}

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (!prevAttributes) {
			prevPropsRef.current = attributes;
		} else if (needsUpdate(prevAttributes)) {
			updateChildAttr(clientId);
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/advanced-list', blockId, isInlineCSS());
	}

	const listTemplate = [
		["ultimate-post/list", { listText: 'List Item' }],
		["ultimate-post/list", { listText: 'List Item' }],
		["ultimate-post/list", { listText: 'List Item' }],
	]

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<InspectorControls>
				<ListsGroupSettings store={store} />
			</InspectorControls>
			<ToolBarElement
				include={[
					{
						type: 'template'
					},
					{
						type: 'layout', block: 'advance-list', key: 'layout', label: __('Layout', 'ultimate-post'),
						options: [
							{ img: 'assets/img/layouts/list/list1.svg', label: 'Layout 1', value: 'layout1' },
							{ img: 'assets/img/layouts/list/list2.svg', label: 'Layout 2', value: 'layout2' },
							{ img: 'assets/img/layouts/list/list3.svg', label: 'Layout 3', value: 'layout3' },
						],
					}
				]}
				store={store} />
			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<ul className={`ultp-list-wrapper ultp-list-${layout}`}>
					<InnerBlocks
						template={listTemplate}
						allowedBlocks={['ultimate-post/list']}
					/>
				</ul>
			</div>
		</Fragment>
	)
}
