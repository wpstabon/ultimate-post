const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import { dynamicRichTextAttributes } from '../../../helper/dynamic_content';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType(
    'ultimate-post/list', {
        title: __('List','ultimate-post'),
        parent:  ["ultimate-post/advanced-list"],
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/single-list.svg'} alt="List"/>,
        category: 'ultimate-post',
        description: __('Create & customize a list item.','ultimate-post'),
        keywords: [ 
            __('list','ultimate-post'),
            __('advanced List','ultimate-post'),
            __('icon list','ultimate-post')
        ],
        attributes,
        supports: {
            reusable: false,
            html: false,
        },
        edit: Edit,
        save: Save,
    }
)