const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../../helper/CommonPanel';
import { Section, Sections } from '../../../helper/Sections';

const ListsSettings = ({ store }) => {
    return (
        <Sections>
            <Section slug="button" title={__('Button','ultimate-post')}>
            <CommonSettings title={`inline`} include={[
                    {
                        position: 1, data: { type:'tag',key:'listIconType', label:__('Icon Type','ultimate-post'), options:[
                            { value:'icon',label:__('Icon','ultimate-post') },
                            { value:'image',label:__('Image','ultimate-post') },
                            { value:'inherit',label:__('Inherit','ultimate-post') },
                            { value:'none',label:__('None','ultimate-post') }] } 
                    },
                    {
                        position: 2, data: { type:'icon', key:'listSinleCustomIcon',label:__('Icon Store','ultimate-post')} 
                    },
                    {
                        position: 3, data: { type:'media',key:'listCustomImg', label:__('Upload Custom Image','ultimate-post')}
                    },
                    {
                        position: 4, data: { type:'separator', label:__('Icon Style','ultimate-post')}
                    },                    
                    {
                        position: 5, data: { type: 'tab', content: [
                            {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                {  
                                    type: 'color', key: 'listIconColor', label: __('Icon Color', 'ultimate-post')
                                },
                                { 
                                    type:'color', key:'listIconBg', label:__('Icon  Background','ultimate-post') 
                                },
                                { 
                                    type:'border', key:'listIconBorder', label:__('Border','ultimate-post') 
                                },
                            ]
                        },
                        {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                            {  
                                type: 'color', key: 'listIconHoverColor', label: __('Icon Hover Color', 'ultimate-post')
                            },
                            { 
                                type:'color', key:'listIconHoverBg', label:__('Icon Hover Background','ultimate-post') 
                            },
                            { 
                                type:'border', key:'listIconHoverBorder', label:__('Hover Border','ultimate-post') 
                            },
                        ]}
                    ]}}
                ]} initialOpen={true}  store={store}/>
                <CommonSettings title={__('Text','ultimate-post')} include={[
                    {
                        position: 1, data: { type:'color', key:'listTextColor', label:__('Title Color','ultimate-post')}
                    },
                    {
                        position: 2, data: { type:'color', key:'listTextHoverColor', label:__('Title Hover Color','ultimate-post')}
                    },
                    {
                        position: 3, data: { type:'separator',key:'separator',  label:__('Subtext','ultimate-post') } 
                    },     
                    {
                        position: 4, data: { type:'range', key:'subTextSpace', min: 0, max: 400, step: 1, responsive: true, label:__('Space Between Text & Subtext','ultimate-post'), unit: ['px']}
                    },
                    {
                        position: 5, data: { type:'color', key:'subTextColor', label:__('Subtext Color','ultimate-post')}
                    },
                    {
                        position: 9, data: { type: 'color', key: 'listSubtextHoverColor', label: __('Subtext Hover Color', 'ultimate-post') }
                    },
                ]} initialOpen={false}  store={store}/>
            </Section>
            <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                <ResponsiveAdvanced store={store}/>
                <CustomCssAdvanced store={store}/>
            </Section>
        </Sections>
    );
};

export default ListsSettings;