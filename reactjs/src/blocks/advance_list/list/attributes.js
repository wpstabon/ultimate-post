import { dynamicRichTextAttributes } from "../../../helper/dynamic_content";

const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    currentPostId: { type: "string" , default: "" },
    /*==========================
        Global Style
    ==========================*/
    /*======= Initialize for Parent Block Attr Value Save.JS ========*/
    layout:{
        type: 'string',
        default: ''
    },
    listGroupCustomImg:{
        type: 'object',
        default: ''
    }, 

    listGroupIconType:{
        type: 'string',
        default: '',
    }, 
    listDisableText:{
        type: 'boolean',
        default: true,
    },
    listCustomIcon:{
        type: 'string',
        default: '',
    },            
    listGroupSubtextEnable:{
        type: 'boolean',
        default: true,
    },
    listGroupBelowIcon:{
        type: 'boolean',
        default: false,
    },
    enableIcon:{
        type: 'boolean',
        default: true,
    },
    listLayout:{
        type: 'string',
        default: 'number',
    },
    
    /*==========================
        Icon Settings
    ==========================*/
    listText: {
        type: 'string',
        default: ''
    },
    listIconType:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key: 'enableIcon', condition:'==', value: true },
                    { key:'listGroupIconType', condition:'!=', value: "default" },
                ], 
            }
        ],
    },
    listSinleCustomIcon:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: "default" },
                    { key:'listIconType', condition:'==', value: "icon" },
                ], 
            }
        ],
    },
    listCustomImg:{
        type: 'object',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: "default" },
                    { key:'listIconType', condition:'==', value: "image" },
                ], 
            }
        ],
    },
    listIconColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before { color:{{listIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before { background-color:{{listIconColor}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}} .ultp-listicon-bg svg { fill:{{listIconColor}}; }'
            }
        ]
    },
    listIconBg:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before { background: {{listIconBg}};}'
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon { background: {{listIconBg}};}'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-listicon-bg { background: {{listIconBg}};}'
            }
        ]
    },
    listIconBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before'
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon'
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list .ultp-listicon-bg'
            },
        ]
    },

    // Hover
    listIconHoverColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon:before { color:{{listIconHoverColor}}; }'
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon:before { background-color:{{listIconHoverColor}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list:hover .ultp-listicon-bg svg { fill:{{listIconHoverColor}}; }'
            }
        ]
        
    },
    listIconHoverBg:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `.block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before,
                .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon:before { background: {{listIconHoverBg}};}`
            },
            { 
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon { background-color:{{listIconHoverBg}}; }'
            },
            { 
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector: '{{ULTP}}.wp-block-ultimate-post-list:hover .ultp-listicon-bg { background: {{listIconHoverBg}};}'
            }
        ]
    },
    listIconHoverBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                depends: [
                    { key:'listLayout', condition:'!=', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `.block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon:before,  
                .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon:before`
            },
            {
                depends: [
                    { key:'listLayout', condition:'==', value: 'bullet' },
                    { key:'listGroupIconType', condition:'==', value: 'default' },
                ],
                selector:
                `.block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-texticon, 
                .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-texticon`
            },
            {
                depends: [
                    { key:'listGroupIconType', condition:'!=', value: 'default' },
                ],
                selector:
                `.block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-listicon-bg, 
                .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-listicon-bg`
            },
        ]
    },
    
    /*==========================
        Text Content Settings
    ==========================*/
    listTextColor:{
        type: 'string',
        default: '',
        style: [{selector:`{{ULTP}} .ultp-list-title,
            {{ULTP}} .ultp-list-title a { color:{{listTextColor}}; } `}]
    },
    listTextHoverColor:{
        type: 'string',
        default: '',
        style: [{
            selector:`
            .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-title, 
            .ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-title a, 
            .block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-title, 
            .block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-title a { color:{{listTextHoverColor}}; } `
        }]
    },
    subtext: {
        type: 'string',
        default: '',
    },
    subTextSpace: {
        type: 'object',
        default: {lg:'', ulg:'px'},
        style:[
            {
                depends: [
                    
                    { key:'layout', condition:'==', value: "layout3" },
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} .ultp-list-subtext { margin-top:{{subTextSpace}}; }'
            },
            {
                depends: [
                    { key:'layout', condition:'!=', value: "layout3" },
                    { key:'listGroupSubtextEnable', condition:'==', value: true },
                ], 
                selector: '{{ULTP}} .ultp-list-content { row-gap:{{subTextSpace}}; }'
            }
        ],
    },
    subTextColor:{
        type: 'string',
        default: '',
        style:[{
            depends: [
                { key:'listGroupSubtextEnable', condition:'==', value: true },
            ], 
            selector: '{{ULTP}}  .ultp-list-subtext { color:{{subTextColor}}; }'
        },
    ],
    },
    listSubtextHoverColor:{
        type: 'string',
        default: '',
        style:[{
            depends: [
                { key:'listGroupSubtextEnable', condition:'==', value: true },
            ], 
            selector: 
            `.ultp-list-wrapper > {{ULTP}}.wp-block-ultimate-post-list:hover .ultp-list-subtext, 
            .block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-list .ultp-list-subtext { color:{{listSubtextHoverColor}}; }`
        }],
    },
    
    /*==========================
        General Advance Settings
    ==========================*/
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has(>
            {{ULTP}}) {display:none;}` }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has(>
            {{ULTP}}) {display:none;}` }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: `div:has(>
            {{ULTP}}) {display:none;}` }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{selector: ''}],
    },

    /*==========================
        Dynamic Content
    ==========================*/
    ...dynamicRichTextAttributes,
}

export default attributes;
