const { __ } = wp.i18n
import React from "react";
const { InspectorControls, RichText } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element;
const { Dropdown } = wp.components
const { createBlock } = wp.blocks;
import { CssGenerator } from '../../../helper/CssGenerator'
import Icon from "../../../helper/fields/Icon";
import IconPack from '../../../helper/fields/tools/IconPack';
import { isInlineCSS, updateCurrentPostId } from '../../../helper/CommonPanel'

import ListsSettings from './Setting';
import { isDCActive } from "../../../helper/dynamic_content";
const { select } = wp.data;
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, className, name, attributes, clientId, attributes: { blockId, advanceId, listText, listCustomImg, listIconType, subtext, listSinleCustomIcon, dcEnabled, currentPostId } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/list', blockId, isInlineCSS());
	}

	const IDs = select('core/block-editor').getBlockParents(clientId);
	const parentAttr = select('core/block-editor').getBlockAttributes(IDs[IDs.length - 1]);
	const { listGroupCustomImg, listGroupIconType, listDisableText, layout, listGroupSubtextEnable, listGroupBelowIcon, listCustomIcon, enableIcon, listLayout } = parentAttr;

	setAttributes({ listGroupCustomImg: listGroupCustomImg, listGroupIconType: listGroupIconType, listDisableText: listDisableText, listGroupBelowIcon: listGroupBelowIcon, listGroupSubtextEnable: listGroupSubtextEnable, listCustomIcon: listCustomIcon, layout: layout, enableIcon: enableIcon, listLayout: listLayout });
	const SubtextBelow = listGroupBelowIcon && layout != "layout3" ? 'div' : Fragment;

	function listIconValidation(val) {
		let isValid = listGroupIconType != "default" && (listIconType == val || (listIconType == "inherit" && listGroupIconType == val)) || (listIconType == "" && listGroupIconType == val);
		return isValid;
	}

	const imgCondition = listIconValidation('image');
	const svgCondition = listIconValidation('icon');

	return (
		<Fragment>
			<InspectorControls>
				<ListsSettings store={store} />
			</InspectorControls>
			<li {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className="ultp-list-content">
					<SubtextBelow {...(listGroupBelowIcon && layout != "layout3" && { class: 'ultp-list-heading' })}>
						{
							listGroupIconType == "default" && enableIcon &&
							<div className="ultp-list-texticon ultp-listicon-bg"></div>
						}
						{
							imgCondition && enableIcon &&
							<div className="ultp-listicon-bg">
								<img src={listCustomImg.url && listIconType != "inherit" ? listCustomImg.url : listGroupCustomImg.url} alt="List Image" />
							</div>
						}
						{
							svgCondition && enableIcon &&
							<Dropdown
								popoverProps={{ placement: 'bottom right' }} 
								// position="bottom right"
								className="ultp-listicon-dropdown"
								renderToggle={({ onToggle, onClose }) => (
									<div onClick={() => onToggle()} className="ultp-listicon-bg">
										{IconPack[listSinleCustomIcon.length > 0 && listIconType != "inherit" ? listSinleCustomIcon : listCustomIcon]}
									</div>
								)}
								renderContent={() => (
									<Icon
										inline={true}
										value={listSinleCustomIcon}
										label="Update Single Icon"
										dynamicClass=" "
										onChange={v => store.setAttributes({ listSinleCustomIcon: v, listIconType: 'icon' })}
									/>
								)}
							/>
						}
						{
							!listDisableText &&
							<RichText
								key="editable"
								tagName={'div'}
								className={'ultp-list-title'}
								keepPlaceholderOnFocus
								placeholder={__('List Text...', 'ultimate-post')}
								onChange={value => setAttributes({ listText: value })}
								allowedFormats={
									isDCActive() && dcEnabled ?
										['ultimate-post/dynamic-content'] :
										undefined
								}
								onReplace={(blocks, indexToSelect, initialPosition) =>
									wp.data.dispatch('core/block-editor').replaceBlocks(clientId, blocks, indexToSelect, initialPosition)
								}
								onSplit={(value, isAfterOriginal) =>
									createBlock('ultimate-post/list', { ...attributes, listText: value })
								}
								value={listText} />
						}
					</SubtextBelow>
					{
						!listDisableText && listGroupSubtextEnable &&
						<div className="ultp-list-subtext">
							<RichText
								key="editable"
								tagName={'span'}
								placeholder={__('List Subtext Text...', 'ultimate-post')}
								onChange={value => setAttributes({ subtext: value })}
								value={subtext} />
						</div>
					}
				</div>
			</li>
		</Fragment>
	)
}