import IconPack from "../../../helper/fields/tools/IconPack";
const { Fragment } = wp.element;

export default function Save(props) {

	const { attributes: { blockId, advanceId, listText, listCustomImg, listIconType, listGroupSubtextEnable, subtext, listGroupCustomImg, listGroupIconType, listDisableText, listGroupBelowIcon, listCustomIcon, listSinleCustomIcon, layout, enableIcon } } = props;

	const SubtextBelow = listGroupBelowIcon && layout != "layout3" ? 'div' : Fragment;

	function listIconValidation(val) {
		let isValid = listGroupIconType != "default" && (listIconType == val || (listIconType == "inherit" && listGroupIconType == val)) || (listIconType == "" && listGroupIconType == val);
		return isValid;
	}

	const imgCondition = listIconValidation('image');
	const svgCondition = listIconValidation('icon');

	return (
		<li {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId}`} >
			<div className="ultp-list-content">
				{
					<SubtextBelow {...(listGroupBelowIcon && layout != "layout3" && { class: 'ultp-list-heading' })}>
						{
							listGroupIconType == "default" && enableIcon &&
							<div className="ultp-list-texticon ultp-listicon-bg"></div>
						}
						{
							imgCondition && enableIcon &&
							<div className="ultp-listicon-bg">
								<img src={listCustomImg.url ? listCustomImg.url : listGroupCustomImg.url} alt="List Image" />
							</div>
						}
						{
							svgCondition && enableIcon &&
							<div className="ultp-listicon-bg">{IconPack[listSinleCustomIcon.length > 0 && listIconType != "inherit" ? listSinleCustomIcon : listCustomIcon]}</div>
						}
						{
							!listDisableText &&
							<div className="ultp-list-title"
								dangerouslySetInnerHTML={{ __html: listText }} />
						}
					</SubtextBelow>
				}
				{
					!listDisableText && listGroupSubtextEnable &&
					<div className="ultp-list-subtext"
						dangerouslySetInnerHTML={{ __html: subtext }} />
				}
			</div>
		</li>
	)
}