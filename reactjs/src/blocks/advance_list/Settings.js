const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced, blockSupportLink } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const ListsGroupSettings = ({ store }) => {
    
    return (
        <>
            <TemplateModal
                prev="https://www.wpxpo.com/postx/blocks/#demoid7994"
                store={store}
            />
            <Sections>
                <Section slug="global" title={__('Global Style','ultimate-post')}>
                    <CommonSettings title={`inline`} include={[
                        {
                            position: 1, data:{ type:'layout', block:'advance-list', key:'layout', label:__('Layout','ultimate-post'),
                                options:[
                                    { img: 'assets/img/layouts/list/list1.svg', label: 'Layout 1', value: 'layout1', pro: false  },
                                    { img: 'assets/img/layouts/list/list2.svg', label: 'Layout 2', value: 'layout2', pro: false },
                                    { img: 'assets/img/layouts/list/list3.svg', label: 'Layout 3', value: 'layout3', pro: false },
                                ],
                            }
                        },
                        {
                            position: 2, data:{ type:'toggle', key: 'listInline', label:__('Inline View','ultimate-post') } 
                        },
                        {
                            position: 3, data: { type: 'alignment', block: 'advance-list', key: 'listAlignment', disableJustify: true, label: __('Vertical Alignment (Align Items)', 'ultimate-post') }
                        },
                        {
                            position: 4, data: { type:'range', key:'listSpaceBetween', min: 0, max: 300, step: 1, responsive: true, label:__('Space Between Items','ultimate-post') } 
                        },
                        {
                            position: 5, data: { type:'range', key:'listSpaceIconText', min: 0, max: 300, step: 1, responsive: true, label:__('Spacing Between Icon & Texts','ultimate-post') } 
                        },
                        {
                            position: 6, data: { type:'group', key:'listPosition',justify:true,label:__('Icon Position','ultimate-post'), options:[
                                { value:'start',label:__('Top','ultimate-post') },
                                { value:'center',label:__('Center','ultimate-post') },
                                { value:'end',label:__('Bottom','ultimate-post') }
                            ]}
                        }
                    ]} initialOpen={true}  store={store}/>
                    <CommonSettings title={__('Icon/Image','ultimate-post')}
                        depend="enableIcon"
                        include={[
                        {
                            position: 1, data: { type:'tag',key:'listGroupIconType', label:__('Icon Type','ultimate-post'), options:[
                                { value:'icon',label:__('Icon','ultimate-post') },
                                { value:'image',label:__('Image','ultimate-post') },
                                { value:'default',label:__('Default','ultimate-post') },
                                { value:'',label:__('None','ultimate-post') }] } 
                        },
                        {
                            position: 2, data:{ type:'select', key:'listLayout', label:__('List Style', 'ultimate-post'),
                                options:[
                                    {  label: __('Select','ultimate-post'), value: ''},
                                    {  label: __('By Abc','ultimate-post'), value: 'abc'},
                                    {  label: __('By Roman Number','ultimate-post'), value: 'roman'},
                                    {  label: __('By 123','ultimate-post'), value: 'number'},
                                    {  label: __('By Bullet','ultimate-post'), value: 'bullet'},
                                ],
                            }
                        },
                        {
                            position: 3, data: { type:'icon', key:'listCustomIcon',label:__('Icon Store','ultimate-post')} 
                        },
                        {
                            position: 4, data: { type:'media',key:'listGroupCustomImg', label:__('Upload Custom Image','ultimate-post')}
                        },
                        {
                            position: 5, data: { type:'dimension', key:'listImgRadius',step:1 ,unit:true , responsive:true ,label:__('Image Radius','ultimate-post')}
                        },
                        {   position: 6, data: { type:'typography',key:'listIconTypo',label:__('Typography','ultimate-post') } },
                        {
                            position: 7, data: { type:'range',key:'listGroupIconSize', label:__('Icon/Image Size','ultimate-post')}
                        },
                        {
                            position: 8, data: { type:'range',key:'listGroupBgSize', label:__('Background Size','ultimate-post'), help: "Icon Background Color Required"}
                        },
                        {
                            position: 9, data: { type:'separator', label:__('Icon Style','ultimate-post')}
                        },
                        {
                            position: 10, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                    {  
                                        type: 'color', key: 'listGroupIconColor', label: __('Icon Color', 'ultimate-post')
                                    },
                                    { 
                                        type:'color', key:'listGroupIconbg', label:__('Icon  Background','ultimate-post') 
                                    },
                                    { 
                                        type:'border', key:'listGroupIconBorder', label:__('Border','ultimate-post') 
                                    },
                                    { 
                                        type:'dimension', key:'listGroupIconRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') 
                                    }
                                ]
                            },
                            {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                {  
                                    type: 'color', key: 'listGroupHoverIconColor', label: __('Icon Color', 'ultimate-post')
                                },
                                { 
                                    type:'color', key:'listGroupHoverIconbg', label:__('Icon  Background','ultimate-post') 
                                },
                                { 
                                    type:'border', key:'listGroupHoverIconBorder', label:__('Border','ultimate-post') 
                                },
                                { 
                                    type:'dimension', key:'listGroupHoverIconRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') 
                                }
                            ]}
                        ]}}

                    ]} initialOpen={false}  store={store}/>
                    <CommonSettings title={__('Content','ultimate-post')} include={[
                        {
                            position: 1, data:{ type:'toggle',key:'listDisableText', label:__('Disable Text','ultimate-post') } 
                        },
                        {
                            position: 2, data: { type:'typography', key:'listTextTypo',label:__('Title Typography','ultimate-post')}
                        },
                        {
                            position: 3, data: { type: 'color', key: 'listGroupTitleColor', label: __('Title Color', 'ultimate-post') },
                        },
                        {
                            position: 4, data: { type: 'color', key: 'listGroupTitleHoverColor', label: __('Title Hover Color', 'ultimate-post') }
                        },
                        {
                            position: 5, data:{ type:'toggle',key:'listGroupSubtextEnable', label:__('Enable Subtext', 'ultimate-post')}
                        },
                        {
                            position: 6, data: { type:'typography',key:'listGroupSubtextTypo',label:__('Subtext Typography','ultimate-post')}
                        },
                        {
                            position: 7, data: { type:'range',key:'listGroupSubtextSpace', unit: true, responsive: true, label:__('Space Between  Text & Subtext','ultimate-post')}
                        },
                        {
                            position: 8, data: { type: 'color', key: 'listGroupSubtextColor', label: __('Subtext Color', 'ultimate-post') }
                        },
                        {
                            position: 9, data: { type: 'color', key: 'listGroupSubtextHoverColor', label: __('Subtext Hover Color', 'ultimate-post') }
                        },
                        {
                            position: 10, data:{ type:'toggle',key:'listGroupBelowIcon', help: "Layout One/Two Required ", label:__('Midpoint Subtext','ultimate-post')} 
                        },
                    ]} initialOpen={false}  store={store}/>
                    <CommonSettings title={__('Content Wrap','ultimate-post')} include={[
                        {
                            position: 1, data: { type:'dimension', key:'listGroupPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') }
                        },
                        {   position: 2, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                    { 
                                        type:'color2', key:'listGroupBg', label:__('Background Color','ultimate-post') 
                                    },
                                    { 
                                        type:'border', key:'listGroupborder', label:__('Border','ultimate-post') 
                                    },
                                    { 
                                        type:'dimension', key:'listGroupRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') 
                                    }
                                    
                                ]
                            },
                            {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                { 
                                    type:'color2', key:'listGroupHoverBg', label:__('Icon  Background','ultimate-post') 
                                },
                                { 
                                    type:'border', key:'listGroupHovborder', label:__('Border','ultimate-post') 
                                },
                                { 
                                    type:'dimension', key:'listGroupHovRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') 
                                },
                            ]}
                        ]}}
                    ]} initialOpen={false}  store={store}/>
                    <CommonSettings title={__('Separator','ultimate-post')} depend='enableSeparator' include={[
                        {
                            position: 1, data: { type: 'color', key: 'listGroupSepColor', label: __('Border Color', 'ultimate-post') },
                        },
                        {
                            position: 2, data: { type:'range',key:'listGroupSepSize', min: 0, max: 30,label:__('Separator Size','ultimate-post')}
                        },
                        {
                            position: 3, data: { type:'select', key:'listGroupSepStyle', options: [
                                { value:'none',label:__('None','ultimate-post') },
                                { value:'dotted',label:__('Dotted','ultimate-post') },
                                { value:'solid',label:__('Solid','ultimate-post') },
                                { value:'dashed',label:__('Dashed','ultimate-post') },
                                { value:'groove',label:__('Groove','ultimate-post') },
                            ],  label:__('Border Style','ultimate-post')}
                        },
                    ]} initialOpen={false}  store={store}/>
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <GeneralAdvanced initialOpen={true} store={store}/>
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
            { blockSupportLink() }
        </>
    );
};

export default ListsGroupSettings;