const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/list-block/', 'block_docs');

registerBlockType(
    'ultimate-post/advanced-list', {
        title: __('List - PostX','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/advanced-list.svg'} alt="List PostX"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Create & customize bullets and numbered lists.','ultimate-post')}
            <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a>
        </span>,
        keywords: [ 
            __('list','ultimate-post'),
            __('list postx','ultimate-post'),
            __('advanced list','ultimate-post'),
            __('icon list','ultimate-post')
        ],
        attributes,
        supports: {
            html: false,
            reusable: false,
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/preview/list.svg',
            },
        },
        edit: Edit,
        save: Save
    }
)