const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { useRef, useState, useEffect, Fragment } = wp.element;
const { Spinner, Placeholder } = wp.components;
import {
	attrBuild,
	FeatureToggleArgsDep,
	FeatureToggleArgsNew,
	isInlineCSS,
	isReload,
	updateCurrentPostId,
} from '../../helper/CommonPanel';
import Category from '../../helper/Components/grid-category';
import Excerpt from '../../helper/Components/grid-excerpt';
import { GHeading2 } from '../../helper/Components/grid-heading';
import { Image4, Video } from '../../helper/Components/grid-image';
import Meta from '../../helper/Components/grid-meta';
import Navigation from '../../helper/Components/grid-nav';
import ReadMore from '../../helper/Components/grid-readmore';
import Title from '../../helper/Components/grid-title';
import { CssGenerator } from '../../helper/CssGenerator';
import ToolBarElement from '../../helper/ToolBarElement';
import { handleCompatibility } from '../../helper/compatibility';
import { isDCActive } from '../../helper/dynamic_content';
import AddDCButton from '../../helper/dynamic_content/AddDCButton';
import MetaGroup from '../../helper/dynamic_content/MetaGroup';
import {
	stateObj,
	SETTING_SECTIONS,
	resetState,
	restoreState,
	saveSelectedSection,
	saveToolbar,
	scrollSidebarSettings,
} from '../../helper/gridFunctions';
import PG4_Settings, {
	AddSettingsToToolbar,
	MAX_CUSTOM_META_GROUPS,
} from './Settings';
const { getBlockAttributes, getBlockRootClientId } =
	wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [state, setState] = useState(stateObj);

	const { setAttributes, name, attributes, context, className, isSelected, clientId, attributes: { blockId, currentPostId, excerptLimit, metaStyle, metaShow, catShow, showImage, metaSeparator, titleShow, catStyle, catPosition, titlePosition, excerptShow, imgAnimation, imgOverlayType, imgOverlay, metaList, metaListSmall, showSmallCat, readMore, readMoreText, readMoreIcon, overlayContentPosition, showSmallBtn, showSmallExcerpt, metaPosition, showFullExcerpt, layout, columnFlip, titleAnimation, customCatColor, onlyCatColor, contentTag, titleTag, showSeoMeta, titleLength, metaMinText, metaAuthorPrefix, titleStyle, metaDateFormat, authorLink, fallbackEnable, imgCropSmall, imgCrop, vidIconEnable, notFoundMessage, previewImg, advanceId, paginationShow, headingShow, filterShow, paginationType, navPosition, dcEnabled, dcFields, V4_1_0_CompCheck: { runComp } } } = props

	useEffect(() => {
		resetState();
		fetchProducts();
	}, []);

	function setSelectedDc(selectedDC) {
		setState({ ...state, selectedDC });
	}

	function selectParentMetaGroup(groupIdx) {
		setSelectedDc(groupIdx);
		setToolbarSettings('dc_group');
	}

	function startMetaFieldOnboarding() {
		setState({
			...state,
			selectedDC: '0,0,1',
			toolbarSettings: 'dc_field',
		});
	}

	function setSection(title) {
		setState({
			...state,
			section: {
				...SETTING_SECTIONS,
				[title]: true,
			},
		});
		scrollSidebarSettings(title);
		saveSelectedSection(title);
	}

	function setToolbarSettings(title) {
		setState(prev => ({ ...prev, toolbarSettings: title }));
		saveToolbar(title);
	}

	useEffect(() => {
		const _client = clientId.substr(0, 6);
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		handleCompatibility(props);

		if (!blockId) {
			setAttributes({ blockId: _client });
			if (ultp_data.archive && ultp_data.archive == 'archive') {
				setAttributes({ queryType: 'archiveBuilder' });
			}
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (isDCActive() && attributes.dcFields.length === 0) {
			setAttributes({
				dcFields: Array(MAX_CUSTOM_META_GROUPS).fill(undefined),
			});
		}
		if (prevAttributes) {
			if (isReload(prevAttributes, attributes)) {
				fetchProducts();
				prevPropsRef.current = attributes;
			}
			if (prevAttributes.isSelected !== isSelected && isSelected) {
				restoreState(setSection, setToolbarSettings);
			}
		} else {
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	function fetchProducts() {
		if (state.error) {
			setState({ ...state, error: false });
		}
		if (!state.loading) {
			setState({ ...state, loading: true });
		}
		wp.apiFetch({
			path: '/ultp/fetch_posts',
			method: 'POST',
			data: attrBuild(attributes),
		})
			.then((obj) => {
				setState({ ...state, postsList: obj, loading: false });
			})
			.catch((error) => {
				setState({ ...state, loading: false, error: true });
			});
	}

	function renderContent() {
		const CustomTag = `${contentTag}`;
		const customMeta = (idx, postId) => {
			return (
				isDCActive() &&
				dcEnabled && (
					<MetaGroup
						idx={idx}
						postId={postId}
						fields={dcFields}
						settingsOnClick={(e, name) => {
							e?.stopPropagation();
							setToolbarSettings(name);
						}}
						selectedDC={state.selectedDC}
						setSelectedDc={setSelectedDc}
						setAttributes={setAttributes}
						dcFields={dcFields}
					/>
				)
			);
		};
		
		return !state.error ? (
			!state.loading ? (
				state.postsList.length > 0 ? (
					<Fragment>
						<div
							className={`ultp-block-items-wrap ultp-block-row ultp-${layout} ultp-block-content-${columnFlip}`}
						>
							{state.postsList.map((post, idx) => {
								const meta =
									idx == 0
										? JSON.parse(
											metaList.replaceAll(
												'u0022',
												'"'
											)
										)
										: JSON.parse(
											metaListSmall.replaceAll(
												'u0022',
												'"'
											)
										);
								return (
									<CustomTag
										key={idx}
										className={`ultp-block-item post-id-${post.ID} ${titleAnimation ? 'ultp-animation-' + titleAnimation : ''}`}
									>
										<div
											className={`ultp-block-content-wrap ultp-block-content-overlay`}
										>
											<Image4
												imgOverlay={imgOverlay}
												imgOverlayType={imgOverlayType}
												imgAnimation={imgAnimation}
												post={post}
												fallbackEnable={fallbackEnable}
												vidIconEnable={vidIconEnable}
												catPosition={catPosition}
												imgCropSmall={imgCropSmall}
												showImage={showImage}
												imgCrop={imgCrop}
												idx={idx}
												showSmallCat={showSmallCat}
												Category={
													(idx == 0 ||
														showSmallCat) &&
														catPosition !=
														'aboveTitle' ? (
														<div
															className={`ultp-category-img-grid`}
														>
															<Category
																post={post}
																catShow={
																	catShow
																}
																catStyle={
																	catStyle
																}
																catPosition={
																	catPosition
																}
																customCatColor={
																	customCatColor
																}
																onlyCatColor={
																	onlyCatColor
																}
																onClick={(
																	e
																) => {
																	setSection('taxonomy-/-category');
																	setToolbarSettings('cat');
																}}
															/>
														</div>
													) : null
												}
												onClick={() => {
													setSection('image');
													setToolbarSettings('image');
												}}
											/>

											{vidIconEnable &&
												post.has_video && (
													<Video
														onClick={() => {
															setSection('video');
															setToolbarSettings('video');
														}}
													/>
												)}

											<div
												className={`ultp-block-content ultp-block-content-${overlayContentPosition}`}
											>
												<div
													className={`ultp-block-content-inner`}
												>
													{customMeta(7, post.ID)}

													{catPosition ==
														'aboveTitle' &&
														(idx == 0 ||
															showSmallCat) && (
															<Category
																post={post}
																catShow={
																	catShow
																}
																catStyle={
																	catStyle
																}
																catPosition={
																	catPosition
																}
																customCatColor={
																	customCatColor
																}
																onlyCatColor={
																	onlyCatColor
																}
																onClick={(
																	e
																) => {
																	setSection('taxonomy-/-category');
																	setToolbarSettings('cat');
																}}
															/>
														)}

													{customMeta(6, post.ID)}

													{post.title &&
														titleShow &&
														titlePosition ==
														true && (
															<Title
																title={
																	post.title
																}
																headingTag={
																	titleTag
																}
																titleLength={
																	titleLength
																}
																titleStyle={
																	titleStyle
																}
																onClick={(
																	e
																) => {
																	setSection('title');
																	setToolbarSettings('title');
																}}
															/>
														)}

													{customMeta(5, post.ID)}

													{metaShow &&
														metaPosition ==
														'top' && (
															<Meta
																meta={meta}
																post={post}
																metaSeparator={
																	metaSeparator
																}
																metaStyle={
																	metaStyle
																}
																metaMinText={
																	metaMinText
																}
																metaAuthorPrefix={
																	metaAuthorPrefix
																}
																metaDateFormat={
																	metaDateFormat
																}
																authorLink={
																	authorLink
																}
																onClick={(
																	e
																) => {
																	setSection('meta');
																	setToolbarSettings('meta');
																}}
															/>
														)}

													{customMeta(4, post.ID)}

													{post.title &&
														titleShow &&
														titlePosition ==
														false && (
															<Title
																title={
																	post.title
																}
																headingTag={
																	titleTag
																}
																titleLength={
																	titleLength
																}
																titleStyle={
																	titleStyle
																}
																onClick={(
																	e
																) => {
																	setSection('title');
																	setToolbarSettings('title');
																}}
															/>
														)}

													{customMeta(3, post.ID)}

													{(idx == 0 ||
														showSmallExcerpt) &&
														excerptShow && (
															<div
																className={`ultp-block-excerpt`}
															>
																<Excerpt
																	excerpt={
																		post.excerpt
																	}
																	excerpt_full={
																		post.excerpt_full
																	}
																	seo_meta={
																		post.seo_meta
																	}
																	excerptLimit={
																		excerptLimit
																	}
																	showFullExcerpt={
																		showFullExcerpt
																	}
																	showSeoMeta={
																		showSeoMeta
																	}
																	onClick={(
																		e
																	) => {
																		setSection('excerpt');
																		setToolbarSettings('excerpt');
																	}}
																/>
															</div>
														)}

													{customMeta(2, post.ID)}

													{(idx == 0 ||
														showSmallBtn) &&
														readMore && (
															<ReadMore
																readMoreText={
																	readMoreText
																}
																readMoreIcon={
																	readMoreIcon
																}
																titleLabel={
																	post.title
																}
																onClick={() => {
																	setSection('read-more');
																	setToolbarSettings('read-more');
																}}
															/>
														)}

													{customMeta(1, post.ID)}

													{metaShow &&
														metaPosition ==
														'bottom' && (
															<Meta
																meta={meta}
																post={post}
																metaSeparator={
																	metaSeparator
																}
																metaStyle={
																	metaStyle
																}
																metaMinText={
																	metaMinText
																}
																metaAuthorPrefix={
																	metaAuthorPrefix
																}
																metaDateFormat={
																	metaDateFormat
																}
																authorLink={
																	authorLink
																}
																onClick={(
																	e
																) => {
																	setSection('meta');
																	setToolbarSettings('meta');
																}}
															/>
														)}

													{customMeta(0, post.ID)}

													{isDCActive() &&
														dcEnabled && (
															<AddDCButton
																dcFields={dcFields}
																setAttributes={setAttributes}
																startOnboarding={startMetaFieldOnboarding}
																overlayMode
																inverseColor
															/>
														)}
												</div>
											</div>
										</div>
									</CustomTag>
								);
							})}
						</div>
						{/* <span className={`ultp-loadmore-insert-before`}></span> */}
					</Fragment>
				) : (
					<Placeholder
						className={`ultp-backend-block-loading`}
						label={notFoundMessage}
					></Placeholder>
				)
			) : (
				<Placeholder
					className={`ultp-backend-block-loading`}
					label={__('Loading...', 'ultimate-post')}
				>
					<Spinner />
				</Placeholder>
			)
		) : (
			<Placeholder
				label={__('Posts are not available.', 'ultimate-post')}
			>
				<div style={{ marginBottom: 15 }}>
					{__('Make sure Add Post.', 'ultimate-post')}
				</div>
			</Placeholder>
		);
	}


	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section: state.section,
		clientId,
		context,
		setSelectedDc,
		selectedDC: state.selectedDC,
		selectParentMetaGroup
	};

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(
			attributes,
			'ultimate-post/post-grid-4',
			blockId,
			isInlineCSS()
		);
	}

	if (previewImg) {
		return (
			<img
				style={{ marginTop: '0px', width: '420px' }}
				src={previewImg}
			/>
		);
	}

	return (
		<Fragment>
			<AddSettingsToToolbar
				store={store}
				selected={state.toolbarSettings}
			/>
			<InspectorControls>
				<PG4_Settings store={store} />
			</InspectorControls>

			<ToolBarElement
				include={[
					{
						type: 'query',
						exclude: ['queryNumber', 'queryNumPosts'],
					},
					{
						type: 'template',
					},
					{
						type: 'grid_align',
						key: 'contentAlign',
					},
					{
						type: 'grid_spacing',
						exclude: [
							'columns',
							'spaceSep',
							'wrapOuterPadding',
							'wrapMargin',
						],
					},
					{
						type: 'layout',
						block: 'post-grid-4',
						key: 'layout',
						options: [
							{
								value: 'layout1',
								img: 'assets/img/layouts/pg4/l1.png',
								label: __('Layout 1', 'ultimate-post'),
								pro: false,
							},
							{
								value: 'layout2',
								img: 'assets/img/layouts/pg4/l2.png',
								label: __('Layout 2', 'ultimate-post'),
								pro: true,
							},
							{
								value: 'layout3',
								img: 'assets/img/layouts/pg4/l3.png',
								label: __('Layout 3', 'ultimate-post'),
								pro: true,
							},
							{
								value: 'layout4',
								img: 'assets/img/layouts/pg4/l4.png',
								label: __('Layout 4', 'ultimate-post'),
								pro: true,
							},
						],
					},
					{
						type: 'feat_toggle',
						label: __('Grid Features', 'ultimate-post'),
						new: FeatureToggleArgsNew,
						[runComp ? 'exclude' : 'dep']: FeatureToggleArgsDep,
					},
				]}
				store={store}
			/>

			<div
				{...(advanceId && { id: advanceId })}
				className={`ultp-block-${blockId} ${className}`}
				onClick={(e) => {
					e.stopPropagation();
					setSection('general');
					setToolbarSettings('');
				}}
			>
				{__preview_css && (
					<style
						dangerouslySetInnerHTML={{ __html: __preview_css }}
					></style>
				)}
				<div className={`ultp-block-wrapper`}>
					{(headingShow || filterShow || paginationShow) && (
						<GHeading2
							attributes={attributes}
							setAttributes={setAttributes}
							onClick={() => {
								setSection('heading');
								setToolbarSettings('heading');
							}}
							setSection={setSection}
							setToolbarSettings={setToolbarSettings}
						/>
					)}
					{renderContent()}
					{paginationShow &&
						paginationType == 'navigation' &&
						navPosition != 'topRight' && (
							<Navigation
								onClick={() => {
									setSection('pagination');
									setToolbarSettings('pagination');
								}}
							/>
						)}
				</div>
			</div>
		</Fragment>
	);
}