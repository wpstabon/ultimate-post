import SocialIconPack from "../../../helper/fields/tools/IconPack";
const { Fragment } = wp.element;

export default function Save(props) {

	const { blockId, advanceId, socialText, customImg, socialIconType, enableText, customIcon, enableIcon, btnLink, btnLinkTarget, btnLinkDownload, btnLinkNoFollow, btnLinkSponsored, enableSubText, socialSubText } = props.attributes;

	function socialIconValidation(val) {
		return socialIconType == val;
	}

	const imgCondition = socialIconValidation('image');
	const svgCondition = socialIconValidation('icon');

	let linkRelation = 'noopener'
	linkRelation += btnLinkNoFollow ? ' nofollow' : '';
	linkRelation += btnLinkSponsored ? ' sponsored' : '';

	return (
		<li {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId}`} >
			<a href={''} className="ultp-social-content" {...(btnLink.url && { href: btnLink.url, target: btnLinkTarget })} rel={linkRelation} download={btnLinkDownload && 'download'} >
				{
					<Fragment>
						{
							imgCondition && enableIcon &&
							<div className="ultp-social-bg">
								<img src={customImg.url} alt="Social Image" />
							</div>
						}
						{
							svgCondition && enableIcon &&
							<div className="ultp-social-bg">{SocialIconPack[customIcon]}</div>
						}
						<div className="ultp-social-title-container">
							{
								enableText &&
								<div className="ultp-social-title"
									dangerouslySetInnerHTML={{ __html: socialText }} />
							}
							{
								enableText && enableSubText &&
								<div className="ultp-social-sub-title"
									dangerouslySetInnerHTML={{ __html: socialSubText }} />
							}
						</div>
					</Fragment>
				}
			</a>
		</li>
	)
}