const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType(
    'ultimate-post/social', {
        title: __('Social item','ultimate-post'),
        parent:  ["ultimate-post/social-icons"],
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/social.svg'} alt="Social item"/>,
        category: 'ultimate-post',
        description: __('Create & customize a Social icon.','ultimate-post'),
        keywords: [ 
            __('link','ultimate-post'),
            __('social','ultimate-post'),
        ],
        supports: {
            reusable: false,
            html: false,
        },
        attributes,
        edit: Edit,
        save: Save,
    }
)