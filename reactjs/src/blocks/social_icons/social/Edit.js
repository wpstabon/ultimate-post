const { __ } = wp.i18n
import React from "react";
const { InspectorControls, RichText } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element;
const { Dropdown } = wp.components
const { createBlock } = wp.blocks;
import { CssGenerator } from '../../../helper/CssGenerator'
import Icon from "../../../helper/fields/Icon";
import SocialIconPack from '../../../helper/fields/tools/IconPack';
import { isInlineCSS } from '../../../helper/CommonPanel'
import SocialSettings from './Setting';
import ToolBarElement from "../../../helper/ToolBarElement";
const { select } = wp.data;
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, className, name, attributes, clientId, attributes: { blockId, advanceId, socialText, customImg, socialIconType, customIcon, btnLink, enableSubText, socialSubText } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/social', blockId, isInlineCSS());
	}

	const IDs = select('core/block-editor').getBlockParents(clientId);
	const parentAttr = select('core/block-editor').getBlockAttributes(IDs[IDs.length - 1]);
	const { enableText, enableIcon } = parentAttr;

	setAttributes({ enableText: enableText, enableIcon: enableIcon });

	function socialIconValidation(val) {
		return socialIconType == val;
	}

	const imgCondition = socialIconValidation('image');
	const svgCondition = socialIconValidation('icon');

	return (
		<Fragment>
			<InspectorControls>
				<SocialSettings store={store} />
			</InspectorControls>
			<ToolBarElement
				include={[
					{
						type: 'linkbutton', key: 'btnLink', onlyLink: true, value: btnLink, placeholder: 'Enter Social URL', label: __('Social Url', 'ultimate-post')
					}
				]}
				store={store}
			/>
			<li {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<a href={'#'} className="ultp-social-content">
					<Fragment>
						{
							imgCondition && enableIcon &&
							<div className="ultp-social-bg">
								<img src={customImg.url || '#'} alt="Social Image" />
							</div>
						}

						{
							svgCondition && enableIcon &&
							<Dropdown
								popoverProps={{ placement: 'bottom right' }} 
								// position="bottom right"
								className="ultp-social-dropdown"
								renderToggle={({ onToggle, onClose }) => (
									<div onClick={() => onToggle()} className="ultp-social-bg">
										{SocialIconPack[customIcon]}
									</div>
								)}
								renderContent={() => (
									<Icon
										inline={true}
										value={customIcon}
										isSocial={true}
										label="Update Single Icon"
										dynamicClass=" "
										onChange={v => store.setAttributes({ customIcon: v, socialIconType: 'icon' })}
									/>
								)}
							/>
						}
						<div className="ultp-social-title-container">
							{
								enableText &&
								<div className={'ultp-social-title'}>
									<RichText
										key="editable"
										tagName={'div'}
										keepPlaceholderOnFocus
										placeholder={__('Label...', 'ultimate-post')}
										onChange={value => setAttributes({ socialText: value })}
										onReplace={(blocks, indexToSelect, initialPosition) =>
											wp.data.dispatch('core/block-editor').replaceBlocks(clientId, blocks, indexToSelect, initialPosition)
										}
										onSplit={(value) =>
											createBlock('ultimate-post/social', { ...attributes, socialText: value })
										}
										value={socialText}
									/>
								</div>
							}
							{
								enableText && enableSubText &&
								<div className={'ultp-social-sub-title'}>
									<RichText
										key="editable"
										tagName={'div'}
										keepPlaceholderOnFocus
										placeholder={__('Sub Text...', 'ultimate-post')}
										onChange={value => setAttributes({ socialSubText: value })}
										value={socialSubText}
									/>
								</div>
							}
						</div>
					</Fragment>
				</a>
			</li>
		</Fragment>
	)
}