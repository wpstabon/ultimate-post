const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, ResponsiveAdvanced } from '../../../helper/CommonPanel';
import { Section, Sections } from '../../../helper/Sections';

const SocialSettings = ({ store }) => {
    return (
        <Sections>
            <Section slug="button" title={__('Icon','ultimate-post')}>
                <CommonSettings title={`inline`} 
                    include={[
                        { position: 1, data: { type:'linkbutton',key:'btnLink', onlyLink: true, placeholder: 'Enter Social URL', label:__('Social Url', 'ultimate-post') } },
                        {
                            position: 2, data: { type:'tag',key:'socialIconType', label:__('Icon Type','ultimate-post'), options:[
                                { value:'icon',label:__('Icon','ultimate-post') },
                                { value:'image',label:__('Image','ultimate-post') },
                                { value:'none',label:__('None','ultimate-post') }] } 
                        },
                        { position: 5, data: { type:'icon', key:'customIcon', isSocial: true, label:__('Icon Store','ultimate-post')} },
                        { position: 10, data: { type:'media',key:'customImg', label:__('Upload Custom Image','ultimate-post')} },
                        { position: 11, data: { type:'color2', key:'contentBg', label:__('Content Background','ultimate-post')} },
                        { position: 15, data: { type:'separator', label:__('Icon Style','ultimate-post')} },                    
                        {
                            position: 20, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), 
                                    options: [
                                        { type: 'color', key: 'iconColor', label: __('Icon Color', 'ultimate-post') },
                                        { type:'color', key:'iconBg', label:__('Icon  Background','ultimate-post') },
                                        { type:'border', key:'iconBorder', label:__('Border','ultimate-post') },
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), 
                                        options: [
                                            {  type: 'color', key: 'iconHvrColor', label: __('Icon Hover Color', 'ultimate-post') },
                                            { type:'color', key:'iconHvrBg', label:__('Icon Hover Background','ultimate-post') },
                                            { type:'border', key:'iconHvrBorder', label:__('Hover Border','ultimate-post')  },
                                        ]
                                }
                            ]}
                        }
                    ]}
                    initialOpen={true}  store={store}
                />
                <CommonSettings title={__('Text','ultimate-post')} 
                    include={[
                        { position: 1, data: { type:'color', key:'socialTextColor', label:__('Text Color','ultimate-post')} },
                        { position: 2, data: { type:'color', key:'socialTextHoverColor', label:__('Text Hover Color','ultimate-post')} },
                        { position: 3, data:{ type:'toggle', key: 'enableSubText', label:__('Enable Sub Text','ultimate-post') } },
                        { position: 4, data: { type:'color', key:'socialSubTextColor', label:__('SubText Color','ultimate-post')} },
                        { position: 5, data: { type:'typography', key:'subTextTypo',label:__('SubText Typography','ultimate-post')} },
                            
                    ]} 
                    initialOpen={false}  store={store}
                />
            </Section>
            <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                <ResponsiveAdvanced store={store}/>
                <CustomCssAdvanced store={store}/>
            </Section>
        </Sections>
    );
};

export default SocialSettings;