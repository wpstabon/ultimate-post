const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },

    /*======= Initialize for Parent Block Attr Value Save.JS ========*/
    enableText:{
        type: 'boolean',
        default: true,
    },
    enableIcon:{
        type: 'boolean',
        default: true,
    },

    /*==========================
        Icon
    ==========================*/
    btnLink:{
        type: 'object',
        default: { url: '#' },
    },
    btnLinkTarget:{
        type: 'string',
        default: '_blank',
    },
    btnLinkNoFollow:{
        type: 'boolean',
        default: false,
    },
    btnLinkSponsored:{
        type: 'boolean',
        default: false,
    },
    btnLinkDownload:{
        type: 'boolean',
        default: false,
    },
    socialText: {
        type: 'string',
        default: 'WordPress'
    },
    socialIconType:{
        type: 'string',
        default: 'icon',
        style: [
            { 
                depends: [
                    { key: 'enableIcon', condition:'==', value: true },
                ], 
            }
        ],
    },
    customIcon:{
        type: 'string',
        default: 'wordpress_solid',
        style: [
            { 
                depends: [
                    { key:'socialIconType', condition:'==', value: "icon" },
                ], 
            }
        ],
    },
    customImg:{
        type: 'object',
        default: '',
        style: [
            { 
                depends: [
                    { key:'socialIconType', condition:'==', value: "image" },
                ], 
            }
        ],
    },
    contentBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[
            {
                selector: '{{ULTP}}.wp-block-ultimate-post-social .ultp-social-content'
            },
        ],
    },
    iconColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'socialIconType', condition:'==', value: 'icon' },
                ],
                selector:'{{ULTP}} .ultp-social-content .ultp-social-bg svg { fill:{{iconColor}}; }'
            },
        ]
    },
    iconBg:{
        type: 'string',
        default: '',
        style: [
            {
                selector:'{{ULTP}} .ultp-social-content .ultp-social-bg { background-color:{{iconBg}}; }'
            },
        ]
    },
    iconBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                selector:'{{ULTP}} .ultp-social-content .ultp-social-bg'
            },
        ]
    },

    // Hover
    iconHvrColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'socialIconType', condition:'==', value: 'icon' },
                ],
                selector:'{{ULTP}} .ultp-social-content:hover .ultp-social-bg svg { fill:{{iconHvrColor}}; }'
            },
        ]
        
    },
    iconHvrBg:{
        type: 'string',
        default: '',
        style: [
            {
                selector: '{{ULTP}} .ultp-social-content .ultp-social-bg:hover { background: {{iconHvrBg}};}'
            },
        ]
    },
    iconHvrBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [
            {
                selector: '{{ULTP}} .ultp-social-content .ultp-social-bg:hover'
            },
        ]
    },
    
    /*==========================
        Text
    ==========================*/
    socialTextColor:{
        type: 'string',
        default: '',
        style: [{ 
            selector:'{{ULTP}} .ultp-social-title, {{ULTP}} .ultp-social-title a { color:{{socialTextColor}}; } '
        }]
    },
    socialTextHoverColor:{
        type: 'string',
        default: '',
        style: [{ 
            selector: 
            `.ultp-social-icons-wrapper > {{ULTP}}.wp-block-ultimate-post-social:hover .ultp-social-title, 
            .ultp-social-icons-wrapper > {{ULTP}}.wp-block-ultimate-post-social:hover .ultp-social-title a, 
            .block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-social .ultp-social-title, 
            .block-editor-block-list__block:hover > {{ULTP}}.wp-block-ultimate-post-social .ultp-social-title a { color:{{socialTextHoverColor}}; } `
        }]
    },
    enableSubText:{
        type: 'boolean',
        default: false,
    },
    subTextTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:12, unit:'px'},height: {lg:'24', unit:'px'}, decoration: 'none',family: '', weight: 400},
        style: [
            { 
                depends: [
                    { key:'enableSubText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-social-sub-title'
            },
        ]
    },
    socialSubText: {
        type: 'string',
        default: 'Sub Text'
    },
    socialSubTextColor:{
        type: 'string',
        default: '',
        style: [
            { 
                depends: [
                    { key:'enableSubText', condition:'==', value: true },
                ],
                selector:'{{ULTP}} .ultp-social-sub-title { color:{{socialSubTextColor}}; } '
            },
        ]
    },
    
    /*==========================
        General Advance Settings
    ==========================*/
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: 'div:has(> {{ULTP}}) {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: 'div:has(> {{ULTP}}) {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: 'div:has(> {{ULTP}}) {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{ selector: ''}],
    }
}

export default attributes;