const { __ } = wp.i18n
import { CommonSettings, CustomCssAdvanced, GeneralAdvanced, ResponsiveAdvanced } from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const SocialIconsGroupSettings = ({ store }) => {
    
    return (
        <>
            <Sections>
                <Section slug="global" title={__('Global Style','ultimate-post')}>
                    <CommonSettings title={`inline`} 
                        include={[
                            {
                                position: 1, data:{ type:'layout', block:'social-icons', key:'layout', label:__('Layout','ultimate-post'),
                                    options:[
                                        { img: 'assets/img/layouts/social_icons/icon1.png', label: 'Layout 1', value: 'layout1', pro: false  },
                                        { img: 'assets/img/layouts/social_icons/icon2.png', label: 'Layout 2', value: 'layout2', pro: false },
                                    ],
                                }
                            },
                            { position: 2, data:{ type:'toggle', key: 'iconInline', label:__('Inline View','ultimate-post') } },
                            { position: 3, data: { type: 'alignment', block: 'social-icons', key: 'iconAlignment', disableJustify: true, label: __('Horizontal Alignment', 'ultimate-post') } },
                            {position: 4, data: { type:'range', key:'iconSpace', min: 0, max: 300, step: 1, responsive: true, label:__('Space Between Items','ultimate-post') } },
                            { position: 5, data: { type:'range', key:'iconTextSpace', min: 0, max: 300, step: 1, responsive: true, label:__('Spacing Between Icon & Texts','ultimate-post') } }
                        ]} 
                        initialOpen={true}  store={store}
                    />
                    <CommonSettings title={__('Icon/Image','ultimate-post')}
                        depend="enableIcon"
                        include={[
                            {
                                position: 2, data: { type:'group', key:'iconPosition',justify:true,label:__('Icon - Label Alignment','ultimate-post'), options:[
                                    { value:'initial',label:__('Top','ultimate-post') },
                                    { value:'center',label:__('Center','ultimate-post') },
                                    { value:'end',label:__('Bottom','ultimate-post') }
                                ]}
                            },
                            { position: 5, data: { type:'dimension', key:'imgRadius',step:1 ,unit:true , responsive:true ,label:__('Image Radius','ultimate-post')} },
                            { position: 7, data: { type:'range',key:'iconSize2', responsive:true, label:__('Icon/Image Size','ultimate-post')} },
                            { position: 8, data: { type:'range',key:'iconBgSize2', responsive:true, label:__('Background Size','ultimate-post'), help: "Icon Background Color Required"} },
                            { position: 9, data: { type:'separator', label:__('Icon Style','ultimate-post')} },
                            {
                                position: 10, data: { type: 'tab', content: [
                                    {   name: 'normal', title: __('Normal', 'ultimate-post'), options: [
                                        { type: 'color', key: 'iconColor', label: __('Icon Color', 'ultimate-post') },
                                        { type:'color', key:'iconBg', label:__('Icon  Background','ultimate-post') },
                                        { type:'border', key:'iconBorder', label:__('Border','ultimate-post') },
                                        { type:'dimension', key:'iconRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') }
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), options: [
                                    { type: 'color', key: 'iconHvrColor', label: __('Icon Color', 'ultimate-post') },
                                    { type:'color', key:'iconHvrBg', label:__('Icon  Background','ultimate-post') },
                                    { type:'border', key:'iconHvrBorder', label:__('Border','ultimate-post') },
                                    { type:'dimension', key:'iconHvrRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') }
                                ]}
                            ]}}
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Text','ultimate-post')} 
                        depend="enableText"
                        include={[
                            { position: 2, data: { type:'typography', key:'textTypo',label:__('Text Typography','ultimate-post')} },
                            { position: 3, data: { type: 'color', key: 'textColor', label: __('Text Color', 'ultimate-post') } },
                            { position: 4, data: { type: 'color', key: 'textHvrColor', label: __('Text Hover Color', 'ultimate-post') } },
                        ]} 
                        initialOpen={false}  store={store}
                    />
                    <CommonSettings title={__('Content Wrap','ultimate-post')} 
                        include={[
                            { position: 1, data: { type:'dimension', key:'contentPadding',step:1 ,unit:true , responsive:true ,label:__('Padding','ultimate-post') } },
                            { position: 2, data:{ type:'toggle', key: 'contentFullWidth', label:__('Full Width','ultimate-post') } },
                            {   position: 3, data: { type: 'tab', content: [
                                {   name: 'normal', title: __('Normal', 'ultimate-post'), 
                                    options: [
                                        { type:'color2', key:'contentBg', label:__('Background Color','ultimate-post') },
                                        { type:'border', key:'contentBorder', label:__('Border','ultimate-post') },
                                        { type:'dimension', key:'contentRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') }
                                        
                                    ]
                                },
                                {   name: 'hover', title: __('Hover', 'ultimate-post'), 
                                    options: [
                                        { type:'color2', key:'contentHvrBg', label:__('Icon  Background','ultimate-post') },
                                        { type:'border', key:'contentHvrBorder', label:__('Border','ultimate-post') },
                                        { type:'dimension', key:'contentHvrRadius',step:1 ,unit:true , responsive:true ,label:__('Border Radius','ultimate-post') },
                                    ]
                                }
                            ]}}
                        ]}
                        initialOpen={false}  store={store}
                    />
                </Section>
                <Section slug="advanced" title={__('Advanced','ultimate-post')}>
                    <GeneralAdvanced initialOpen={true} store={store}/>
                    <ResponsiveAdvanced store={store}/>
                    <CustomCssAdvanced store={store}/>
                </Section>
            </Sections>
        </>
    );
};

export default SocialIconsGroupSettings;