const { __ } = wp.i18n
const { InspectorControls, InnerBlocks } = wp.blockEditor
const { useEffect, useState, useRef, Fragment } = wp.element
import { isInlineCSS, updateChildAttr, updateCurrentPostId } from '../../helper/CommonPanel'
import { CssGenerator } from '../../helper/CssGenerator'
import SocialIconsGroupSettings from './Settings'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const [section, setSection] = useState('Content');
	const { setAttributes, name, attributes, className, clientId, attributes: { previewImg, enableIcon, enableText, blockId, advanceId, layout, iconSize2, iconBgSize2, iconSize, iconBgSize, currentPostId } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (iconSize2.replace && iconSize) {
			setAttributes({ iconSize2: { lg: iconSize } });
		}
		if (iconBgSize2.replace && iconBgSize) {
			setAttributes({ iconBgSize2: { lg: iconBgSize } });
		}

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;

		if (!prevAttributes) {
			prevPropsRef.current = attributes;
		} else if (needsUpdate(prevAttributes)) {
			updateChildAttr(clientId);
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	function needsUpdate(prevProps) {
		return (
			prevProps.enableText != enableText ||
			prevProps.enableIcon != enableIcon
		)
	}

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/social-icons', blockId, isInlineCSS());
	}

	const socialTemplate = [
		["ultimate-post/social", { socialText: 'Facebook', customIcon: 'facebook' }],
		["ultimate-post/social", { socialText: 'WordPress', customIcon: 'wordpress_solid' }],
		["ultimate-post/social", { socialText: 'WhatsApp', customIcon: 'whatsapp' }],
	]

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<InspectorControls>
				<SocialIconsGroupSettings store={store} />
			</InspectorControls>
			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<ul className={`ultp-social-icons-wrapper ultp-social-icons-${layout}`}>
					<InnerBlocks
						template={socialTemplate}
						allowedBlocks={['ultimate-post/social']}
					/>
				</ul>
			</div>
		</Fragment>
	)
}