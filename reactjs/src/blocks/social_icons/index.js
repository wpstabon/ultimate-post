const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';
const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/social-icon-block/', 'block_docs');

registerBlockType(
    'ultimate-post/social-icons', {
        title: __('Social Icons','ultimate-post'),
        icon: <img className='ultp-block-icon' src={ultp_data.url + 'assets/img/blocks/social_icons.svg'} alt="Social Icons"/>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Display customizable Social Icons that link to your social profiles','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [ 
            __('social','ultimate-post'),
            __('icon','ultimate-post'),
        ],
        attributes,
        supports: {
            html: false,
            reusable: false,
        },
        // example: {
        //     attributes: {
        //         previewImg: ultp_data.url+'assets/img/preview/social_icon.svg',
        //     },
        // },
        edit: Edit,
        save: Save,
    }
)