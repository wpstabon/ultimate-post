const attributes = {
    blockId: {
        type: 'string',
        default: '',
    },
    // previewImg: {
    //     type: 'string',
    //     default: '',
    // },
    currentPostId: { type: "string" , default: "" },

    /*==========================
        Global Style
    ==========================*/
    layout: {
        type: 'string',
        default: 'layout1',
        style: [
            {
                depends: [ 
                    { key:'layout', condition:'==', value: 'layout1' } 
                ], 
            },
            {
                depends: [  
                    { key:'layout', condition:'==', value: 'layout2' }, 
                ], 
            },
        ]
    },
    iconInline:{
        type: 'boolean',
        default: true,
        style: [
            {
                depends: [
                    { key:'iconInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, 
                {{ULTP}} .ultp-social-icons-wrapper:has( > .wp-block-ultimate-post-social) { display: flex;} 
                {{ULTP}} .ultp-social-icons-wrapper > li:last-child { padding-right: 0px; margin-right: 0px; } 
                {{ULTP}} .block-editor-block-list__layout > div, 
                {{ULTP}} .wp-block-ultimate-post-social { width: auto !important; }`
            },
            {
                depends: [
                    { key:'iconInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, 
                {{ULTP}} .ultp-social-icons-wrapper:has( > .wp-block-ultimate-post-social) { display: block;}  
                {{ULTP}} .block-editor-block-list__layout > div, 
                {{ULTP}} .wp-block-ultimate-post-social { width: 100%; }`
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout2' },
                    { key:'enableSeparator', condition:'==', value: true },
                    { key:'iconInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, 
                {{ULTP}} .ultp-social-icons-wrapper:has( > .wp-block-ultimate-post-social) { display: flex;} 
                {{ULTP}} .block-editor-block-list__layout>div:last-child li{ padding-right: 0px; margin-right: 0px;}  
                {{ULTP}} .block-editor-block-list__layout > div, 
                {{ULTP}} .wp-block-ultimate-post-social { width: auto !important; }`
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout2' },
                    { key:'enableSeparator', condition:'==', value: true },
                    { key:'iconInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, 
                {{ULTP}} .ultp-social-icons-wrapper:has( > .wp-block-ultimate-post-social) { display: block; }  
                {{ULTP}} .block-editor-block-list__layout > div, 
                {{ULTP}} .wp-block-ultimate-post-social{ width: 100%; }`
            }
        ]
    },
    iconAlignment:{
        type: 'string',
        default: 'left',
        style: [
            {
                depends: [
                    { key:'iconAlignment', condition:'==', value: 'left' },
                    { key:'iconInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-right:  auto; display: flex; flex-direction: column; align-items: flex-start;}`
            },
            {
                depends: [
                    { key:'iconAlignment', condition:'==', value: 'right' },
                    { key:'iconInline', condition:'==', value: false },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-left: auto; display: flex; flex-direction: column; align-items: flex-end;}`
            },
            {
                depends: [
                    { key:'iconInline', condition:'==', value: false },
                    { key:'iconAlignment', condition:'==', value: 'center' },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin: auto auto; display: flex; flex-direction: column; align-items: center;}`
            },
            {
                depends: [
                    { key:'iconInline', condition:'==', value: true },
                    { key:'iconAlignment', condition:'==', value: 'center' },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin: auto auto; display: flex; flex-wrap: wrap; justify-content: center;} 
                {{ULTP}} .ultp-social-icons-wrapper .ultp-social-content { justify-content: center; }`
            },
            {
                depends: [
                    { key:'iconAlignment', condition:'==', value: 'left' },
                    { key:'iconInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-right:  auto; display: flex; justify-content: flex-start; flex-wrap: wrap;}`
            },
            {
                depends: [
                    { key:'iconAlignment', condition:'==', value: 'right' },
                    { key:'iconInline', condition:'==', value: true },
                ], 
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social ), 
                {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin-left:  auto; display: flex; justify-content: flex-end; flex-wrap: wrap;}`
            },
        ]
    },
    iconSpace:{
        type: 'object',
        default: { lg:'17', unit: 'px' },
        style: [{
            selector:
            `{{ULTP}} > .ultp-social-icons-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, 
            {{ULTP}} .ultp-social-icons-wrapper:has( > .wp-block-ultimate-post-social) { gap: {{iconSpace}}; }`
        }]
    },
    iconTextSpace:{
        type: 'object',
        default: { lg:'12', ulg:'px' },
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content { gap:{{iconTextSpace}}; }'
        }]
    },
    
    /*==========================
        Icon/Image
    ==========================*/
    enableIcon:{
        type: 'boolean',
        default: true,
    },
    iconPosition: {
        type: 'string',
        default: 'center',
        style: [{ 
            depends: [
                { key:'enableIcon', condition:'==', value: true },
            ], 
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content { align-items:{{iconPosition}}; }'
        }],
    },
    imgRadius: {
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [{
            depends: [
                { key:'socialIconsType', condition:'==', value: 'image' },
            ],
            selector: '{{ULTP}} .wp-block-ultimate-post-social img { border-radius: {{imgRadius}}; }'
        }]
    },
    // compatibilty start
    iconSize: {
        type: 'string',
        // default: '18',
    },
    iconBgSize: {
        type: 'string',
        // default: '',
    },
    // compatibilty end
    iconSize2: {
        type: 'object',
        default: { lg: 18, replace: 1 },
        style: [{
            selector:
            `{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg svg, 
            {{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg img { height:{{iconSize2}}px; width:{{iconSize2}}px; }`
        }]
    },
    iconBgSize2: {
        type: 'object',
        default: { lg: '', replace: 1 },
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg { height:{{iconBgSize2}}px; width:{{iconBgSize2}}px; }'
        }]
    },

    // color and others
    iconColor:{
        type: 'string',
        default: 'var(--postx_preset_Contrast_1_color)',
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg svg { fill: {{iconColor}}; }'
        }],
    },
    iconBg:{
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg { background-color: {{iconBg}}; }'
        }],
    },
    iconBorder: {
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [{
                selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg'
        }]
    },
    iconRadius:{
        type: 'object',
        default: {lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit:'px'},
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg { border-radius: {{iconRadius}}; }'
        }]
    },

    // Hover
    iconHvrColor:{
        type: 'string',
        default: 'var(--postx_preset_Primary_color)',
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg:hover svg { fill: {{iconHvrColor}}; }'
        }],
    },
    iconHvrBg:{
        type: 'string',
        default: '',
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg:hover { background-color: {{iconHvrBg}}; }'
        }],
    },
    iconHvrBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg:hover'
        }]
    },
    iconHvrRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-bg:hover { border-radius: {{iconHvrRadius}}; }'
        }]
    },

    /*==========================
        Content
    ==========================*/
    enableText:{
        type: 'boolean',
        default: true,
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                ], 
            },
            {
                depends: [
                    { key:'enableText', condition:'==', value: false },
                ], 
                selector: '{{ULTP}} .ultp-social-content { display: block !important; }'
            },
        ]
    },
    textTypo:{
        type: 'object',
        default: {openTypography: 1,size: {lg:16, unit:'px'},height: {lg:'24', unit:'px'}, decoration: 'none',family: '', weight: 400},
        style: [{ selector: `{{ULTP}}  .ultp-social-title, {{ULTP}}  .ultp-social-title a` }]
    },
    textColor:{
        type: 'string',
        default: 'var(--postx_preset_Contrast_1_color)',
        style: [{
            selector: `{{ULTP}} .ultp-social-title, {{ULTP}} .ultp-social-title a { color: {{textColor}}; }` 
        }]
    },
    textHvrColor:{
        type: 'string',
        default: 'var(--postx_preset_Primary_color)',
        style: [{
            selector:
            `{{ULTP}} .ultp-social-icons-wrapper  > .wp-block-ultimate-post-social:hover .ultp-social-title, 
            {{ULTP}} .ultp-social-icons-wrapper  > .wp-block-ultimate-post-social:hover .ultp-social-title a, 
            {{ULTP}} .block-editor-block-list__block:hover > .wp-block-ultimate-post-social .ultp-social-title, 
            {{ULTP}} .block-editor-block-list__block:hover > .wp-block-ultimate-post-social .ultp-social-title a { color: {{textHvrColor}}; }`
        }]
    },

    /*==========================
        Content Wrap
    ==========================*/
    contentPadding:{
        type: 'object',
        default: {lg: { top: "2", bottom: "2", left: "6", right: "6", unit: "px" } },
        style:[{
            selector: '{{ULTP}} .wp-block-ultimate-post-social  .ultp-social-content { padding: {{contentPadding}}; }'
        }],
    },
    contentBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: 'var(--postx_preset_Base_2_color)', size: 'cover', repeat: 'no-repeat'},
        style:[{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content'
        }],
    },
    contentBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: 'var(--postx_preset_Contrast_2_color)' },
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content'
        }]
    },
    contentRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content {border-radius: {{contentRadius}}}'
        }]
    },
    // Hover
    contentHvrBg:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5', size: 'cover', repeat: 'no-repeat'},
        style:[{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content:hover'
        }],
    },
    contentHvrBorder:{
        type: 'object',
        default: { openBorder: 0, width:{top: 2, right: 2, bottom: 2, left: 2},color: '#dfdfdf' },
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content:hover'
        }],
    },
    contentHvrRadius:{
        type: 'object',
        default: {lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit:'px'},
        style: [{
            selector: '{{ULTP}} .wp-block-ultimate-post-social .ultp-social-content:hover { border-radius: {{contentHvrRadius}}; }'
        }],
    },
    contentFullWidth: {
        type: 'boolean',
        default: false,
        style: [
            {
                depends: [
                    { key:'iconInline', condition:'==', value: false }
                ],
                selector:
                `{{ULTP}} .ultp-social-icons-wrapper .block-editor-inner-blocks > .block-editor-block-list__layout, 
                {{ULTP}}.wp-block-ultimate-post-social-icons .ultp-social-icons-wrapper { width: 100%; max-width: 100%; }`
            }
        ]
    },
    /*==========================
        General Advance Settings
    ==========================*/
    wrapBg: {
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#f5f5f5'},
        style: [{
            selector:
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout`
        }]
    },
    wrapBorder:{
        type: 'object',
        default: {openBorder:0, width:{top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout` 
        }],
    },
    wrapShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout` 
        }],
    },
    wrapRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { border-radius:{{wrapRadius}}; }` 
        }],
    },
    wrapHoverBackground:{
        type: 'object',
        default: {openColor: 0, type: 'color', color: '#037fff'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social):hover, 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover` 
        }]
    },
    wrapHoverBorder:{
        type: 'object',
        default: {openBorder:0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4',type: 'solid'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social):hover, 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover` 
        }],
    },
    wrapHoverRadius:{
        type: 'object',
        default: {lg:'', unit:'px'},
        style: [{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social):hover, 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover { border-radius:{{wrapHoverRadius}}; }` 
        }],
    },
    wrapHoverShadow:{
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style:[{ 
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social):hover, 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout:hover` 
        }],
    },
    wrapMargin:{
        type: 'object',
        default: {lg:{top: '',bottom: '', unit:'px'}},
        style:[{ 
            selector:
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { margin:{{wrapMargin}}; }` 
        }],
    },
    wrapOuterPadding:{
        type: 'object',
        default: {lg:{top: '',bottom: '',left: '', right: '', unit:'px'}},
        style:[{
            selector: 
            `{{ULTP}} .ultp-social-icons-wrapper:has(> .wp-block-ultimate-post-social), 
            {{ULTP}} .ultp-social-icons-wrapper > .block-editor-inner-blocks .block-editor-block-list__layout { padding:{{wrapOuterPadding}}; }`
        }],
    },
    advanceId:{
        type: 'string',
        default: '',
    },
    advanceZindex:{
        type: 'string',
        default: '',
        style:[{ selector: '{{ULTP}} .ultp-social-icons-wrapper {z-index:{{advanceZindex}};}' }],
    },
    hideExtraLarge: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideTablet: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    hideMobile: {
        type: 'boolean',
        default: false,
        style: [{ selector: '{{ULTP}} {display:none;}' }],
    },
    advanceCss:{
        type: 'string',
        default: '',
        style: [{selector: ''}],
    }
};
export default attributes;