const { InnerBlocks } = wp.blockEditor;

export default function Save(props) {
    
    const { blockId, advanceId, layout } = props.attributes;

    return (
        <div {...(advanceId && {id:advanceId})} className={`ultp-block-${blockId}`}>
            <ul className={`ultp-social-icons-wrapper ultp-social-icons-${layout}`}>
                <InnerBlocks.Content />
            </ul>
        </div>
    )
}
