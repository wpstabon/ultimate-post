const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { useState, useRef, useEffect, Fragment } = wp.element;
const { Spinner, Placeholder } = wp.components;
import {
	attrBuild,
	FeatureToggleArgsDep,
	isInlineCSS,
	isReload,
	updateCurrentPostId,
} from '../../helper/CommonPanel';
import { handleCompatibility } from '../../helper/compatibility';
import Category from '../../helper/Components/grid-category';
import Excerpt from '../../helper/Components/grid-excerpt';
import GHeading from '../../helper/Components/grid-heading';
import Image1 from '../../helper/Components/grid-image';
import LoadMore from '../../helper/Components/grid-loadmore';
import Meta from '../../helper/Components/grid-meta';
import Navigation from '../../helper/Components/grid-nav';
import Pagination from '../../helper/Components/grid-pagination';
import ReadMore from '../../helper/Components/grid-readmore';
import Title from '../../helper/Components/grid-title';
import { CssGenerator } from '../../helper/CssGenerator';
import { isDCActive } from '../../helper/dynamic_content';
import AddDCButton from '../../helper/dynamic_content/AddDCButton';
import MetaGroup from '../../helper/dynamic_content/MetaGroup';
import {
	stateObj,
	SETTING_SECTIONS,
	fetchProducts,
	resetState,
	restoreState,
	saveSelectedSection,
	saveToolbar,
	scrollSidebarSettings,
} from '../../helper/gridFunctions';
import ToolBarElement from '../../helper/ToolBarElement';
import PG1_Settings, {
	AddSettingsToToolbar,
	MAX_CUSTOM_META_GROUPS,
} from './Settings';
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const prevPropsRef = useRef(null);
	const { setAttributes, name, isSelected, clientId, context, attributes, className, attributes: { blockId, advanceId, paginationShow, paginationAjax, headingShow, filterShow, paginationType, navPosition, paginationNav, loadMoreText, paginationText, V4_1_0_CompCheck: { runComp }, previewImg, readMoreIcon, imgCrop, imgCropSmall, gridStyle, excerptLimit, columns, metaStyle, metaShow, catShow, showImage, metaSeparator, titleShow, catStyle, catPosition, titlePosition, excerptShow, showFullExcerpt, imgAnimation, imgOverlayType, imgOverlay, metaList, readMore, readMoreText, metaPosition, layout, customCatColor, onlyCatColor, contentTag, titleTag, showSeoMeta, titleLength, metaMinText, metaAuthorPrefix, titleStyle, metaDateFormat, authorLink, fallbackEnable, vidIconEnable, notFoundMessage, dcEnabled, dcFields, currentPostId } } = props
	const [state, setState] = useState(stateObj);

	useEffect(() => {
		resetState();
		fetchProducts( attributes, state, setState );
	}, []);

	function setSelectedDc(selectedDC) {
		setState({ ...state, selectedDC });
	}

	function selectParentMetaGroup(groupIdx) {
		setSelectedDc(groupIdx);
		setToolbarSettings('dc_group');
	}

	function startMetaFieldOnboarding() {
		setState({
			...state,
			selectedDC: '0,0,1',
			toolbarSettings: 'dc_field',
		});
	}

	function setSection(title) {
		setState({
			...state,
			section: {
				...SETTING_SECTIONS,
				[title]: true,
			},
		});
		scrollSidebarSettings(title);
		saveSelectedSection(title);
	}

	function setToolbarSettings(title) {
		setState(prev => ({ ...prev, toolbarSettings: title }));
		saveToolbar(title);
	}

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		handleCompatibility(props);

		if (!blockId) {
			setAttributes({
				blockId: _client,
			});
			if (ultp_data.archive && ultp_data.archive == 'archive') {
				setAttributes({ queryType: 'archiveBuilder' });
			}
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
	}, [clientId]);

	useEffect(() => {
		const prevAttributes = prevPropsRef.current;
		if (isDCActive() && attributes.dcFields.length === 0) {
			setAttributes({
				dcFields: Array(MAX_CUSTOM_META_GROUPS).fill(undefined),
			});
		}
		if (prevAttributes) {
			if (isReload(prevAttributes, attributes)) {
				fetchProducts( attributes, state, setState );
				prevPropsRef.current = attributes;
			}
			if (prevAttributes.isSelected !== isSelected && isSelected) {
				restoreState(setSection, setToolbarSettings);
			}
		} else {
			prevPropsRef.current = attributes;
		}
	}, [attributes]);

	function renderContent() {
		const CustomTag = `${contentTag}`;
		const colClass =
			gridStyle == 'style1' || gridStyle == 'style2'
				? `ultp-block-column-${columns.lg}`
				: '';

		const customMeta = (idx, postId) => {
			return (
				isDCActive() &&
				dcEnabled && (
					<MetaGroup
						idx={idx}
						postId={postId}
						fields={dcFields}
						settingsOnClick={(e, name) => {
							e?.stopPropagation();
							setToolbarSettings(name);
						}}
						selectedDC={state.selectedDC}
						setSelectedDc={setSelectedDc}
						setAttributes={setAttributes}
						dcFields={dcFields}
					/>
				)
			);
		};

		return !state.error ? (
			!state.loading ? (
				state.postsList.length > 0 ? (
					<div
						className={`ultp-block-items-wrap ultp-block-row ultp-pg1a-${gridStyle} ${colClass} ultp-${layout}`}
					>
						{state.postsList.map((post, idx) => {
							const meta = JSON.parse(
								metaList.replaceAll('u0022', '"')
							);
							const divStyle = {
								backgroundImage:
									post.image && showImage
										? 'url(' + post.image[imgCrop] + ')'
										: '#333',
							};
							const imgSize =
								gridStyle == 'style1' ||
									(gridStyle == 'style2' && idx == 0) ||
									(gridStyle == 'style3' && idx % 3 == 0) ||
									(gridStyle == 'style4' &&
										(idx == 0 || idx == 1))
									? imgCrop
									: imgCropSmall;

							return (
								<CustomTag
									key={idx}
									className={`ultp-block-item post-id-${post.ID}`}
								>
									<div className={`ultp-block-content-wrap`}>
										{customMeta(8, post.ID)}

										{((post.image && !post.is_fallback) ||
											fallbackEnable) &&
											showImage &&
											layout != 'layout2' && (
												<Image1
													catPosition={catPosition}
													imgOverlay={imgOverlay}
													imgOverlayType={
														imgOverlayType
													}
													imgAnimation={imgAnimation}
													post={post}
													imgSize={imgSize}
													fallbackEnable={
														fallbackEnable
													}
													vidIconEnable={
														vidIconEnable
													}
													idx={idx}
													Category={
														catPosition !=
															'aboveTitle' ? (
															<Category
																post={post}
																catShow={
																	catShow
																}
																catStyle={
																	catStyle
																}
																catPosition={
																	catPosition
																}
																customCatColor={
																	customCatColor
																}
																onlyCatColor={
																	onlyCatColor
																}
																onClick={() => {
																	setSection('taxonomy-/-category');
																	setToolbarSettings('cat');
																}}
															/>
														) : null
													}
													onClick={() => {
														setSection('image');
														setToolbarSettings('image');
													}}
													vidOnClick={() => {
														setSection('video');
														setToolbarSettings('video');
													}}
												/>
											)}

										{layout === 'layout2' && (
											<div
												className={`ultp-block-content-image`}
												style={divStyle}
											></div>
										)}

										<div className={`ultp-block-content`}>
											{customMeta(7, post.ID)}

											{catPosition == 'aboveTitle' && (
												<Category
													post={post}
													catShow={catShow}
													catStyle={catStyle}
													catPosition={catPosition}
													customCatColor={
														customCatColor
													}
													onlyCatColor={onlyCatColor}
													onClick={(e) => {
														setSection('taxonomy-/-category');
														setToolbarSettings('cat');
													}}
												/>
											)}

											{customMeta(6, post.ID)}

											{post.title &&
												titleShow &&
												titlePosition == true && (
													<Title
														title={post.title}
														headingTag={titleTag}
														titleLength={
															titleLength
														}
														titleStyle={titleStyle}
														onClick={(e) => {
															setSection('title');
															setToolbarSettings('title');
														}}
													/>
												)}

											{customMeta(5, post.ID)}

											{metaShow &&
												metaPosition == 'top' && (
													<Meta
														meta={meta}
														post={post}
														metaSeparator={
															metaSeparator
														}
														metaStyle={metaStyle}
														metaMinText={
															metaMinText
														}
														metaAuthorPrefix={
															metaAuthorPrefix
														}
														metaDateFormat={
															metaDateFormat
														}
														authorLink={authorLink}
														onClick={(e) => {
															setSection('meta');
															setToolbarSettings('meta');
														}}
													/>
												)}

											{customMeta(4, post.ID)}

											{post.title &&
												titleShow &&
												titlePosition == false && (
													<Title
														title={post.title}
														headingTag={titleTag}
														titleLength={
															titleLength
														}
														titleStyle={titleStyle}
														onClick={(e) => {
															setSection('title');
															setToolbarSettings('title');
														}}
													/>
												)}

											{customMeta(3, post.ID)}

											{excerptShow && (
												<div
													className={`ultp-block-excerpt`}
												>
													<Excerpt
														excerpt={post.excerpt}
														excerpt_full={
															post.excerpt_full
														}
														seo_meta={post.seo_meta}
														excerptLimit={
															excerptLimit
														}
														showFullExcerpt={
															showFullExcerpt
														}
														showSeoMeta={
															showSeoMeta
														}
														onClick={(e) => {
															setSection('excerpt');
															setToolbarSettings('excerpt');
														}}
													/>
												</div>
											)}

											{customMeta(2, post.ID)}

											{readMore && (
												<>
													<ReadMore
														readMoreText={
															readMoreText
														}
														readMoreIcon={
															readMoreIcon
														}
														titleLabel={post.title}
														onClick={() => {
															setSection('read-more');
															setToolbarSettings('read-more');
														}}
													/>
												</>
											)}

											{customMeta(1, post.ID)}

											{metaShow &&
												metaPosition == 'bottom' && (
													<>
														<Meta
															meta={meta}
															post={post}
															metaSeparator={
																metaSeparator
															}
															metaStyle={
																metaStyle
															}
															metaMinText={
																metaMinText
															}
															metaAuthorPrefix={
																metaAuthorPrefix
															}
															metaDateFormat={
																metaDateFormat
															}
															authorLink={
																authorLink
															}
															onClick={(e) => {
																setSection('meta');
																setToolbarSettings('meta');
															}}
														/>
													</>
												)}

											{customMeta(0, post.ID)}

											{isDCActive() && dcEnabled && (
												<AddDCButton
													dcFields={dcFields}
													setAttributes={
														setAttributes
													}
													startOnboarding={
														startMetaFieldOnboarding
													}
												/>
											)}
										</div>
									</div>
								</CustomTag>
							);
						})}
					</div>
				) : (
					<Placeholder
						className={`ultp-backend-block-loading`}
						label={notFoundMessage}
					></Placeholder>
				)
			) : (
				<Placeholder
					className={`ultp-backend-block-loading`}
					label={__('Loading...', 'ultimate-post')}
				>
					<Spinner />
				</Placeholder>
			)
		) : (
			<Placeholder
				label={__('Posts are not available.', 'ultimate-post')}
			>
				<div style={{ marginBottom: 15 }}>
					{__('Make sure Add Post.', 'ultimate-post')}
				</div>
			</Placeholder>
		);
	}

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section: state.section,
		clientId,
		context,

		setSelectedDc,
		selectedDC: state.selectedDC,
		selectParentMetaGroup,
	};

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(
			attributes,
			'ultimate-post/post-grid-1',
			blockId,
			isInlineCSS()
		);
	}

	if (previewImg) {
		return (
			<img
				style={{ marginTop: '0px', width: '420px' }}
				src={previewImg}
			/>
		);
	}

	return (
		<Fragment>
			<AddSettingsToToolbar
				store={store}
				selected={state.toolbarSettings}
			/>
			<InspectorControls>
				<PG1_Settings store={store} />
			</InspectorControls>
			<ToolBarElement
				include={[
					{
						type: 'query',
					},
					{
						type: 'template',
					},
					{
						type: 'grid_align',
						key: 'contentAlign',
					},
					{
						type: 'grid_spacing',
						include: [
							{
								position: 1,
								data: {
									type: 'range',
									key: 'rowSpace',
									min: 0,
									max: 80,
									step: 1,
									responsive: true,
									label: __('Row Gap', 'ultimate-post'),
								},
							},
						],
					},
					{
						type: 'layout+adv_style',

						layoutData: {
							type: 'layout',
							key: 'layout',
							label: __('Layout', 'ultimate-post'),
							pro: true,
							block: 'post-grid-1',
							options: [
								{
									img: 'assets/img/layouts/pg1/l1.png',
									label: 'Layout 1',
									value: 'layout1',
									pro: false,
								},
								{
									img: 'assets/img/layouts/pg1/l2.png',
									label: 'Layout 2',
									value: 'layout2',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pg1/l3.png',
									label: 'Layout 3',
									value: 'layout3',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pg1/l4.png',
									label: 'Layout 4',
									value: 'layout4',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pg1/l5.png',
									label: 'Layout 5',
									value: 'layout5',
									pro: true,
								},
							],
						},

						advStyleData: {
							type: 'layout',
							key: 'gridStyle',
							label: __('Advanced Style', 'ultimate-post'),
							pro: true,
							tab: true,
							block: 'post-grid-1',
							options: [
								{
									img: 'assets/img/layouts/pg1/s1.png',
									label: __('Style 1', 'ultimate-post'),
									value: 'style1',
									pro: false,
								},
								{
									img: 'assets/img/layouts/pg1/s2.png',
									label: __('Style 2', 'ultimate-post'),
									value: 'style2',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pg1/s3.png',
									label: __('Style 3', 'ultimate-post'),
									value: 'style3',
									pro: true,
								},
								{
									img: 'assets/img/layouts/pg1/s4.png',
									label: __('Style 4', 'ultimate-post'),
									value: 'style4',
									pro: true,
								},
							],
						},
					},
					{
						type: 'feat_toggle',
						label: __('Grid Features', 'ultimate-post'),
						[runComp ? 'exclude' : 'dep']: FeatureToggleArgsDep,
					},
				]}
				store={store}
			/>

			<div
				{...(advanceId && { id: advanceId })}
				className={`ultp-block-${blockId} ${className}`}
				onClick={(e) => {
					e.stopPropagation();
					setSection('general');
					setToolbarSettings('');
				}}
			>
				{__preview_css && (
					<style
						dangerouslySetInnerHTML={{ __html: __preview_css }}
					></style>
				)}
				<div className={`ultp-block-wrapper`}>
					{(headingShow || filterShow || paginationShow) && (
						<GHeading
							attributes={attributes}
							setAttributes={setAttributes}
							onClick={() => {
								setSection('heading');
								setToolbarSettings('heading');
							}}
							setSection={setSection}
							setToolbarSettings={setToolbarSettings}
						/>
					)}
					{renderContent()}
					{paginationShow && paginationType == 'loadMore' && (
						<LoadMore
							loadMoreText={loadMoreText}
							onClick={(e) => {
								setSection('pagination');
								setToolbarSettings('pagination');
							}}
						/>
					)}
					{paginationShow &&
						paginationType == 'navigation' &&
						navPosition != 'topRight' && (
							<Navigation
								onClick={() => {
									setSection('pagination');
									setToolbarSettings('pagination');
								}}
							/>
						)}
					{paginationShow && paginationType == 'pagination' && (
						<Pagination
							paginationNav={paginationNav}
							paginationAjax={paginationAjax}
							paginationText={paginationText}
							onClick={(e) => {
								setSection('pagination');
								setToolbarSettings('pagination');
							}}
						/>
					)}
				</div>
			</div>
		</Fragment>
	);
}