import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";
import { V4_1_0_CompCheck } from "../../helper/compatibility";
import { getDCBlockAttributes } from "../../helper/dynamic_content/attributes";

const attributes = {
  blockId: { type: "string", default: "" },
  currentPostId: { type: "string" , default: "" },
  previewImg: { type: "string", default: "" },
  layout: { type: "string", default: "layout1" },
  headingText: { type: "string", default: "Post Grid #1" },
  /*============================
      General Settings
  ============================*/
  gridStyle: { type: "string", default: "style1" },
  columns: {
    type: "object",
    default: { lg: "3", xs: "1", sm: "2" },
    style: [
      {
        depends: [
          { key: "gridStyle", condition: "==", value: ["style1", "style2"] },
        ],
        selector: `{{ULTP}} .ultp-block-items-wrap { grid-template-columns: repeat({{columns}}, 1fr); }`,
      },
    ],
  },
  columnGridGap: {
    type: "object",
    default: { lg: "30", unit: "px" },
    style: [
      {
        selector: "{{ULTP}} .ultp-block-row { grid-column-gap: {{columnGridGap}}; }",
      },
    ],
  },
  rowSpace: {
    type: "object",
    default: { lg: "30" },
    style: [
      {
        depends: [{ key: "separatorShow", condition: "!=", value: true }],
        selector: "{{ULTP}} .ultp-block-row {row-gap: {{rowSpace}}px; }",
      },
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-item { padding-bottom: {{rowSpace}}px; margin-bottom:{{rowSpace}}px; }",
      },
    ],
  },
  equalHeight: {
    type: "boolean",
    default: false,
    style: [
      { 
        selector: `{{ULTP}} .ultp-block-content-wrap { height: 100%; color: red; } 
        {{ULTP}} .ultp-block-content,
        {{ULTP}} .ultp-block-content-wrap { display: flex; flex-direction: column; }
        {{ULTP}} .ultp-block-content { flex-grow: 1; flex: 1; }
        {{ULTP}} .ultp-block-readmore { margin-top: auto;}`
      },
    ],
  },
  contentTag: { type: "string", default: "div" },
  /*============================
      Title Setting/Style Settings
  ============================*/
  //post grid 1
  titleShow: { type: "boolean", default: true },
  titleStyle: { type: "string", default: "none" },
  titleAnimColor: {
    type: "string",
    default: "black",
    style: [
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style1" }],
        selector:
          `{{ULTP}} .ultp-title-style1 a {  
            cursor: pointer; 
            text-decoration: none; 
            display: inline; 
            padding-bottom: 2px; 
            transition: all 0.35s linear !important;
            background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% ); 
            background-size: 0px 2px; 
            background-repeat: no-repeat; 
            background-position: left 100%; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style2" }],
        selector:
          `{{ULTP}} .ultp-title-style2 a:hover {  
            border-bottom:none; 
            padding-bottom: 2px; 
            background-position:0 100%; 
            background-repeat: repeat; 
            background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style3" }],
        selector:
          `{{ULTP}} .ultp-title-style3 a {  
              text-decoration: none; transition: all 1s cubic-bezier(1,.25,0,.75) 0s; 
              position: relative; 
              transition: all 0.35s ease-out; 
              padding-bottom: 3px; 
              border-bottom:none; 
              padding-bottom: 2px; 
              background-position:0 100%; 
              background-repeat: repeat; 
              background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style4" }],
        selector:
          `{{ULTP}} .ultp-title-style4 a { 
            cursor: pointer; 
            font-weight: 600; 
            text-decoration: none; 
            display: inline; padding-bottom: 2px; 
            transition: all 0.3s linear !important; 
            background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% ); 
            background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
            `{{ULTP}} .ultp-title-style5 a:hover { 
              text-decoration: none; 
              transition: all 0.35s ease-out; 
              border-bottom:none; 
              padding-bottom: 2px; 
              background-position:0 100%; 
              background-repeat: repeat; 
              background-size:auto; 
              background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); 
            }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style5" }],
        selector:
          `{{ULTP}} .ultp-title-style5 a { 
            cursor: pointer; 
            text-decoration: none; 
            display: inline; 
            padding-bottom: 2px; 
            transition: all 0.3s linear !important; 
            background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  
            background-size: 100% 2px; 
            background-repeat: no-repeat; background-position: left 100%; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style6" }],
        selector:
          `{{ULTP}} .ultp-title-style6 a { 
            background-image: linear-gradient(120deg, {{titleAnimColor}} 0%, {{titleAnimColor}} 100%); 
            background-repeat: no-repeat; 
            background-size: 100% 2px; background-position: 0 88%; 
            transition: background-size 0.15s ease-in; padding: 5px 5px; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style7" }],
        selector:
          `{{ULTP}} .ultp-title-style7 a { 
            cursor: pointer; 
            text-decoration: none; 
            display: inline; 
            padding-bottom: 2px; 
            transition: all 0.3s linear !important; 
            background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  
            background-size: 0px 2px; 
            background-repeat: no-repeat; 
            background-position: right 100%; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style8" }],
        selector:
          `{{ULTP}} .ultp-title-style8 a {
            cursor: pointer; 
            text-decoration: none; 
            display: inline; 
            padding-bottom: 2px; 
            transition: all 0.3s linear !important; 
            background: linear-gradient( to bottom, {{titleAnimColor}} 0%, {{titleAnimColor}} 98% );  
            background-size: 0px 2px; background-repeat: no-repeat; background-position: center 100%; 
          }`,
      },
      {
        depends: [{ key: "titleStyle", condition: "==", value: "style9" }],
        selector:
          `{{ULTP}} .ultp-title-style9 a {
            background-image: linear-gradient(120deg,{{titleAnimColor}} 0%, {{titleAnimColor}} 100%); 
            background-repeat: no-repeat; 
            background-size: 100% 10px; 
            background-position: 0 88%; 
            transition: 0.3s ease-in; padding: 3px 5px; 
          } `,
      },
    ],
  },
  headingShow: { type: "boolean", default: true },
  excerptShow: { type: "boolean", default: true },
  showImage: { type: "boolean", default: true },
  filterShow: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [
          {
            key: "queryType",
            condition: "!=",
            value: ["posts", "customPosts"],
          },
        ],
      },
    ],
  },
  paginationShow: { type: "boolean", default: true },
  openInTab: { type: "boolean", default: false },
  notFoundMessage: { type: "string", default: "No Post Found" },

  /*============================
      Title Setting/Style Settings
  ============================*/
  titleTag: { type: "string", default: "h3" },
  titlePosition: { type: "boolean", default: true },
  titleColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-content .ultp-block-title a { color:{{titleColor}} !important; }",
      },
    ],
  },
  titleHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-content .ultp-block-title a:hover { color:{{titleHoverColor}} !important; }",
      },
    ],
  },
  titleTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: "22", unit: "px" },
      spacing: { lg: "0", unit: "px" },
      height: { lg: "26", unit: "px" },
      transform: "",
      decoration: "none",
      family: "",
      weight: "500",
    },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-title, 
          {{ULTP}} div.ultp-block-wrapper .ultp-block-items-wrap .ultp-block-item .ultp-block-content .ultp-block-title a`,
      },
    ],
  },
  titlePadding: {
    type: "object",
    default: { lg: { top: 10, bottom: 5, unit: "px" } },
    style: [
      {
        depends: [{ key: "titleShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-title { padding:{{titlePadding}}; }",
      },
    ],
  },
  titleLength: { type: "string", default: 0 },
  /*============================
      Image Style Settings
  ============================*/
  imgCrop: {
    type: "string",
    default: (ultp_data.disable_image_size == 'yes' ? "full" : "ultp_layout_landscape"),
    depends: [{ key: "showImage", condition: "==", value: true }],
  },
  imgCropSmall: {
    type: "string",
    default: (ultp_data.disable_image_size == 'yes' ? "full" : "ultp_layout_square"),
    style: [
      {
        depends: [
          { key: "showImage", condition: "==", value: true },
          { key: "gridStyle", condition: "!=", value: "style1" },
        ],
      },
    ],
  },
  imgWidth: {
    type: "object",
    default: { lg: "", ulg: "%" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          // `{{ULTP}} .ultp-block-image { max-width: {{imgWidth}}; width: 100%; } 
          // {{ULTP}} .ultp-block-image img.ultp-block-image-content { width: 100% }`,
          `{{ULTP}} .ultp-block-image { max-width: {{imgWidth}}; width: 100%; }
          {{ULTP}} .ultp-block-item .ultp-block-image img { width: 100% }`,
      },
    ],
  },
  imgHeight: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-block-image img { height: {{imgHeight}} !important; }",
      },
    ],
  },
  imageScale: {
    type: "string",
    default: "cover",
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item .ultp-block-image img {object-fit: {{imageScale}};}",
      },
    ],
  },
  imgAnimation: { type: "string", default: "opacity" },
  imgGrayScale: {
    type: "object",
    default: { lg: "0", ulg: "%", unit: "%" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-image { filter: grayscale({{imgGrayScale}}); }",
      },
    ],
  },
  imgHoverGrayScale: {
    type: "object",
    default: { lg: "0", unit: "%" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image { filter: grayscale({{imgHoverGrayScale}}); }",
      },
    ],
  },
  imgRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-image { border-radius:{{imgRadius}}; }",
      },
    ],
  },
  imgHoverRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item:hover .ultp-block-image { border-radius:{{imgHoverRadius}}; }",
      },
    ],
  },
  imgShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-image",
      },
    ],
  },
  imgHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-item:hover .ultp-block-image",
      },
    ],
  },
  imgSpacing: {
    type: "object",
    default: { lg: "10" },
    style: [
      {
        depends: [{ key: "showImage", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-image { margin-bottom: {{imgSpacing}}px; }",
      },
    ],
  },
  imgOverlay: { type: "boolean", default: false },
  imgOverlayType: {
    type: "string",
    default: "default",
    style: [{ depends: [{ key: "imgOverlay", condition: "==", value: true }] }],
  },
  overlayColor: {
    type: "object",
    default: { openColor: 1, type: "color", color: "#0e1523" },
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector: "{{ULTP}} .ultp-block-image-custom > a::before",
      },
    ],
  },
  imgOpacity: {
    type: "string",
    default: 0.69999999999999996,
    style: [
      {
        depends: [{ key: "imgOverlayType", condition: "==", value: "custom" }],
        selector:
          "{{ULTP}} .ultp-block-image-custom > a::before { opacity: {{imgOpacity}}; }",
      },
    ],
  },
  fallbackEnable: {
    type: "boolean",
    default: true,
    style: [{ depends: [{ key: "showImage", condition: "==", value: true }] }],
  },
  fallbackImg: {
    type: "object",
    default: "",
    style: [
      {
        depends: [
          { key: "showImage", condition: "==", value: true },
          { key: "fallbackEnable", condition: "==", value: true },
        ],
      },
    ],
  },
  imgSrcset: { type: "boolean", default: false },
  imgLazy: { type: "boolean", default: false },
 
  /*============================
      Excerpt Setting/Style Settings
  ============================*/
  showSeoMeta: { type: "boolean", default: false },
  showFullExcerpt: { 
    type: "boolean", 
    default: false,
    style: [
      {
        depends: [{ key: "showSeoMeta", condition: "==", value: false }]
      }
    ]
  },
  excerptLimit: {
    type: "string",
    default: 10,
    style: [
      { depends: [{ key: "showFullExcerpt", condition: "==", value: false }] },
    ],
  },
  excerptColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector: `{{ULTP}} .ultp-block-excerpt, {{ULTP}} .ultp-block-excerpt p { color:{{excerptColor}}; }`,
      },
    ],
  },
  excerptTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 26, unit: "px" },
      decoration: "none",
      family: "",
    },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt, {{ULTP}} .ultp-block-excerpt p",
      },
    ],
  },
  excerptPadding: {
    type: "object",
    default: { lg: { top: 10, bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "excerptShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-excerpt{ padding: {{excerptPadding}}; }",
      },
    ],
  },

  /*============================
      Inner Content Setting/Style Settings
  ============================*/
  contentWidth: {
    type: "object",
    default: { lg: "85" },
    style: [{
        depends: [
          { key: "layout", condition: "!=", value: ["layout1", "layout2"] }
        ],
        selector: 
        `{{ULTP}} .ultp-layout3 .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout4 .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout5 .ultp-block-content-wrap .ultp-block-content { max-width:{{contentWidth}}% !important;  }`,
    }],
  },
  contentShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{
      depends: [
        { key: "layout", condition: "!=", value: ["layout1", "layout2"] }
      ],
      selector: 
        `{{ULTP}} .ultp-layout3 .ultp-block-content-wrap, 
        {{ULTP}} .ultp-layout4 .ultp-block-content-wrap, 
        {{ULTP}} .ultp-layout5 .ultp-block-content-wrap { overflow: visible;  } 
        {{ULTP}} .ultp-layout3 .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout4  .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout5 .ultp-block-content-wrap .ultp-block-content`,
      },
    ],
  },
  contentRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [{
        depends: [
          { key: "layout", condition: "!=", value: ["layout1", "layout2"] }
        ],
        selector: 
        `{{ULTP}} .ultp-layout3 .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout4 .ultp-block-content-wrap .ultp-block-content, 
        {{ULTP}} .ultp-layout5 .ultp-block-content-wrap .ultp-block-content { border-radius:{{contentRadius}}; }`,
    }],
  },
  contentAlign: {
    type: "string",
    default: "center",
    style: [
      {
        depends: [{ key: "contentAlign", condition: "==", value: "left" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: flex-start;} 
          {{ULTP}} .ultp-block-image img, 
          {{ULTP}} .ultp-block-image { margin-right: auto; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "center" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: center;} 
          {{ULTP}} .ultp-block-image img, 
          {{ULTP}} .ultp-block-image { margin: 0 auto; }`,
      },
      {
        depends: [{ key: "contentAlign", condition: "==", value: "right" }],
        selector:
          `{{ULTP}} .ultp-block-content { text-align:{{contentAlign}}; } 
          {{ULTP}} .ultp-block-meta {justify-content: flex-end;} 
          .rtl {{ULTP}} .ultp-block-meta {justify-content: start;} 
          {{ULTP}} .ultp-block-image img, 
          {{ULTP}} .ultp-block-image { margin-left: auto; }`,
      },
      {
        depends: [
          { key: "contentAlign", condition: "==", value: "right" },
          { key: "readMore", condition: "==", value: true },
        ],
        selector:
          `.rtl {{ULTP}} .ultp-block-readmore a { display:flex; flex-direction:row-reverse;justify-content: flex-end; }`,
      },
    ],
  },
  contentWrapBg: {
    type: "string",
    default: "",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-wrap { background:{{contentWrapBg}}; }",
      },
    ],
  },
  contentWrapHoverBg: {
    type: "string",
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-wrap:hover { background:{{contentWrapHoverBg}}; }",
      },
    ],
  },
  contentWrapBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-wrap" }],
  },
  contentWrapHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-wrap:hover" }],
  },
  contentWrapRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-wrap { border-radius: {{contentWrapRadius}}; }",
      },
    ],
  },
  contentWrapHoverRadius: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-wrap:hover { border-radius: {{contentWrapHoverRadius}}; }",
      },
    ],
  },
  contentWrapShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-wrap" }],
  },
  contentWrapHoverShadow: {
    type: "object",
    default: {
      openShadow: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
    },
    style: [{ selector: "{{ULTP}} .ultp-block-content-wrap:hover" }],
  },
  innerBgColor: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        depends: [
          { key: "layout", condition: "!=", value: "layout1" },
          { key: "layout", condition: "!=", value: "layout2" }
        ],
        selector:
          `{{ULTP}} .ultp-layout3 .ultp-block-content-wrap .ultp-block-content,
          {{ULTP}} .ultp-layout4 .ultp-block-content-wrap .ultp-block-content, 
          {{ULTP}} .ultp-layout5 .ultp-block-content-wrap .ultp-block-content { background:{{innerBgColor}}; }`,
      },
    ],
  },
  contentWrapInnerPadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        selector:
          `{{ULTP}} .ultp-block-content, 
          {{ULTP}} .ultp-layout2 .ultp-block-content, 
          {{ULTP}} .ultp-layout3 .ultp-block-content { padding: {{contentWrapInnerPadding}}; }`,
      },
    ],
  },
  contentWrapPadding: {
    type: "object",
    default: { lg: { unit: "px" } },
    style: [
      {
        selector:
          "{{ULTP}} .ultp-block-content-wrap { padding: {{contentWrapPadding}}; }",
      },
    ],
  },

  /*============================
      Separator Style Settings
  ============================*/            
  separatorShow: { type: "boolean", default: true },
  septColor: {
    type: "string",
    default: "var(--postx_preset_Base_3_color)",
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item { border-bottom-color:{{septColor}}; }",
      },
    ],
  },
  septStyle: {
    type: "string",
    default: "dashed",
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item { border-bottom-style:{{septStyle}}; }",
      },
    ],
  },
  septSize: {
    type: "string",
    default: { lg: "1" },
    style: [
      {
        depends: [{ key: "separatorShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-block-item { border-bottom-width: {{septSize}}px; }",
      },
    ],
  },

/*============================
    Filter Setting/Style
============================*/
  filterBelowTitle: {
    type: "boolean",
    default: false,
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-navigation { 
              position: relative; display: block; margin: auto 0 0 0; height: auto;
          }`,
      },
    ],
  },
  filterAlign: {
    type: "object",
    default: { lg: "" },
    style: [
      {
        depends: [{ key: "filterBelowTitle", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-navigation { text-align:{{filterAlign}}; }",
      },
    ],
  },
  filterType: { type: "string", default: "category" },
  filterText: { type: "string", default: "all" },
  filterValue: {
    type: "string",
    default: "[]",
    style: [{ depends: [{ key: "filterType", condition: "!=", value: "" }] }],
  },
  fliterTypo: {
    type: "object",
    default: {
      openTypography: 1,
      size: { lg: 14, unit: "px" },
      height: { lg: 22, unit: "px" },
      decoration: "none",
      family: "",
      weight: 500,
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a",
      },
    ],
  },
  filterColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_2_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-navigation .ultp-filter-wrap ul li a { color:{{filterColor}}; } 
          {{ULTP}} .flexMenu-viewMore a:before { border-color:{{filterColor}}}`,
      },
    ],
  },
  filterHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-wrap ul li a:hover, 
          {{ULTP}} .ultp-filter-wrap ul li a.filter-active { color:{{filterHoverColor}} !important; } 
          {{ULTP}} .ultp-flex-menu .flexMenu-viewMore a:hover::before { border-color:{{filterHoverColor}}}`,
      },
    ],
  },
  filterBgColor: {
    type: "string",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a { background:{{filterBgColor}}; }",
      },
    ],
  },
  filterHoverBgColor: {
    type: "string",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-wrap ul li.filter-item > a:hover, 
          {{ULTP}} .ultp-filter-wrap ul li.filter-item > a.filter-active, 
          {{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore > a:hover { background:{{filterHoverBgColor}}; }`,
      },
    ],
  },
  filterBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a",
      },
    ],
  },
  filterHoverBorder: {
    type: "object",
    default: {
      openBorder: 0,
      width: { top: 1, right: 1, bottom: 1, left: 1 },
      color: "#009fd4",
      type: "solid",
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a:hover",
      },
    ],
  },
  filterRadius: {
    type: "object",
    default: { lg: "", unit: "px" },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.filter-item > a { border-radius:{{filterRadius}}; }",
      },
    ],
  },
  fliterSpacing: {
    type: "object",
    default: { lg: { top: "", bottom: "", right: "", left: "20", unit: "px" } },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li { margin:{{fliterSpacing}}; }",
      },
    ],
  },
  fliterPadding: {
    type: "object",
    default: { lg: { top: "", bottom: "", unit: "px" } },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          `{{ULTP}} .ultp-filter-wrap ul li.filter-item > a, 
          {{ULTP}} .ultp-filter-wrap .flexMenu-viewMore > a { padding:{{fliterPadding}}; }`,
      },
    ],
  },
  filterDropdownColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a { color:{{filterDropdownColor}}; }",
      },
    ],
  },
  filterDropdownHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a:hover { color:{{filterDropdownHoverColor}}; }",
      },
    ],
  },
  filterDropdownBg: {
    type: "string",
    default: "var(--postx_preset_Base_2_color)",
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { background:{{filterDropdownBg}}; }",
      },
    ],
  },
  filterDropdownRadius: {
    type: "object",
    default: { lg: "0" },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { border-radius:{{filterDropdownRadius}}; }",
      },
    ],
  },
  filterDropdownPadding: {
    type: "object",
    default: {
      lg: { top: "15", bottom: "15", left: "20", right: "20", unit: "px" },
    },
    style: [
      {
        depends: [{ key: "filterShow", condition: "==", value: true }],
        selector:
          "{{ULTP}} .ultp-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { padding:{{filterDropdownPadding}}; }",
      },
    ],
  },
  filterMobile: { type: "boolean", default: true },
  filterMobileText: {
    type: "string",
    default: "More",
    style: [
      { depends: [{ key: "filterMobile", condition: "==", value: true }] },
    ],
  },
  /*============================
      Pagination Setting/Style Settings
  ============================*/
  paginationType: { type: "string", default: "pagination" },
  enhancedPaginationEnabled: {
    type: "boolean",
    default: true,
  },

  /*============================
    Wrapper Style Settings
  ============================*/
  ...commonAttributes([
    'advFilter', 
    'heading', // deprecated
    'advanceAttr', 
    'pagination', // deprecated
    'video',
    'meta',
    'category',
    'readMore',
    'query'
  ],
  [],
  [
    {
      key: 'vidIconPosition',
      default: 'center'
    },
    {
      key: 'iconSize',
      default: { lg: "80", sm: "50", xs: "50", unit: "px" }
    },
    // Category
    {
      key: 'catLineColor',
      default: "var(--postx_preset_Primary_color)",
    },
    {
        key: 'catLineHoverColor',
        default: "var(--postx_preset_Secondary_color)",
    },
    {
      key: 'catTypo',
      default: {
          openTypography: 1,
          size: { lg: 14, unit: "px"},
          height: { lg: 20, unit: "px" },
          spacing: { lg: 0, unit: "px" },
          transform: "",
          weight: "400",
          decoration: "none",
          family: "",
      },
    },
    {
        key: 'catColor',
        default: "var(--postx_preset_Over_Primary_color)",
    },
    {
      key: 'catBgColor',
      default: { openColor: 1, type: "color", color: 'var(--postx_preset_Primary_color)' }
    },
    {
      key: 'catHoverColor',
      default: 'var(--postx_preset_Over_Primary_color)'
    },
    {
        key: 'catBgHoverColor',
        default: { 
            openColor: 1,
            type: 'color',
            color: "var(--postx_preset_Secondary_color)" 
        },
    },
    {
        key: 'catSacing',
        default: { lg:{ top: 10, bottom: 5, unit: 'px'}, unit: 'px' }
    },
    // read more 
    {
      key: 'readMoreColor',
      default: "var(--postx_preset_Primary_color)",
    },
    {
      key: 'readMoreBgColor',
      default: { openColor: 0, type: "color", color: "var(--postx_preset_Base_2_color)" },
    },
    {
      key: 'readMoreHoverColor',
      default: "var(--postx_preset_Secondary_color)",
    },
    {
      key: 'readMoreBgHoverColor',
      default: { openColor: 0, type: "color", color: "" },
    },
    {
      key: 'readMorePadding',
      default: { lg: { top: "2", bottom: "2", left: "10", right: "8", unit: "px" } },
    }
  ]
  ),
  ...getDCBlockAttributes({}),
  
  // Compatibility
  V4_1_0_CompCheck,
  
  // loadMoreText: {
  //   type: "string",
  //   default: "Load More",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "loadMore" },
  //       ],
  //     },
  //   ],
  // },
  // paginationText: {
  //   type: "string",
  //   default: "Previous|Next",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // paginationAjax: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // paginationNav: {
  //   type: "string",
  //   default: "textArrow",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "pagination" },
  //       ],
  //     },
  //   ],
  // },
  // navPosition: {
  //   type: "string",
  //   default: "topRight",
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //     },
  //   ],
  // },
  // pagiAlign: {
  //   type: "object",
  //   default: { lg: "center" },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-loadmore, 
  //         {{ULTP}} .ultp-next-prev-wrap ul, 
  //         {{ULTP}} .ultp-pagination, 
  //         {{ULTP}} .ultp-pagination-wrap { text-align:{{pagiAlign}}; }`,
  //     },
  //   ],
  // },
  // pagiTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 14, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiArrowSize: {
  //   type: "object",
  //   default: { lg: "14" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-next-prev-wrap ul li a svg { width:{{pagiArrowSize}}px; }",
  //     },
  //   ],
  // },
  // pagiColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action { color:{{pagiColor}}; } 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a svg { fill:{{pagiColor}}; } 
  //         {{ULTP}} .ultp-pagination svg, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action svg { fill:{{pagiColor}}; }`,
  //     },
  //   ],
  // },
  // pagiBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a,
  //         {{ULTP}} .ultp-next-prev-wrap ul li a,
  //         {{ULTP}} .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiShadow: {
  //   type: "object",
  //   default: {
  //     openShadow: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a,
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action`,
  //     },
  //   ],
  // },
  // pagiRadius: {
  //   type: "object",
  //   default: {
  //     lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action { border-radius:{{pagiRadius}}; }`,
  //     },
  //   ],
  // },
  // pagiHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore-action:hover { color:{{pagiHoverColor}}; } 
  //         {{ULTP}} .ultp-pagination li a:hover svg { fill:{{pagiHoverColor}}; } 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover svg, 
  //         {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action:hover svg { fill:{{pagiHoverColor}}; } 
  //         @media (min-width: 768px) { 
  //           {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover { color:{{pagiHoverColor}};}
  //         }`,
  //     },
  //   ],
  // },
  // pagiHoverbg: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)", replace: 1 },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:focus, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover{ {{pagiHoverbg}} }`,
  //     },
  //   ],
  // },
  // pagiHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover`,
  //     },
  //   ],
  // },
  // pagiHoverShadow: {
  //   type: "object",
  //   default: {
  //     openShadow: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover`,
  //     },
  //   ],
  // },
  // pagiHoverRadius: {
  //   type: "object",
  //   default: {
  //     lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li.pagination-active a, 
  //         {{ULTP}} .ultp-pagination li a:hover, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
  //         {{ULTP}} .ultp-loadmore-action:hover { border-radius:{{pagiHoverRadius}}; }`,
  //     },
  //   ],
  // },
  // pagiPadding: {
  //   type: "object",
  //   default: {
  //     lg: { top: "8", bottom: "8", left: "14", right: "14", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "paginationShow", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-pagination li a, 
  //         {{ULTP}} .ultp-next-prev-wrap ul li a, 
  //         {{ULTP}} .ultp-loadmore-action { padding:{{pagiPadding}}; }`,
  //     },
  //   ],
  // },
  // navMargin: {
  //   type: "object",
  //   default: {
  //     lg: { top: "0", right: "0", bottom: "0", left: "0", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "==", value: "navigation" },
  //       ],
  //       selector: "{{ULTP}} .ultp-next-prev-wrap ul { margin:{{navMargin}}; }",
  //     },
  //   ],
  // },
  // pagiMargin: {
  //   type: "object",
  //   default: {
  //     lg: { top: "30", right: "0", bottom: "0", left: "0", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "paginationType", condition: "!=", value: "navigation" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-pagination-wrap .ultp-pagination, 
  //         {{ULTP}} .ultp-loadmore { margin:{{pagiMargin}}; }`,
  //     },
  //   ],
  // },
  
  /*============================
      Video Settings
  ============================*/
  // post grid 1
  // vidIconPosition: {
  //   type: "string",
  //   default: "center",
  //   style: [
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "bottomRight" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; right: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "center" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon {  
  //         margin: 0 auto; 
  //         position: absolute; 
  //         top: 50%; 
  //         left: 50%; 
  //         transform: translate(-50%,-60%); 
  //         -o-transform: translate(-50%,-60%); 
  //         -ms-transform: translate(-50%,-60%); 
  //         -moz-transform: translate(-50%,-60%); 
  //         -webkit-transform: translate(-50%,-50%); 
  //         z-index: 998;}`,
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "bottomLeft" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; left: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "topRight" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { top: 20px; right: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "topLeft" },
  //       ],
  //       selector: "{{ULTP}} .ultp-video-icon { top: 20px; left: 20px; }",
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "rightMiddle" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon { display: flex; justify-content: flex-end; align-items: center; height: 100%; width: 100%; top:0px;}`,
  //     },
  //     {
  //       depends: [
  //         { key: "vidIconEnable", condition: "==", value: true },
  //         { key: "vidIconPosition", condition: "==", value: "leftMiddle" },
  //       ],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon {
  //           display: flex; justify-content: flex-start; align-items: center; height: 100%; width: 100%; top: 0px;
  //         }`,
  //     },
  //   ],
  // },
  // iconSize: {
  //   type: "object",
  //   default: { lg: "80", sm: "50", xs: "50", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-video-icon svg { height:{{iconSize}}; width: {{iconSize}};}",
  //     },
  //   ],
  // },

  // vidIconEnable: { type: "boolean", default: true },
  // popupAutoPlay: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     { depends: [{ key: "enablePopup", condition: "==", value: true }] },
  //   ],
  // },
  // popupIconColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon svg { fill: {{popupIconColor}}; } 
  //         {{ULTP}} .ultp-video-icon svg circle { stroke: {{popupIconColor}}; }`,
  //     },
  //   ],
  // },
  // popupHovColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "vidIconEnable", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-video-icon svg:hover { fill: {{popupHovColor}}; } 
  //         {{ULTP}} .ultp-video-icon svg:hover circle { stroke: {{popupHovColor}};}`,
  //     },
  //   ],
  // },
  // enablePopup: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "vidIconEnable", condition: "==", value: true }] },
  //   ],
  // },
  // popupWidth: {
  //   type: "object",
  //   default: { lg: "70" },
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-video-modal__Wrapper {width:{{popupWidth}}% !important;}",
  //     },
  //   ],
  // },
  // enablePopupTitle: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     { depends: [{ key: "enablePopup", condition: "==", value: true }] },
  //   ],
  // },
  // popupTitleColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [
  //         { key: "enablePopupTitle", condition: "==", value: true },
  //         { key: "enablePopup", condition: "==", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-video-modal__Wrapper a { color:{{popupTitleColor}} !important; }",
  //     },
  //   ],
  // },
  // closeIconSep: {
  //   type: "string",
  //   // default: "#fff",
  //   style: [
  //     { depends: [{ key: "enablePopup", condition: "==", value: true }] },
  //   ],
  // },
  // closeIconColor: {
  //   type: "string",
  //   default: "#fff",
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-video-close { color:{{closeIconColor}}; }",
  //     },
  //   ],
  // },
  // closeHovColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-video-close:hover { color:{{closeHovColor}}; }",
  //     },
  //   ],
  // },
  // closeSize: {
  //   type: "object",
  //   default: { lg: "70", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "enablePopup", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-video-close { font-size:{{closeSize}}; }",
  //     },
  //   ],
  // },
  
  /*============================
      Category Settings
  ============================*/
  // post grid 1
  // catShow: { type: "boolean", default: true },
  // maxTaxonomy: { type: "string", default: "30" },
  // taxonomy: { type: "string", default: "category" },
  // catStyle: { type: "string", default: "classic" },
  // catPosition: { type: "string", default: "aboveTitle" },
  // customCatColor: { type: "boolean", default: false },
  // seperatorLink: {
  //   type: "string",
  //   default: ultp_data.category_url,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // onlyCatColor: {
  //   type: "boolean",
  //   default: false,
  //   style: [
  //     { depends: [{ key: "customCatColor", condition: "==", value: true }] },
  //   ],
  // },
  // catLineWidth: {
  //   type: "object",
  //   default: { lg: "20" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after,
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before { width:{{catLineWidth}}px; }`,
  //     },
  //   ],
  // },
  // catLineSpacing: {
  //   type: "object",
  //   default: { lg: "30" },
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth .ultp-category-in { padding-left: {{catLineSpacing}}px; padding-right:{{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in { padding-left: {{catLineSpacing}}px; } 
  //         {{ULTP}} .ultp-category-borderRight .ultp-category-in { padding-right:{{catLineSpacing}}px; }`,
  //     },
  //   ],
  // },
  // catLineColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before { background:{{catLineColor}}; }`,
  //     },
  //   ],
  // },
  // catLineHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Secondary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
  //       selector:
  //         `{{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:after, 
  //         {{ULTP}} .ultp-category-borderLeft:hover .ultp-category-in:before, 
  //         {{ULTP}} .ultp-category-borderRight:hover .ultp-category-in:before { background:{{catLineHoverColor}}; }`,
  //     },
  //   ],
  // },
  // catTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 14, unit: "px" },
  //     height: { lg: 20, unit: "px" },
  //     spacing: { lg: 0, unit: "px" },
  //     transform: "",
  //     weight: "400",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
  //     },
  //   ],
  // },
  // catBgColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a",
  //     },
  //   ],
  // },
  // catRadius: {
  //   type: "object",
  //   default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit: "px" },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
  //     },
  //   ],
  // },
  // catHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Over_Primary_color)",
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
  //     },
  //   ],
  // },
  // catBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)" },
  //   style: [{
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     }/* ,
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "==", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     }, */
  //   ],
  // },
  // catHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a:hover",
  //     },
  //   ],
  // },
  // catSacing: {
  //   type: "object",
  //   default: { lg: { top: 10, bottom: 5, left: 0, right: 0, unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "catShow", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-category-grid { margin:{{catSacing}}; }",
  //     },
  //   ],
  // },
  // catPadding: {
  //   type: "object",
  //   default: {
  //     lg: { top: "2", bottom: "2", left: "4", right: "4", unit: "px" },
  //   },
  //   style: [
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "onlyCatColor", condition: "!=", value: true },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //     {
  //       depends: [
  //         { key: "catShow", condition: "==", value: true },
  //         { key: "customCatColor", condition: "==", value: false },
  //       ],
  //       selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
  //     },
  //   ],
  // },
  

  /*============================
      Read More Style Settings
  ============================*/
  // // Post Grid 1
  // readMore: { type: "boolean", default: false },
  // readMoreText: { type: "string", default: "" },
  // readMoreIcon: { type: "string", default: "rightArrowLg" },
  // readMoreTypo: {
  //   type: "object",
  //   default: {
  //     openTypography: 1,
  //     size: { lg: 12, unit: "px" },
  //     height: { lg: "", unit: "px" },
  //     spacing: { lg: 1, unit: "px" },
  //     transform: "",
  //     weight: "400",
  //     decoration: "none",
  //     family: "",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreIconSize: {
  //   type: "object",
  //   default: { lg: "", unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore svg { width:{{readMoreIconSize}}; }",
  //     },
  //   ],
  // },
  // readMoreColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Primary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { color:{{readMoreColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a svg { fill:{{readMoreColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "var(--postx_preset_Base_2_color)" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a",
  //     },
  //   ],
  // },
  // readMoreRadius: {
  //   type: "object",
  //   default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         "{{ULTP}} .ultp-block-readmore a { border-radius:{{readMoreRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreHoverColor: {
  //   type: "string",
  //   default: "var(--postx_preset_Secondary_color)",
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector:
  //         `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a:hover { color:{{readMoreHoverColor}}; } 
  //         {{ULTP}} .ultp-block-readmore a:hover svg { fill:{{readMoreHoverColor}}; }`,
  //     },
  //   ],
  // },
  // readMoreBgHoverColor: {
  //   type: "object",
  //   default: { openColor: 0, type: "color", color: "" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverBorder: {
  //   type: "object",
  //   default: {
  //     openBorder: 0,
  //     width: { top: 1, right: 1, bottom: 1, left: 1 },
  //     color: "#009fd4",
  //     type: "solid",
  //   },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover",
  //     },
  //   ],
  // },
  // readMoreHoverRadius: {
  //   type: "object",
  //   default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit: "px" },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }",
  //     },
  //   ],
  // },
  // readMoreSacing: {
  //   type: "object",
  //   default: { lg: { top: 15, bottom: "", left: "", right: "", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore { margin:{{readMoreSacing}}; }",
  //     },
  //   ],
  // },
  // readMorePadding: {
  //   type: "object",
  //   default: { lg: { top: "2", bottom: "2", left: "10", right: "8", unit: "px" } },
  //   style: [
  //     {
  //       depends: [{ key: "readMore", condition: "==", value: true }],
  //       selector: "{{ULTP}} .ultp-block-readmore a { padding:{{readMorePadding}}; }",
  //     },
  //   ],
  // },
  /*============================
      Query Settings
  ============================*/
  // post grid 1
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 6 }
  // },
  // queryNumber: {
  //   type: "string",
  //   default: 4,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // metaKey: {
  //   type: "string",
  //   default: "custom_meta_key",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrder: { type: "string", default: "desc" },
  // queryInclude: { type: "string", default: "" },
  // queryExclude: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },
  /*============================
      Query Settings
  ============================*/
  // queryQuick: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryNumPosts: {
  //   type: "object",
  //   default: { lg: 6 }
  // },
  // queryNumber: {
  //   type: "string",
  //   default: 4,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryType: { type: "string", default: "post" },
  // queryTax: {
  //   type: "string",
  //   default: "category",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryTaxValue: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTax", condition: "!=", value: "" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryRelation: {
  //   type: "string",
  //   default: "OR",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryTaxValue", condition: "!=", value: "[]" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrderBy: { type: "string", default: "date" },
  // metaKey: {
  //   type: "string",
  //   default: "custom_meta_key",
  //   style: [
  //     {
  //       depends: [
  //         { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOrder: { type: "string", default: "desc" },
  // queryInclude: { type: "string", default: "" },
  // queryExclude: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryOffset: {
  //   type: "string",
  //   default: "0",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeTerm: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryExcludeAuthor: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // querySticky: {
  //   type: "boolean",
  //   default: true,
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts", "archiveBuilder"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryUnique: {
  //   type: "string",
  //   default: "",
  //   style: [
  //     {
  //       depends: [
  //         {
  //           key: "queryType",
  //           condition: "!=",
  //           value: ["customPosts", "posts"],
  //         },
  //       ],
  //     },
  //   ],
  // },
  // queryPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
  //   ],
  // },
  // queryCustomPosts: {
  //   type: "string",
  //   default: "[]",
  //   style: [
  //     {
  //       depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
  //     },
  //   ],
  // },

};


export default attributes;
