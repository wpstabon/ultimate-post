import {
    CommonSettings,
    getPagiToolbarSettings,
    spaceingIcon,
    ToolbarSettingsAndStyles,
    TypographyTB,
} from "../../helper/CommonPanel";
const { BlockControls } = wp.blockEditor;
const { Toolbar } = wp.components;
import ExtraToolbarSettings from "../../helper/Components/ExtraToolbarSettings";
import { pagiAjaxDisableText } from "../../helper/gridFunctions";
import { Section, Sections } from "../../helper/Sections";

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;

export const pagiSettings = [
    "paginationType",
    "loadMoreText",
    "paginationText",
    "pagiAlign",
    "paginationNav",
    "paginationAjax",
];

export const pagiStyles = [
    "pagiTypo",
    "pagiArrowSize",
    "pagiTab",
    "pagiMargin",
    "navMargin",
    "pagiPadding",
];

const PagiSettingsArgs = [
    {
        data: {
            type: "tag",
            key: "paginationType",
            label: __("Pagination Type", "ultimate-post"),
            options: [
                {
                    value: "loadMore",
                    label: __("Loadmore", "ultimate-post"),
                },
                {
                    value: "navigation",
                    label: __("Navigation", "ultimate-post"),
                },
                {
                    value: "pagination",
                    label: __("Pagination", "ultimate-post"),
                },
            ],
        },
    },
    {
        data: {
            type: "text",
            key: "loadMoreText",
            label: __("Loadmore Text", "ultimate-post"),
        },
    },
    {
        data: {
            type: "text",
            key: "paginationText",
            label: __("Pagination Text", "ultimate-post"),
        },
    },
    {
        data: {
            type: "alignment",
            key: "pagiAlign",
            responsive: true,
            label: __("Alignment", "ultimate-post"),
            disableJustify: true,
        },
    },
    {
        data: {
            type: "select",
            key: "paginationNav",
            label: __("Pagination", "ultimate-post"),
            options: [
                {
                    value: "textArrow",
                    label: __("Text & Arrow", "ultimate-post"),
                },
                {
                    value: "onlyarrow",
                    label: __("Only Arrow", "ultimate-post"),
                },
            ],
        },
    },
    {
        data: {
            type: "toggle",
            key: "paginationAjax",
            label: __("Ajax Pagination", "ultimate-post"),
        },
    },
];

const getPagiSettingsArgs = (forceAjax = false) => {
    if (!forceAjax) {
        return PagiSettingsArgs;
    }
    return PagiSettingsArgs.map((settings) => {
        if (settings.data.key === "paginationAjax") {
            return {
                data: {
                    ...settings.data,
                    disabled: true,
                    showDisabledValue: true,
                    help: pagiAjaxDisableText,
                },
            };
        } else {
            return settings;
        }
    });
};

const PagiStyleArgs = [
    {
        data: {
            type: "typography",
            key: "pagiTypo",
            label: __("Typography", "ultimate-post"),
        },
    },
    {
        data: {
            type: "range",
            key: "pagiArrowSize",
            min: 0,
            max: 100,
            step: 1,
            responsive: true,
            label: __("Arrow Size", "ultimate-post"),
        },
    },
    {
        data: {
            type: "tab",
            key: "pagiTab",
            content: [
                {
                    name: "normal",
                    title: __("Normal", "ultimate-post"),
                    options: [
                        {
                            type: "color",
                            key: "pagiColor",
                            label: __("Color", "ultimate-post"),
                            clip: true,
                        },
                        {
                            type: "color2",
                            key: "pagiBgColor",
                            label: __("Background Color", "ultimate-post"),
                        },
                        {
                            type: "border",
                            key: "pagiBorder",
                            label: __("Border", "ultimate-post"),
                        },
                        {
                            type: "boxshadow",
                            key: "pagiShadow",
                            label: __("BoxShadow", "ultimate-post"),
                        },
                        {
                            type: "dimension",
                            key: "pagiRadius",
                            label: __("Border Radius", "ultimate-post"),
                            step: 1,
                            unit: true,
                            responsive: true,
                        },
                    ],
                },
                {
                    name: "hover",
                    title: __("Hover", "ultimate-post"),
                    options: [
                        {
                            type: "color",
                            key: "pagiHoverColor",
                            label: __("Hover Color", "ultimate-post"),
                            clip: true,
                        },
                        {
                            type: "color2",
                            key: "pagiHoverbg",
                            label: __("Hover Bg Color", "ultimate-post"),
                        },
                        {
                            type: "border",
                            key: "pagiHoverBorder",
                            label: __("Hover Border", "ultimate-post"),
                        },
                        {
                            type: "boxshadow",
                            key: "pagiHoverShadow",
                            label: __("BoxShadow", "ultimate-post"),
                        },
                        {
                            type: "dimension",
                            key: "pagiHoverRadius",
                            label: __("Hover Radius", "ultimate-post"),
                            step: 1,
                            unit: true,
                            responsive: true,
                        },
                    ],
                },
            ],
        },
    },
    {
        data: {
            type: "dimension",
            key: "pagiMargin",
            label: __("Margin", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
    {
        data: {
            type: "dimension",
            key: "navMargin",
            label: __("Margin", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
    {
        data: {
            type: "dimension",
            key: "pagiPadding",
            label: __("Padding", "ultimate-post"),
            step: 1,
            unit: true,
            responsive: true,
        },
    },
];

const PagiArgs = (forceAjax) => {
    return [
        ...getPagiSettingsArgs(forceAjax).map((arg) => arg.data),
        ...PagiStyleArgs.map((arg) => arg.data),
    ];
};

export default function Settings({
    name,
    attributes,
    setAttributes,
    clientId,
    forceAjax,
}) {
    const store = { setAttributes, name, attributes, clientId };

    return (
        <>
            <BlockControls>
                <Toolbar>
                    <TypographyTB
                        store={store}
                        attrKey="pagiTypo"
                        label={__("Pagination Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spaceingIcon}
                        include={getPagiToolbarSettings({
                            include: ["pagiMargin", "pagiPadding"],
                            exclude: "__all",
                            title: __("Pagination Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Pagination Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__(
                            "Pagination Settings",
                            "ultimate-post"
                        )}
                        styleTitle={__("Pagination Style", "ultimate-post")}
                        settingsKeys={pagiSettings}
                        styleKeys={pagiStyles}
                        oArgs={PagiArgs(forceAjax)}
                        exStyle={["pagiTypo", "pagiMargin", "pagiPadding"]}
                    />
                </Toolbar>
            </BlockControls>

            <InspectorControls>
                <Sections>
                    <Section
                        slug="setting"
                        title={__("Settings", "ultimate-post")}
                    >
                        <CommonSettings
                            store={store}
                            title={"inline"}
                            include={getPagiSettingsArgs(forceAjax)}
                        />
                    </Section>
                    <Section slug="style" title={__("Styles", "ultimate-post")}>
                        <CommonSettings
                            store={store}
                            title={"inline"}
                            include={PagiStyleArgs}
                        />
                    </Section>
                </Sections>
            </InspectorControls>
        </>
    );
}
