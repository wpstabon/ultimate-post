const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from "./Edit";
import Save from "./save";
import attributes from "./attributes";
import UltpLinkGenerator from "../../helper/UltpLinkGenerator";

const docsUrl = UltpLinkGenerator(
    "https://wpxpo.com/docs/postx/postx-features/advanced-pagination/",
    "block_docs"
);

const icon = <img src={ultp_data.url + 'assets/img/blocks/pagination.svg'}/>

registerBlockType("ultimate-post/post-pagination", {
    apiVersion: 2,
    title: __("Advanced Post Pagination", "ultimate-post"),
    icon: icon,
    category: "ultimate-post",
    description: (
        <span className="ultp-block-info">
            {__("Post Pagination from PostX", "ultimate-post")}
            <a target="_blank" href={docsUrl}>
                {__("Documentation", "ultimate-post")}
            </a>
        </span>
    ),
    keywords: [
        __("Pagination", "ultimate-post"),
    ],
    attributes,
    usesContext: [
        "postId", 
        "post-grid-parent/postBlockClientId",
        "post-grid-parent/pagi"
    ],
    ancestor: ["ultimate-post/post-grid-parent"],
    supports: {
        inserter: false,
    },
    edit: Edit,
    save: Save
});