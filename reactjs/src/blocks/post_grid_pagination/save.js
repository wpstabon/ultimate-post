import {
    isInlineCSS,
    renderLoadmore,
    renderNavigation,
    renderPagination,
} from "../../helper/CommonPanel";
import { CssGenerator } from "../../helper/CssGenerator";
const { useBlockProps } = wp.blockEditor;

const name = "ultimate-post/post-pagination";

export default function Save({attributes}) {

    const {
        blockId,
        paginationType,
        paginationNav,
        paginationAjax,
        paginationText,
        loadMoreText,
        postId
    } = attributes;

    const blockProps = useBlockProps.save({
        className: `ultp-block-${blockId} ultp-pagination-block ultp-pagination-wrap`,
    });

    let __preview_css = "";
    if (blockId) {
        __preview_css = CssGenerator(attributes, name, blockId, isInlineCSS());
    }

    return (
        <>
            <>
                {__preview_css && (
                    <style
                        dangerouslySetInnerHTML={{ __html: __preview_css }}
                    ></style>
                )}
            </>
            <div {...blockProps}>
                {paginationType == "loadMore" && renderLoadmore(loadMoreText)}
                {paginationType == "navigation" && renderNavigation()}
                {paginationType == "pagination" &&
                    renderPagination(
                        paginationNav,
                        paginationAjax,
                        paginationText
                    )}
            </div>
        </>
    );
}