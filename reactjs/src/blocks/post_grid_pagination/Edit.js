/** @format */

import {
    isInlineCSS,
    renderLoadmore,
    renderNavigation,
    renderPagination,
} from "../../helper/CommonPanel";
import { CssGenerator } from "../../helper/CssGenerator";
import {
    getAllInnerBlocks,
    syncAttributesWithGrids,
} from "../../helper/gridFunctions";
import Settings from "./Settings";

const { useBlockProps } = wp.blockEditor;
const { useEffect } = wp.element;
const { useSelect } = wp.data;

const name = "ultimate-post/post-pagination";

export default function Edit({ attributes, setAttributes, clientId, context }) {
    const {
        blockId,
        paginationType,
        paginationNav,
        paginationAjax,
        paginationText,
        loadMoreText,
        postId,
    } = attributes;

    useEffect(() => {
        setAttributes({
            blockId: clientId.slice(-6),
            postId: context.postId,
        });
    }, [postId]);

    const isAdvFilterPresent = useSelect(
        (select) => {
            const pId = context["post-grid-parent/postBlockClientId"];

            if (!pId) {
                return false;
            }

            const parentBlocks = select("core/block-editor").getBlocks(pId);
            const allBlocks = getAllInnerBlocks(parentBlocks);

            const advFilter = allBlocks.filter((block) =>
                block.name.includes("advanced-filter")
            );

            return advFilter.length > 0;
        },
        [context["post-grid-parent/postBlockClientId"]]
    );

    // Turn on Ajax if Adv Filter is present
    useEffect(() => {
        if (isAdvFilterPresent && !paginationAjax) {
            setAttributes({
                paginationAjax: true
            });
        }
    }, [isAdvFilterPresent, paginationAjax])

    useEffect(() => {
        // Syncs attributes to the pagination feature that exists in the grid.
        // We use the exisitng pagination logic to generate the html for the new
        // independent pagination block
        syncAttributesWithGrids(
            context["post-grid-parent/postBlockClientId"],
            {
                paginationType,
                paginationNav,
                paginationAjax,
                paginationText,
                loadMoreText,
                navPosition: "bottom",
            }
        );
    }, [
        paginationType,
        paginationNav,
        paginationAjax,
        paginationText,
        loadMoreText,
    ]);

    const blockProps = useBlockProps({
        className: `ultp-block-${blockId} ultp-pagination-block ultp-pagination-wrap`,
    });

    let __preview_css;
    if (blockId) {
        __preview_css = CssGenerator(attributes, name, blockId, isInlineCSS());
    }

    return (
        <>
            <Settings
                attributes={attributes}
                setAttributes={setAttributes}
                name={name}
                clientId={clientId}
                forceAjax={isAdvFilterPresent}
            />
            {__preview_css && (
                <style
                    dangerouslySetInnerHTML={{ __html: __preview_css }}
                ></style>
            )}

            <div {...blockProps}>
                {paginationType == "loadMore" && renderLoadmore(loadMoreText)}
                {paginationType == "navigation" && renderNavigation()}
                {paginationType == "pagination" &&
                    renderPagination(
                        paginationNav,
                        paginationAjax,
                        paginationText
                    )}
            </div>
        </>
    );
}
