const { __ } = wp.i18n;
import {
    GeneralContent,
    PaginationContent,
    FilterContent,
    HeadingContent,
    OverlayStyle,
    ReadMoreStyle,
    TitleStyle,
    CategoryStyle,
    ExcerptStyle,
    MetaStyle,
    GeneralAdvanced,
    CustomCssAdvanced,
    ResponsiveAdvanced,
    ImageStyle,
    titleAnimationArg,
    VideoStyle,
    blockSupportLink,
    TypographyTB,
    ToolbarSettingsAndStyles,
    headingSettings,
    headingStyle,
    HeadingContentArg,
    getPagiToolbarSettings,
    pagiSettings,
    pagiStyles,
    PaginationContentArg,
    colorIcon,
    getVideoToolbarSettings,
    settingsIcon,
    getTitleToolbarSettings,
    getExcerptToolbarSettings,
    getMetaToolbarSettings,
    metaSettings,
    metaStyle,
    MetaStyleArg,
    metaTextIcon,
    metaText,
    getReadMoreToolbarSettings,
    spaceingIcon as spacingIcon,
    readMoreSettings,
    readMoreStyle,
    ReadMoreStyleArg,
    imageSettingsKeys,
    imageStyleKeys,
    ImageStyleArg,
    CategoryStyleArg,
    categoryStyle,
    categorySettings,
    getCategoryToolbarSettings,
    getFilterToolbarSettings,
    filterSettings,
    filterStyle,
    FilterContentArg,
    GeneralContentWithQuery,
} from "../../helper/CommonPanel";
import TemplateModal from "../../helper/TemplateModal";
import { Sections, Section } from "../../helper/Sections";
import { handleAdvOptions } from "../../helper/gridFunctions";
import DeperciatedSettings from "../../helper/Components/DepreciatedSettings";
import UltpToolbarGroup from "../../helper/Components/UltpToolbarGroup";
import ExtraToolbarSettings from "../../helper/Components/ExtraToolbarSettings";
import React from "react";
import DCToolbar from "../../helper/dynamic_content/DCToolbar";
import { isDCActive } from "../../helper/dynamic_content";
const { useEffect } = wp.element;

export const MAX_CUSTOM_META_GROUPS = 8;

const PG2_Settings = (props) => {
    const { store } = props;
    const {
        queryType,
        advFilterEnable,
        advPaginationEnable,
        V4_1_0_CompCheck: { runComp },
    } = store.attributes;
    const { context, section } = store;

    useEffect(() => {
        handleAdvOptions(context, advFilterEnable, store, advPaginationEnable);
    }, [advFilterEnable, advPaginationEnable, store.clientId]);

    return (
        <>
            <TemplateModal
                prev="https://www.wpxpo.com/postx/blocks/#demoid6830"
                store={store}
            />
            <Sections>
                <Section slug="setting" title={__("Setting", "ultimate-post")}>
                    <GeneralContentWithQuery
                        initialOpen={section["general"]}
                        store={store}
                        exclude={["columnGridGap"]}
                        include={[
                            {
								position: 0,
								data: {
									type: isDCActive() ? 'toggle' : '',
									label: __(
										'Enable Dynamic Content',
										'ultimate-post'
									),
									key: 'dcEnabled',
									help: __(
										'Insert dynamic data & custom fields that update automatically.',
										'ultimate-post'
									),
								},
							},
                            {
                                position: 1,
                                data: {
                                    type: "advFilterEnable",
                                    key: "advFilterEnable",
                                },
                            },
                            {
                                position: 2,
                                data: {
                                    type: "advPagiEnable",
                                    key: "advPaginationEnable",
                                },
                            },
                            {
                                position: 3,
                                data: { type: "separator" },
                            },
                            {
                                position: 4,
                                data: {
                                    type: "range",
                                    _inline: true,
                                    key: "columnGridGap",
                                    min: 0,
                                    max: 120,
                                    step: 1,
                                    responsive: true,
                                    unit: ["px", "em", "rem"],
                                    label: __("Gap", "ultimate-post"),
                                },
                            },
                            {
                                position: 5,
                                data: {
                                    type: "range",
                                    _inline: true,
                                    key: "overlayHeight",
                                    min: 0,
                                    max: 700,
                                    step: 1,
                                    unit: true,
                                    responsive: true,
                                    label: __("Height", "ultimate-post"),
                                },
                            },
                            {
                                data: {
                                    type: "text",
                                    key: "notFoundMessage",
                                    label: __(
                                        "No result found Text",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                    />

                    <TitleStyle
                        store={store}
                        depend="titleShow"
                        initialOpen={section["title"]}
                        hrIdx={[4, 9]}
                    />

                    <ImageStyle
                        isTab={true}
                        store={store}
                        depend="showImage"
                        initialOpen={section["image"]}
                        include={[
                            {
                                data: {
                                    type: "toggle",
                                    key: "fallbackEnable",
                                    label: __(
                                        "Fallback Image Enable",
                                        "ultimate-post"
                                    ),
                                },
                            },
                            {
                                data: {
                                    type: "media",
                                    key: "fallbackImg",
                                    label: __(
                                        "Fallback Image",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                        exclude={[
                            "imgWidth",
                            "imageScale",
                            "imgHeight",
                            "imgMargin",
                            "imgCropSmall",
                        ]}
                        hrIdx={[
                            {
                                tab: "settings",
                                hr: [2, 10],
                            },
                        ]}
                    />

                    <MetaStyle
                        isTab={true}
                        store={store}
                        initialOpen={section["meta"]}
                        depend="metaShow"
                        exclude={["metaListSmall"]}
                        hrIdx={[
                            {
                                tab: "settings",
                                hr: [4],
                            },
                            {
                                tab: "style",
                                hr: [7],
                            },
                        ]}
                    />

                    <CategoryStyle
                        isTab={true}
                        initialOpen={section["taxonomy-/-category"]}
                        store={store}
                        depend="catShow"
                        hrIdx={[
                            {
                                tab: "style",
                                hr: [8],
                            },
                        ]}
                    />

                    <ExcerptStyle
                        depend="excerptShow"
                        initialOpen={section["excerpt"]}
                        store={store}
                        hrIdx={[3, 6]}
                    />

                    <ReadMoreStyle
                        isTab={true}
                        store={store}
                        initialOpen={section["read-more"]}
                        depend="readMore"
                        hrIdx={[
                            {
                                tab: "style",
                                hr: [3],
                            },
                        ]}
                    />

                    <VideoStyle
                        initialOpen={section["video"]}
                        depend="vidIconEnable"
                        store={store}
                    />

                    <OverlayStyle
                        store={store}
                        include={titleAnimationArg}
                        exclude={["overlaySmallHeight"]}
                    />

                    {!runComp && (
                        <DeperciatedSettings
                            open={
                                section["filter"] ||
                                section["pagination"] ||
                                section["heading"]
                            }
                        >
                            <HeadingContent
                                isTab={true}
                                store={store}
                                initialOpen={section["heading"]}
                                depend="headingShow"
                            />

                            {queryType != "posts" &&
                                queryType != "customPosts" && (
                                    <FilterContent
                                        isTab={true}
                                        store={store}
                                        initialOpen={section["filter"]}
                                        depend="filterShow"
                                        hrIdx={[
                                            {
                                                tab: "settings",
                                                hr: [5],
                                            },
                                            {
                                                tab: "style",
                                                hr: [4, 9],
                                            },
                                        ]}
                                    />
                                )}

                            <PaginationContent
                                isTab={true}
                                store={store}
                                initialOpen={section["pagination"]}
                                depend="paginationShow"
                                hrIdx={[
                                    {
                                        tab: "style",
                                        hr: [4],
                                    },
                                ]}
                            />
                        </DeperciatedSettings>
                    )}
                </Section>
                <Section
                    slug="advanced"
                    title={__("Advanced", "ultimate-post")}
                >
                    <GeneralAdvanced
                        initialOpen={true}
                        store={store}
                        include={[
                            {
                                position: 2,
                                data: {
                                    type: "color",
                                    key: "loadingColor",
                                    label: __("Loading Color", "ultimate-post"),
                                    pro: true,
                                },
                            },
                        ]}
                    />
                    <ResponsiveAdvanced store={store} />
                    <CustomCssAdvanced store={store} />
                </Section>
            </Sections>
            {blockSupportLink()}
        </>
    );
};

export default PG2_Settings;

export function AddSettingsToToolbar({ selected, store }) {
    const {
        V4_1_0_CompCheck: { runComp },
    } = store.attributes;

    const {
		metaShow,
		showImage,
		titleShow,
		catPosition,
		titlePosition,
		excerptShow,
		readMore,
		metaPosition,
		layout,
		fallbackEnable,
		catShow,
	} = store.attributes;

	const layoutContext = [
		metaShow && metaPosition == 'bottom',
		readMore,
		excerptShow,
		titleShow && titlePosition == false,
		metaShow && metaPosition == 'top',
		titleShow && titlePosition == true,
		catShow && catPosition == 'aboveTitle',
	];

    if (isDCActive() && selected.startsWith('dc_')) {
		return (
			<DCToolbar
				store={store}
				selected={selected}
				layoutContext={layoutContext}
			/>
		);
	}

    switch (selected) {
        case "filter":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Filter"}>
                    <TypographyTB
                        store={store}
                        attrKey="fliterTypo"
                        label={__("Filter Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getFilterToolbarSettings({
                            include: ["fliterSpacing", "fliterPadding"],
                            exclude: "__all",
                            title: __("Filter Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Filter Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Filter Settings", "ultimate-post")}
                        styleTitle={__("Filter Style", "ultimate-post")}
                        settingsKeys={filterSettings}
                        styleKeys={filterStyle}
                        oArgs={FilterContentArg}
                        exStyle={[
                            "fliterTypo",
                            "fliterSpacing",
                            "fliterPadding",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "heading":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Heading"}>
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Heading Settings", "ultimate-post")}
                        styleTitle={__("Heading Style", "ultimate-post")}
                        settingsKeys={headingSettings}
                        styleKeys={headingStyle}
                        oArgs={HeadingContentArg}
                    />
                </UltpToolbarGroup>
            );
        case "pagination":
            if (runComp) return null;
            return (
                <UltpToolbarGroup text={"Pagination"}>
                    <TypographyTB
                        store={store}
                        attrKey="pagiTypo"
                        label={__("Pagination Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getPagiToolbarSettings({
                            include: ["pagiMargin", "pagiPadding"],
                            exclude: "__all",
                            title: __("Pagination Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Pagination Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__(
                            "Pagination Settings",
                            "ultimate-post"
                        )}
                        styleTitle={__("Pagination Style", "ultimate-post")}
                        settingsKeys={pagiSettings}
                        styleKeys={pagiStyles}
                        oArgs={PaginationContentArg}
                        exStyle={["pagiTypo", "pagiMargin", "pagiPadding"]}
                    />
                </UltpToolbarGroup>
            );
        case "video":
            return (
                <UltpToolbarGroup text={"Video"}>
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getVideoToolbarSettings({
                            include: [
                                "popupIconColor",
                                "popupHovColor",
                                "popupTitleColor",
                                "closeIconColor",
                                "closeHovColor",
                            ],
                            exclude: "__all",
                            title: __("Video Icon Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Video Icon Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getVideoToolbarSettings({
                            exclude: [
                                "popupIconColor",
                                "popupHovColor",
                                "popupTitleColor",
                                "closeIconColor",
                                "closeHovColor",
                            ],
                        })}
                        store={store}
                        label={__("Video Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "title":
            return (
                <UltpToolbarGroup text={"Title"}>
                    <TypographyTB
                        store={store}
                        attrKey="titleTypo"
                        label={__("Title Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getTitleToolbarSettings({
                            include: [
                                "titleColor",
                                "titleHoverColor",
                                "titleBackground",
                            ],
                            exclude: "__all",
                            title: __("Title Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Title Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getTitleToolbarSettings({
                            exclude: [
                                "titleBackground",
                                "titleTypo",
                                "titleColor",
                                "titleHoverColor",
                            ],
                            title: __("Title Settings", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Title Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "excerpt":
            return (
                <UltpToolbarGroup text={"Excerpt"}>
                    <TypographyTB
                        store={store}
                        attrKey="excerptTypo"
                        label={__("Excerpt Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={colorIcon}
                        include={getExcerptToolbarSettings({
                            include: ["excerptColor"],
                            exclude: "__all",
                            title: __("Excerpt Color", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Excerpt Color", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={settingsIcon}
                        include={getExcerptToolbarSettings({
                            exclude: ["excerptTypo", "excerptColor"],
                            title: __("Excerpt Settings", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Excerpt Settings", "ultimate-post")}
                    />
                </UltpToolbarGroup>
            );
        case "meta":
            return (
                <UltpToolbarGroup text={"Meta"}>
                    <TypographyTB
                        store={store}
                        attrKey="metaTypo"
                        label={__("Meta Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getMetaToolbarSettings({
                            include: [
                                "metaSpacing",
                                "metaMargin",
                                "metaPadding",
                            ],
                            exclude: "__all",
                            title: __("Meta Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Meta Spacing", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={metaTextIcon}
                        include={getMetaToolbarSettings({
                            include: metaText,
                            exclude: "__all",
                            title: __("Meta Texts", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Meta Texts", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Meta Settings", "ultimate-post")}
                        styleTitle={__("Meta Style", "ultimate-post")}
                        settingsKeys={metaSettings}
                        styleKeys={metaStyle}
                        oArgs={MetaStyleArg}
                        exSettings={["metaListSmall"]}
                        exStyle={[
                            "metaTypo",
                            "metaMargin",
                            "metaPadding",
                            "metaSpacing",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "read-more":
            return (
                <UltpToolbarGroup text={"Read More"}>
                    <TypographyTB
                        store={store}
                        attrKey="readMoreTypo"
                        label={__("Read More Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getReadMoreToolbarSettings({
                            include: [
                                "readMoreSacing", // 💩 yes, it's a typo
                                "readMorePadding",
                            ],
                            exclude: "__all",
                            title: __("Read More Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Read More Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__(
                            "Read More Settings",
                            "ultimate-post"
                        )}
                        styleTitle={__("Read More Style", "ultimate-post")}
                        settingsKeys={readMoreSettings}
                        styleKeys={readMoreStyle}
                        oArgs={ReadMoreStyleArg}
                        exStyle={[
                            "readMoreTypo",
                            "readMoreSacing",
                            "readMorePadding",
                        ]}
                    />
                </UltpToolbarGroup>
            );
        case "cat":
            return (
                <UltpToolbarGroup text={"Taxonomy/Category"}>
                    <TypographyTB
                        store={store}
                        attrKey="catTypo"
                        label={__("Category Typography", "ultimate-post")}
                    />
                    <ExtraToolbarSettings
                        buttonContent={spacingIcon}
                        include={getCategoryToolbarSettings({
                            include: ["catSacing", "catPadding"],
                            exclude: "__all",
                            title: __("Category Spacing", "ultimate-post"),
                        })}
                        store={store}
                        label={__("Category Spacing", "ultimate-post")}
                    />
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Category Settings", "ultimate-post")}
                        styleTitle={__("Category Style", "ultimate-post")}
                        settingsKeys={categorySettings}
                        styleKeys={categoryStyle}
                        oArgs={CategoryStyleArg}
                        exStyle={["catTypo", "catSacing", "catPadding"]}
                    />
                </UltpToolbarGroup>
            );
        case "image":
            return (
                <UltpToolbarGroup text={"Image"}>
                    <ToolbarSettingsAndStyles
                        store={store}
                        settingsTitle={__("Image Settings", "ultimate-post")}
                        styleTitle={__("Image Style", "ultimate-post")}
                        settingsKeys={imageSettingsKeys}
                        styleKeys={imageStyleKeys}
                        oArgs={ImageStyleArg}
                        exStyle={[
                            "imgWidth",
                            "imageScale",
                            "imgHeight",
                            "imgMargin",
                        ]}
                        exSettings={["imgCropSmall"]}
                        incSettings={[
                            {
                                data: {
                                    type: "toggle",
                                    key: "fallbackEnable",
                                    label: __(
                                        "Fallback Image Enable",
                                        "ultimate-post"
                                    ),
                                },
                            },
                            {
                                data: {
                                    type: "media",
                                    key: "fallbackImg",
                                    label: __(
                                        "Fallback Image",
                                        "ultimate-post"
                                    ),
                                },
                            },
                        ]}
                    />
                </UltpToolbarGroup>
            );
        default:
            return null;
    }
}
