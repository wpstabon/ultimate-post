const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import UltpLinkGenerator from '../../helper/UltpLinkGenerator';
import Edit from './Edit'
import attributes from './attributes'

const docsUrl = UltpLinkGenerator('https://wpxpo.com/docs/postx/all-blocks/dark-light/', 'block_docs');

registerBlockType(
    'ultimate-post/dark-light', {
        title: __('Dark/Light - PostX','ultimate-post'),
        icon: <div className={`ultp-block-inserter-icon-section`}>
                <img src={ultp_data.url + 'assets/img/blocks/dark_light.svg'}/>
                {!ultp_data.active &&
                    <span className={`ultp-pro-block`}>Pro</span>
                }
            </div>,
        category: 'ultimate-post',
        description: <span className="ultp-block-info">
            {__('Switch between Dark and Light versions of your site','ultimate-post')}
            {/* <a target="_blank" href={docsUrl} >{__('Documentation', 'ultimate-post')}</a> */}
        </span>,
        keywords: [ 
            __('dark','ultimate-post'),
            __('light','ultimate-post'),
            __('night mode','ultimate-post')
        ],
        attributes,
        supports: {
            align: ['center', 'wide', 'full'],
        },
        example: {
            attributes: {
                previewImg: ultp_data.url+'assets/img/layouts/dark_light/dark7.svg',
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)