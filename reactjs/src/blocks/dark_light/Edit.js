const { __ } = wp.i18n
const { InspectorControls } = wp.blockEditor
const { useState, useEffect, Fragment } = wp.element
import { CssGenerator } from '../../helper/CssGenerator'
import { GeneralAdvanced, CustomCssAdvanced, ResponsiveAdvanced, isInlineCSS, CommonSettings, updateCurrentPostId } from '../../helper/CommonPanel'
import { Sections, Section } from '../../helper/Sections'
import dlIcons from './dlIcons'
const { getBlockAttributes, getBlockRootClientId } = wp.data.select('core/block-editor');

export default function Edit(props) {

	const [section, setSection] = useState('Content');
	const { setAttributes, name, clientId, className, attributes, attributes: { blockId, previewImg, advanceId, layout, iconType, reverseSwitcher, lightText, darkText, enableText, textAppears, previewDark, currentPostId, blockPubDate } } = props

	useEffect(() => {
		const _client = clientId.substr(0, 6)
		const reference = getBlockAttributes(getBlockRootClientId(clientId));
		updateCurrentPostId(setAttributes, reference, currentPostId, clientId);

		if (!blockId) {
			setAttributes({ blockId: _client });
		} else if (blockId && blockId != _client) {
			if (!reference?.hasOwnProperty('ref') && !isInlineCSS()) {
				if (!reference?.hasOwnProperty('theme')) {
					setAttributes({ blockId: _client });
				}
			}
		}
		if(blockPubDate == "empty") {
			setAttributes({ blockPubDate: new Date().toISOString() });
		}
	}, [clientId]);

	const store = { setAttributes, name, attributes, setSection, section, clientId }

	let __preview_css;
	if (blockId) {
		__preview_css = CssGenerator(attributes, 'ultimate-post/dark-light', blockId, isInlineCSS());
	}

	if (previewImg) {
		return <img style={{ marginTop: '0px', width: '420px' }} src={previewImg} />
	}

	return (
		<Fragment>
			<InspectorControls>
				<Sections>
					<Section slug="setting" title={__('Style', 'ultimate-post')}>
						<CommonSettings title={`inline`} include={[
							{
								position: 5, data: {
									type: 'layout', block: 'dark-light', key: 'layout', label: __('Layout', 'ultimate-post'),
									options: [
										{ img: 'assets/img/layouts/dark_light/dark1.svg', label: 'Layout 1', value: 'layout1', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark2.svg', label: 'Layout 2', value: 'layout2', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark3.svg', label: 'Layout 3', value: 'layout3', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark4.svg', label: 'Layout 4', value: 'layout4', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark5.svg', label: 'Layout 5', value: 'layout5', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark6.svg', label: 'Layout 6', value: 'layout6', pro: false },
										{ img: 'assets/img/layouts/dark_light/dark7.svg', label: 'Layout 7', value: 'layout7', pro: false },
									],
								}
							},
							{
								position: 6, data: { type: 'toggle', key: 'previewDark', label: __('Preview Dark Design', 'ultimate-post') }
							},
							{
								position: 10, data: {
									type: 'group', key: 'iconType', justify: true, label: __('Icon Type', 'ultimate-post'), options: [
										{ value: 'line', label: __('Line', 'ultimate-post') },
										{ value: 'solid', label: __('Solid', 'ultimate-post') }
									]
								}
							},
							{
								position: 15, data: { type: 'range', key: 'iconSize', min: 10, max: 300, step: 1, responsive: false, label: __('Icon Size', 'ultimate-post') }
							},
							{
								position: 20, data: { type: 'toggle', key: 'reverseSwitcher', label: __('Reverse Switcher', 'ultimate-post') }
							},
							{
								position: 25, data: { type: 'toggle', key: 'enableText', label: __('Enable Text', 'ultimate-post') }
							},
							{
								position: 26, data: {
									type: 'select', key: 'textAppears', label: __('Text Appears', 'ultimate-post'),
									options: [
										{ value: 'left', label: __('Selected (Left)', 'ultimate-post') },
										{ value: 'right', label: __('Selected (Right)', 'ultimate-post') },
										{ value: 'both', label: __('Both', 'ultimate-post') }
									]
								},
							},
							{
								position: 30, data: { type: 'text', key: 'lightText', label: __('Light Mode Text', 'ultimate-post') }
							},
							{
								position: 35, data: { type: 'text', key: 'darkText', label: __('Dark Mode Text', 'ultimate-post') }
							},
							{
								position: 40, data: { type: 'typography', key: 'textTypo', label: __('Text Typography', 'ultimate-post') }
							},
							{
								position: 45, data: { type: 'range', key: 'textSwictherGap', min: 0, max: 300, step: 1, responsive: false, label: __('Text to Switcher Gap', 'ultimate-post') }
							},
							{
								position: 50, data: { type: 'separator', label: __('Switcher Color', 'ultimate-post') }
							},
							{
								position: 55, data: {
									type: 'tab', content: [
										{
											name: 'light', title: __('Light Mode', 'ultimate-post'), options: [
												{ type: 'color', key: 'lightTextColor', label: __('Light Color', 'ultimate-post') },
												{ type: 'color', key: 'lightIconColor', label: __('Icon Color', 'ultimate-post') },
												{ type: 'color2', key: 'lightIconBg', label: __('Icon Background Color', 'ultimate-post') },
												{ type: 'color2', key: 'switcherBg', label: __('Background Color', 'ultimate-post') },
												{ type: 'border', key: 'switcherBorder', label: __('Border', 'ultimate-post') },
												{ type: 'boxshadow', key: 'switcherShadow', step: 1, unit: true, responsive: true, label: __('Box shadow', 'ultimate-post') },
											]
										},
										{
											name: 'dark', title: __('Dark Mode', 'ultimate-post'), options: [
												{ type: 'color', key: 'darkTextColor', label: __('Dark Color', 'ultimate-post') },
												{ type: 'color', key: 'lightIconColorDark', label: __('Icon Color', 'ultimate-post') },
												{ type: 'color2', key: 'lightIconBgDark', label: __('Icon Background Color', 'ultimate-post') },
												{ type: 'color2', key: 'switcherBgDark', label: __('Background Color', 'ultimate-post') },
												{ type: 'border', key: 'switcherBorderDark', label: __('Border', 'ultimate-post') },
												{ type: 'boxshadow', key: 'switcherShadowDark', step: 1, unit: true, responsive: true, label: __('Box shadow', 'ultimate-post') },
											]
										}
									]
								}
							},

						]} initialOpen={true} store={store}
						/>
					</Section>
					<Section slug="advanced" title={__('Advanced', 'ultimate-post')}>
						<GeneralAdvanced initialOpen={true} store={store} />
						<ResponsiveAdvanced store={store} />
						<CustomCssAdvanced store={store} />
					</Section>
				</Sections>
			</InspectorControls>
			<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId} ${className}`}>
				{__preview_css &&
					<style dangerouslySetInnerHTML={{ __html: __preview_css }}></style>
				}
				<div className={`ultp-dark-light-block-wrapper ultp-block-wrapper`}>
					{
						!ultp_data.active && <div className="ultp-pro-helper">
							<div className="ultp-pro-helper__upgrade">
								<span>To Unlock Dark Light Block</span>
								<a className="ultp-upgrade-pro" href={'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=darklight-pro&utm_campaign=postx-dashboard'} target="_blank"> Upgrade to Pro </a>
							</div>
						</div>
					}
					<div className={`ultp-dark-light-block-wrapper-content ${layout}`}>
						{
							!previewDark ? <div className={`ultp-dl-after-before-con ultp-dl-light ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
								{enableText && layout != 'layout7' && ['left', 'both'].includes(textAppears) && <div className="ultp-dl-before-after-text lightText">{lightText}</div>}
								<div className={`ultp-dl-con ultp-light-con ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
									<div className="ultp-dl-svg-con">
										<div className="ultp-dl-svg">{dlIcons[iconType == 'line' ? 'sun_line' : 'sun']}</div>
									</div>
									{
										['layout5', 'layout6', 'layout7'].includes(layout) && <div className="ultp-dl-text lightText">
											{layout == 'layout5' && <div className="ultp-dl-democircle ultphidden"></div>}
											{layout == 'layout6' && <div className="ultp-dl-democircle"></div>}
											{layout == 'layout7' && lightText}
										</div>
									}
								</div>
								{enableText && layout != 'layout7' && ['right', 'both'].includes(textAppears) && <div className={`ultp-dl-before-after-text ${textAppears != 'both' ? 'lightText' : 'darkText'}`}>{textAppears != 'both' ? lightText : darkText}</div>}
							</div>
								:
								<div className={`ultp-dl-after-before-con ultp-dl-dark ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
									{enableText && layout != 'layout7' && ['left', 'both'].includes(textAppears) && <div className={`ultp-dl-before-after-text ${textAppears != 'both' ? 'darkText' : 'lightText'}`}>{textAppears != 'both' ? darkText : lightText}</div>}
									<div className={`ultp-dl-con ultp-dark-con ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
										{
											['layout5', 'layout6', 'layout7'].includes(layout) && <div className="ultp-dl-text darkText">
												{layout == 'layout5' && <div className="ultp-dl-democircle ultphidden"></div>}
												{layout == 'layout6' && <div className="ultp-dl-democircle"></div>}
												{layout == 'layout7' && darkText}
											</div>
										}
										<div className="ultp-dl-svg-con">
											<div className="ultp-dl-svg">{dlIcons[iconType == 'line' ? 'moon_line' : 'moon']}</div>
										</div>
									</div>
									{enableText && layout != 'layout7' && ['right', 'both'].includes(textAppears) && <div className="ultp-dl-before-after-text darkText">{darkText}</div>}
								</div>
						}
					</div>
				</div>
			</div>
		</Fragment>
	)
}

