import { commonAttributes } from "../../helper/commonAttribute/commonAttribute";

const attributes = {
    /*============================
        General  Settings
    ============================*/
    blockId: {
        type: 'string',
        default: '',
    },
    blockPubDate: { type: "string", default: "empty" },
    previewImg: {
        type: 'string',
        default: '',
    },
    previewDark: {
        type: "boolean", 
        default: false,
    },
    currentPostId: { 
        type: "string" , 
        default: "" 
    },
    layout: {
        type: "string", 
        default: "layout1" 
    },
    iconType: {
        type: 'string',
        default: 'solid',
    },
    reverseSwitcher: { 
        type: "boolean", 
        default: false,
        style: [
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout5' },
                ],
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout6' },
                ],
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ],
            },
        ]
    },
    iconSize: {
        type: 'string',
        default: '24',
        style: [
            {
                selector:
                `{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-svg-con svg,
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-democircle { height: {{iconSize}}px; width: {{iconSize}}px; }`
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout1' }],
                selector:
                `{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-svg-con svg { height: calc({{iconSize}}px*4/6); width:calc({{iconSize}}px*4/6); }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { padding: calc({{iconSize}}px/3) {{iconSize}}px ; border-radius: {{iconSize}}px;  }`
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout3' }],
                selector:
                '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { padding: calc({{iconSize}}px/3); border-radius: 100%;  }'
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout4' }],
                selector:
                '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { padding: {{iconSize}}px calc({{iconSize}}px/3); border-radius: {{iconSize}}px;  }'
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout5' }],
                selector: 
                `{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-svg-con svg { height: calc({{iconSize}}px*4/6); width:calc({{iconSize}}px*4/6); }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { gap: calc({{iconSize}}px/2);  padding: calc({{iconSize}}px/6); border-radius: {{iconSize}}px; }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con .ultp-dl-svg { padding: calc({{iconSize}}px/6); border-radius: {{iconSize}}px; }`
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout6' }],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { gap: calc({{iconSize}}px/2);  padding: calc({{iconSize}}px/6); border-radius: {{iconSize}}px; }'
            },
            {
                depends: [ { key:'layout', condition:'==', value: 'layout7' }],
                selector: 
                `{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-svg-con svg { height: calc({{iconSize}}px*4/6); width:calc({{iconSize}}px*4/6); }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con { padding: calc({{iconSize}}px/6); border-radius: {{iconSize}}px; }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con .ultp-dl-svg { padding: calc({{iconSize}}px/6); border-radius: {{iconSize}}px; }
                {{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con .ultp-dl-text {margin: 0px calc({{iconSize}}px/2); }`
            },
        ]
    },
    enableText: { 
        type: "boolean", 
        default: false,
        style: [
            {
                depends: [
                    { key:'layout', condition:'!=', value: 'layout7' },
                ],
            },
        ]
    },

    /*============================
        Switcher Color  Settings
    ============================*/
    textAppears: { 
        type: "string", 
        default: "both",
        style: [
            {
                depends: [
                    { key:'layout', condition:'!=', value: 'layout7' },
                    { key:'enableText', condition:'==', value: true },
                ],
            },
        ] 
    },
    lightText : {
        type: 'string',
        default: 'Light Mode',
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                ],
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ],
            },
        ]
    },
    darkText : {
        type: 'string',
        default: 'Dark Mode',
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                ],
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ],
            },
        ]
    },
    textTypo: {
        type: 'object',
        default: { openTypography: 1,size: {lg:12, unit:'px'},height: {lg:'14', unit:'px'}, decoration: 'none',family: '', weight: 400 },
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true }
                ],
                selector:'{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-before-after-text'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ],
                selector:'{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-text'
            },
        ]
    },
    textSwictherGap: {
        type: 'string',
        default: '10',
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                    { key:'layout', condition:'!=', value: 'layout7' }
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-after-before-con { gap: {{textSwictherGap}}px; }'
            },
        ]
    },


    //Light Mode
    lightTextColor: {
        type: 'string',
        default: '#2E2E2E',
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-light .ultp-dl-before-after-text.lightText { color: {{lightTextColor}}; }'
            },
            {
                depends: [
                    { key:'textAppears', condition:'==', value: 'both' },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-light .ultp-dl-before-after-text.darkText { color: {{lightTextColor}}; opacity: 0.4; }'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' }
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-light .ultp-dl-text.lightText { color: {{lightTextColor}}; }'
            },
        ]
    },
    lightIconColor: {
        type: 'string',
        default: '#2E2E2E',
        style: [{selector:'{{ULTP}} .ultp-dark-light-block-wrapper .ultp-light-con svg { fill: {{lightIconColor}}; }'}]
    },
    lightIconBg: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#9A9A9A', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout5' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-light-con .ultp-dl-svg'
            },
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout6' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-light-con .ultp-dl-democircle'
            },
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-light-con .ultp-dl-svg'
            },
        ],
    },
    switcherBg: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#C3C3C3', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-light-con'
            },
        ],
    },
    switcherBorder: {
        type: 'object',
        default: { openBorder: 0, width: {top: 2, right: 2, bottom: 2, left: 2},color: '#9A9A9A' },
        style: [
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-light-con'
            }
        ]
    },
    switcherShadow: {
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-light-con'
            }
        ]
    },
    //Dark Mode
    darkTextColor: {
        type: 'string',
        default: '#F4F4F4',
        style: [
            {
                depends: [
                    { key:'enableText', condition:'==', value: true },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-dark .ultp-dl-before-after-text.darkText { color: {{darkTextColor}}; }'
            },
            {
                depends: [
                    { key:'textAppears', condition:'==', value: 'both' },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-dark .ultp-dl-before-after-text.lightText { color: {{darkTextColor}}; opacity: 0.4; }'
            },
            {
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' }
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-dark .ultp-dl-text.darkText { color: {{darkTextColor}}; }'
            },
        ]
    },
    lightIconColorDark: {
        type: 'string',
        default: '#F4F4F4',
        style: [{selector:'{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dark-con svg { fill: {{lightIconColorDark}}; }'}]
    },
    lightIconBgDark: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#9D9D9D', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout5' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dark-con .ultp-dl-svg'
            },
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout6' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dark-con .ultp-dl-democircle'
            },
            { 
                depends: [
                    { key:'layout', condition:'==', value: 'layout7' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dark-con .ultp-dl-svg'
            },
        ],
    },
    switcherBgDark: {
        type: 'object',
        default: {openColor: 1, type: 'color', color: '#646464', size: 'cover', repeat: 'no-repeat'},
        style:[
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ],
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-dark-con'
            },
        ],
    },
    switcherBorderDark: {
        type: 'object',
        default: { openBorder: 0, width: {top: 2, right: 2, bottom: 2, left: 2},color: '#9D9D9D' },
        style: [
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-dark-con'
            }
        ]
    },
    switcherShadowDark: {
        type: 'object',
        default: {openShadow: 0, width: {top: 1, right: 1, bottom: 1, left: 1},color: '#009fd4'},
        style: [
            { 
                depends: [
                    { key:'layout', condition:'!=', value: 'layout2' },
                ], 
                selector: '{{ULTP}} .ultp-dark-light-block-wrapper .ultp-dl-con.ultp-dark-con'
            }
        ]
    },
    
    /*========= General  Settings =========*/
    ...commonAttributes(['advanceAttr']),
}
export default attributes;