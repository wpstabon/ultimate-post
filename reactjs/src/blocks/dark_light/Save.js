import dlIcons from "./dlIcons";
export default function Save(props) {

	const { blockId, advanceId, layout, reverseSwitcher, enableText, textAppears, lightText, darkText, iconType } = props.attributes;

	return (
		<div {...(advanceId && { id: advanceId })} className={`ultp-block-${blockId}`}>
			{
				<div className={`ultp-dark-light-block-wrapper ultp-block-wrapper`}>
					<div className={`ultp-dark-light-block-wrapper-content ${layout}`}>
						<div className={`ultp-dl-after-before-con ultp-dl-light ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
							{enableText && layout != 'layout7' && ['left', 'both'].includes(textAppears) && <div className="ultp-dl-before-after-text lightText">{lightText}</div>}
							<div className={`ultp-dl-con ultp-light-con ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
								<div className="ultp-dl-svg-con">
									<div className="ultp-dl-svg">{dlIcons[iconType == 'line' ? 'sun_line' : 'sun']}</div>
								</div>
								{
									['layout5', 'layout6', 'layout7'].includes(layout) && <div className="ultp-dl-text lightText">
										{layout == 'layout5' && <div className="ultp-dl-democircle ultphidden"></div>}
										{layout == 'layout6' && <div className="ultp-dl-democircle"></div>}
										{layout == 'layout7' && lightText}
									</div>
								}
							</div>
							{enableText && layout != 'layout7' && ['right', 'both'].includes(textAppears) && <div className={`ultp-dl-before-after-text ${textAppears != 'both' ? 'lightText' : 'darkText'}`}>{textAppears != 'both' ? lightText : darkText}</div>}
						</div>

						<div className={`ultp-dl-after-before-con ultp-dl-dark ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
							{enableText && layout != 'layout7' && ['left', 'both'].includes(textAppears) && <div className={`ultp-dl-before-after-text ${textAppears != 'both' ? 'darkText' : 'lightText'}`}>{textAppears != 'both' ? darkText : lightText}</div>}
							<div className={`ultp-dl-con ultp-dark-con ${reverseSwitcher ? 'ultp-dl-reverse' : ''}`}>
								{
									['layout5', 'layout6', 'layout7'].includes(layout) && <div className="ultp-dl-text darkText">
										{layout == 'layout5' && <div className="ultp-dl-democircle ultphidden"></div>}
										{layout == 'layout6' && <div className="ultp-dl-democircle"></div>}
										{layout == 'layout7' && darkText}
									</div>
								}
								<div className="ultp-dl-svg-con">
									<div className="ultp-dl-svg">{dlIcons[iconType == 'line' ? 'moon_line' : 'moon']}</div>
								</div>
							</div>
							{enableText && layout != 'layout7' && ['right', 'both'].includes(textAppears) && <div className="ultp-dl-before-after-text darkText">{darkText}</div>}
						</div>
					</div>
				</div>
			}
		</div>
	)
}