import { CssGenerator } from './CssGenerator'

const savePostCssApi = (data) => {
    const { post_id, block_css, hasBlocks, preview, bindCss, src, fseTempId } = data;
    if ( bindCss == 'handleBindCss' ) {
        window.bindCssPostX = true;
    }
    return wp.apiFetch({
        path: '/ultp/v1/save_block_css',
        method: 'POST',
        data: { 
            block_css: block_css, 
            post_id: post_id, 
            has_block: hasBlocks ? hasBlocks : false, 
            preview: preview ? preview : false,
            src: src,
            fseTempId: fseTempId || ''
        }
    }).then( data => {
        if ( bindCss == 'handleBindCss' ) {
            setTimeout(() => {
                window.bindCssPostX = false;
            }, 100);
        }
    })
}

function buildBlockCss( data, cssObject = {}) {
    const { blocks, pId, preview } = data;
    blocks?.forEach(row => {
        const { attributes, name } = row;
        const blockName = name.split('/');
        if (blockName[0] === 'ultimate-post' && attributes.blockId) {
            cssObject[pId] = (cssObject[pId] || '') + CssGenerator(attributes, name, attributes.blockId, true);
        }
        if (row.innerBlocks && row.innerBlocks.length > 0) {
            buildBlockCss({ blocks: row.innerBlocks, pId: pId, preview: preview }, cssObject);
        }
        if ( row.name?.indexOf('core/block')!= -1 ) {
            setTimeout(() => {
                getPatternCss({ pId: attributes?.ref, preview: preview});
            }, 700);
        } else if ( row.name == "core/template-part" ) {
            setTimeout(() => {
                handleTemplatePartCss( {slug: attributes?.slug, theme: attributes?.theme, preview: preview} );
            }, 700);
        }
    });
    return cssObject;
}

function handleTemplatePartCss(data) {
    const { slug, theme, preview } = data;
    const fseAllTemplates =  wp.data.select( 'core' )?.getEntityRecords( 'postType', 'wp_template_part' , { per_page: -1 } ) || [];
    const fseItem = fseAllTemplates.find( (item) => item.id == ( theme + "//" + slug ) );
    const pId = fseItem?.wp_id;

    wp.apiFetch({
        path: '/ultp/v1/get_other_post_content',
        method: 'POST',
        data: { 
            postId: pId 
        }
    }).then( response => {
        if ( response.success ) {
            const thePatternCss = response.data?.includes('ultimate-post') ? 
                buildBlockCss({ blocks: wp.blocks.parse(response.data), pId: pId }) 
                : 
                { [pId]: ''};
            savePostCssApi(
                { 
                    post_id: pId, 
                    block_css: thePatternCss[pId], 
                    hasBlocks: !!thePatternCss[pId], 
                    preview: false, 
                    src: 'partCss'
                }
            ).then( data => {} );
        }
    });
}

function getPatternCss({ pId, preview }) {
    wp.apiFetch({
        path: '/ultp/v1/get_other_post_content',
        method: 'POST',
        data: { 
            postId: pId 
        }
    }).then( response => {
        if ( response.success ) {
            const thePatternCss = response.data?.includes('ultimate-post') ? 
                buildBlockCss({ blocks: wp.blocks.parse(response.data), pId: pId }) 
                :
                { [pId]: ''};
            savePostCssApi(
                { 
                    post_id: pId, 
                    block_css: thePatternCss[pId], 
                    hasBlocks: !!thePatternCss[pId], 
                    preview: false, 
                    src: 'wpBlock',
                }
            ).then( data => {} );
        }
    })
};

const ParseCssGen = (data = {}) => {
    const { preview = false, handleBindCss = true } = data;
    const all_blocks = wp.data.select('core/block-editor').getBlocks();
    const { getCurrentPostId } = wp.data.select('core/editor');
    let pId = getCurrentPostId();

    const isFseEditor = wp.data.select('core/edit-site');
    const _fsetype = isFseEditor?.getEditedPostType() || '';

    if ( _fsetype ) {
        const coreSelector = wp.data.select( 'core' );
        const dummyData1 = coreSelector?.getEntityRecords( 'postType', 'wp_template_part', {per_page: -1} );
        const dummyData2 = coreSelector?.getEntityRecords( 'postType', 'wp_template' , {per_page: -1} );

        // fse template, template part css
        let fseTempId = '';
        if ( typeof pId == 'string' && pId.includes('//') ) {
            fseTempId =  _fsetype == 'wp_template' ? pId : '';
            const fseAllTemplates = coreSelector?.getEntityRecords( 'postType', _fsetype , {per_page: -1} ) || [];
            const fseItem = fseAllTemplates.find( (item) => item.id == pId );
            pId = fseItem?.wp_id;
        }
        const blockCss = buildBlockCss({ blocks: all_blocks, pId: pId, preview: preview });
        savePostCssApi(
            { 
                post_id: pId, 
                block_css: blockCss[pId], 
                hasBlocks: !!blockCss[pId], 
                preview: preview, 
                bindCss: handleBindCss ? 'handleBindCss' : '',
                src: 'fse_type',
                fseTempId: fseTempId
            }
        ).then( data => {} );

        // fse theme save css corresponding to postID if have
        const currentPage = isFseEditor?.getPage();
        if ( currentPage?.hasOwnProperty('context') && currentPage?.context?.postId ) {
            const pId = currentPage.context.postId;
            const pageBlocks = wp.data.select( 'core' ).getEditedEntityRecord('postType', currentPage.context?.postType, currentPage.context?.postId)?.blocks || [];
            const blockCss = buildBlockCss({ blocks: pageBlocks, pId: pId, preview: preview });
            savePostCssApi(
                { 
                    post_id: pId, 
                    block_css: blockCss[pId], 
                    hasBlocks: !!blockCss[pId], 
                    preview: preview, 
                    bindCss: handleBindCss ? 'handleBindCss' : '',
                    src: 'fse_pageId'
                }
            ).then( data => {} );
        }
    } else {
        const blockCss = buildBlockCss({ blocks: all_blocks, pId: pId, preview: preview });
        savePostCssApi(
            { 
                post_id: pId, 
                block_css: blockCss[pId], 
                hasBlocks: !!blockCss[pId], 
                preview: preview, 
                bindCss: handleBindCss ? 'handleBindCss' : '',
                src: 'all_editor'
            }
        ).then( data => {} );
    }
}

const ParseWidgetGen = (all_blocks) => {
    let __CSS = ''
    const { getBlockAttributes } = wp.data.select('core/block-editor')
    all_blocks.forEach( block => {
        const attr = getBlockAttributes(block.id)
        __CSS += CssGenerator( attr, block.name, attr.blockId, true )
    });
    savePostCssApi(
        { 
            post_id: 'ultp-widget', 
            block_css: __CSS, 
            hasBlocks: !!__CSS, 
            preview: false, 
            src: 'widget'
        }
    ).then( data => {} );
}

export { ParseCssGen, ParseWidgetGen }