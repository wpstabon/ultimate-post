import StarterPacks from "../dashboard/starter_pack/StarterPacks";

const { __ } = wp.i18n
const { parse } = wp.blocks
const { Fragment, useState, useEffect, useRef } = wp.element

const Library = (props) => {

    const [ wishListArr , setWishlistArr] = useState([]);
    const [state, setState] = useState({
        isPopup: (props.isShow || false),
        starterLists: [],
        designs: [],
        starterChildLists: [],
        starterParentLists: [],
        reloadId: '',
        reload: false,
        isStarterLists: true,
        error: false,
        fetching: false,
        starterListsFilter: 'all',
        designFilter: 'all',
        current: [],
        sidebarOpen: true,
        templatekitCol: 'ultp-templatekit-col3',
        loading: false
    });
    const [starterListModule, setStarterListModule] = useState('');

    const isBuilder = ["singular", "archive", "header", "footer", "404"].includes(ultp_data.archive);
    const isHomePage = ultp_data.archive == 'front_page';
    const { isPopup, isStarterLists, starterListsFilter, designFilter, fetching, starterLists, designs } = state;

    const fetchTemplates = async () => {
        
        setState({...state, loading: true});
        const _path = '/ultp/v2/fetch_premade_data';
        wp.apiFetch({
            path: _path,
            method: 'POST',
            data: {
                type: 'get_starter_lists_nd_design'
            }
        })
        .then((response) => {
            if (response.success) {
                if(response.data){
                    const data = response.data;
                    if ( isBuilder || isHomePage ) {
                        setState({...state, current: handleBuilderPremade(JSON.parse(data.starter_lists)), loading: false, isStarterLists: false})
                    } else {
                        let designData = handleDesignData(JSON.parse(data.design));
                        const starter_lists = JSON.parse(data.starter_lists);
                        const {parentObj, childArr} = splitStarterParentChild(starter_lists);
                        setState({...state, starterLists: starter_lists, designs: designData, current: isStarterLists ? starter_lists : designData, loading: false, starterChildLists: childArr, starterParentLists: parentObj})
                    }
                }
            }
        })
    }

    const handleBuilderPremade = ( data ) => {
        const builderPages = [];
        data.forEach((item) => {
            item.templates.forEach((val)=> {
                let temp = { ...val, parentID: item.ID };
                if ( isHomePage ) {
                    if ( temp.type == 'page' && temp.home_page == 'home_page' ) {
                        temp = { ...temp, }
                        builderPages.push(temp);
                    }
                } else {
                    if ( temp.type == 'ultp_builder' && ultp_data.archive == temp.builder_type ) {
                        builderPages.push(temp)
                    }
                }
            })
        })
        return builderPages;
    }

    const handleDesignData = (data) => {
        const transformedData = [];
        for (const category in data) {
            data[category].forEach(item => {
                item.category = category;
                transformedData.push(item);
            });
        }
        return transformedData;
    }

    const handleClickOutside = (e) => {
        if (e.keyCode === 27) {
            document.querySelector( '.ultp-builder-modal' ).remove();
            setState({...state, isPopup:false})
        }
    }

    useEffect(() => {
        setWListAction('', '', 'fetchData');
        document.addEventListener('keydown', handleClickOutside);
        fetchTemplates();
        return () => document.removeEventListener('keydown', handleClickOutside);
    },[]);

    const close = () => {
        const element = document.querySelector( '.ultp-builder-modal' );
        if (element.length > 0) {
            element.remove();
        }
        setState({...state, isPopup:false});
    }

    const _changeVal = (templateID, isPro) => {
        if (isPro) {
            if (ultp_data.active) {
                insertBlock(templateID);
            }
        }else{
            insertBlock(templateID);
        }
    }

    const insertBlock = async templateID => {
        setState({...state, reload:true, reloadId:templateID});

        window.fetch('https://ultp.wpxpo.com/wp-json/restapi/v2/single-template', {
            method: 'POST',
            body: new URLSearchParams('license='+ultp_data.license+'&template_id='+templateID)
        })
        .then(response => response.text())
        .then((jsonData) => {
            jsonData = JSON.parse(jsonData)
            if (jsonData.success && jsonData.rawData) {
                wp.data.dispatch('core/block-editor').insertBlocks(parse(jsonData.rawData));
                close();
                setState({...state, isPopup:false, reload:false, reloadId:'', error: false});
            } else {
                setState({...state, error: true});
            }
        })
        .catch((error) => {
            console.error(error);
        })
    }

    const _fetchFile = () => {
        setState({...state, fetching: true})
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'fetch_all_data'
            }
        })
        .then((response) => {
            if (response.success) {
                fetchTemplates();
                setState({...state, fetching: false})
            }
        })
    }

    const splitArchiveData = (data=[] , key='') => {
        const newData = data.filter( (item) => {
            const newItem = key ? ( Array.isArray(item.category) ? item?.category?.filter( v => key == 'all' ? true : v.slug == key) : item.category == key ) : ( item?.category.filter( v => ["singular", "header", "footer", "404"].includes(ultp_data.archive) ? v.slug == ultp_data.archive : !["singular", "header", "footer", "404"].includes(v.slug)) );
            return Array.isArray(newItem) ? ( newItem?.length > 0 ? newItem : false ) : newItem ;
        });
        return newData;
    }
    const setWListAction = (id, action='', type='') => {
        wp.apiFetch({
            path: '/ultp/v2/premade_wishlist_save',
            method: 'POST',
            data: {
                id: id,
                action: action,
                type: type
            }
        })
        .then((res) => {
            if(res.success) {
                setWishlistArr( Array.isArray(res.wishListArr) ? res.wishListArr : Object.values(res.wishListArr || {}));
            }
        })
    }
    const splitStarterParentChild = (starterData, type='') => {
        const childArr = [];
        const parentObj = {};
        starterData.forEach((item, index) => {
            childArr.push(...item.templates);
            parentObj[item.title] = {
                'pro': item.pro || '',
                'live': item.live,
                'ID': item.ID
            };
        });

        return {
            childArr: childArr,
            parentObj: parentObj
        };
    }
    
    const filterValue = isStarterLists ? starterListsFilter : designFilter;

    return (
        <Fragment>
            { isPopup &&
                <div className="ultp-builder-modal-shadow">
                    <div className="ultp-popup-wrap">
                        <div className="ultp-popup-header">
                            <div className="ultp-popup-filter-title">
                                <div className="ultp-popup-filter-image-head">
                                    <img src={ultp_data.url+'assets/img/logo-sm.svg'}/>
                                    <span>{isBuilder ? __("Builder Library","ultimate-post") : __("Template Kits","ultimate-post")}</span>
                                </div>
                                { !isBuilder &&
                                    <div className="ultp-popup-filter-nav">
                                        <div className={'ultp-popup-tab-title'+(isStarterLists ? ' ultp-active' : '')}
                                            onClick={ () => { 
                                                    setStarterListModule(''); 
                                                    setState({...state, isStarterLists: true, starterListsFilter: 'all', current: starterLists})
                                                }
                                            }
                                        >
                                            {__('Starter Packs','ultimate-post')}
                                        </div>
                                        <div className={'ultp-popup-tab-title'+(isStarterLists ? '' : ' ultp-active')}
                                            onClick={ () => { 
                                                    setStarterListModule(''); 
                                                    setState({...state, isStarterLists: false, designFilter: 'all', current: designs})
                                                } 
                                            } 
                                        >
                                            {__('Premade Patterns','ultimate-post')}
                                        </div>
                                    </div>
                                }
                                <div className="ultp-popup-filter-sync-close">
                                    <span className={'ultp-popup-sync'} onClick={() => _fetchFile()}><i className={'dashicons dashicons-update-alt' + ( fetching ? ' rotate' : '')}/>{__('Synchronize','ultimate-post')}</span>
                                    <button className="ultp-btn-close" onClick={() => close()} id="ultp-btn-close"><span className="dashicons dashicons-no-alt"></span></button>
                                </div>
                            </div>
                        </div>

                        <StarterPacks 
                            filterValue = { filterValue }
                            state = { state }
                            setState = { setState }
                            useState={useState}
                            useEffect={useEffect}
                            useRef={useRef}
                            _changeVal = { _changeVal }
                            splitArchiveData = { splitArchiveData }
                            setWListAction = {(id , action) => setWListAction(id , action)}
                            wishListArr={wishListArr}
                            setWishlistArr={setWishlistArr}
                            starterListModule={starterListModule}
                            setStarterListModule={setStarterListModule}
                        />
                    </div>
                </div>
            }
        </Fragment>
    );
}

export default Library;
