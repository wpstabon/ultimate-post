/** @format */

const { __ } = wp.i18n;
import { attrBuild } from '../helper/CommonPanel';

export const SETTING_SECTIONS = {
	general: false,
	query_builder: false,
	heading: false,
	title: false,
	meta: false,
	'taxonomy-/-category': false,
	tax: false,
	image: false,
	video: false,
	wrap: false,
	excerpt: false,
	filter: false,
	pagination: false,
	'read-more': false,
	separator: false,
	
	dc_field: false, // Extra Field
	dc_group: false,
	arrow: false,
	dot: false,
};

export const stateObj = {
	postsList: [],
	loading: true,
	error: false,
	section: {
		...SETTING_SECTIONS,
		general: true,
	},
	toolbarSettings: '',
	selectedDC: '',
};

export const fetchProducts = (attributes, state, setState, ) => {
	if (state.error) {
		setState({ ...state, error: false });
	}
	if (!state.loading) {
		setState({ ...state, loading: true });
	}
	wp.apiFetch({
		path: '/ultp/fetch_posts',
		method: 'POST',
		data: attrBuild(attributes),
	})
	.then((obj) => {
		setState({ ...state, postsList: obj, loading: false });
	})
	.catch((error) => {
		setState({ ...state, loading: false, error: true });
	});
}

const proLink =
	'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=quick-query&utm_campaign=postx-dashboard';

export const pagiAjaxDisableText = (
	<>
		AJAX Pagination must be used with Advanced Filter.
		<br />
		<strong>Remove Advance Filter first to use non-AJAX pagination</strong>
	</>
);

// Attributes that should be synced when user switches from builtin pagination block
// to the independent Adv Pagination block
const pagiAttr = [
	'pagiAlign',
	'pagiArrowSize',
	'pagiBgColor',
	'pagiBorder',
	'pagiColor',
	'pagiHoverBorder',
	'pagiHoverColor',
	'pagiHoverRadius',
	'pagiHoverShadow',
	'pagiHoverbg',
	'pagiMargin',
	'pagiPadding',
	'pagiRadius',
	'pagiShadow',
	'pagiTypo',
	'paginationNav',
	'paginationText',
	'paginationType',
];


const quickQuery = [
	{
		value: 'popular_post_1_day_view',
		label: __('Trending Today', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_7_days_view',
		label: __('This Week’s Popular Posts', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_30_days_view',
		label: __('Top Posts of the Month', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_all_times_view',
		label: __('All-Time Favorites', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post',
		label: __('Random Posts', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post_7_days',
		label: __('Random Posts (7 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post_30_days',
		label: __('Random Posts (30 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'latest_post_published',
		label: __('Latest Posts - Published Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'latest_post_modified',
		label: __('Latest Posts - Last Modified Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'oldest_post_published',
		label: __('Oldest Posts - Published Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'oldest_post_modified',
		label: __('Oldest Posts - Last Modified Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'alphabet_asc',
		label: __('Alphabetical ASC', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'alphabet_desc',
		label: __('Alphabetical DESC', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'sticky_posts',
		label: __('Sticky Post', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment',
		label: __('Most Comments', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_1_day',
		label: __('Most Comments (1 Day)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_7_days',
		label: __('Most Comments (7 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_30_days',
		label: __('Most Comments (30 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
];

const COMPATIBLE_BLOCKS_PREFIX = ['post-grid', 'post-list', 'post-module'];

/**
 *
 * @param {string} name
 * @returns {boolean}
 */
export const isCompatibleBlock = (name) => {
	if (name.startsWith('ultimate-post/')) {
		const [, blockPrefix] = name.split('/');
		return COMPATIBLE_BLOCKS_PREFIX.some((prefix) =>
			blockPrefix.startsWith(prefix)
		);
	}
	return false;
};

/**
 *
 * @param {string} clientId
 * @returns {object | null}
 */
const getFilterBlock = (clientId) => {
	const { getBlocks, getBlockRootClientId } =
		wp.data.select('core/block-editor');

	const parentClientId = getBlockRootClientId(clientId);

	if (clientId) {
		const siblingBlocks = getBlocks(parentClientId);
		const filterBlock = siblingBlocks.filter(
			(child) => child.name === 'ultimate-post/advanced-filter'
		);
		return filterBlock[0] ? filterBlock[0] : null;
	}

	return null;
};

/**
 *
 * @param {string} clientId
 * @returns {object | null}
 */
const getSelectFilterBlock = (clientId, variationName) => {
	const { getBlocks } = wp.data.select('core/block-editor');
	const parentFilterBlockCId = getFilterBlock(clientId);
	const siblingBlocks = getBlocks(parentFilterBlockCId);
	const filterBlock = siblingBlocks.filter(
		(child) =>
			child.name === 'ultimate-post/filter-select' &&
			child.attributes?.type === variationName
	);
	return filterBlock[0] ? filterBlock[0] : null;
};

/**
 *
 * @param {string} clientId
 * @returns {object | null}
 */
const getPaginationBlock = (clientId) => {
	const { getBlocks, getBlockRootClientId } =
		wp.data.select('core/block-editor');

	const parentClientId = getBlockRootClientId(clientId);

	if (clientId) {
		const siblingBlocks = getBlocks(parentClientId);
		const paginationBlock = siblingBlocks.filter(
			(child) => child.name === 'ultimate-post/post-pagination'
		);
		return paginationBlock[0] ? paginationBlock[0] : null;
	}

	return null;
};

export const getGridBlocks = (clientId, gridBlockId) => {
	const { getBlocks } = wp.data.select('core/block-editor');
	const childBlocks = getBlocks(clientId);
	const filteredBlocks = childBlocks.filter((block) =>
		isCompatibleBlock(block.name)
	);
	return filteredBlocks;
};

const getPostTypes = (clientId) => {
	const { getBlocks, getBlockRootClientId } =
		wp.data.select('core/block-editor');
	const parentClientId = getBlockRootClientId(clientId);

	if (clientId) {
		const siblingBlocks = getBlocks(parentClientId);
		const gridBlocks = siblingBlocks.filter((child) =>
			isCompatibleBlock(child.name)
		);

		return gridBlocks.map((block) => block.attributes.queryType);
	}

	return null;
};

const insertFilterBlock = (clientId, pClientId) => {
	const { getBlock, getBlockRootClientId } =
		wp.data.select('core/block-editor');

	const { selectBlock, replaceBlock } = wp.data.dispatch('core/block-editor');
	const { createBlock, cloneBlock } = wp.blocks;

	const filterBlock = getFilterBlock(clientId);

	if (!filterBlock) {
		const parentClientId = pClientId
			? pClientId
			: getBlockRootClientId(clientId);
		const parent = getBlock(parentClientId);

		const clonedBlock = cloneBlock(getBlock(clientId));
		const advFilterBlock = createBlock('ultimate-post/advanced-filter', {});

		const newBlocks =
			parent && parent.name === 'ultimate-post/post-grid-parent'
				? [advFilterBlock, clonedBlock]
				: createBlock('ultimate-post/post-grid-parent', {}, [
						advFilterBlock,
						clonedBlock,
					]);

		replaceBlock(clientId, newBlocks).then(() => {
			selectBlock(advFilterBlock.clientId);
		});
	}
};

const removeFilterBlock = (clientId) => {
	const { removeBlock, selectBlock } = wp.data.dispatch('core/block-editor');

	const filterBlock = getFilterBlock(clientId);

	if (filterBlock) {
		removeBlock(filterBlock.clientId).then(() => {
			selectBlock(clientId);
		});
	}
};

const getPagiAttr = (oldBlock) => {
	if (!oldBlock) return;

	const newAttr = {};

	pagiAttr.forEach((attr) => {
		if (oldBlock.attributes[attr] !== undefined) {
			newAttr[attr] = oldBlock.attributes[attr];
		}
	});

	return newAttr;
};

export function insertPaginationBlock(clientId, pClientId) {
	const { getBlock, getBlockRootClientId } =
		wp.data.select('core/block-editor');
	const { replaceBlock } = wp.data.dispatch('core/block-editor');
	const { createBlock, cloneBlock } = wp.blocks;

	const pagiBlock = getPaginationBlock(clientId);

	if (!pagiBlock) {
		const parentClientId = pClientId
			? pClientId
			: getBlockRootClientId(clientId);
		const parent = getBlock(parentClientId);

		const oldBlock = getBlock(clientId);
		const clonedBlock = cloneBlock(oldBlock);
		const paginationBlock = createBlock(
			'ultimate-post/post-pagination',
			getPagiAttr(oldBlock)
		);

		if (parent && parent.name === 'ultimate-post/post-grid-parent') {
			replaceBlock(clientId, [clonedBlock, paginationBlock]).then(() => {
				document
					.querySelector(
						'[data-block="' + paginationBlock.clientId + '"]'
					)
					?.scrollIntoView({ behavior: 'smooth' });
			});
		} else {
			replaceBlock(
				clientId,
				createBlock('ultimate-post/post-grid-parent', {}, [
					clonedBlock,
					paginationBlock,
				])
			).then(() => {
				document
					.querySelector(
						'[data-block="' + paginationBlock.clientId + '"]'
					)
					?.scrollIntoView({ behavior: 'smooth' });
			});
		}
	}
}

export function removePaginationBlock(clientId) {
	const { removeBlock, selectBlock } = wp.data.dispatch('core/block-editor');

	const pagiBlock = getPaginationBlock(clientId);

	if (pagiBlock) {
		removeBlock(pagiBlock.clientId).then(() => {
			selectBlock(clientId);
		});
	}
}

function getData(type, defValues = {}) {
	const { useEntityRecords } = wp.coreData;
	const { queryOrder } = defValues;

	switch (type) {
		case 'tags':
			return useEntityRecords('taxonomy', 'post_tag', {
				per_page: -1,
			});
		case 'category':
			return useEntityRecords('taxonomy', 'category', {
				per_page: -1,
			});
		case 'author':
			return useEntityRecords('root', 'user', {
				per_page: -1,
			});
		case 'order':
			return {
				hasResolved: true,
				records:
					queryOrder && queryOrder !== 'desc'
						? [
								{
									id: 'asc',
									name: 'ASC',
								},
								{
									id: 'desc',
									name: 'DESC',
								},
							]
						: [
								{
									id: 'desc',
									name: 'DESC',
								},
								{
									id: 'asc',
									name: 'ASC',
								},
							],
			};
		case 'order_by':
			return {
				hasResolved: true,
				records: [
					{
						id: 'date',
						name: 'Created Date',
					},
					{
						id: 'modified',
						name: 'Date Modified',
					},
					{
						id: 'title',
						name: 'Title',
					},
					{
						id: 'menu_order',
						name: 'Menu Order',
					},
					{
						id: 'rand',
						name: 'Random',
					},
					{
						id: 'comment_count',
						name: 'Number of Comments',
					},
				],
			};
		case 'adv_sort':
			return {
				hasResolved: true,
				records: quickQuery.map((item) => ({
					id: item.value,
					name: item.label,
				})),
			};
		case 'custom_tax':
			return null;

		default:
			return null;
	}
}

function getDefMap(type, allText) {
	const results = new Map();

	if (!['order', 'order_by'].includes(type)) {
		results.set('_all', {
			label: allText,
		});
	}

	return results;
}

/**
 *
 * @param {string} type
 * @param {string} allText
 * @returns {['success' | 'error' | 'none' | 'loading', Map]}
 */
function getOptions(type, allText, defValues = {}) {
	let data = getData(type, defValues);
	const results = getDefMap(type, allText);

	if (data === null) {
		return ['none', results];
	}

	if (!data.hasResolved) {
		return ['loading', results];
	}

	if (data.hasResolved && !data.records) {
		return ['none', results];
	}

	if (data.hasResolved && data.status === 'ERROR') {
		return ['error', results];
	}

	if (type === 'author') {
		data.records.forEach((user) => {
			if (user.roles && user.roles.includes('author')) {
				const name = user.name;
				results.set(String(user.id), {
					label: name,
				});
			}
		});
	} else if (type === 'custom_tax') {
		data.records.forEach((record) => {
			results.set(String(record.slug), {
				label: record.labels.name,
			});
		});
	} else {
		data.records.forEach((record) => {
			results.set(String(record.id), {
				label: record.name,
			});
		});
	}

	return ['success', results];
}

function changeGridQuery(parentClientId, queryArgs) {
	const { getBlocks } = wp.data.select('core/block-editor');
	const { updateBlockAttributes } = wp.data.dispatch('core/block-editor');

	const grids = getBlocks(parentClientId).filter((block) =>
		isCompatibleBlock(block.name)
	);

	grids.forEach((grid) => {
		updateBlockAttributes(grid.clientId, queryArgs);
	});
}

function getTaxType(type) {
	switch (type) {
		case 'tags':
			return 'post_tag';
		case 'category':
			return 'category';
		default:
			return '';
	}
}

function getChangeGridQueryFunc(parentClientId, type) {
	return (queryArgs) => {
		changeGridQuery(parentClientId, {
			...queryArgs,
			queryTax: getTaxType(type),
		});
	};
}

function getSelectedFilters(filters) {
	if (filters.length === 0) {
		return [];
	}

	return filters
		.filter((filter) => filter.attributes.selectedLabel)
		.map((filter) => ({
			[filter.clientId]: filter.attributes.selectedLabel,
		}));
}

function getTitleCase(str) {
	return str.toLowerCase().replace(/\b(\w)/g, (s) => s.toUpperCase());
}

function clearFilter(clientId) {
	const { updateBlockAttributes } = wp.data.dispatch('core/block-editor');

	updateBlockAttributes(clientId, {
		selectedValue: '_all',
		selectedLabel: '',
	});
}

const getAllInnerBlocks = (blocks) => {
	let allInnerBlocks = [];
	blocks.forEach((block) => {
		allInnerBlocks.push(block);
		if (block.innerBlocks && block.innerBlocks.length > 0) {
			allInnerBlocks = allInnerBlocks.concat(
				getAllInnerBlocks(block.innerBlocks)
			);
		}
	});
	return allInnerBlocks;
};

export function scrollSidebarSettings(id) {
	function _scroll(_id) {
		// if(_id == "heading") { 
		// 	return false;
		// }
		const section = document.getElementById('ultp-sidebar-' + _id);
		if (section) {
			section.classList.add('ultp-sidebar-highlight');
			section.style.scrollMarginTop = '51px';
			section.scrollIntoView({
				behavior: 'smooth',
			});
			setTimeout(function () {
				section?.classList.remove('ultp-sidebar-highlight');
			}, 2000);
		} else {
			// throw new Error('section not found: ' + _id);
			console.warn('section not found: ' + _id);
		}
	}

	id = id.replace(/ /g, '-').toLowerCase();

	setTimeout(() => {
		try {
			_scroll(id);
		} catch {
			setTimeout(() => _scroll(id), 1500); // for slow devices
		}
	}, 500);
}

export function syncAttributesWithGrids(clientId, payload) {
	const { updateBlockAttributes } = wp.data.dispatch('core/block-editor');
	const gridBlocks = getGridBlocks(clientId);

	gridBlocks.forEach((block) => {
		updateBlockAttributes(block.clientId, payload);
	});
}

export function saveSelectedSection(title) {
	localStorage.setItem('ultp_selected_settings_section', title);
}

export function saveToolbar(title) {
	localStorage.setItem('ultp_selected_toolbar', title);
}

export function resetState() {
	if (localStorage.getItem('ultp_settings_reset_disabled') !== 'true') {
		localStorage.removeItem('ultp_selected_settings_section');
		localStorage.removeItem('ultp_selected_toolbar');
		localStorage.removeItem('ultp_settings_save_state');
		localStorage.removeItem('ultp_prev_sel_block');
		localStorage.removeItem('ultp_settings_active_tab');
	}
}

export function saveTab(title, tab) {
	localStorage.setItem('ultp_settings_active_tab', title + '###' + tab);
}

export function restoreTab(title) {
	if (localStorage.getItem('ultp_settings_save_state') === 'true') {
		const tabData = localStorage.getItem('ultp_settings_active_tab');
		if (tabData) {
			const [savedTitle, tab] = tabData.split('###');
			return savedTitle === title ? tab : undefined;
		}
	}

	return undefined;
}

export function restoreState(sectionCb, toolbarCb) {
	const shouldRestore =
		localStorage.getItem('ultp_settings_save_state') === 'true';
	if (shouldRestore) {
		const savedSection = localStorage.getItem(
			'ultp_selected_settings_section'
		);
		if (savedSection) {
			sectionCb(savedSection);
		}

		const savedToolbar = localStorage.getItem('ultp_selected_toolbar');
		if (savedToolbar) {
			toolbarCb(savedToolbar);
		}

		resetState();
	}
}

export function handleAdvOptions(
	context,
	advFilterEnable,
	store,
	advPaginationEnable
) {
	let parentClientId = null;

	if (context) {
		parentClientId = context['post-grid-parent/postBlockClientId'];
	}

	// If the user has deleted the adv filter block manually,
	// update the attribute
	const filterBlock = getFilterBlock(store.clientId);
	if (advFilterEnable && filterBlock === null) {
		store.setAttributes({
			advFilterEnable: false,
		});
		removeFilterBlock(store.clientId);
	}

	// If the user has deleted the adv pagination block manually,
	// update the attribute
	const pagiBlock = getPaginationBlock(store.clientId);
	if (advPaginationEnable && pagiBlock === null) {
		store.setAttributes({
			advPaginationEnable: false,
		});
		removePaginationBlock(store.clientId);
	}
}

export {
	clearFilter,
	getAllInnerBlocks,
	getChangeGridQueryFunc,
	getOptions,
	getPostTypes,
	getSelectedFilters,
	getTitleCase,
	insertFilterBlock,
	removeFilterBlock
};

