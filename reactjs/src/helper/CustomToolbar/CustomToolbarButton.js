import IconPack from "../fields/tools/IconPack";

const { Dropdown } = wp.components;

function ToolbarButton({label, icon, onClick}) {
    return (
        <div className="components-dropdown" tabIndex={-1} onClick={onClick}>
            <span>
                <button
                    type="button"
                    data-command=""
                    id=":r3i:"
                    data-toolbar-item="true"
                    className="components-button components-toolbar-button has-icon"
                    aria-label={label}
                    tabIndex={-1}
                >
                    {icon}
                </button>
            </span>
        </div>
    );
}

export default function CustomToolbarButton() {
    return (
        <div className="components-dropdown" tabIndex={-1}>
            <Dropdown
                contentClassName="ultp-custom-toolbar-wrapper"
                renderToggle={({ onToggle }) => (
                    <span>
                        <ToolbarButton
                            label={"Settings"}
                            icon={IconPack["cog_line"]}
                            onClick={() => onToggle()}
                        />
                    </span>
                )}
                renderContent={() => {
                    <p>Settings</p>
                }}
            />
        </div>
    );
}
