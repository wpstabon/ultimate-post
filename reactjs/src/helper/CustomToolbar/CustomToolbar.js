import CustomToolbarButton from "./CustomToolbarButton";

export default function CustomToolbar({isShow, children}) {

    if (!isShow) return null;

    return (
        <div
            className="block-editor-block-list__block-popover is-positioned is-unstyled"
            tabIndex={-1}
            style={{
                position: "absolute",
                top: "-100%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "white"
            }}
        >
            <div className="components-popover__content">
                <div
                    role="toolbar"
                    aria-orientation="horizontal"
                    aria-label="Block tools"
                    className="components-accessible-toolbar has-parent"
                >
                    <div className="block-editor-block-toolbar">
                        <div>
                            <div className="components-toolbar-group block-editor-block-toolbar__block-controls">
                                <div className="components-toolbar-group">
                                    <div
                                        className="components-dropdown components-dropdown-menu block-editor-block-switcher"
                                        tabIndex={-1}
                                    >
                                        <button
                                            type="button"
                                            data-command=""
                                            id=":r3a:"
                                            data-toolbar-item="true"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            className="components-button components-dropdown-menu__toggle has-icon"
                                            aria-label="Post Pagination"
                                            aria-describedby="components-button__description-123"
                                            tabIndex={-1}
                                        >
                                            <span className="block-editor-block-icon block-editor-block-switcher__toggle has-colors">
                                                <span className="dashicon dashicons dashicons-smiley" />
                                            </span>
                                        </button>
                                        <div
                                            data-wp-c16t="true"
                                            data-wp-component="VisuallyHidden"
                                            className="components-visually-hidden css-0 e19lxcc00"
                                            style={{
                                                border: 0,
                                                clip: "rect(1px, 1px, 1px, 1px)",
                                                clipPath: "inset(50%)",
                                                height: 1,
                                                margin: "-1px",
                                                overflow: "hidden",
                                                padding: 0,
                                                position: "absolute",
                                                width: 1,
                                                overflowWrap: "normal",
                                            }}
                                        >
                                            <span id="components-button__description-123">
                                                Post Pagination: Change block
                                                type or style
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {children}

                        {/* <div className="components-toolbar-group">
                            <div
                                className="components-dropdown components-dropdown-menu block-editor-block-settings-menu"
                                tabIndex={-1}
                            >
                                <button
                                    type="button"
                                    data-command=""
                                    id=":r3g:"
                                    data-toolbar-item="true"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    className="components-button components-dropdown-menu__toggle has-icon"
                                    aria-label="Options"
                                    tabIndex={-1}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24"
                                        width={24}
                                        height={24}
                                        aria-hidden="true"
                                        focusable="false"
                                    >
                                        <path d="M13 19h-2v-2h2v2zm0-6h-2v-2h2v2zm0-6h-2V5h2v2z" />
                                    </svg>
                                </button>
                            </div>
                        </div> */}

                    </div>
                </div>
            </div>
        </div>
    );
}
