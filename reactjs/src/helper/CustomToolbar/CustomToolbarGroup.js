export default function CustomToolbarGroup({ children }) {
    return (
        <div className="block-editor-block-toolbar__slot css-0 e19lxcc00">
            <div className="components-toolbar-group">{children}</div>
        </div>
    );
}
