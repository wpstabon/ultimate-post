import IconPack from "../../fields/tools/IconPack";
const { __ } = wp.i18n;

export default function Pagination({
    paginationNav,
    paginationAjax,
    paginationText = "",
    pages = 4,
    onClick,
}) {
    const data = paginationText.split("|");
    const prevText = data[0] || __("Previous", "ultimate-post");
    const nextText = data[1] || __("Next", "ultimate-post");

    return (
        <div
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={
                "ultp-pagination-wrap ultp-disable-editor-click" +
                (paginationAjax ? " ultp-pagination-ajax-action" : "") + " ultp-component"
            }
        >
            <ul className={`ultp-pagination ultp-component-hover`}>
                <li>
                    <a href="#" className={`ultp-prev-page-numbers`}>
                        {IconPack.leftAngle2}
                        {paginationNav == "textArrow" ? " " + prevText : ""}
                    </a>
                </li>
                {pages > 4 && (
                    <li className={`ultp-first-dot`}>
                        <a href="#">...</a>
                    </li>
                )}
                {new Array(pages > 2 ? 3 : pages).fill(0).map((v, i) => (
                    <li key={i}>
                        <a href="#">{i + 1}</a>
                    </li>
                ))}
                {pages > 4 && (
                    <li className={`ultp-last-dot`}>
                        <a href="#">...</a>
                    </li>
                )}
                {pages > 5 && (
                    <li className={`ultp-last-pages`}>
                        <a href="#">{pages}</a>
                    </li>
                )}
                <li>
                    <a href="#" className={`ultp-next-page-numbers`}>
                        {paginationNav == "textArrow" ? nextText + " " : ""}
                        {IconPack.rightAngle2}
                    </a>
                </li>
            </ul>
        </div>
    );
}
