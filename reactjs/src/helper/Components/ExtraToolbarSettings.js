import { ToolbarSettings } from "../CommonPanel";

const { Dropdown, ToolbarButton } = wp.components;

export default function ExtraToolbarSettings({
    store,
    include,
    buttonContent,
    label,
}) {

    return (
        <Dropdown
            contentClassName="ultp-custom-toolbar-wrapper"
            renderToggle={({ onToggle }) => (
                <span>
                    <ToolbarButton onClick={() => onToggle()} label={label}>
                        <span className="ultp-click-toolbar-settings">
                            {buttonContent}
                        </span>
                    </ToolbarButton>
                </span>
            )}
            renderContent={() => (
                <ToolbarSettings store={store} include={include} />
            )}
        />
    );
}
