const {Panel, PanelBody} = wp.components;

export default function DeperciatedSettings({children, open}) {
    return (
        <Panel className="ultp-depr-panel">
            <PanelBody title="Depreciated Settings" initialOpen={open}>
                {children}
            </PanelBody>
        </Panel>
    );
}
