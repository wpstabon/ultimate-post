/** @format */

import { styleIcon } from '../CommonPanel';
import IconPack from '../fields/tools/IconPack';
import useStyleButtonToggle from '../hooks/use-style-button';

const DisabledImage = ({ onClick }) => {
	const { showStyleButton, StyleButton } = useStyleButtonToggle(onClick);

	return (
		<div
			className={`ultp-block-image ultp-block-empty-image`}
			onClick={showStyleButton}
		>
			{StyleButton}
		</div>
	);
};

export default function Image1({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	imgSize,
	fallbackEnable,
	vidIconEnable,
	idx,
	catPosition,
	Category,
	onClick,
	vidOnClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	return (
		<div
			className={`ultp-block-image ultp-block-image-${imgAnimation} ${
				imgOverlay === true
					? 'ultp-block-image-overlay ultp-block-image-' +
						imgOverlayType +
						' ' +
						'ultp-block-image-' +
						imgOverlayType +
						idx
					: ''
			} ultp-component-simple`}
			onClick={showStyleButton}
		>
			{StyleButton}
			<a className="ultp-component-hover">
				<img
					alt={post.title || ''}
					src={
						post.image
							? post.image[imgSize]
							: vidIconEnable && post.has_video
								? `https://img.youtube.com/vi/` +
									post.has_video +
									`/0.jpg`
								: fallbackEnable &&
									ultp_data.url +
										`assets/img/ultp-fallback-img.png`
					}
				/>
			</a>
			{vidIconEnable && post.has_video && <Video onClick={vidOnClick} />}

			{Category}
		</div>
	);
}

export function Video({ onClick }) {
	return (
		<div
			onClick={(e) => {
				e.stopPropagation();
				onClick();
			}}
			className="ultp-video-icon"
		>
			{IconPack.play_line}
		</div>
	);
}

export function Image2({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	catPosition,
	Category,
	showImage,
	imgCrop,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' '
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}

				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[imgCrop]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>

				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image3({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	catPosition,
	imgCropSmall,
	showImage,
	imgCrop,
	idx,
	showSmallCat,
	Category,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};
	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);
	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' '
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[idx == 0 ? imgCrop : imgCropSmall]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>
				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image4({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	imgCropSmall,
	showImage,
	imgCrop,
	idx,
	Category,
	Video = null,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[idx == 0 ? imgCrop : imgCropSmall]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>
				{Video}
				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image5({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	catPosition,
	showImage,
	idx,
	showSmallCat,
	imgSize,
	Category,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[imgSize]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>
				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image6({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	catPosition,
	showImage,
	idx,
	showSmallCat,
	imgSize,
	Category,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-ux-style-btn-parent ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}

				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[imgSize]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>

				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image7({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	catPosition,
	showImage,
	idx,
	showSmallCat,
	imgSize,
	layout,
	Category,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}

				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[imgSize]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>

				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image8({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	showImage,
	idx,
	imgCrop,
	Category = null,
	Video = null,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (((post.image && !post.is_fallback) || fallbackEnable) && showImage) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[imgCrop]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>
				{Video}
				{Category}
			</div>
		);
	}

	return <DisabledImage onClick={_onClick} />;
}

export function Image9({
	imgOverlay,
	imgOverlayType,
	imgAnimation,
	post,
	fallbackEnable,
	vidIconEnable,
	showImage,
	idx,
	imgCrop,
	imgCropSmall,
	layout,
	Category = null,
	Video = null,
	onClick,
}) {
	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	if (
		((post.image && !post.is_fallback) || fallbackEnable) &&
		showImage &&
		(idx == 0 || (idx != 0 && layout === 'layout1') || layout === 'layout4')
	) {
		return (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true && idx == 0
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				<a href="#">
					<img
						alt={post.title || ''}
						src={
							post.image
								? post.image[idx == 0 ? imgCrop : imgCropSmall]
								: vidIconEnable && post.has_video
									? `https://img.youtube.com/vi/` +
										post.has_video +
										`/0.jpg`
									: fallbackEnable &&
										ultp_data.url +
											`assets/img/ultp-fallback-img.png`
						}
					/>
				</a>
				{Video}
				{Category}
			</div>
		);
	}

	return null;
}

export function Image10({
	attributes,
	post,
	idx,
	VideoContent,
	CatCotent,
	onClick,
}) {
	const {
		fallbackEnable,
		showImage,
		imgAnimation,
		imgOverlay,
		imgOverlayType,
		layout,
		imgCrop,
		vidIconEnable,
		imgCropSmall,
	} = attributes;

	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	return (
		((post.image && !post.is_fallback) || fallbackEnable) &&
		showImage &&
		(idx == 0 ||
			layout == 'layout3' ||
			layout == 'layout2' ||
			layout == 'layout5') && (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				{idx == 0 ? (
					<>
						<a href="#">
							<img
								alt={post.title || ''}
								src={
									post.image
										? post.image[imgCrop]
										: vidIconEnable && post.has_video
											? `https://img.youtube.com/vi/` +
												post.has_video +
												`/0.jpg`
											: fallbackEnable &&
												ultp_data.url +
													`assets/img/ultp-fallback-img.png`
								}
							/>
						</a>
						{VideoContent}
					</>
				) : (
					(layout === 'layout3' ||
						layout === 'layout2' ||
						layout === 'layout5') && (
						<>
							<a href="#">
								<img
									alt={post.title || ''}
									src={
										post.image
											? post.image[imgCropSmall]
											: vidIconEnable && post.has_video
												? `https://img.youtube.com/vi/` +
													post.has_video +
													`/0.jpg`
												: fallbackEnable &&
													ultp_data.url +
														`assets/img/ultp-fallback-img.png`
									}
								/>
							</a>
							{VideoContent}
						</>
					)
				)}

				{CatCotent}
			</div>
		)
	);
}

export function Image11({
	attributes,
	post,
	idx,
	VideoContent,
	CatCotent,
	onClick,
}) {
	const {
		fallbackEnable,
		showImage,
		imgAnimation,
		imgOverlay,
		imgOverlayType,
		layout,
		imgCrop,
		vidIconEnable,
		imgCropSmall,
	} = attributes;

	const _onClick = (e) => {
		e.stopPropagation();
		onClick();
	};

	const { showStyleButton, StyleButton } = useStyleButtonToggle(_onClick);

	return (
		((post.image && !post.is_fallback) || fallbackEnable) &&
		showImage &&
		(idx == 0 ||
			layout == 'layout3' ||
			layout == 'layout2' ||
			layout == 'layout5') && (
			<div
				className={`ultp-block-image ultp-block-image-${imgAnimation} ${
					imgOverlay === true
						? 'ultp-block-image-overlay ultp-block-image-' +
							imgOverlayType +
							' ' +
							'ultp-block-image-' +
							imgOverlayType +
							idx
						: ''
				}`}
				onClick={showStyleButton}
			>
				{StyleButton}
				{idx == 0 ? (
					<>
						<a href="#">
							<img
								alt={post.title || ''}
								src={
									post.image
										? post.image[imgCrop]
										: vidIconEnable && post.has_video
											? `https://img.youtube.com/vi/` +
												post.has_video +
												`/0.jpg`
											: fallbackEnable &&
												ultp_data.url +
													`assets/img/ultp-fallback-img.png`
								}
							/>
						</a>
						{VideoContent}
					</>
				) : (
					(layout === 'layout3' ||
						layout === 'layout2' ||
						layout === 'layout5') &&
					((post.image && !post.is_fallback) || fallbackEnable) && (
						<>
							<a href="#">
								<img
									alt={post.title || ''}
									src={
										post.image
											? post.image[imgCropSmall]
											: vidIconEnable && post.has_video
												? `https://img.youtube.com/vi/` +
													post.has_video +
													`/0.jpg`
												: fallbackEnable &&
													ultp_data.url +
														`assets/img/ultp-fallback-img.png`
									}
								/>
							</a>
							{VideoContent}
						</>
					)
				)}

				{CatCotent}
			</div>
		)
	);
}

export function ImageSlider({
	imgOverlay,
	imgOverlayType,
	post,
	fallbackEnable,
	idx,
	imgCrop,
	onClick,
}) {
	return (
		<div
			className={`ultp-block-image ${
				imgOverlay === true
					? 'ultp-block-image-overlay ultp-block-image-' +
						imgOverlayType +
						' ' +
						'ultp-block-image-' +
						imgOverlayType +
						idx
					: ''
			}`}
		>
			<a
				href="#"
				className="ultp-component-hover"
				onClick={(e) => e.preventDefault()}
			>
				<img
					alt={post.title || ''}
					src={
						post.image
							? post.image[imgCrop]
							: fallbackEnable &&
								ultp_data.url +
									`assets/img/ultp-fallback-img.png`
					}
				/>
			</a>
		</div>
	);
}

export function SliderContent({
	children,
	onClick,
	contentHorizontalPosition,
	contentVerticalPosition,
}) {
	const { showStyleButton, StyleButton } = useStyleButtonToggle(onClick);

	return (
		<div
			className={`ultp-block-content ultp-block-content-${contentVerticalPosition} ultp-block-content-${contentHorizontalPosition} ultp-component-hover`}
			onClick={showStyleButton}
		>
			{StyleButton}
			{children}
		</div>
	);
}
