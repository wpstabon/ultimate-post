import IconPack from "../fields/tools/IconPack";

const { __ } = wp.i18n;

export default function ReadMore({
    readMoreText,
    readMoreIcon,
    titleLabel = "",
    onClick,
}) {
    return (
        <div className="ultp-block-readmore">
            <a
                onClick={(e) => {
                    e.stopPropagation();
                    onClick();
                }}
                className="ultp-component-simple"
                aria-label={titleLabel}
            >
                {readMoreText ? readMoreText : __("Read More", "ultimate-post")}
                {IconPack[readMoreIcon]}
            </a>
        </div>
    );
}
