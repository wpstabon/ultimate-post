const { __ } = wp.i18n;
import {
	colorIcon,
	metaTextIcon,
	settingsIcon,
	styleIcon,
	typoIcon,
} from '../CommonPanel';
import UltpLinkGenerator from '../UltpLinkGenerator';

const { BlockControls } = wp.blockEditor;
const { Toolbar } = wp.components;
const { useSelect } = wp.data;

export default function UltpToolbarGroup({
	children,
	text,
	pro = false,
	textScroll = false,
}) {
	const isTopToolbarEnabled = useSelect((select) => {
		return select('core/preferences').get('core', 'fixedToolbar');
	});

	const freeUser = pro && !ultp_data.active;

	return (
		<BlockControls>
			<div className={`ultp-toolbar-group ultp-toolbar-group-bg`}>
				{text && !isTopToolbarEnabled && (
					<div
						className={`ultp-toolbar-group-text${isTopToolbarEnabled ? '-bottom' : ''} ${textScroll ? 'ultp-toolbar-text-ticker-wrapper' : ''}`}
					>
						<div className="ultp-toolbar-group-text-inner">
							{__(text, 'ultimate-post') +
								(freeUser ? ' (Pro)' : '')}
						</div>
					</div>
				)}
				{freeUser ? (
					<div className="ultp-toolbar-group-block">
						<div className="ultp-toolbar-group-block-overlay">
							<a
								href={UltpLinkGenerator(
									'https://www.wpxpo.com/postx/all-features/',
									'blockProFeat',
									ultp_data.affiliate_id
								)}
								target="_blank"
							>
								{__('Unlock It', 'ultimate-post')}
							</a>
						</div>

						<div className="ultp-toolbar-group-block-placeholder">
							{typoIcon}
							{colorIcon}
							{styleIcon}
							{metaTextIcon}
							{settingsIcon}
						</div>
					</div>
				) : (
					<Toolbar>{children}</Toolbar>
				)}
			</div>
		</BlockControls>
	);
}
