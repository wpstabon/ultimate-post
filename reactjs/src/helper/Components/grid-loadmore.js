export default function LoadMore({ loadMoreText, onClick }) {
    return (
        <div
            className={`ultp-loadmore ultp-component`}
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
        >
            <a className={`ultp-loadmore-action ultp-disable-editor-click ultp-component-hover`}>
                {loadMoreText}
            </a>
        </div>
    );
}
