export default function Excerpt({
    excerpt,
    excerpt_full,
    seo_meta,
    excerptLimit,
    showFullExcerpt,
    showSeoMeta,
    onClick,
}) {
    return (
        <div
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-excerpt ultp-component-simple`}
            dangerouslySetInnerHTML={{
                __html: showSeoMeta
                    ? seo_meta.split(" ").splice(0, excerptLimit).join(" ")
                    : showFullExcerpt
                    ? excerpt_full
                    : excerpt.split(" ").splice(0, excerptLimit).join(" ") +
                      "...",
            }}
        ></div>
    );
}
