import Heading from "../block_template/Heading";
import Filter from "./grid-filter";
import Navigation from "./grid-nav";

export default function GHeading({
    attributes,
    setAttributes,
    onClick,
    setSection,
    setToolbarSettings,
}) {
    const {
        headingShow,
        headingStyle,
        headingAlign,
        headingURL,
        headingText,
        headingBtnText,
        subHeadingShow,
        subHeadingText,
        headingTag,
        filterShow,
        paginationShow,
        queryType,
        filterText,
        filterType,
        filterValue,
        paginationType,
        navPosition,
    } = attributes;

    return (
        <div
            className={`ultp-heading-filter`}
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
        >
            <div className={`ultp-heading-filter-in`}>
                <Heading
                    props={{
                        headingShow,
                        headingStyle,
                        headingAlign,
                        headingURL,
                        headingText,
                        setAttributes,
                        headingBtnText,
                        subHeadingShow,
                        subHeadingText,
                        headingTag,
                    }}
                />
                {(filterShow || paginationShow) && (
                    <div className={`ultp-filter-navigation`}>
                        {filterShow &&
                            queryType != "posts" &&
                            queryType != "customPosts" && (
                                <Filter
                                    filterText={filterText}
                                    filterType={filterType}
                                    filterValue={filterValue}
                                    onClick={() => {
                                        setSection("filter");
                                        setToolbarSettings("filter");
                                    }}
                                />
                            )}
                        {paginationShow &&
                            paginationType == "navigation" &&
                            navPosition == "topRight" && (
                                <Navigation
                                    onClick={() => {
                                        setSection("pagination");
                                        setToolbarSettings("pagination");
                                    }}
                                />
                            )}
                    </div>
                )}
            </div>
        </div>
    );
}

export function GHeading2({
    attributes,
    setAttributes,
    onClick,
    setSection,
    setToolbarSettings,
}) {
    const {
        headingShow,
        headingStyle,
        headingAlign,
        headingURL,
        headingText,
        headingBtnText,
        subHeadingShow,
        subHeadingText,
        headingTag,
        filterShow,
        paginationShow,
        queryType,
        filterText,
        filterType,
        filterValue,
        paginationType,
        navPosition,
    } = attributes;

    return (
        <div
            className={`ultp-heading-filter`}
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
        >
            <div className={`ultp-heading-filter-in`}>
                <Heading
                    props={{
                        headingShow,
                        headingStyle,
                        headingAlign,
                        headingURL,
                        headingText,
                        setAttributes,
                        headingBtnText,
                        subHeadingShow,
                        subHeadingText,
                        headingTag,
                    }}
                />
                {(filterShow || paginationShow) &&
                    queryType != "posts" &&
                    queryType != "customPosts" && (
                        <div className={`ultp-filter-navigation`}>
                            {filterShow &&
                                queryType != "posts" &&
                                queryType != "customPosts" && (
                                    <Filter
                                        filterText={filterText}
                                        filterType={filterType}
                                        filterValue={filterValue}
                                        onClick={() => {
                                            setSection("filter");
                                            setToolbarSettings("filter");
                                        }}
                                    />
                                )}
                            {paginationShow &&
                                paginationType == "navigation" &&
                                navPosition == "topRight" && (
                                    <Navigation
                                        onClick={() => {
                                            setSection("pagination");
                                            setToolbarSettings("pagination");
                                        }}
                                    />
                                )}
                        </div>
                    )}
            </div>
        </div>
    );
}
