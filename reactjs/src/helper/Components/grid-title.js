export default function Title({ title, headingTag, titleLength, titleStyle, onClick }) {
    const Tag = `${headingTag}`;
    if (title && titleLength != 0) {
        let arr = title.split(" ");
        title =
            arr.length > titleLength
                ? arr.splice(0, titleLength).join(" ") + "..."
                : title;
    }
    return (
        <Tag
            className={`ultp-block-title ${
                titleStyle === "none" ? "" : `ultp-title-${titleStyle}`
            } `}
        >
            <a 
                className={"ultp-component-simple"}
                dangerouslySetInnerHTML={{ __html: title }} 
                onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    onClick(e);
                }}
            />
        </Tag>
    );
}
