const { __ } = wp.i18n;
import IconPack from "../fields/tools/IconPack";

export default function Navigation({ onClick }) {
    return (
        <div
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-next-prev-wrap ultp-disable-editor-click ultp-component`}
        >
            <ul className="ultp-component-hover">
                <li>
                    <a className={`ultp-prev-action ultp-disable`} href="#">
                        {IconPack.leftAngle2}
                        <span className={`screen-reader-text`}>
                            {__("Previous", "ultimate-post")}
                        </span>
                    </a>
                </li>
                <li>
                    <a className={`ultp-prev-action`} href="#">
                        {IconPack.rightAngle2}
                        <span className={`screen-reader-text`}>
                            {__("Next", "ultimate-post")}
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    );
}
