export default function Filter({
    filterText,
    filterType,
    filterValue,
    onClick,
}) {
    filterValue = filterValue.length > 2 ? filterValue : "[]";

    return (
        <div
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-filter-wrap ultp-disable-editor-click ultp-component`}
            data-taxtype={filterType}
        >
            <ul className={`ultp-flex-menu`}>
                {filterText && (
                    <li className={`filter-item ultp-component-hover`}>
                        <a className="filter-active" href="#">
                            {filterText}
                        </a>
                    </li>
                )}
                {JSON.parse(filterValue).map((v, k) => {
                    v = v.value ? v.value : v;
                    return (
                        <li key={k} className={`filter-item ultp-component-hover`}>
                            <a href="#">{v.replace(/-/g, " ")}</a>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}
