const { __ } = wp.i18n;
const { dateI18n } = wp.date;
import { _dateFormat } from "../CommonPanel";
import IconPack from "../fields/tools/IconPack";

export default function Meta({
    meta,
    post,
    metaSeparator,
    metaStyle,
    metaMinText,
    metaAuthorPrefix,
    metaDateFormat,
    authorLink,
    onClick,
}) {
    const disableClick = (e) => {
        e.preventDefault();
    };

    const authorImgOnly = meta.includes("metaAuthor") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-author ultp-component-simple ultp-block-meta-element`}
        >
            <img className={`ultp-meta-author-img`} src={post.avatar_url} />
        </span>
    ) : (
        ""
    );
    const authorImg = meta.includes("metaAuthor") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-author ultp-component-simple ultp-block-meta-element`}
        >
            <img className={`ultp-meta-author-img`} src={post.avatar_url} />{" "}
            {metaAuthorPrefix}{" "}
            <a
                href={authorLink ? post.author_link : "#"}
                onClick={disableClick}
            >
                {post.display_name}
            </a>
        </span>
    ) : (
        ""
    );
    const authorIcon = meta.includes("metaAuthor") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-author ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.user}
            <a
                href={authorLink ? post.author_link : "#"}
                onClick={disableClick}
            >
                {post.display_name}
            </a>
        </span>
    ) : (
        ""
    );
    const authorBy = meta.includes("metaAuthor") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-author ultp-component-simple ultp-block-meta-element`}
        >
            {metaAuthorPrefix}
            <a
                href={authorLink ? post.author_link : "#"}
                onClick={disableClick}
            >
                {post.display_name}
            </a>
        </span>
    ) : (
        ""
    );
    const date = meta.includes("metaDate") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-date ultp-component-simple ultp-block-meta-element`}
        >
            {dateI18n(_dateFormat(metaDateFormat), post.time)}
        </span>
    ) : (
        ""
    );
    const dateIcon = meta.includes("metaDate") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-date ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.calendar}
            {dateI18n(_dateFormat(metaDateFormat), post.time)}
        </span>
    ) : (
        ""
    );
    const dateModified = meta.includes("metaDateModified") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-date ultp-component-simple ultp-block-meta-element`}
        >
            {dateI18n(_dateFormat(metaDateFormat), post.timeModified)}
        </span>
    ) : (
        ""
    );
    const dateModifiedIcon = meta.includes("metaDateModified") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-block-date ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.calendar}
            {dateI18n(_dateFormat(metaDateFormat), post.timeModified)}
        </span>
    ) : (
        ""
    );
    const comments = meta.includes("metaComments") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-comment ultp-component-simple ultp-block-meta-element `}
        >
            {post.comments === 0
                ? post.comments + __(" comment", "ultimate-post")
                : post.comments + __(" comments", "ultimate-post")}
        </span>
    ) : (
        ""
    );
    const commentsIcon = meta.includes("metaComments") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-comment ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.comment}
            {post.comments}
        </span>
    ) : (
        ""
    );
    const view = meta.includes("metaView") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-view ultp-component-simple ultp-block-meta-element`}
        >
            {post.view == 0 ? "0 view" : post.view + " views"}
        </span>
    ) : (
        ""
    );
    const viewIcon = meta.includes("metaView") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-view ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.eye}
            {post.view}
        </span>
    ) : (
        ""
    );
    const postTime = meta.includes("metaTime") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-time ultp-component-simple ultp-block-meta-element`}
        >
            {post.post_time} {__("ago", "ultimate-post")}
        </span>
    ) : (
        ""
    );
    const postTimeIcon = meta.includes("metaTime") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-time ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.clock}
            {post.post_time} {__("ago", "ultimate-post")}
        </span>
    ) : (
        ""
    );
    const reading = meta.includes("metaRead") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-read ultp-component-simple ultp-block-meta-element`}
        >
            {post.reading_time} {metaMinText}
        </span>
    ) : (
        ""
    );
    const readingIcon = meta.includes("metaRead") ? (
        <span
            onClick={(e) => {
                e.stopPropagation();
                onClick();
            }}
            className={`ultp-post-read ultp-component-simple ultp-block-meta-element`}
        >
            {IconPack.book}
            {post.reading_time} {metaMinText}
        </span>
    ) : (
        ""
    );
    return (
        <div
            className={`ultp-block-meta ultp-block-meta-${metaSeparator} ultp-block-meta-${metaStyle}`}
        >
            {metaStyle == "noIcon" && (
                <>
                    {authorBy} {date} {dateModified} {comments} {view} {reading}{" "}
                    {postTime}
                </>
            )}
            {metaStyle == "icon" && (
                <>
                    {authorIcon} {dateIcon} {dateModifiedIcon} {commentsIcon}{" "}
                    {viewIcon} {postTimeIcon} {readingIcon}
                </>
            )}
            {metaStyle == "style2" && (
                <>
                    {authorBy} {dateIcon} {dateModifiedIcon} {commentsIcon}{" "}
                    {viewIcon} {postTimeIcon} {readingIcon}
                </>
            )}
            {metaStyle == "style3" && (
                <>
                    {authorImg} {dateIcon} {dateModifiedIcon} {commentsIcon}{" "}
                    {viewIcon} {postTimeIcon} {readingIcon}
                </>
            )}
            {metaStyle == "style4" && (
                <>
                    {authorIcon} {dateIcon} {dateModifiedIcon} {commentsIcon}{" "}
                    {viewIcon} {postTimeIcon} {readingIcon}
                </>
            )}
            {metaStyle == "style5" && (
                <>
                    <div className={`ultp-meta-media`}>{authorImgOnly}</div>
                    <div className={`ultp-meta-body`}>
                        {authorBy} {dateIcon} {dateModifiedIcon} {commentsIcon}{" "}
                        {viewIcon} {postTimeIcon} {readingIcon}
                    </div>
                </>
            )}
            {metaStyle == "style6" && (
                <>
                    {authorImgOnly} {authorBy} {dateIcon} {dateModifiedIcon}{" "}
                    {commentsIcon} {viewIcon} {postTimeIcon} {readingIcon}
                </>
            )}
        </div>
    );
}
