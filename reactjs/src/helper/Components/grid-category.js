export default function Category({
    post,
    catShow,
    catStyle,
    catPosition,
    customCatColor,
    onlyCatColor,
    onClick,
}) {
    return post.category && catShow ? (
        <div
            className={`ultp-category-grid ultp-category-${catStyle} ultp-category-${catPosition}`}
        >
            <div
                className={`ultp-category-in ultp-cat-color-${customCatColor}`}
            >
                {post.category.map((v, k) =>
                    customCatColor ? (
                        onlyCatColor ? (
                            <a
                                key={k}
                                className={`ultp-cat-${v.slug} ultp-component-simple`}
                                style={{ color: v.color || "#CE2746" }}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    onClick();
                                }}
                            >
                                {v.name}
                            </a>
                        ) : (
                            <a
                                key={k}
                                className={`ultp-cat-${v.slug} ultp-cat-only-color-${customCatColor} ultp-component-simple`}
                                style={{
                                    backgroundColor: v.color || "#CE2746",
                                }}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    onClick();
                                }}
                            >
                                {v.name}
                            </a>
                        )
                    ) : (
                        <a
                            key={k}
                            className={`ultp-cat-${v.slug} ultp-component-simple`}
                            onClick={(e) => {
                                e.stopPropagation();
                                onClick();
                            }}
                        >
                            {v.name}
                        </a>
                    )
                )}
            </div>
        </div>
    ) : null;
}
