import DesignTemplate from "./DesignTemplate";
const { __ } = wp.i18n

const { Modal } = wp.components
const { useState } = wp.element
const TemplateModal = ({ store, prev, designLibrary = true }) => {
    const [ isOpen, setOpen ] = useState( false );
    const TemplateModal = () => setOpen( true );
    const closeModal = () => setOpen( false );

    return (
        <>
            <div className="ultp-ready-design">
                <div>
                    {
                        designLibrary &&
                        <button className="ultp-ready-design-btns patterns" onClick={ TemplateModal }> {__('Design Library', 'ultimate-post')} </button>
                    }
                    
                    <a className="ultp-ready-design-btns preview" target="_blank" href={prev}>{__('Blocks Preview', 'ultimate-post')}</a>
                </div>
            </div>
            { isOpen && (
                <Modal isFullScreen onRequestClose={ closeModal }>
                    <DesignTemplate store={store} closeModal={ closeModal } />
                </Modal>
            ) }
        </>
    );
};

export default TemplateModal;
