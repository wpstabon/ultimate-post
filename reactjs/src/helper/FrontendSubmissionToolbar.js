
const { __ } = wp.i18n
const { BlockControls } = wp.blockEditor;
const { ToolbarButton, TextControl, Button, Dropdown } = wp.components;
const {  useState } = wp.element;
const { registerFormatType, toggleFormat, applyFormat, removeFormat } = wp.richText;

import icons from "./icons";


const Comment = ({ isActive, value, onChange, activeAttributes }) => {
    const [inputValue, setInputValue] = useState('');

    const onConfirm = () => {
        const format = { type: 'ultimate-post/fs-comment', attributes: { data_fs_comment: inputValue } };
        const newValue = applyFormat(value, format);
        onChange(newValue);
    };


    const fsUser = () => {
        return typeof ultpFsSettings != 'undefined';
    }

    return (
        <>
            {fsUser() && !activeAttributes.data_fs_comment ? '' : <BlockControls>
                <Dropdown
                    contentClassName="ultp-cs-toolbar-comment"
                    renderToggle={({ onToggle }) => (

                        <ToolbarButton
                            onClick={() => {
                                onToggle();
                            }}
                            label="Comment"
                            className="ultp-cs-toolbar-comment components-toolbar-group"
                            icon={isActive ? icons.fs_comment_selected : icons.fs_comment}
                        >
                        </ToolbarButton>

                    )}
                    renderContent={({ onToggle }) => (
                        <>
                            {activeAttributes && activeAttributes.data_fs_comment && (
                                <div className="ultp-fs-comment-view__wrapper">
                                    <div className="ultp-fs-comment-view">
                                        <div className="ultp-fs-comment">
                                            {activeAttributes.data_fs_comment}
                                        </div>
                                        <Button className="ultp-fs-comment-resolve" onClick={() => {
                                            const format = { type: 'ultimate-post/fs-comment', attributes: { comment: '' } };
                                            const newValue = toggleFormat(value, format);
                                            onChange(newValue);
                                            onToggle();
                                        }} label="Resolve Comment"> {__("Resolve Comment","ultimate-post")}  </Button>
                                    </div>
                                </div>
                            )}
                            {
                                !(activeAttributes && activeAttributes.data_fs_comment) && (
                                    <div className="ultp-fs-comment-input__wrapper">
                                        <TextControl
                                            __nextHasNoMarginBottom={true}
                                            label="Enter Comment"
                                            value={inputValue}
                                            className={'ultp-fs-comment-input'}
                                            onChange={(newValue) => setInputValue(newValue)}
                                        />
                                        <Button isPrimary className={'ultp-fs-comment-btn'} onClick={() => {
                                            onConfirm();
                                            onToggle();
                                        }}> {__("Add Comment","ultimate-post")}</Button>
                                    </div>

                                )
                            }
                        </>
                    )}
                />
            </BlockControls>}
        </>
    );
};

const Suggestion = ({ isActive, value, onChange, activeAttributes, ...props }) => {
    const [inputValue, setInputValue] = useState('');

    const onConfirm = () => {
        const format = { type: 'ultimate-post/fs-suggestion', attributes: { data_fs_suggestion: inputValue } };
        const newValue = applyFormat(value, format);
        onChange(newValue);
    };

    const fsUser = () => {
        return typeof ultpFsSettings != 'undefined';
    }

    return (
        <>
            {fsUser() && !activeAttributes.data_fs_suggestion ? '' : <BlockControls>
                <Dropdown
                    contentClassName="ultp-cs-toolbar-comment"
                    renderToggle={({ onToggle }) => (

                        <ToolbarButton
                            onClick={() => {
                                onToggle();
                            }}
                            label="Suggestion"
                            className="ultp-cs-toolbar-comment components-toolbar-group"
                            icon={isActive ? icons.fs_suggestion_selected : icons.fs_suggestion}
                        >
                        </ToolbarButton>

                    )}
                    renderContent={({ onToggle }) => (
                        <>
                            {activeAttributes && activeAttributes.data_fs_suggestion && (
                                <div className="ultp-fs-comment-view__wrapper">
                                    <div className="ultp-fs-comment-view">
                                        <div className="ultp-fs-comment">
                                            {activeAttributes.data_fs_suggestion}
                                        </div>
                                        <Button className="ultp-fs-comment-resolve" onClick={() => {
                                            const format = { type: 'ultimate-post/fs-suggestion', attributes: { data_fs_suggestion: '' } };
                                            const newValue = removeFormat(value, format);

                                            const suggestion = activeAttributes.fs_suggestion;
                                            const newString = value.text.substring(0, value.start) + activeAttributes.data_fs_suggestion + value.text.substring(value.end);
                                            onChange(wp.richText.create({ text: newString }));
                                            onToggle();

                                        }}> {__("Accept","ultimate-post")} </Button>
                                    </div>

                                </div>
                            )}
                            {
                                !(activeAttributes && activeAttributes.data_fs_suggestion) && (
                                    <div className="ultp-fs-comment-input__wrapper">
                                        <TextControl
                                            __nextHasNoMarginBottom={true}
                                            label="Enter Suggestion"
                                            value={inputValue}
                                            className={'ultp-fs-comment-input'}
                                            onChange={(newValue) => setInputValue(newValue)}
                                        />
                                        <Button isPrimary className={'ultp-fs-comment-btn'} onClick={() => {
                                            onConfirm();
                                            onToggle();
                                        }}> {__("Add Suggestion","ultimate-post")} </Button>
                                    </div>

                                )
                            }
                        </>
                    )}
                />
            </BlockControls>}
        </>
    );
};

if (ultp_data.settings['ultp_frontend_submission'] == 'true') {
    registerFormatType(
        'ultimate-post/fs-comment', {
        title: 'Frontend Submission Comment',
        tagName: 'span',
        className: 'ultp-fs-has-comment',
        attributes: {
            data_fs_comment: 'data-fs-comment',
        },
        edit: Comment
    }
    )
    registerFormatType(
        'ultimate-post/fs-suggestion', {
        title: 'Frontend Submission Suggestion',
        tagName: 'span',
        className: 'ultp-fs-has-suggestion',
        attributes: {
            data_fs_suggestion: 'data-fs-suggestion',
        },
        edit: Suggestion
    }
    )
}