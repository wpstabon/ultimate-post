import React from "react";
const { __ } = wp.i18n;
const { RichText } = wp.blockEditor;
import IconPack from "../../helper/fields/tools/IconPack";
import { isDCActive } from "../dynamic_content";

const Heading = (props) => {
    const {
        headingShow,
        headingStyle,
        headingAlign,
        headingURL,
        headingText,
        setAttributes,
        headingBtnText,
        subHeadingShow,
        subHeadingText,
        headingTag,
        dcEnabled = false,
    } = props.props;
    const CustomHeadingTag = headingTag ? `${headingTag}` : "h3";

    const headingOnChange = (val) => {
        setAttributes({ 
            headingText: val,
        });
    }

    return (
        <>
            {headingShow && (
                <div
                    className={`ultp-heading-wrap ultp-heading-${headingStyle} ultp-heading-${headingAlign}`}
                >
                    {headingURL ? (
                        <CustomHeadingTag className={`ultp-heading-inner`}>
                            <a>
                                <RichText
                                    key="editable"
                                    tagName={"span"}
                                    placeholder={__(
                                        "Add Text...",
                                        "ultimate-post"
                                    )}
                                    onChange={headingOnChange}
                                    value={headingText}
                                    allowedformats={
                                        dcEnabled ?
                                        [ 'ultimate-post/dynamic-content' ] :
                                        undefined
                                    }
                                />
                            </a>
                        </CustomHeadingTag>
                    ) : (
                        <CustomHeadingTag className={`ultp-heading-inner`}>
                            <RichText
                                key="editable"
                                tagName={"span"}
                                placeholder={__("Add Text...", "ultimate-post")}
                                onChange={headingOnChange}
                                value={headingText}
                                allowedFormats={
                                    isDCActive() && dcEnabled ?
                                    [ 'ultimate-post/dynamic-content' ] :
                                    undefined
                                }
                            />
                        </CustomHeadingTag>
                    )}
                    {headingStyle == "style11" && headingURL && (
                        <a className={`ultp-heading-btn`}>
                            {headingBtnText}
                            {IconPack.rightAngle2}
                        </a>
                    )}
                    {subHeadingShow && (
                        <div className={`ultp-sub-heading`}>
                            <RichText
                                key="editable"
                                tagName={"div"}
                                className={"ultp-sub-heading-inner"}
                                placeholder={__("Add Text...", "ultimate-post")}
                                allowedFormats={[]}
                                onChange={(value) =>
                                    setAttributes({ subHeadingText: value })
                                }
                                value={subHeadingText}
                            />
                        </div>
                    )}
                </div>
            )}
        </>
    );
};

export default Heading;
