const Separator = (props) => {
    const { label, fancy } = props;

    if ( fancy && label ) {
        return (
            <div className="ultp-separator-ribbon-wrapper">
                <div className="ultp-separator-ribbon">{label}</div>
            </div>
        );
    }
    return (
        <div className={'ultp-field-wrap ultp-field-separator' + (!label ? " ultp-separator-padding" : "")}>
            <div>{ label && <span>{label}</span> }</div>
        </div>
    )
}
export default Separator