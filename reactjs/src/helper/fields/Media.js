import { isDCActive } from "../dynamic_content";

const {__} = wp.i18n
const { MediaUpload } = wp.blockEditor;
const { useState } = wp.element;
const Media = (props) => {
	
	const {multiple, onChange, value, type, label, block, handleLoader, dcEnabled = false, DynamicContent =null } = props;
	const [ imgSizeList, setImgSizeList ] = useState({});
	const [ imgLoader, setImgLoader ] = useState(false)

	const setSettings = (media) => {
		if(media.id && imgSizeList[media?.id] == undefined && block == 'image-block' ){
			setImgLoader(true);
			handleLoader(true);
			wp.apiFetch( {
				path: '/ultp/v2/get_ultp_image_size',
				method: 'POST',
				data: {
					id: media.id,
					wpnonce: ultp_data.security
				}
			} ).then( (obj) => {
				setImgSizeList({...imgSizeList, [media.id]: obj?.size})
				onChange({url: media.url, id: media.id, size: obj?.size, alt: media.alt, caption: media.caption});
				setImgLoader(false);
				handleLoader(false);	
			}).catch( (error) => {
				console.log("error");
			})
		} else if(block == 'image-block'){
			onChange({url: media.url, id: media.id, size: imgSizeList[media?.id], alt: media.alt, caption: media.caption});
			setImgLoader(false);
			handleLoader(false);	
		}
		if (multiple) {
			let medias = [];
			media.forEach( single => {
				if (single && single.url) {
					medias.push( {url: single.url, id: single.id, alt: single.alt, caption: single.caption} );
				}
			});
			onChange( value ? value.concat(medias) : medias );
		} else {
			if (media && media.url && media.id && block != 'image-block') {
				onChange({url: media.url, id: media.id, size: media.sizes, alt: media.alt, caption: media.caption});
			}
		}
	}

	const removeImage = (id) => {
		if (multiple) {
			let value = (props.value).slice()
			value.splice(id, 1)
			onChange( value )
		} else {
			onChange({})
		}
	}

	const isUrl = (url) => {
		if (['wbm','jpg','jpeg','gif','png'].indexOf( url.split('.').pop().toLowerCase() ) != -1 ) {
			return url;
		} else {
			return ultp_data.url+'assets/img/ultp-placeholder.jpg';
		}
	}

	const changeUrl = (val) => {
		if (!multiple) {
			onChange({url: val.target.value, id: 99999});
		}
	}
		
	return (
		<div className={`ultp-field-wrap ultp-field-media ${imgLoader ? 'ultp-img-uploading' : '' }`}>
			{ label &&
				<label>{label}</label>
			}
			<MediaUpload
				onSelect={ val => setSettings(val) }
				allowedTypes={ type || ['image'] }
				multiple={ multiple || false }
				value={ value }
				render={({ open }) => (
					<div className="ultp-media-img">
						{ multiple ?
							<div className="ultp-multiple-img">
								{ ( value.length > 0 ) &&
									value.map( (v,index) => {
										return(
											<span key={index} className="ultp-media-image-item">
												<img src={isUrl(v.url)} alt={__('image','ultimate-post')}/>
												<span className={'ultp-image-close-icon dashicons dashicons-no-alt'} onClick={() => removeImage(index)}/>
											</span>
										)
									} )
								}
								<div onClick={open} className={"ultp-placeholder-image"}><span>{__('Upload','ultimate-post')}</span></div>
							</div>
							:
							( 
								( value && value.url ) ? 
								<span className="ultp-media-image-item">
									<input type="text" onChange={(v) => changeUrl(v)} value={value.url} disabled={isDCActive() && dcEnabled} />
									<div className="ultp-media-preview-container">
										<div className={"ultp-placeholder-image"}>
											<img onClick={open} src={value.url} alt="Saved Media" />
											{
												!dcEnabled && (
													<span className={'ultp-image-close-icon dashicons dashicons-no-alt'} onClick={() => removeImage()}/>
												)
											}
										</div>
										{
											isDCActive() && DynamicContent
										}
									</div>
								</span>
								:
								<span className="ultp-media-image-item">
									<input type="text" onChange={(v) => changeUrl(v)} value="" 
										disabled={isDCActive() && dcEnabled}/>
									<div className="ultp-media-preview-container">
										<div onClick={open} className={"ultp-placeholder-image"}>
											<span className="dashicons dashicons-plus-alt2 ultp-media-upload">
											</span>
										</div>
										{
											isDCActive() && DynamicContent
										}
									</div>
								</span>
							)
						}
					</div>
				)}
			/>
		</div>
	)
}
export default Media;