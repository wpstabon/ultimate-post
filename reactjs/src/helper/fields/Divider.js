const Divider = (props) => {
    const {label} = props
    return (
        <div className="ultp-field-wrap ultp-field-divider">
            { label &&
                <div className="ultp-field-label">{label}</div>
            }
            <hr/>
        </div>
    )
}
export default Divider
