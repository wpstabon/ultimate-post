const { useState } = wp.element;
import UltpLinkGenerator from "../UltpLinkGenerator";
import IconPack from "./tools/IconPack";

const Layout2 = ({
    value,
    options,
    label,
    pro,
    tab,
    onChange,
    block,
    isInline,
}) => {
    const [isOpen, setIsOpen] = useState(false);

    const getPostAttr = (val, tab) => {
        let data = {};
        switch (block) {
            case "post-grid-7":
                data.layout = val;
                data.queryNumber =
                    val == "layout1" || val == "layout4" ? "4" : "3";
                break;
            case "post-grid-3":
                data.layout = val;
                if (val == "layout3" || val == "layout4" || val == "layout5") {
                    data.column = "3";
                } else {
                    data.column = "2";
                }
                data.queryNumber = 5;
                break;
            case "post-grid-4":
                data.layout = val;
                if (val == "layout4" || val == "layout5") {
                    data.queryNumber = "4";
                } else {
                    data.queryNumber = "3";
                }
                break;
            case "post-grid-1":
                if (tab) {
                    data.gridStyle = val;
                    if (val == "style3") {
                        data.columns = { lg: 2 };
                    }
                    if (val == "style4") {
                        data.columns = { lg: 3 };
                    }
                } else {
                    data.layout = val;
                }
                break;
            case "post-list-1":
                if (tab) {
                    data.gridStyle = val;
                    if (val == "style2") {
                        data.columns = { lg: 2, xs: 1 };
                    }
                    if (val == "style3") {
                        data.columns = { lg: 2, xs: 1 };
                    }
                } else {
                    data.layout = val;
                }
                break;
            default:
                data.layout = val;
                break;
        }

        return data;
    };

    const setValue = (val, isPro, tab) => {
        const data = getPostAttr(val, tab);
        if (isPro) {
            if (ultp_data.active) {
                onChange(data);
            } else {
                // window.open(ultp_data.premium_link, '_blank');
                window.open(
                    UltpLinkGenerator(
                        "https://www.wpxpo.com/postx/all-features/",
                        "blockProLay",
                        ultp_data.affiliate_id
                    ),
                    "_blank"
                );
            }
        } else {
            onChange(data);
        }
    };

    return (
        <div className={`ultp-field-wrap`}>
            <div
                className={`ultp-field-layout2 ${
                    isInline
                        ? "ultp-field-layout2-inline"
                        : "ultp-field-layout2-block"
                }`}
            >
                <label className="ultp-field-layout2-label">
                    {label || "Layout"}
                </label>
                <div
                    className="ultp-field-layout2-select"
                    onClick={() => setIsOpen((prev) => !prev)}
                >
                    {options
                        .filter((data) => data.value == value)
                        .map((data, i) => {
                            return (
                                <div
                                    key={i}
                                    className="ultp-field-layout2-select-items"
                                >
                                    <div className="ultp-field-layout2-img">
                                        <img src={ultp_data.url + data.img} />

                                        {data.pro && !ultp_data.active && (
                                            <span
                                                className={
                                                    "ultp-field-layout2-pro"
                                                }
                                            >
                                                Pro
                                            </span>
                                        )}
                                    </div>
                                    <span className="ultp-field-layout2-select-text">
                                        {data.label}
                                        <span
                                            className={`ultp-field-layout2-select-icon ${
                                                isOpen
                                                    ? "ultp-dropdown-icon-rotate"
                                                    : ""
                                            }`}
                                        >
                                            {IconPack["collapse_bottom_line"]}
                                        </span> 
                                    </span>
                                </div>
                            );
                        })}

                    {isOpen && (
                        <div className="ultp-field-layout2-dropdown">
                            {options
                                .filter((data) => data.value != value)
                                .map((data, i) => {
                                    return (
                                        <div
                                            key={i}
                                            onClick={() =>
                                                setValue(
                                                    data.value,
                                                    data.pro,
                                                    tab
                                                )
                                            }
                                            className="ultp-field-layout2-select-items ultp-field-layout2-dropdown-items"
                                        >
                                            <div className="ultp-field-layout2-img">
                                                <img
                                                    src={
                                                        ultp_data.url + data.img
                                                    }
                                                />

                                                {data.pro &&
                                                    !ultp_data.active && (
                                                        <span
                                                            className={
                                                                "ultp-field-layout2-pro"
                                                            }
                                                        >
                                                            Pro
                                                        </span>
                                                    )}
                                            </div>
                                            <span>{data.label}</span>
                                        </div>
                                    );
                                })}
                        </div>
                    )}
                </div>
            </div>

            {pro && !ultp_data.active && 
            (
                <div className="ultp-field-pro-message">
                    To Unlock All Layouts{" "}
                    <a href={'https://www.wpxpo.com/postx/all-features/?utm_source=db-postx-editor&utm_medium=pro-layout&utm_campaign=postx-dashboard'} target="_blank">
                        Upgrade Pro
                    </a>
                </div>
            )}
        </div>
    );
};
export default Layout2;
