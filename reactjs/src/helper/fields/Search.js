import UltpLinkGenerator from '../UltpLinkGenerator';

const { __ } = wp.i18n;
const { apiFetch } = wp;
const { useState, useEffect, useRef } = wp.element;
const { Spinner } = wp.components;

const Search = (props) => {
	const myRef = useRef();
	const [isOpen, setOpen] = useState(false);
	const [defValue, setDefValue] = useState([]);
	const [searchValue, setSearchValue] = useState('');
	const [loading, setLoading] = useState(true);
	const {
		label,
		value,
		onChange,
		search,
		condition,
		pro,
		single = false,
		noIdInTitle = false,
	} = props;

	const fetchData = async (term = '') => {
		setLoading(true);
		apiFetch({
			path: '/ultp/v1/search',
			method: 'POST',
			data: { type: search, term: term, condition: condition },
		}).then((res) => {
			if (res.success) {
				if (value.length > 0 && res.data.length > 0) {
					const _val = JSON.stringify(value);
					const myArray = res.data.filter(function ({
						value,
						title,
						live_title,
					}) {
						return (
							_val.indexOf(
								JSON.stringify(
									search == 'taxvalue' && live_title
										? {
												value: value,
												title: title,
												live_title: live_title,
											}
										: { value: value, title: title }
								)
							) < 0
						);
					});
					setDefValue(myArray);
					setLoading(false);
				} else {
					setDefValue(res.data);
					setLoading(false);
				}
			}
		});
	};

	const handleClickOutside = (e) => {
		if (!myRef.current.contains(e.target)) {
			setOpen(false);
		}
	};

	useEffect(() => {
		document.addEventListener('mousedown', handleClickOutside);
		return () =>
			document.removeEventListener('mousedown', handleClickOutside);
	}, []);

	const fetchOpen = (status = true) => {
		if (pro && !ultp_data.active) {
			return;
		}

		if (status) {
			setLoading(true);
			fetchData();
		}
		setOpen(status);
	};

	const removeValue = (index) => {
		if (pro && !ultp_data.active) {
			return;
		}

		value.splice(index, 1);
		const myArray = defValue.filter(function (el) {
			return value.indexOf(el) < 0;
		});
		setDefValue(myArray);

		onChange(value);
	};

	const setValue = (val) => {
		if (pro && !ultp_data.active) {
			return;
		}

		if (single) {
			value.splice(0, value.length);
		}

		value.push(val);
		const myArray = defValue.filter(function (el) {
			return value.indexOf(el) < 0;
		});
		setDefValue(myArray);
		onChange(value);
		setOpen(false);
	};

	const fetchValue = (val) => {
		const v = val.target.value;
		if (v.length > 0) {
			fetchData(v);
		}
		setSearchValue(v);
	};

	const upValue = (index) => {
		if (pro && !ultp_data.active) {
			return;
		}
		let myArray = value.slice(0);
		if (index > 0) {
			myArray[index] = myArray.splice(index - 1, 1, myArray[index])[0];
			setDefValue(myArray);
			onChange(myArray);
		}
	};
	const downValue = (index) => {
		if (pro && !ultp_data.active) {
			return;
		}
		let myArray = value.slice(0);
		if (value.length > index + 1) {
			myArray[index + 1] = myArray.splice(
				index,
				1,
				myArray[index + 1]
			)[0];
			setDefValue(myArray);
			onChange(myArray);
		}
	};

	return (
		<div
			className={`ultp-field-search ultp-field-wrap ultp-field-select ${pro && !ultp_data.active ? 'ultp-pro-field' : ''}`}
		>
			{label && (
				<span className={`ultp-label-control`}>
					<label>{label}</label>
				</span>
			)}
			<div ref={myRef} className={`ultp-popup-select`}>
				<span
					className={
						(isOpen ? 'isOpen ' : '') +
						' ultp-selected-text ultp-selected-dropdown--icon'
					}
				>
					<span
						onClick={() => fetchOpen(true)}
						className="ultp-search-value ultp-multiple-value"
					>
						{value.map((item, k) => {
							return (
								<span key={k}>
									{!single && (
										<span className="ultp-updown-container">
											<span
												onClick={(e) => {
													e.stopPropagation();
													upValue(k);
												}}
												className="ultp-select-swap dashicons dashicons-arrow-up-alt2"
											/>
											<span
												onClick={(e) => {
													e.stopPropagation();
													downValue(k);
												}}
												className="ultp-select-swap dashicons dashicons-arrow-down-alt2"
											/>
										</span>
									)}
									{noIdInTitle
										? item.title
												?.replaceAll('&amp;', '&')
												.replace(/^\[ID:\s\d+\]/, '')
												.trim()
										: item.title?.replaceAll('&amp;', '&')}
									<span
										onClick={() => removeValue(k)}
										className="ultp-select-close"
									>
										×
									</span>
								</span>
							);
						})}
					</span>
					<span className="ultp-search-divider"></span>
					<span
						className="ultp-search-icon ultp-search-dropdown--icon"
						onClick={() => fetchOpen(!isOpen)}
					>
						<i
							className={
								'dashicons dashicons-arrow-' +
								(isOpen ? 'up-alt2' : 'down-alt2')
							}
						/>
					</span>
				</span>
				<span>
					{isOpen && (
						<ul>
							<div className="ultp-select-search">
								<input
									type="text"
									placeholder={__(
										'Search here for more...',
										'ultimate-post'
									)}
									value={searchValue}
									onChange={(v) => fetchValue(v)}
									autocomplete="off"
								/>
								{loading && <Spinner />}
							</div>
							{defValue.map((item, k) => (
								<li key={k} onClick={() => setValue(item)}>
									{noIdInTitle
										? item.title
												?.replaceAll('&amp;', '&')
												.replace(/^\[ID:\s\d+\]/, '')
												.trim()
										: item.title?.replaceAll('&amp;', '&')}
								</li>
							))}
						</ul>
					)}
				</span>
			</div>
			{pro && !ultp_data.active && (
				<div className="ultp-field-pro-message">
					{__('To Enable This Feature', 'ultimate-post')}{' '}
					<a
						href={UltpLinkGenerator(
							'https://www.wpxpo.com/postx/all-features/',
							'blockProFeat',
							ultp_data.affiliate_id
						)}
						target="_blank"
					>
						{__('Upgrade to Pro', 'ultimate-post')}
					</a>
				</div>
			)}
		</div>
	);
};
export default Search;
