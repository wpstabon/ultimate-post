const { __ } = wp.i18n
const { useState, useEffect } = wp.element

const Template = (props) => {
    const {clientId, attributes, label, name} = props.store
    const [state, setState] = useState({ designList: [], error: false, reload: false, reloadId: '' });
    const {designList, error, reload, reloadId} = state

    useEffect(() => {
        const fetchData = async () => {
            const blockName = name.split('/')
            wp.apiFetch({
                path: '/ultp/v2/fetch_premade_data',
                method: 'POST',
                data: {
                    type: 'design'
                }
            })
            .then((response) => {
                if (response.success) {
                    const data = JSON.parse(response.data)
                    setState({...state, designList: data[blockName[1]]})
                }
            })
        };
        fetchData();
    }, []);


    const _changeVal = (designID, isPro) => {
        setState({...state, reload: true, reloadId: designID})
        if (isPro) {
            if (ultp_data.active) {
                _changeDesign(designID);
            }
        } else {
            _changeDesign(designID);
        }
    }

    const _changeDesign = (designID) => {
        const designEndpoint = 'https://ultp.wpxpo.com/wp-json/restapi/v2/';
        const { replaceBlock } = wp.data.dispatch('core/block-editor')
        const removeItem = ['queryNumber', 'queryNumPosts', 'queryType', 'queryTax', 'queryRelation', 'queryOrderBy', 'queryOrder', 'queryInclude', 'queryExclude', 'queryAuthor', 'queryOffset', 'metaKey', 'queryExcludeTerm', 'queryExcludeAuthor', 'querySticky', 'queryUnique', 'queryPosts', 'queryCustomPosts'];
        
        window.fetch(designEndpoint+'single-design', {
            method: 'POST',
            body: new URLSearchParams('license='+ultp_data.license+'&design_id='+designID)
        })
        .then(response => response.text())
        .then((jsonData) => {
            jsonData = JSON.parse(jsonData)
            if (jsonData.success && jsonData.rawData) {
                let parseData = wp.blocks.parse(jsonData.rawData)
                let attr = parseData[0].attributes
                for ( let i=0; i < removeItem.length; i++ ) {
                    if (attr[removeItem[i]]) { delete attr[removeItem[i]]; }
                }
                attr = Object.assign({}, attributes,  attr);
                parseData[0].attributes = attr;
                setState({...state, error: false, reload: false, reloadId: '' });
                replaceBlock(clientId, parseData );
            }else{
                setState({...state, error: true, reload: false, reloadId: '' });
            }
        })
        .catch((error) => {
            console.error(error)
        })
    }

    if (typeof designList === 'undefined' ) return null;

    return(
        <div className="ultp-field-wrap ultp-field-template">
            { label && 
                <label>{label}</label>
            }
            <div className="ultp-sub-field-template">
                { designList.map( (data, k) => {
                        return (
                            <div key={k} className={`ultp-field-template-item ${(data.pro && !ultp_data.active) ? 'ultp-field-premium':''}`}>
                                <img loading="lazy" src={data.image}/>
                                {(data.pro && !ultp_data.active) &&
                                    <span className="ultp-field-premium-badge">{__('Premium','ultimate-post')}</span>
                                }
                                {(data.pro && !ultp_data.active) ?
                                    <div className="ultp-field-premium-lock">
                                        <a href={ultp_data.premium_link} target="_blank">{__('Go Pro','ultimate-post')}</a>
                                    </div>
                                    :( (data.pro && error) ?
                                        <div className="ultp-field-premium-lock">
                                            <a href={ultp_data.premium_link} target="_blank">{__('Get License','ultimate-post')}</a>
                                        </div>
                                        :
                                        <div className="ultp-field-premium-lock">
                                            <button className={'ultp-popup-btn'} onClick={()=>_changeVal(data.ID, data.pro)}>
                                            {__('Import','ultimate-post')}{(reload && reloadId == data.ID) && <span className="dashicons dashicons-update rotate" />}</button>
                                        </div>
                                    )
                                }
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )

}
export default Template