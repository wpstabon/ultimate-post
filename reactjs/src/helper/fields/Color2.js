const { __ } = wp.i18n
import { getPresetColorLists, getPresetGradientLists, handleTypoFieldPresetData, presetKeyToLabel, redirectToPostxSettingsPannel } from "../CommonFunctions"
import UltpLinkGenerator from "../UltpLinkGenerator"
import Color from "./Color"
import Filter from "./Filter"
import Range from "./Range"
import Select from "./Select"
const { Dropdown, GradientPicker, TextControl, ToggleControl, SelectControl, FocalPointPicker, Tooltip } = wp.components
const { MediaUpload } = wp.blockEditor;
const globalSettings = wp.data.select( "core/editor" );
const { useState } = wp.element


const Color2 = (props) => {
    const { value, label, pro, onChange, image, video, extraClass, customGradient, gbbodyBackground } = props;
    const colorDef = {openColor: 0,type:'color',color:'#037fff',gradient: 'linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)',clip:false}
    const [presetTab, setPresetTab] = useState( typeof value.gradient == 'string' && value.gradient?.includes('var(--postx_preset') ? true : false );
    const presetGradients = getPresetGradientLists();
    let valueGradient = handleTypoFieldPresetData(value.gradient) ? value.gradient : getPresetGradientLists('colorcode', value.gradient);

    if ( typeof valueGradient == 'string' && ( valueGradient?.includes('postx_preset_Primary_color') || valueGradient.includes('postx_preset_Secondary_color') ) ) {
        valueGradient = valueGradient.replace('var(--postx_preset_Primary_color)', getPresetColorLists('colorcode', 'var(--postx_preset_Primary_color)')).replace('var(--postx_preset_Secondary_color)', getPresetColorLists('colorcode', 'var(--postx_preset_Secondary_color)'))
    }

    const _set = (val, type) => {
        if (pro && !ultp_data.active) {
            return ;
        }
        
        // for Compatiblity
        let __val = value;
        if (typeof __val.gradient == 'object') {
            __val.gradient = _compatiblity(__val.gradient);
        }

        onChange(Object.assign({}, colorDef, __val, {openColor: 1}, {[type]: val}/*, {type: def}*/));
    }

    // for Compatiblity
    const _compatiblity = ( data ) => {
        if (typeof data == 'object') {
            if (data.type == 'linear') {
                return 'linear-gradient('+data.direction+'deg, '+data.color1+' '+data.start+'%, '+data.color2+' '+data.stop+'%)'
            } else {
                return 'radial-gradient( circle at ' + data.radial + ' , ' + data.color1 + ' ' + data.start + '%,' + data.color2 + ' ' + data.stop + '%)'
            }
        }
        return data || {};
    }

    const gradientHtml = () => {
        return <>
            <div className="ultp-color-field-tab">
                <div className={`${presetTab ? 'active' : ''}`} onClick={()=> setPresetTab(true)}>Preset Palette</div>
                <div className={`${presetTab ? '' : 'active'}`} onClick={()=> setPresetTab(false)}>Custom</div>
            </div>
            <div className='ultp-color-field-options'>
                {
                    presetTab ? <>
                        { presetGradients.length > 0 &&
                            <div className="ultp-preset-gradient-options">
                                <div className="ultp-preset-label">
                                    <div className="ultp-preset-label-content"> {__('PostX Gradient', 'ultimate-post')} </div>
                                    <div className="ultp-preset-label-link" onClick={() => redirectToPostxSettingsPannel()}> Customize &nbsp;&gt;</div>
                                </div>
                                <div className="gradient-lists">
                                    { presetGradients.map( (val, k) => {
                                        return <Tooltip key={k} position={'top'} text={presetKeyToLabel(val, 'gradient')}>
                                            <span className={value.gradient == val ? 'active' : ''} key={k} onClick={() => _set(val,'gradient')} style={{background: val}}/>
                                        </Tooltip>
                                    })}
                                </div>
                            </div>
                        }
                    </>
                    :
                    <>
                        <GradientPicker
                            clearable={true}
                            __nextHasNoMargin={true}
                            value={ typeof valueGradient == 'object' ? _compatiblity(valueGradient) : valueGradient }
                            // value={ typeof value.gradient == 'object' ? _compatiblity(value.gradient) : value.gradient }
                            onChange={val => _set(val,'gradient')}
                            gradients={ customGradient.length ? customGradient : globalSettings ? globalSettings.getEditorSettings().gradients :[] }
                        />
                    </>
                }

            </div>
        </>
    }
    
    return (
        <div className={`ultp-field-wrap ultp-field-color2 ${ (pro && !ultp_data.active) ? 'ultp-pro-field':''} ${video && image ? 'ultp-field-video-image' : ''}`}>
            { label && 
                <label>{label}</label>
            }
            <div className="ultp-sub-field">
                { value.openColor ? 
                <div className={ 'active ultp-base-control-btn dashicons dashicons-image-rotate' } onClick={() => { _set(0,'openColor'); }} />
               : '' }
                <div className="ultp-color2-btn__group">
                    <Dropdown
                        className="ultp-range-control"
                        focusOnMount={true}
                        renderToggle={({ onToggle, onClose }) => (
                            <div className="ultp-button-group">
                                <button  className={`${(value.type == 'color' && value.openColor) ? 'active' : ''} ultp-icon-style` } onClick={ () => { onToggle(); _set('color','type');}}>{__('Solid', 'ultimate-post')}</button>
                            </div>
                        )}
                        renderContent={() => (
                            <div className="ultp-popup-select ultp-common-popup">
                                <Color
                                    label={'Color'}
                                    inline
                                    color2
                                    disableClear
                                    value={ value.color || '#16d03e' }
                                    onChange={val => { _set(val,'color')}}/>
                            </div>
                        )}
                    />
                    <Dropdown
                        className="ultp-range-control"
                        focusOnMount={true}
                        renderToggle={({ onToggle, onClose }) => (
                            <div className="ultp-button-group">
                                <button className={`${(value.type == 'gradient' && value.openColor) ? 'active' : ''} ultp-icon-style` } onClick={ () => { onToggle(); _set('gradient','type'); }}>{__('Gradient', 'ultimate-post')}</button>
                            </div>
                        )}
                        renderContent={() => (
                            <div className={`ultp-popup-select ultp-common-popup ultp-color-container ${presetTab ? 'typo-active' : ''} ${extraClass}`}>
                                {gradientHtml()}
                            </div>
                        )}
                    />
                    { image &&
                        <Dropdown
                            focusOnMount={true}
                            className="ultp-range-control"
                            renderToggle={({ onToggle, onClose }) => (
                                <div className="ultp-button-group">
                                    <button className={`${(value.type == 'image' && value.openColor) ? 'active' : ''} ultp-icon-style` } onClick={ () => { onToggle(); _set('image','type'); }}>{__('Image', 'ultimate-post')}</button>
                                </div>
                            )}
                            renderContent={() => (
                                <div className="ultp-image-control--popup">
                                    <MediaUpload
                                            onSelect={ ( v ) => {
                                                if (v.url) {
                                                    _set(v.url, 'image')
                                                }
                                            }}
                                            allowedTypes={ ['image'] }
                                            value={ value.image || '' }
                                            render={ ( { open } ) => (
                                                <div className="ultp-field-media">
                                                    <span className="ultp-media-image-item">
                                                        {   value.image ?
                                                            <div className="ultp-imgvalue-wrap">
                                                                <input type="text" value={value.image} placeholder="Image Url" onChange={(v) => _set(v.target.value, 'image')} /> 
                                                                <span className={'ultp-image-close-icon dashicons dashicons-no-alt'} onClick={() => _set('', 'image')}/> 
                                                            </div>
                                                            : 
                                                            <input type="text" placeholder="Image Url" onChange={(v) => _set( v.target.value, 'image')} /> 
                                                        }
                                                        <div className={"ultp-placeholder-image"}>
                                                            {
                                                                value.image ?
                                                                <div className="ultp-focalpoint-wrapper">
                                                                    <FocalPointPicker
                                                                        url={ value.image }
                                                                        value={value.position}
                                                                        // label={label}
                                                                        onDragStart={ ( position ) => 
                                                                            _set(position, 'position')
                                                                        }
                                                                        onDrag={ ( position ) => 
                                                                            _set(position, 'position')
                                                                        }
                                                                        onChange={ ( position ) => 
                                                                            _set(position, 'position')
                                                                        }
                                                                    />
                                                                </div>
                                                                :   <span onClick={ open } className="dashicons dashicons-plus-alt2 ultp-media-upload"/>
                                                            }
                                                        </div> 
                                                    </span>
                                                </div>
                                            ) }
                                        />
                                    <div className="ultp-popup-select ultp-common-popup">
                                        <SelectControl
                                            __nextHasNoMarginBottom={true}
                                            label={__('Attachment','ultimate-post')}
                                            value={ value.attachment }
                                            options={[
                                                { label: 'Default', value: '' },
                                                { label: 'Scroll', value: 'scroll' },
                                                { label: 'fixed', value: 'Fixed' },
                                            ]}
                                            onChange={ v => _set(v, 'attachment')} />
                                        <SelectControl
                                            __nextHasNoMarginBottom={true}
                                            label={__('Repeat','ultimate-post')}
                                            value={ value.repeat }
                                            options={[
                                                { label: 'Default', value: '' },
                                                { label: 'No-repeat', value: 'no-repeat' },
                                                { label: 'Repeat', value: 'repeat' },
                                                { label: 'Repeat-x', value: 'repeat-x' },
                                                { label: 'Repeat-y', value: 'repeat-y' },
                                            ]}
                                            onChange={ v => _set(v, 'repeat')} />
                                        <SelectControl
                                            __nextHasNoMarginBottom={true}
                                            label={__('Size','ultimate-post')}
                                            value={ value.size }
                                            options={[
                                                { label: 'Default', value: '' },
                                                { label: 'Auto', value: 'auto' },
                                                { label: 'Cover', value: 'cover' },
                                                { label: 'Contain', value: 'contain' },
                                                // { label: 'Custom', value: 'initial' }
                                            ]}
                                            onChange={ v => _set(v, 'size')} />
                                    </div>
                                    <div className="ultp-editor-fallback-bg">
                                        <Color
                                            label={'Fallback Color'}
                                            value={ value.fallbackColor ?? value.color }
                                            onChange={v => { _set(v,'fallbackColor')}}
                                        />
                                    </div>
                                    { // for future plan now ignored
                                        gbbodyBackground && 1==2 && <>
                                            <Range
                                                value={value.bgOpacity || ''}
                                                max={100} 
                                                step={1} 
                                                // unit={false}
                                                label={__('Opacity','ultimate-post')}
                                                onChange={ v => _set( v, 'bgOpacity' )} 
                                            />
                                            <Filter
                                                value={value.bgFilter || ''}
                                                label={__('Filter','ultimate-post')}
                                                onChange={ v => _set( v, 'bgFilter' )} 
                                            />
                                            <Select
                                                value={value.bgBlend || ''}
                                                classes="ultp-preset-background-blend"
                                                keys={'bgBlend'}
                                                label={__('Blend Mode','ultimate-post')}
                                                options={[
                                                    { value: '', label: __('Select', 'ultimate-post') },
                                                    { value: 'normal', label: __('Normal', 'ultimate-post') },
                                                    { value: 'multiply', label: __('Multiply', 'ultimate-post') },
                                                    { value: 'screen', label: __('Screen', 'ultimate-post') },
                                                    { value: 'overlay', label: __('Overlay', 'ultimate-post') },
                                                    { value: 'darken', label: __('Darken', 'ultimate-post') },
                                                    { value: 'lighten', label: __('Lighten', 'ultimate-post') },
                                                    { value: 'color-dodge', label: __('Color Dodge', 'ultimate-post') },
                                                    { value: 'color-burn', label: __('Color Burn', 'ultimate-post') },
                                                    { value: 'hard-light', label: __('Hard Light', 'ultimate-post') },
                                                    { value: 'soft-light', label: __('Soft Light', 'ultimate-post') },
                                                    { value: 'difference', label: __('Difference', 'ultimate-post') },
                                                    { value: 'exclusion', label: __('Exclusion', 'ultimate-post') },
                                                    { value: 'hue', label: __('Hue', 'ultimate-post') },
                                                    { value: 'saturation', label: __('Saturation', 'ultimate-post') },
                                                    { value: 'luminosity', label: __('Luminosity', 'ultimate-post') },
                                                    { value: 'color', label: __('Color', 'ultimate-post') },
                                                ]} 
                                                beside={false}
                                                onChange={ v => _set( v, 'bgBlend' )}
                                            />
                                        </>
                                    }

                                </div>

                            )}
                        />
                    }
                    { video &&
                        <Dropdown
                            focusOnMount={true}
                            className="ultp-range-control"
                            renderToggle={({ onToggle, onClose }) => (
                                <div className="ultp-button-group">
                                    <button className={`${(value.type == 'video' && value.openColor) ? 'active' : ''} ultp-icon-style` } onClick={ () => { onToggle(); _set('video','type'); }}>{__('Video', 'ultimate-post')}</button>
                                </div>
                            )}
                            renderContent={() => (
                                <div className="ultp-popup-select ultp-common-popup">
                                    <TextControl
                                        __nextHasNoMarginBottom={true}
                                        value={value.video}
                                        placeholder={__('Only Youtube & Vimeo  & Self Hosted Url', 'ultimate-post')}
                                        label={__('Video URL', 'ultimate-post')}
                                        onChange={ v => _set(v, 'video')}/>
                                    <Range
                                        value={value.start || 0}
                                        min={1}
                                        max={12000}
                                        step={1}
                                        label={__('Start Time(Seconds)','ultimate-post')}
                                        onChange={ v => _set(v, 'start') }/>  
                                    <Range
                                        value={value.end || ''}
                                        min={1}
                                        max={12000}
                                        step={1}
                                        label={__('End Time(Seconds)','ultimate-post')}
                                        onChange={ v => _set(v, 'end') }/>  
                                    <ToggleControl 
                                        __nextHasNoMarginBottom={true}
                                        label={__('Loop Video','ultimate-post')}
                                        checked={ value.loop ? 1 : 0 }
                                        onChange={ v => _set(v, 'loop') } />
                                    <MediaUpload
                                        onSelect={ ( v ) => {
                                            if (v.url) {
                                                _set(v.url, 'fallback')
                                            }
                                        }}
                                        allowedTypes={ ['image'] }
                                        value={ value.fallback || '' }
                                        render={ ( { open } ) => (
                                            <>
                                            <div className="ultp-field-media">
                                            <div className="ultp-fallback-lebel">{__('Video Fallback Image', 'ultimate-post')}</div>
                                                <span className="ultp-media-image-item">
                                                    {   value.fallback && 
                                                        <input type="text" value={value.fallback} onChange={(v) => changeUrl(v)}/> 
                                                    }
                                                    <div className={"ultp-placeholder-image"}>
                                                        {
                                                            value.fallback ?
                                                            <>
                                                                <img src={value.fallback} alt="Fallback Image" />
                                                                <span className={'ultp-image-close-icon dashicons dashicons-no-alt'} onClick={() => _set('', 'fallback')}/>
                                                            </>
                                                            :   <span onClick={ open } className="dashicons dashicons-plus-alt2 ultp-media-upload"/>
                                                        }
                                                    </div> 
                                                </span>
                                            </div>
                                        </>
                                        ) }
                                    />
                                </div>
                            )}
                        />
                    }
                </div>
            </div>
            { (pro && !ultp_data.active) &&
                <div className="ultp-field-pro-message">{__('Unlock It! Explore More','ultimate-post')} <a href={UltpLinkGenerator('https://www.wpxpo.com/postx/all-features/', 'blockProFeat', ultp_data.affiliate_id)}target="_blank">{__('Pro Features','ultimate-post')}</a></div>
            }
        </div>
    )
}
export default Color2