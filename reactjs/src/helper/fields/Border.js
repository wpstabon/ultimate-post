const { __ } = wp.i18n
const { Dropdown } = wp.components
import Dimension from './Dimension'
import Color from "./Color"
import icons from '../../helper/icons'

const Border = (props) => {
    const {value, label, onChange} = props
    const isActive = value && value.openBorder ? true : false;
    const defValue = {width:{ top:0, right:0, bottom:0, left:0 }, type:'solid', color:'#555d66'}

    const setSettings = (type , val) => {
        onChange( Object.assign( {}, defValue, value, {openBorder:1} , {[type]:val} ) )
    }

    const type = value&&value.type || ''

    return (
        <div className="ultp-field-wrap ultp-popup-control ultp-field-border">
            { label &&
                <label>{label}</label>
            }
            <Dropdown
                className="ultp-range-control"
                renderToggle={({ isOpen, onToggle }) => (
                    <div className="ultp-edit-btn">
                        {
                            isActive &&
                            <div className={"active ultp-base-control-btn dashicons dashicons-image-rotate"} onClick={() => { if (isOpen) { onToggle(); } setSettings( 'openBorder', 0 );}}></div> 
                        }                        
                        <div className={`${isActive && 'active '} ultp-icon-style`} onClick={() => { onToggle(); setSettings( 'openBorder', 1 ); }} aria-expanded={isOpen}>{icons.setting}</div>
                    </div>
                )}
                renderContent={() => (
                    <div className={"ultp-common-popup"}>
                        <div className={"ultp-border-style"}>
                            {['solid','dashed','dotted'].map( (val, k) => {
                                return <span className={type==val? val+ ' active': val+ ' '} key={k} onClick={() => setSettings('type', val)}></span>
                            })}
                        </div>
                        <Dimension 
                            min={0}
                            step={1}
                            max={100}
                            label={__('Border Width','ultimate-post')}
                            value={ value&&value.width || '' }
                            onChange={ val => setSettings('width', val) }
                        />
                        <Color
                            inline
                            label={__('Color','ultimate-post')}
                            value={ value&&value.color || '' } 
                            onChange={ val => setSettings('color', val) } 
                        />
                    </div>
                )}
            />
        </div>
    )
}

export default Border