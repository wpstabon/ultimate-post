const {__} = wp.i18n;
import { CommonSettings } from "../CommonPanel";
const { Tooltip } = wp.components;
const { useState } = wp.element

const LinkButton = (props) => {
    const { text, value, label, placeholder, onlyLink, onChange, store, disableLink = false, extraBtnAttr } = props;

    const [toggle, setToggle ] = useState(false);
	const removeUrl = (id) => {
		onChange({url: ''})
	}
    const { target, follow, sponsored, download } = extraBtnAttr || {};

	const changeUrl = (val) => {
        if (disableLink) return;
		onChange(extraBtnAttr ? val.target.value : {url: val.target.value});
    }

    return(
        <div className={`ultp-field-wrap ${onlyLink ? 'ultp-field-selflink' : 'ultp-field-link-button'}`}>
            { label && 
                <label>{label}</label>
            }
            {
                onlyLink ? 
                <div>
                    <span className={`ultp-link-section`}>
                        <span className="ultp-link-input">
                            <input type="text" onChange={(v) => changeUrl(v)}  value={disableLink ? "Dynamic URL" : extraBtnAttr ? value : value?.url} placeholder={placeholder} disabled={disableLink}/>
                            {   ( ( extraBtnAttr && value ) || value?.url ) &&
                                <span className="ultp-image-close-icon dashicons dashicons-no-alt" onClick={() => removeUrl()}/>
                            }
                        </span>
                        <span className={`ultp-link-options`}>
                            <span className={`ultp-collapse-section`} onClick={()=>{setToggle(toggle ? false : true)}}>
                                <span className={`ultp-short-collapse ${toggle && ' active'}`} type="button"/>
                            </span>
                        </span>
                    </span>
                    { toggle &&
                        <div className={`ultp-short-content active`}>
                            <CommonSettings
                                include={[
                                    ( (extraBtnAttr && extraBtnAttr.target) || !extraBtnAttr ) && {
                                        position: 1, data: { type:'select', key: target?.key || 'btnLinkTarget',label: target?.label || __('Link Target', 'ultimate-post'), options: target?.options || [
                                            {value:'_self', label:__('Same Tab/Window', 'ultimate-post')},
                                            {value:'_blank', label:__('Open in New Tab', 'ultimate-post')},
                                        ]}
                                    },
                                    ( (extraBtnAttr && extraBtnAttr.follow) || !extraBtnAttr ) && { position: 2, data: { type: 'toggle', key: follow?.key || 'btnLinkNoFollow', label: follow?.label || __('No Follow', 'ultimate-post') } },
                                    ( (extraBtnAttr && extraBtnAttr.sponsored) || !extraBtnAttr ) && { position: 3, data: { type: 'toggle', key: sponsored?.key || 'btnLinkSponsored', label: sponsored?.label || __('Sponsored', 'ultimate-post') } },
                                    ( (extraBtnAttr && extraBtnAttr.download) || !extraBtnAttr ) && { position: 4, data: { type: 'toggle', key: download?.key || 'btnLinkDownload',label: download?.label || __('Download', 'ultimate-post') } },
                                ]} initialOpen={true}  store={store}/>
                        </div>
                    }
                </div>
                :  
                <div className="ultp-sub-field">
                    <Tooltip text={placeholder}>
                        <a href={value} target="_blank">{text}</a>
                    </Tooltip>
                </div>
            }
        </div>
    )
}
export default LinkButton