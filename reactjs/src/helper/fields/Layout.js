import UltpLinkGenerator from "../UltpLinkGenerator";

const Layout = (props) => {
    
    const { value, options, label, pro, col, tab, onChange, block } = props;

    const getPostAttr = (val, tab) => {
        let data = {};
        switch (block) {
            case 'post-grid-7':
                data.layout = val
                data.queryNumber = (val == 'layout1' || val == 'layout4' ? '4' : '3');
                break;
            case 'post-grid-3':
                data.layout = val
                if (val == 'layout3' || val == 'layout4' || val == 'layout5') {
                    data.column = '3';
                } else{
                    data.column = '2';
                }
                data.queryNumber = 5;
                break;
            case 'post-grid-4':
                data.layout = val
                if (val == 'layout4' || val == 'layout5') {
                    data.queryNumber = '4';
                }else{
                    data.queryNumber = '3';
                }
                break;
            case 'post-grid-1':
                if (tab) {
                    data.gridStyle = val
                    if (val == 'style3') {
                        data.columns = {lg:2};
                    }
                    if (val == 'style4') {
                        data.columns = {lg:3};
                    }
                } else {
                    data.layout = val
                }
                break;
            case 'post-list-1':
                    if (tab) {
                        data.gridStyle = val
                        if (val == 'style2') {
                            data.columns = {lg:2, xs:1};
                        }
                        if (val == 'style3') {
                            data.columns = {lg:2, xs:1};
                        }
                    } else {
                        data.layout = val
                    }
                break;
            default:
                data.layout = val
                break;
        }
        
        return data;
    }

    const setValue = (val, isPro, tab) => {
        const data = getPostAttr(val, tab)
        if (isPro) {
            if (ultp_data.active) {
                onChange(data)
            }else{
                // window.open(ultp_data.premium_link, '_blank');
                window.open(UltpLinkGenerator('https://www.wpxpo.com/postx/all-features/', 'blockProLay', ultp_data.affiliate_id), '_blank');
            }
        }else{
            onChange(data)   
        }
    }
    

    return(
        <div className={`ultp-field-wrap ultp-field-layout column-`+(col?col:'two')} tabIndex={0}>
            { label && 
                <label>{label}</label>
            }
            <div className={`ultp-field-layout-wrapper`} /*style={{gridTemplateColumns: `repeat(${(col?col:'2')}, 1fr)`}}*/>
                {
                    options.map((data, index) => {
                        return (
                            <div
                                key={index}
                                onClick={() => setValue(data.value, data.pro, tab)}
                                className={data.value==value?'ultp-field-layout-items active':'ultp-field-layout-items'}
                                >
                                <div className={`ultp-field-layout-item ${data.pro ? '' : 'ultp-layout-free' } ${ (pro && ultp_data.active) ? 'ultp-field-layout-item-pro': ''}`}>
                                    <div className={'ultp-field-layout-content'}>
                                        { (data.pro && !ultp_data.active) &&
                                            <span className={'ultp-field-layout-pro'}>Pro</span>
                                        }
                                        <img src={ultp_data.url + data.img}/>
                                    </div>
                                </div>
                                { data.demoImg &&
                                    <div className={`ultp-layout-hover__content ultp-layout-tooltip-layout${index + 1}`}>
                                        <div className="ultp-layout-hover__popup">
                                            <img src={data.demoImg} alt="Layout Popup Image" />
                                            <div>
                                                {(data.pro && !ultp_data.active) && 
                                                    <a href={UltpLinkGenerator('', 'blockUpgrade', ultp_data.affiliate_id)}>Upgrade Pro</a>
                                                }
                                                <a href={data.demoUrl} target="_blank" className="view-demo">View Demo <span className="dashicons dashicons-arrow-right-alt"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        )
                    })
                }
            </div>
            {/* { (pro && !ultp_data.active) &&
                <div className="ultp-field-pro-message">To Unlock All Layouts <a href={ultp_data.premium_link} target="_blank">Upgrade Pro</a></div>
            } */}
        </div>
    )
}
export default Layout;