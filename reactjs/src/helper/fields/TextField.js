const { __ } = wp.i18n

const { TextControl, TextareaControl } = wp.components;


const TextField = (props) => {
    const { value, onChange, isTextField, attr, DC = null } = props;
    const { placeholder, label, help, disabled = false } = attr;
    
    const handleChange = (v) => {
        onChange(v);
    }

    return(

        <div className={'ultp-field-wrap ultp-field-groupbutton'}>
            {DC && label && (
                <div className="ultp-field-label-responsive">
                    <label htmlFor="">{label}</label>
                </div>
            )}
            <div className={`${DC ? `ultp-acf-field${isTextField ? "" : "-textarea"}` : ''}`}>
                {
                    isTextField ? <TextControl
                        __nextHasNoMarginBottom={true}
                        value={value}
                        placeholder={placeholder}
                        label={DC ? '' : label}
                        help={help}
                        disabled={disabled}
                        // className={`ultp-field-wrap `}
                        onChange={ v => handleChange(v)}
                    /> :
                    <TextareaControl
                        value={value}
                        label={DC ? '' : label}
                        placeholder={placeholder}
                        disabled={disabled}
                        // className={`ultp-field-wrap `}
                        onChange={ v => handleChange(v) }
                    />
                }
                {DC}
            </div>
        </div>
    )
}
export default TextField