import ResponsiveDevice from './tools/ResponsiveDevice';

const alignIcon = {
	left: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M4 19.8h8.9v-1.5H4v1.5zm8.9-15.6H4v1.5h8.9V4.2zm-8.9 7v1.5h16v-1.5H4z" />
		</svg>
	),
	center: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M16.4 4.2H7.6v1.5h8.9V4.2zM4 11.2v1.5h16v-1.5H4zm3.6 8.6h8.9v-1.5H7.6v1.5z" />
		</svg>
	),
	right: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M11.1 19.8H20v-1.5h-8.9v1.5zm0-15.6v1.5H20V4.2h-8.9zM4 12.8h16v-1.5H4v1.5z" />
		</svg>
	),
	justify: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path
				clipRule="evenodd"
				fillRule="evenodd"
				d="M4 5h16v2H4V5zm0 4v2h16V9H4zm0 4h16v2H4v-2zm16 6H4v-2h16v2z"
			/>
		</svg>
	),
	juststart: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g transform="translate(-278 -193)">
					<g>
						<rect
							width="6"
							height="86.8"
							fill="#393939"
							rx="3"
							transform="translate(278 193)"
						/>
					</g>
					<rect
						width="18"
						height="43.9"
						rx="2.9"
						transform="translate(288 215)"
					/>
					<rect
						width="18"
						height="43.9"
						rx="2.9"
						transform="translate(310 215)"
					/>
				</g>
				<g transform="translate(-278 -193)">
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(353 193)"
					/>
				</g>
				<g transform="translate(-278 -193)">
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(278 193)"
					/>
				</g>
			</g>
		</svg>
	),
	justcenter: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g transform="translate(-747 -193)">
				<g>
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(822 193)"
					/>
				</g>
				<rect
					width="18"
					height="43.9"
					rx="2.9"
					transform="rotate(180 404 129)"
				/>
				<g>
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(747 193)"
					/>
				</g>
				<rect
					width="18"
					height="43.9"
					rx="2.9"
					transform="rotate(180 393 129)"
				/>
			</g>
		</svg>
	),
	justend: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g transform="translate(-538 -193)">
					<g>
						<rect
							width="6"
							height="86.8"
							rx="3"
							transform="translate(612 193)"
						/>
					</g>
					<rect
						width="18"
						height="43.9"
						rx="2.9"
						transform="rotate(180 304 129)"
					/>
					<rect
						width="18"
						height="43.9"
						rx="2.9"
						transform="rotate(180 294 129)"
					/>
				</g>
				<g transform="translate(-538 -193)">
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(538 193)"
					/>
				</g>
				<g transform="translate(-538 -193)">
					<rect
						width="6"
						height="86.8"
						rx="3"
						transform="translate(613 193)"
					/>
				</g>
			</g>
		</svg>
	),
	justevenly: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g transform="translate(-1448.977 -193.46)">
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1523.787 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 754.579 129.4105)"
				/>
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1448.976 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 743.782 129.4105)"
				/>
			</g>
		</svg>
	),
	justaround: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g transform="translate(-1260.211 -193.46)">
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1335.022 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 662.1925 129.4105)"
				/>
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1260.211 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 647.4025 129.4105)"
				/>
			</g>
		</svg>
	),
	justbetween: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g transform="translate(-1025.441 -193.46)">
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1100.253 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 548.308 129.4105)"
				/>
				<g>
					<rect
						width="6"
						height="86.827"
						rx="3"
						transform="translate(1025.441 193.46)"
					/>
				</g>
				<rect
					width="17.957"
					height="43.895"
					rx="2.864"
					transform="rotate(180 526.5175 129.4105)"
				/>
			</g>
		</svg>
	),
	algnStart: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g>
					<g transform="translate(-275.353 -406.799)">
						<g>
							<rect
								width="86.827"
								height="6"
								rx="3"
								transform="translate(275.353 406.995)"
							/>
						</g>
						<rect
							width="17.957"
							height="43.895"
							rx="2.864"
							transform="rotate(90 -37.959 378.673)"
						/>
						<rect
							width="17.957"
							height="43.895"
							rx="2.864"
							transform="rotate(90 -48.756 389.47)"
						/>
					</g>
					<g transform="translate(-275.353 -406.799)">
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(275.353 481.61)"
						/>
					</g>
					<g transform="translate(-275.353 -406.799)">
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(275.353 406.799)"
						/>
					</g>
				</g>
			</g>
		</svg>
	),
	algnEnd: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g>
					<g transform="translate(-500.33 -406.799)">
						<g>
							<rect
								width="86.827"
								height="6"
								rx="3"
								transform="translate(500.329 481.414)"
							/>
						</g>
						<rect
							width="17.957"
							height="43.895"
							rx="2.864"
							transform="rotate(-90 499.786 -22.009)"
						/>
						<rect
							width="17.957"
							height="43.895"
							rx="2.864"
							transform="rotate(-90 488.989 -32.806)"
						/>
					</g>
					<g transform="translate(-500.33 -406.799)">
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(500.329 406.799)"
						/>
					</g>
					<g transform="translate(-500.33 -406.799)">
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(500.329 481.61)"
						/>
					</g>
				</g>
			</g>
		</svg>
	),
	algnCenter: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g transform="translate(-744.317 -406.799)">
					<g>
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(744.317 481.61)"
						/>
					</g>
					<rect
						width="17.957"
						height="43.895"
						rx="2.864"
						transform="rotate(-90 616.3845 -149.3975)"
					/>
					<g>
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(744.317 406.799)"
						/>
					</g>
					<rect
						width="17.957"
						height="43.895"
						rx="2.864"
						transform="rotate(-90 605.5875 -160.1945)"
					/>
				</g>
			</g>
		</svg>
	),
	stretch: (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.8 86.8">
			<g>
				<g transform="translate(-1022.434 -406.799)">
					<g>
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(1022.434 481.61)"
						/>
					</g>
					<rect
						width="17.957"
						height="43.895"
						rx="2.864"
						transform="rotate(-90 760.9365 -282.9625)"
					/>
					<g>
						<rect
							width="86.827"
							height="6"
							rx="3"
							transform="translate(1022.434 406.799)"
						/>
					</g>
					<rect
						width="17.957"
						height="43.895"
						rx="2.864"
						transform="rotate(-90 739.146 -304.753)"
					/>
				</g>
			</g>
		</svg>
	),

	flow: (
		<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M2.31212 9L1 10.5094L4.77355 13.7897L6.28297 15.1018L7.59509 13.5924L9.13456 11.8214L11.3988 13.7897L12.9082 15.1018L14.2203 13.5924L15.7584 11.823L18.0209 13.7897L19.5303 15.1018L20.8424 13.5924L22.8106 11.3283L21.3012 10.0162L19.333 12.2803L15.5594 9L14.2473 10.5094L14.249 10.5109L12.7109 12.2803L8.93736 9L8.05395 10.0163L6.08567 12.2803L2.31212 9Z"
			/>
		</svg>
	),

	left_new: (
		<svg
			viewBox="0 0 24 24"
			width={24}
			height={24}
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M3.25 21V3h1.5v18h-1.5ZM8 11.25A1.75 1.75 0 0 1 6.25 9.5V7c0-.966.784-1.75 1.75-1.75h11c.966 0 1.75.784 1.75 1.75v2.5A1.75 1.75 0 0 1 19 11.25H8ZM7.75 9.5c0 .138.112.25.25.25h11a.25.25 0 0 0 .25-.25V7a.25.25 0 0 0-.25-.25H8a.25.25 0 0 0-.25.25v2.5ZM8 18.75A1.75 1.75 0 0 1 6.25 17v-2.5c0-.966.784-1.75 1.75-1.75h6c.966 0 1.75.784 1.75 1.75V17A1.75 1.75 0 0 1 14 18.75H8ZM7.75 17c0 .138.112.25.25.25h6a.25.25 0 0 0 .25-.25v-2.5a.25.25 0 0 0-.25-.25H8a.25.25 0 0 0-.25.25V17Z"
			/>
		</svg>
	),
	justbetween_new: (
		<svg
			width={24}
			height={24}
			viewBox="0 0 24 24"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M21.75 21V3h-1.5v18h1.5ZM3.75 21V3h-1.5v18h1.5ZM10.75 8A1.75 1.75 0 0 0 9 6.25H7A1.75 1.75 0 0 0 5.25 8v8c0 .966.784 1.75 1.75 1.75h2A1.75 1.75 0 0 0 10.75 16V8ZM9 7.75a.25.25 0 0 1 .25.25v8a.25.25 0 0 1-.25.25H7a.25.25 0 0 1-.25-.25V8A.25.25 0 0 1 7 7.75h2ZM18.75 8A1.75 1.75 0 0 0 17 6.25h-2A1.75 1.75 0 0 0 13.25 8v8c0 .966.784 1.75 1.75 1.75h2A1.75 1.75 0 0 0 18.75 16V8ZM17 7.75a.25.25 0 0 1 .25.25v8a.25.25 0 0 1-.25.25h-2a.25.25 0 0 1-.25-.25V8a.25.25 0 0 1 .25-.25h2Z"
			/>
		</svg>
	),
	center_new: (
		<svg
			width={24}
			height={24}
			viewBox="0 0 24 24"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M18 5.25h-5.25V3h-1.5v2.25H6A1.75 1.75 0 0 0 4.25 7v2.5c0 .966.784 1.75 1.75 1.75h5.25v1.5H9a1.75 1.75 0 0 0-1.75 1.75V17c0 .966.784 1.75 1.75 1.75h2.25V21h1.5v-2.25H15A1.75 1.75 0 0 0 16.75 17v-2.5A1.75 1.75 0 0 0 15 12.75h-2.25v-1.5H18a1.75 1.75 0 0 0 1.75-1.75V7A1.75 1.75 0 0 0 18 5.25Zm.25 4.25a.25.25 0 0 1-.25.25H6a.25.25 0 0 1-.25-.25V7A.25.25 0 0 1 6 6.75h12a.25.25 0 0 1 .25.25v2.5ZM15 17.25a.25.25 0 0 0 .25-.25v-2.5a.25.25 0 0 0-.25-.25H9a.25.25 0 0 0-.25.25V17c0 .138.112.25.25.25h6Z"
			/>
		</svg>
	),
	right_new: (
		<svg
			width={24}
			height={24}
			viewBox="0 0 24 24"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M20.75 21V3h-1.5v18h1.5ZM16 11.25a1.75 1.75 0 0 0 1.75-1.75V7A1.75 1.75 0 0 0 16 5.25H5A1.75 1.75 0 0 0 3.25 7v2.5c0 .966.784 1.75 1.75 1.75h11Zm.25-1.75a.25.25 0 0 1-.25.25H5a.25.25 0 0 1-.25-.25V7A.25.25 0 0 1 5 6.75h11a.25.25 0 0 1 .25.25v2.5ZM16 18.75A1.75 1.75 0 0 0 17.75 17v-2.5A1.75 1.75 0 0 0 16 12.75h-6a1.75 1.75 0 0 0-1.75 1.75V17c0 .966.784 1.75 1.75 1.75h6Zm.25-1.75a.25.25 0 0 1-.25.25h-6a.25.25 0 0 1-.25-.25v-2.5a.25.25 0 0 1 .25-.25h6a.25.25 0 0 1 .25.25V17Z"
			/>
		</svg>
	),
	alignStretch: ( 
		<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M21.75 21V3h-1.5v18h1.5ZM3.75 21V3h-1.5v18h1.5ZM17.5 10.75c.69 0 1.25-.56 1.25-1.25v-3c0-.69-.56-1.25-1.25-1.25h-11c-.69 0-1.25.56-1.25 1.25v3c0 .69.56 1.25 1.25 1.25h11Zm-.25-1.5H6.75v-2.5h10.5v2.5ZM17.5 18.75c.69 0 1.25-.56 1.25-1.25v-3c0-.69-.56-1.25-1.25-1.25h-11c-.69 0-1.25.56-1.25 1.25v3c0 .69.56 1.25 1.25 1.25h11Zm-.25-1.5H6.75v-2.5h10.5v2.5Z"/>
		</svg>
	),
	alignCenterR: (
		<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M3 11.25h3v1.5H3v-1.5ZM18 11.25h3v1.5h-3v-1.5ZM10.5 11.25h3v1.5h-3v-1.5Z"/>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M12.75 18c0 .97.78 1.75 1.75 1.75H17c.97 0 1.75-.78 1.75-1.75V6c0-.97-.78-1.75-1.75-1.75h-2.5c-.97 0-1.75.78-1.75 1.75v12Zm1.75.25a.25.25 0 0 1-.25-.25V6c0-.14.11-.25.25-.25H17c.14 0 .25.11.25.25v12c0 .14-.11.25-.25.25h-2.5ZM5.25 15c0 .97.78 1.75 1.75 1.75h2.5c.97 0 1.75-.78 1.75-1.75V9c0-.97-.78-1.75-1.75-1.75H7c-.97 0-1.75.78-1.75 1.75v6Zm1.75.25a.25.25 0 0 1-.25-.25V9c0-.14.11-.25.25-.25h2.5c.14 0 .25.11.25.25v6c0 .14-.11.25-.25.25H7Z"/>
		</svg>
	),
	alignStartR: (
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			<path fill-rule="evenodd" d="M3 3.25h18v1.5H3v-1.5ZM12.75 8c0-.97.78-1.75 1.75-1.75H17c.97 0 1.75.78 1.75 1.75v11c0 .97-.78 1.75-1.75 1.75h-2.5c-.97 0-1.75-.78-1.75-1.75V8Zm1.75-.25a.25.25 0 0 0-.25.25v11c0 .14.11.25.25.25H17c.14 0 .25-.11.25-.25V8a.25.25 0 0 0-.25-.25h-2.5ZM5.25 8c0-.97.78-1.75 1.75-1.75h2.5c.97 0 1.75.78 1.75 1.75v6c0 .97-.78 1.75-1.75 1.75H7c-.97 0-1.75-.78-1.75-1.75V8ZM7 7.75a.25.25 0 0 0-.25.25v6c0 .14.11.25.25.25h2.5c.14 0 .25-.11.25-.25V8a.25.25 0 0 0-.25-.25H7Z" clip-rule="evenodd"/>
		</svg>
	),
	alignEndR: (
		<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M3 20.75h18v-1.5H3v1.5ZM12.75 16c0 .97.78 1.75 1.75 1.75H17c.97 0 1.75-.78 1.75-1.75V5c0-.97-.78-1.75-1.75-1.75h-2.5c-.97 0-1.75.78-1.75 1.75v11Zm1.75.25a.25.25 0 0 1-.25-.25V5c0-.14.11-.25.25-.25H17c.14 0 .25.11.25.25v11c0 .14-.11.25-.25.25h-2.5ZM5.25 16c0 .97.78 1.75 1.75 1.75h2.5c.97 0 1.75-.78 1.75-1.75v-6c0-.97-.78-1.75-1.75-1.75H7c-.97 0-1.75.78-1.75 1.75v6Zm1.75.25a.25.25 0 0 1-.25-.25v-6c0-.14.11-.25.25-.25h2.5c.14 0 .25.11.25.25v6c0 .14-.11.25-.25.25H7Z"/>
		</svg>
	),
	alignStretchR: (
		<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M21 2.25H3v1.5h18v-1.5ZM21 20.25H3v1.5h18v-1.5ZM10.75 6.5c0-.69-.56-1.25-1.25-1.25h-3c-.69 0-1.25.56-1.25 1.25v11c0 .69.56 1.25 1.25 1.25h3c.69 0 1.25-.56 1.25-1.25v-11Zm-1.5.25v10.5h-2.5V6.75h2.5ZM18.75 6.5c0-.69-.56-1.25-1.25-1.25h-3c-.69 0-1.25.56-1.25 1.25v11c0 .69.56 1.25 1.25 1.25h3c.69 0 1.25-.56 1.25-1.25v-11Zm-1.5.25v10.5h-2.5V6.75h2.5Z"/>
		</svg>
	),
};

const Alignment = (props) => {
	const {
		value,
		onChange,
		alignIcons,
		options,
		responsive,
		device,
		label,
		disableJustify,
		toolbar,
		setDevice,
	} = props;

	const _set = (val) => {
		onChange(
			responsive ? Object.assign({}, value, { [device]: val }) : val
		);
	};

	const align = options
		? options
		: disableJustify
			? ['left', 'center', 'right']
			: ['left', 'center', 'right', 'justify'];
	const finalVal = value ? (responsive ? value[device] || '' : value) : '';
	// Check alignIcons Availability
	const iconSVG = alignIcons
		? alignIcons
		: ['left', 'center', 'right', 'justify'];

	return (
		<div
			className={`ultp-field-wrap ultp-field-alignment ${toolbar ? 'ultp-align-inline' : ''}${alignIcons?.length ? ' ultp-row-alignment' : ''}`}
		>
			<div className="ultp-field-label-responsive">
				{label && <label>{label}</label>}
				{responsive && (
					<ResponsiveDevice setDevice={setDevice} device={device} />
				)}
			</div>
			<div className={`ultp-sub-field`}>
				{align.map((data, k) => {
					return (
						<span
							tabIndex={0}
							key={k}
							onClick={() => _set(data)}
							className={
								'ultp-align-button' +
								(data == finalVal ? ' alignActive' : '')
							}
						>
							{alignIcon[iconSVG[k]]}
							{toolbar ? 'Align text ' + data : ''}
						</span>
					);
				})}
			</div>
		</div>
	);
};

export default Alignment;
