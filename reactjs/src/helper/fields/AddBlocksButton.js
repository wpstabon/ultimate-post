export default function AddBlockButton({onClick}) {
    return (
        <button
            type="button"
            aria-haspopup="true"
            aria-expanded="false"
            className="components-button block-list-appender__toggle block-editor-button-block-appender"
            aria-label="Add block"
        >
            <span
                data-wp-c16t="true"
                data-wp-component="VisuallyHidden"
                className="components-visually-hidden css-0 e19lxcc00"
                style={{
                    border: 0,
                    clip: "rect(1px, 1px, 1px, 1px)",
                    clipPath: "inset(50%)",
                    height: 1,
                    margin: "-1px",
                    overflow: "hidden",
                    padding: 0,
                    position: "absolute",
                    width: 1,
                    overflowWrap: "normal",
                }}
            >
                Add block
            </span>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width={24}
                height={24}
                aria-hidden="true"
                focusable="false"
            >
                <path d="M18 11.2h-5.2V6h-1.6v5.2H6v1.6h5.2V18h1.6v-5.2H18z" />
            </svg>
        </button>
    );
}
