const { __ } = wp.i18n
import IconPack, { SocialIconPack } from './tools/IconPack'
const { Dropdown } = wp.components
const { useState } = wp.element

const Icon = ({ value, label, onChange, inline, isSocial, dynamicClass, selection, hideFilter }) => {
    const [ searchQuery, setSearchQuery] = useState('');
    const [ iconType, setIconType ] = useState('');
    let iconLists;
    iconLists = isSocial ? { ...SocialIconPack } : { ...IconPack };

    if ( selection && selection.length ) {
        iconLists = Object.fromEntries( Object.entries(iconLists).filter(([key]) =>  selection?.includes(key)) );
    }

    const _filterValue = () => {
        let iconSlug = Object.keys(iconLists)
        if (searchQuery) {
            iconSlug = iconSlug.filter(el => el.includes(searchQuery.toLowerCase()))
        }
        if (iconType) {
            iconSlug = iconSlug.filter(el => el.includes(iconType))
        }
        return iconSlug;
    }
    
    return(
        <div className={`ultp-field-wrap ultp-field-icons`}>
            { label && !inline &&
                <label>{label}</label>
            }
            
            { !inline && <div className="ultp-sub-field">
                <Dropdown
                    popoverProps={{ placement: 'bottom right' }}
                    // position="bottom right"  
                    className=""
                    renderToggle={({ onToggle, onClose }) => (
                        <div className="ultp-icon-input">
                            { value && iconLists[value] &&
                                <>
                                    
                                    <div onClick={() => onToggle()}>{ iconLists[value] }</div>
                                    <div className="ultp-icon-close" onClick={() => onChange('') }></div>
                                    
                                </>
                            }
                        <span onClick={() => onToggle()}></span>
                    </div>
                    )}
                    renderContent={() => (
                        <div className="ultp-sub-dropdown ultp-icondropdown-container">
                            <div className="ultp-search-icon">
                                <input type="text" placeholder="Search Icons" onChange={(e)=> setSearchQuery(e.target.value)} />
                            </div>
                            {
                                !hideFilter && <div className="ultp-searchIcon-filter">
                                    {__('Icon Type Name', 'ultimate-post')}
                                    <select onChange = {e => setIconType(e.target.value)}>
                                        <option selected value="">{__('Show All Icon', 'ultimate-post')}</option>
                                        <option value="solid">{__('Solid Icon', 'ultimate-post')}</option>
                                        <option value="line">{__('Line Icon', 'ultimate-post')}</option>
                                    </select>
                                </div>
                            }
                            <div className="ultp-dropdown-icon">
                                { _filterValue().map( (key) => {
                                    return (
                                        <span onClick={() => onChange(key)} key={key} className={`${value == key ? 'active' : key}`}>{iconLists[key]}</span>
                                    );
                                })}
                            </div>
                        </div>
                    )}
                />
            </div> }
            {
                inline && 
                <div className="ultp-sub-dropdown ultp-icondropdown-container">
                    <div className="ultp-search-icon">
                        <input type="text" placeholder="Search Icons" onChange={(e)=> setSearchQuery(e.target.value)} />
                    </div>
                    <div className="ultp-searchIcon-filter">
                        {__('Icon Type Name', 'ultimate-post')}
                        <select onChange = {e => setIconType(e.target.value)}>
                            <option selected value="">{__('Show All Icon', 'ultimate-post')}</option>
                            <option value="solid">{__('Solid Icon', 'ultimate-post')}</option>
                            <option value="line">{__('Line Icon', 'ultimate-post')}</option>
                        </select>
                    </div>
                    <div className="ultp-dropdown-icon">
                        { _filterValue().map( (key) => {
                            return (
                                <span onClick={() => onChange(key)} key={key} className={`${value == key ? 'active' : key}`}>{iconLists[key]}</span>
                            );
                        })}
                    </div>
                </div>
            }
        </div>
    )
}
export default Icon