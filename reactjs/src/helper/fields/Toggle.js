import { updateChildAttr } from "../CommonPanel"
import UltpLinkGenerator from "../UltpLinkGenerator"

const {__} = wp.i18n
const { Fragment } = wp.element
const { ToggleControl } = wp.components

const Toggle = (props) => {
    const {value, label, pro, help, onChange,clientId, updateChild, disabled = false, showDisabledValue = false} = props

    const _onChange = (val) => {
        if (!disabled) {
            onChange(val)
        }
    }

    const setValue = (val) => {
        if (pro) {
            if (ultp_data.active) {
                _onChange(val)
            } else {
                // window.open(ultp_data.premium_link, '_blank');
                // window.open('https://www.wpxpo.com/postx/all-features/', '_blank');
            }
        } else {
            _onChange(val)
        }
        if(updateChild) {
            updateChildAttr(clientId);
        }
    }

    return(
        <div className={`ultp-field-wrap ultp-field-toggle`}>
            <div className="ultp-sub-field">
                <ToggleControl
                    __nextHasNoMarginBottom={true}
                    help={ help }
                    checked={ disabled ? (showDisabledValue ? value : false) : value }
                    label={ label ? <Fragment>{label} {(pro && !ultp_data.active) ?<a className={`ultp-field-pro-message-inline`} href={UltpLinkGenerator('https://www.wpxpo.com/postx/all-features/', 'blockProFeat', ultp_data.affiliate_id)} target="_blank">{__('Pro Features','ultimate-post')}</a>:''}</Fragment> : '' }
                    onChange={ (val) => setValue(val) }
                />
            </div>
        </div>
    )
}
export default Toggle