const { __ } = wp.i18n
import { updateChildAttr } from "../CommonPanel";
import UltpLinkGenerator from "../UltpLinkGenerator";
import ResponsiveDevice from "./tools/ResponsiveDevice";
import Units from "./tools/Units";

const Range = (props) => {
    const {value, unit, onChange, label, responsive, pro, device, setDevice, step, min, max, help, clientId, updateChild, _inline} = props

    const _steps = () => _filterValue('unit') == 'em' ? .01 : ( step || 1 )

    const _filterValue = (type) => {
        if ( type == 'unit' ) {
            if ( value?.onlyUnit ) {
                return value?.unit || 'px';
            }
            return value ? ( responsive ? (value['u'+device] || 'px') : (value.unit || 'px') ) : 'px'
        } else {
            if ( value?.onlyUnit ) {
                return value?._value || '';
            }
            return value ? ( responsive ? ( value[device] || '' ): value ) : ''
        }
    }

    const setSettings = (val,type) => {
        let final = value ? {...value} : {}
        if ( value?.onlyUnit ) {
            if ( type == 'unit' ) {
                final['unit'] = val;
            } else {
                final['_value'] = val;
            }
        } else {
            if (device) {
                if (unit && (!final?.hasOwnProperty('u'+device))) {
                    final['u'+device] = final?.hasOwnProperty('unit') ? final.unit : 'px'
                }
            }
            if (type == 'unit' && (responsive)) {
                final['u'+device] = val;
            } else {
                final = (responsive) ? Object.assign({}, value, final, { [device]: val }) : val
                final = min ? ( final < min ? min : final ) : ( final < 0 ? 0 : final )
                final = max ? ( final > max ? max : final ) : ( final > 1000 ? 1000 : final )
            }
        }
        if (pro) {
            if (ultp_data.active) {
                onChange(final)
            }
        } else {
            onChange(final)
        }
        if(updateChild) {
            updateChildAttr(clientId);
        }
    }

    
    return (
        <div className={'ultp-field-wrap ultp-field-range  ' + (responsive ? 'ultp-base-control-responsive' : '') +  ' ' + ( (pro && !ultp_data.active) ? 'ultp-pro-field':'' ) + (_inline ? ' ultp-range-inline' : '') }>
            <div className="ultp-field-label-responsive">
                { label && <label>{label}</label>}
                { responsive &&
                    <ResponsiveDevice setDevice={setDevice} device={device}/>
                }
                { unit &&
                    <Units
                        unit={unit}
                        value={_filterValue('unit')}
                        setSettings={setSettings}
                    />
                }
            </div>
            <div className="ultp-range-control">
                <div className="ultp-range-input">
                    <input
                        type="range"
                        min={min}
                        max={max}
                        value={_filterValue()}
                        step={_steps()}
                        onChange={e => setSettings(e.target.value)}
                    />
                    <input
                        type="number"
                        min={min}
                        max={max}
                        value={_filterValue()}
                        step={_steps()}
                        onChange={e => setSettings(e.target.value)}
                    />
                </div>
            </div>
            { help &&
                <div className="ultp-sub-help"><i>{help}</i></div>
            }
            { (pro && !ultp_data.active) &&
                <div className="ultp-field-pro-message">{__('Unlock It! Explore More','ultimate-post')} <a href={UltpLinkGenerator('https://www.wpxpo.com/postx/all-features/', 'blockProFeat', ultp_data.affiliate_id)} target="_blank">{__('Pro Features','ultimate-post')}</a></div>
            }
        </div>
    )
}
export default Range