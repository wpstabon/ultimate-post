const Tag = (props) => {
    const { value, options, label, toolbar, disabled, onChange, inline } = props;

    const tagValue = options ||
        [{value:'h1',label:'H1'},
        {value:'h2',label:'H2'},
        {value:'h3',label:'H3'},
        {value:'h4',label:'H4'},
        {value:'h5',label:'H5'},
        {value:'h6',label:'H6'},
        {value:'p',label:'P'},
        {value:'div',label:'DIV'},
        {value:'span',label:'SPAN'}
    ];

    const _set = (val) => {
        if (disabled && value == val) {
            onChange('')
        } else {
            onChange(val)
        }
    }

    return(
        <div className={`ultp-field-wrap ultp-field-tag ${inline ? 'ultp-taginline' : ''} ${toolbar?'ultp-tag-inline':''}`}>
            { label && 
                <label>{label}</label>
            }
            <div className="ultp-sub-field">
                { tagValue.map((data, index) => {
                    return ( <span key={index} tabIndex={0} className={data.value==value?'ultp-tag-button active':'ultp-tag-button'} onClick={()=>_set(data.value)}>{data.label}</span> )
                })}
            </div>
        </div>
    )
}
export default Tag