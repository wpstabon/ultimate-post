const RadioImage = (props) => {
    const {value, options, label, onChange, isText} = props
    return(
        <div className="ultp-field-wrap ultp-field-radio-image">
            { label && 
                <label>{label}</label>
            }
            <div className="ultp-field-radio-image-content">
                { isText ?
                    options.map((data, index) => {
                        return ( <div className="ultp-field-radio-text" key={index} onClick={()=>onChange(data.value)}><span className={data.value==value?'active':''}>{data.label}</span></div> )
                    })
                    :
                    <div className="ultp-field-radio-image-items">
                    {options.map((data, index) => {
                        return ( <div className={data.value==value?'ultp-radio-image active':'ultp-radio-image'} key={index} onClick={()=>onChange(data.value)}><img loading="lazy" src={data.url} /></div> )
                    })}
                    </div>
                }
            </div>
        </div>
    )
}
export default RadioImage