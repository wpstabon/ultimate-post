const { __ } = wp.i18n
const { useState, useEffect } = wp.element
const { Dropdown, SelectControl, ToolbarButton, TabPanel } = wp.components
import GoogleList from './tools/GoogleFonts'
import SystemList from './tools/SystemFonts'
import Number from "./Number"
import icons from '../../helper/icons'
import ResponsiveDevice from "./tools/ResponsiveDevice";
import { getCustomFonts } from '../CommonPanel'
import UltpLinkGenerator from '../UltpLinkGenerator'
import { generateTypoStyle, getPresetTypoLists, getTypoVariableObj, handleTypoFieldPresetData, presetKeyToLabel, redirectToPostxSettingsPannel } from '../CommonFunctions'
const { fontSizes } = wp.data.select( 'core/block-editor' ).getSettings()


const Typography = (props) => {
    const { value, disableClear, label, device, setDevice, onChange, presetSetting, presetSettingParent, isToolbar = false } = props
    const [state, setState] = useState({open: false, text: '', custom: false});
    const [openGlobalTypo, setOpenGlobalTypo] = useState(false);
    const {open, text, custom} = state;
    const CustomList = getCustomFonts();

    const styleOptions = [
        { value: 'normal', label: __('Normal','ultimate-post') },
        { value: 'italic', label: __('Italic','ultimate-post') },
        { value: 'oblique', label: __('Oblique','ultimate-post') },
        { value: 'initial', label: __('Initial','ultimate-post') },
        { value: 'inherit', label: __('Inherit','ultimate-post') },
    ];

    const decorationOptions = [
        { value: 'none', label: __('None','ultimate-post') },
        { value: 'inherit', label: __('Inherit','ultimate-post') },
        { value: 'underline', label: __('Underline','ultimate-post') },
        { value: 'overline', label: __('Overline','ultimate-post') },
        { value: 'line-through', label: __('Line Through','ultimate-post') },
    ];

    useEffect(() => {
        const fetchData = async () => {
            const _size = getSize();
            let hasItem = false;
            (fontSizes||[{size:13},{size:16},{size:20}]).forEach( data => { 
                if (_size == data.size) {
                    hasItem = true;
                }
            })
            if (!hasItem && typeof value.size == 'undefined') {
                hasItem = true;
            }
            setState({...state, custom: hasItem})
        };
        fetchData();
    }, []);


    const _getWeight = (value) => {
        if( value && value.family && value.family != 'none' && handleTypoFieldPresetData(value.family) ){
            let weight = GoogleList.filter( o => { return o.n == value.family } )
            if (weight.length == 0) {
                weight = SystemList.filter( o => { return o.n == value.family } )
            }
            if (weight.length == 0) {
                weight = CustomList.filter( o => { return o.n == value.family } )
            }
            if (typeof weight[0] != 'undefined') {
                weight = weight[0].v
                return weight.map( ( w ) => {
                    return { value: w, label: w };
                } );
            }
            return [{ value: '', label: '- None -' }];
        }else{
            return [{value: '100', label: '100'},{value: '200', label: '200'},{value: '300', label: '300'},{value: '400', label: '400'},{value: '500', label: '500'},{value: '600', label: '600'},{value: '700', label: '700'},{value: '800', label: '800'},{value: '900', label: '900'}];
        }
    }

    const setSettings = (type, val, source) => {
        if (type == 'family') {
            if (val) {
                setState({...state, open: false});
                // val = { [type]:val, type:( source == 'preset_font' ? val.replace('_font_family', '_font_family_type') : (source == 'google' ? GoogleList : source == 'custom_font' ? CustomList : SystemList ).filter( o => { return o.n == val } )[0].f) }
                val = { [type]:val, type:( (source == 'google' ? GoogleList : source == 'custom_font' ? CustomList : SystemList ).filter( o => { return o.n == val } )[0].f) }
                const fontWeight = _getWeight(val);
                if(fontWeight[0]) {
                    val = {...val , ['weight']:fontWeight[0].value }
                }
            }
        } else if (type == 'define') {
            val = {size: Object.assign({}, value.size ,{[device]: val})} 
        } else {
            val = {[type]:val}
        }
        onChange( Object.assign( {}, value, val ) )
    }

    const searchText = (val) => {
        setState({...state, text: val});
    }

    const _fontArray = (fonts) => {
        return fonts.filter( ( name ) => {
            if (text) {
                if ((name.n).toLowerCase().indexOf(text) !== -1) {
                    return { value: name.n };
                }
            } else {
                return { value: name.n };
            }
        } );
    }

    const getSize = () => {
        return value?.size ? value.size[device] : ''
    }

    const typoHtml = () => {
        return <div className={`ultp-field-typography`}>
            {
                !presetSettingParent && <div className="ultp-typo-option">
                    { custom ?
                    <Number
                        min={ 1 }
                        max={ 300 }
                        step={ 1 }
                        unit={['px','em','rem']}
                        responsive
                        typo={true}
                        label={__('Size','ultimate-post')}
                        value={ value && value.size }
                        device={device}
                        setDevice={setDevice}
                        onChange={ v => setSettings('size', v )}
                        handleTypoFieldPresetData={handleTypoFieldPresetData}
                    />
                    :
                    <>
                        <div className="ultp-field-label-responsive">
                            <label>Size</label>
                            <ResponsiveDevice setDevice={setDevice} device={device} />
                        </div>
                        <div className="ultp-default-font-field">
                            { (fontSizes||[{size:13},{size:16},{size:20}]).map( (data, k) => {
                                return <span key={k} className={'ultp-default-font'+(_size == data.size ? ' active': '')} onClick={() => { setSettings('define', data.size ); setState({...state, custom:false}); }}>{data.size}</span>
                            })}
                        </div>
                    </>
                    }
                    <div className={`ultp-typo-custom-setting`+(custom?' active':'')} onClick={() => setState({...state, custom: !custom}) }><span className="dashicons dashicons-admin-settings"></span></div>
                </div>
            }

            <div className="ultp-typo-container">
                <div className={`ultp-typo-section`}>
                    <div className={`ultp-typo-family ultp-typo-family-wrap`}>
                        <div className={`ultp-typo-family`}>
                            <label>Family</label>
                            <span className={`ultp-family-field-data ${presetSettingParent ? 'parent-typo' : ''}`} onClick={ () => setState({...state, open: !open}) }>
                                { (value && handleTypoFieldPresetData(value.family)) || __('Select Font','ultimate-post') }
                                { open ? <span className= "dashicons dashicons-arrow-up-alt2"></span> : <span className="dashicons dashicons-arrow-down-alt2"></span> }
                            </span>
                            { open &&
                                <span className="ultp-family-field">
                                    <span className="ultp-family-list">
                                        <input type="text" onChange={e => searchText(e.target.value)} placeholder="Search.." value={text}/>
                                        { _CustomFont.length > 0 && !presetSetting &&
                                            <>
                                                <span className="ultp-family-disabled">Custom Fonts</span>
                                                {_CustomFont.map((item, k) => <span className="ultp_custom_font_option" key={k} onClick={ () => setSettings('family', item.n, 'custom_font' ) }>{item.n} {!ultp_data.active && <a href={customFontProUrl} target="blank">(PRO)</a>}</span>)}
                                            </>
                                        }
                                        { _System.length > 0 &&
                                            <>
                                                <span className="ultp-family-disabled">System Fonts</span>
                                                {_System.map((item, k) => <span key={k} onClick={ () => setSettings('family', item.n, 'system' ) }>{item.n}</span>)}
                                            </>
                                        }
                                        { googleDisabled ?
                                            <span className="ultp-family-disabled" style={{color:'red'}}>Google Fonts Disabled</span>
                                            :
                                            <>
                                                { _Google.length > 0 &&
                                                    <>
                                                        <span className="ultp-family-disabled">Google Fonts</span>
                                                        {_Google.map((item, k) => <span key={k} onClick={ () => setSettings('family', item.n, 'google' ) }>{item.n}</span>)}
                                                    </>
                                                }
                                            </>
                                        }
                                    </span>
                                </span>
                            }
                        </div>
                    </div>
                </div>
                <div className={`ultp-typo-section ultp-typo-weight`}>
                    <SelectControl
                        __nextHasNoMarginBottom={true}
                        label={__('Weight','ultimate-post')}
                        value={ value&&value.weight }
                        options={ _getWeight(value) }
                        onChange={ v => setSettings('weight', v)} 
                    />
                </div>
                {
                    !presetSettingParent && <div className={`ultp-typo-section ultp-typo-height`}>
                        <Number
                            min={ 1 }
                            max={ 300 }
                            step={ 1 }
                            responsive
                            device={device}
                            setDevice={setDevice}
                            unit={['px','em','rem']}
                            label={__('Height','ultimate-post')}
                            value={ value && value.height }
                            onChange={ v => setSettings('height', v ) } 
                            handleTypoFieldPresetData={handleTypoFieldPresetData}
                        />
                    </div>
                }
            </div>
            <div className="ultp-typo-container ultp-typ-family ultp-typo-spacing-container" >
                <div className={`ultp-typo-section ultp-typo-spacing  ${presetSettingParent ? 'preset-active' : ''}`}>
                    <Number
                        min={ -10 }
                        max={ 30 }
                        responsive
                        step={ .1 }
                        unit={['px','em','rem']}
                        device={device}
                        setDevice={setDevice}
                        label={__('Spacing','ultimate-post')}
                        value={ value && value.spacing }
                        onChange={ v => setSettings('spacing', v ) }
                        handleTypoFieldPresetData={handleTypoFieldPresetData}
                    />
                </div>
                <div className="ultp-typo-container ultp-style-container" >
                    <div className="ultp-transform-style">
                        <span title="italic" style={{fontStyle: 'italic'}} className={value.style=='italic'?'active':''} onClick={() => setSettings('style', (value.style=='italic'?'normal':'italic'))}>l</span>
                        <span title="underline" style={{textDecoration: 'underline' }} className={value.decoration=='underline'?'active':''} onClick={() => setSettings('decoration', (value.decoration=='underline'?'none':'underline'))}>U</span>
                        <span title="line-through" style={{textDecoration: 'line-through' }} className={value.decoration=='line-through'?'active':''} onClick={() => setSettings('decoration', (value.decoration=='line-through'?'none':'line-through'))}>U</span>
                        <span title='uppercase' style={{textTransform: 'uppercase' }} className={transform=='uppercase'?'active':''} onClick={() => setSettings('transform', (transform=='uppercase'?'':'uppercase'))}>tt</span>
                        <span title='lowercase' style={{textTransform: 'lowercase' }} className={transform=='lowercase'?'active':''} onClick={() => setSettings('transform', (transform=='lowercase'?'':'lowercase'))}>tt</span>
                        <span title='capitalize' style={{textTransform: 'capitalize' }} className={transform=='capitalize'?'active':''} onClick={() => setSettings('transform', (transform=='capitalize'?'':'capitalize'))}>tt</span>
                    </div>
                </div>
            </div>
        </div>
    }
    
    const transform = value && value.transform ? value.transform : '';
    const isActive = value && value.openTypography ? true : false;
    const _size = getSize();

    const googleDisabled = (ultp_data.settings?.hasOwnProperty('disable_google_font') && ultp_data.settings['disable_google_font'] == 'yes') ? true : false;
    const _System = _fontArray(SystemList)
    const _Google = _fontArray(GoogleList)
    const _CustomFont = _fontArray(CustomList);

    const customFontProUrl = UltpLinkGenerator( '', 'customFont', ultp_data.affiliate_id);

    const globalTypos = getPresetTypoLists();

    return(
        <div className={`${isToolbar ? "test-class" : "ultp-field-wrap ultp-field-typo"} ${presetSettingParent ? 'ultp-preset-typo' : ''}`}>
            <div className="ultp-field-header ultp-flex-content">
                {
                    !isToolbar && (
                        <div className="ultp-field-label-responsive">
                        { 
                            label &&
                            <label>{label}</label>
                        }
                        </div>
                    )
                }

                {
                    presetSetting ? typoHtml() 
                    :
                    <Dropdown
                        className={`${isToolbar ? "" : "ultp-range-control"}`}
                        contentClassName={`${isToolbar ? "ultp-typography-toolbar" : ""}`}
                        renderToggle={({ isOpen, onToggle }) => {
                            return isToolbar ? (
                                    <ToolbarButton
                                        className="ultp-typography-toolbar-btn"
                                        label={label || "Typography"}
                                        onClick={() => {
                                            onToggle();
                                            setSettings("openTypography", 1);
                                        }}
                                    >
                                        <img src={ultp_data.url + 'assets/img/toolbar/typography.svg'}/>
                                    </ToolbarButton>
                                ) : (
                                    <div className="ultp-edit-btn">
                                        <div className="ultp-typo-control">
                                            { !disableClear && isActive &&
                                                <div className={ `active ultp-base-control-btn dashicons dashicons-image-rotate` } onClick={() => { if (isOpen) { onToggle(); } setSettings( 'openTypography', 0 );}}></div>
                                            }
                                            <div className={ (isActive ? 'active ' : '')+' ultp-icon-style' } onClick={() => { onToggle(); setSettings( 'openTypography', 1 ); }}>
                                                {icons.setting}
                                            </div>
                                        </div>
                                    </div>
                                )
                        }}
                        renderContent={ () => (
                            <>
                                {
                                    isToolbar ? (
                                        <TabPanel
                                            className="ultp-toolbar-tab"
                                            tabs={ [
                                                {
                                                    name: "typo",
                                                    title: label
                                                }
                                            ] }
                                        >
                                            {
                                                (tab) => (
                                                        <Content
                                                            value={value}
                                                            openGlobalTypo={openGlobalTypo}
                                                            setOpenGlobalTypo={setOpenGlobalTypo}
                                                            globalTypos={globalTypos}
                                                            onChange={onChange}
                                                            typoHtml={typoHtml}
                                                        />
                                                )
                                            }
                                        </TabPanel>
                                    ) : (
                                        <Content
                                            value={value}
                                            openGlobalTypo={openGlobalTypo}
                                            setOpenGlobalTypo={setOpenGlobalTypo}
                                            globalTypos={globalTypos}
                                            onChange={onChange}
                                            typoHtml={typoHtml}
                                        />
                                    )
                                }
                            </>
                        ) }/>
                }
                
                </div>
        </div>
    )
}

const Content = ({value, openGlobalTypo, setOpenGlobalTypo, globalTypos, onChange, typoHtml}) => {
    return (
        <div className='ultp-field-typo-settings'>
        <div className="ultp-field-global-typo ultp-typo-family ultp-typo-family-wrap">
            <div className={`ultp-typo-family`}>
                <div className="ultp-preset-label">
                    <div className="ultp-preset-label-content"> {__('Select Global Style', 'ultimate-post')} </div>
                    <div className="ultp-preset-label-link" onClick={() => redirectToPostxSettingsPannel('typo')}> Customize &nbsp;&gt;</div>
                </div>
                <span className="ultp-family-field-data" onClick={ () => setOpenGlobalTypo(!openGlobalTypo) }>
                    { (value && presetKeyToLabel( value.presetTypo, 'typo')) || __('Select Global','ultimate-post') }
                    { openGlobalTypo ? <span className= "dashicons dashicons-arrow-up-alt2"></span> : <span className="dashicons dashicons-arrow-down-alt2"></span> }
                </span>
                { openGlobalTypo &&
                    <div className="ultp-family-field">
                        <div className="ultp-family-list preset-family">
                            { globalTypos.map((item, k) => !['Body_and_Others_typo', 'Heading_typo', 'presetTypoCSS'].includes(item) && <span style={generateTypoStyle(item)} key={k} onClick={ () => { setOpenGlobalTypo(false); onChange(getTypoVariableObj(item)) } }>{presetKeyToLabel(item, 'typo')}</span>) }
                        </div>
                    </div>
                }
            </div>
        </div>
        {
            typoHtml()
        }
    </div>
    );
}

export default Typography