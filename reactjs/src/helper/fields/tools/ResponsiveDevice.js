/** @format */

const { __ } = wp.i18n;
import IconPack from "../../fields/tools/IconPack";
// import useDevice from "../../hooks/use-device";

const ResponsiveDevice = (props) => {
    const {setDevice, device} = props;
    // const editType = wp.data.select("core/edit-site") ? 'core/edit-site' : 'core/edit-post';

    // const [device, setDevice] = useDevice();

    const _set = (value) => {
        setDevice(value);
    };

    return (
        <div className={`ultp-responsive-device`}>
            <div
                className={`ultp-responsive-dropdown ultp-responsive-` + device}
            >
                <span onClick={() => _set("lg")}>{IconPack.desktop}</span>
                <span
                    className={device == "sm" ? "ultp-responsive-active" : ""}
                    onClick={() => _set("sm")}
                >
                    {IconPack.tablet}
                </span>
                <span
                    className={device == "xs" ? "ultp-responsive-active" : ""}
                    onClick={() => _set("xs")}
                >
                    {IconPack.mobile}
                </span>
            </div>
        </div>
    );
};
export default ResponsiveDevice;
