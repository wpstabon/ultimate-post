const Units = (props) => {
    const {setSettings, unit, value} = props
    const unitList =  typeof unit == 'object' ? unit : ['px', 'em','rem','%','vh','vw'];

    return(
        <div className="ultp-field-unit-list">
            <span className="ultp-unit-checked">{value ? value : 'px'}</span>
            {
                unitList.length > 1 &&
                <div className="ultp-field-unit-dropdown">
                    {   unitList.map((val, k) => (
                            <span 
                                key={k}
                                className={val == value ? 'ultp-unit-active' : ''} 
                                onClick={() => {
                                    setSettings(val,'unit')
                                }}> 
                                {val}
                            </span>
                    ))}
                </div>
            }
        </div>
    )
}

export default Units;