export default [
    {n:'none',f:'None'},
    {n:'Arial',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Tahoma',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Verdana',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Helvetica',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Times New Roman',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Trebuchet MS',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
    {n:'Georgia',f:'sans-serif',v:[100,200,300,400,500,600,700,800,900]},
];