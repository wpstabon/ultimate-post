const Checkbox = (props) => {
    const {value, label, onChange, options} = props

    const setValue = (val) => {
        var index = value.indexOf(val);
        if (index !== -1) {
            const arr = value.filter(item => item !== val)
            onChange(arr)
        } else {
            onChange(value.concat([val]))
        }
    }

    return (
        <div className="ultp-field-wrap ultp-field-checkbox">
            { label &&
                <label>{label}</label>
            }
            <div className="ultp-sub-field-checkbox">
                { options.map((data, index) => {
                    return (
                        <div key={index}>
                            <input onClick={()=>setValue(data.value)} id={index} type="checkbox" value={data.value} defaultChecked={value.indexOf(data.value)!=-1}/>
                            <label htmlFor={index}>{data.label}</label>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Checkbox