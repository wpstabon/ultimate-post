const { __ } = wp.i18n
const { Dropdown } = wp.components;
import icons from '../icons'

const inputSource = [
    {key:'hue', label:__('Hue', 'ultimate-post')},
    {key:'saturation', label:__('Saturation', 'ultimate-post')},
    {key:'brightness', min: 0, max: 200, label:__('Brightness', 'ultimate-post')},
    {key:'contrast', min: 0, max: 200, label:__('Contrast', 'ultimate-post')},
    {key:'invert', label:__('Invert', 'ultimate-post')},
    {key:'blur', min: 0, max: 50, label:__('Blur', 'ultimate-post')}
];

const Filter = ({value, label, onChange}) => {

    const isActive = value && value.openFilter ? true : false

    const _filterValue = (type) => {
        return value?.hasOwnProperty(type) ? value[type] : ''
    }

    const setSettings = (val,type) => {
        onChange( Object.assign({}, value, {[type]: val}) )
    }

    return (
        <div className="ultp-field-wrap ultp-field-imgfilter">
            { label &&
                <label>{label}</label>
            }
            <Dropdown
                className="ultp-range-control"
                renderToggle={({ isOpen, onToggle }) => (
                    <div className="ultp-edit-btn">
                        <div className="ultp-typo-control">
                            { isActive &&
                                <div className={ `active ultp-base-control-btn dashicons dashicons-image-rotate` } onClick={() => { if (isOpen) { onToggle(); } setSettings( 0, 'openFilter' );}}></div>
                            }
                            <div className={`${isActive && 'active '} ultp-icon-style`} onClick={() => { onToggle(); setSettings(1, 'openFilter'); }}>
                                {icons.setting}
                            </div>
                        </div>
                    </div>
                )}
                renderContent={() => (
                    <div className={'ultp-common-popup ultp-color-popup ultp-imgfilter-range'}>
                        { inputSource.map((data, k) => {
                            return (
                                <div key={k} className="ultp-range-input">
                                    <span>{data.label}</span>
                                    <input
                                        type="range"
                                        min={data.min||0}
                                        max={data.max||100}
                                        value={_filterValue(data.key)}
                                        step={1}
                                        onChange={e => setSettings(e.target.value, data.key)}
                                    />
                                    <input
                                        type="number"
                                        min={data.min||0}
                                        max={data.max||100}
                                        value={_filterValue(data.key)}
                                        step={1}
                                        onChange={e => setSettings(e.target.value, data.key)}
                                    />
                                </div>
                            )
                        })}
                    </div>
                )}
            />
        </div>
    )
}
export default Filter;