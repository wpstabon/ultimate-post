import {
	getPresetColorLists,
	handleTypoFieldPresetData,
	presetKeyToLabel,
	redirectToPostxSettingsPannel,
} from '../CommonFunctions';
import UltpLinkGenerator from '../UltpLinkGenerator';
const { __ } = wp.i18n;
const { ColorPicker, Dropdown, Tooltip } = wp.components;
const { Fragment } = wp.element;
const editor = wp.data.select('core/editor');
const { useState } = wp.element;

const Color = (props) => {
	const {
		label,
		value,
		onChange,
		disableClear,
		pro,
		inline,
		color2,
		presetSetting,
		value2 = null,
	} = props;

	const [presetTab, setPresetTab] = useState(
		presetSetting
			? false
			: typeof value == 'string' && value?.includes('var(--postx_preset')
				? true
				: false
	);

	const [openedIdx, setOpenedIdx] = useState(0);

	const _setColor = (color) => {
		if (pro && !ultp_data.active) {
			return;
		}

		if (value2 !== null) {
			const _val = Array(2).fill(undefined);
			_val[openedIdx] =
				typeof color === 'string'
					? color
					: 'rgba(' +
						color.rgb.r +
						',' +
						color.rgb.g +
						',' +
						color.rgb.b +
						',' +
						color.rgb.a +
						')';
			onChange(_val);
		} else {
			onChange(
				typeof color === 'string'
					? color
					: 'rgba(' +
							color.rgb.r +
							',' +
							color.rgb.g +
							',' +
							color.rgb.b +
							',' +
							color.rgb.a +
							')'
			);
		}
	};

	const _resetColor = () => {
		onChange(value2 !== null ? Array(2).fill('') : '');
	};

	const _getPreset = () => {
		return {
			theme: editor ? editor.getEditorSettings().colors : [],
			colors: getPresetColorLists(),
		};
	};

	const PresetColor = () => {
		const colorField = _getPreset();
		return (
			<div className="ultp-preset-color">
				{colorField.colors.length > 0 && (
					<Fragment>
						<div className="ultp-preset-label">
							<div className="ultp-preset-label-content">
								{__('PostX Color', 'ultimate-post')}{' '}
							</div>
							<div
								className="ultp-preset-label-link"
								onClick={() => redirectToPostxSettingsPannel()}
							>
								Customize &nbsp;&gt;
							</div>
						</div>
						{colorField.colors.map((val, k) => {
							return (
								<Tooltip
									key={k}
									position={'top'}
									text={presetKeyToLabel(val, 'color')}
								>
									<a
										className={`color_span ${value == val ? 'active' : ''}`}
										key={k}
										onClick={() => _setColor(val)}
										style={{ backgroundColor: val }}
									/>
								</Tooltip>
							);
						})}
					</Fragment>
				)}
				{colorField.theme.length > 0 && (
					<Fragment>
						<div className="ultp-preset-label-content">
							{__('Theme Color', 'ultimate-post')}
						</div>
						{colorField.theme.map((val, k) => {
							return (
								<Tooltip
									key={k}
									position={'top'}
									text={val.name}
								>
									<a
										className={`color_span ${value == val.color ? 'active' : ''}`}
										key={k}
										onClick={() => _setColor(val.color)}
										style={{ backgroundColor: val.color }}
									/>
								</Tooltip>
							);
						})}
					</Fragment>
				)}
			</div>
		);
	};

	const colorHtml = () => {
		return (
			<div
				className={`ultp-common-popup ultp-color-popup ultp-color-container ${presetTab ? 'active' : ''}`}
			>
				{value2 !== null && (
					<div
						style={{
							textAlign: 'center',
							fontWeight: 500,
							fontSize: 'large',
						}}
					>
						{openedIdx == 0
							? __('Normal Color', 'ultimate-post')
							: __('Hover Color', 'ultimate-post')}
					</div>
				)}
				{!presetSetting && (
					<div className="ultp-color-field-tab">
						<div
							className={`${presetTab ? 'active' : ''}`}
							onClick={() => setPresetTab(true)}
						>
							Preset Palette
						</div>
						<div
							className={`${presetTab ? '' : 'active'}`}
							onClick={() => setPresetTab(false)}
						>
							Custom
						</div>
					</div>
				)}
				<div className="ultp-color-field-options">
					{presetTab ? (
						<PresetColor />
					) : (
						<ColorPicker
							color={
								handleTypoFieldPresetData(value)
									? value
									: getPresetColorLists('colorcode', value)
							}
							onChangeComplete={(color) => _setColor(color)}
						/>
					)}
				</div>
			</div>
		);
	};

	if (presetSetting) {
		return colorHtml();
	}

	return (
		<div
			className={`ultp-field-wrap ultp-field-color ${inline ? 'ultp-color-inline' : 'ultp-color-block'} ${pro && !ultp_data.active ? 'ultp-pro-field' : ''}`}
		>
			{label && !inline && (
				<span className={`ultp-color-label`}>
					<label>{label}</label>
				</span>
			)}
			{inline ? (
				color2 ? (
					colorHtml()
				) : (
					<Fragment>
						<div className={`ultp-color-inline__label`}>
							<div className={`ultp-color-label__content`}>
								{inline && label && (
									<label className={`ultp-color-label`}>
										{label}
									</label>
								)}
								{value && (
									<>
										<span
											className={`ultp-color-preview`}
											style={{ backgroundColor: value }}
										></span>
										{value2 !== null && (
											<span
												className={`ultp-color-preview`}
												style={{
													backgroundColor: value2,
													marginLeft: '5px',
												}}
											></span>
										)}
									</>
								)}
							</div>
							<div className="ultp-color-label__content">
								{!disableClear && value && (
									<div
										className={
											'ultp-clear dashicons dashicons-image-rotate'
										}
										onClick={_resetColor}
									/>
								)}
								<Dropdown
									focusOnMount={true}
									className="ultp-range-control"
									renderToggle={({ onToggle, onClose }) => (
										<div
											className={'ultp-control-button'}
											onClick={() => {
												onToggle();
												setOpenedIdx(0);
											}}
											style={{ background: value }}
										/>
									)}
									renderContent={() => colorHtml()}
								/>
								{value2 !== null && (
									<Dropdown
										focusOnMount={true}
										className="ultp-range-control"
										renderToggle={({
											onToggle,
											onClose,
										}) => (
											<div
												className={
													'ultp-control-button'
												}
												onClick={() => {
													onToggle();
													setOpenedIdx(1);
												}}
												style={{
													background: value2,
													marginLeft: '10px',
												}}
											/>
										)}
										renderContent={() => colorHtml()}
									/>
								)}
							</div>
						</div>
					</Fragment>
				)
			) : (
				<div className={`ultp-sub-field`}>
					{label && (
						<span className={`ultp-color-label`}>
							{!disableClear && value && (
								<div
									className={
										'ultp-clear dashicons dashicons-image-rotate'
									}
                                    onClick={_resetColor}
								/>
							)}
						</span>
					)}
					<Dropdown
						className="ultp-range-control"
						// position=""
						popoverProps={{ placement: 'top left' }}
						focusOnMount={true}
						renderToggle={({ onToggle, onClose }) => (
							<div
								className={'ultp-control-button'}
								onClick={() => {
									onToggle();
									setOpenedIdx(0);
								}}
								style={{ background: value }}
							/>
						)}
						renderContent={() => colorHtml()}
					/>
					{value2 !== null && (
						<Dropdown
							className="ultp-range-control"
							// position="top left"
							popoverProps={{ placement: 'top left' }}
							focusOnMount={true}
							renderToggle={({ onToggle, onClose }) => (
								<div
									className={'ultp-control-button'}
									onClick={() => {
										onToggle();
										setOpenedIdx(1);
									}}
									style={{
										background: value2,
										marginLeft: '10px',
									}}
								/>
							)}
							renderContent={() => colorHtml()}
						/>
					)}
				</div>
			)}
			{pro && !ultp_data.active && (
				<div className="ultp-field-pro-message">
					{__('Unlock It! Explore More', 'ultimate-post')}{' '}
					<a
						href={UltpLinkGenerator(
							'https://www.wpxpo.com/postx/all-features/',
							'blockProFeat',
							ultp_data.affiliate_id
						)}
						target="_blank"
					>
						{__('Pro Features', 'ultimate-post')}
					</a>
				</div>
			)}
		</div>
	);
};
export default Color;
