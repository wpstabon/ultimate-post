const { __ } = wp.i18n;
const { Fragment, useState, useEffect, useRef } = wp.element;
import IconPack from '../../helper/fields/tools/IconPack';
import { updateChildAttr } from '../CommonPanel';
import UltpLinkGenerator from '../UltpLinkGenerator';
import cn from '../cn';
import ResponsiveDevice from './tools/ResponsiveDevice';

const Select = (props) => {
	const {
		label,
		responsive,
		options,
		multiple,
		value,
		pro,
		image,
		setDevice,
		device,
		onChange,
		help,
		condition,
		keys,
		beside,
		svg,
		svgClass,
		clientId,
		updateChild,
		defaultMedia,
		classes,
		proLink,
		dynamicHelpText
	} = props;
	const myRef = useRef();
	const [isOpen, setOpen] = useState(false);

	useEffect(() => {
		const handleClickOutside = (e) => {
			if (!myRef.current.contains(e.target)) {
				setOpen(false);
			}
		};
		document.addEventListener('mousedown', handleClickOutside);
		return () =>
			document.removeEventListener('mousedown', handleClickOutside);
	}, []);

	const _filterValue = () => {
		if (!multiple) {
			return value ? (responsive ? value[device] || '' : value) : '';
		} else {
			return value || [];
		}
	};

	const _set = (val) => {
		if (pro && !ultp_data.active) {
			return;
		}
		if (!multiple) {
			let noCondition = true;
			if (condition) {
				Object.keys(condition).forEach(function (key) {
					if (val == key) {
						noCondition = false;
						onChange(
							Object.assign({}, { [keys]: val }, condition[key])
						);
					}
				});
			}
			if (noCondition) {
				let final = responsive
					? Object.assign({}, value, { [device]: val })
					: val;
				onChange(final);
			}
		} else {
			let filter = value.length ? value : [];
			if (value.indexOf(val) == -1) {
				filter.push(val);
			}
			onChange(filter);
		}
		if (updateChild) {
			updateChildAttr(clientId);
		}
	};

	const getLabel = (val, svg) => {
		const data = options.filter((attr) => attr.value == val);
		if (data[0]) {
			return svg && data[0].svg ? data[0].svg : data[0].label;
		}
	};

	const removeItem = (val) => {
		onChange(value.filter((item) => item != val));
	};
	const handleDefaultMedia = (value) => {
		const defaultArr = options.filter((data) => data.value == value);
		return ultp_data.url + defaultArr[0]['img'];
	};

	const openLink = (link) => {
		window.open(
			link ? link : proLink ? proLink : ultp_data.premium_link,
			'_blank'
		);
	};

	return (
		<Fragment>
			<div
				ref={myRef}
				className={`ultp-field-wrap ultp-field-select ${classes ? classes : ''} ${pro && !ultp_data.active ? 'ultp-pro-field' : ''} `}
			>
				<div className={`${beside ? 'ultp-field-beside' : ''}`}>
					<div className="ultp-label-control">
						{label && <label>{label}</label>}
						{responsive && !multiple && (
							<ResponsiveDevice
								setDevice={setDevice}
								device={device}
							/>
						)}
					</div>
					<div
						className={`ultp-popup-select ${svg ? 'svgIcon ' + svgClass : ''}`}
					>
						<span
							tabIndex={0}
							className={
								(isOpen ? 'isOpen ' : '') +
								' ultp-selected-text'
							}
							onClick={() => setOpen(!isOpen)}
						>
							{multiple ? (
								<span className="ultp-search-value ultp-multiple-value ultp-select-dropdown--tag">
									{value.map((item, k) => (
										<span
											key={k}
											onClick={() => removeItem(item)}
										>
											{getLabel(item) &&
												getLabel(item).replaceAll(
													'&amp;',
													'&'
												)}
											<span className="ultp-select-close">
												×
											</span>
										</span>
									))}
								</span>
							) : (
								<span className="ultp-search-value">
									{defaultMedia ? (
										<img src={handleDefaultMedia(value)} />
									) : (
										getLabel(_filterValue(), svg)
									)}
								</span>
							)}
							{multiple && (
								<span className="ultp-search-divider"></span>
							)}
							<span className="ultp-search-icon">
								<i
									className={
										'dashicons dashicons-arrow-' +
										(isOpen ? 'up-alt2' : 'down-alt2')
									}
								/>
							</span>
						</span>
						{isOpen && (
							<ul>
								{options.map((item, k) => (
									<li
										key={k}
										onClick={() => {
											if (item.disabled) return;
											setOpen(!isOpen);
											if (item.pro) {
												if (ultp_data.active) {
													_set(item.value);
												} else if (
													item.isHeader ||
													item.isChild
												) {
													openLink(item.link);
												}
											} else {
												_set(item.value);
											}
										}}
										value={item.value}
										className={cn({
											'ultp-select-header': item.isHeader,
											'ultp-select-header-pro':
												item.isHeader &&
												item.pro &&
												!ultp_data.active,
											'ultp-select-group': item.isChild,
											'ultp-select-group-pro':
												item.isChild &&
												item.pro &&
												!ultp_data.active,
										})}
									>
										{svg && item.svg ? (
											item.svg
										) : image ? (
											<img
												alt={item.label || ''}
												src={ultp_data.url + item.img}
											/>
										) : (
											<>
												{item.label.replaceAll(
													'&amp;',
													'&'
												)}{' '}
												{item.isHeader &&
													item.pro &&
													!ultp_data.active && (
														<span className="ultp-pro-text2">
															(Get Pro)
														</span>
													)}
											</>
										)}
										{IconPack[item.icon] || ''}{' '}
										{item.pro &&
										!item.isHeader &&
										!item.isChild &&
										!ultp_data.active ? (
											<span
												onClick={() =>
													openLink(item.link)
												}
												className="ultp-pro-text"
											>
												[Pro]
											</span>
										) : null}
									</li>
								))}
							</ul>
						)}
					</div>
				</div>
				{help && (
					<div className="ultp-sub-help">
						<i>{help}</i>
					</div>
				)}
				
				{dynamicHelpText && keys == "queryQuick"  && (
					<div className="ultp-sub-help">
						{
							value == 'popular_post_1_day_view' ? 
							<i>- Most viewed posts published in the last 24 hours</i> 
							: ''
						},
						{
							value == 'popular_post_7_days_view' ? 
							<i>- Most viewed posts published in the last 7 days</i> 
							: ''
						}
						{
							value == 'popular_post_30_days_view' ? 
							<i>- Most viewed posts published in the last 30 days</i>
							: ''
						}
						{
							value == 'popular_post_all_times_view' ? 
							<i>- Most viewed posts of all time</i>
							: ''
						}
					</div>
				)}

				{pro && !ultp_data.active && (
					<div className="ultp-field-pro-message">
						{__('To Enable This Feature', 'ultimate-post')}{' '}
						<a
							href={UltpLinkGenerator(
								'https://www.wpxpo.com/postx/all-features/',
								'blockProFeat',
								ultp_data.affiliate_id
							)}
							target="_blank"
						>
							{__('Upgrade to Pro', 'ultimate-post')}
						</a>
					</div>
				)}
			</div>
		</Fragment>
	);
};
export default Select;
