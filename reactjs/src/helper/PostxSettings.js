const { __ } = wp.i18n;
const { apiFetch, editPost } = wp
const { Fragment, useState, useEffect } = wp.element;
const { RadioControl, Modal, ColorPicker, Dropdown } = wp.components

import Toggle from './fields/Toggle'
// import Number from './fields/Number'
import Range from './fields/Range'
// import PG1_Settings from '../blocks/post_grid_1/Settings';
// import PG2_Settings from '../blocks/post_grid_2/Settings';
// import PL1_Settings from '../blocks/post_list_1/Settings';
// import PL2_Settings from '../blocks/post_list_2/Settings';
// import PL3_Settings from '../blocks/post_list_3/Settings';
// import PL4_Settings from '../blocks/post_list_4/Settings';

const icons = {
    editorWidth: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="m46 22-8-7h-4v4l4 3H12l4-3v-4h-4l-8 7-1 3 1 3 8 7 2 1 2-1v-4l-4-3h26l-4 3v4l2 1 2-1 8-7 1-3-1-3zM2 50l-2-2V2l2-2 1 2v46l-1 2zm46 0-1-2V2l1-2 2 2v46l-2 2z"/></svg>,
    editorColor: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M46 10C41 4 33 0 25 0a26 26 0 0 0-10 48s7 3 11 2c2-1 3-2 3-4l-2-5c0-2 1-4 3-4l9-1 6-3c7-6 6-17 1-23zM10 24c-3 0-5-2-5-5s2-5 5-5c2 0 5 3 5 5s-3 5-5 5zm8-10c-2 0-5-2-5-5s3-5 5-5c3 0 5 3 5 5s-2 5-5 5zm14 0c-3 0-5-2-5-5s2-5 5-5 5 3 5 5-2 5-5 5zm9 10c-2 0-5-2-5-5s2-5 5-5 5 3 5 5-2 5-5 5z"/></svg>,
    // editorBlock: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M21 0H2L0 2v19l2 2h19l2-2V2l-2-2zm-3 19H6l-2-2 2-1h12l1 1-1 2zM48 0H29l-2 2v19l2 2h19l2-2V2l-2-2zm-4 19H32l-1-2 1-1h12l2 1-2 2zm-23 8H2l-2 2v19l2 2h19l2-2V29l-2-2zm-3 18H6l-2-1 2-2h12l1 2-1 1zm30-18H29l-2 2v19l2 2h19l2-2V29l-2-2zm-4 18H32l-1-1 1-2h12l2 2-2 1z"/></svg>,
    editorBreak: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M46 0H17c-2 0-4 2-4 4v6h20c4 0 7 3 7 7v20h6c2 0 4-2 4-4V4c0-2-2-4-4-4z"/><path d="M33 14H4c-2 0-4 1-4 3v29c0 2 2 4 4 4h29c2 0 4-2 4-4V17c0-2-2-4-4-4z"/></svg>,
    logo: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 48.3"><path d="M23 2c-3 1-5 3-5 6v9H8c-3 0-6 2-6 5l-2-2V3c0-2 1-3 3-3h17l3 2zm22 6v14H31a7 7 0 0 0-6-5h-3V8l1-2 2-1h17c2 0 3 1 3 3zm5 20v17c0 2-1 3-3 3H30l-3-2c3-1 5-3 5-6V26h17l1 2zm-22-5v17l-1 2-2 1H8c-2 0-3-1-3-3V23c0-2 1-3 3-3h17l1 1 2 1v1z"/></svg>
}

const Accordions = (props) => {
    const {children, title, icon} = props
    const [isShow, setShow] = useState(false);
    return (
        <div className="ultp-section-accordion">
            <div className="ultp-section-accordion-item ultp-global-accordion-item" onClick={() => setShow(!isShow)}>
                <div className="ultp-global-accordion-title">
                    {icon}
                    {title}
                    {isShow ? <span className="ultp-section-arrow dashicons dashicons-arrow-down-alt2"></span> : <span className= "ultp-section-arrow dashicons dashicons-arrow-up-alt2"></span>}
                </div>
            </div>
            <div className={`ultp-section-show ` + (isShow ? '' : 'is-hide')}>
                {children}
            </div>
        </div>
    )
}

const colorStacks = [
    ['#001524', '#445D48', '#D6CC99', '#FDE5D4', '#C70039', '#141E46', '#78D6C6', '#12486B', '#FFFFFF'],
    ['#186F65', '#B5CB99', '#FCE09B', '#B2533E', '#C70039', '#141E46', '#78D6C6', '#12486B', '#FFFFFF'],
    ['#132043', '#1F4172', '#F1B4BB', '#FDF0F0', '#C70039', '#141E46', '#78D6C6', '#12486B', '#FFFFFF'],
    ['#219C90', '#E9B824', '#EE9322', '#D83F31', '#C70039', '#141E46', '#78D6C6', '#12486B', '#FFFFFF'],
    ['#3D0C11', '#D80032', '#F78CA2', '#F9DEC9', '#C70039', '#141E46', '#78D6C6', '#12486B', '#FFFFFF']
];

const presetKeys = [
    'presetColor1',
    'presetColor2',
    'presetColor3',
    'presetColor4',
    'presetColor5',
    'presetColor6',
    'presetColor7',
    'presetColor8',
    'presetColor9'
];

const PostxSettings = (props) => {

    const [stateData, setState] = useState({
        presetColor1: '#00ADB5',
        presetColor2: '#F08A5D',
        presetColor3: '#B83B5E',
        presetColor4: '#B83B5E',
        presetColor5: '#71C9CE',
        presetColor6: '#F38181',
        presetColor7: '#FF2E63',
        presetColor8: '#EEEEEE',
        presetColor9: '#F9ED69',
        postxColorOnly: false
    });
    const [ isOpen, setOpen ] = useState(false);
    // const [ nowPop, setPop ] = useState('');
    // const [ nowTitle, setTitle ] = useState('');

    const {postxColorOnly, editorType, editorWidth, breakpointSm, breakpointXs} = stateData

    const _getColor = (color) => {
        return 'rgba('+color.rgb.r+','+color.rgb.g+','+color.rgb.b+','+color.rgb.a+')'
    }


    const setEditor = (data = {}) => {
        let styleCss = '';
        if (data.editorType == 'fullscreen' || data.editorType == 'custom') {
            styleCss = 'body.block-editor-page #editor .wp-block-post-content > .block-editor-block-list__block.wp-block:not(:is(.alignfull, .alignwide), :has( .block-editor-inner-blocks  .block-editor-block-list__block.wp-block)){ max-width: '+(data.editorType == 'fullscreen' ? '100%' : (data.editorWidth || 1200)+'px')+';}';
        }
        if (window.document.getElementById('ultp-block-global') === null) {
            let cssInline = document.createElement('style');
            cssInline.type = 'text/css';
            cssInline.id = 'ultp-block-global';
            if (cssInline.styleSheet) {
                cssInline.styleSheet.cssText = styleCss;
            } else {
                cssInline.innerHTML = styleCss;
            }
            window.document.getElementsByTagName("head")[0].appendChild(cssInline);
        } else {
            window.document.getElementById('ultp-block-global').innerHTML = styleCss;
        }
    }

    
    const setGlobalColor = (data = {}) => {
        let styleCss = '';
        styleCss += ':root { --preset-color1: '+(data.presetColor1||'#037fff')+';';
        styleCss += '--preset-color2: '+(data.presetColor2||'#026fe0')+';';
        styleCss += '--preset-color3: '+(data.presetColor3||'#071323')+';';
        styleCss += '--preset-color4: '+(data.presetColor4||'#132133')+';';
        styleCss += '--preset-color5: '+(data.presetColor5||'#34495e')+';';
        styleCss += '--preset-color6: '+(data.presetColor6||'#787676')+';';
        styleCss += '--preset-color7: '+(data.presetColor7||'#f0f2f3')+';';
        styleCss += '--preset-color8: '+(data.presetColor8||'#f8f9fa')+';';
        styleCss += '--preset-color9: '+(data.presetColor9||'#ffffff')+';}';

        window.document.getElementById('wpxpo-global-style-inline-css').innerHTML = styleCss;
    }

    useEffect(() => {
        const fetchData = async () => {
            apiFetch({
                path: '/ultp/v1/action_option',
                method: 'POST',
                data: { type: 'get'}
            }).then( res => {
                if (res.success) {
                    setState(Object.assign({},stateData,res.data));
                    localStorage.setItem('ultpGlobal' + ultp_data.blog, JSON.stringify(res.data));
                    setEditor(res.data);
                }
            })
        };
        fetchData();
    }, []);


    const setValue = (type, value) => {
        if (type.indexOf('presetColor') > -1 && typeof value === 'object') { // For Only Color
            value = _getColor(value)
        }
        const allData = Object.assign({}, stateData, (type == 'palette' ? value : {[type]:value}) )
        setState(allData)
        localStorage.setItem('ultpGlobal' + ultp_data.blog, JSON.stringify(allData))
        apiFetch({
            method: 'POST',
            path: '/ultp/v1/action_option',
            data: { type: 'set', data: allData }
        });
        if (type == 'editorType' || type == 'editorWidth') {
            setEditor(allData);
        } else {
            setGlobalColor(allData);
        }
    }


    // Check for Widget
    if (!editPost) {
        return '';
    }
    
    return (
        <Fragment>
            <editPost.PluginSidebar
                icon={icons.logo}
                name="postx-settings"
                title={__('PostX Settings','ultimate-post')}>

                    <Accordions title={__('Colors','ultimate-post')} icon={icons.editorColor}>
                        
                        <div className={`ultp-global-color-label`}>Color Palette</div>
                        <div className={`ultp-global-color-preset`}>
                            { presetKeys.map( (val, k) => {
                                return (
                                    <Dropdown
                                        key={k}
                                        renderToggle={({ onToggle }) => (
                                            <span key={k} className={`ultp-global-color`} onClick={() => onToggle()} style={{backgroundColor: stateData[val]}}></span>
                                        )}
                                        renderContent={() => <ColorPicker onChangeComplete={(v) => {
                                            let _v = [...stateData['palette']]
                                            _v[k] = v.hex
                                            setValue('palette', _v)
                                            setState( Object.assign({}, stateData,{['palette']: _v}) );
                                        }}/>}
                                    />
                                )
                            })}
                            <span onClick={() => {setOpen(!isOpen)}}>Open/Close</span>
                            { isOpen &&
                                <ul>
                                    { colorStacks.map((val, key) => {
                                        return <li key={key} onClick={() => {
                                                const obj = {};
                                                presetKeys.forEach((el, i) => {
                                                    obj[el] = val[i];
                                                });
                                                setValue('palette', obj)
                                            }}>
                                            {val.map((v, k) => {
                                                return <span key={k} className={`ultp-global-color`} style={{backgroundColor:v}}></span>
                                            })}
                                        </li>
                                    }) }
                                </ul>
                            }
                        </div>
                        
                        <div className={`ultp-global-color-label`}>Theme Color</div>
                        { (wp.data.select( "core/editor" ).getEditorSettings().colors || []).map( (val, k) => {
                            return <span key={k} className={`ultp-global-color`} style={{backgroundColor:val.color}}></span>
                        })}

                        <Toggle 
                            label={__('Use only PostX Blocks Colors?','ultimate-post')}
                            value={postxColorOnly}
                            onChange={v => setValue('postxColorOnly',v)} />
                    </Accordions>

                    {/* <Accordions title={__('Block Default','ultimate-post')} icon={icons.editorBlock}>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-grid-1.svg'}/> Post Grid 1 <span className={'ultp-icon-style'+(nowPop=='pg1'?' active':'')} onClick={() => {setPop('pg1'); setTitle('Post Grid 1')}}>{icons.setting}</span>
                        </div>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-grid-2.svg'}/> Post Grid 2 <span className={'ultp-icon-style'+(nowPop=='pg2'?' active':'')} onClick={() => {setPop('pg2'); setTitle('Post Grid 2')}}>{icons.setting}</span>
                        </div>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-list-1.svg'}/> Post List 1 <span className={'ultp-icon-style'+(nowPop=='pl1'?' active':'')} onClick={() => {setPop('pl1'); setTitle('Post List 1')}}>{icons.setting}</span>
                        </div>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-list-2.svg'}/> Post List 2 <span className={'ultp-icon-style'+(nowPop=='pl2'?' active':'')} onClick={() => {setPop('pl2'); setTitle('Post List 2')}}>{icons.setting}</span>
                        </div>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-list-3.svg'}/> Post List 3 <span className={'ultp-icon-style'+(nowPop=='pl3'?' active':'')} onClick={() => {setPop('pl3'); setTitle('Post List 3')}}>{icons.setting}</span>
                        </div>
                        <div className={`postx-default-settings`}>
                            <img src={ultp_data.url + 'assets/img/blocks/post-list-4.svg'}/> Post List 4 <span className={'ultp-icon-style'+(nowPop=='pl4'?' active':'')} onClick={() => {setPop('pl4'); setTitle('Post List 4')}}>{icons.setting}</span>
                        </div>
                        { nowPop != '' &&
                            <Modal style={{width:'300px'}} title={nowTitle} onRequestClose={()=>setPop('')}>
                                {FieldTypeData()}
                            </Modal>
                        }
                    </Accordions> */}

                    <Accordions title={__('Editor Width','ultimate-post')} icon={icons.editorWidth}>
                        <RadioControl
                            selected={editorType||'default'}
                            options={[
                                { label: 'Theme Default', value: 'default' },
                                { label: 'Full Screen', value: 'fullscreen' },
                                { label: 'Custom Size', value: 'custom' },
                            ]}
                            onChange={ v => setValue('editorType',v) }
                        />
                        { editorType == 'custom' &&
                        <Range
                            value={editorWidth||1200}
                            min={500}
                            max={3000}
                            placeholder={'1200'}
                            onChange={ v => setValue('editorWidth',v) }/>
                        }
                    </Accordions>

                    <Accordions title={__('Breakpoints','ultimate-post')} icon={icons.editorBreak}>
                        <Range
                            value={breakpointSm||991}
                            min={600}
                            max={1200}
                            step={1}
                            label={__('Tablet (Max Width)','ultimate-post')}
                            onChange={ v => setValue('breakpointSm',v) }/>
                        <Range
                            value={breakpointXs||767}
                            min={300}
                            max={1000}
                            step={1}
                            label={__('Mobile (Max Width)','ultimate-post')}
                            onChange={ v => setValue('breakpointXs',v) }/>
                    </Accordions>
            </editPost.PluginSidebar>

            {/* <editPost.PluginSidebarMoreMenuItem
                icon={icons.logo}
                target="global-settings-sidebar"
            >
                {__('PostX Settings','ultimate-post')}
            </editPost.PluginSidebarMoreMenuItem> */}
        </Fragment>
    );

}
export default PostxSettings;