const { __ } = wp.i18n
const { Fragment, useState } = wp.element

const getIcon = (slug) => {
    switch(slug) {
        case "setting":
            return "dashicons dashicons-admin-tools";
        case "advanced":
            return "dashicons dashicons-admin-generic";
        case "style":
        default:
            return "dashicons dashicons-admin-appearance";
    }
}

const Sections = (props) => {
    const {children, callback, classes} = props;
    const [current, setCurrent] = useState(children[0].props.slug);
    return(
        <div className={`ultp-section-tab${classes ||''}`}>
            <div className={`ultp-section-wrap`}>
                <div className="ultp-section-wrap-inner">
                    { 
                        children.map((tab, k) => {
                            return tab ? <div 
                                key={k}
                                className={"ultp-section-title" + (tab.props.slug === current ? ' active' : '')}
                                onClick={() => {
                                        setCurrent(tab.props.slug);
                                        callback && callback({ slug: tab.props.slug })
                                    }
                                }
                            >
                                {
                                    tab.props.hasOwnProperty("icon") && tab.props.icon ? <div className="ultp-section-tab-custom-icon">
                                        {tab.props.icon}
                                    </div>
                                    :
                                    <span className={getIcon(tab.props.slug)}></span>
                                }
                                <span>{tab.props.title}</span>
                            </div>
                            :
                            <Fragment key={k}></Fragment>
                        })
                    }
                </div>
            </div>   
            { children.map((tab,k) => 
                <div key={k} className={`ultp-section-content`+ (tab?.props?.slug === current ? ' active' : '')}>
                    {tab}
                </div>
            )}
        </div>
    )
}

const SectionsNoTab = ({children}) => {
    return(
        <div className={`ultp-section-tab`}>
            {children}
        </div>
    )
}

const Section = (props) => {
    const {children} = props
    return <Fragment> {Array.isArray(children) ? children.map(item => item) : children} </Fragment>
}

export {Sections, Section, SectionsNoTab}