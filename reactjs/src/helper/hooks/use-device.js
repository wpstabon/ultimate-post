/** @format */

const { useSelect, useDispatch } = wp.data;
const {useEffect} = wp.element;

const deviceMap = {
    Desktop: "lg",
    Tablet: "sm",
    Mobile: "xs",
    lg: "Desktop",
    sm: "Tablet",
    xs: "Mobile",
};

const store = wp.data.select("core/edit-site")
    ? "core/edit-site"
    : "core/edit-post";

export default function useDevice() {
    if ( document.querySelector('.widgets-php') || document.querySelector('body.wp-customizer') ) {
        return [
            localStorage.getItem('ultpDevice') || 'lg',
            (value)=> {
                if ( localStorage.getItem('ultpDevice') != value ) {
                    localStorage.setItem('ultpDevice', value);
                    const prevSelBlock = wp.data?.select("core/block-editor")?.getSelectedBlock();
                    wp.data?.dispatch("core/block-editor")?.clearSelectedBlock();
                    setTimeout(() => {
                        if ( prevSelBlock?.clientId ) {
                            wp.data.dispatch("core/block-editor")?.selectBlock(prevSelBlock.clientId);
                        }
                    }, 100);
                }
            }
        ]
    }
    let setDeviceFunc;

    const { clearSelectedBlock, selectBlock } =
        useDispatch("core/block-editor");

    const getSelectedBlock = useSelect(
        (select) => select("core/block-editor").getSelectedBlock,
        []
    );

    const { setPreviewDevice } = useDispatch("core/editor");
    setDeviceFunc = setPreviewDevice;

    // Backward comp
    const { __experimentalSetPreviewDeviceType } = useDispatch(store);

    if (!setDeviceFunc) {
        setDeviceFunc = __experimentalSetPreviewDeviceType;
    }

    const device = useSelect((select) => {
        let getDeviceFunc;

        const { getDeviceType } = select("core/editor");
        getDeviceFunc = getDeviceType;

        // Backward comp
        if (!getDeviceFunc) {
            const { __experimentalGetPreviewDeviceType } = select(store);
            getDeviceFunc = __experimentalGetPreviewDeviceType;
        }

        return deviceMap[getDeviceFunc()];
    }, []);

    const setDevice = (newDevice) => {
        // In block editor, when switching from desktop to tablet/mobile device or the vice-versa
        // it re-renders all the blocks. This is bad when editing a block as we lose track of where we
        // are in the sidebar settings. The following code fixes this.
        const prevSelBlock = getSelectedBlock();
        if (
            (["Desktop", "lg"].includes(newDevice) ||
                ["Desktop", "lg"].includes(device)) &&
            device !== newDevice &&
            prevSelBlock?.clientId &&
            !wp.data.select("core/edit-site")
        ) {
            // Necessary to trigger restore state for supported blocks
            localStorage.setItem("ultp_settings_save_state", "true");

            // Prevent state reset to improve UX
            localStorage.setItem("ultp_settings_reset_disabled", "true");

            clearSelectedBlock();
            setDeviceFunc(deviceMap[newDevice]);

            // Compatability for blocks that cannot use hooks
            localStorage.setItem('ultpDevice', newDevice.length < 3 ? device : deviceMap[newDevice]);

            // Preventing the race condition
            setTimeout(() => {
                selectBlock(prevSelBlock.clientId);
                setTimeout(() => {
                    localStorage.removeItem("ultp_settings_reset_disabled");
                }, 500);
            }, 500);

        } else {
            setDeviceFunc(deviceMap[newDevice]);
        }

    };

    return [device, setDevice];
}
