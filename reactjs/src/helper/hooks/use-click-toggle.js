const { useEffect, useState, useRef } = wp.element;

export default function useClickToggle() {
	const elementRef = useRef();
	const [isSelected, setIsSelected] = useState(false);

	useEffect(() => {
		const isDescendant = (parent, child) => {
			let node = child.parentNode;
			while (node !== null) {
				if (node === parent) {
					return true;
				}
				node = node.parentNode;
			}
			return false;
		};

		const handleClickOutside = (event) => {
			if (
				elementRef.current &&
				!elementRef.current.contains(event.target) &&
				!isDescendant(elementRef.current, event.target)
			) {
				setIsSelected(false);
			}
		};

		document.addEventListener('click', handleClickOutside, true);

		return () => {
			document.removeEventListener('click', handleClickOutside, true);
		};
	}, []);

	return {
		elementRef,
		isSelected,
        setIsSelected,
	};
}
