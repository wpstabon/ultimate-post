import { updateCurrentPostId } from "../CommonPanel";

const { useEffect } = wp.element;
const { getBlockAttributes, getBlockRootClientId } =
	wp.data.select('core/block-editor');

export const useBlockId = (data={}) => {
    const {
        blockId, 
        clientId,
        currentPostId,
        setAttributes,
        changePostId,
        checkRef
    } = data;
    
    useEffect(() => {
        const _client = clientId.substr(0, 6);
        const reference = checkRef !=false ? getBlockAttributes( getBlockRootClientId(clientId) ) : '';

        if ( !blockId ) {
            setAttributes({ blockId: _client });
        } else if (blockId && blockId != _client) {
            if ( !reference?.hasOwnProperty('ref') ) {
                setAttributes({ blockId: _client });
            }
        }
        if ( changePostId !=false ) {
            updateCurrentPostId(setAttributes, reference, currentPostId, clientId );
        }
    }, [clientId]);
}