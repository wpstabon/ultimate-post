const { __ } = wp.i18n
const { useState, useEffect } = wp.element
import Skeleton from "../dashboard/utility/Skeleton"
import IconPack from "./fields/tools/IconPack"
import icons from "./icons"
import UltpLinkGenerator from "./UltpLinkGenerator"

const DesignTemplate = (props) => {
    const {clientId, attributes, label, name} = props.store
    const [state, setState] = useState({ designList: [], error: false, reload: false, reloadId: '', templatekitCol: 'ultp-templatekit-col3', fetch: false, loading: false });
    const {designList, error, reload, reloadId, templatekitCol, fetch, loading} = state;
    const [loader , setLoader] = useState(false);
    const [ wishListArr , setWishlistArr] = useState([]);
    const [ showWishList , setShowWishList] = useState(false);
    
    const fetchData = async () => {
        setState({...state, loading: true});
        const blockName = name.split('/');
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'design'
            }
        })
        .then((response) => {
            if(response.success){
                const data = JSON.parse(response.data);
                if(blockName[1] && typeof data[blockName[1]] == 'undefined') {
                    setLoader(true);
                    _fetchFile();
                }
                else {
                    setState({...state, loading: false, designList: data[blockName[1]]});
                    setLoader(false);
                }
            }
        })
    };

    useEffect(() => {
        setWListAction('', '', 'fetchData');
        fetchData();
    }, []);

    const _changeVal = (designID, isPro) => {
        setState({...state, reload: true, reloadId: designID});
        if (isPro) {
            if(ultp_data.active){
                _changeDesign(designID);
            }
        } else {
            _changeDesign(designID);
        }
    }

    const _fetchFile = () => {
        setState({...state, fetch: true});
        wp.apiFetch({
            path: '/ultp/v2/fetch_premade_data',
            method: 'POST',
            data: {
                type: 'fetch_all_data'
            }
        })
        .then((response) => {
            if (response.success) {
                fetchData();
                setState({...state, fetch: false});
            }
        })
    }

    const _changeDesign = (designID) => {
        const designEndpoint = 'https://ultp.wpxpo.com/wp-json/restapi/v2/';
        const { replaceBlock } = wp.data.dispatch('core/block-editor')
        const removeItem = ['queryNumber', 'queryNumPosts', 'queryType', 'queryTax', 'queryRelation', 'queryOrderBy', 'queryOrder', 'queryInclude', 'queryExclude', 'queryAuthor', 'queryOffset', 'metaKey', 'queryExcludeTerm', 'queryExcludeAuthor', 'querySticky', 'queryUnique', 'queryPosts', 'queryCustomPosts'];
        
        window.fetch(designEndpoint+'single-design', {
            method: 'POST',
            body: new URLSearchParams('license='+ultp_data.license+'&design_id='+designID)
        })
        .then(response => response.text())
        .then((jsonData) => {
            jsonData = JSON.parse(jsonData)
            if (jsonData.success && jsonData.rawData) {
                let parseData = wp.blocks.parse(jsonData.rawData)
                let attr = parseData[0].attributes
                for ( let i=0; i < removeItem.length; i++ ) {
                    if(attr[removeItem[i]]){ delete attr[removeItem[i]]; }
                }
                attr = Object.assign({}, attributes,  attr);
                parseData[0].attributes = attr;
                setState({...state, error: false, reload: false, reloadId: '' });
                replaceBlock(clientId, parseData );
            }else{
                setState({...state, error: true, reload: false, reloadId: '' });
            }
        })
        .catch((error) => {
            console.error(error);
        })  
    }
    const setWListAction = (id, action='', type='') => {
        wp.apiFetch({
            path: '/ultp/v2/premade_wishlist_save',
            method: 'POST',
            data: {
                id: id,
                action: action,
                type: type
            }
        })
        .then((res) => {
            if(res.success) {
                setWishlistArr( Array.isArray(res.wishListArr) ? res.wishListArr : Object.values(res.wishListArr || {}));
            }
        })
    }

    const modal_title =  <span className="ultp-templatekit-design-template-modal-title">
        <img src={ultp_data.url+`assets/img/blocks/${props.store.name.split('/')[1]}.svg`}/>
        <span>{props.store.name.split('/')[1].replace('-',' ').replace('-', ' #')}</span>
    </span>;

    let patternsGetProUrl = UltpLinkGenerator('', 'blockPatternPro', ultp_data.affiliate_id);
    return(
        <div className="ultp-templatekit-design-template-container ultp-templatekit-list-container ultp-predefined-patterns">
            { label && 
                <label>{label}</label>
            }
            <div className="ultp-popup-header ">
                <div className="ultp-popup-filter-title">
                    <div className="ultp-popup-filter-image-head">
                        {modal_title}
                    </div>
                    <div className="ultp-popup-filter-sync-close">
                        <span className={ `ultp-templatekit-iconcol2 ${templatekitCol == 'ultp-templatekit-col2' ? 'ultp-lay-active' : ''}`} onClick ={ ()=> setState({...state, templatekitCol: 'ultp-templatekit-col2'}) }>{icons.grid_col1}</span>
                        <span className={ `ultp-templatekit-iconcol3 ${templatekitCol == 'ultp-templatekit-col3' ? 'ultp-lay-active' : ''}`} onClick ={ ()=> setState({...state, templatekitCol: 'ultp-templatekit-col3'}) }>{icons.grid_col2}</span>
                        <div className="ultp-premade-wishlist-con"><span className={`ultp-premade-wishlist cursor ${showWishList ? 'ultp-wishlist-active' : ''}`} onClick={()=> { setShowWishList( showWishList ? false : true)}}>{IconPack[showWishList ? 'love_solid' : 'love_line']}</span></div>
                        <div onClick={() => _fetchFile()} className="ultp-filter-sync">
                            <span className={'dashicons dashicons-update-alt' + ( fetch ? ' rotate' : '')}></span>{__('Synchronize','ultimate-post')}
                        </div>
                        <button className="ultp-btn-close" onClick={() => props.closeModal()} id="ultp-btn-close"><span className="dashicons dashicons-no-alt"></span></button>
                    </div>
                </div>
            </div>
            
            { designList?.length ?
                <>
                    <div className={`ultp-premade-grid ultp-templatekit-content-designs ${templatekitCol}`}>
                        { designList.map( (data, k) => {
                                return (
                                    (!showWishList || (showWishList && wishListArr?.includes(data.ID))) && <div key={k} className={`ultp-card ultp-item-list`}>
                                        <div className="ultp-item-list-overlay">
                                            <a className="ultp-templatekit-img" href={data.liveurl}    target="_blank">
                                                <img src={data.image} loading="lazy" alt={data.title} />
                                            </a>
                                            <div className="ultp-list-dark-overlay">
                                                { (data.pro) ?
                                                    !ultp_data.active &&
                                                    <span className="ultp-templatekit-premium-btn">
                                                        {__('Pro','ultimate-post')}
                                                    </span>
                                                    :
                                                    <span 
                                                    className="ultp-templatekit-premium-btn ultp-templatekit-premium-free-btn">
                                                        {__('Free','ultimate-post')}
                                                    </span>
                                                }
                                                <a className="ultp-overlay-view" 
                                                    href={`https://www.wpxpo.com/postx/patterns/#demoid`+data.ID} target="_blank">
                                                    <span className="dashicons dashicons-visibility"></span> {__('Live Preview','ultimate-post')}     
                                                </a>
                                            </div>
                                        </div>

                                        <div className="ultp-item-list-info ultp-p10">
                                            <span className="ultp-templatekit-title">{data.name}</span>
                                            <span className="ultp-action-btn">
                                                <span className="ultp-premade-wishlist" onClick={()=> { setWListAction(data.ID, wishListArr?.includes(data.ID) ? 'remove' : '')}}>{IconPack[wishListArr?.includes(data.ID) ? 'love_solid' : 'love_line']}</span>
                                                {(data.pro && !ultp_data.active) ?
                                                    <a className="ultp-btns ultpProBtn" target="_blank" href={patternsGetProUrl}>{__('Upgrade to Pro','ultimate-post')}&nbsp; &#10148;</a>
                                                    :( (data.pro && error) ?
                                                        <a className="ultp-btn ultp-btn-sm ultp-btn-success" target="_blank" href={patternsGetProUrl}>{__('Get License','ultimate-post')}</a>
                                                        :
                                                        <span onClick={() => _changeVal(data.ID, data.pro)} className="ultp-btns ultp-btn-import">{IconPack['arrow_down_line']}{__('Import','ultimate-post')}{(reload && reloadId == data.ID) ? <span className="dashicons dashicons-update rotate" /> : '' }</span>
                                                    )
                                                }
                                            </span>
                                        </div>
                                    </div>    
                                )
                            }) 
                        }
                    </div>
                </> : 
                ( loader || loading ) ? <div className="ultp-premade-grid ultp-templatekit-col3 skeletonOverflow">
                    { Array(6).fill(1).map((v, i) => {
                        return(
                            <div key={i} className="ultp-item-list">
                                <div className="ultp-item-list-overlay">
                                    <Skeleton type="custom_size" c_s={{ size1: 100, unit1: '%',  size2: 300, unit2: 'px' }}/>
                                </div>
                                <div className="ultp-item-list-info">
                                    <Skeleton type="custom_size" c_s={{ size1: 50, unit1: '%',  size2: 25, unit2: 'px', br: 2 }}/>
                                    <span className="ultp-action-btn">
                                        <span className='ultp-premade-wishlist'><Skeleton type="custom_size" c_s={{ size1: 30, unit1: 'px',  size2: 25, unit2: 'px', br: 2 }}/></span>
                                        <Skeleton type="custom_size" c_s={{ size1: 70, unit1: 'px',  size2: 25, unit2: 'px', br: 2 }}/>
                                    </span>
                                </div>
                            </div>
                        )
                    })}                
                </div>
                : <span className="ultp-image-rotate">{__('No Data Found...','ultimate-post')}</span>
            }
        </div>
    )

}
export default DesignTemplate;