import { handleTypoFieldPresetData } from "./CommonFunctions";
import { getCustomFonts } from "./CommonPanel";

const enableGoogleFont = ((ultp_data.settings?.hasOwnProperty('disable_google_font') && ultp_data.settings['disable_google_font'] != 'yes') || !ultp_data.settings?.hasOwnProperty('disable_google_font')) ? true : false;
const defaultFont = ['Arial', 'Tahoma', 'Verdana', 'Helvetica', 'Times New Roman', 'Trebuchet MS', 'Georgia']
// CSS Box Shadow
export const cssBoxShadow = (v) => {
    return '{ box-shadow:' + (v.inset || '') + ' ' + v.width.top + 'px ' + v.width.right + 'px ' + v.width.bottom + 'px ' + v.width.left + 'px ' + v.color + '; }'
}

// CSS Border
export const cssBorder = (v) => {
    v = Object.assign({}, { type: 'solid', width: {}, color: '#e5e5e5' }, v);
    const unit = v.width.unit ? v.width.unit : 'px'
    return `{ border-color:  ${v.color ? v.color : '#555d66'}; border-style: ${v.type ? v.type : 'solid'}; border-width: ${cssDimension(v.width)}; }`
}

// CSS Typography
const _device = (val, selector) => {
    let data = {}
    if (val && val.lg) { data.lg = selector.replace(new RegExp('{{key}}', "g"), val.lg + ( handleTypoFieldPresetData(val.lg) ? (val.ulg || val.unit || 'px') : '')) }
    if (val && val.sm) { data.sm = selector.replace(new RegExp('{{key}}', "g"), val.sm + ( handleTypoFieldPresetData(val.sm) ? (val.usm || val.unit || 'px') : '')) }
    if (val && val.xs) { data.xs = selector.replace(new RegExp('{{key}}', "g"), val.xs + ( handleTypoFieldPresetData(val.xs) ? (val.uxs || val.unit || 'px') : '')) }
    return data;
}
const _push = (val, data) => {
    if (val.lg) { data.lg.push(val.lg) }
    if (val.sm) { data.sm.push(val.sm) }
    if (val.xs) { data.xs.push(val.xs) }
    return data;
}
export const cssTypography = (v) => {
    let font = ''
    if (v.family && v.family != 'none') {
        if (!defaultFont.includes(v.family) && !getCustomFonts().some((item) => item.n == v.family)) {
            if (enableGoogleFont && handleTypoFieldPresetData(v.family)) {
                font = "@import url('https://fonts.googleapis.com/css?family=" + v.family.replace(' ', '+') + ':' + (v.weight || 400) + "');"
            }
        }
    }
    const isAdded = (v.family && ( !handleTypoFieldPresetData(v.family) || defaultFont.includes(v.family) || getCustomFonts().some((item) => item.n == v.family) )) ? true : enableGoogleFont
    let data = { lg: [], sm: [], xs: [] }
    if (v.size) { data = _push(_device(v.size, 'font-size:{{key}}'), data) }
    if (v.height) { data = _push(_device(v.height, 'line-height:{{key}} !important'), data) }
    if (v.spacing) { data = _push(_device(v.spacing, 'letter-spacing:{{key}}'), data) }
    let simple = '{' + ( (v.family && isAdded && v.family != 'none') ? "font-family:" + v.family + "," + (v.type||"sans-serif") + ";" : '') +
        (v.weight ? 'font-weight:' + v.weight + ';' : '') +
        (v.color ? 'color:' + v.color + ';' : '') +
        (v.style ? 'font-style:' + v.style + ';' : '') +
        (v.transform ? 'text-transform:' + v.transform + ';' : '') +
        (v.decoration ? 'text-decoration:' + v.decoration + ';' : '') + '}';
    return { lg: data.lg, sm: data.sm, xs: data.xs, simple, font };
}

// CSS Dimension
export const cssDimension = (v) => {
    const unit = v.unit ? v.unit : 'px';
    if (
        (v.top == "" || v.top == undefined) &&
        (v.right == "" || v.right == undefined) &&
        (v.bottom == "" || v.bottom == undefined) &&
        (v.left == "" || v.left == undefined)
    ) {
        return '';
    } else {
        return (v.top || 0) + unit + ' ' + (v.right || 0) + unit + ' ' + (v.bottom || 0) + unit + ' ' + (v.left || 0) + unit
    }
}

// CSS ColorAdvanced
export const cssColor = (v) => {
    let data = (v.clip ? '-webkit-background-clip: text; -webkit-text-fill-color: transparent;' : '');
    if (v.type == 'color') {
        data += (v.color ? 'background-color: ' + v.color + ';' : '')
    } else if (v.type == 'gradient' && v.gradient) {
        if (typeof v.gradient == 'object') {
            // for Compatiblity
            if ( v.gradient.type == 'linear') {
                data += 'background-image : linear-gradient(' + v.gradient.direction + 'deg, ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
            } else {
                data += 'background-image : radial-gradient( circle at ' + v.gradient.radial + ' , ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
            }
        } else {
            data += 'background-image:'+v.gradient+';';
        }
    } else if (v.type == 'image') { 
        if(v.fallbackColor || v.color){
            data += 'background-color:'+(v.fallbackColor ?? v.color)+';';
        }
        if (v.image) {
            data += 'background-image: url("' + v.image + '");' +
                (v.position ? 'background-position-x:' + (v.position.x * 100)+ '%;background-position-y:' + (v.position.y * 100) + '%;' : '') +
                (v.attachment ? 'background-attachment:' + v.attachment + ';' : '') +
                (v.repeat ? 'background-repeat:' + v.repeat + ';' : '') +
                (v.size ? 'background-size:' + v.size + ';' : '');
        }
    } else if(v.type == 'video' && v.fallback) {
        data += 'background-image: url("' + v.fallback + '"); background-size: cover; background-position: 50% 50%';
    }
    if (v.replace) {
        return data;
    } else {
        return '{' + data + '}';
    }
}

// Css Filter Field
export const cssFilter = (v) => {
    let data = '';
    if (v.hue) { data = ' hue-rotate(' + v.hue + 'deg)' }
    if (v.saturation) { data += ' saturate(' + v.saturation + '%)' }
    if (v.brightness) { data += ' brightness(' + v.brightness + '%)' }
    if (v.contrast) { data += ' contrast(' + v.contrast + '%)' }
    if (v.invert) { data += ' invert(' + v.invert + '%)' }
    if (v.blur) { data += ' blur(' + v.blur + 'px)' }
    return 'filter:'+data+';';
}