const UltpLinkGenerator = (url, tagArgs, affiliate) => {
    const urlList = {
        // Wizard Page
        explore_pro_feature: [
            { key:'source', value: 'db-postx-wizard'}, 
            { key: 'medium', value: 'explore-features'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
        ticket_support: [
            { key:'source', value: 'db-postx-wizard'}, 
            { key: 'medium', value: 'ticket-support'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
        postx_doc: [
            { key:'source', value: 'db-postx-wizard'}, 
            { key: 'medium', value: 'postx-doc'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],

        addons_popup: [
            { key:'source', value: 'db-postx-addons'}, 
            { key: 'medium', value: 'popup'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
        builder_popup: [
            { key:'source', value: 'db-postx-builder'}, 
            { key: 'medium', value: 'popup-upgrade-pro'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
         
        //Postx Blocks Documentation
        block_docs: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'block-docs'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        //Postx Menu
        blockProFeat: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'pro-features'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        blockUpgrade: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'block-pro'}, 
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        blockPatternPro: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'blocks-premade'}, 
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        blockProLay: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'pro-layout'}, 
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        
        // Installation Wizard
        wizardPatternPro: [
            { key:'source', value: 'db-postx-wizard'}, 
            { key: 'medium', value: 'core_features-patterns'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
        wizardStaterPackPro:[
            { key:'source', value: 'db-postx-wizard'}, 
            { key: 'medium', value: 'core_features-SP'},
            { key: 'campaign', value: 'postx-dashboard'}	
        ],
        // Block
        slider_2: [
            { key:'source', value: 'db-postx-editor'},
            { key: 'medium', value: 'slider2-pro'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        advanced_search: [
            { key:'source', value: 'db-postx-editor'},
            { key: 'medium', value: 'adv_search-pro'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        // end Block
        customFont: [
            { key:'source', value: 'db-postx-editor'},
            { key: 'medium', value: 'custom-font'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],

        dc: [
            { key:'source', value: 'db-postx-editor'}, 
            { key: 'medium', value: 'acf-pro'}, 
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        // Dasboard
       
        postx_dashboard_settings: [
            { key: 'source', value: 'db-postx-setting' }, 
            { key: 'medium', value: 'upgrade-pro-sidebar' }, 
            { key: 'campaign', value: 'postx-dashboard' }
        ],
        postx_dashboard_tutorials: [
            { key: 'source', value: 'db-postx-tutorial' }, 
            { key: 'medium', value: 'tutorials-upgrade_to_pro' }, 
            { key: 'campaign', value: 'postx-dashboard' }
        ],
        postx_dashboard_tutorialsdocs: [
            { key: 'source', value: 'db-postx-tutorial' }, 
            { key: 'medium', value: 'tutorials-doc' }, 
            { key: 'campaign', value: 'postx-dashboard' }
        ],
        menu_save_temp_pro: [
            { key : 'source', value: 'db-postx-save-template' },
            { key : 'medium', value: 'popup-upgrade' },
            { key : 'campaign', value: 'postx-dashboard' },
        ],
        settingsFR: [
            { key:'source', value: 'db-postx-setting'},
            { key: 'medium', value: 'settings-upgrade-pro'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
        tutorialsFR: [
            { key:'source', value: 'db-postx-tutorial'},
            { key: 'medium', value: 'tutorials-FR'},
            { key: 'campaign', value: 'postx-dashboard'}
        ],
    }
    url = url ? url+'?' : 'https://www.wpxpo.com/postx/pricing/?';
    { 
        urlList[tagArgs]?.map((tag, i) => {
            return url = url + `utm_${tag.key}` + '=' +tag.value + '&';
        })
        if(affiliate){
            url = url + 'ref' + '=' + affiliate + '&';
        }
    }
    return url.slice(0, -1);
};

export default UltpLinkGenerator;