const readMoreAttribute = {
    // Post Grid 2
    readMore: { type: "boolean", default: false },
    readMoreText: { type: "string", default: "" },
    readMoreIcon: { type: "string", default: "rightArrowLg" },
    readMoreTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: 12, unit: "px" },
        height: { lg: "", unit: "px" },
        spacing: { lg: 1, unit: "px" },
        transform: "",
        weight: "400",
        decoration: "none",
        family: "",
        },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a",
        },
        ],
    },
    readMoreIconSize: {
        type: "object",
        default: { lg: "", unit: "px" },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-readmore svg { width:{{readMoreIconSize}}; }",
        },
        ],
    },
    readMoreColor: {
        type: "string",
        default:  "var(--postx_preset_Base_3_color)",
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-block-items-wrap .ultp-block-readmore a { color:{{readMoreColor}}; } 
            {{ULTP}} .ultp-block-readmore a svg { fill:{{readMoreColor}}; }`,
        },
        ],
    },
    readMoreBgColor: {
        type: "object",
        default: { openColor: 0, type: "color", color: "#000" },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-block-readmore a",
        },
        ],
    },
    readMoreBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-block-readmore a",
        },
        ],
    },
    readMoreRadius: {
        type: "object",
        default: { lg: { top: 2, right: 2, bottom: 2, left: 2, unit: 'px' }, unit: "px" },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-readmore a { border-radius:{{readMoreRadius}}; }",
        },
        ],
    },
    readMoreHoverColor: {
        type: "string",
        default: "rgba(255,255,255,0.80)",
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-block-readmore a:hover { color:{{readMoreHoverColor}}; } 
            {{ULTP}} .ultp-block-readmore a:hover svg { fill:{{readMoreHoverColor}} !important; }`,
        },
        ],
    },
    readMoreBgHoverColor: {
        type: "object",
        default: { openColor: 0, type: "color", color: "var(--postx_preset_Primary_color)" },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-block-readmore a:hover",
        },
        ],
    },
    readMoreHoverBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-block-readmore a:hover",
        },
        ],
    },
    readMoreHoverRadius: {
        type: "object",
        default: { lg: "", unit: "px" },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }",
        },
        ],
    },
    readMoreSacing: {
        type: "object",
        default: { lg: { top: 15, bottom: "", left: "", right: "", unit: "px" }},
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-readmore { margin:{{readMoreSacing}}; }",
        },
        ],
    },
    readMorePadding: {
        type: "object",
        default: { lg: { top: "2", bottom: "2", left: "6", right: "6", unit: "px" } },
        style: [
        {
            depends: [{ key: "readMore", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-block-readmore a { padding:{{readMorePadding}}; }",
        },
        ],
    },

};


export default readMoreAttribute;
