const queryAttribute = {
    // post grid 1
    queryQuick: {
        type: "string",
        default: "",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryNumPosts: {
        type: "object",
        default: { lg: 6 }
    },
    queryNumber: {
        type: "string",
        default: 4,
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryType: { type: "string", default: "post" },
    queryTax: {
        type: "string",
        default: "category",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryTaxValue: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [
            { key: "queryTax", condition: "!=", value: "" },
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryRelation: {
        type: "string",
        default: "OR",
        style: [
        {
            depends: [
            { key: "queryTaxValue", condition: "!=", value: "[]" },
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryOrderBy: { type: "string", default: "date" },
    metaKey: {
        type: "string",
        default: "custom_meta_key",
        style: [
        {
            depends: [
            { key: "queryOrderBy", condition: "==", value: "meta_value_num" },
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryOrder: { type: "string", default: "desc" },
    queryInclude: { type: "string", default: "" },
    queryExclude: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryAuthor: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryOffset: {
        type: "string",
        default: "0",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryExcludeTerm: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryExcludeAuthor: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    querySticky: {
        type: "boolean",
        default: true,
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts", "archiveBuilder"],
            },
            ],
        },
        ],
    },
    queryUnique: {
        type: "string",
        default: "",
        style: [
        {
            depends: [
            {
                key: "queryType",
                condition: "!=",
                value: ["customPosts", "posts"],
            },
            ],
        },
        ],
    },
    queryPosts: {
        type: "string",
        default: "[]",
        style: [
        { depends: [{ key: "queryType", condition: "==", value: "posts" }] },
        ],
    },
    queryCustomPosts: {
        type: "string",
        default: "[]",
        style: [
        {
            depends: [{ key: "queryType", condition: "==", value: "customPosts" }],
        },
        ],
    },
};

export default queryAttribute;