// vidIconPosition, iconSize
const videoAttribute = {
    // common
    vidIconEnable: { type: "boolean", default: true },
    popupAutoPlay: {
        type: "boolean",
        default: true,
        style: [
        { depends: [{ key: "enablePopup", condition: "==", value: true }] },
        ],
    },
    vidIconPosition: {
        type: "string",
        default: "topRight",
        style: [
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "bottomRight" },
            ],
            selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; right: 20px; }",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "center" },
            ],
            selector:
            "{{ULTP}} .ultp-video-icon {  margin: 0 auto; position: absolute; top: 50%; left: 50%; transform: translate(-50%,-60%); -o-transform: translate(-50%,-60%); -ms-transform: translate(-50%,-60%); -moz-transform: translate(-50%,-60%); -webkit-transform: translate(-50%,-50%); z-index: 998;}",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "bottomLeft" },
            ],
            selector: "{{ULTP}} .ultp-video-icon { bottom: 20px; left: 20px; }",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "topRight" },
            ],
            selector: "{{ULTP}} .ultp-video-icon { top: 20px; right: 20px; }",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "topLeft" },
            ],
            selector: "{{ULTP}} .ultp-video-icon { top: 20px; left: 20px; }",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "rightMiddle" },
            ],
            selector:
            "{{ULTP}} .ultp-video-icon {display: flex; justify-content: flex-end; align-items: center; height: 100%; width: 100%; top:0px;}",
        },
        {
            depends: [
            { key: "vidIconEnable", condition: "==", value: true },
            { key: "vidIconPosition", condition: "==", value: "leftMiddle" },
            ],
            selector:
            "{{ULTP}} .ultp-video-icon {display: flex; justify-content: flex-start; align-items: center; height: 100%; width: 100%; top: 0px;}",
        },
        ],
    },
    popupIconColor: {
        type: "string",
        default: "#fff",
        style: [
        {
            depends: [{ key: "vidIconEnable", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-video-icon svg { fill: {{popupIconColor}}; } 
            {{ULTP}} .ultp-video-icon svg circle { stroke: {{popupIconColor}}; }`,
        },
        ],
    },
    popupHovColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
        {
            depends: [{ key: "vidIconEnable", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-video-icon svg:hover { fill: {{popupHovColor}}; } 
            {{ULTP}} .ultp-video-icon svg:hover circle { stroke: {{popupHovColor}};}`,
        },
        ],
    },
    iconSize: {
        type: "object",
        default: { lg: "40", sm: "30", xs: "30", unit: "px" },
        style: [
        {
            depends: [{ key: "vidIconEnable", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-video-icon svg { height:{{iconSize}}; width: {{iconSize}};}",
        },
        ],
    },
    enablePopup: {
        type: "boolean",
        default: false,
        style: [
        { depends: [{ key: "vidIconEnable", condition: "==", value: true }] },
        ],
    },
    popupWidth: {
        type: "object",
        default: { lg: "70" },
        style: [
        {
            depends: [{ key: "enablePopup", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-video-modal__Wrapper {width:{{popupWidth}}% !important;}",
        },
        ],
    },
    enablePopupTitle: {
        type: "boolean",
        default: true,
        style: [
        { depends: [{ key: "enablePopup", condition: "==", value: true }] },
        ],
    },
    popupTitleColor: {
        type: "string",
        default: "#fff",
        style: [
        {
            depends: [
            { key: "enablePopupTitle", condition: "==", value: true },
            { key: "enablePopup", condition: "==", value: true },
            ],
            selector:
            "{{ULTP}} .ultp-video-modal__Wrapper a { color:{{popupTitleColor}} !important; }",
        },
        ],
    },
    closeIconSep: {
        type: "string",
        default: "#fff",
        style: [
        { depends: [{ key: "enablePopup", condition: "==", value: true }] },
        ],
    },
    closeIconColor: {
        type: "string",
        default: "#fff",
        style: [
        {
            depends: [{ key: "enablePopup", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-video-close { color:{{closeIconColor}}; }",
        },
        ],
    },
    closeHovColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
        {
            depends: [{ key: "enablePopup", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-video-close:hover { color:{{closeHovColor}}; }",
        },
        ],
    },
    closeSize: {
        type: "object",
        default: { lg: "70", unit: "px" },
        style: [
        {
            depends: [{ key: "enablePopup", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-video-close { font-size:{{closeSize}}; }",
        },
        ],
    },
};

export default videoAttribute;