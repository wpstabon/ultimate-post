// Post grid 3
// exclude - 'paginationText','loadMoreText', 'paginationAjax', 'pagiMargin', 'pagiAlign'
// pagiAlign, pagiTypo, pagiColor, pagiBgColor, pagiBorder, pagiShadow, pagiRadius, 

const paginationAttribute = {
    // paginationType: { type: "string", default: "pagination" },
    loadMoreText: {
        type: "string",
        default: "Load More",
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "loadMore" },
            ],
        },
        ],
    },
    paginationText: {
        type: "string",
        default: "Previous|Next",
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "pagination" },
            ],
        },
        ],
    },
    paginationNav: {
        type: "string",
        default: "textArrow",
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "pagination" },
            ],
        },
        ],
    },
    paginationAjax: {
        type: "boolean",
        default: true,
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "pagination" },
            ],
        },
        ],
    },
    navPosition: {
        type: "string",
        default: "topRight",
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "navigation" },
            ],
        },
        ],
    },
    pagiAlign: {
        type: "object",
        default: { lg: "center" },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-loadmore, 
            {{ULTP}} .ultp-next-prev-wrap ul, 
            {{ULTP}} .ultp-pagination, 
            {{ULTP}} .ultp-pagination-wrap { text-align:{{pagiAlign}}; }`,
        },
        ],
    },
    pagiTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: 14, unit: "px" },
        height: { lg: 20, unit: "px" },
        decoration: "none",
        family: "",
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
            {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
        },
        ],
    },
    pagiArrowSize: {
        type: "object",
        default: { lg: "14" },
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "navigation" },
            ],
            selector:
            "{{ULTP}} .ultp-next-prev-wrap ul li a svg { width:{{pagiArrowSize}}px; }",
        },
        ],
    },
    pagiColor: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)",
        style: [
            {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
                `{{ULTP}} .ultp-next-prev-wrap ul li a,
                {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a,
                {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action { color:{{pagiColor}}; }
                {{ULTP}} .ultp-pagination svg,
                {{ULTP}} .ultp-next-prev-wrap ul li a svg,
                {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action svg { fill:{{pagiColor}}; }`,
            },
        ],
    },
    pagiBgColor: {
        type: "object",
        default: { openColor: 1, type: "color", color: "var(--postx_preset_Primary_color)" },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a, 
            {{ULTP}} .ultp-next-prev-wrap ul li a, 
            {{ULTP}} .ultp-loadmore .ultp-loadmore-action`,
        },
        ],
    },
    pagiBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a,
            {{ULTP}} .ultp-next-prev-wrap ul li a,
            {{ULTP}} .ultp-loadmore-action`,
        },
        ],
    },
    pagiShadow: {
        type: "object",
        default: {
        openShadow: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a,
            {{ULTP}} .ultp-next-prev-wrap ul li a, 
            {{ULTP}} .ultp-loadmore-action`,
        },
        ],
    },
    pagiRadius: {
        type: "object",
        default: {
        lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a, 
            {{ULTP}} .ultp-next-prev-wrap ul li a, 
            {{ULTP}} .ultp-loadmore-action { border-radius:{{pagiRadius}}; }`,
        },
        ],
    },
    pagiHoverColor: {
        type: "string",
        default: "var(--postx_preset_Over_Primary_color)",
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-next-prev-wrap ul li a:hover,
            {{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a,
            {{ULTP}} .ultp-block-wrapper .ultp-loadmore-action:hover { color:{{pagiHoverColor}}; } 
            {{ULTP}} .ultp-pagination li a:hover svg,
            {{ULTP}} .ultp-next-prev-wrap ul li a:hover svg, 
            {{ULTP}} .ultp-block-wrapper .ultp-loadmore .ultp-loadmore-action:hover svg { fill:{{pagiHoverColor}}; } 
            @media (min-width: 768px) { 
                {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover { color:{{pagiHoverColor}};}
            }`,
        },
        ],
    },
    pagiHoverbg: {
        type: "object",
        default: { openColor: 1, type: "color", color: "var(--postx_preset_Secondary_color)", replace: 1 },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:hover, 
            {{ULTP}} .ultp-pagination-wrap .ultp-pagination li.pagination-active a, 
            {{ULTP}} .ultp-pagination-wrap .ultp-pagination li a:focus, 
            {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
            {{ULTP}} .ultp-loadmore-action:hover{ {{pagiHoverbg}} }`,
        },
        ],
    },
    pagiHoverBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a:hover, 
            {{ULTP}} .ultp-pagination li.pagination-active a, 
            {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
            {{ULTP}} .ultp-loadmore-action:hover`,
        },
        ],
    },
    pagiHoverShadow: {
        type: "object",
        default: {
        openShadow: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a:hover, 
            {{ULTP}} .ultp-pagination li.pagination-active a, 
            {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
            {{ULTP}} .ultp-loadmore-action:hover`,
        },
        ],
    },
    pagiHoverRadius: {
        type: "object",
        default: {
        lg: { top: "2", bottom: "2", left: "2", right: "2", unit: "px" },
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li.pagination-active a,
            {{ULTP}} .ultp-pagination li a:hover, 
            {{ULTP}} .ultp-next-prev-wrap ul li a:hover, 
            {{ULTP}} .ultp-loadmore-action:hover { border-radius:{{pagiHoverRadius}}; }`,
        },
        ],
    },
    pagiPadding: {
        type: "object",
        default: {
        lg: { top: "8", bottom: "8", left: "14", right: "14", unit: "px" },
        },
        style: [
        {
            depends: [{ key: "paginationShow", condition: "==", value: true }],
            selector:
            `{{ULTP}} .ultp-pagination li a, 
            {{ULTP}} .ultp-next-prev-wrap ul li a, 
            {{ULTP}} .ultp-loadmore-action { padding:{{pagiPadding}}; }`,
        },
        ],
    },
    navMargin: {
        type: "object",
        default: {
        lg: { top: "0", right: "0", bottom: "0", left: "0", unit: "px" },
        },
        style: [
        {
            depends: [
            { key: "paginationType", condition: "==", value: "navigation" },
            ],
            selector: "{{ULTP}} .ultp-next-prev-wrap ul { margin:{{navMargin}}; }",
        },
        ],
    },
    pagiMargin: {
        type: "object",
        default: {
        lg: { top: "35", right: "0", bottom: "0", left: "0", unit: "px" },
        },
        style: [
        {
            depends: [
            { key: "paginationType", condition: "!=", value: "navigation" },
            ],
            selector:
            `{{ULTP}} .ultp-pagination-wrap .ultp-pagination, 
            {{ULTP}} .ultp-loadmore { margin:{{pagiMargin}}; }`,
        },
        ],
    },
};

export default paginationAttribute;