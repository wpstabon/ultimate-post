const categoryAttribute = {
    // post grid 2
    catShow: { type: "boolean", default: true },
    maxTaxonomy: { type: "string", default: "30" },
    taxonomy: { type: "string", default: "category" },
    catStyle: { type: "string", default: "classic" },
    catPosition: { type: "string", default: "aboveTitle" },
    customCatColor: { type: "boolean", default: false },
    seperatorLink: {
        type: "string",
        default: ultp_data.category_url,
        style: [
        { depends: [{ key: "customCatColor", condition: "==", value: true }] },
        ],
    },
    onlyCatColor: {
        type: "boolean",
        default: false,
        style: [
        { depends: [{ key: "customCatColor", condition: "==", value: true }] },
        ],
    },
    catLineWidth: {
        type: "object",
        default: { lg: "20" },
        style: [
        {
            depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
            selector:
            `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
            {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before { width:{{catLineWidth}}px; }`,
        },
        ],
    },
    catLineSpacing: {
        type: "object",
        default: { lg: "30" },
        style: [
        {
            depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
            selector:
            `{{ULTP}} .ultp-category-borderBoth .ultp-category-in { padding-left: {{catLineSpacing}}px; padding-right: {{catLineSpacing}}px; } 
            {{ULTP}} .ultp-category-borderLeft .ultp-category-in { padding-left: {{catLineSpacing}}px; } 
            {{ULTP}} .ultp-category-borderRight .ultp-category-in { padding-right:{{catLineSpacing}}px; }`,
        },
        ],
    },
    catLineColor: {
        type: "string",
        default: "#000000",
        style: [
        {
            depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
            selector:
            `{{ULTP}} .ultp-category-borderRight .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderLeft .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderBoth .ultp-category-in:after, 
            {{ULTP}} .ultp-category-borderBoth .ultp-category-in:before { background:{{catLineColor}}; }`,
        },
        ],
    },
    catLineHoverColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
        {
            depends: [{ key: "catStyle", condition: "!=", value: "classic" }],
            selector:
            `{{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderBoth:hover .ultp-category-in:after, 
            {{ULTP}} .ultp-category-borderLeft:hover .ultp-category-in:before, 
            {{ULTP}} .ultp-category-borderRight:hover .ultp-category-in:before { background:{{catLineHoverColor}}; }`,
        },
        ],
    },
    catTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: 11, unit: "px" },
        height: { lg: 15, unit: "px" },
        spacing: { lg: 1, unit: "px" },
        transform: "",
        weight: "400",
        decoration: "none",
        family: "",
        },
        style: [
        {
            depends: [{ key: "catShow", condition: "==", value: true }],
            selector:
            "body {{ULTP}} div.ultp-block-wrapper .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a",
        },
        ],
    },
    catColor: {
        type: "string",
        default: "#fff",
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "==", value: false },
            ],
            selector:
            "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector:
            "{{ULTP}} .ultp-block-items-wrap .ultp-block-item .ultp-category-grid a { color:{{catColor}}; }",
        },
        ],
    },
    catBgColor: {
        type: "object",
        default: { openColor: 1, type: "color", color: "#000000" },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "!=", value: true },
            ],
            selector: "{{ULTP}} .ultp-category-grid a",
        },
        ],
    },
    catBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "!=", value: true },
            ],
            selector: "{{ULTP}} .ultp-category-grid a",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector: "{{ULTP}} .ultp-category-grid a",
        },
        ],
    },
    catRadius: {
        type: "object",
        default: { lg: "2", unit: "px" },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "!=", value: true },
            ],
            selector:
            "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector:
            "{{ULTP}} .ultp-category-grid a { border-radius:{{catRadius}}; }",
        },
        ],
    },
    catHoverColor: {
        type: "string",
        default: "#fff",
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "==", value: false },
            ],
            selector:
            "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector:
            "{{ULTP}} .ultp-block-items-wrap .ultp-category-grid a:hover { color:{{catHoverColor}}; }",
        },
        ],
    },
    catBgHoverColor: {
        type: "object",
        default: { openColor: 1, type: "color", color:  "var(--postx_preset_Primary_color)" },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "!=", value: true },
            ],
            selector: "{{ULTP}} .ultp-category-grid a:hover",
        },
        ],
    },
    catHoverBorder: {
        type: "object",
        default: {
        openBorder: 0,
        width: { top: 1, right: 1, bottom: 1, left: 1 },
        color: "#009fd4",
        type: "solid",
        },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "!=", value: true },
            ],
            selector: "{{ULTP}} .ultp-category-grid a:hover",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector: "{{ULTP}} .ultp-category-grid a:hover",
        },
        ],
    },
    catSacing: {
        type: "object",
        default: { lg: { top: 5, bottom: 5, left: 0, right: 0, unit: "px" } },
        style: [
        {
            depends: [{ key: "catShow", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-category-grid { margin:{{catSacing}}; }",
        },
        ],
    },
    catPadding: {
        type: "object",
        default: { lg: { top: 3, bottom: 3, left: 7, right: 7, unit: "px" } },
        style: [
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "onlyCatColor", condition: "!=", value: true },
            ],
            selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
        },
        {
            depends: [
            { key: "catShow", condition: "==", value: true },
            { key: "customCatColor", condition: "==", value: false },
            ],
            selector: "{{ULTP}} .ultp-category-grid a { padding:{{catPadding}}; }",
        },
        ],
    },
};

export default categoryAttribute;