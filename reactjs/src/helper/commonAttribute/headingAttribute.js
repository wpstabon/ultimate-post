const headingAttribute = {
    headingURL: { type: "string", default: "" },
    headingBtnText: {
        type: "string",
        default: "View More",
        style: [
        { depends: [{ key: "headingStyle", condition: "==", value: "style11" }] },
        ],
    },
    headingStyle: { type: "string", default: "style1" },
    headingTag: { type: "string", default: "h2" },
    headingAlign: {
      type: "string",
      default: "left",
      style: [{
          selector:
          `{{ULTP}} .ultp-heading-inner, 
          {{ULTP}} .ultp-sub-heading-inner { text-align:{{headingAlign}}; }`,
      },
    ],
  },
    headingTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: "20", unit: "px" },
        height: { lg: "", unit: "px" },
        decoration: "none",
        transform: "",
        family: "",
        weight: "700",
        },
        style: [{ selector: "{{ULTP}} .ultp-heading-wrap .ultp-heading-inner" }],
    },
    headingColor: {
        type: "string",
        default: "var(--postx_preset_Contrast_1_color)",
        style: [
        {
            selector:
            "{{ULTP}} .ultp-heading-inner span { color:{{headingColor}}; }",
        },
        ],
    },
    headingBorderBottomColor: {
        type: "string",
        default: "var(--postx_preset_Contrast_2_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
            selector:
            `{{ULTP}} .ultp-heading-inner span:before, 
            {{ULTP}} .ultp-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { border-color:{{headingBorderBottomColor}}; }",
        },
        ],
    },
    headingBorderBottomColor2: {
        type: "string",
        default: "var(--postx_preset_Base_3_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }",
        },
        ],
    },
    headingBg: {
        type: "string",
        default: "var(--postx_preset_Base_3_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
            selector:
            `{{ULTP}} .ultp-heading-style5 .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } 
            {{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; 
            }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
            selector:
            `{{ULTP}} .ultp-heading-inner span,
            {{ULTP}} .ultp-heading-inner span:after { background-color:{{headingBg}}; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { background-color:{{headingBg}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
            selector:
            `{{ULTP}} .ultp-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } 
            {{ULTP}} .ultp-heading-inner { background-color:{{headingBg}}; }`,
        },
        ],
    },
    headingBg2: {
        type: "string",
        default: "var(--postx_preset_Base_2_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { background-color:{{headingBg2}}; }",
        },
        ],
    },
    headingBtnTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: "14", unit: "px" },
        height: { lg: "", unit: "px" },
        decoration: "none",
        family: "",
        },
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
            selector:
            "{{ULTP}} .ultp-heading-wrap .ultp-heading-btn",
        },
        ],
    },
    headingBtnColor: {
        type: "string",
        default: "var(--postx_preset_Primary_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
            selector:
            `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn { color:{{headingBtnColor}}; } 
            {{ULTP}} .ultp-heading-wrap .ultp-heading-btn svg { fill:{{headingBtnColor}}; }`,
        },
        ],
    },
    headingBtnHoverColor: {
        type: "string",
        default: "var(--postx_preset_Secondary_color)",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style11" }],
            selector:
            `{{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover { color:{{headingBtnHoverColor}}; } 
            {{ULTP}} .ultp-heading-wrap .ultp-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }`,
        },
        ],
    },
    headingBorder: {
        type: "string",
        default: "3",
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-bottom-width:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { width:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style7" }],
            selector:
            `{{ULTP}} .ultp-heading-inner span:before, 
            {{ULTP}} .ultp-heading-inner span:after { height:{{headingBorder}}px; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style8" }],
            selector:
            `{{ULTP}} .ultp-heading-inner:before, 
            {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style9" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style10" }],
            selector:
            `{{ULTP}} .ultp-heading-inner:before, 
            {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-width:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style14" }],
            selector:
            `{{ULTP}} .ultp-heading-inner:before, 
            {{ULTP}} .ultp-heading-inner:after { height:{{headingBorder}}px; }`,
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style15" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style16" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span:before { height:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style17" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:before { height:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
            selector:
            "{{ULTP}} .ultp-heading-inner:after { width:{{headingBorder}}px; }",
        },
        ],
    },
    headingSpacing: {
        type: "object",
        default: { lg: 20, sm: 10, unit: "px" },
        style: [
        {
            selector:
            "{{ULTP}} .ultp-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}}; }",
        },
        ],
    },
    headingRadius: {
        type: "object",
        default: { lg: { top: "", bottom: "", left: "", right: "", unit: "px" } },
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
            selector:
            "{{ULTP}} .ultp-heading-inner { border-radius:{{headingRadius}}; }",
        },
        ],
    },
    headingPadding: {
        type: "object",
        default: { lg: { unit: "px" } },
        style: [
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style2" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style3" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style4" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style5" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style6" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style12" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style13" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style18" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style19" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style20" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        {
            depends: [{ key: "headingStyle", condition: "==", value: "style21" }],
            selector:
            "{{ULTP}} .ultp-heading-inner span { padding:{{headingPadding}}; }",
        },
        ],
    },
    subHeadingShow: { type: "boolean", default: false },
    subHeadingText: {
        type: "string",
        default:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.",
        style: [
        { depends: [{ key: "subHeadingShow", condition: "==", value: true }] },
        ],
    },
    subHeadingTypo: {
        type: "object",
        default: {
        openTypography: 1,
        size: { lg: "16", unit: "px" },
        spacing: { lg: "0", unit: "px" },
        height: { lg: "27", unit: "px" },
        decoration: "none",
        transform: "",
        family: "",
        weight: "500",
        },
        style: [
        {
            depends: [{ key: "subHeadingShow", condition: "==", value: true }],
            selector: "{{ULTP}} .ultp-sub-heading div",
        },
        ],
    },
    subHeadingColor: {
        type: "string",
        default: "var(--postx_preset_Contrast_2_color)",
        style: [
        {
            depends: [{ key: "subHeadingShow", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-sub-heading div { color:{{subHeadingColor}}; }",
        },
        ],
    },
    subHeadingSpacing: {
        type: "object",
        default: { lg: { top: "8", unit: "px" } },
        style: [
        {
            depends: [{ key: "subHeadingShow", condition: "==", value: true }],
            selector:
            "{{ULTP}} .ultp-sub-heading-inner { margin:{{subHeadingSpacing}}; }",
        },
        ],
    },
    enableWidth: {
        type: 'toggle',
        default: false,
        style: [
        {
            depends: [{ key: "subHeadingShow", condition: "==", value: true }],
        },
        ],
    },
    customWidth: {
        type: "object",
        default: { lg: { top: "", unit: "px" } },
        style: [
        {
            depends: [
            { key: "subHeadingShow", condition: "==", value: true },
            { key: "enableWidth", condition: "==", value: true }
            ],
            selector:
            "{{ULTP}} .ultp-sub-heading-inner { max-width:{{customWidth}}; }",
        },
        ],
    },
};

const attr = {}

const changes = findDifferentKeys(headingAttribute, attr);

function findDifferentKeys(obj1, obj2) {
  let differentKeys = [];

  // Loop through keys in obj1
  for (let key in obj1) {
      // Check if the key exists in obj2
      if (!(key in obj2)) {
          differentKeys.push(key);
          continue;
      }

      // Check for differences in values
      if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
          let nestedDiffKeys = findDifferentKeys(obj1[key], obj2[key]);
          if (nestedDiffKeys.length > 0) {
              differentKeys.push(key);
          }
      } else if (obj1[key] !== obj2[key]) {
          differentKeys.push(key);
      }
  }

  // Check for keys in obj2 that are not in obj1
  for (let key in obj2) {
      if (!(key in obj1)) {
          differentKeys.push(key);
      }
  }

  return differentKeys;
}


export default headingAttribute;