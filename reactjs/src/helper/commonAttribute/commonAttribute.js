import advancedWrapperAttribute from "./advancedWrapperAttribute";
import categoryAttribute from "./categoryAttribute";
import headingAttribute from "./headingAttribute";
import metaAttribute from "./metaAttribute";
import paginationAttribute from "./paginationAttribute";
import queryAttribute from "./queryAttribute";
import readMoreAttribute from "./readMoreAttribute";
import videoAttribute from "./videoAttribute";

export function commonAttributes(attrName, removeAttr, updateAttr) {
    let attr = {};

    const attrStore = {
        meta: metaAttribute,
        query: queryAttribute,
        video: videoAttribute,
        heading: headingAttribute,
        category: categoryAttribute,
        readMore: readMoreAttribute,
        pagination: paginationAttribute,
        advanceAttr: advancedWrapperAttribute,
        advFilter: {
            querySearch: {
                type: "string",
                default: "",
            },
            advFilterEnable: {
                type: "boolean",
                default: false,
            },
            advPaginationEnable: {
                type: "boolean",
                default: false,
            },

            defQueryTax: {
				type: 'object',
				default: {},
			},

            advRelation: {
                type: 'string',
                default: 'AND'
            }
        },
        pagiBlockCompatibility: {
            loadMoreText: {
                type: "string",
                default: "Load More",
                style: [
                    {
                        depends: [
                            {
                                key: "paginationType",
                                condition: "==",
                                value: "loadMore",
                            },
                        ],
                    },
                ],
            },
            paginationText: {
                type: "string",
                default: "Previous|Next",
                style: [
                    {
                        depends: [
                            {
                                key: "paginationType",
                                condition: "==",
                                value: "pagination",
                            },
                        ],
                    },
                ],
            },
            paginationAjax: {
                type: "boolean",
                default: true,
                style: [
                    {
                        depends: [
                            {
                                key: "paginationType",
                                condition: "==",
                                value: "pagination",
                            },
                        ],
                    },
                ],
            },
        },
    };

    /* ====== Dynamic Attribute By Specific Name  ====== */
    if (attrName?.length > 0) {
        attrName.forEach(function (name) {
            if(attrStore[name]) {
                attr = { ...attr, ...JSON.parse(JSON.stringify(attrStore[name])) }; // deep copy
            } else {
                console.log(name, ' - This Section not found in Dynamic attr store');
            }
        });
    }

    /* ====== Remove Attribute ====== */
    if (removeAttr?.length > 0) {
        removeAttr.forEach(function (attrKey, val) {
            delete attr[attrKey];
        });
    }

    /* ====== Update Attribute Value & Style ====== */
    if (updateAttr && updateAttr.length > 0) {
        updateAttr.forEach(function (attrKey) {
            if(attr[attrKey.key] && (attrKey?.default || attrKey?.style)) {
                if (attrKey?.default && attr[attrKey.key]) {
                    attr[attrKey.key].default = attrKey?.default;
                }
                if (attrKey?.style) {
                    attr[attrKey.key].style = attrKey?.style;
                }
            } else {
                console.log(attrKey.key, ' - This key not found in Dynamic attr store', attrKey?.default );
            }
        });
    }
    return attr;
}
