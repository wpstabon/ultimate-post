const metaAttribute = {
    /*============================
        Meta Setting/Style Settings
    ============================*/
    // post grid 1
    metaShow: { type: "boolean", default: true },
    metaPosition: { type: "string", default: "top" },
    metaStyle: { type: "string", default: "icon" },
    authorLink: { type: "boolean", default: true },
    metaSeparator: { type: "string", default: "dot" },
    metaList: { type: "string", default: '["metaAuthor","metaDate","metaRead"]' },
    metaMinText: { type: "string", default: "min read" },
    metaAuthorPrefix: { type: "string", default: "By" },
    metaDateFormat: { type: "string", default: "M j, Y" },
    metaTypo: {
    type: "object",
    default: {
        openTypography: 1,
        size: { lg: 12, unit: "px" },
        height: { lg: 20, unit: "px" },
        transform: "capitalize",
        decoration: "none",
        family: "",
    },
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector:
            `{{ULTP}} span.ultp-block-meta-element, 
            {{ULTP}} .ultp-block-item span.ultp-block-meta-element a`,
        },
    ],
    },
    metaColor: {
        type: "string",
        default: "var(--postx_preset_Contrast_3_color)",
        style: [
            {
            depends: [{ key: "metaShow", condition: "==", value: true }],
            selector:
                `{{ULTP}} span.ultp-block-meta-element svg { fill: {{metaColor}}; } 
                {{ULTP}} span.ultp-block-meta-element,
                {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element a { color: {{metaColor}}; }`,
                // {{ULTP}} .ultp-block-meta-dot span:after { background:{{metaColor}}; } {{ULTP}} span.ultp-block-meta-element:after { color:{{metaColor}}; }
            },
        ],
    },
    metaHoverColor: {
    type: "string",
    default: "var(--postx_preset_Primary_color)",
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector:
            `{{ULTP}} span.ultp-block-meta-element:hover, 
            {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:hover a { color: {{metaHoverColor}}; } 
            {{ULTP}} span.ultp-block-meta-element:hover svg { fill: {{metaHoverColor}}; }`,
        },
    ],
    },
    metaSeparatorColor: {
    type: "string",
    default: "var(--postx_preset_Contrast_1_color)",
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: 
        `{{ULTP}} .ultp-block-meta-dot span:after { background:{{metaSeparatorColor}}; } 
        {{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:after { color:{{metaSeparatorColor}}; }`,
        },
    ],
    },
    metaSpacing: {
    type: "object",
    default: { lg: "15", unit: "px" },
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector:
            `{{ULTP}} span.ultp-block-meta-element { margin-right:{{metaSpacing}}; } 
            {{ULTP}} span.ultp-block-meta-element { padding-left: {{metaSpacing}}; } 
            .rtl {{ULTP}} span.ultp-block-meta-element {margin-right:0; margin-left:{{metaSpacing}}; } 
            .rtl {{ULTP}} span.ultp-block-meta-element { padding-left:0; padding-right: {{metaSpacing}}; }`,
        },
        {
        depends: [
            { key: "metaShow", condition: "==", value: true },
            { key: "contentAlign", condition: "==", value: "right" },
        ],
        selector:
            "{{ULTP}} .ultp-block-items-wrap span.ultp-block-meta-element:last-child { margin-right:0px; }",
        },
    ],
    },
    metaMargin: {
    type: "object",
    default: { lg: { top: "5", bottom: "", left: "", right: "", unit: "px" } },
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-meta { margin:{{metaMargin}}; }",
        },
    ],
    },
    metaPadding: {
    type: "object",
    default: { lg: { top: "5", bottom: "5", left: "", right: "", unit: "px" } },
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-meta { padding:{{metaPadding}}; }",
        },
    ],
    },
    metaBorder: {
    type: "object",
    default: {
        openBorder: 0,
        width: { top: 1, right: "0", bottom: "0", left: "0" },
        color: "#009fd4",
        type: "solid",
    },
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-meta",
        },
    ],
    },
    metaBg: {
    type: "string",
    default: "",
    style: [
        {
        depends: [{ key: "metaShow", condition: "==", value: true }],
        selector: "{{ULTP}} .ultp-block-meta { background:{{metaBg}}; }",
        },
    ],
    },
};

export default metaAttribute;