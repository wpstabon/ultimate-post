const defaultLoadingColor = "var(--postx_preset_Primary_color)";


const advancedWrapperAttribute = {
    loadingColor: {
        type: "string",
        default: defaultLoadingColor,
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-loading .ultp-loading-blocks div { --loading-block-color: {{loadingColor}}; }",
            },
        ],
    },
    advanceId: {
        type: "string",
        default: "",
    },
    advanceZindex: {
        // type number
        type: "string",
        default: "",
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-block-wrapper{z-index:{{advanceZindex}};}",
            },
        ],
    },
    wrapBg: {
        type: "object",
        default: { openColor: 0, type: "color", color: "#f5f5f5" },
        style: [
            {
                selector: "{{ULTP}} .ultp-block-wrapper",
            },
        ],
    },
    wrapBorder: {
        type: "object",
        default: {
            openBorder: 0,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "#009fd4",
            type: "solid",
        },
        style: [
            {
                selector: "{{ULTP}} .ultp-block-wrapper",
            },
        ],
    },
    wrapShadow: {
        type: "object",
        default: {
            openShadow: 0,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "#009fd4",
        },
        style: [{ selector: "{{ULTP}} .ultp-block-wrapper" }],
    },
    wrapRadius: {
        type: "object",
        default: { lg: "", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-block-wrapper { border-radius:{{wrapRadius}}; }",
            },
        ],
    },
    wrapHoverBackground: {
        type: "object",
        default: { openColor: 0, type: "color", color: "#037fff" },
        style: [{ selector: "{{ULTP}} .ultp-block-wrapper:hover" }],
    },
    wrapHoverBorder: {
        type: "object",
        default: {
            openBorder: 0,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "#009fd4",
            type: "solid",
        },
        style: [{ selector: "{{ULTP}} .ultp-block-wrapper:hover" }],
    },
    wrapHoverRadius: {
        type: "object",
        default: { lg: "", unit: "px" },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }",
            },
        ],
    },
    wrapHoverShadow: {
        type: "object",
        default: {
            openShadow: 0,
            width: { top: 1, right: 1, bottom: 1, left: 1 },
            color: "#009fd4",
        },
        style: [{ selector: "{{ULTP}} .ultp-block-wrapper:hover" }],
    },
    wrapMargin: {
        type: "object",
        default: { lg: { top: "", bottom: "", unit: "px" } },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-block-wrapper { margin:{{wrapMargin}}; }",
            },
        ],
    },
    wrapOuterPadding: {
        type: "object",
        default: {
            lg: {
                top: "",
                bottom: "",
                left: "",
                right: "",
                unit: "px",
            },
        },
        style: [
            {
                selector:
                    "{{ULTP}} .ultp-block-wrapper { padding:{{wrapOuterPadding}}; }",
            },
        ],
    },

    /* ====== General Advnace Settings End ====== */
    hideExtraLarge: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    hideTablet: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    hideMobile: {
        type: "boolean",
        default: false,
        style: [{ selector: "{{ULTP}} {display:none;}" }],
    },
    advanceCss: {
        type: "string",
        default: "",
        style: [{ selector: "" }],
    },
}

export default advancedWrapperAttribute;