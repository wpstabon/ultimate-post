// Attributes
export const V4_1_0_CompCheck = {
    type: "object",
    default: {
        runComp: true,
    },
};

// Functions
export function handleCompatibility(props) {
    const {
        attributes: {
            V4_1_0_CompCheck: { runComp },
        },
    } = props;

    if (runComp) {
        handleV4_1_0_Comp(props);
    }
}

function handleV4_1_0_Comp(props) {

    const {
        attributes: {
            blockId,
            V4_1_0_CompCheck: { runComp },
            headingShow,
            paginationShow,
            filterShow,
        },
        setAttributes,
        name,
        clientId
    } = props;

    function turnOffComp() {
        // Old block that has depr settings turned on,
        // we have to skip running comp
        if (runComp) {
            setAttributes({
                V4_1_0_CompCheck: {
                    runComp: false,
                },
            });
        }
    }

    if (name === "ultimate-post/ultp-taxonomy" && headingShow) {
        if (!blockId) {
            setAttributes({
                headingShow: false,
            });
            insertHeadingBlock(clientId);
        } else {
            turnOffComp();
        }
        return;
    }

    if (!blockId) {
        // If its a New block we can turn off depr settings
        setAttributes({
            readMore: false,
            headingShow: false,
            paginationShow: false,
            filterShow: false,
        });
    } 

    else if (
        ["ultimate-post/post-slider-1", "ultimate-post/post-slider-2"]
            .includes(name) &&
        blockId &&
        headingShow
    ) {
        turnOffComp();
    } 
    
    else if (blockId && (headingShow || paginationShow || filterShow)) {
        turnOffComp();
    }
}

function insertHeadingBlock(clientId) {
    const {getBlockIndex, getBlockOrder, getBlockRootClientId, getBlockName} = wp.data.select('core/block-editor');
    const {insertBlock} = wp.data.dispatch('core/block-editor');
    const {createBlock} = wp.blocks;

    const index = getBlockIndex(clientId);
    const rootCId = getBlockRootClientId(clientId);

    // Prevents double heading insertion
    if (index > 0) {
        const allBlocks = getBlockOrder(rootCId);
        if (getBlockName(allBlocks[index - 1]) === "ultimate-post/heading") {
            return;
        }
    }

    insertBlock(
        createBlock("ultimate-post/heading", {}),
        index,
        rootCId,
        false
    );
}