import { getCustomFonts } from './CommonPanel'
import { cssBorder, cssBoxShadow, cssColor, cssDimension, cssFilter, cssTypography } from './CssHelper'
import { isDCActive } from './dynamic_content'

// Replace Value
const replaceData = (selector, key, value) => {
    return selector.replace(new RegExp(key, "g"), value)
}

// Object Empty Check
const isEmpty = (obj) => {
    return (typeof obj == 'object' && Object.keys(obj).length != 0) ? true : false
}

// {{ULTP}} Replace
const replaceWarp = (selector, ID) => {
    selector = selector.replace(new RegExp('{{WOPB}}', "g"), '.wopb-block-' + ID)
    return selector.replace(new RegExp('{{ULTP}}', "g"), '.ultp-block-' + ID)
}

// {{REPEAT_CLASS}} Replace
const replaceRepeat = (selector, ID) => {
    return selector.replace(new RegExp('{{REPEAT_CLASS}}', "g"), '.ultp-repeat-' + ID)
}

export const objectReplace = (warp, value) => {
    let output = ''
    value.forEach(sel => { output += sel + ';'; })
    return warp + '{' + output + '}';
}


export const objectAppend = (warp, value) => {
    let output = ''
    value.forEach(sel => { output += warp + sel; })
    return output
}

// Plain String/Number Field CSS Replace
export const singleField = (style, blockID, key, value, anotherKey=false, anotherKeyValue='') => {
    value = typeof value != 'object' ? value : objectField(value).data;
    if (typeof style == 'string') {
        if (style) {
            if (value) {
                let warpData = replaceWarp(style, blockID)
                if (typeof value == 'boolean') {
                    return [warpData]
                } else {
                    if (warpData.indexOf('{{') == -1 && warpData.indexOf('{') < 0) {
                        return [warpData + value]
                    } else {
                        if(anotherKey && (anotherKeyValue || anotherKeyValue == 0)) {
                            warpData = replaceData(warpData, '{{' + anotherKey + '}}', typeof anotherKeyValue == 'object' ? '' : anotherKeyValue);
                        }
                        return [replaceData(warpData, '{{' + key + '}}', value)]
                    }
                }
            } else {
                return []
            }
        } else {
            return [replaceWarp(value, blockID)] // Custom CSS Field
        }
    } else {
        let output = []
        style.forEach(sel => {
            output.push(replaceData(replaceWarp(sel, blockID), '{{' + key + '}}', value))
        });
        return output;
    }
}


// Object Field CSS Replace
const objectField = (data) => {
    if (data.openTypography) {
        return { data: cssTypography(data), action: 'append' }; //Typography
    } else if (data.openBorder) {
        return { data: cssBorder(data), action: 'append' }; //Border
    } else if (data.openShadow && data.color) {
        return { data: cssBoxShadow(data), action: 'append' }; //Shadow
    } else if (typeof (data.top) != 'undefined' || typeof (data.left) != 'undefined' || typeof (data.right) != 'undefined' || typeof (data.bottom) != 'undefined') {
        return { data: cssDimension(data), action: 'replace' }; //Dimension
    } else if (data.openColor) {
        if (data.replace) {
            return { data: cssColor(data), action: 'replace' }; //Color Advanced
        } else {
            return { data: cssColor(data), action: 'append' }; //Color Advanced
        }
    } else if (data.openFilter) {
        return { data: cssFilter(data), action: 'replace' }; //Filter
    } else if (data.onlyUnit) {
        return {
            data: data._value ? data._value + (data.unit || '') : '',
            action: 'replace'
        }
    } else {
        return { data: '', action: 'append' };
    }
}


export const checkDepends = (settings, selectData, dc={}) => {
    let _depends = true
    if (selectData?.hasOwnProperty('depends')) {
        selectData.depends.forEach((data) => {

            let setting;

            if (dc?.id) {
                setting = settings[dc.key][dc.id]?.[data.key] 
            } else {
                setting = settings[data.key]
            }

            let previous = _depends
            if (data.condition == '==') {
                if ((typeof data.value == 'string') || (typeof data.value == 'number') || (typeof data.value == 'boolean')) {
                    _depends = setting == data.value
                } else {
                    let select = false
                    data.value.forEach(arrData => {
                        if (setting == arrData) { select = true; }
                    })
                    _depends = select;
                }
            } else if (data.condition == '!=') {
                if ((typeof data.value == 'string') || (typeof data.value == 'number') || (typeof data.value == 'boolean')) {
                    _depends = setting != data.value
                } else {
                    let select = false
                    if (Array.isArray(data.value)) {
                        data.value.forEach(arrData => {
                            if (setting != arrData) { select = true; }
                        })
                    }
                    if (select) { _depends = true; }
                }
            }
            _depends = previous == false ? false : _depends
        })
    }

    return _depends;
}

function getStyles(settings, key, selectData, cssSelecor, blockID, isInline, globalSettings, anotherKey, anotherKeyValue, dc = {} ) {
    if (!checkDepends(settings, selectData, dc)) {
        return {_lg: [], _sm: [], _xs: [], _notResponsiveCss: []};
    }

    let lg = [];
    let sm = [];
    let xs = [];
    let notResponsiveCss = [];
    
    let setting;

    if (dc?.id) {
       setting = settings[dc.key][dc.id] ? 
                    settings[dc.key][dc.id][key] :
                    settings[dc.key].default[key];
    } else {
        setting = settings[key];
    }

    if (typeof setting == 'object') {
        let device = false;
        let dimension = '';
        if (setting.lg) { // Exta Large
            device = true
            dimension = typeof setting.lg == 'object' ? objectField(setting.lg).data : setting.lg + (setting.ulg || setting.unit || '')
            lg = lg.concat(singleField(cssSelecor, blockID, key, dimension, anotherKey, typeof anotherKeyValue == 'object' ? anotherKeyValue?.lg : anotherKeyValue ));
        }
        // if (settings[key].md) { // Desktop
        //     device = true
        //     dimension = typeof settings[key].md == 'object' ? objectField(settings[key].md).data : settings[key].md + (settings[key].umd || settings[key].unit || '')
        //     md = md.concat(singleField(cssSelecor, blockID, key, dimension))
        // }
        if (setting.sm) { // Tablet
            device = true
            dimension = typeof setting.sm == 'object' ? objectField(setting.sm).data : setting.sm + (setting.usm || setting.unit || '')
            sm = sm.concat(singleField(cssSelecor, blockID, key, dimension, anotherKey, typeof anotherKeyValue == 'object' ? anotherKeyValue?.sm : anotherKeyValue ))
        }
        if (setting.xs) { // Phone
            device = true
            dimension = typeof setting.xs == 'object' ? objectField(setting.xs).data : setting.xs + (setting.uxs || setting.unit || '')
            xs = xs.concat(singleField(cssSelecor, blockID, key, dimension, anotherKey, typeof anotherKeyValue == 'object' ? anotherKeyValue?.xs : anotherKeyValue ))
        }
        if (!device) { // Object Field Type Only
            const objectCss = objectField(setting)
            const repWarp = replaceWarp(cssSelecor, blockID)
            if (typeof objectCss.data == 'object') {
                if (Object.keys(objectCss.data).length != 0) {
                    if (objectCss.data.background) { notResponsiveCss.push(repWarp + objectCss.data.background) }

                    // Typography
                    if (isEmpty(objectCss.data.lg)) { lg.push(objectReplace(repWarp, objectCss.data.lg)) }
                    //if (isEmpty(objectCss.data.md)) { md.push(objectReplace(repWarp, objectCss.data.md)) }
                    if (isEmpty(objectCss.data.sm)) { sm.push(objectReplace(repWarp, objectCss.data.sm)) }
                    if (isEmpty(objectCss.data.xs)) { xs.push(objectReplace(repWarp, objectCss.data.xs)) }
                    if (objectCss.data.simple) { notResponsiveCss.push(repWarp + objectCss.data.simple) }
                    if (objectCss.data.font) { lg.unshift(objectCss.data.font) }

                    if (objectCss.data.shape) {
                        (objectCss.data.shape).forEach(function (el) { notResponsiveCss.push(repWarp + el) });
                        if (isEmpty(objectCss.data.data.lg)) { lg.push(objectAppend(repWarp, objectCss.data.data.lg)) }
                        if (isEmpty(objectCss.data.data.sm)) { sm.push(objectAppend(repWarp, objectCss.data.data.sm)) }
                        if (isEmpty(objectCss.data.data.xs)) { xs.push(objectAppend(repWarp, objectCss.data.data.xs)) }
                    }
                }
            } else if (objectCss.data && (objectCss.data).indexOf('{{') == -1) {
                if (objectCss.action == 'append') {
                    notResponsiveCss.push(repWarp + objectCss.data)
                } else {
                    notResponsiveCss.push(singleField(cssSelecor, blockID, key, objectCss.data, anotherKey, anotherKeyValue))
                }
            }
        }
    } else {
        if (key == 'hideExtraLarge') {
            if (isInline) { notResponsiveCss = notResponsiveCss.concat('@media (min-width: '+(globalSettings.breakpointSm || 992)+'px) {' + (singleField(cssSelecor, blockID, key, setting, anotherKey, anotherKeyValue)) + '}') }
        } else if (key == 'hideTablet') {
            if (isInline) { notResponsiveCss = notResponsiveCss.concat( '@media only screen and (max-width: '+(globalSettings.breakpointSm || 991)+'px) and (min-width: 768px) {' + (singleField(cssSelecor, blockID, key, setting, anotherKey, anotherKeyValue)) + '}' ) }
        } else if (key == 'hideMobile') {
            if (isInline) { notResponsiveCss = notResponsiveCss.concat( '@media (max-width: '+(globalSettings.breakpointXs || 767)+'px) {' + (singleField(cssSelecor, blockID, key, setting, anotherKey, anotherKeyValue)) + '}' ) }
        }
        else {
            if (setting) {
                notResponsiveCss = notResponsiveCss.concat(singleField(cssSelecor, blockID, key, setting, anotherKey, anotherKeyValue))
            }
        }
    }

    return {
        _lg: lg,
        _sm: sm,
        _xs: xs,
        _notResponsiveCss: notResponsiveCss
    }
}

function getGlobalSettings() {
    const _temp = localStorage.getItem('ultpGlobal'+ultp_data.blog);

    if (_temp) {
        try {
            const settings = JSON.parse(_temp);
            return typeof settings === 'object' ? settings : {};
        } catch {
            return {};
        }
    }

    return {};
}

export const CssGenerator = (settings, blockName, blockID, isInline = false) => {
    if (!blockID) return;
    let __CSS = '',
        lg = [],
        sm = [],
        xs = [],
        notResponsiveCss = [];

    const globalSettings = getGlobalSettings();

    Object.keys(settings).forEach((key) => {
        const attributes = typeof blockName === 'string' ? wp.blocks.getBlockType(blockName).attributes : blockName;
        const anotherKey = attributes[key]?.anotherKey;
        const anotherKeyValue = settings[anotherKey];

        if (attributes[key] && attributes[key].hasOwnProperty('style')) {         
            attributes[key].style.forEach((selectData, indexStyle) => {
                if (selectData?.hasOwnProperty('selector')) {
                    const cssSelector = selectData.selector;
                    const {_lg, _sm, _xs, _notResponsiveCss} = getStyles(settings, key, selectData, cssSelector, blockID, isInline, globalSettings, anotherKey, anotherKeyValue);
                    lg = lg.concat(_lg);
                    sm = sm.concat(_sm);
                    xs = xs.concat(_xs);
                    notResponsiveCss = notResponsiveCss.concat(_notResponsiveCss);
                }
            });
        } else {

            // Dynamic Content
            if (isDCActive() && settings.dcEnabled && key === "dcFields") {
                settings[key].forEach(group => {
                    if (!group) return;

                    // Group Styles
                    Object.keys(
                        settings.dcGroupStyles[group.id] ?? 
                        settings.dcGroupStyles.default
                    ).forEach(attrKey => {
                        attributes.dcGroupStyles.fields[attrKey].style?.forEach(( style ) => {
                            const selector = style.selector.replace('{{dcID}}', group.id);

                            const {_lg, _sm, _xs, _notResponsiveCss} = getStyles(settings, attrKey, style, selector, blockID, isInline, globalSettings, anotherKey, anotherKeyValue, {
                                id: group.id,
                                key: 'dcGroupStyles'
                            });
                            lg = lg.concat(_lg);
                            sm = sm.concat(_sm);
                            xs = xs.concat(_xs);
                            notResponsiveCss = notResponsiveCss.concat(_notResponsiveCss);
                        });

                    });

                    // Field Styles
                    group.fields.forEach(field => {
                        Object.keys(
                            settings.dcFieldStyles[field.id] ?? 
                            settings.dcFieldStyles.default
                        ).forEach(attrKey => {

                            attributes.dcFieldStyles.fields[attrKey].style?.forEach(( style ) => {
                                const selector = style.selector.replace('{{dcID}}', field.id);

                                const {_lg, _sm, _xs, _notResponsiveCss} = getStyles(settings, attrKey,style, selector, blockID, isInline, globalSettings, anotherKey, anotherKeyValue, {
                                    id: field.id,
                                    key: 'dcFieldStyles'
                                });
                                lg = lg.concat(_lg);
                                sm = sm.concat(_sm);
                                xs = xs.concat(_xs);
                                notResponsiveCss = notResponsiveCss.concat(_notResponsiveCss);
                            });
                        });
                    });
                });
            }

            // Repetable Fields
            if (attributes[key] && attributes[key].type == 'array' ) {
                if (attributes[key].hasOwnProperty('fields')) {
                    Object.keys(attributes[key].fields).forEach((k) => {
                        if (attributes[key].fields[k].hasOwnProperty('style')) {
                            attributes[key].fields[k].style.forEach((data, _k) => {
                                if (data?.hasOwnProperty('selector')) {
                                    if (Array.isArray(settings[key])) {
                                        settings[key].forEach((el, __k) => {
                                            let repWarp = replaceWarp(data.selector, blockID)
                                            repWarp = replaceRepeat(repWarp, __k)
                                            repWarp = replaceData(repWarp, '{{'+k+'}}', el[k])
                                            notResponsiveCss.push(repWarp)
                                        });
                                    }
                                }
                            });
                        }
                    })
                }
            }
        }
    })
    
    // Join CSS
    if (lg.length > 0) { __CSS += lg.join('') } //lg
    if (sm.length > 0) { __CSS += '@media (max-width: '+(globalSettings.breakpointSm || 991)+'px) {' + sm.join('') + '}' } //sm
    if (xs.length > 0) { __CSS += '@media (max-width: '+(globalSettings.breakpointXs || 767)+'px) {' + xs.join('') + '}' } //xs
    if (notResponsiveCss.length > 0) { __CSS += notResponsiveCss.join('') }
    
    if (isInline) {
        if(ultp_data.settings?.ultp_custom_font == 'true' && ultp_data.active) {
            return getCustomFonts(__CSS, true);
        }
        return __CSS;
    }

    // set custom font
    if(ultp_data.settings?.ultp_custom_font == 'true') {
        __CSS = getCustomFonts(__CSS, true);
    }

    // Set CSS
    setDevice(__CSS, blockID);
}

// Check Device
const setDevice = (styleCss, blockID) => {
    const currentDevice = wp.data.select("core/edit-post");
    if (currentDevice || document.body.classList.contains('site-editor-php')) { // For FSE Checking
        const editor_canvas = window.document.getElementsByName('editor-canvas');
        if (editor_canvas[0]?.contentDocument) {
            setTimeout(function() {
                setStyle(editor_canvas[0]?.contentDocument, styleCss, blockID);
            }, 0);
        } else {
            setStyle(window.document, styleCss, blockID);
        }
    } else {
        setStyle(window.document, styleCss, blockID);
    }
}

// Set CSS to Head
const setStyle = (styleSelector, styleCss, blockID) => {
    styleSelector = styleSelector || window.document;
    if (styleSelector.getElementById('ultp-block-' + blockID) === null) {
        let cssInline = document.createElement('style');
        cssInline.type = 'text/css';
        cssInline.id = 'ultp-block-' + blockID;
        if (cssInline.styleSheet) {
            cssInline.styleSheet.cssText = styleCss;
        } else {
            cssInline.innerHTML = styleCss;
        }
        styleSelector.getElementsByTagName("head")[0].appendChild(cssInline);
    } else {
        styleSelector.getElementById('ultp-block-' + blockID).innerHTML = styleCss;
    }
}