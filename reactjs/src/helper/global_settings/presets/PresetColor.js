import { handlePostxPresets, postxPresetAttr } from "../../CommonFunctions";
import Color from "../../fields/Color";
import icons from "../../icons";
const { useState, Fragment } = wp.element;
const { __ } = wp.i18n;
const { Dropdown, Tooltip, GradientPicker } = wp.components

const PresetColor = (props) => {
    const { presetColors, setPresetColors, presetGradients, setPresetGradients, _getColor, setGlobalColor, setSaveNotice } = props;
    const [ newColorLabel, setNewColorLabel ] = useState('');
    const [ newColor, setNewColor ] = useState('');
    const [ presetGradientTab, setPresetGradientTab ] = useState(false);
   
    const handleColors = (key, value, oldKey, colorType) => {
        setSaveNotice(true);
        let newObj = colorType == 'gradient' ? {...presetGradients} : {...presetColors};
        const setColorGradient = colorType == 'gradient' ? setPresetGradients : setPresetColors;
        const replaceKey = colorType == 'gradient' ? '_gradient' : '_color';

        if(key == 'delete') {
            delete newObj[value];
        } else if(key=='colorlabel') {
            value = value.replaceAll(' ', '_') + replaceKey;
            const updatedObj = {};
            for (const key in newObj) {
                updatedObj[key == oldKey ? value : key ] = newObj[key];
            }
            newObj = {...updatedObj};

        } else {
            if (typeof value == 'object' && colorType != 'gradient') {
                value = _getColor(value)
            }
            newObj = {...newObj, [key]: value}
        }

        newObj = {...newObj, ['rootCSS']: setGlobalColor(newObj, colorType)}
        setColorGradient(newObj);
    }

    // for presetGradients Compatiblity
    const _compatiblity = ( data ) => {
        if (typeof data == 'object') {
            if (data.type == 'linear') {
                return 'linear-gradient('+data.direction+'deg, '+data.color1+' '+data.start+'%, '+data.color2+' '+data.stop+'%)'
            } else {
                return 'radial-gradient( circle at ' + data.radial + ' , ' + data.color1 + ' ' + data.start + '%,' + data.color2 + ' ' + data.stop + '%)'
            }
        }
        return data || {};
    }

    const generateLabel = (val, onClose, action, colorType) => {

        const reserveKey = postxPresetAttr( colorType == 'gradient' ? 'presetGradientKeys' : 'presetColorKeys');
        const replaceKey = colorType == 'gradient' ? '_gradient' : '_color';
        if ( colorType != 'gradient' ) {
            reserveKey.push('Over_Primary_color');
        }
        const label = <div className="ultp-editor-preset-typo-lists ultp-preset-color-label">
            <div className="typo-label-container">
                <div className="typo-label-demo">
                    <div>Name: </div>
                    {
                        reserveKey.includes(val) ?
                        <div className='typo-label'>{val.replace(replaceKey, '').replaceAll('_', ' ') }</div>
                        : 
                        <input className='typo-label' defaultValue={val.replace(replaceKey, '').replaceAll('_', ' ')} onBlur={(e)=> {setNewColorLabel(e.target.value.replaceAll(' ', '_')+replaceKey); handleColors('colorlabel', e.target.value, action ? newColorLabel : val, colorType)} } onClick={(e) => e.stopPropagation()}/>
                    }
                </div>
                { !reserveKey.includes(val) && <div className="color-delete" onClick={(e) => { e.stopPropagation(); handleColors('delete', val, '', colorType); onClose()  }}>{icons['delete']}</div> }
            </div>
        </div>
        
        return label;
    }

    return (
        <div>
            <div className={`ultp-global-color-label`}>
                <div className="ultp-global-color-label-content">Color Palette</div>
                <div className="ultp-color-field-tab">
                    <div className={`${presetGradientTab ? '' : 'active'}`} onClick={()=> setPresetGradientTab(false)}>Solid</div>
                    <div className={`${presetGradientTab ? 'active' : ''}`} onClick={()=> setPresetGradientTab(true)}>Gradient</div>
                </div>
            </div>
            {
                !presetGradientTab ? <div className={`ultp-global-color-preset`}>
                    { Object.keys(presetColors).map( (val, k) => {
                        return <Fragment key={k}> 
                            {val !='rootCSS' && <Dropdown
                                className="ultp-range-control"
                                contentClassName="ultp-editor-preset-color-dropdown components-dropdown__content"
                                // position="top left"
                                popoverProps={{ placement: 'top left' }}
                                focusOnMount={true}
                                renderToggle={({ onToggle, onClose }) => (
                                    <Tooltip position={'top'} text={val.replace('_color', '').replaceAll('_', ' ')}>
                                        <span className={`ultp-global-color`} onClick={() => onToggle()} style={{backgroundColor: presetColors[val]}}/>
                                    </Tooltip>
                                )}
                                renderContent={({onClose}) => (
                                    <div>
                                        <Color
                                            presetSetting={true}
                                            value={presetColors[val]}
                                            onChange={v => handleColors(val, v, '', '')}
                                        />
                                        { generateLabel(val, onClose, '', '') }
                                    </div>
                                )}
                            />}
                        </Fragment>
                    })}
                    <Fragment>
                        <Dropdown
                            className="ultp-range-control"
                            contentClassName="ultp-editor-preset-color-dropdown components-dropdown__content"
                            // position="top left"
                            popoverProps={{ placement: 'top left' }}
                            focusOnMount={true}
                            renderToggle={({ onToggle, onClose }) => (
                                <span className="ultp-global-color-add" onClick={() => {
                                        onToggle(); 
                                        const randLetter = String.fromCharCode(97 + Math.floor(Math.random() * 26));
                                        handleColors(`Custom_${Object.keys(presetColors).length - 9}${randLetter}_color`, '#000', '', 'color');
                                        setNewColor('#000'); 
                                        setNewColorLabel(`Custom_${Object.keys(presetColors).length - 9}${randLetter}_color`);
                                    }}
                                >{icons['plus']}</span>
                            )}
                            renderContent={({onClose}) => (
                                <div>
                                    <Color
                                        presetSetting={true}
                                        value={newColor || '#000'}
                                        onChange={v => {
                                            if (typeof v == 'object') {
                                                v = _getColor(v);
                                            }
                                            setNewColor(v)
                                            handleColors(newColorLabel, v, '', '');
                                        }}
                                    />
                                    { generateLabel(newColorLabel, onClose, 'save', '')  }
                                </div>
                                
                            )}
                        />
                    </Fragment>
                </div>
                :
                <div className={`ultp-global-color-preset`}>
                    { Object.keys(presetGradients).map( (val, k) => {
                        return <Fragment key={k}>
                            {val !='rootCSS' && <Dropdown
                                className="ultp-range-control"
                                contentClassName="ultp-editor-preset-gradient-dropdown components-dropdown__content"
                                // position="top left"
                                popoverProps={{ placement: 'top left' }}
                                focusOnMount={true}
                                renderToggle={({ onToggle, onClose }) => (
                                    <Tooltip position={'top'} text={val.replace('_gradient', '').replaceAll('_', ' ')}>
                                        <span className={`ultp-global-color`} onClick={() => {
                                                !['Primary_to_Secondary_to_Right_gradient', 'Primary_to_Secondary_to_Bottom_gradient', 'Secondary_to_Primary_to_Right_gradient', 'Secondary_to_Primary_to_Bottom_gradient'].includes(val) && onToggle()
                                            }} 
                                            style={{background: presetGradients[val]}}
                                        />
                                    </Tooltip>
                                )}
                                renderContent={({onClose}) => (
                                    <>
                                        <div className="ultp-common-popup ultp-color-popup ultp-color-container">
                                            <GradientPicker
                                                clearable={false}
                                                __nextHasNoMargin={true}
                                                value={ typeof presetGradients[val] == 'object' ? _compatiblity(presetGradients[val]) : presetGradients[val] }
                                                onChange={v => {
                                                    handleColors(val, _compatiblity(v), '', 'gradient');
                                                }}
                                            />
                                        </div>
                                        { generateLabel(val, onClose, '', 'gradient') }
                                    </>
                                )}
                            />}
                        </Fragment>
                    })}
                    <Fragment>
                        <Dropdown
                            className="ultp-range-control"
                            contentClassName="ultp-editor-preset-gradient-dropdown components-dropdown__content"
                            // position="top left" 
                            popoverProps={{ placement: 'top left' }}
                            focusOnMount={true}
                            renderToggle={({ onToggle, onClose }) => (
                                <span className="ultp-global-color-add" onClick={() => { 
                                    onToggle();
                                    const randLetter = String.fromCharCode(97 + Math.floor(Math.random() * 26));
                                    handleColors(`Custom_${Object.keys(presetGradients).length - 5}${randLetter}_gradient`, 'linear-gradient(113deg, rgb(102, 126, 234) 0%, rgb(118, 75, 162) 100%)', '', 'gradient');
                                    setNewColor('linear-gradient(113deg, rgb(102, 126, 234) 0%, rgb(118, 75, 162) 100%)'); 
                                    setNewColorLabel(`Custom_${Object.keys(presetGradients).length - 5}${randLetter}_gradient`) 
                                }}>{icons['plus']}</span>
                            )}
                            renderContent={({onClose}) => (
                                <>
                                    <div className="ultp-common-popup ultp-color-popup">
                                        <GradientPicker
                                            className="ultp-common-popup ultp-color-popup ultp-color-container"
                                            clearable={false}
                                            value={newColor || 'linear-gradient(113deg, rgb(102, 126, 234) 0%, rgb(118, 75, 162) 100%)' }
                                            onChange={val => {
                                                setNewColor(_compatiblity(val));
                                                handleColors(newColorLabel, val, '', 'gradient');
                                            }}
                                        />
                                    </div>
                                    { generateLabel(newColorLabel, onClose, 'save', 'gradient')  }
                                </>
                                
                            )}
                        />
                    </Fragment>
                </div>
            }
        </div>
    );
};

export default PresetColor;