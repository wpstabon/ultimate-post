import { handlePostxPresets, postxPresetAttr } from "../../CommonFunctions";
import Number from "../../fields/Number";
import Toggle from "../../fields/Toggle";
import Typography from "../../fields/Typography";
import icons from "../../icons";
const { Fragment } = wp.element;
const { Dropdown } = wp.components
const { __ } = wp.i18n

const multipleTypos = postxPresetAttr('multipleTypos')

const PresetTypos = (props) => {
    const { presetTypos, setPresetTypos, device, setDevice, setGlobalTypo, enablePresetTypoCSS, setValue, setSaveNotice } = props;

    const handleTypos = (type, value, key, oldKey) => {
        setSaveNotice(true);
        let newObj = {...presetTypos};
        if(type == 'delete') {
            delete newObj[key];
        } else if(['size', 'height'].includes(type)) {
            newObj = {...newObj, [key] : { ...presetTypos[key], [type]: value }};
        } else if(type=='all') {
            multipleTypos[key].forEach(element => {
                value = {...value , ['size']: presetTypos[element].size }
                newObj = {...newObj, [element] : value}
            });
        } else if(type == 'typolabel') {
            key = key.replaceAll(' ', '_') + '_typo';
            const updatedObj = {};

            for (const keys in newObj) {
                updatedObj[keys == oldKey ? key : keys ] = newObj[keys]
            }
            newObj = {...updatedObj};

        } else {
            newObj = {...newObj, [key]: value}
        }
        newObj = {...newObj, ['presetTypoCSS']: setGlobalTypo(newObj)};
        setPresetTypos(newObj);
    }

    const generateLabel = (val) => {
        const reserveKey = [...postxPresetAttr('presetTypoKeys'), 'presetTypoCSS'] ;
        const replaceKey = '_typo';
        
        const label = <div className="ultp-editor-preset-typo-lists">
            <div className="typo-label-container">
              <div className="typo-label-demo">
                <div className="typo-demo" style={{fontFamily: presetTypos[val].family}}>Aa</div>
                {
                    reserveKey.includes(val) ?
                    <div className='typo-label'>{val.replace(replaceKey, '').replaceAll('_', ' ') }</div>
                    : 
                    <input type="text" className='typo-label' value={val.replace(replaceKey, '').replaceAll('_', ' ')} onChange={(e)=> {handleTypos('typolabel', '', e.target.value, val)} } onClick={(e) => e.stopPropagation()}/>
                }
              </div>
            </div>
            <div className="ultp-editor-typo-actions">
                <div className="typo-edit" >{icons['edit']}</div>
                { !reserveKey.includes(val) && <div className="typo-delete" onClick={(e) => { e.stopPropagation(); handleTypos('delete', '', val,); }}>{icons['delete']}</div> }
            </div>
        </div>
        return label;
    }

    const typoHtml = (item , parent, k) => {
        return <Fragment key={k}>
            <Dropdown
                className="ultp-editor-dropdown"
                contentClassName="ultp-editor-typo-content ultp-editor-preset-dropdown components-dropdown__content"
                popoverProps={ { placement: 'top-start' } }
                focusOnMount={true}
                renderToggle={({ onToggle }) => (
                    <div className={`ultp-global-label ultp-field-typo`} onClick={() => onToggle()} >
                        {generateLabel(item)}
                    </div>
                )}
                renderContent={({onClose}) => <div className="ultp-global-typos">
                    <Typography
                        presetSetting={true}
                        presetSettingParent={parent == 'all' ? true : false}
                        key={k}
                        value={presetTypos[item]}
                        label={''} 
                        device={device}
                        setDevice={(v) =>{setDevice(v)}}
                        onChange={v => handleTypos('parent', v, item)}
                        onDeviceChange={value => {}} 
                    />
                    { parent == 'all' && <div className="ultp-field-wrap ultp-field-separator ultp-typo-separator"><div><span className="typo_title">Font Size & Line Height</span></div></div> }
                    {
                        parent == 'all' && multipleTypos[item].map( (val, k) => {                            
                            return <div key={k} className="ultp-preset-typo-seperate-setiings">
                                <div className="ultp-field-wrap ultp-field-separator ultp-typo-separator"><div className="typo-left"><span className="typo_title">{val.replace('_typo', '').replaceAll('_', ' ')}</span></div></div> 
                                <div key={k} className="ultp-typo-container ultp-typ-family">
                                    <div className={`ultp-typo-section`}>
                                        <Number
                                            min={ 1 }
                                            max={ 300 }
                                            step={ 1 }
                                            unit={['px','em','rem']}
                                            responsive
                                            label={'Font Size'}
                                            value={ presetTypos[val].size || '' }
                                            device={device}
                                            setDevice={setDevice}
                                            onChange={ v => handleTypos('size', v, val )}
                                        />
                                    </div>
                                    <div className={`ultp-typo-section`}>
                                        <Number
                                            min={ 1 }
                                            max={ 300 }
                                            step={ 1 }
                                            responsive
                                            device={device}
                                            setDevice={setDevice}
                                            unit={['px','em','rem']}
                                            label={'Line Height'}
                                            value={ presetTypos[val].height || '' }
                                            onChange={ v => handleTypos('height', v, val )}
                                        />
                                    </div>
                                </div>
                            </div>
                        })
                    }
                </div>}
            />
        </Fragment>
    }

    return (
        <div className="ultp-editor-global-typo-container"> 
            <Toggle
                label={__('Override Theme Typography','ultimate-post')}
                value={enablePresetTypoCSS}
                onChange={v => setValue('enablePresetTypoCSS',v)}
            />
            <div className="ultp-editor-global-typo-section">
                {
                    Object.keys(multipleTypos).map( (item, k) => {
                        return typoHtml(item, 'all', k)
                    })
                }
                {
                    Object.keys(presetTypos).map( (item, k) => {
                        return ![...multipleTypos.Body_and_Others_typo, ...multipleTypos.Heading_typo, ...postxPresetAttr('presetTypoKeys'), 'presetTypoCSS'].includes(item) && typoHtml(item, 'single', k)
                    })
                }
            </div>
            <span className="ultp-global-typo-add" onClick={() => {handleTypos('',  {openTypography: 1, family: "Arial", type: "sans-serif", weight: 100 }, `Custom_${Object.keys(presetTypos).length - 12}${String.fromCharCode(97 + Math.floor(Math.random() * 26))}_typo`, '')}}><span className="typo-add">{icons['plus']}</span>Add Item</span>
        </div>
    );
};

export default PresetTypos;