const { __ } = wp.i18n;
const { apiFetch, editSite, editPost, editor } = wp
const { Fragment, useState, useEffect } = wp.element;
const { Dropdown } = wp.components
const {useSelect, useDispatch} = wp.data;

import Skeleton from '../../dashboard/utility/Skeleton';
import { handlePostxPresets, postxPresetAttr, saveLocalValue } from '../CommonFunctions';
import Color2 from '../fields/Color2';
import Range from '../fields/Range';
import Select from '../fields/Select';
import Toggle from '../fields/Toggle';
import IconPack from '../fields/tools/IconPack';
import PresetColor from './presets/PresetColor';
import PresetTypos from './presets/PresetTypo';

const icons = {
    editorWidth: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="m46 22-8-7h-4v4l4 3H12l4-3v-4h-4l-8 7-1 3 1 3 8 7 2 1 2-1v-4l-4-3h26l-4 3v4l2 1 2-1 8-7 1-3-1-3zM2 50l-2-2V2l2-2 1 2v46l-1 2zm46 0-1-2V2l1-2 2 2v46l-2 2z"/></svg>,
    editorColor: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M46 10C41 4 33 0 25 0a26 26 0 0 0-10 48s7 3 11 2c2-1 3-2 3-4l-2-5c0-2 1-4 3-4l9-1 6-3c7-6 6-17 1-23zM10 24c-3 0-5-2-5-5s2-5 5-5c2 0 5 3 5 5s-3 5-5 5zm8-10c-2 0-5-2-5-5s3-5 5-5c3 0 5 3 5 5s-2 5-5 5zm14 0c-3 0-5-2-5-5s2-5 5-5 5 3 5 5-2 5-5 5zm9 10c-2 0-5-2-5-5s2-5 5-5 5 3 5 5-2 5-5 5z"/></svg>,
    editorBreak: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M46 0H17c-2 0-4 2-4 4v6h20c4 0 7 3 7 7v20h6c2 0 4-2 4-4V4c0-2-2-4-4-4z"/><path d="M33 14H4c-2 0-4 1-4 3v29c0 2 2 4 4 4h29c2 0 4-2 4-4V17c0-2-2-4-4-4z"/></svg>,
    logo: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 48.3"><path d="M23 2c-3 1-5 3-5 6v9H8c-3 0-6 2-6 5l-2-2V3c0-2 1-3 3-3h17l3 2zm22 6v14H31a7 7 0 0 0-6-5h-3V8l1-2 2-1h17c2 0 3 1 3 3zm5 20v17c0 2-1 3-3 3H30l-3-2c3-1 5-3 5-6V26h17l1 2zm-22-5v17l-1 2-2 1H8c-2 0-3-1-3-3V23c0-2 1-3 3-3h17l1 1 2 1v1z"/></svg>
}

const Accordions = (props) => {
    const {children, title, icon, initOpen} = props
    const [isShow, setShow] = useState(initOpen ? true : false);
    return (
        <div className="ultp-section-accordion">
            <div className="ultp-section-accordion-item ultp-global-accordion-item" onClick={() => setShow(!isShow)}>
                <div className="ultp-global-accordion-title">
                    {icon}
                    {title}
                    {isShow ? <span className="ultp-section-arrow dashicons dashicons-arrow-down-alt2"></span> : <span className= "ultp-section-arrow dashicons dashicons-arrow-up-alt2"></span>}
                </div>
            </div>
            <div className={`ultp-section-show ` + (isShow ? '' : 'is-hide')}>
                { isShow && children}
            </div>
        </div>
    )
}

const SettingTitle = () => {
    return (
        <div>
        <div className="ultp-postx-settings-panel-title">
            <span
                title='Go Back'
                arial-label="Go Back"
                role="button"
                onClick={
                    () => {
                        const prevBlockClientId = localStorage.getItem('ultp_prev_sel_block');
                        if (prevBlockClientId) {
                            wp.data.dispatch( 'core/block-editor' ).selectBlock().then(() => {
                                wp.data.dispatch( 'core/block-editor' ).selectBlock(prevBlockClientId);
                            });
                            localStorage.removeItem('ultp_prev_sel_block');
                        }
                        wp.data.dispatch( 'core/edit-post' ).openGeneralSidebar( 'edit-post/block' );
                    }
                }
            >
                {IconPack["leftArrowLg"]}
            </span>
            <span>
                {__('PostX Settings','ultimate-post')}
            </span>
        </div>
        </div>
    )
} 

/**
 * 
 * @param {Object} props 
 * @returns {JSX.Element}
 */
const PostxSettings = (props) => {

    // Check for Widget
    if ( !( editSite || editPost) ) {
        return null;
    }

    const { isSavingPost, isAutosavingPost, isDirty, isReady } = useSelect( ( select ) => {
        let isReady = false;

        try {
            isReady = select('core/editor').__unstableIsEditorReady() 
        } catch (e) {
            try {
                isReady = select( 'core/editor' ).isCleanNewPost() || select( 'core/block-editor' ).getBlockCount() > 0
            } catch (e) {
            }
        }

        return {
            isSavingPost: select( 'core/editor' ).isSavingPost(),
            isAutosavingPost: select( 'core/editor' ).isAutosavingPost(),
            isDirty: select( 'core/editor' ).isEditedPostDirty(),
            isReady
        }
    } );

    const { editPost: editPostAction } = useDispatch('core/editor');

    // Forcefully making the post dirty when user changes global settings
    function unlockSave() {
        if (!isDirty && isReady) {
            editPostAction({makingPostDirty: ''}, {undoIgnore: true})
        }
    }

    const [stateData, setState] = useState({ 
        gbbodyBackground: { openColor: 1, type: "color", color: "var(--postx_preset_Base_1_color)" },
        enablePresetColorCSS: false,
        enablePresetTypoCSS: false,
        enableDark: false,
        globalCSS: ''
    });

    const [saveNotice, setSaveNotice] = useState(false);
    const [saveLoading, setSaveLoading] = useState(false);
    const [loading, setLoading] = useState(false);
    const [device, setDevice] = useState('lg');
    const { editorType, editorWidth, breakpointSm, breakpointXs, gbbodyBackground, enablePresetColorCSS, enablePresetTypoCSS, enableDark} = stateData;
    const [isnow, setNow] = useState(false);
    
    const [presetColors, setPresetColors] = useState({
        rootCSS: '',
        Base_1_color:     '#f4f4ff', // light:bg
        Base_2_color:     '#dddff8', // light:bg
        Base_3_color:     '#B4B4D6', // light:bg
        Primary_color:    '#3323f0',
        Secondary_color:  '#4a5fff',
        Tertiary_color:   '#FFFFFF',
        Contrast_3_color: '#545472', //light:text
        Contrast_2_color: '#262657', //light:text
        Contrast_1_color: '#10102e', //light:text
        Over_Primary_color: '#ffffff'
    });
    const [presetGradients, setPresetGradients] = useState({
        rootCSS: '',
        Primary_to_Secondary_to_Right_gradient : "linear-gradient(90deg, var(--postx_preset_Primary_color) 0%, var(--postx_preset_Secondary_color) 100%)",
        Primary_to_Secondary_to_Bottom_gradient : "linear-gradient(180deg, var(--postx_preset_Primary_color) 0%, var(--postx_preset_Secondary_color) 100%)",
        Secondary_to_Primary_to_Right_gradient : "linear-gradient(90deg, var(--postx_preset_Secondary_color) 0%, var(--postx_preset_Primary_color) 100%)",
        Secondary_to_Primary_to_Bottom_gradient : "linear-gradient(180deg, var(--postx_preset_Secondary_color) 0%, var(--postx_preset_Primary_color) 100%)",
        Cold_Evening_gradient : "linear-gradient(0deg, rgb(12, 52, 131) 0%, rgb(162, 182, 223) 100%, rgb(107, 140, 206) 100%, rgb(162, 182, 223) 100%)",
        Purple_Division_gradient : "linear-gradient(0deg, rgb(112, 40, 228) 0%, rgb(229, 178, 202) 100%)",
        Over_Sun_gradient : "linear-gradient(60deg, rgb(171, 236, 214) 0%, rgb(251, 237, 150) 100%)",
        Morning_Salad_gradient : "linear-gradient(-255deg, rgb(183, 248, 219) 0%, rgb(80, 167, 194) 100%)",
        Fabled_Sunset_gradient : "linear-gradient(-270deg, rgb(35, 21, 87) 0%, rgb(68, 16, 122) 29%, rgb(255, 19, 97) 67%, rgb(255, 248, 0) 100%)",
    });
    const [presetTypos, setPresetTypos] = useState({
        
        Heading_typo:   { openTypography: 1, transform: "capitalize", family: "Helvetica", weight: 600 },
        Body_and_Others_typo: { openTypography: 1, transform: "lowercase",  family: "Helvetica", weight: 400 },
        
        presetTypoCSS: '',
        body_typo: { openTypography: 1, size: { lg: "16", unit: "px"} },
        paragraph_1_typo: { openTypography: 1, size: { lg: "12", unit: "px"} },
        paragraph_2_typo: { openTypography: 1, size: { lg: "12", unit: "px"} },
        paragraph_3_typo: { openTypography: 1, size: { lg: "12", unit: "px"} },
        
        heading_h1_typo:  { size: { lg: "42", unit: "px"} },
        heading_h2_typo:  { size: { lg: "36", unit: "px"} },
        heading_h3_typo:  { size: { lg: "30", unit: "px"} },
        heading_h4_typo:  { size: { lg: "24", unit: "px"} },
        heading_h5_typo:  { size: { lg: "20", unit: "px"} },
        heading_h6_typo:  { size: { lg: "16", unit: "px"} },
    });

    const _getColor = (color) => {
        return 'rgba('+color.rgb.r+','+color.rgb.g+','+color.rgb.b+','+color.rgb.a+')'
    }


    const setEditor = (data = {}) => {
        let styleCss = '';
        if (data.editorType == 'fullscreen' || data.editorType == 'custom') {
            styleCss = 'body.block-editor-page #editor .wp-block-post-content > .block-editor-block-list__block.wp-block:not(:is(.alignfull, .alignwide), :has( .block-editor-inner-blocks  .block-editor-block-list__block.wp-block)){ max-width: '+(data.editorType == 'fullscreen' ? '100%' : (data.editorWidth || 1200)+'px')+';}';
        }
        if (window.document.getElementById('ultp-block-global') === null) {
            let cssInline = document.createElement('style');
            cssInline.type = 'text/css';
            cssInline.id = 'ultp-block-global';
            if (cssInline.styleSheet) {
                cssInline.styleSheet.cssText = styleCss;
            } else {
                cssInline.innerHTML = styleCss;
            }
            window.document.getElementsByTagName("head")[0].appendChild(cssInline);
        } else {
            window.document.getElementById('ultp-block-global').innerHTML = styleCss;
        }

        unlockSave();
    }

    const fetchOldData = async () => {
        apiFetch({
            path: '/ultp/v1/action_option',
            method: 'POST',
            data: { type: 'get'}
        }).then( res => {
            if (res.success) {
                let newObj = Object.assign({}, stateData, res.data);
                newObj = {...newObj, ['globalCSS']: postxPresetAttr('globalCSS', newObj)}
                setState(newObj);
                setGlobalCss(newObj.globalCSS, 'wpxpo-global-style-inline-css')
                localStorage.setItem('ultpGlobal' + ultp_data.blog, JSON.stringify(newObj));
                setEditor(newObj);
            }
        })
    };

    const fetchPresetColors = async () => {
        setLoading(true);
        handlePostxPresets('get','ultpPresetColors', '', (res) => {
            if(res.data) {
                let newObj = {...presetColors, ...res.data};
                newObj = {...newObj, ['rootCSS']: setGlobalColor(newObj, '')};
                saveLocalValue('ultpPresetColors', newObj);
                setPresetColors(newObj);
                setLoading(false);
                if(res.data.length < 1) {
                    handlePostxPresets('set', 'ultpPresetColors', newObj);
                }
            }
        });
    }
    const fetchPresetGradients = async () => {
        handlePostxPresets('get','ultpPresetGradients', '', (res) => {
            if(res.data) {
                let newObj = {...presetGradients, ...res.data};
                newObj = {...newObj, ['rootCSS']: setGlobalColor(newObj, 'gradient')};
                saveLocalValue('ultpPresetGradients', newObj);
                setPresetGradients(newObj);
                if(res.data.length < 1) {
                    handlePostxPresets('set', 'ultpPresetGradients', newObj);
                }
            }
        });
    }
    const fetchPresetTypos = async () => {
        handlePostxPresets('get','ultpPresetTypos', '', (res) => {
            if(res.data) {
                let newObj = {...presetTypos, ...res.data};
                newObj = {...newObj, ['presetTypoCSS']: setGlobalTypo(newObj)};
                saveLocalValue('ultpPresetTypos', newObj);
                setPresetTypos(newObj);
                if(res.data.length < 1) {
                    handlePostxPresets('set', 'ultpPresetTypos', newObj );
                }
            }
        });
    }

    useEffect(() => {
        fetchOldData();
        fetchPresetColors();
        fetchPresetGradients();
        fetchPresetTypos();
    }, []);

    const typoStacks = postxPresetAttr('typoStacks');
    const presetTypoKeys = postxPresetAttr('presetTypoKeys');
    const colorStacks = postxPresetAttr('colorStacks');
    const presetColorKeys = postxPresetAttr('presetColorKeys');

    const handleChoosePreset = (index) => {
        // for color
        setValue('enableDark', false, true);
        const presentColorObj = {};
        presetColorKeys.forEach((el, i) => {
            presentColorObj[el] = colorStacks[index][i];
        });
        let newColorObj = {...presetColors, ...presentColorObj};
        newColorObj = {...newColorObj, ['rootCSS']: setGlobalColor(newColorObj, '') };
        setPresetColors(newColorObj);

        // for typography
        if(index < typoStacks.length) {
            const presentTypoObj = {};
            presetTypoKeys.forEach((el, i) => {
                presentTypoObj[el] = {...presetTypos[el]};
                presentTypoObj[el]['family'] = typoStacks[index][i]['family'];
                presentTypoObj[el]['type'] = typoStacks[index][i]['type'];
                presentTypoObj[el]['weight'] = typoStacks[index][i]['weight'];
            });
            let newTypoObj = {...presetTypos, ...presentTypoObj};
            newTypoObj = {...newTypoObj, ['presetTypoCSS']: setGlobalTypo(newTypoObj) };
            setPresetTypos(newTypoObj);
        }
    }

    const savePostxSettingsData = () => {

        if ( !saveNotice ) {
            return ;
        }
        setSaveLoading(true);
        handlePostxPresets('set', 'ultpPresetColors', presetColors )
        handlePostxPresets('set', 'ultpPresetGradients', presetGradients )
        handlePostxPresets('set', 'ultpPresetTypos', presetTypos )
        apiFetch({
            method: 'POST',
            path: '/ultp/v1/action_option',
            data: { type: 'set', data: stateData }
        }).then( res => {
            setSaveNotice(false);
            setSaveLoading(false);
        })
    }

    const setValue = (type, value, extra) => {
        setSaveNotice(true);
        if (type.indexOf('presetColor') > -1 && typeof value === 'object') { // For Only Color
            value = _getColor(value)
        }
        let allData = {...stateData, [type]: value};
        if(type == 'enableDark' && !extra) {
            let newObj = { ...presetColors, 
                ['Base_1_color']: presetColors['Contrast_1_color'],
                ['Base_2_color']: presetColors['Contrast_2_color'],
                ['Base_3_color']: presetColors['Contrast_3_color'],
                ['Contrast_1_color']: presetColors['Base_1_color'],
                ['Contrast_2_color']: presetColors['Base_2_color'],
                ['Contrast_3_color']: presetColors['Base_3_color'],
            }
            newObj = {...newObj, ['rootCSS']: setGlobalColor(newObj, '')};
            setPresetColors(newObj);
            if(value) {
                allData = {...allData, ['gbbodyBackground']: {...gbbodyBackground, openColor: 1, type: "color", color: "var(--postx_preset_Base_1_color)"} }
            }
        }
        
        const globalCSS = postxPresetAttr('globalCSS', allData);
        allData = {...allData, ['globalCSS']: globalCSS};
        setState(allData);
        localStorage.setItem('ultpGlobal' + ultp_data.blog, JSON.stringify(allData));

        setGlobalCss(globalCSS, 'wpxpo-global-style-inline-css');
        if (type == 'editorType' || type == 'editorWidth') {
            setEditor(allData);
        }

        unlockSave();
    }


    // Save global settings when user manually saves the post
    useEffect(() => {
        if (isSavingPost && !isAutosavingPost && !saveLoading) {
            savePostxSettingsData();
        }
    }, [isSavingPost, isAutosavingPost, savePostxSettingsData, saveLoading])


    const setGlobalColor = (data = {}, colorType) => {
        const datafallback = colorType == 'gradient' ? '' : '#026fe0';
        const htmlId = colorType == 'gradient' ? 'ultp-preset-gradient-style-inline-css' : 'ultp-preset-colors-style-inline-css';
        const styleCss = postxPresetAttr('styleCss', data, datafallback);
        setGlobalCss(styleCss, htmlId);
        unlockSave();
        return styleCss;
    }
    const setGlobalTypo = (data = {}) => {
        const htmlId = 'ultp-preset-typo-style-inline-css';
        const styleCss = postxPresetAttr('typoCSS', data);
        setGlobalCss(styleCss, htmlId);
        unlockSave();
        return styleCss;
    }

    const setGlobalCss = ( globalCss='{}', htmlId='' ) => {
        if ( window.document.getElementById(htmlId) ) {
            window.document.getElementById(htmlId).innerHTML = globalCss;
        }
        const editor_canvas = window.document.getElementsByName('editor-canvas');
        if ( editor_canvas && editor_canvas[0]?.contentDocument && editor_canvas[0]?.contentDocument.getElementById(htmlId) ) {
            editor_canvas[0].contentDocument.getElementById(htmlId).innerHTML = globalCss;
        }
        unlockSave();
    }

    // const PostXSideBar = editPost ? editPost.PluginSidebar : editSite.PluginSidebar;
    const PostXSideBar = editor ? editor.PluginSidebar : editSite ? editSite.PluginSidebar : null;


    return (
        <Fragment>
            <PostXSideBar
                icon={icons.logo}
                name="postx-settings"
                title={<SettingTitle />}>
                    <div className="ultp-preset-save-wrapper">
                        <button className={`ultp-preset-save-btn ${saveLoading ? ' s_loading' : ''} ${saveNotice ? ' active' : ''}`} onClick={savePostxSettingsData}>Save changes { saveLoading && IconPack['refresh'] }</button>
                    </div>
                    <div className="ultp-preset-options-tab">
                        <style> { postxPresetAttr('font_load_all', '', false) }</style>
                        <Toggle 
                            label={__('Override Theme Style & Color','ultimate-post')}
                            value={enablePresetColorCSS}
                            onChange={v => setValue('enablePresetColorCSS',v)}
                        />
                        <div className={`ultp-editor-dark-key-container ${enableDark ? 'dark-active' : ''}`}>
                            <div className="light-label">Light Mode</div>
                            <Toggle 
                                label={__('Dark Mode','ultimate-post')}
                                value={enableDark}
                                onChange={v => setValue('enableDark', v)}
                            />
                        </div>
                        <div className="ultp-global-current-content seleted" onClick={() => setNow(!isnow)}>
                            <div className="ultp-preset-typo-show" >
                                <span className={``} style={{color: presetColors['Contrast_1_color'], fontFamily: presetTypos['Heading_typo'].family, fontWeight: presetTypos['Heading_typo'].weight }}>A</span>
                                <span className={``} style={{color: presetColors['Contrast_2_color'], fontFamily: presetTypos['Body_and_Others_typo'].family, fontWeight: presetTypos['Body_and_Others_typo'].weight }}>a</span>
                            </div>
                            <div className="ultp-preset-color-show show-settings">
                                {presetColorKeys.map((v, k) => {
                                    return ['Base_3_color', 'Primary_color', 'Secondary_color', 'Contrast_3_color'].includes(v) && ( loading ? <Skeleton key={k} type="custom_size" c_s={{ size1: 20, unit1: 'px',  size2: 20, unit2: 'px', br: 20 }}/> : <span key={k} className={`ultp-global-color`} style={{backgroundColor: presetColors[v]}}/> )
                                })}
                            </div>
                        </div>
                        <div className="ultp-color-field-tab">
                            <Fragment>
                                <Dropdown
                                    open={isnow}
                                    className="ultp-range-control"
                                    popoverProps={ { placement: 'top-start' } }
                                    contentClassName="ultp-editor-preset-color-dropdown components-dropdown__content"
                                    focusOnMount={true}
                                    renderToggle={({ onToggle, onClose }) => (
                                        <div className="preset-choose" onClick={() => setNow(!isnow)} >Choose Style <div>{IconPack['collapse_bottom_line']}</div></div>
                                    )}
                                    renderContent={({ onClose }) => (
                                        <div className="ultp-global-preset-choose-popup">
                                            {
                                                colorStacks.map((item, i) => {
                                                    return <div key={i} className="ultp-global-current-content" onClick={() => { onClose(); handleChoosePreset(i) }} style={{background: item[0]}}>
                                                        <div className="ultp-preset-typo-show" style={{color: item[2]}}>
                                                            {typoStacks[i]?.map((v, k) => {
                                                                return <span key={k}>
                                                                    <span key={k} className={``} style={{ color: item[k+7], fontFamily: v.family, fontWeight: v.weight }}>{k == 0 ? 'A' : 'a'}</span>
                                                                </span>
                                                            })}
                                                        </div>
                                                        <div className="ultp-preset-color-show">
                                                            {item.map((v, k) => {
                                                                return ![1, 2, 6, 8, 9].includes(k+1) && <span key={k} className={`ultp-global-color`} style={{backgroundColor: v}}/>
                                                            })}
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    )}
                                />
                            </Fragment>
                        </div>
                    </div>
                    <div className="ultp-preset-color-tabs">
                        {
                            loading ? <>
                                <div className={`ultp-global-color-label`}>
                                    <div className="ultp-global-color-label-content">Color Palette</div>
                                    <div className="ultp-color-field-tab">
                                        <div className={'active'} >Solid</div>
                                        <div>Gradient</div>
                                    </div>
                                </div>
                                <div className="ultp-global-color-preset">
                                    {Array(9).fill(1).map((val, k) => {
                                        return <Skeleton key={k} type="custom_size" c_s={{ size1: 23, unit1: 'px',  size2: 23, unit2: 'px', br: 23 }}/>
                                    })}
                                </div>
                            </>
                            :
                            <PresetColor
                                presetColors={presetColors}
                                setSaveNotice={setSaveNotice}
                                setPresetColors={setPresetColors}
                                presetGradients={presetGradients}
                                setPresetGradients={setPresetGradients}
                                _getColor={_getColor}
                                setGlobalColor={setGlobalColor}
                            />
                        }
                        <div className="ultp-editor-background-setting-container">
                            <div className="ultp-editor-background-setting-label">Background</div>
                            <div className="ultp-editor-background-setting-content">
                                <div style={postxPresetAttr('bgCSS', gbbodyBackground)} className="ultp-editor-background-demo"></div>
                                <Color2
                                    label={''}
                                    value={gbbodyBackground}
                                    image={true}
                                    video={false}
                                    extraClass={''}
                                    customGradient={[]}
                                    gbbodyBackground={true}
                                    onChange={ v => {
                                        setValue('gbbodyBackground',v)
                                    } }
                                />
                            </div>
                        </div>
                    </div>
                    <Accordions initOpen={true} title={__('Typography','ultimate-post')} >
                        <PresetTypos
                            device={device}
                            setDevice={setDevice}
                            setSaveNotice={setSaveNotice}
                            presetTypos={presetTypos}
                            setPresetTypos={setPresetTypos}
                            setGlobalTypo={setGlobalTypo}
                            enablePresetTypoCSS={enablePresetTypoCSS}
                            setValue={setValue}
                        />
                    </Accordions>

                    <Accordions title={__('Editor Width','ultimate-post')} icon={icons.editorColor}>
                        <Select
                            value={editorType || ''}
                            keys={'editorType'}
                            label={__('Editor Width','ultimate-post')}
                            options={[
                                { label: 'Choose option', value: '' },
                                { label: 'Theme Default', value: 'default' },
                                { label: 'Full Screen', value: 'fullscreen' },
                                { label: 'Custom Size', value: 'custom' },
                            ]}
                            beside={false}
                            onChange={ v => setValue('editorType',v)}
                        />
                        { editorType == 'custom' &&
                            <Range
                                value={editorWidth||1200}
                                min={500}
                                max={3000}
                                placeholder={'1200'}
                                onChange={ v => setValue('editorWidth',v) }
                            />
                        }
                    </Accordions>

                    <Accordions title={__('Breakpoints','ultimate-post')} icon={icons.editorBreak}>
                        <Range
                            value={breakpointSm||991}
                            min={600}
                            max={1200}
                            step={1}
                            label={__('Tablet (Max Width)','ultimate-post')}
                            onChange={ v => setValue('breakpointSm',v) }
                        />
                        <Range
                            value={breakpointXs||767}
                            min={300}
                            max={1000}
                            step={1}
                            label={__('Mobile (Max Width)','ultimate-post')}
                            onChange={ v => setValue('breakpointXs',v) }
                        />
                    </Accordions>
            </PostXSideBar>
        </Fragment>
    );

}
export default PostxSettings;