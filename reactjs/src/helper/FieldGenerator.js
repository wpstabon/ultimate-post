const { __ } = wp.i18n;
const { Fragment, useState, useRef, useEffect } = wp.element;
const {
	TextControl,
	TextareaControl,
	TabPanel,
	Tooltip,
	ToggleControl,
	Notice,
} = wp.components;
import ToolbarDropdown from './Components/ToolbarDropdown';
import { isDCActive } from './dynamic_content';
import { getDCBlockAttributes } from './dynamic_content/attributes';
import Alignment from './fields/Alignment';
import Border from './fields/Border';
import BoxShadow from './fields/BoxShadow';
import Checkbox from './fields/Checkbox';
import Color from './fields/Color';
import Color2 from './fields/Color2';
import Dimension from './fields/Dimension';
import Divider from './fields/Divider';
import Filter from './fields/Filter';
import GroupButton from './fields/GroupButton';
import Icon from './fields/Icon';
import Layout from './fields/Layout';
import Layout2 from './fields/Layout2';
import LinkButton from './fields/LinkButton';
import Media from './fields/Media';
import Number from './fields/Number';
import RadioImage from './fields/RadioImage';
import Range from './fields/Range';
import RowLayout from './fields/RowLayout';
import Search from './fields/Search';
import Select from './fields/Select';
import Separator from './fields/Separator';
import Tag from './fields/Tag';
import Template from './fields/Template';
import TextField from './fields/TextField';
import Toggle from './fields/Toggle';
import Typography from './fields/Typography';
import {
	insertFilterBlock,
	insertPaginationBlock,
	pagiAjaxDisableText,
	removeFilterBlock,
	removePaginationBlock,
	restoreTab,
	saveTab
} from './gridFunctions';
import useDevice from './hooks/use-device';
import icons from './icons';
import UltpLinkGenerator from './UltpLinkGenerator';

const proLink =
	'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=quick-query&utm_campaign=postx-dashboard';

const DEF_TABS = [
	{
		name: 'settings',
		title: 'Settings',
		icon: icons.settings3,
	},
	{
		name: 'style',
		title: 'Style',
		icon: icons.style,
	},
];

const FieldGenerator = (props) => {
	const {
		title,
		initialOpen,
		pro,
		data,
		col,
		store,
		youtube,
		doc,
		depend,
		hrIdx = [],
		isToolbar,
		isTab,
		tabData,
		tabs = DEF_TABS,
		dynamicHelpText,
	} = props;


	const [device, setDevice] = useDevice();

	const [section, setSection] = useState('no-section');
	const [toggle, setToggle] = useState('');
	const [activeTab, setActiveTab] = useState('settings');

    const _isShow = (settings, style, key) => {
        let _show = true
        let _pre = '';
        style.forEach( (selectData,i) => {
            if (selectData?.hasOwnProperty('depends')) {
                let previous = '';
                selectData.depends.forEach( data => {
                    if (data.condition == '==') {
                        if (typeof data.value == 'object') {
                            _show = data.value.includes(settings[data.key])
                        }else{
                            _show = settings[data.key] == data.value
                        }
                    }else if (data.condition == '!=') {
                        if (typeof data.value == 'object' ) {
                            if (Array.isArray(data.value)) {
                                _show = data.value.includes(settings[data.key]) ? false : true
                            } else {
                                _show = settings[data.key].lg ? true : false
                            }
                        } else {
                            _show = settings[data.key] != data.value
                        }
                    } else if (data.condition === "MATCH_VALUE_ONLY") {
                        _show = data.value === settings[data.key];
                    }
                    if (previous == '' && _show == false) {
                        previous = 'no';
                    }
                });
                _show = previous == 'no' ? false : true;
            }
            if (_show == true && _pre == '') {
                _pre = 'yes';
            }
        })
        _show = _pre == 'yes'
        return _show;
    }


	const _setRepeat = (store, parent, id, key, value) => {
		let final = [...store.attributes[parent]];
		final[id][key] = value;
		return final;
	};

	const Tab = (renderCallback) => {
		if (!isTab) return renderCallback();

		return (
			<TabPanel
				onSelect={(tab) => {
					saveTab(title, tab);
					setActiveTab(tab);
				}}
				initialTabName={restoreTab(title)}
				className="ultp-accordion-tab"
				tabs={tabs}
			>
				{(tab) => {
					return renderCallback(null, true, false, false);
				}}
			</TabPanel>
		);
	};

	function getDcId(dcIdx) {
		if (typeof dcIdx == 'number') {
			return store.attributes.dcFields[dcIdx]?.id;
		} else if (typeof dcIdx === 'object') {
			const { groupIdx, fieldIdx } = dcIdx;
			return store.attributes.dcFields[groupIdx].fields[fieldIdx].id;
		}
	}

	function updateDcAttribute(groupIdx, key, value) {
		const id = getDcId(groupIdx);

		const attrKey =
			typeof groupIdx == 'number' ? 'dcGroupStyles' : 'dcFieldStyles';
		const attributes = wp.data
			.select('core/block-editor')
			.getBlockAttributes(store.clientId);

		const newDcStyles = {
			...store.attributes[attrKey],
			[id]: {
				...attributes?.[attrKey]?.default,
				...store.attributes[attrKey][id],
				...(key === null && typeof value === 'object'
					? value
					: { [key]: value }),
			},
		};
		store.setAttributes({
			[attrKey]: newDcStyles,
		});
	}

	const _fieldRender = (
		customData,
		common = true,
		repetable = false,
		ignoreTab = true,
		ignoreHr = false
	) => {
		const initVal = wp.blocks.getBlockType(store.name);

		// Filter content based on currently active tab
		const filteredData = (customData || data).filter((attr) => {
			if (!ignoreTab && !['separator', 'notice'].includes(attr.type)) {
				if (isTab) {
					return (
						(tabData &&
							tabData[activeTab] &&
							tabData[activeTab].includes(attr.key)) ||
						attr.tab === activeTab
					);
				}
			} else {
				return true;
			}
		});

		// Inserting HRs
		if (!ignoreHr) {
			hrIdx.forEach((idx) => {
				if (isTab) {
					if (idx.tab === activeTab) {
						idx['hr'].forEach((hr) => {
							if (hr instanceof Object) {
								if (
									hr['idx'] &&
									hr['idx'] <= filteredData.length
								) {
									filteredData.splice(hr['idx'], 0, {
										type: 'separator',
										label: hr['label'],
									});
								}
							} else {
								if (hr && hr <= filteredData.length) {
									filteredData.splice(hr, 0, {
										type: 'separator',
									});
								}
							}
						});
					}
				} else {
					if (idx <= filteredData.length) {
						filteredData.splice(idx, 0, { type: 'separator' });
					}
				}
			});
		}

		return filteredData.map((attr, k) => {
			if (attr && attr.disableIf && attr.disableIf(store.attributes)) {
				return;
			}

			let _val;
			let _val2 = null;

			if (attr.dcIdx != undefined) {
				const id = getDcId(attr.dcIdx);
				if (
					store.attributes[attr.dcParentKey][id]?.hasOwnProperty(
						attr.key
					)
				) {
					_val = store.attributes[attr.dcParentKey][id][attr.key];
					if (attr.key2)
						_val2 =
							store.attributes[attr.dcParentKey][id][attr.key2];
				} else {
					_val = store.attributes[attr.dcParentKey].default[attr.key];
					if (attr.key2)
						_val2 =
							store.attributes[attr.dcParentKey].default[
								attr.key2
							];
				}
			} else {
				_val = repetable
					? store.attributes[repetable.parent_id][repetable.block_id][
							attr.key
						]
					: store.attributes[attr.key];
				if (attr.key2)
					_val2 = repetable
						? store.attributes[repetable.parent_id][
								repetable.block_id
							][attr.key2]
						: store.attributes[attr.key2];
			}

			if (common) {
				const _data = _val === false ? true : _val;
				if (
					typeof _data != 'undefined' &&
					initVal.attributes[attr.key].style
				) {
					if (
						_isShow(
							store.attributes,
							initVal.attributes[attr.key].style,
							attr.key
						) === false
					) {
						return;
					}
				}
			}
			if (attr.type == 'alignment') {
				return (
					<Alignment
						key={k}
						value={_val}
						label={attr.label}
						alignIcons={attr.icons}
						responsive={attr.responsive}
						disableJustify={attr.disableJustify}
						device={device}
						options={attr.options}
						toolbar={attr.toolbar}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => {
							attr.dcIdx != undefined
								? updateDcAttribute(attr.dcIdx, attr.key, v)
								: store.setAttributes({ [attr.key]: v });
						}}
					/>
				);
			} else if (attr.type == 'border') {
				return (
					<Border
						key={k}
						value={_val}
						label={attr.label}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'boxshadow') {
				return (
					<BoxShadow
						key={k}
						value={_val}
						label={attr.label}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'color') {
				return (
					<Color
						key={k}
						value={_val}
						value2={_val2}
						label={attr.label}
						pro={attr.pro}
						inline={attr.inline}
						onChange={(v) => {
							if (attr.key2) {
								const newVal = Array.isArray(v)
									? {
											...(typeof v[0] !== 'undefined'
												? { [attr.key]: v[0] }
												: {}),
											...(typeof v[1] !== 'undefined'
												? { [attr.key2]: v[1] }
												: {}),
										}
									: {
											[attr.key]: v,
											[attr.key2]: v,
										};

								attr.dcIdx != undefined
									? updateDcAttribute(
											attr.dcIdx,
											null,
											newVal
										)
									: store.setAttributes(newVal);
							} else {
								attr.dcIdx != undefined
									? updateDcAttribute(attr.dcIdx, attr.key, v)
									: store.setAttributes(
											repetable
												? {
														[repetable.parent_id]:
															_setRepeat(
																store,
																repetable.parent_id,
																repetable.block_id,
																attr.key,
																v
															),
													}
												: {
														[attr.key]: v,
													}
										);
							}
						}}
					/>
				);
			} else if (attr.type == 'color2') {
				return (
					<Color2
						key={k}
						value={_val}
						clip={attr.clip}
						label={attr.label}
						pro={attr.pro}
						image={attr.image}
						video={attr.video}
						extraClass={attr.extraClass || ''}
						customGradient={attr.customGradient || []}
						onChange={(v) =>
							store.setAttributes(
								repetable
									? {
											[repetable.parent_id]: _setRepeat(
												store,
												repetable.parent_id,
												repetable.block_id,
												attr.key,
												v
											),
										}
									: { [attr.key]: v }
							)
						}
					/>
				);
			} else if (attr.type == 'divider') {
				return <Divider key={k} label={attr.label} />;
			} else if (attr.type == 'dimension') {
				return (
					<Dimension
						key={k}
						value={_val}
						min={attr.min}
						max={attr.max}
						unit={attr.unit}
						step={attr.step}
						label={attr.label}
						responsive={attr.responsive}
						device={device}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'media') {
				return (
					<Media
						key={k}
						value={_val}
						label={attr.label}
						multiple={attr.multiple}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'radioimage') {
				return (
					<RadioImage
						key={k}
						value={_val}
						label={attr.label}
						isText={attr.isText}
						options={attr.options}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'range') {
				// for compatibility since v.2.5.4
				let _final_val = _val;
				if (attr.key == 'queryNumPosts') {
					if (
						JSON.stringify(store.attributes[attr.key]) ==
						JSON.stringify(initVal.attributes[attr.key].default)
					) {
						const curNumber = store.attributes['queryNumber'];
						const defNumber =
							initVal.attributes['queryNumber'].default;
						if (
							JSON.stringify(curNumber) !=
							JSON.stringify(defNumber)
						) {
							_final_val = {
								lg: curNumber,
								sm: curNumber,
								xs: curNumber,
							};
							store.setAttributes({ queryNumPosts: _final_val });
						}
					}
				}

				return (
					<Range
						key={k}
						_inline={attr._inline}
						metaKey={attr.key}
						value={_final_val}
						min={attr.min}
						max={attr.max}
						step={attr.step}
						unit={attr.unit}
						label={attr.label}
						pro={attr.pro}
						help={attr.help}
						responsive={attr.responsive}
						device={device}
						clientId={attr.clientId || ''}
						updateChild={attr.updateChild || false}
						compact={attr.compact}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => {
							attr.dcIdx != undefined
								? updateDcAttribute(attr.dcIdx, attr.key, v)
								: store.setAttributes({ [attr.key]: v });
						}}
					/>
				);
			} else if (attr.type == 'select') {
				// Taxonomy Set Option Field Start
				let _final_val = _val;

				// for compatibility since v.2.5.4
				if (attr.key == 'queryType') {
					const currentVal = store.attributes['queryInclude'];
					if (
						currentVal &&
						store.attributes['queryCustomPosts'] == '[]' &&
						_val != 'customPosts'
					) {
						store.setAttributes({
							queryType: 'customPosts',
							queryCustomPosts: JSON.stringify(
								currentVal.split(',').map((v) => {
									return { value: v, title: v };
								})
							),
						});
					}
				}

				let data = attr.options;
				if (
					attr.key == 'queryTax' ||
					attr.key == 'queryTaxValue' ||
					attr.key == 'filterType' ||
					attr.key == 'filterValue' ||
					attr.key == 'taxSlug' ||
					attr.key == 'taxValue' ||
					attr.key == 'taxonomy'
				) {
					let category = [];
					let retrievedObject = localStorage.getItem('ultpTaxonomy');
					retrievedObject = JSON.parse(retrievedObject);

					if (
						attr.key == 'queryTax' ||
						attr.key == 'filterType' ||
						attr.key == 'taxonomy'
					) {
						if (
							retrievedObject?.hasOwnProperty(
								store.attributes['queryType']
							)
						) {
							retrievedObject =
								retrievedObject[store.attributes['queryType']];
							category = [{ value: '', label: '- Select -' }];
							if (Object.keys(retrievedObject).length > 0) {
								Object.keys(retrievedObject).forEach(
									function (key) {
										category.push({
											value: key,
											label: key,
										});
									}
								);
								if (attr.key == 'queryTax') {
									category.push({
										value: 'multiTaxonomy',
										label: __(
											'Multiple Taxonomy',
											'ultimate-post'
										),
										pro: true,
										link: proLink,
									});
								}
							}
						}
					} else if (
						attr.key == 'taxSlug' ||
						attr.key == 'taxValue'
					) {
						if (attr.key == 'taxSlug') {
							let temp = [];
							Object.keys(retrievedObject).forEach(
								function (postType) {
									if (Object.keys(postType).length > 0) {
										Object.keys(
											retrievedObject[postType]
										).forEach(function (taxonomy) {
											if (
												temp.includes(taxonomy) ===
												false
											) {
												category.push({
													value: taxonomy,
													label: taxonomy,
												});
												temp.push(taxonomy);
											}
										});
									}
								}
							);
						} else {
							let tax = store.attributes['taxSlug'];
							if (tax) {
								Object.keys(retrievedObject).forEach(
									function (postType) {
										if (
											retrievedObject[
												postType
											].hasOwnProperty(tax)
										) {
											Object.keys(
												retrievedObject[postType][tax]
											).forEach(function (key) {
												category.push({
													value: key,
													label: key,
												});
											});
										}
									}
								);
							}
						}
					} else {
						if (
							retrievedObject?.hasOwnProperty(
								store.attributes['queryType']
							)
						) {
							retrievedObject =
								retrievedObject[store.attributes['queryType']];
							if (Object.keys(retrievedObject).length > 0) {
								const type =
									attr.key == 'queryTaxValue'
										? 'queryTax'
										: 'filterType';

								const allTax =
									retrievedObject[store.attributes[type]];

								if (typeof allTax !== 'undefined') {
									if (!Array.isArray(allTax)) {
										Object.keys(allTax).forEach(
											function (key) {
												category.push({
													value: key,
													label: allTax[key],
												});
											}
										);
									}
								}
							}
						}
					}
					if (category.length > 0) {
						data = category;
					}
				}

				const _condition = repetable
					? ''
					: initVal.attributes[attr.key];

				return (
					<Select
						key={k}
						keys={attr.key}
						value={
							attr.multiple
								? _final_val
									? JSON.parse(
											_final_val.replaceAll('u0022', '"')
										)
									: []
								: _final_val
						}
						label={attr.label}
						clear={attr.clear}
						options={data}
						dynamicHelpText={dynamicHelpText}
						multiple={attr.multiple}
						responsive={attr.responsive}
						pro={attr.pro}
						help={attr.help}
						image={attr.image}
						svg={attr.svg || false}
						svgClass={attr.svgClass || ''}
						condition={_condition?.condition || ''}
						clientId={attr.clientId || ''}
						updateChild={attr.updateChild || false}
						device={device}
						setDevice={(v) => setDevice(v)}
						beside={attr.beside}
						defaultMedia={attr.defaultMedia}
						onChange={(v) => {
							if (typeof _condition.condition == 'undefined') {
								if (attr.multiple) {
									store.setAttributes({
										[attr.key]: JSON.stringify(v),
									});
								} else {
									store.setAttributes(
										repetable
											? {
													[repetable.parent_id]:
														_setRepeat(
															store,
															repetable.parent_id,
															repetable.block_id,
															attr.key,
															v
														),
												}
											: { [attr.key]: v }
									);
								}
								if (attr.key == 'queryTax') {
									if (_val != v) {
										store.setAttributes({
											queryTaxValue: '[]',
										});
									}
								}
								if (attr.key == 'filterType') {
									if (_val != v) {
										store.setAttributes({
											filterValue: '[]',
										});
									}
								}
								if (attr.key == 'taxSlug') {
									if (_val != v) {
										store.setAttributes({ taxValue: '[]' });
									}
								}
							} else {
								store.setAttributes(v);
							}
						}}
					/>
				);
			} else if (attr.type == 'tag') {
				return (
					<Tag
						key={k}
						value={_val}
						layoutCls={attr.layoutCls}
						label={attr.label}
						options={attr.options}
						disabled={attr.disabled}
						inline={attr.inline}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'group') {
				return (
					<GroupButton
						key={k}
						value={_val}
						label={attr.label}
						options={attr.options}
						disabled={attr.disabled}
						justify={attr.justify}
						inline={attr.inline}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'text') {
				return (
					<TextField
						key={k}
						value={_val.replaceAll('&amp;', '&')}
						isTextField={true}
						attr={attr}
						DC={attr.DC ?? null}
						onChange={(v) =>
							store.setAttributes(
								repetable
									? {
											[repetable.parent_id]: _setRepeat(
												store,
												repetable.parent_id,
												repetable.block_id,
												attr.key,
												v
											),
										}
									: { [attr.key]: v }
							)
						}
					/>
				);
			} else if (attr.type == 'number') {
				return (
					<Number
						key={k}
						value={_val}
						min={attr.min}
						max={attr.max}
						unit={attr.unit}
						step={attr.step}
						label={attr.label}
						responsive={attr.responsive}
						device={device}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'textarea') {
				return (
					<TextField
						key={k}
						value={_val}
						isTextField={false}
						attr={attr}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'toggle') {
				let _disabled = attr.disabled;
				let _help = attr.help;
				let _valueToShowWhenDisabled = attr.showDisabledValue;
				if (
					store.attributes['advFilterEnable'] &&
					attr.key === 'paginationAjax'
				) {
					_disabled = true;
					_valueToShowWhenDisabled = _val;
					_help = pagiAjaxDisableText;
				}

				return (
					<Toggle
						key={k}
						value={_val}
						label={attr.label}
						pro={attr.pro}
						disabled={_disabled}
						showDisabledValue={_valueToShowWhenDisabled}
						clientId={attr.clientId || ''}
						updateChild={attr.updateChild || false}
						help={_help}
						onChange={(v) => {
							attr.dcIdx != undefined
								? updateDcAttribute(attr.dcIdx, attr.key, v)
								: store.setAttributes(
										repetable
											? {
													[repetable.parent_id]:
														_setRepeat(
															store,
															repetable.parent_id,
															repetable.block_id,
															attr.key,
															v
														),
												}
											: { [attr.key]: v }
									);
						}}
					/>
				);
			} else if (attr.type == 'typography') {
				return (
					<Typography
						key={k}
						value={_val}
						label={attr.label}
						device={device}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => {
							attr.dcIdx != undefined
								? updateDcAttribute(attr.dcIdx, attr.key, v)
								: store.setAttributes({ [attr.key]: v });
						}}
					/>
				);
			} else if (attr.type == 'typography_toolbar') {
				return (
					<Typography
						isToolbar={true}
						key={k}
						value={_val}
						label={attr.label}
						device={device}
						setDevice={(v) => setDevice(v)}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'tab') {
				return (
					<TabPanel
						key={k}
						tabs={attr.content}
						activeClass="active-tab"
						className="ultp-field-wrap ultp-inspect-tabs ultp-hover-tabs"
					>
						{(tab) => (
							<Fragment>
								{_fieldRender(
									tab.options,
									true,
									false,
									true,
									true
								)}
							</Fragment>
						)}
					</TabPanel>
				);
			} else if (attr.type == 'tab_toolbar') {
				return (
					<TabPanel
						key={k}
						tabs={attr.content}
						className="ultp-toolbar-tab"
					>
						{(tab) => (
							<Fragment>
								{_fieldRender(
									tab.options,
									true,
									false,
									true,
									true
								)}
							</Fragment>
						)}
					</TabPanel>
				);
			} else if (attr.type == 'toolbar_dropdown') {
				return (
					<ToolbarDropdown
						key={k}
						options={attr.options}
						value={_val}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
						label={attr.label}
					/>
				);
			} else if (attr.type == 'checkbox') {
				return (
					<Checkbox
						key={k}
						value={JSON.parse(_val)}
						label={attr.label}
						options={attr.options}
						onChange={(v) =>
							store.setAttributes({
								[attr.key]: JSON.stringify(v),
							})
						}
					/>
				);
			} else if (attr.type == 'separator') {
				return (
					<Separator key={k} label={attr.label} fancy={attr.fancy} />
				);
			} else if (attr.type == 'linkbutton') {
				const { dcEnabled, dc } = store.attributes;

				return (
					<LinkButton
						key={k}
						onlyLink={attr.onlyLink}
						value={_val}
						text={attr.text}
						label={attr.label}
						store={store}
						placeholder={attr.placeholder}
						extraBtnAttr={attr.extraBtnAttr}
						disableLink={
							isDCActive() && dcEnabled && dc.linkEnabled
						}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'template') {
				return <Template key={k} label={attr.label} store={store} />;
			} else if (attr.type == 'layout') {
				return (
					<Layout
						key={k}
						value={_val}
						label={attr.label}
						col={col}
						pro={attr.pro}
						tab={attr.tab}
						block={attr.block}
						options={attr.options}
						onChange={(v) => {
							if (attr.variation && v.layout) {
								let _obj = {};
								Object.keys(initVal.attributes).forEach(
									(val, key) => {
										// Dynamic Content attributes remain unchanged
										if (
											Object.keys(
												getDCBlockAttributes({})
											).includes(val)
										) {
											return;
										}
										if (val != 'blockPubDate' && initVal.attributes[val].default) {
											_obj[val] = initVal.attributes[val].default;
										} else {
											_obj['blockPubDate'] = store?.attributes?.blockPubDate; 
										}
									}
								);
								store.setAttributes(
									attr.variation?.hasOwnProperty(v.layout)
										? Object.assign(
												{},
												_obj,
												{ [attr.key]: v.layout },
												attr.variation[v.layout]
											)
										: { [attr.key]: v.layout }
								);
							} else {
								store.setAttributes(v);
							}
						}}
					/>
				);
			} else if (attr.type == 'layout2') {
				return (
					<Layout2
						key={k}
						value={_val}
						label={attr.label}
						isInline={attr.isInline}
						pro={attr.pro}
						tab={attr.tab}
						block={attr.block}
						options={attr.options}
						onChange={(v) => {
							if (attr.variation && v.layout) {
								let _obj = {};
								Object.keys(initVal.attributes).forEach(
									(val, key) => {
										// Dynamic Content attributes remain unchanged
										if (
											Object.keys(
												getDCBlockAttributes({})
											).includes(val)
										) {
											return;
										}

										if (initVal.attributes[val].default) {
											_obj[val] =
												initVal.attributes[val].default;
										}
									}
								);
								store.setAttributes(
									attr.variation?.hasOwnProperty(v.layout)
										? Object.assign(
												{},
												_obj,
												{ [attr.key]: v.layout },
												attr.variation[v.layout]
											)
										: { [attr.key]: v.layout }
								);
							} else {
								store.setAttributes(v);
							}
						}}
					/>
				);
			} else if (attr.type == 'rowlayout') {
				return (
					<RowLayout
						key={k}
						value={_val}
						label={attr.label}
						block={attr.block}
						device={device}
						setDevice={(v) => setDevice(v)}
						responsive={attr.responsive}
						clientId={attr.clientId}
						layout={attr.layout}
						onChange={(v) => {
							store.setAttributes({ [attr.key]: v });
						}}
					/>
				);
			} else if (attr.type === 'dynamicContent') {
				return _fieldRender(attr.fields, false);
			} else if (attr.type == 'repetable') {
				let _finalVal = [..._val];

				let dragElem = {};
				const dragStart = (index) => {
					dragElem.start = index;
				};
				const dragEnter = (index) => {
					dragElem.stop = index;
				};
				const dropElement = () => {
					const temp = _finalVal[dragElem.start];
					_finalVal.splice(dragElem.start, 1);
					_finalVal.splice(dragElem.stop, 0, temp);
					store.setAttributes({ [attr.key]: _finalVal });
				};
				return (
					<div key={k} className={`ultp-field-wrap ultp-field-sort`}>
						{attr.label && <label>{attr.label}</label>}
						{_val.map((_v, _k) => {
							const uniqueKey = _k + 1;
							return (
								<div
									key={_k}
									className={`ultp-sort-items ${toggle == uniqueKey && 'active'}`}
									draggable={
										toggle == uniqueKey ? false : true
									}
									onDragStart={() => dragStart(_k)}
									onDragEnter={() => dragEnter(_k)}
									onDragEnd={() => dropElement()}
								>
									<div className="short-field-wrapper">
										<div className="short-field-wrapper__control">
											<span className="dashicons dashicons-move" />
										</div>
										<div
											className="short-field-label"
											onClick={() => {
												setToggle(
													toggle == uniqueKey
														? ''
														: uniqueKey
												);
											}}
										>
											<div className="short-field-wrapper__inside">
												<div>{_v.type}</div>
											</div>
											<span
												className={`ultp-short-collapse ${toggle == uniqueKey && 'active'}`}
												onClick={() => {
													setToggle(
														toggle == uniqueKey
															? ''
															: uniqueKey
													);
												}}
												type="button"
											/>
										</div>
										<span
											onClick={() => {
												_finalVal.splice(_k, 1);
												store.setAttributes({
													[attr.key]: _finalVal,
												});
											}}
											className="dashicons dashicons-no-alt ultp-sort-close"
										></span>
									</div>
									<div
										className={`ultp-short-content ${toggle == uniqueKey && 'active'}`}
									>
										{_fieldRender(attr.fields, false, {
											parent_id: attr.key,
											block_id: _k,
										})}
									</div>
								</div>
							);
						})}
						<button
							className="ultp-sort-btn"
							onClick={() => {
								let temp = {};
								Object.keys(
									initVal.attributes[attr.key].fields
								).forEach((el) => {
									temp[el] =
										initVal.attributes[attr.key].fields[
											el
										].default;
								});
								_finalVal.push(temp);
								store.setAttributes({ [attr.key]: _finalVal });
							}}
						>
							<span className="dashicons dashicons-plus-alt2"></span>{' '}
							Add New Fields
						</button>
					</div>
				);
			} else if (attr.type == 'sort') {
				let _finalSort = [..._val];

				let dragElem = {};
				const sortStart = (pos) => {
					dragElem.start = pos;
				};
				const sortEnter = (pos) => {
					dragElem.stop = pos;
				};
				const dropSort = () => {
					const temp = _finalSort[dragElem.start];
					_finalSort.splice(dragElem.start, 1);
					_finalSort.splice(dragElem.stop, 0, temp);
					store.setAttributes({ [attr.key]: _finalSort });
				};

				return (
					<div key={k} className={`ultp-field-wrap ultp-field-sort`}>
						{attr.label && <label>{attr.label}</label>}
						{_val.map((data, ky) => {
							const uniqueKey = ky + 1;
							return (
								<div
									key={ky}
									onDragStart={() => sortStart(ky)}
									onDragEnter={() => sortEnter(ky)}
									onDragEnd={() => dropSort()}
									draggable={
										toggle == uniqueKey ? false : true
									}
									className={`ultp-sort-items ${toggle == uniqueKey && 'active'}`}
								>
									<div className="short-field-wrapper">
										<div className="short-field-wrapper__control">
											<span className="dashicons dashicons-move" />
										</div>
										<div
											className="short-field-label"
											onClick={() => {
												setToggle(
													toggle == uniqueKey
														? ''
														: uniqueKey
												);
											}}
										>
											<div className="short-field-wrapper__inside">
												<div>
													{attr.options[data].label}
												</div>
											</div>
											<span
												className={`ultp-short-collapse ${toggle == uniqueKey && 'active'}`}
												onClick={() => {
													setToggle(
														toggle == uniqueKey
															? ''
															: uniqueKey
													);
												}}
												type="button"
											/>
										</div>
										{attr.options[data].action && (
											<span
												onClick={() => {
													store.setAttributes({
														[attr.options[data]
															.action]:
															!store.attributes[
																attr.options[
																	data
																].action
															],
													});
												}}
												className={`dashicons ultp-sort-close dashicons-${store.attributes[attr.options[data].action] ? 'visibility' : 'hidden'}`}
											/>
										)}
									</div>
									<div
										className={`ultp-short-content ${toggle == uniqueKey && 'active'}`}
									>
										{_fieldRender(attr.options[data].inner)}
									</div>
								</div>
							);
						})}
					</div>
				);
			} else if (attr.type == 'search') {
				let _value = [];
				if (_val) {
					if (_val.indexOf('[{') == 0) {
						_value = JSON.parse(_val.replaceAll('u0022', '"'));
					} else if (_val.indexOf('[') == 0) {
						JSON.parse(_val).forEach((v) => {
							_value.push({ value: v, title: v });
						});
					} else {
						_val.split(',').forEach((v) => {
							_value.push({ value: v, title: v });
						});
					}
				}

				let _pro = attr.pro;
				if (attr.key == 'queryPosts') {
					if (store.attributes['queryType'] == 'posts') {
						_pro = store.attributes['queryInclude']
							? false
							: attr.pro;
					}
				}

				let condition = '';
				switch (attr.key) {
					case 'filterValue':
						condition = '###' + store.attributes['filterType'];
						break;
					case 'queryTaxValue':
						condition =
							store.attributes['queryType'] +
							'###' +
							store.attributes['queryTax'];
						break;
					case 'queryExclude':
					case 'queryExcludeTerm':
						condition = store.attributes['queryType'];
						break;
					default:
						break;
				}

				return (
					<Search
						key={k}
						value={_value}
						pro={_pro}
						label={attr.label}
						search={attr.search}
						condition={condition}
						onChange={(v) =>
							store.setAttributes({
								[attr.key]: JSON.stringify(v),
							})
						}
					/>
				);
			} else if (attr.type == 'filter') {
				return (
					<Filter
						key={k}
						value={_val}
						label={attr.label}
						onChange={(v) => store.setAttributes({ [attr.key]: v })}
					/>
				);
			} else if (attr.type == 'icon') {
				return (
					<Icon
						key={k}
						value={_val}
						label={attr.label}
						isSocial={attr.isSocial}
						selection={attr.selection}
						hideFilter={attr.hideFilter}
						dynamicClass={attr.dynamicClass}
						onChange={(v) =>
							store.setAttributes(
								repetable
									? {
											[repetable.parent_id]: _setRepeat(
												store,
												repetable.parent_id,
												repetable.block_id,
												attr.key,
												v
											),
										}
									: { [attr.key]: v }
							)
						}
					/>
				);
			} else if (attr.type === 'advFilterEnable') {
				return (
					<Toggle
						key={k}
						value={_val}
						onChange={(v) => {
							if (v) {
								store.setAttributes({
									[attr.key]: true,
									filterShow: false,
									paginationAjax: true,
								});
								insertFilterBlock(
									store.clientId,
									store.context[
										'post-grid-parent/postBlockClientId'
									]
								);
							} else {
								store.setAttributes({
									[attr.key]: false,
								});
								removeFilterBlock(store.clientId);
							}
						}}
						label={'Enable Advanced Filter'}
					/>
				);
			} else if (attr.type === 'advPagiEnable') {
				return (
					<Toggle
						key={k}
						value={_val}
						onChange={(v) => {
							if (v) {
								store.setAttributes({
									[attr.key]: true,
									paginationShow: false,
								});
								insertPaginationBlock(
									store.clientId,
									store.context[
										'post-grid-parent/postBlockClientId'
									]
								);
							} else {
								store.setAttributes({
									[attr.key]: false,
								});
								removePaginationBlock(store.clientId);
							}
						}}
						label={'Enable Advanced Pagination'}
					/>
				);
			} else if (attr.type === 'notice') {
				return (
					<Notice
						status={attr.status}
						isDismissible={attr.isDismissible || false}
					>
						{attr.text}
					</Notice>
				);
			}
		});
	};

	const shouldRender =
		section == title ||
		title === 'inline' ||
		(section == 'no-section' && initialOpen);

	const id = title?.replace(/ /g, '-').toLowerCase();

	return (
		<Fragment>
			{title ? (
				<div
					className="ultp-section-accordion"
					id={'ultp-sidebar-' + id}
				>
					{title != 'inline' ? (
						<Fragment>
							<div
								className="ultp-section-accordion-item ultp-section-control"
								onClick={() =>
									setSection(
										section == title ||
											(section == 'no-section' &&
												initialOpen)
											? ''
											: title
									)
								}
							>
								<span className="ultp-section-control__help">
									{title}
									{youtube && (
										<Tooltip
											position={'top'}
											text={'Video Tutorials'}
										>
											<a
												onClick={(e) =>
													e.stopPropagation()
												}
												href={youtube}
												target="_blank"
											>
												<span className="dashicons dashicons-youtube" />
											</a>
										</Tooltip>
									)}
									{doc && (
										<Tooltip
											position={'top'}
											text={'Documentation'}
										>
											<a
												onClick={(e) =>
													e.stopPropagation()
												}
												href={doc}
												target="_blank"
											>
												<span className="ultp-section-control__docs dashicons dashicons-format-aside" />
											</a>
										</Tooltip>
									)}
								</span>
								{depend && (
									// <button onClick={(e) => {e.stopPropagation(); store.setAttributes( { [depend]: !store.attributes[depend] } );}}>Show</button>
									<span onClick={(e) => e.stopPropagation()}>
										<ToggleControl
											__nextHasNoMarginBottom={true}
											className="ultp-section-control__toggle"
											checked={store.attributes[depend]}
											onChange={() =>
												store.setAttributes({
													[depend]:
														!store.attributes[
															depend
														],
												})
											}
										/>
									</span>
								)}
								{section == title ||
								(section == 'no-section' && initialOpen) ? (
									<span className="ultp-section-arrow dashicons dashicons-arrow-up-alt2"></span>
								) : (
									<span className="ultp-section-arrow dashicons dashicons-arrow-down-alt2"></span>
								)}
							</div>
							<div
								className={
									`${isToolbar ? 'ultp-toolbar-section-show' : ''} ultp-section-show` +
									(section == title ||
									(section == 'no-section' && initialOpen)
										? ''
										: ' is-hide')
								}
							>
								{pro && !ultp_data.active ? (
									<div className="ultp-pro-total">
										<div className="ultp-section-pro-message">
											<span>
												{__(
													'Unlock It! Explore More',
													'ultimate-post'
												)}{' '}
												<a
													href={UltpLinkGenerator(
														'https://www.wpxpo.com/postx/all-features/',
														'blockProFeat',
														ultp_data.affiliate_id
													)}
													target="_blank"
												>
													{__(
														'Pro Features',
														'ultimate-post'
													)}
												</a>
											</span>
										</div>
										{shouldRender && Tab(_fieldRender)}
									</div>
								) : (
									shouldRender && Tab(_fieldRender)
								)}
							</div>
						</Fragment>
					) : (
						<div
							className={`ultp-section-show ${isToolbar ? 'ultp-toolbar-section-show' : ''}`}
						>
							{shouldRender && Tab(_fieldRender)}
						</div>
					)}
				</div>
			) : (
				<Fragment>
					{data ? _fieldRender(data /*, false*/) : _fieldRender()}
				</Fragment>
			)}
		</Fragment>
	);
};
export default FieldGenerator;
