import icons from "./icons";
import ChatGPT from "./ChatGPT";
const { BlockControls } = wp.blockEditor;
const { ToolbarButton } = wp.components;
const { render } = wp.element
const { registerFormatType } = wp.richText;

const ChatGptToolbar = props => {
    return (
        <>
            <BlockControls>
                <ToolbarButton
                    icon={icons.chatgpt}
                    label="ChatGPT"
                    className="components-toolbar"
                    onClick={() => {
                        let node = document.createElement('div')
                        node.className = "ultp-builder-modal ultp-blocks-layouts"
                        document.body.appendChild(node)
                        render(
                            <ChatGPT
                                isShow={true}
                                value={props.value}
                                onChange={v => props.onChange(v)}
                                fromEditPostToolbar={false}
                            />
                        , node)
                        document.body.classList.add('ultp-popup-open')
                    }}
                />
            </BlockControls>
        </>
    );
};

if (ultp_data.settings['ultp_chatgpt'] == 'true') {
    registerFormatType(
        'ultimate-post/chatgpt', {
            title: 'ChatGPT',
            tagName: 'span',
            className: 'ultp-chatgpt',
            edit: ChatGptToolbar
        }
    )
}