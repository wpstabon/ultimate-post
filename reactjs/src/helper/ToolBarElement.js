const {__} = wp.i18n
const { useState } = wp.element
const { BlockControls } = wp.blockEditor;
const { Modal,Toolbar, Dropdown, ToolbarButton } = wp.components;
import IconPack from './fields/tools/IconPack'
import LinkButton from './fields/LinkButton'
const loopSVG = <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 108"><path d="M179 54a33 33 0 0 1-65 8l-8-24a54 54 0 1 0-32 66l-8-19a33 33 0 1 1 20-39l8 22a54 54 0 1 0 32-64l8 19a33 33 0 0 1 45 31Z"/></svg>

import { QueryContent, Layout, TaxQuery, AdvFilterArgs, CommonSettings, UltpToolbarDropdown, GridAlignment, getGridSpacingToolbarSettings, spaceingIcon, FeatureToggleStyle } from '../helper/CommonPanel'
import DesignTemplate from './DesignTemplate';
import ExtraToolbarSettings from './Components/ExtraToolbarSettings';
import icons from './icons';

const ToolBarElement = (props) => {
    const { include, store  /*, store:{setAttributes}, */} = props;
    const [ isOpen, setOpen ] = useState( false );
    const TemplateModal = () => setOpen( true );
    const closeModal = () => setOpen( false );
    
    return (
        <BlockControls>
                { include.map( (e, k) => {
                    if (e.type == 'template') {
                        return (
                            <Toolbar key={k}>
                                <ToolbarButton className="ultp-toolbar-template__btn" onClick={ TemplateModal } label={'Patterns'}>
                                <span className="dashicons dashicons-images-alt2"></span>
                                </ToolbarButton>
                                { isOpen &&
                                    <Modal isFullScreen onRequestClose={ closeModal }>
                                        <DesignTemplate store={store} closeModal={ closeModal }/>
                                    </Modal>
                                }
                            </Toolbar>
                        )
                    } else if (e.type == 'layout') {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-layout-toolbar"
                                    renderToggle={({ onToggle }) => (
                                        <ToolbarButton
                                            label={'Layout'}
                                            onClick={() => onToggle()} >
                                            <span className="dashicons dashicons-schedule"/>
                                        </ToolbarButton>
                                    )}
                                    renderContent={() => (
                                        <div className={`ultp-field-wrap`}>
                                            <Layout 
                                                title ={'inline'}
                                                store={store}
                                                block={e.block}
                                                options={e.options}
                                                col={e.col}
                                                data={e} />
                                        </div>
                                    )}
                                />
                            </Toolbar>
                        )

                    } else if (e.type == 'layout+adv_style') {
                        const {
                            layoutData,
                            advStyleData
                        } = e;
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-layout-toolbar"
                                    renderToggle={({ onToggle }) => (
                                        <ToolbarButton
                                            label={'Layout & Advance Style'}
                                            onClick={() => onToggle()} >
                                            <span className="dashicons dashicons-schedule"/>
                                        </ToolbarButton>
                                    )}
                                    renderContent={() => (
                                        <div className={`ultp-field-wrap`}>
                                            <Layout 
                                                title ={"inline"}
                                                store={store}
                                                block={layoutData.block}
                                                col={layoutData.col}
                                                data={layoutData}
                                            />
                                            <Layout 
                                                title ={"inline"}
                                                store={store}
                                                block={advStyleData.block}
                                                col={advStyleData.col}
                                                data={advStyleData}
                                            />
                                        </div>
                                    )}
                                />
                            </Toolbar>
                        )

                    } else if (e.type == 'query') {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-query-toolbar"
                                    renderToggle={({ onToggle }) => (
                                        <span className={`ultp-query-toolbar__btn`}>
                                            <ToolbarButton
                                                label={'Query'}
                                                icon={loopSVG}
                                                onClick={() => onToggle()} />
                                        </span>
                                    )}
                                    renderContent={() => (
                                        <div className={`ultp-field-wrap`}>
                                            { e.taxQuery ? 
                                                <TaxQuery title={'inline'} store={store} /> 
                                                : 
                                                <QueryContent dynamicHelpText={true} exclude={e.exclude} title={'inline'} store={store}/>
                                            }
                                        </div>
                                    )}
                                />
                            </Toolbar>
                        )
                    } 
                    else if (e.type === "grid_align") {
                        return (
                            <Toolbar key={k}>
                                <UltpToolbarDropdown 
                                    store={store}
                                    attrKey={e.key}
                                    options={GridAlignment}
                                    label={__('Grid Content Alignment','ultimate-post')}
                                />
                            </Toolbar>
                        );
                    }
                    else if (e.type === "dropdown") {
                        return (
                            <Toolbar key={k}>
                                <UltpToolbarDropdown 
                                    store={store}
                                    attrKey={e.key}
                                    options={e.options}
                                    label={e.label}
                                />
                            </Toolbar>
                        );
                    }
                    else if (e.type === "feat_toggle") {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    contentClassName="ultp-custom-toolbar-wrapper"
                                    renderToggle={({ onToggle }) => (
                                        <span>
                                            <ToolbarButton
                                                label={e.label || 'Features'}
                                                icon={e.icon || icons.setting}
                                                onClick={() => onToggle()} 
                                            />
                                        </span>
                                    )}
                                    renderContent={() => (
                                        <FeatureToggleStyle 
                                            exclude={e.exclude}
                                            include={e.include}
                                            label={e.label}
                                            store={store}
                                            data={e.data}
                                            new={e.new}
                                            dep={e.dep}
                                            pro={e.pro}
                                        />
                                    )}
                                />
                            </Toolbar>
                        );
                    }
                    else if (e.type === "grid_spacing") {
                        return (
                            <Toolbar key={k}>
                                <ExtraToolbarSettings 
                                    buttonContent={spaceingIcon}
                                    include={
                                        getGridSpacingToolbarSettings(
                                            {
                                                include: e.include,
                                                exclude: e.exclude
                                            }
                                        )
                                    }
                                    store={store}
                                    label={__('Grid Spacing','ultimate-post')}
                                />
                            </Toolbar>
                        );
                    }
                    else if (e.type == 'linkbutton') {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-link-toolbar"
                                    renderToggle={({ onToggle }) => (
                                        <ToolbarButton
                                            name="link"
                                            label={'Link'}
                                            icon={ IconPack.link }
                                            onClick={() => onToggle()} >
                                        </ToolbarButton>
                                    )}
                                    renderContent={() => (
                                        <LinkButton 
                                            // key={k}
                                            onlyLink={e.onlyLink}
                                            value={e.value}
                                            text={e?.text} 
                                            label={e.label} 
                                            store={store}
                                            placeholder={e.placeholder}
                                            onChange={ v => store.setAttributes( { [e.key]: v } ) }/>
                                    )}
                                />
                            </Toolbar>
                        )
                    } 
                    else if (e.type === "adv_filter") {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-query-toolbar"
                                    renderToggle={({ onToggle }) => (
                                        <span className={`ultp-query-toolbar__btn`}>
                                            <ToolbarButton
                                                label={__("Settings", "ultimate-post")}
                                                icon={icons.setting}
                                                onClick={() => onToggle()} />
                                        </span>
                                    )}
                                    renderContent={() => (
                                        <div className={`ultp-field-wrap`}>
                                            <CommonSettings
                                                include={AdvFilterArgs}
                                                store={store}
                                                initialOpen={true}
                                            />
                                        </div>
                                    )}
                                />
                            </Toolbar>
                        );
                    } else if (e.type === "inserter") {
                        return (
                            <Toolbar key={k}>
                                <ToolbarButton 
                                    icon={icons.add} 
                                    label={e.label || __("Insert Blocks", 'ultimate-post')}
                                    onClick={ () => {
                                        if( wp.data.dispatch('core/editor').setIsInserterOpened != null ) {
                                            wp.data.dispatch('core/editor').setIsInserterOpened({rootClientId: e.clientId, insertionIndex: 99 });
                                        } else if(wp.data.dispatch('core/edit-post').setIsInserterOpened != null) {
                                            wp.data.dispatch('core/edit-post').setIsInserterOpened({rootClientId: e.clientId, insertionIndex: 99 });
                                        } else {
                                            return false;
                                        }
                                    } }
                                />
                            </Toolbar>
                        );
                    } else if (e.type === "custom") {
                        return (
                            <Toolbar key={k}>
                                <Dropdown
                                    key={k}
                                    contentClassName="ultp-custom-toolbar-wrapper"
                                    renderToggle={({ onToggle }) => (
                                        <span 
                                        >
                                            <ToolbarButton
                                                label={e.label || "Settings"}
                                                icon={e.icon || IconPack["cog_line"]}
                                                onClick={() => onToggle()} />
                                        </span>
                                    )}
                                    renderContent={() => props.children}
                                />
                            </Toolbar>
                        );
                    }
                })}
        </BlockControls>
    )
}

export default ToolBarElement;