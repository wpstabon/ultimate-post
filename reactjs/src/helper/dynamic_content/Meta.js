const { __ } = wp.i18n;
import { ULTP_STORE } from '../../store';
import IconPack from '../fields/tools/IconPack';
import useClickToggle from '../hooks/use-click-toggle';
import {
	ACF_FIELD_PREFIX,
	AUTHOR_PREFIX,
	CUSTOM_META_PREFIX,
	METABOX_FIELD_PREFIX,
	PODS_FIELD_PREFIX,
	POST_PREFIX,
	SITE_PREFIX
} from './utils';
const { useEffect, useState, useRef } = wp.element;

const { useSelect } = wp.data;

export default function Meta({
	field,
	postId,
	settingsOnClick,
	resetOnboarding,
	groupIdx,
	idx,
}) {
	const { text, hasResolved } = useSelect((select) => {
		let text = '';
		let method = '';
		let args = [];

		const pId =
			field.dataSrc === 'post_type' ? field.postId : String(postId);

		if (field.contentSrc.startsWith(POST_PREFIX)) {
			text = select(ULTP_STORE).getPostInfo(
				pId,
				field.contentSrc.slice(POST_PREFIX.length)
			);
			method = 'getPostInfo';
			args = [pId, field.contentSrc.slice(POST_PREFIX.length)];
		}

		if (field.contentSrc.startsWith(AUTHOR_PREFIX)) {
			text = select(ULTP_STORE).getAuthorInfo(
				pId,
				field.contentSrc.slice(AUTHOR_PREFIX.length)
			);
			method = 'getAuthorInfo';
			args = [pId, field.contentSrc.slice(AUTHOR_PREFIX.length)];
		}

		if (
			field.contentSrc.startsWith(ACF_FIELD_PREFIX) ||
			field.contentSrc.startsWith(CUSTOM_META_PREFIX) ||
			field.contentSrc.startsWith(PODS_FIELD_PREFIX) ||
			field.contentSrc.startsWith(METABOX_FIELD_PREFIX)
		) {
			text = select(ULTP_STORE).getCFValue(pId, field.contentSrc);
			method = 'getCFValue';
			args = [pId, field.contentSrc];
		}

		if (field.dataSrc.startsWith(SITE_PREFIX)) {
			text = select(ULTP_STORE).getSiteInfo(
				field.dataSrc.slice(SITE_PREFIX.length)
			);
			method = 'getSiteInfo';
			args = [field.dataSrc.slice(SITE_PREFIX.length)];
		}

		// if (field.contentSrc.startsWith(LINK_PREFIX)) {
		//   text =
		//     select(ULTP_STORE)
		//     .getLink(postId, field.contentSrc);
		// }

		// if (field.linkEnabled && field.linkSrc) {
		// 	text = select(ULTP_STORE).getLink(pId, field.linkSrc);
		// 	method = 'getLink';
		// 	args = [pId, field.linkSrc];
		// }

		if (['string', 'number', 'boolean', 'bigint'].includes(typeof text)) {
			text = String(text);

			if (field.maxCharLen) {
				text = text.split(' ').slice(0, +field.maxCharLen).join(' ');
			}
		} else {
			console.log('Invalid Data Type: ', text);
			text = null;
		}

		if (!text) {
			text = field.fallback || '[EMPTY]';
		}

		const hasResolved =
			field.dataSrc !== ''
				? select(ULTP_STORE).hasFinishedResolution(method, args)
				: false;

		return {
			text: (
				<>
					{field.beforeText && (
						<p className="ultp-dynamic-content-field-before">
							{field.beforeText}
						</p>
					)}
					<p className="ultp-dynamic-content-field-dc">
						{text}
					</p>
					{field.afterText && (
						<p className="ultp-dynamic-content-field-after">
							{field.afterText}
						</p>
					)}
				</>
			),
			hasResolved,
		};
	});

	useEffect(() => {
		if (hasResolved) {
			resetOnboarding(groupIdx, idx);
		}
	}, [hasResolved]);

	const { elementRef, isSelected, setIsSelected } = useClickToggle();

	return (
		<div
			ref={elementRef}
			className={`ultp-dynamic-content-field-common ultp-dynamic-content-field-${field.id} ultp-component-simple ${isSelected ? 'ultp-component-selected' : ''}`}
			onClick={(e) => {
				setIsSelected(true);
				settingsOnClick(e, `dc_field`);
			}}
		>
			{field.dataSrc === '' ? (
				<span className="ultp-dynamic-content-field-dc">
					{__('Custom Field', 'ultimate-post')}
				</span>
			) : hasResolved ? (
				<>
					{field.icon && (
						<span className="ultp-dynamic-content-field-icon">
							{IconPack[field.icon]}
						</span>
					)}
					{text}
				</>
			) : (
				<span className="ultp-dynamic-content-field-dc">
					{__('Loading...', 'ultimate-post')}
				</span>
			)}
		</div>
	);
}
