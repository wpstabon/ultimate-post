const { __ } = wp.i18n;
const { ToolbarButton } = wp.components;
import { getMetaPositionFunctions } from '.';
import { formatSettingsForToolbar, styleIcon } from '../CommonPanel';
import ExtraToolbarSettings from '../Components/ExtraToolbarSettings';
import UltpToolbarGroup from '../Components/UltpToolbarGroup';
import DynamicContent from './dynamic-content';
import ToolbarMove from './ToolbarMove';

const metaParentIcon = (
	<svg
		width="24"
		height="24"
		viewBox="0 0 24 24"
		xmlns="http://www.w3.org/2000/svg"
		style={{
			fill: 'transparent',
		}}
	>
		<path
			d="M4 18V6a2 2 0 0 1 2-2h8.864a2 2 0 0 1 1.414.586l3.136 3.136A2 2 0 0 1 20 9.136V18a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2Z"
			stroke="#070707"
			stroke-width="1.5"
		/>
		<circle
			cx="9.5"
			cy="9.5"
			r=".5"
			fill="#070707"
			stroke="#070707"
			stroke-width="1.5"
		/>
		<circle
			cx="9.5"
			cy="14.5"
			r=".5"
			fill="#070707"
			stroke="#070707"
			stroke-width="1.5"
		/>
		<circle
			cx="14.5"
			cy="9.5"
			r=".5"
			fill="#070707"
			stroke="#070707"
			stroke-width="1.5"
		/>
		<circle
			cx="14.5"
			cy="14.5"
			r=".5"
			fill="#070707"
			stroke="#070707"
			stroke-width="1.5"
		/>
	</svg>
);

export default function DCToolbar({ store, selected, layoutContext }) {
	const {
		selectedDC,
		setSelectedDc,
		selectParentMetaGroup,
		attributes: { dcFields, dcGroupStyles },
	} = store;

	const {
		moveMetaGroupUp,
		moveMetaGroupDown,
		removeMetaGroup,
		addMeta,
		removeMeta,
		moveMetaUp,
		moveMetaDown,
	} = getMetaPositionFunctions({ ...store, layoutContext });

	if (selected === 'dc_group') {
		return (
			<UltpToolbarGroup text={'Meta Group'}>
				<ToolbarMove
					moveUp={moveMetaGroupUp}
					moveDown={moveMetaGroupDown}
					idx={+selectedDC}
				/>
				<ExtraToolbarSettings
					buttonContent={styleIcon}
					include={formatSettingsForToolbar([
						{
							name: 'tab',
							title: __('Meta Group Style', 'ultimate-post'),
							options: [
								{
									type: 'dynamicContent',
									key: 'dcGroupStyles',
									fields: [
										{
											type: 'alignment',
											label: __(
												'Alignment',
												'ultimate-post'
											),
											responsive: true,
											key: 'dcGAlign',
											options: [
												'flex-start',
												'center',
												'flex-end',
												'space-between',
											],
											icons: [
												'left_new',
												'center_new',
												'right_new',
												'justbetween_new',
											],
											dcParentKey: 'dcGroupStyles',
											dcIdx: +selectedDC,
										},
										{
											type: 'toggle',
											label: __(
												'Inline',
												'ultimate-post'
											),
											key: 'dcGInline',
											dcParentKey: 'dcGroupStyles',
											dcIdx: +selectedDC,
										},
										{
											type: 'range',
											label: __(
												'Spacing Top',
												'ultimate-post'
											),
											key: 'dcGSpacingTop',
											min: 0,
											max: 200,
											step: 1,
											_inline: true,
											responsive: true,
											unit: true,
											dcParentKey: 'dcGroupStyles',
											dcIdx: +selectedDC,
										},
										{
											type: 'range',
											label: __(
												'Spacing Bottom',
												'ultimate-post'
											),
											key: 'dcGSpacingBottom',
											min: 0,
											max: 200,
											step: 1,
											_inline: true,
											responsive: true,
											unit: true,
											dcParentKey: 'dcGroupStyles',
											dcIdx: +selectedDC,
										},
										{
											type: 'range',
											label: __(
												'Spacing Between',
												'ultimate-post'
											),
											key: 'dcGSpacing',
											min: 0,
											max: 200,
											step: 1,
											_inline: true,
											responsive: true,
											unit: true,
											dcParentKey: 'dcGroupStyles',
											dcIdx: +selectedDC,
										},
									],
								},
							],
						},
					])}
					store={store}
					label={__('Meta Group Style', 'ultimate-post')}
				/>
				<ToolbarButton
					label={__('Add New Meta Field', 'ultimate-post')}
					onClick={() => addMeta(+selectedDC)}
					icon={<span className="dashicons dashicons-plus"></span>}
				></ToolbarButton>
				<ToolbarButton
					label={__('Delete Meta Group', 'ultimate-post')}
					onClick={() => removeMetaGroup(+selectedDC)}
					icon={<span className="dashicons dashicons-trash"></span>}
				></ToolbarButton>
			</UltpToolbarGroup>
		);
	}

	if (selected === 'dc_field') {
		if (typeof selectedDC !== 'string') {
			console.log('whoa', selectedDC);
			return null;
		}

		const [groupIdx, idx, isOnboarding] = selectedDC.split(',').map(Number);

		const shouldShowArrow = dcFields[groupIdx]?.fields?.length > 1;

		const block = wp.data
			.select('core/block-editor')
			.getBlock(store.clientId);

		return (
			<UltpToolbarGroup text={'Meta Field'}>
				<div className="ultp-dynamic-content-sel-parent-btn">
					<ToolbarButton
						label={__('Select Parent Meta Group', 'ultimate-post')}
						onClick={() => selectParentMetaGroup(groupIdx)}
						icon={metaParentIcon}
					></ToolbarButton>
				</div>
				{shouldShowArrow && (
					<ToolbarMove
						moveUp={moveMetaDown}
						moveDown={moveMetaUp}
						idx={groupIdx}
						subIdx={idx}
						mode="horizontal"
					/>
				)}
				<DynamicContent
					isActive={false}
					headingBlock={block}
					type="toolbar"
					config={{
						groupIdx,
						fieldIdx: idx,
						isOnboarding: isOnboarding === 1,
						icon: true,
					}}
				/>
				<ExtraToolbarSettings
					buttonContent={styleIcon}
					include={formatSettingsForToolbar([
						{
							name: 'tab',
							title: __('Meta Field Style', 'ultimate-post'),
							options: [
								{
									type: 'dynamicContent',
									key: 'dcFieldStyles',
									fields: [
										{
											type: 'separator',
											label: __(
												'Meta Field',
												'ultimate-post'
											),
										},
										{
											type: 'typography',
											label: __(
												'Typography',
												'ultimate-post'
											),
											key: 'dcFTypo',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},
										{
											type: 'color',
											label: __('Color', 'ultimate-post'),
											key: 'dcFColor',
											key2: 'dcFColorHover',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},

										// {
										// 	type: 'range',
										// 	label: __(
										// 		'Spacing',
										// 		'ultimate-post'
										// 	),
										// 	key: 'dcFSpacing',
										// 	min: 0,
										// 	max: 100,
										// 	step: 1,
										// 	responsive: false,
										// 	unit: false,
										// 	_inline: true,
										// 	dcParentKey: 'dcFieldStyles',
										// 	dcIdx: {
										// 		groupIdx,
										// 		fieldIdx: idx,
										// 	},
										// },

										{
											type: 'separator',
											label: __(
												'Before Text',
												'ultimate-post'
											),
										},

										{
											type: 'typography',
											label: __(
												'Before Typography',
												'ultimate-post'
											),
											key: 'dcFBeforeTypo',
											key2: 'dcFBeforeColor',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},
										{
											type: 'color',
											label: __(
												'Before Color',
												'ultimate-post'
											),
											key: 'dcFBeforeColor',
											key2: 'dcFBeforeColorHover',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},

										{
											type: 'range',
											label: __(
												'Before Spacing',
												'ultimate-post'
											),
											key: 'dcBeforeSpacing',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											_inline: true,
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},

										{
											type: 'separator',
											label: __(
												'After Text',
												'ultimate-post'
											),
										},

										{
											type: 'typography',
											label: __(
												'Afte Typography',
												'ultimate-post'
											),
											key: 'dcFAfterTypo',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},
										{
											type: 'color',
											label: __(
												'After Color',
												'ultimate-post'
											),
											key: 'dcFAfterColor',
											key2: 'dcFAfterColorHover',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},

										{
											type: 'range',
											label: __(
												'After Spacing',
												'ultimate-post'
											),
											key: 'dcAfterSpacing',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											_inline: true,
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},

										{
											type: 'separator',
											label: __('Icon', 'ultimate-post'),
										},

										{
											type: 'range',
											label: __(
												'Icon Size',
												'ultimate-post'
											),
											key: 'dcFIconSize',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											_inline: true,
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},
										{
											type: 'color',
											label: __(
												'Icon Color',
												'ultimate-post'
											),
											key: 'dcFIconColor',
											key2: 'dcFIconColorHover',
											dcParentKey: 'dcFieldStyles',
											dcIdx: {
												groupIdx,
												fieldIdx: idx,
											},
										},
									],
								},
							],
						},
					])}
					store={store}
					label={__('Meta Group Style', 'ultimate-post')}
				/>
				<ToolbarButton
					label={__('Delete Meta Field', 'ultimate-post')}
					onClick={() => removeMeta(groupIdx, idx)}
					icon={<span className="dashicons dashicons-trash"></span>}
				></ToolbarButton>
			</UltpToolbarGroup>
		);
	}

	return null;
}
