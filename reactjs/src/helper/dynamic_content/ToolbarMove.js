export default function ToolbarMove({ moveUp, moveDown, idx, subIdx = null, mode = 'vertical' }) {

    return (
        <div className={"ultp-toolbar-sort" + (mode === "horizontal" ? " ultp-toolbar-sort-horizontal" : "")}>
            <span
                title="Move Element Up"
                role="button"
                aria-label="Move element up"
                className="ultp-toolbar-sort-btn"
                onClick={() => moveDown(idx, subIdx)}
            >
                <svg
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    focusable="false"
                >
                    <path d="M6.5 12.4L12 8l5.5 4.4-.9 1.2L12 10l-4.5 3.6-1-1.2z" />
                </svg>
            </span>
            <span
                title="Move Element Down"
                role="button"
                aria-label="Move element down"
                className="ultp-toolbar-sort-btn"
                onClick={() => moveUp(idx, subIdx)}
            >
                <svg
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    focusable="false"
                >
                    <path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z" />
                </svg>
            </span>
         </div>
    );
}
