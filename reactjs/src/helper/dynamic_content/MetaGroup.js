import useClickToggle from '../hooks/use-click-toggle';
import { getDCDefState } from './dynamic-content';
import Meta from './Meta';

export default function MetaGroup({
	idx,
	postId,
	fields,
	settingsOnClick,
	selectedDC,
	setSelectedDc,
	dcFields,
	setAttributes,
}) {

	const field = fields[idx];

	const {elementRef, isSelected, setIsSelected} = useClickToggle();
	
	if (!field) {
		return null;
	}

	return (
		<div
			ref={elementRef}
			className={
				'ultp-dynamic-content-group-common ultp-dynamic-content-group-' +
				fields[idx].id
			}
			onClick={(e) => {
				setIsSelected(true);
				setSelectedDc(idx);
				settingsOnClick(e, `dc_group`);
			}}
		>
			{fields[idx]?.fields.map((field, index) => {
				return (
					<Meta
						key={index}
						idx={index}
						postId={postId}
						groupIdx={idx}
						field={field}
						resetOnboarding={(fieldGroupIdx, fieldIdx) => {
							if (
								selectedDC === `${fieldGroupIdx},${fieldIdx},1`
							) {
								setSelectedDc('');
								settingsOnClick(null, '');
								wp.data
									.dispatch('core/block-editor')
									.clearSelectedBlock();
							}
						}}
						settingsOnClick={(e) => {
							setSelectedDc(`${idx},${index}`);
							settingsOnClick(e, `dc_field`);
						}}
					/>
				);
			})}

			{
				isSelected && (
					<button
						type="button"
						className="components-button block-editor-inserter__toggle has-icon"
						aria-label="Add a new Meta Field"
						onClick={(e) => {
							e.stopPropagation();
							const newDcFields = [...dcFields];
							newDcFields[idx].fields.push(getDCDefState());
							setAttributes({ dcFields: newDcFields });
						}}
					>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 24 24"
							width={18}
							height={24}
							aria-hidden="true"
							focusable="false"
							fill='white'
						>
							<path d="M11 12.5V17.5H12.5V12.5H17.5V11H12.5V6H11V11H6V12.5H11Z" />
						</svg>
					</button>
				)
			}
		</div>
	);
}
