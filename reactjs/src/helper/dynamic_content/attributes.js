export function getDCBlockAttributes(options = {}) {

    const {defColor = null} = options;

	return {
		dcEnabled: {
			type: 'boolean',
			default: false,
		},

		dcFields: {
			type: 'array',
			default: [],
		},

		dcGroupStyles: {
			type: 'object',
			fields: {
				dcGAlign: {
					type: 'string',
					default: '',
					style: [
						{
							depends: [
								{
									key: 'dcGInline',
									condition: '==',
									value: true,
								},
							],
							selector:
								'{{ULTP}} .ultp-dynamic-content-group-{{dcID}} { justify-content:{{dcGAlign}}; flex-direction:row; }',
						},
						{
							depends: [
								{
									key: 'dcGInline',
									condition: '==',
									value: false,
								},
							],
							selector:
								'{{ULTP}} .ultp-dynamic-content-group-{{dcID}} { align-items:{{dcGAlign}}; flex-direction:column; }',
						},
					],
				},

				dcGInline: {
					type: 'boolean',
					default: true,
				},

				dcGSpacing: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-group-{{dcID}} { gap:{{dcGSpacing}}; }',
						},
					],
				},

				dcGSpacingTop: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-group-{{dcID}} { padding-top:{{dcGSpacingTop}}; }',
						},
					],
				},

				dcGSpacingBottom: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-group-{{dcID}} { padding-bottom:{{dcGSpacingBottom}}; }',
						},
					],
				},
			},
			default: {
				default: {
					dcGAlign: 'flex-start',
					dcGInline: true,
					dcGSpacing: {
						lg: '10',
						ulg: 'px',
					},
					dcGSpacingTop: {
						lg: '5',
						ulg: 'px',
					},
					dcGSpacingBottom: {
						lg: '5',
						ulg: 'px',
					},
				},
			},
		},

		dcFieldStyles: {
			type: 'object',
			fields: {
				dcFTypo: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-dc',
						},
					],
				},

				dcFColor: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-dc { color:{{dcFColor}}; }',
						},
					],
				},

				dcFSpacing: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-dc { margin-inline:{{dcFSpacing}}px; }',
						},
					],
				},

				dcFColorHover: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-dc:hover { color:{{dcFColorHover}}; }',
						},
					],
				},

				dcFBeforeTypo: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-before',
						},
					],
				},

				dcFBeforeColor: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-before { color:{{dcFBeforeColor}}; }',
						},
					],
				},

				dcFBeforeColorHover: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-before:hover { color:{{dcFBeforeColorHover}}; }',
						},
					],
				},

				dcBeforeSpacing: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-before { margin-right:{{dcBeforeSpacing}}px; }',
						},
					],
				},

				dcFAfterTypo: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-after',
						},
					],
				},

				dcFAfterColor: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-after { color:{{dcFAfterColor}}; }',
						},
					],
				},

				dcFAfterColorHover: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-after:hover { color:{{dcFAfterColorHover}}; }',
						},
					],
				},

				dcAfterSpacing: {
					type: 'object',
					default: {},
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-after { margin-left:{{dcAfterSpacing}}px; }',
						},
					],
				},

				dcFIconSize: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-icon svg { width:{{dcFIconSize}}px; height:auto; }',
						},
					],
				},

				dcFIconColor: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-icon svg { fill: {{dcFIconColor}};  }',
						},
					],
				},

				dcFIconColorHover: {
					type: 'string',
					default: '',
					style: [
						{
							selector:
								'{{ULTP}} .ultp-dynamic-content-field-{{dcID}} .ultp-dynamic-content-field-icon:hover svg { fill: {{dcFIconColorHover}};  }',
						},
					],
				},
			},
			default: {
				default: {
					dcFTypo: {
						openTypography: 1,
						size: { lg: '12', unit: 'px' },
						height: { lg: '', unit: 'px' },
						decoration: 'none',
						transform: '',
						family: '',
						weight: '300',
					},
					dcFColor: defColor || 'var(--postx_preset_Contrast_1_color)',
					dcFColorHover: defColor || 'var(--postx_preset_Primary_color)',
					dcFSpacing: '0',

					dcFBeforeTypo: {
						openTypography: 1,
						size: { lg: '12', unit: 'px' },
						height: { lg: '', unit: 'px' },
						decoration: 'none',
						transform: '',
						family: '',
						weight: '300',
					},
					dcFBeforeColor: defColor || 'var(--postx_preset_Contrast_1_color)',
					dcFBeforeColorHover: defColor || 'var(--postx_preset_Primary_color)',

					dcFAfterTypo: {
						openTypography: 1,
						size: { lg: '12', unit: 'px' },
						height: { lg: '', unit: 'px' },
						decoration: 'none',
						transform: '',
						family: '',
						weight: '300',
					},
					dcFAfterColor: defColor || 'var(--postx_preset_Contrast_1_color)',
					dcFAfterColorHover: defColor || 'var(--postx_preset_Primary_color)',

					dcFIconSize: '11',
					dcFIconColor: defColor || 'var(--postx_preset_Contrast_1_color)',
					dcFIconColorHover: defColor || 'var(--postx_preset_Primary_color)',
				},
			},
		},
	};
}
