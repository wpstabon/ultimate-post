import DynamicContent, { DC_DEF_STATE, getDCDefState } from './dynamic-content';

const { __ } = wp.i18n;
const { useSelect } = wp.data;
const { registerFormatType } = wp.richText;

const title = __('Dynamic Content', 'ultimate-post');

const ALLOWED_BLOCKS = [
	'ultimate-post/heading',
	'ultimate-post/list',
	'ultimate-post/button',
];

const arrayMove = (arr, oldIndex, newIndex) => {
	arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
	return arr;
};

export const getMetaPositionFunctions = (props) => {
	const {
		attributes,
		setAttributes,
		setSelectedDc,
		selectedDC,
		layoutContext,
	} = props;

	const { dcFields } = attributes;

	return {
		moveMetaGroupUp: (index) => {
			const dec = Math.min(
				...[
					layoutContext
						.slice(0, index)
						.reverse()
						.findIndex((f) => f),
					dcFields
						.slice(0, index)
						.reverse()
						.findIndex((f) => f),
				].filter((i) => i >= 0)
			);
			const size = dcFields.length;
			const newIndex = index - (dec + 1);
			if (newIndex >= 0 && newIndex < size) {
				const newDcFields = [...dcFields];
				arrayMove(newDcFields, index, newIndex);
				setAttributes({ dcFields: newDcFields });
				setSelectedDc(newIndex);
			}
		},
		moveMetaGroupDown: (index) => {
			const inc = Math.min(
				...[
					layoutContext
						.slice(index, layoutContext.length)
						.findIndex((f) => f),
					dcFields
						.slice(index + 1, dcFields.length)
						.findIndex((f) => f),
				].filter((i) => i >= 0)
			);
			const size = dcFields.length;
			const newIndex = index + inc + 1;
			if (newIndex >= 0 && newIndex < size) {
				const newDcFields = [...dcFields];
				arrayMove(newDcFields, index, newIndex);
				setAttributes({ dcFields: newDcFields });
				setSelectedDc(newIndex);
			}
		},
		removeMetaGroup: (index) => {
			if (Number.isInteger(index)) {
				const newDcFields = [...dcFields];
				newDcFields[index] = null;
				setAttributes({ dcFields: newDcFields });
				setSelectedDc('')
				wp.data.dispatch('core/block-editor').clearSelectedBlock();
			}
		},
		addMeta: (index) => {
			if (Number.isInteger(index)) {
				const newDcFields = [...dcFields];
				newDcFields[index].fields.push(getDCDefState());
				setAttributes({ dcFields: newDcFields });
			}
		},
		removeMeta: (groupIndex, index) => {
			if (Number.isInteger(groupIndex) && Number.isInteger(index)) {
				const newDcFields = [...dcFields];
				newDcFields[groupIndex].fields.splice(index, 1);
				// Delete empty group
				if (newDcFields[groupIndex].fields.length === 0) {
					newDcFields[groupIndex] = null;
				}
				setAttributes({ dcFields: newDcFields });
				setSelectedDc('')
				wp.data.dispatch('core/block-editor').clearSelectedBlock();
			}
		},
		moveMetaUp: (groupIndex, index) => {
			if (Number.isInteger(groupIndex) && Number.isInteger(index)) {
				if (index > 0) {
					const newDcFields = [...dcFields];
					arrayMove(newDcFields[groupIndex].fields, index, index - 1);
					setAttributes({ dcFields: newDcFields });
					setSelectedDc(`${groupIndex},${index - 1}`);
				}
			}
		},
		moveMetaDown: (groupIndex, index) => {
			if (Number.isInteger(groupIndex) && Number.isInteger(index)) {
				const newDcFields = [...dcFields];
				if (index < newDcFields[groupIndex].fields.length - 1) {
					arrayMove(newDcFields[groupIndex].fields, index, index + 1);
					setAttributes({ dcFields: newDcFields });
					setSelectedDc(`${groupIndex},${index + 1}`);
				}
			}
		},
	};
};

export const dynamicRichTextAttributes = {
	dc: {
		type: 'object',
		default: DC_DEF_STATE,
	},

	dcEnabled: {
		type: 'boolean',
		default: false,
	},
};

export const dynamicImageAttributes = {
	dc: {
		type: 'object',
		default: {
			imageUpload: DC_DEF_STATE,
			darkImage: DC_DEF_STATE,
			imgLink: DC_DEF_STATE,
			btnLink: DC_DEF_STATE,
		},
	},

	dcEnabled: {
		type: 'object',
		default: {
			imageUpload: false,
			darkImage: false,
			imgLink: false,
			btnLink: false,
		},
	},
};

export function isDCActive() {
	return ultp_data?.settings?.ultp_dynamic_content === 'true' && wp.data.select('core/editor');
}

if (isDCActive()) {
	// @see https://github.com/WordPress/gutenberg/blob/4741104c2e035a6b80ab7e01031a9d4086b3f75d/packages/rich-text/src/register-format-type.js#L17
	registerFormatType('ultimate-post/dynamic-content', {
		title,
		tagName: 'span',
		className: 'ultp-richtext-dynamic-content',
		// attributes: { 'data-test': 'test_value' },
		edit(props) {
			const { isActive, value } = props;

			const selectedBlock = useSelect((select) => {
				return select('core/block-editor').getSelectedBlock();
			}, []);

			if (selectedBlock && !ALLOWED_BLOCKS.includes(selectedBlock.name)) {
				return null;
			}

			return (
				<DynamicContent
					isActive={isActive}
					richTextProps={props}
					headingBlock={selectedBlock}
					type={'heading'}
				/>
			);
		},
	});
}
