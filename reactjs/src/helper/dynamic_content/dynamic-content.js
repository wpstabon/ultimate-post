const { applyFormat, insert, removeFormat } = wp.richText;

import { ULTP_STORE, ultpStoreClearCache } from '../../store';
import { settingsIcon } from '../CommonPanel';
import Icon from '../fields/Icon';
import {
	ContentSourceSelect,
	DataSourceOptGroups,
	dcEditorProLink,
	LINK_PREFIX,
	LinkSourceSelect,
	PODS_FIELD_PREFIX,
	PostsSearch,
	PostTypeOptGroups,
	TextField,
} from './utils';

const { __ } = wp.i18n;
const { useState } = wp.element;
const { BlockControls } = wp.blockEditor;
const {useSelect} = wp.data;
const {
	Dropdown,
	ToolbarButton,
	ToolbarGroup,
	SelectControl,
	Button,
	ToggleControl,
	PanelBody,
	__experimentalNumberControl: NumberControl,
} = wp.components;

import {
	ACF_FIELD_PREFIX,
	AUTHOR_PREFIX,
	CUSTOM_META_PREFIX,
	METABOX_FIELD_PREFIX,
	POST_PREFIX,
	SITE_PREFIX,
} from './utils';

export const DC_DEF_STATE = {
	dataSrc: '',
	postType: '',
	postId: '',
	contentSrc: '',
	linkEnabled: false,
	linkSrc: '',
	beforeText: '',
	afterText: '',
	iconType: '',
	icon: '',
	fallback: '',
	maxCharLen: '',
};

export function getDCDefState() {
	return {
		...DC_DEF_STATE,
		id: new Date().getTime(),
	};
}

export default function DynamicContent({
	isActive,
	headingBlock: block,
	richTextProps = {},
	type = 'field',
	attrKey = '',
	config = null,
}) {
	const html = () => {
		return (
			<Dropdown
				contentClassName="ultp-dynamic-content-wrapper"
				defaultOpen={config?.isOnboarding}
				renderToggle={({ onToggle, isOpen }) => {
					if (isActive && !isOpen) {
						onToggle();
					}

					if (type === 'field') {
						return (
							<div
								onClick={(e) => {
									e.stopPropagation();
									e.preventDefault();
									onToggle();
								}}
								className="ultp-dc-field-dropdown"
							>
								<span
									className={`ultp-dc-field-dropdown dashicons dashicons-database-add`}
								></span>
							</div>
						);
					}

					if (type === 'heading') {
						return (
							<span>
								<ToolbarButton
									label={__(
										'Dynamic Content',
										'ultimate-post'
									)}
									icon={
										<span className="dashicons dashicons-database-add"></span>
									}
									onClick={() => onToggle()}
									isActive={isOpen}
								/>
							</span>
						);
					}

					if (type === 'toolbar') {
						return (
							<ToolbarButton
								label={__(
									'Meta Field Settings',
									'ultimate-post'
								)}
								onClick={() => onToggle()}
								icon={settingsIcon}
							></ToolbarButton>
						);
					}
				}}
				renderContent={() => (
					<div className="ultp-dynamic-content-dropdown">
						<Content
							richTextProps={richTextProps}
							headingBlock={block}
							type={type}
							attrKey={attrKey}
							config={config}
						/>
					</div>
				)}
			/>
		);
	};

	if (type === 'field' || type === 'toolbar') {
		return html();
	}

	if (type === 'heading') {
		return (
			<>
				<BlockControls>
					<ToolbarGroup>{html()}</ToolbarGroup>
				</BlockControls>
			</>
		);
	}
}

/**
 * Gets the start and ending indexes of DC format
 *
 * @param {array} formats
 * @returns
 */
function getIndexes(formats) {
	let start;
	let end;

	for (let i = 0; i < formats.length; i++) {
		if (
			formats[i] &&
			formats[i].some(
				(format) => format.type === 'ultimate-post/dynamic-content'
			)
		) {
			start = i;
			break;
		}
	}

	for (let i = formats.length; i >= 0; i--) {
		if (
			formats[i] &&
			formats[i].some(
				(format) => format.type === 'ultimate-post/dynamic-content'
			)
		) {
			end = i;
			break;
		}
	}

	return {
		start,
		end,
	};
}

function richTextApplyDynamicContent(newText, richTextProps, headingBlock) {
	const { isActive, value, onChange, onFocus } = richTextProps;
	const { start, end } = getIndexes(value.formats);
	let startIdx = start ?? value.start;
	const newValue = insert(
		value,
		newText,
		startIdx,
		end ? end + 1 : value.end
	);

	const endIdx = startIdx + newText.length;

	onChange(
		applyFormat(
			newValue,
			{
				type: 'ultimate-post/dynamic-content',
				title: __('Dynamic Content', 'ultimate-post'),
			},
			startIdx,
			endIdx
		)
	);

	updateAttributes(headingBlock.clientId, {
		dcText: {
			start: startIdx,
			end: endIdx,
		},
	});

	// Removes focus from the rich text, better UX
	wp.data.dispatch('core/block-editor').selectBlock();
}

function richTextRemoveDynamicContent(richTextProps) {
	const { value, onChange } = richTextProps;
	onChange(removeFormat(value, 'ultimate-post/dynamic-content'));
	wp.data.dispatch('core/block-editor').selectBlock();
}

function updateAttributes(clientId, payload) {
	const { updateBlockAttributes } = wp.data.dispatch('core/block-editor');
	updateBlockAttributes(clientId, payload);
}

function Content({
	richTextProps,
	headingBlock: block,
	type,
	attrKey,

	/**
	 * @type {Object}
	 * @param {boolean} disableLink
	 * @param {string} fieldType
	 * @param {boolean} linkOnly
	 */
	config,
}) {
	const [opts, setOpts] = useState(
		type === 'toolbar'
			? block.attributes.dcFields[config.groupIdx].fields[config.fieldIdx]
			: attrKey
				? block.attributes.dc[attrKey]
				: block.attributes.dc
	);

	const [isBusy, setIsBusy] = useState(false);

	const postId = useSelect((select) => {
		return opts.postId || select('core/editor').getCurrentPostId();
	}, [opts.postId])

	const onClick = async (formatEnabled) => {
		setIsBusy(true);

		try {
			if (!formatEnabled) {
				if (type === 'heading') {
					updateAttributes(block.clientId, {
						dcEnabled: false,
						dc: DC_DEF_STATE,
					});
					richTextRemoveDynamicContent(richTextProps);
				}

				if (type === 'field') {
					if (block.name === 'ultimate-post/image') {
						const data = {
							dc: {
								...block.attributes.dc,
								[attrKey]: DC_DEF_STATE,
							},
							dcEnabled: {
								...block.attributes.dcEnabled,
								[attrKey]: false,
							},
						};

						if (['imageUpload', 'darkImage'].includes(attrKey)) {
							data[attrKey] = {
								...block.attributes[attrKey],
								url: '',
							};
						} else {
							data[attrKey] = '';
						}
						updateAttributes(block.clientId, data);
					}
				}

				if (type === 'toolbar') {
					updateAttributes(block.clientId, {
						dcEnabled: false,
					});
				}

				setOpts((prev) => ({
					...prev,
					...DC_DEF_STATE,
				}));
			} else {
				if (type === 'toolbar') {
					const newDcFields = [...block.attributes.dcFields];
					newDcFields[config.groupIdx].fields[config.fieldIdx] = opts;
					updateAttributes(block.clientId, {
						dcEnabled: true,
						dcFields: newDcFields,
					});
				} else {
					let text = '';
					let selector = '';
					let args = [];

					if (opts.contentSrc.startsWith(POST_PREFIX)) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getPostInfo(
								postId,
								opts.contentSrc.slice(POST_PREFIX.length)
							);

						selector = 'getPostInfo';
						args = [
							postId,
							opts.contentSrc.slice(POST_PREFIX.length),
						];
					}

					if (opts.contentSrc.startsWith(AUTHOR_PREFIX)) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getAuthorInfo(
								postId,
								opts.contentSrc.slice(AUTHOR_PREFIX.length)
							);

						selector = 'getAuthorInfo';
						args = [
							postId,
							opts.contentSrc.slice(AUTHOR_PREFIX.length),
						];
					}

					if (
						opts.contentSrc.startsWith(ACF_FIELD_PREFIX) ||
						opts.contentSrc.startsWith(CUSTOM_META_PREFIX) ||
						opts.contentSrc.startsWith(METABOX_FIELD_PREFIX) ||
						opts.contentSrc.startsWith(PODS_FIELD_PREFIX)
					) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getCFValue(postId, opts.contentSrc);

						selector = 'getCFValue';
						args = [postId, opts.contentSrc];
					}

					if (opts.dataSrc.startsWith(SITE_PREFIX)) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getSiteInfo(
								opts.dataSrc.slice(SITE_PREFIX.length)
							);

						selector = 'getSiteInfo';
						args = [opts.dataSrc.slice(SITE_PREFIX.length)];
					}

					if (opts.contentSrc.startsWith(LINK_PREFIX)) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getLink(postId, opts.contentSrc);

						selector = 'getLink';
						args = [postId, opts.contentSrc];
					}

					if (opts.linkEnabled && opts.linkSrc && config?.linkOnly) {
						text = await wp.data
							.resolveSelect(ULTP_STORE)
							.getLink(postId, opts.linkSrc);

						selector = 'getLink';
						args = [postId, opts.linkSrc];
					}

					// Invalidating Cache
					ultpStoreClearCache(selector, args);

					if (
						['string', 'number', 'boolean', 'bigint'].includes(
							typeof text
						)
					) {
						text = String(text);

						if (opts.maxCharLen) {
							text = text.split(' ').slice(0, +opts.maxCharLen).join(' ');
						}
					} else {
						console.log('Invalid Data Type: ', text);
						text = null;
					}

					if (!text) {
						text = opts.fallback || '[EMPTY]';
					}

					text = opts.beforeText + text + opts.afterText;

					if (type === 'heading') {
						updateAttributes(block.clientId, {
							dcEnabled: true,
							dc: opts,
						});
						richTextApplyDynamicContent(text, richTextProps, block);
					}

					if (block.name === 'ultimate-post/image') {
						const data = {
							dc: {
								...block.attributes.dc,
								[attrKey]: opts,
							},
							dcEnabled: {
								...block.attributes.dcEnabled,
								[attrKey]: true,
							},
						};

						if (['imageUpload', 'darkImage'].includes(attrKey)) {
							data[attrKey] = {
								...block.attributes[attrKey],
								url: text,
							};
						} else {
							data[attrKey] = text;
						}

						updateAttributes(block.clientId, data);
					}
				}
			}
		} catch (ex) {
			console.log(ex);
		} finally {
			setIsBusy(false);
		}
	};

	if (type === 'heading') {
		const isDcActive =
			!richTextProps.isActive && block.attributes.dcEnabled;

		if (isDcActive) {
			return (
				<div className="ultp-dynamic-content-empty">
					{__(
						'This block only supports 1 Dynamic Data Binding.',
						'ultimate-post'
					)}
				</div>
			);
		}
	}

	const showResetButton =
		(type === 'heading' &&
			richTextProps.isActive &&
			block.attributes.dcEnabled) ||
		(type === 'field' &&
			(attrKey
				? block.attributes.dcEnabled[attrKey]
				: block.attributes.dcEnabled));

	let isApplyDisabled = false;

	if (
		['current_post', 'post_type'].includes(opts.dataSrc) && !config?.linkOnly
			? opts.contentSrc === ''
			: opts.dataSrc === ''
	) {
		isApplyDisabled = true;
	} else if (
		!config?.linkOnly
			? !config?.disableLink && opts.linkEnabled && opts.linkSrc === ''
			: opts.linkSrc === ''
	) {
		isApplyDisabled = true;
	}

	return (
		<>
			<div className="ultp-dynamic-content-fields">
				<SelectControl
					__nextHasNoMarginBottom={true}
					labelPosition="side"
					label={__('Data Source', 'ultimate-post')}
					value={opts.dataSrc}
					onChange={(dataSrc) => {
						setOpts((prev) => {
							return {
								...prev,
								contentSrc: '',
								searchValue: [],
								postId: '',
								postType: '',
								dataSrc,
							};
						});
					}}
				>
					<DataSourceOptGroups config={config} type={type} />
				</SelectControl>

				{opts.dataSrc === 'post_type' && (
					<>
						<SelectControl
							__nextHasNoMarginBottom={true}
							labelPosition="side"
							label={__('Select Post Type', 'ultimate-post')}
							value={opts.postType}
							onChange={(postType) => {
								setOpts((prev) => {
									return {
										...prev,
										postId: '',
										searchValue: [],
										contentSrc: '',
										linkSrc: '',
										postType,
									};
								});
							}}
						>
							<PostTypeOptGroups />
						</SelectControl>

						{opts.postType !== '' && (
							<PostsSearch opts={opts} setOpts={setOpts} />
						)}
					</>
				)}

				{(opts.dataSrc === 'current_post' ||
					(opts.dataSrc === 'post_type' &&
						opts.postType !== '' &&
						opts.postId !== '')) && (
					<>
						{!config?.linkOnly && (
							<ContentSourceSelect
								opts={opts}
								setOpts={setOpts}
								type={config?.fieldType ?? 'text'}
								req={(function () {
									const data = {};

									data['post_type'] =
										type === 'toolbar'
											? opts.dataSrc === 'post_type'
												? opts.postType
												: block.attributes.queryType
											: opts.dataSrc === 'current_post'
												? wp.data
														.select('core/editor')
														.getCurrentPostType()
												: opts.postType;

									const fieldType =
										config?.fieldType ?? 'text';
									data['acf_field_type'] = fieldType;

									if (
										type !== 'toolbar' ||
										(type === 'toolbar' &&
											opts.dataSrc === 'post_type')
									) {
										data['post_id'] = postId;
									}

									return data;
								})()}
							/>
						)}
					</>
				)}

				{!config?.disableLink && opts.dataSrc !== '' && (
					<>
						{!config?.linkOnly && (
							<ToggleControl
								__nextHasNoMarginBottom={true}
								checked={opts.linkEnabled}
								label={__('Enable Link', 'ultimate-post')}
								onChange={(linkEnabled) =>
									setOpts((prev) => ({
										...prev,
										linkEnabled,
										linkSrc: '',
									}))
								}
							/>
						)}

						{(opts.linkEnabled || config?.linkOnly) && (
							<LinkSourceSelect
								opts={opts}
								setOpts={setOpts}
								req={(function () {
									const data = {};

									data['post_type'] =
										type === 'toolbar'
											? block.attributes.queryType
											: opts.dataSrc === 'current_post'
												? wp.data
														.select('core/editor')
														.getCurrentPostType()
												: opts.postType;

									data['acf_field_type'] = 'url';

									if (type !== 'toolbar') {
										data['post_id'] = postId;
									}

									return data;
								})()}
							/>
						)}
					</>
				)}

				{!config?.disableAdv && (
					<>
						<PanelBody
							title={__('Advanced', 'ultimate-post')}
							initialOpen={false}
						>
							<TextField
								value={opts.beforeText}
								onChange={(beforeText) => {
									setOpts((prev) => {
										return {
											...prev,
											beforeText,
										};
									});
								}}
								label={__('Before Text', 'ultimate-post')}
							/>
							<TextField
								value={opts.afterText}
								onChange={(afterText) => {
									setOpts((prev) => {
										return {
											...prev,
											afterText,
										};
									});
								}}
								label={__('After Text', 'ultimate-post')}
							/>
							<TextField
								value={opts.fallback}
								onChange={(fallback) => {
									setOpts((prev) => {
										return {
											...prev,
											fallback,
										};
									});
								}}
								label={__('Fallback', 'ultimate-post')}
							/>

							{config?.icon && (
								<>
									<div className="ultp-dc-icon-control">
										<Icon
											value={opts.icon}
											label={__('Icon', 'ultimate-post')}
											isSocial={false}
											inline={false}
											dynamicClass={''}
											onChange={(icon) =>
												setOpts((prev) => ({
													...prev,
													icon,
												}))
											}
										/>
									</div>
								</>
							)}

							<NumberControl
								min={1}
								labelPosition="side"
								value={opts.maxCharLen}
								onChange={(maxCharLen) => {
									setOpts((prev) => {
										return {
											...prev,
											maxCharLen: String(maxCharLen),
										};
									});
								}}
								label={__('Max Length', 'ultimate-post')}
							/>
						</PanelBody>
					</>
				)}

				<Button
					className="ultp-dynamic-content-dropdown-button"
					// variant={block.attributes.dcEnabled ? 'secondary' : 'primary'}
					variant={'primary'}
					onClick={() => onClick(true)}
					isBusy={isBusy}
					disabled={isApplyDisabled}
				>
					{__('Apply', 'ultimate-post')}
					{/* {
						block.attributes.dcEnabled ? 
						__('Update', 'ultimate-post') :
						__('Apply', 'ultimate-post') 
					} */}
				</Button>

				{showResetButton && (
					<Button
						style={{ marginTop: '24px' }}
						isDestructive={true}
						className="ultp-dynamic-content-dropdown-button"
						variant="secondary"
						onClick={() => onClick(false)}
						isBusy={isBusy}
					>
						{__('Reset', 'ultimate-post')}
					</Button>
				)}
			</div>

			{!ultp_data.active && (
				<div className="ultp-dyanmic-content-pro">
					<span>
						{__(
							'Get ACF Integration & Meta Box Access',
							'ultimate-post'
						)}
					</span>
					<a
						href={dcEditorProLink}
						target="_blank"
						className="ultp-dyanmic-content-pro-link"
					>
						<span>{__('Upgrade to PRO', 'ultimate-post')}</span>
						<svg
							width={14}
							height={14}
							viewBox="0 0 14 14"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path d="M6.08594 0.707031H13.293V7.91406L11.5 7V4.22266L6.47266 9.25L5.20703 7.98438L10.6562 2.5H7L6.08594 0.707031ZM8.79297 11.5V8.79297L10.5859 7V13.293H0.707031V3.41406H7.91406L6.08594 5.20703H2.5V11.5H8.79297Z" />
						</svg>
					</a>
				</div>
			)}
		</>
	);
}
