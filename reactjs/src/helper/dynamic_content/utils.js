import { ULTP_STORE } from '../../store';
import Search from '../fields/Search';
import Select from '../fields/Select';
import UltpLinkGenerator from '../UltpLinkGenerator';

const { __ } = wp.i18n;
const { useSelect } = wp.data;
const { TextControl } = wp.components;

export const CUSTOM_META_PREFIX = 'cmeta_';
export const ACF_FIELD_PREFIX = 'acf_';
export const METABOX_FIELD_PREFIX = 'mb_';
export const PODS_FIELD_PREFIX = 'pods_';
export const POST_PREFIX = 'post_';
export const AUTHOR_PREFIX = 'a_';
export const SITE_PREFIX = 'site_';
export const LINK_PREFIX = 'link_';
export const OTHER_PREFIX = 'o_';

export const dcEditorProLink = UltpLinkGenerator(
	null,
	'dc',
	ultp_data.affiliate_id
);

export function TextField(props) {
	return (
		<div className="ultp-dynamic-content-textfield">
			<label>{props.label}</label>
			<TextControl {...{ ...props, label: undefined, __nextHasNoMarginBottom: true  }} />
		</div>
	);
}

export function DataSourceOptGroups({ config, type }) {
	return (
		<>
			<option value="">{__('None', 'ultimate-post')}</option>
			<option value="current_post">
				{__('Current Post', 'ultimate-post')}
			</option>
			<option value="post_type">
				{__('Post Type', 'ultimate-post')}
			</option>

			{!config?.linkOnly && config?.fieldType !== 'image' && (
				<>
					<optgroup label={__('Archive', 'ultimate-post')}>
						<option value={SITE_PREFIX + 'arc_desc'}>
							{__('Archive Description', 'ultimate-post')}
						</option>
						<option value={SITE_PREFIX + 'arc_title'}>
							{__('Archive Title', 'ultimate-post')}
						</option>
					</optgroup>

					<optgroup label={__('Site', 'ultimate-post')}>
						<option value={SITE_PREFIX + 'tagline'}>
							{__('Site Tagline', 'ultimate-post')}
						</option>
						<option value={SITE_PREFIX + 'title'}>
							{__('Site Title', 'ultimate-post')}
						</option>
						{/* <option value={SITE_PREFIX + "req_param"}>
					{__('Request Parameter', 'ultimate-post')}
				</option> */}
						{/* <option value={SITE_PREFIX + "shortcode"}>
					{__('Shortcode', 'ultimate-post')}
				</option> */}
						{/* <option value="user_cf">
					{__('User Custom Field - Current User', 'ultimate-post')}
				</option> */}
					</optgroup>
				</>
			)}
		</>
	);
}

export function PostTypeOptGroups() {
	const postTypes = useSelect((select) => {
		return select(ULTP_STORE).getPostTypes();
	});

	const hasResolved = useSelect((select) => {
		return select(ULTP_STORE).hasFinishedResolution('getPostTypes');
	});

	return (
		<>
			<option value="" disabled>
				{__('Choose', 'ultimate-post')}
			</option>
			{!hasResolved ? (
				<option value="loading" disabled>
					{__('Loading...', 'ultimate-post')}
				</option>
			) : !postTypes?.length ? (
				<option value="no-data" disabled>
					{__('No Post Types Found', 'ultimate-post')}
				</option>
			) : (
				postTypes?.map((postType) => {
					return (
						<option key={postType.value} value={postType.value}>
							{postType.label}
						</option>
					);
				})
			)}
		</>
	);
}

export function PostsSearch({ opts, setOpts }) {
	return (
		<Search
			label={__('Select Post', 'ultimate-post')}
			value={opts?.searchValue ?? []}
			search={'postExclude'}
			single={true}
			condition={opts.postType}
			noIdInTitle={true}
			onChange={(val) => {
				setOpts((prev) => {
					return {
						...prev,
						contentSrc: '',
						postId: val[0] !== undefined ? val[0].value : '',
						searchValue: val,
					};
				});
			}}
		/>
	);
}

const POST_CONTENT_OPTIONS = [
	{
		label: __('Post Title', 'ultimate-post'),
		value: POST_PREFIX + 'title',
	},
	{
		label: __('Post Excerpt', 'ultimate-post'),
		value: POST_PREFIX + 'excerpt',
	},
	{
		label: __('Post Date', 'ultimate-post'),
		value: POST_PREFIX + 'date',
	},
	{
		label: __('Post Id', 'ultimate-post'),
		value: POST_PREFIX + 'id',
	},
	{
		label: __('Post Number of Comments', 'ultimate-post'),
		value: POST_PREFIX + 'comments',
	},
];

const AUTHOR_CONTENT_OPTIONS = [
	{
		label: __('Name', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'display_name',
	},
	{
		label: __('First Name', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'first_name',
	},
	{
		label: __('Last Name', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'last_name',
	},
	{
		label: __('Nickname', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'nickname',
	},
	{
		label: __('Bio', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'description',
	},
	{
		label: __('Email', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'email',
	},
	{
		label: __('Website', 'ultimate-post'),
		value: AUTHOR_PREFIX + 'url',
	},
];

const POST_CONTENT_OPTIONS_IMAGE = [
	{
		label: __('Post Feature Image', 'ultimate-post'),
		value: LINK_PREFIX + 'post_featured_image',
	},
	{
		label: __('Author Profile Image', 'ultimate-post'),
		value: LINK_PREFIX + 'a_profile',
	},
];

export function ContentSourceSelect({ req, type, opts, setOpts }) {
	const {
		options: { acfFields, cMetaFields, mbFields, podsFields },
		error,
		hasResolved,
	} = useSelect(
		(select) => {
			return select(ULTP_STORE).getCustomFields(req);
		},
		[req]
	);

	/**
	 * @type {Array[Object]}
	 */
	const options = [
		{
			value: '',
			label: __('Choose', 'ultimate-post'),
		},
		{
			value: '',
			label: __('Post', 'ultimate-post'),
			isHeader: true,
		},
	];

	(type === 'image' ? POST_CONTENT_OPTIONS_IMAGE : POST_CONTENT_OPTIONS).map(
		(post) => {
			options.push({
				value: post.value,
				label: post.label,
				isChild: true,
			});
		}
	);

	// Post Meta
	options.push({
		value: '',
		label: __('Post Meta', 'ultimate-post'),
		isHeader: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			disabled: true,
		});
	} else if (!cMetaFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Post Meta Found', 'ultimate-post'),
			isChild: true,
			disabled: true,
		});
	} else {
		cMetaFields?.map((opt) => {
			options.push({
				value: CUSTOM_META_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
			});
		});
	}

	// ACF
	options.push({
		value: '',
		label: __('ACF', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			disabled: true,
			pro: true,
		});
	} else if (!acfFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No ACF Data Found', 'ultimate-post'),
			isChild: true,
			disabled: true,
			pro: true,
		});
	} else {
		acfFields?.map((opt) => {
			options.push({
				value: ACF_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Meta box
	options.push({
		value: '',
		label: __('Meta Box', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else if (!mbFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Meta Box Data Found', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else {
		mbFields?.map((opt) => {
			options.push({
				value: METABOX_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Pods
	options.push({
		value: '',
		label: __('Pods', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else if (!podsFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Pods Data Found', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else {
		podsFields?.map((opt) => {
			options.push({
				value: PODS_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Author
	options.push({
		value: '',
		label: __('Author', 'ultimate-post'),
		isHeader: true,
	});
	if (type !== 'image') {
		AUTHOR_CONTENT_OPTIONS.map((author) => {
			options.push({
				value: author.value,
				label: author.label,
				isChild: true,
			});
		});
	}

	return (
		<Select
			beside={true}
			label={__('Content Source', 'ultimate-post')}
			value={opts.contentSrc}
			onChange={(contentSrc) => {
				setOpts((prev) => {
					return {
						...prev,
						contentSrc,
					};
				});
			}}
			options={options}
			responsive={false}
			proLink={dcEditorProLink}
		/>
	);
}

export function LinkSourceSelect({ req, opts, setOpts }) {
	const {
		options: { acfFields, cMetaFields, mbFields, podsFields },
		error,
		hasResolved,
	} = useSelect(
		(select) => {
			return select(ULTP_STORE).getCustomFields(req);
		},
		[req]
	);

	/**
	 * @type {Array[Object]}
	 */
	const options = [
		{
			value: '',
			label: __('None', 'ultimate-post'),
		},
		{
			value: '',
			label: __('Post', 'ultimate-post'),
			isHeader: true,
		},
		{
			value: LINK_PREFIX + 'post_permalink',
			label: __('Post Permalink', 'ultimate-post'),
			isChild: true,
		},
		{
			value: LINK_PREFIX + 'post_permalink',
			label: __('Post Comments', 'ultimate-post'),
			isChild: true,
		},
		{
			value: '',
			label: __('Post Meta', 'ultimate-post'),
			isHeader: true,
		},
	];

	// Post
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			disabled: true,
		});
	} else if (!cMetaFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Post Meta Found', 'ultimate-post'),
			isChild: true,
			disabled: true,
		});
	} else {
		cMetaFields?.map((opt) => {
			options.push({
				value: CUSTOM_META_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
			});
		});
	}

	// ACF
	options.push({
		value: '',
		label: __('ACF', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else if (!acfFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No ACF Data Found', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else {
		acfFields?.map((opt) => {
			options.push({
				value: ACF_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Meta box
	options.push({
		value: '',
		label: __('Meta Box', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else if (!mbFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Meta Box Data Found', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else {
		mbFields?.map((opt) => {
			options.push({
				value: METABOX_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Pods
	options.push({
		value: '',
		label: __('Pods', 'ultimate-post'),
		isHeader: true,
		pro: true,
	});
	if (!hasResolved) {
		options.push({
			value: 'loading',
			label: __('Loading...', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else if (!podsFields?.length) {
		options.push({
			value: 'no-data',
			label: __('No Pods Data Found', 'ultimate-post'),
			isChild: true,
			pro: true,
			disabled: true,
		});
	} else {
		podsFields?.map((opt) => {
			options.push({
				value: PODS_FIELD_PREFIX + opt.value,
				label: opt.label,
				isChild: true,
				pro: true,
			});
		});
	}

	// Author
	options.push(
		{
			value: '',
			label: __('Author', 'ultimate-post'),
			isHeader: true,
		},
		{
			value: LINK_PREFIX + 'a_profile',
			label: __('Avatar (Profile URL)', 'ultimate-post'),
			isChild: true,
		},
		{
			value: LINK_PREFIX + 'a_arc',
			label: __('Author Archive URL', 'ultimate-post'),
			isChild: true,
		},
		{
			value: LINK_PREFIX + 'a_page',
			label: __('Author Page URL', 'ultimate-post'),
			isChild: true,
		}
	);

	return (
		<Select
			beside={true}
			label={__('Link Source', 'ultimate-post')}
			value={opts.linkSrc}
			onChange={(linkSrc) => {
				setOpts((prev) => {
					return {
						...prev,
						linkEnabled: true,
						linkSrc,
					};
				});
			}}
			options={options}
			responsive={false}
			proLink={dcEditorProLink}
		/>
	);
}
