import { getDCDefState } from './dynamic-content';

export default function AddDCButton({
	dcFields,
	setAttributes,
	startOnboarding,
	overlayMode = false,
	inverseColor = false,
}) {
	const disabled = dcFields.every((f) => f);

	const onClick = (e) => {
		if (disabled) return;
		e.stopPropagation();
		const newDcFields = [...dcFields];
		const idx = newDcFields.findIndex((f) => !f);
		if (idx >= 0) {
			newDcFields.splice(idx, 1);
			newDcFields.unshift({
				id: new Date().getTime(),
				fields: [getDCDefState()],
			});
			setAttributes({
				dcFields: newDcFields,
			});
			startOnboarding();
		}
	};

	return (
		<button
			type="button"
			className={`ultp-add-dc-button ${disabled ? 'ultp-add-dc-button-disabled' : ''} ${overlayMode ? 'ultp-add-dc-button-top' : ''} ${inverseColor ? 'ultp-add-dc-button-inverse' : ''}`}
			onClick={onClick}
		>
			<span style={{
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center'
			}}>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="19px"
					height="19px"
					viewBox="0 0 24 24"
				>
					<path
						fill="currentColor"
						d="M19 12.998h-6v6h-2v-6H5v-2h6v-6h2v6h6z"
					/>
				</svg>
			</span>
			<span>Add Custom Field</span>
		</button>
	);
}
