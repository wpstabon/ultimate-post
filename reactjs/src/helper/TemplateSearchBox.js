const TemplateSearchBox = ({searchQuery , setSearchQuery , setTemplateModule, changeStates}) => {
    return (
        <div className="ultp-design-search-wrapper">
            <input
                type="search"
                id="ultp-design-search-form"
                className="ultp-design-search-input"
                placeholder="Search for..."
                value={searchQuery}
                onChange={(e) => {
                    setSearchQuery && setSearchQuery(e.target.value)
                    setTemplateModule && setTemplateModule('')
                    changeStates && changeStates('search', e.target.value)
                }}
            />
        </div>
    );
};

export default TemplateSearchBox;