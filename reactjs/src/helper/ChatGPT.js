const { __ } = wp.i18n
const { useState, useEffect, useRef } = wp.element
const { rawHandler } = wp.blocks;
const { store } = wp.blockEditor;

const ChatGPT = (props) => {
    const { fromEditPostToolbar, onChange, value } = props;

    const askRef = useRef();

    const [tone, setTone] = useState('');
    const [style, setStyle] = useState('');
    const [language, setLanguage] = useState('');

    const [issues, setIssues] = useState('');
    const [isLoading, setLoading] = useState('');
    const [searchQuery , setSearchQuery] = useState('');
    const [isPopup, setPopup] = useState(props.isShow || false)
    const [searchText , setSearchText] = useState(value?.text ? (value.start == value.end ? value.text : value.text.substring(value.start, value.end)) : ''); 
    
    const fetchFromAPI = async (prompt, action) => {
        let issues = '',
            responseData = '';
        setLoading(action);

        try {
            let url = 'https://api.openai.com/v1/chat/completions';
            const apiKey = ultp_data.settings.chatgpt_secret_key;
            const chatgpt_model = ultp_data.settings.chatgpt_model;
            const chatgpt_response_time = ultp_data.settings.chatgpt_response_time || 35;
            const chatgpt_max_tokens = ultp_data.settings.chatgpt_max_tokens || 200;
            let data = '';

            if(chatgpt_model == 'gpt-3.5-turbo' || chatgpt_model == 'gpt-4') {
                data = {
                    model: chatgpt_model,
                    messages: [ { role: 'user', content: prompt }]
                };
            }
            else if ( chatgpt_model == 'text-davinci-002' || chatgpt_model == 'text-davinci-003') {
				url = 'https://api.openai.com/v1/completions'; 
				data = {
                    model : chatgpt_model,
					prompt : prompt,
					max_tokens : chatgpt_max_tokens,
                }
			}
            if (data) {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${apiKey}`,
                    },
                    timeout: chatgpt_response_time,
                    body: JSON.stringify(data),
                });

                const responseObj = await response.json(); 

                if (responseObj?.choices?.length > 0 && ( responseObj?.choices[0]?.message?.content || responseObj?.choices[0]?.text )) {
                    if ( chatgpt_model == 'text-davinci-002' || chatgpt_model == 'text-davinci-003') {
                        responseData = responseObj.choices[0].text;
                    } else {
                        responseData = responseObj.choices[0].message.content;
                    }
                } else if (responseObj?.error) {
                    if (responseObj?.error.code == 'invalid_api_key') {
                        issues = __('Your OpenAI API Secret Key is invalid. Please save a valid key.', 'ultimate-post')
                    } else if (responseObj?.error?.code == 'model_not_found' &&  responseObj?.error?.message?.includes('gpt-4') ) {
                        issues = __('You are not eligible to use GPT-4 model. Please contact OpenAI to get the access.', 'ultimate-post');
                    } else {
                        issues = __('Due to some error, we could not get response from OpenAI server. Please try again.', 'ultimate-post');
                    }
                }
            }
        } catch (error) {
            issues = __('Due to some error, we could not get response from OpenAI server. Please try again.', 'ultimate-post');
        }
        setIssues(issues);
        if (!issues) {
            setSearchText(responseData);
        }
        setLoading('');
    }

    const handleClickOutside = (e) => {
        if (e.keyCode === 27) {
            document.querySelector( '.ultp-builder-modal' ).remove();
            setPopup(false)
        }
    }

    useEffect(() => {
        document.addEventListener('keydown', handleClickOutside);
        return () => document.removeEventListener('keydown', handleClickOutside);
    },[]);

    const close = () => {
        const element = document.querySelector( '.ultp-builder-modal' );
        if (element.length > 0) {
            element.remove();
        }
        setPopup(false);
    }

    const appendContent = (val) => {
        if (fromEditPostToolbar) {
            // Form Topbar Elements Action
            const htmlBlock = wp.blocks.createBlock( 'core/html', {content: val} );
            wp.data.dispatch('core/block-editor').insertBlock( htmlBlock, wp.data.select('core/block-editor').getBlockCount() );
            wp.data.dispatch(store).replaceBlock(htmlBlock.clientId, rawHandler( {HTML: htmlBlock.attributes.content} ));
        } else {
            // Form Toolbar Bar Elements Action
            const newString = (value.start == value.end) ? val : value.text.substring(0, value.start) + val + value.text.substring(value.end);
            onChange(wp.richText.create({text: newString}));
        }
    }

    const generateContent = (prompt, action) => {
        const finalPrompt = prompt.replace("%s", searchText);
        if (isLoading) {
            setIssues('There is an ongoing process. Kindly hold on for a moment.')
        } else {
            fetchFromAPI(finalPrompt, action);
        }
    }

    const generatePrompt = () => {
        let prompt = askRef.current.value
        if (prompt) {
            if (style) {
                prompt += ' in ' + style + ' style'
            }
            if (tone) {
                prompt += ' using ' + tone + ' tone'
            }
            if (language) {
                prompt += ' translate in ' + language + ' language'
            }
            generateContent(prompt, 'extra')
        } else {
            setIssues('No prompt detected.');
        }
    }

    const GenerateOptions = (props) => {
        return (
            <div className="ultp-btn-item">
                <select 
                    value={ props.loading == 'tone' ? tone : (props.loading == 'style' ? style : language) }
                    onChange={(e) => {
                        if (searchText) {
                            generateContent(props.prompt.replace("%t", e.target.value), props.loading)
                        }
                        switch (props.loading) {
                            case 'tone':
                                setTone(e.target.value)
                            break;
                            case 'style':
                                setStyle(e.target.value)
                            break;
                            case 'language':
                                setLanguage(e.target.value)
                            break;
                            default:
                                break;
                        }
                    }}>
                    { props.options.map( (item, k)=> (
                        <option key={k} value={item.includes('-') ? '' : item.toLowerCase()}>{item}</option>
                    ))}
                </select>
                {loadingIcon(props.loading)}
            </div>
        )
    }

    const loadingIcon = (loading) => {
        return isLoading == loading ? <span className="chatgpt-loader"></span> : ''
    }

    return (
        <>
            { isPopup &&
                <div className="ultp-builder-modal-shadow ultp-chatgpt-popup">
                    <div className="ultp-popup-wrap">
                        
                        <div className="ultp-popup-header">
                            <div className="ultp-popup-filter-title">
                                <div className="ultp-popup-filter-image-head">
                                    <img src={ultp_data.url+'assets/img/addons/ChatGPT.svg'}/>
                                    <span>{ __("ChatGPT","ultimate-post")}</span>
                                </div>
                                <div className="ultp-popup-filter-sync-close">
                                    <button className="ultp-btn-close" onClick={() => close()} id="ultp-btn-close"><span className="dashicons dashicons-no-alt"></span></button>
                                </div>
                            </div>
                        </div>
                        <div className={`ultp-chatgpt-wrap ${!ultp_data.settings.chatgpt_secret_key ? 'ultp-chatgpt-nokey' : ''}`}>
                            <div className="ultp-chatgpt-input-container">
                                <div className="ultp-chatgpt-popup-content">
                                    { !ultp_data.settings.chatgpt_secret_key &&
                                        <div className="ultp-chatgpt-api-warning">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27.3" height="24"><defs><clipPath id="a"><path fill="#ffa62c" d="M0 0h27v24H0z"/></clipPath></defs><g clip-path="url(#a)"><path fill="#ffa62c" d="M27 21 15 0a1 1 0 0 0-3 0L0 21a1 1 0 0 0 1 2h25a1 1 0 0 0 1-2m-12-2a1 1 0 1 1 0-1 1 1 0 0 1 0 1m0-5a1 1 0 1 1-3 0V8a1 1 0 0 1 3 0Z"/></g></svg> Apply <a href="https://platform.openai.com/account/api-keys" target="blank"> API key </a> to use ChatGPT 
                                        </div> 
                                    }
                                    { issues &&
                                        <div className="ultp-error-notice">{issues}</div>
                                    }
                                    { searchText ?
                                        <textarea 
                                            type={`text`}
                                            onChange={e => setSearchText(e.target.value)}
                                            value={searchText} />
                                        :
                                        <div className={`ultp-chatgpt-search`}>
                                            <input 
                                                ref={askRef}
                                                type={`text`} 
                                                value={searchQuery} 
                                                placeholder={`Ask Anything`}
                                                onChange={e => setSearchQuery(e.target.value)}
                                                onKeyDown={e => {
                                                    if (e.key === 'Enter' && !isLoading) {
                                                        generatePrompt()
                                                    }
                                                }}
                                            />
                                            <button 
                                                className="button ultp-ask-chatgpt-button" 
                                                onClick={e => {
                                                    if (!isLoading) {
                                                        generatePrompt()
                                                    }
                                                }}>
                                                { isLoading ?
                                                    loadingIcon('extra')
                                                    :
                                                    <i className={`dashicons dashicons-search`}/>
                                                }  Ask ChatGPT
                                            </button>
                                        </div>
                                    }
                                    <div className="ultp-btn-items">
                                        { searchText &&
                                            <>
                                                <div className="ultp-btn-item" onClick={() => generateContent('rewrite this sentences "%s"','rewrite')}>Rewrite {loadingIcon('rewrite')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('improve this sentences "%s"','improve')}>Improve {loadingIcon('improve')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('make shorter this sentences "%s"','shorter')}>Make Shorter {loadingIcon('shorter')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('make longer this sentences "%s"','longer')}>Make Longer {loadingIcon('longer')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('summarize this sentences "%s"','summarize')}>Summarize {loadingIcon('summarize')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('Write a introduction of this sentences "%s"','introduction')}>Introduction {loadingIcon('introduction')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('Write a conclusion of this sentences "%s"','conclusion')}>Conclusion {loadingIcon('conclusion')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('Convert to Passive Voice of this sentences "%s"','passive')}>Convert to Passive Voice {loadingIcon('passive')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('Convert to Active Voice of this sentences "%s"','active')}>Convert to Active Voice {loadingIcon('active')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('make a paraphrase of this sentences "%s"','phrase')}>Paraphrase {loadingIcon('phrase')}</div>
                                                <div className="ultp-btn-item" onClick={() => generateContent('make a outline of this sentences "%s"','outline')}>Outline {loadingIcon('outline')}</div>
                                            </>
                                        }
                                        <GenerateOptions
                                            loading={`style`}
                                            prompt={'write this sentences using %t style "%s"'} 
                                            options={['- Writing Style -', 'Descriptive', 'Expository', 'Narrative', 'Normal', 'Persuasive']}/>
                                        <GenerateOptions 
                                            loading={`tone`}
                                            prompt={'write this sentences using %t tone "%s"'} 
                                            options={['- Writing Tone -', 'Assertive', 'Cooperative', 'Curious', 'Encouraging', 'Formal', 'Friendly', 'Informal', 'Optimistic', 'Surprised', 'Worried']}/>
                                        <GenerateOptions 
                                            loading={`language`}
                                            prompt={'translate this sentences in %t language "%s"'} 
                                            options={['- Writing Language -', 'Arabic', 'Bengali', 'English', 'French', 'German', 'Hindi', 'Italian', 'Indonesian', 'Japanese', 'Javanese', 'Korean', 'Mandarin Chinese', 'Marathi', 'Norwegian','Polish', 'Portuguese', 'Punjabi', 'Russian', 'Spanish', 'Telugu', 'Thai', 'Turkish', 'Urdu', 'Vietnamese', 'Wu Chinese' ]}/>
                                    </div>
                                    { searchText &&
                                        <div className="ultp-center">
                                            <span onClick={() => {
                                                setPopup(false)
                                                appendContent(searchText);
                                            }} className="ultp-btn ultp-btn-primary"> 
                                                <span className="dashicons dashicons-arrow-down-alt"/> {__('Import', 'ultimate-post')}
                                            </span>
                                            <span onClick={() => setSearchText('')} className="ultp-btn ultp-btn-transparent"> 
                                                <span className="dashicons dashicons-plus"></span> {__('New Prompt', 'ultimate-post')}
                                            </span>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default ChatGPT;