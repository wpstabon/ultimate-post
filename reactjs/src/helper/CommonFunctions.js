import GoogleFonts from "./fields/tools/GoogleFonts";

const { __ } = wp.i18n

const handlePostxPresets = (type, key, data, callbackFunc) => {
    wp.apiFetch({
        path: '/ultp/v1/postx_presets',
        method: 'POST',
        data: { 
            type: type,
            key: key,
            data: data,
        }
    }).then( res => {
        if(res.success) {
            if(type == 'set') {
                saveLocalValue(key, data);
            }
            callbackFunc && callbackFunc(res);
            
        }
    });
}

const generateColor2CSS = (v) => {
    let data = (v.clip ? '-webkit-background-clip: text; -webkit-text-fill-color: transparent;' : '');
    if(v.type == 'color') {
        data += (v.color ? 'background-color: ' + v.color + ';' : '')
    } else if(v.type == 'gradient' && v.gradient) {
        if(typeof v.gradient == 'object') {
            // for Compatiblity
            if( v.gradient.type == 'linear') {
                data += 'background-image : linear-gradient(' + v.gradient.direction + 'deg, ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
            } else {
                data += 'background-image : radial-gradient( circle at ' + v.gradient.radial + ' , ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
            }
        } else {
            data += 'background-image:'+v.gradient+';';
        }
    } else if(v.type == 'image') { 
        if(v.fallbackColor || v.color){
            data += 'background-color:'+(v.fallbackColor ?? v.color)+';';
        }
        if(v.image) {
            data += 'background-image: url("' + v.image + '");' +
                (v.position ? 'background-position-x:' + (v.position.x * 100)+ '%;background-position-y:' + (v.position.y * 100) + '%;' : '') +
                (v.attachment ? 'background-attachment:' + v.attachment + ';' : '') +
                (v.repeat ? 'background-repeat:' + v.repeat + ';' : '') +
                (v.size ? 'background-size:' + v.size + ';' : '');
        }
    }
    return data;
}

const postxPresetAttr = (type, data='', datafallback='') => {
    if(type == 'typoStacks') {
        return [
            [ { type: "sans-serif",     weight: 500, family: "Roboto" },                   { type: "sans-serif", weight: 400, family: "Roboto"} ],
            [ { type: "serif",          weight: 600, family: "Roboto Slab" },              { type: "sans-serif", weight: 400, family: "Roboto"} ],
            [ { type: "sans-serif",     weight: 600, family: "Jost" },                     { type: "sans-serif", weight: 400, family: "Jost"} ],
            [ { type: "display",        weight: 500, family: "Roboto" },                   { type: "sans-serif", weight: 400, family: "Roboto"} ],
            [ { type: "serif",          weight: 700, family: "Arvo" },                     { type: "sans-serif", weight: 400, family: "Roboto"} ],
            [ { type: "sans-serif",     weight: 500, family: "Roboto" },                   { type: "sans-serif", weight: 400, family: "Roboto"} ],
            [ { type: "sans-serif",     weight: 700, family: "Merriweather" },             { type: "sans-serif", weight: 400, family: "Merriweather"} ],
            [ { type: "sans-serifs",    weight: 500, family: "Oswald" },                   { type: "sans-serif", weight: 400, family: "Source Sans Pro"} ],
            [ { type: "display",        weight: 400, family: "Abril Fatface" },            { type: "sans-serif", weight: 400, family: "Poppins"} ],
            [ { type: "serif",          weight: 700, family: "Cardo" },                    { type: "sans-serif", weight: 400, family: "Inter"} ],
        ]

    } else if(type == 'multipleTypos') {
        return {
            Body_and_Others_typo: ['body_typo', 'paragraph_1_typo', 'paragraph_2_typo', 'paragraph_3_typo'],
            Heading_typo: ['heading_h1_typo', 'heading_h2_typo', 'heading_h3_typo', 'heading_h4_typo', 'heading_h5_typo', 'heading_h6_typo'],
        }
    } else if(type == 'presetTypoKeys') {
        return [
            'Heading_typo',
            'Body_and_Others_typo',
        ]
    } else if(type == 'colorStacks') {
        return [
            ['#f4f4ff', '#dddff8', '#B4B4D6', '#3323f0', '#4a5fff', '#1B1B47', '#545472', '#262657', '#10102e'],
            ['#ffffff', '#f7f4ed', '#D6D1B4', '#fab42a', '#f4cd4e', '#3B3118', '#6F6C53', '#483d1f', '#29230f'],
            ['#ffffff', '#eaf7ea', '#C2DBBF', '#3b9138', '#54a757', '#1E381A', '#586E56', '#23411f', '#162c11'],
            ['#fdf7ff', '#eadef5', '#C1B4D6', '#8749d0', '#995ede', '#301B42', '#635472', '#38204e', '#231133'],
            ['#fffcfc', '#fce5ec', '#D6B4BC', '#f01f50', '#ff5878', '#431B23', '#72545B', '#4d2029', '#36141b'],
            ['#ffffff', '#ecf3f8', '#B4C2D6', '#2890e8', '#6cb0f4', '#1D3347', '#4B586C', '#2c4358', '#10202b'],
            ['#f8f3ed', '#f2e2d0', '#D6C4B4', '#dd8336', '#f09f4d', '#3D2A1D', '#6E5F52', '#483324', '#2e1e11'],
            ['#ffffff', '#faf0f4', '#D6B4CF', '#d948a2', '#e56ab5', '#401B2E', '#725468', '#4e2239', '#290e1d'],
            ['#f2f7ea', '#e1e6c4', '#D2DBBF', '#829d46', '#a1c36b', '#30371A', '#5F6551', '#38401f', '#242e10'],
            ['#ffffff', '#e9f7f3', '#B5D1C7', '#3cbe8b', '#59d5a5', '#1C3D3F', '#46675E', '#20484b', '#153234']
        ];   
    } else if(type == 'presetColorKeys') {
        return [
            'Base_1_color',
            'Base_2_color',
            'Base_3_color',
            'Primary_color',
            'Secondary_color',
            'Tertiary_color',
            'Contrast_3_color',
            'Contrast_2_color',
            'Contrast_1_color',
        ];

    } else if(type == 'presetGradientKeys') {
        return ['Cold_Evening_gradient',  'Purple_Division_gradient',  'Over_Sun_gradient',  'Morning_Salad_gradient',  'Fabled_Sunset_gradient'];

    } else if(type == 'styleCss') {
        let styleCss = ':root { ';
        Object.keys(data).forEach((el, i) => {
            if(!['rootCSS', 'globalColorCSS'].includes(el)) {
                const key = el;
                const value = data[el]?.hasOwnProperty('openColor') ? ( data[el].type == 'color' ? data[el].color : data[el].gradient ) : ( data[el] || datafallback || '' );
                styleCss += `--postx_preset_${key}: ${value}; `;
            }
        });
        styleCss += ' }';
        return styleCss;
    } else if(type == 'typoCSS') {
        const multipleTypos = postxPresetAttr('multipleTypos');
        let googleFontsUrl = '';
        let typoCSS = ':root { ';
        const defaultFont = ['Arial', 'Tahoma', 'Verdana', 'Helvetica', 'Times New Roman', 'Trebuchet MS', 'Georgia']

        Object.keys(data).forEach((key, i) => {
            const currentObj = data[key];

            const parentExists = [...multipleTypos.Body_and_Others_typo, ...multipleTypos.Heading_typo].includes(key) ? true : false;

            if( !['rootCSS', 'presetTypoCSS'].includes(key) && typeof currentObj == 'object' && Object.keys(currentObj).length ) {
                const isGoogleFont = defaultFont.includes(currentObj['family']) ? false : true;

                if(isGoogleFont && currentObj['family'] && !currentObj['family'].includes('--postx_preset') && !googleFontsUrl.includes(currentObj['family'].replace(' ', '+') + ':')) {
                    googleFontsUrl += "@import url('https://fonts.googleapis.com/css?family=" + currentObj['family'].replace(' ', '+') + ':' + (GoogleFonts?.filter( o => { return o.n == currentObj['family'] } )[0]?.v || []).join(',') + "'); ";
                }

                if(!parentExists) {
                    // common var - use parent
                    typoCSS += currentObj['family'] ? `--postx_preset_${key}_font_family: ${currentObj['family']}; ` : '';
                    typoCSS += currentObj['family'] ? `--postx_preset_${key}_font_family_type: ${currentObj['type'] || 'sans-serif'}; ` : '';
                    typoCSS += currentObj['weight'] ? `--postx_preset_${key}_font_weight: ${currentObj['weight']}; ` : '';
                    typoCSS += currentObj['style'] ? `--postx_preset_${key}_font_style: ${currentObj['style']}; ` : '';
                    typoCSS += currentObj['decoration'] ? `--postx_preset_${key}_text_decoration: ${currentObj['decoration']}; ` : '';
                    typoCSS += currentObj['transform'] ? `--postx_preset_${key}_text_transform: ${currentObj['transform']}; ` : '';

                    typoCSS += currentObj['spacing']?.lg ? `--postx_preset_${key}_letter_spacing_lg: ${currentObj['spacing'].lg}${currentObj['spacing'].ulg || 'px'}; ` : '';
                    typoCSS += currentObj['spacing']?.sm ? `--postx_preset_${key}_letter_spacing_sm: ${currentObj['spacing'].sm}${currentObj['spacing'].usm || 'px'}; ` : '';
                    typoCSS += currentObj['spacing']?.xs ? `--postx_preset_${key}_letter_spacing_xs: ${currentObj['spacing'].xs}${currentObj['spacing'].uxs || 'px'}; ` : '';
                }
                
                // specefic var
                typoCSS += currentObj['size']?.lg ? `--postx_preset_${key}_font_size_lg: ${currentObj['size'].lg}${currentObj['size'].ulg || 'px'}; ` : '';
                typoCSS += currentObj['size']?.sm ? `--postx_preset_${key}_font_size_sm: ${currentObj['size'].sm}${currentObj['size'].usm || 'px'}; ` : '';
                typoCSS += currentObj['size']?.xs ? `--postx_preset_${key}_font_size_xs: ${currentObj['size'].xs}${currentObj['size'].uxs || 'px'}; ` : '';
                
                typoCSS += currentObj['height']?.lg ? `--postx_preset_${key}_line_height_lg: ${currentObj['height'].lg}${currentObj['height'].ulg || 'px'}; ` : '';
                typoCSS += currentObj['height']?.sm ? `--postx_preset_${key}_line_height_sm: ${currentObj['height'].sm}${currentObj['height'].usm || 'px'}; ` : '';
                typoCSS += currentObj['height']?.xs ? `--postx_preset_${key}_line_height_xs: ${currentObj['height'].xs}${currentObj['height'].uxs || 'px'}; ` : '';

                
            }
        });
        typoCSS += '}';
        return googleFontsUrl + typoCSS;
    } else if(type == 'font_load') { // from postx dashboard starter packs preview

        let googleFontsUrl = '';
        const defaultFont = ['Arial', 'Tahoma', 'Verdana', 'Helvetica', 'Times New Roman', 'Trebuchet MS', 'Georgia'];

        const coreData = datafallback ? ultp_dashboard_pannel : ultp_data;
        const enableGoogleFont = ((coreData.settings?.hasOwnProperty('disable_google_font') && coreData.settings['disable_google_font'] != 'yes') || !coreData.settings?.hasOwnProperty('disable_google_font')) ? true : false;

        if(  typeof data == 'object' && Object.keys(data).length ) {
            const isGoogleFont = defaultFont.includes(data['family']) ? false : true;
            if(enableGoogleFont && isGoogleFont && data['family']) {
                googleFontsUrl += "@import url('https://fonts.googleapis.com/css?family=" + data['family'].replace(' ', '+') + ':' + data['weight'] + "'); "
            }
        }
        return googleFontsUrl;
    } else if(type == 'font_load_all') {
        const fonrtArray =  [ 'Roboto', 'Roboto Slab', 'Jost', 'Arvo', 'Merriweather', 'Oswald', 'Abril Fatface', 'Cardo', 'Source Sans Pro', 'Poppins', 'Inter' ];
        const weightArray =  [ '400,500', '600', '400,600', '700', '400,700', '500', '400', '700', '400', '400', '400' ];
        let googleFontsUrl = '';
        const defaultFont = ['Arial', 'Tahoma', 'Verdana', 'Helvetica', 'Times New Roman', 'Trebuchet MS', 'Georgia'];

        const coreData = datafallback ? ultp_dashboard_pannel : ultp_data;
        const enableGoogleFont = ((coreData.settings?.hasOwnProperty('disable_google_font') && coreData.settings['disable_google_font'] != 'yes') || !coreData.settings?.hasOwnProperty('disable_google_font')) ? true : false;

        fonrtArray.forEach((el, i)=> {
            const isGoogleFont = defaultFont.includes(el) ? false : true;
            if(enableGoogleFont && isGoogleFont && el) {
                googleFontsUrl += "@import url('https://fonts.googleapis.com/css?family=" + el.replace(' ', '+') + ':' + weightArray[i] + "'); ";
            }

        });
        return googleFontsUrl;
    }  else if(type == 'bgCSS') {
        let bgCSS = {};
        const v = typeof data == 'object' ?  {...data} : {};
        if(v.type == 'color') {
            bgCSS['backgroundColor'] = v.color;
        } else if(v.type == 'gradient' && v.gradient) {
            let gradient = v.gradient;
            if(typeof v.gradient == 'object') {
                // for Compatiblity
                if( v.gradient.type == 'linear') {
                    gradient = 'linear-gradient(' + v.gradient.direction + 'deg, ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
                } else {
                    gradient = 'radial-gradient( circle at ' + v.gradient.radial + ' , ' + v.gradient.color1 + ' ' + v.gradient.start + '%,' + v.gradient.color2 + ' ' + v.gradient.stop + '%);'
                }
            }
            bgCSS['backgroundImage'] = gradient;
        } else if(v.type == 'image') { 
            if(v.fallbackColor || v.color) {
                bgCSS['backgroundColor'] = v.fallbackColor ?? v.color;
            }
            if(v.image) {
                bgCSS['backgroundImage'] = 'url("' + v.image + '")';
                if(v.position) {
                    bgCSS['backgroundPositionX'] = (v.position.x * 100)+'%';
                    bgCSS['backgroundPositionY'] = (v.position.y * 100)+'%';
                }
                if(v.attachment) { 
                    bgCSS['backgroundAttachments'] = v.attachment;
                }
                if(v.repeat) { 
                    bgCSS['backgroundRepeat'] = v.repeat;
                }
                if(v.size) {
                    bgCSS['backgroundSize'] = v.size;
                }
            }
        } else if(v.type == 'video' && v.fallback) {
            bgCSS['backgroundImage'] = 'url("' + v.fallback + '")';
            bgCSS['backgroundSize'] = 'cover'; 
            bgCSS['backgroundPosition'] = '50% 50%';
        }
        return bgCSS;
    } else if(type == 'globalCSS') {
        let globalCSS = `:root {
            --preset-color1: ${data['presetColor1'] ||'#037fff'}
            --preset-color2: ${data['presetColor2'] ||'#026fe0'}
            --preset-color3: ${data['presetColor3'] ||'#071323'}
            --preset-color4: ${data['presetColor4'] ||'#132133'}
            --preset-color5: ${data['presetColor5'] ||'#34495e'}
            --preset-color6: ${data['presetColor6'] ||'#787676'}
            --preset-color7: ${data['presetColor7'] ||'#f0f2f3'}
            --preset-color8: ${data['presetColor8'] ||'#f8f9fa'}
            --preset-color9: ${data['presetColor9'] ||'#ffffff'}
        }`;
        if(data['enablePresetColorCSS']) {
            globalCSS += `
            html body.postx-admin-page .editor-styles-wrapper,
            html body.postx-admin-page .editor-styles-wrapper p,
            html body.postx-page,
            html body.postx-page p,
            html.colibri-wp-theme body.postx-page, html.colibri-wp-theme body.postx-page p,
            body.block-editor-iframe__body, body.block-editor-iframe__body p
            { 
                color: var(--postx_preset_Contrast_2_color); 
            }
            html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
            html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
            html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
            html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
            html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
            html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6
            {
                color: var(--postx_preset_Contrast_1_color);
            }
            html.colibri-wp-theme body.postx-page h1,
            html.colibri-wp-theme body.postx-page h2,
            html.colibri-wp-theme body.postx-page h3,
            html.colibri-wp-theme body.postx-page h4,
            html.colibri-wp-theme body.postx-page h5,
            html.colibri-wp-theme body.postx-page h6 
            {
                color: var(--postx_preset_Contrast_1_color);
            }

            body.block-editor-iframe__body h1,
            body.block-editor-iframe__body h2,
            body.block-editor-iframe__body h3,
            body.block-editor-iframe__body h4,
            body.block-editor-iframe__body h5,
            body.block-editor-iframe__body h6
            { 
                color: var(--postx_preset_Contrast_1_color);
            }
            `;
            // body background 
            if(data['gbbodyBackground'].openColor) {
                globalCSS += `
                    html body.postx-admin-page .editor-styles-wrapper, html body.postx-page,
                    html body.postx-admin-page.block-editor-page.post-content-style-boxed .editor-styles-wrapper::before,
                    html.colibri-wp-theme body.postx-page,
                    body.block-editor-iframe__body
                    { ${generateColor2CSS(data['gbbodyBackground'])} }
                `;
            }
        }
        if(data['enablePresetTypoCSS']) {   // Heading Typo
            globalCSS += `
            html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
            html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
            html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
            html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
            html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
            html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6
            { 
                font-family: var(--postx_preset_Heading_typo_font_family),var(--postx_preset_Heading_typo_font_family_type); 
                font-weight: var(--postx_preset_Heading_typo_font_weight);
                font-style: var(--postx_preset_Heading_typo_font_style);
                text-transform: var(--postx_preset_Heading_typo_text_transform);
                text-decoration: var(--postx_preset_Heading_typo_text_decoration);
                letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_lg, normal);
            }
            html.colibri-wp-theme body.postx-page h1,
            html.colibri-wp-theme body.postx-page h2,
            html.colibri-wp-theme body.postx-page h3,
            html.colibri-wp-theme body.postx-page h4,
            html.colibri-wp-theme body.postx-page h5,
            html.colibri-wp-theme body.postx-page h6
            { 
                font-family: var(--postx_preset_Heading_typo_font_family),var(--postx_preset_Heading_typo_font_family_type); 
                font-weight: var(--postx_preset_Heading_typo_font_weight);
                font-style: var(--postx_preset_Heading_typo_font_style);
                text-transform: var(--postx_preset_Heading_typo_text_transform);
                text-decoration: var(--postx_preset_Heading_typo_text_decoration);
                letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_lg, normal);
            }
            body.block-editor-iframe__body h1,
            body.block-editor-iframe__body h2,
            body.block-editor-iframe__body h3,
            body.block-editor-iframe__body h4,
            body.block-editor-iframe__body h5,
            body.block-editor-iframe__body h6
            { 
                font-family: var(--postx_preset_Heading_typo_font_family),var(--postx_preset_Heading_typo_font_family_type); 
                font-weight: var(--postx_preset_Heading_typo_font_weight);
                font-style: var(--postx_preset_Heading_typo_font_style);
                text-transform: var(--postx_preset_Heading_typo_text_transform);
                text-decoration: var(--postx_preset_Heading_typo_text_decoration);
                letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_lg, normal);
            }
            html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
            html.colibri-wp-theme body.postx-page h1,
            body.block-editor-iframe__body h1
            { 
                font-size: var(--postx_preset_heading_h1_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h1_typo_line_height_lg, normal) !important;
            }
            html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
            html.colibri-wp-theme body.postx-page h2,
            body.block-editor-iframe__body h2
            { 
                font-size: var(--postx_preset_heading_h2_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h2_typo_line_height_lg, normal) !important;
            }
            html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
            html.colibri-wp-theme body.postx-page h3,
            body.block-editor-iframe__body h3
            { 
                font-size: var(--postx_preset_heading_h3_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h3_typo_line_height_lg, normal) !important;
            }
            html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
            html.colibri-wp-theme body.postx-page h4,
            body.block-editor-iframe__body h4
            { 
                font-size: var(--postx_preset_heading_h4_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h4_typo_line_height_lg, normal) !important;
            }
            html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
            html.colibri-wp-theme body.postx-page h5,
            body.block-editor-iframe__body h5
            { 
                font-size: var(--postx_preset_heading_h5_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h5_typo_line_height_lg, normal) !important;
            }
            html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6,
            html.colibri-wp-theme body.postx-page h6,
            body.block-editor-iframe__body h6
            { 
                font-size: var(--postx_preset_heading_h6_typo_font_size_lg, initial);
                line-height: var(--postx_preset_heading_h6_typo_line_height_lg, normal) !important;
            }

            @media (max-width: ${data['breakpointSm'] || 991 }px) {
                html body.postx-admin-page .editor-styles-wrapper h1 , html body.postx-page h1,
                html body.postx-admin-page .editor-styles-wrapper h2 , html body.postx-page h2,
                html body.postx-admin-page .editor-styles-wrapper h3 , html body.postx-page h3,
                html body.postx-admin-page .editor-styles-wrapper h4 , html body.postx-page h4,
                html body.postx-admin-page .editor-styles-wrapper h5 , html body.postx-page h5,
                html body.postx-admin-page .editor-styles-wrapper h6 , html body.postx-page h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_sm, normal);
                }
                html.colibri-wp-theme body.postx-page h1,
                html.colibri-wp-theme body.postx-page h2,
                html.colibri-wp-theme body.postx-page h3,
                html.colibri-wp-theme body.postx-page h4,
                html.colibri-wp-theme body.postx-page h5,
                html.colibri-wp-theme body.postx-page h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_sm, normal);
                }
                body.block-editor-iframe__body h1,
                body.block-editor-iframe__body h2,
                body.block-editor-iframe__body h3,
                body.block-editor-iframe__body h4,
                body.block-editor-iframe__body h5,
                body.block-editor-iframe__body h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_sm, normal);
                }

                html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
                html.colibri-wp-theme body.postx-page h1,
                body.block-editor-iframe__body h1
                {
                    font-size: var(--postx_preset_heading_h1_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h1_typo_line_height_sm, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
                html.colibri-wp-theme body.postx-page h2,
                body.block-editor-iframe__body h2
                {
                    font-size: var(--postx_preset_heading_h2_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h2_typo_line_height_sm, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
                html.colibri-wp-theme body.postx-page h3,
                body.block-editor-iframe__body h3
                {
                    font-size: var(--postx_preset_heading_h3_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h3_typo_line_height_sm, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
                html.colibri-wp-theme body.postx-page h4,
                body.block-editor-iframe__body h4
                {
                    font-size: var(--postx_preset_heading_h4_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h4_typo_line_height_sm, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
                html.colibri-wp-theme body.postx-page h5,
                body.block-editor-iframe__body h5
                {
                    font-size: var(--postx_preset_heading_h5_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h5_typo_line_height_sm, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6,
                html.colibri-wp-theme body.postx-page h6,
                body.block-editor-iframe__body h6
                {
                    font-size: var(--postx_preset_heading_h6_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_heading_h6_typo_line_height_sm, normal) !important;
                }
            }

            @media (max-width: ${data['breakpointXs'] || 767}px) {
                html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
                html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
                html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
                html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
                html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
                html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_xs, normal);
                }
                html.colibri-wp-theme body.postx-page h1,
                html.colibri-wp-theme body.postx-page h2,
                html.colibri-wp-theme body.postx-page h3,
                html.colibri-wp-theme body.postx-page h4,
                html.colibri-wp-theme body.postx-page h5,
                html.colibri-wp-theme body.postx-page h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_xs, normal);
                }
                body.block-editor-iframe__body h1,
                body.block-editor-iframe__body h2,
                body.block-editor-iframe__body h3,
                body.block-editor-iframe__body h4,
                body.block-editor-iframe__body h5,
                body.block-editor-iframe__body h6
                {
                    letter-spacing: var(--postx_preset_Heading_typo_letter_spacing_xs, normal);
                }
                html body.postx-admin-page .editor-styles-wrapper h1, html body.postx-page h1,
                html.colibri-wp-theme body.postx-page h1,
                body.block-editor-iframe__body h1
                {
                    font-size: var(--postx_preset_heading_h1_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h1_typo_line_height_xs, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h2, html body.postx-page h2,
                html.colibri-wp-theme body.postx-page h2,
                body.block-editor-iframe__body h2
                {
                    font-size: var(--postx_preset_heading_h2_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h2_typo_line_height_xs, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h3, html body.postx-page h3,
                html.colibri-wp-theme body.postx-page h3,
                body.block-editor-iframe__body h3
                {
                    font-size: var(--postx_preset_heading_h3_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h3_typo_line_height_xs, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h4, html body.postx-page h4,
                html.colibri-wp-theme body.postx-page h4,
                body.block-editor-iframe__body h4
                {
                    font-size: var(--postx_preset_heading_h4_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h4_typo_line_height_xs, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h5, html body.postx-page h5,
                html.colibri-wp-theme body.postx-page h5,
                body.block-editor-iframe__body h5
                {
                    font-size: var(--postx_preset_heading_h5_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h5_typo_line_height_xs, normal) !important;
                }
                html body.postx-admin-page .editor-styles-wrapper h6, html body.postx-page h6,
                html.colibri-wp-theme body.postx-page h6,
                body.block-editor-iframe__body h6
                {
                    font-size: var(--postx_preset_heading_h6_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_heading_h6_typo_line_height_xs, normal) !important;
                }
            }
            `;
        }
        if(data['enablePresetTypoCSS']) {       // Body and Paragraph
            globalCSS += `
            html body.postx-admin-page .editor-styles-wrapper, html body.postx-page,
            html body.postx-admin-page .editor-styles-wrapper p, html body.postx-page p,
            html.colibri-wp-theme body.postx-page, html.colibri-wp-theme body.postx-page p,
            body.block-editor-iframe__body, body.block-editor-iframe__body p
            { 
                font-family: var(--postx_preset_Body_and_Others_typo_font_family),var(--postx_preset_Body_and_Others_typo_font_family_type); 
                font-weight: var(--postx_preset_Body_and_Others_typo_font_weight);
                font-style: var(--postx_preset_Body_and_Others_typo_font_style);
                text-transform: var(--postx_preset_Body_and_Others_typo_text_transform);
                text-decoration: var(--postx_preset_Body_and_Others_typo_text_decoration);
                letter-spacing: var(--postx_preset_Body_and_Others_typo_letter_spacing_lg, normal);
                font-size: var(--postx_preset_body_typo_font_size_lg, initial);
                line-height: var(--postx_preset_body_typo_line_height_lg, normal) !important;
            }
            @media (max-width: ${data['breakpointSm'] || 991 }px) {
                .postx-admin-page .editor-styles-wrapper, .postx-page,
                .postx-admin-page .editor-styles-wrapper p, .postx-page p,
                html.colibri-wp-theme body.postx-page, html.colibri-wp-theme body.postx-page p,
                body.block-editor-iframe__body, body.block-editor-iframe__body p
                {
                    letter-spacing: var(--postx_preset_Body_and_Others_typo_letter_spacing_sm, normal);
                    font-size: var(--postx_preset_body_typo_font_size_sm, initial);
                    line-height: var(--postx_preset_body_typo_line_height_sm, normal) !important;
                }
            }
            @media (max-width: ${data['breakpointXs'] || 767}px) {
                .postx-admin-page .editor-styles-wrapper, .postx-page,
                .postx-admin-page .editor-styles-wrapper p, .postx-page p,
                html.colibri-wp-theme body.postx-page, html.colibri-wp-theme body.postx-page p,
                body.block-editor-iframe__body, body.block-editor-iframe__body p
                {
                    letter-spacing: var(--postx_preset_Body_and_Others_typo_letter_spacing_xs, normal);
                    font-size: var(--postx_preset_body_typo_font_size_xs, initial);
                    line-height: var(--postx_preset_body_typo_line_height_xs, normal) !important;
                }
            }
            `;
        }
        return globalCSS;
    }
}

const getTypoVariableObj = key => { // this is for CSS generator
    const multipleTypos = postxPresetAttr('multipleTypos');
    const parentKey = [...multipleTypos.Body_and_Others_typo, ...multipleTypos.Heading_typo].includes(key) ? [...multipleTypos.Body_and_Others_typo].includes(key) ? 'Body_and_Others_typo' : 'Heading_typo' : key;
    if(key) {
        return {
            openTypography: 1,
            presetTypo: key,
            // common typo - use parent key
            family: `var(--postx_preset_${parentKey}_font_family)`,
            type: `var(--postx_preset_${parentKey}_font_family_type)`,
            weight: `var(--postx_preset_${parentKey}_font_weight)`,
            spacing: {
                lg: `var(--postx_preset_${parentKey}_letter_spacing_lg, normal)`,
                sm: `var(--postx_preset_${parentKey}_letter_spacing_sm, normal)`,
                xs: `var(--postx_preset_${parentKey}_letter_spacing_xs, normal)`
            },
            decoration: `var(--postx_preset_${parentKey}_text_decoration)`,
            style: `var(--postx_preset_${parentKey}_font_style)`,
            transform: `var(--postx_preset_${parentKey}_text_transform)`,
            
            size: {
                lg: `var(--postx_preset_${key}_font_size_lg, initial)`,
                sm: `var(--postx_preset_${key}_font_size_sm, initial)`,
                xs: `var(--postx_preset_${key}_font_size_xs, initial)`
            },
            height: {
                lg: `var(--postx_preset_${key}_line_height_lg, normal)`,
                sm: `var(--postx_preset_${key}_line_height_sm, normal)`,
                xs: `var(--postx_preset_${key}_line_height_xs, normal)`
            },
        }
    }
    return { openTypography: 1 }
}

const getPresetTypoLists = (type) => {

    let presetTypos = JSON.parse(localStorage.getItem('ultpPresetTypos'));
    function callAfter(res) {
        if(res.data) {
            presetTypos = res.data;
            saveLocalValue('ultpPresetTypos', presetTypos);
            return typeof presetTypos == 'object' ? type == 'options' ? Object.keys(presetTypos).filter(item => item !== 'presetTypoCSS').map((item) => ( {value: item, label: item.replace('_typo', '').replaceAll('_', ' ')})) : Object.keys(presetTypos).filter(item => item !== 'presetTypoCSS') : [];
        }
    }
    if(!presetTypos) {
        handlePostxPresets('get', 'ultpPresetTypos', '', callAfter );
    } else {
        return typeof presetTypos == 'object' ?  type == 'options' ? Object.keys(presetTypos).filter(item => item !== 'presetTypoCSS').map((item) => ( {value: item, label: item.replace('_typo', '').replaceAll('_', ' ')})) : Object.keys(presetTypos).filter(item => item !== 'presetTypoCSS') : [];
    }
}

const getPresetColorLists = (type, key) => {
    let presetColors = JSON.parse(localStorage.getItem('ultpPresetColors'));
    function callAfter(res) {
        if(res.data) {
            presetColors = res.data;
            saveLocalValue('ultpPresetColors', presetColors);
            if(type=='colorcode') {
                key = key ?  key.replace('var(--postx_preset_', '').replace(')', '') : '';
                return presetColors[key];
            }
            return typeof presetColors == 'object' ? Object.keys(presetColors).filter(item => item !== 'rootCSS').map((item) => `var(--postx_preset_${item})`) : [];
        }
    }
    if(!presetColors) {
        handlePostxPresets('get', 'ultpPresetColors', '', callAfter );
    } else {
        if(type=='colorcode') {
            key = key ?  key.replace('var(--postx_preset_', '').replace(')', '') : '';
            return presetColors[key];
        }
        return typeof presetColors == 'object' ? Object.keys(presetColors).filter(item => item !== 'rootCSS').map((item) => `var(--postx_preset_${item})`) : [];
    }
}
const getPresetGradientLists = (type, key) => {
    let presetGradients = JSON.parse(localStorage.getItem('ultpPresetGradients'));
    function callAfter(res) {
        if(res.data) {
            presetGradients = res.data;
            saveLocalValue('ultpPresetGradients', presetGradients);
            if(type=='colorcode') {
                key = key ?  key.replace('var(--postx_preset_', '').replace(')', '') : '';
                return presetGradients[key];
            }
            return typeof presetGradients == 'object' ? Object.keys(presetGradients).filter(item => item !== 'rootCSS').map((item) => `var(--postx_preset_${item})`) : [];
        }
    }
    if(!presetGradients) {
        handlePostxPresets('get', 'presetGradients', '', callAfter );
    } else {
        if(type=='colorcode') {
            key = key ?  key.replace('var(--postx_preset_', '').replace(')', '') : '';
            return presetGradients[key];
        }
        return typeof presetGradients == 'object' ? Object.keys(presetGradients).filter(item => item !== 'rootCSS').map((item) => `var(--postx_preset_${item})`) : [];
    }
}

const saveLocalValue = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}
const handleTypoFieldPresetData = (value) => {
    return typeof value == 'string' && value.includes('--postx_preset') ? '' : value;
}

const presetKeyToLabel = (value, type) => {
    if(type == 'color' && value) {
        value = value.replace('var(--postx_preset_', '').replace('_color)', '').replaceAll('_', ' ');
    } else if(type == 'typo' && value) {
        value = value.replace('_typo', '').replaceAll('_', ' ');
    } else if( type == 'gradient' && value) {
        value = value.replace('var(--postx_preset_', '').replace('_gradient)', '').replaceAll('_', ' ');
    }
    return value;
}
const generateTypoStyle = ( key ) => {
    const obj = getTypoVariableObj(key);
    let styles = {
        fontFamily : obj.family,
        fontSize: obj.size.lg,
        fontWeight: obj.weight
    };
    return styles;
}

// Saves the block settings state for PostX Back button
function saveSettingsState() {
    const prevSelBlock = wp.data.select("core/block-editor").getSelectedBlock();
    localStorage.setItem("ultp_prev_sel_block", prevSelBlock?.clientId);
    localStorage.setItem("ultp_settings_save_state", "true");
}

const redirectToPostxSettingsPannel = (type) => {
    const scroolTo = type == 'typo' ? 350 : 0;
    const editType = wp.data.select("core/edit-site") ? 'core/edit-site' : 'core/edit-post';
    if(wp.data && wp.data.dispatch( editType) ) {
        // For back button
        saveSettingsState();

        wp.data.dispatch( editType ).openGeneralSidebar( 'ultp-postx-settings/postx-settings' );
        document.getElementsByClassName('interface-interface-skeleton__sidebar')[0]?.scrollTo({ top: scroolTo, behavior: 'smooth' })
    }
}

const jsonDataParsing = ( data, fallback={} ) => {
    try {
        return JSON.parse(data);
    } catch (e) {
        return fallback;
    }
}

export { 
    postxPresetAttr,
    getTypoVariableObj,
    getPresetTypoLists,
    handlePostxPresets,
    getPresetColorLists,
    getPresetGradientLists,
    saveLocalValue,
    handleTypoFieldPresetData,
    presetKeyToLabel,
    generateTypoStyle,
    redirectToPostxSettingsPannel,
    jsonDataParsing
}