import React, { useState } from "react";
import './tooltip.scss';

const Tooltip = (props) => {
	let timeout;
	const [active, setActive] = useState(false);

	const showToolTip = () => {
		timeout = setTimeout(() => {
			setActive(true);
		}, props.delay || 400);
	};

	const hideToolTip = () => {
		clearInterval(timeout);
		setActive(false);
	};

	return (
		<div className={`ultp-tooltip-wrapper ${props.extraClass}`} onMouseEnter={showToolTip} onMouseLeave={hideToolTip}>
			{props.children}
			{active && (
				<div className={`tooltip-content ${props.direction || "top"}`}>
					{props.content}
				</div>
			)}
		</div>
	);
};

export default Tooltip;