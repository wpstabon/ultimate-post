const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { addQueryArgs } = wp.url;
const { dateI18n } = wp.date;
const { dispatch } = wp.data;
const { TabPanel } = wp.components;
import IconPack from '../helper/fields/tools/IconPack';
import ExtraToolbarSettings from './Components/ExtraToolbarSettings';
import { isDCActive } from './dynamic_content';
import FieldGenerator from './FieldGenerator';
import icons from './icons';
import UltpLinkGenerator from './UltpLinkGenerator';

const proLink =
	'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=quick-query&utm_campaign=postx-dashboard';
let imageSize = [];
let postType = [];

export const typoIcon    = <img src={ultp_data.url + 'assets/img/toolbar/typography.svg'}/>;
export const colorIcon    = <img src={ultp_data.url + 'assets/img/toolbar/color.svg'}/>;
export const spaceingIcon = <img src={ultp_data.url + 'assets/img/toolbar/spacing.svg'}/>;
export const settingsIcon = <img src={ultp_data.url + 'assets/img/toolbar/setting.svg'}/>
export const styleIcon    = <img src={ultp_data.url + 'assets/img/toolbar/style.svg'}/>
export const metaTextIcon = <img src={ultp_data.url + 'assets/img/toolbar/meta-text.svg'}/>


// for caching in reload 
let reloadedTime = 0;
if( wp.data.select('core/edit-site') ) {
    reloadedTime = new Date().getTime();
    const fseTemplates = wp.data.select( 'core' )?.getEntityRecords( 'postType', 'wp_template' , {per_page: -1} ) || [];
    const fseTemplateParts = wp.data.select( 'core' )?.getEntityRecords( 'postType', 'wp_template_part' , {per_page: -1} ) || [];
}
const getAllTemplatePartsInPage = ( ) => {
    let partsData = {};
    const parts = getWindowDocument()?.querySelectorAll("*[data-type='core/template-part']");
    parts?.forEach(part => {
        const wpBlocks = part.querySelectorAll('.wp-block');
        const partId = part.dataset.block;
        const partIdBlock = [];
        wpBlocks?.forEach(block => {
            if ( block?.dataset?.type?.includes('ultimate-post/') && block?.dataset?.block ) {
                partIdBlock.push(block.dataset.block)
            }
        });
        if ( partIdBlock.length ) {
            partsData = {
                ...partsData,
                hasItems: "yes",
                [partId]: partIdBlock
            }
        }
    });
    return partsData;
}

const fetchTaxonomy = () => {
	localStorage.setItem('ultpDevice', 'lg');
	const query = addQueryArgs('/ultp/common_data', { wpnonce: ultp_data.security });
	wp.apiFetch({ path: query }).then((obj) => {
		localStorage.setItem('ultpTaxonomy', JSON.stringify(obj.taxonomy));
		localStorage.setItem(
			'ultpGlobal' + ultp_data.blog,
			JSON.stringify(obj.global)
		);
		const objImg = JSON.parse(obj.image);
		Object.keys(objImg).forEach(function (key) {
			imageSize.push({ value: key, label: objImg[key] });
		});

		const objPost = JSON.parse(obj.posttype);
		Object.keys(objPost).forEach(function (key) {
			postType.push({ value: key, label: objPost[key] });
		});
		if (ultp_data.archive && ultp_data.archive == 'archive') {
			postType.unshift({
				value: 'archiveBuilder',
				label: 'Archive Builder',
				link: proLink,
			});
		}
		postType.unshift({
			value: 'posts',
			label: 'Specific Posts',
			link: proLink,
		});
		postType.unshift({
			value: 'customPosts',
			label: 'Custom Selections',
			link: proLink,
		});
	});
};
fetchTaxonomy();

const filterFields = (include, exclude, args) => {
	let data = args.slice(0);
	if (exclude) {
		if (exclude === '__all') {
			data = [];
		} else {
			data = data.filter((el) => !exclude.includes(el.key));
		}
	}
	if (include) {
		include.forEach((val) => {
			if (typeof val === 'string') {
				const newData = args.find((el) => el.key === val);
				if (newData) {
					data.push(newData);
				}
			} else {
				if (val.data) {
					if (data[val.position]) {
						if (
							data[val.position].key != val.data.key ||
							val.data.type === 'separator'
						) {
							data.splice(val.position, 0, val.data);
						}
					} else {
						data.push(val.data);
					}
				}
			}
		});
	}
	return data;
};

const filterFields2 = (props, args) => {
	const { data, opType } = props;

	if (opType === 'keep') {
		const incData = [];
		args.forEach((arg) => {
			if (data.includes(arg.key) || arg.type === 'separator') {
				incData.push(arg);
			}
		});
		return incData;
	} else if (opType === 'discard') {
		return args.filter((arg) => {
			return !data.includes(arg.key) || arg.type === 'separator';
		});
	}

	return [...args];
};

// Common Snippet Start
const metaList = {
	type: 'select',
	key: 'metaList',
	label: __('Meta', 'ultimate-post'),
	multiple: true,
	options: [
		{ value: 'metaAuthor', label: __('Author', 'ultimate-post') },
		{ value: 'metaDate', label: __('Date', 'ultimate-post') },
		{
			value: 'metaDateModified',
			label: __('Modified Date', 'ultimate-post'),
		},
		{ value: 'metaComments', label: __('Comment', 'ultimate-post') },
		{ value: 'metaView', label: __('View Count', 'ultimate-post') },
		{ value: 'metaTime', label: __('Date Time', 'ultimate-post') },
		{ value: 'metaRead', label: __('Reading Time', 'ultimate-post') },
	],
};
// Common Snippet Close

// Category - Style
export const CategoryStyleArg = [
	{
		type: 'select',
		key: 'taxonomy',
		pro: true,
		label: __('Taxonomy', 'ultimate-post'),
		options: [],
	},
	{
		type: 'select',
		key: 'catStyle',
		label: __('Category Style', 'ultimate-post'),
		options: [
			{ value: 'classic', label: __('Classic', 'ultimate-post') },
			{ value: 'borderLeft', label: __('Left Border', 'ultimate-post') },
			{
				value: 'borderRight',
				label: __('Right Border', 'ultimate-post'),
			},
			{
				value: 'borderBoth',
				label: __('Both Side Border', 'ultimate-post'),
			},
		],
	},
	{
		type: 'select',
		key: 'catPosition',
		label: __('Category Position', 'ultimate-post'),
		options: [
			{ value: 'aboveTitle', label: __('Above Title', 'ultimate-post') },
			{
				value: 'topLeft',
				label: __('Over Image(Top Left)', 'ultimate-post'),
			},
			{
				value: 'topRight',
				label: __('Over Image(Top Right)', 'ultimate-post'),
			},
			{
				value: 'bottomLeft',
				label: __('Over Image(Bottom Left)', 'ultimate-post'),
			},
			{
				value: 'bottomRight',
				label: __('Over Image(Bottom Right)', 'ultimate-post'),
			},
			{
				value: 'centerCenter',
				label: __('Over Image(Center)', 'ultimate-post'),
			},
		],
	},
	{
		type: 'range',
		key: 'catLineWidth',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		label: __('Line Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'catLineSpacing',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		label: __('Line Spacing', 'ultimate-post'),
	},
	{
		type: 'typography',
		key: 'catTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'customCatColor',
		label: __('Specific Color', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'linkbutton',
		key: 'seperatorLink',
		placeholder: __('Link', 'ultimate-post'),
		label: __('Category Specific (Pro)', 'ultimate-post'),
		text: 'Choose Color',
	},
	{
		type: 'toggle',
		key: 'onlyCatColor',
		label: __('Only Category Text Color (Pro)', 'ultimate-post'),
	},
	{
		type: 'tab',
		key: 'cTab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'catColor',
						label: __('Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'catBgColor',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'catLineColor',
						label: __('Line Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'catBorder',
						label: __('Border', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'catHoverColor',
						label: __('Hover Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'catBgHoverColor',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'catLineHoverColor',
						label: __('Line Hover Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'catHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'catRadius',
		label: __('Border Radius', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'catSacing',
		label: __('Spacing', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'catPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'range',
		key: 'maxTaxonomy',
		min: 0,
		max: 150,
		step: 1,
		label: __('Maximum Number of Taxonomy', 'ultimate-post'),
	},
];

export const categorySettings = [
	'taxonomy',
	'catPosition',
	'catStyle',
	'maxTaxonomy',
];

export const categoryStyle = [
	'catLineWidth',
	'catLineSpacing',
	'catTypo',
	'customCatColor',
	'seperatorLink',
	'onlyCatColor',
	'cTab',
	'catRadius',
	'catSacing',
	'catPadding',
];

// Button - Style
const ButtonStyleArg = [
	{
		type: 'typography',
		key: 'btnTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'btnColor',
						label: __('Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'btnBgColor',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'btnBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'btnRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'btnShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'btnHoverColor',
						label: __('Hover Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'btnBgHoverColor',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'btnHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'btnHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'btnHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
				],
			},
		],
	},

	{
		type: 'dimension',
		key: 'btnSacing',
		label: __('Spacing', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'btnPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// ReadMore - Style
export const ReadMoreStyleArg = [
	{
		type: 'text',
		key: 'readMoreText',
		label: __('Read More Text', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'readMoreIcon',
		label: __('Icon Style', 'ultimate-post'),
		options: [
			{ value: '', label: __('None', 'ultimate-post'), icon: '' },
			{
				value: 'rightAngle',
				label: __('Angle', 'ultimate-post'),
				icon: 'rightAngle',
			},
			{
				value: 'rightAngle2',
				label: __('Arrow', 'ultimate-post'),
				icon: 'rightAngle2',
			},
			{
				value: 'rightArrowLg',
				label: __('Long Arrow', 'ultimate-post'),
				icon: 'rightArrowLg',
			},
		],
	},
	{
		type: 'typography',
		key: 'readMoreTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'readMoreIconSize',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __('Icon Size', 'ultimate-post'),
	},
	{
		type: 'tab',
		key: 'rmTab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'readMoreColor',
						label: __('Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'readMoreBgColor',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'readMoreBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'readMoreRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'readMoreHoverColor',
						label: __('Hover Color', 'ultimate-post'),
					},
					{
						type: 'color2',
						key: 'readMoreBgHoverColor',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'readMoreHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'readMoreHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'readMoreSacing',
		label: __('Spacing', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'readMorePadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const readMoreSettings = ['readMoreText', 'readMoreIcon'];

export const readMoreStyle = [
	'readMoreTypo',
	'readMoreIconSize',
	'rmTab',
	'readMoreSacing',
	'readMorePadding',
];

//counter circle
const CounterStyleArg = [
	{
		type: 'typography',
		key: 'counterTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{ type: 'color', key: 'counterColor', label: __('Color', 'ultimate-post') },
	{
		type: 'color2',
		key: 'counterBgColor',
		label: __('Background Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'counterWidth',
		min: 0,
		max: 300,
		step: 1,
		label: __('Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'counterHeight',
		min: 0,
		max: 300,
		step: 1,
		label: __('Height', 'ultimate-post'),
	},
	{
		type: 'border',
		key: 'counterBorder',
		label: __('Border', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'counterRadius',
		label: __('Border Radius', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Arrow - Content
const ArrowContentArg = [
	{
		type: 'select',
		key: 'arrowStyle',
		label: __('Arrow Style', 'ultimate-post'),
		options: [
			{
				value: 'leftAngle#rightAngle',
				label: __('Style 1', 'ultimate-post'),
				icon: 'leftAngle',
			},
			{
				value: 'leftAngle2#rightAngle2',
				label: __('Style 2', 'ultimate-post'),
				icon: 'leftAngle2',
			},
			{
				value: 'leftArrowLg#rightArrowLg',
				label: __('Style 3', 'ultimate-post'),
				icon: 'leftArrowLg',
			},
		],
	},

	// Style
	{ type: 'separator', key: 'separatorStyle' },

	{
		type: 'range',
		key: 'arrowSize',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __('Size', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'arrowWidth',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __('Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'arrowHeight',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __('Height', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'arrowVartical',
		min: -200,
		max: 1000,
		step: 1,
		responsive: true,
		unit: true,
		label: __('Vertical Position', 'ultimate-post'),
	},

	{ type: 'separator', key: 'separatorStyle' },
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'arrowColor',
						label: __('Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'arrowBg',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'arrowBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'arrowRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'arrowShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'arrowHoverColor',
						label: __('Hover Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'arrowHoverBg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'arrowHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'arrowHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'arrowHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
				],
			},
		],
	},
];

export const arrowSpacing = [
	'arrowSize',
	'arrowWidth',
	'arrowHeight',
	'arrowVartical',
];

// Dot - Style
const DotStyleArg = [
	{
		type: 'range',
		key: 'dotSpace',
		min: 0,
		max: 100,
		step: 1,
		unit: true,
		responsive: true,
		label: __('Space', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'dotVartical',
		min: -200,
		max: 700,
		step: 1,
		unit: true,
		responsive: true,
		label: __('Vertical Position', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'dotHorizontal',
		min: -800,
		max: 800,
		step: 1,
		unit: true,
		responsive: true,
		label: __('Horizontal Position', 'ultimate-post'),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'range',
						key: 'dotWidth',
						min: 0,
						max: 100,
						step: 1,
						responsive: true,
						label: __('Width', 'ultimate-post'),
					},
					{
						type: 'range',
						key: 'dotHeight',
						min: 0,
						max: 100,
						step: 1,
						unit: true,
						responsive: true,
						label: __('Height', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'dotBg',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'dotBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'dotRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'dotShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Active', 'ultimate-post'),
				options: [
					{
						type: 'range',
						key: 'dotHoverWidth',
						min: 0,
						max: 100,
						step: 1,
						responsive: true,
						label: __('Width', 'ultimate-post'),
					},
					{
						type: 'range',
						key: 'dotHoverHeight',
						min: 0,
						max: 100,
						step: 1,
						unit: true,
						responsive: true,
						label: __('Height', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'dotHoverBg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'dotHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'dotHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'dotHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
				],
			},
		],
	},
];

export const dotSpacing = ['dotSpace', 'dotVartical', 'dotHorizontal'];

const quickQueryEmpty = [
	{ value: '', label: __('- Select Advance Query -', 'ultimate-post') },
];
const quickQueryAction = [
	{
		value: 'related_tag',
		label: __('Related by Tag (Single Post)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'related_category',
		label: __('Related by Category (Single Post)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'related_cat_tag',
		label: __('Related by Category & Tag (Single Post)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'related_posts',
		label: __('Related by Query Builder (Single Post)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
];
const quickQuery = [
	{
		value: 'popular_post_1_day_view',
		label: __('Trending Today', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_7_days_view',
		label: __('This Week’s Popular Posts', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_30_days_view',
		label: __('Top Posts of the Month', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'popular_post_all_times_view',
		label: __('All-Time Favorites', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post',
		label: __('Random Posts', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post_7_days',
		label: __('Random Posts (7 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'random_post_30_days',
		label: __('Random Posts (30 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'latest_post_published',
		label: __('Latest Posts - Published Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'latest_post_modified',
		label: __('Latest Posts - Last Modified Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'oldest_post_published',
		label: __('Oldest Posts - Published Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'oldest_post_modified',
		label: __('Oldest Posts - Last Modified Date', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'alphabet_asc',
		label: __('Alphabetical ASC', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'alphabet_desc',
		label: __('Alphabetical DESC', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'sticky_posts',
		label: __('Sticky Post', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment',
		label: __('Most Comments', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_1_day',
		label: __('Most Comments (1 Day)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_7_days',
		label: __('Most Comments (7 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
	{
		value: 'most_comment_30_days',
		label: __('Most Comments (30 Days)', 'ultimate-post'),
		pro: true,
		link: proLink,
	},
];

// Query - Content
let __option = [
	{ value: 'title', label: __('Title (Alphabetical)', 'ultimate-post') },
	{ value: 'date', label: __('Date (Published)', 'ultimate-post') },
	{ value: 'modified', label: __('Date (Last Modified)', 'ultimate-post') },
	{ value: 'rand', label: __('Random Posts', 'ultimate-post') },
	{
		value: 'post__in',
		label: __('Post In (Show Post by Post ID Order)', 'ultimate-post'),
	},
	{
		value: 'menu_order',
		label: __('Menu Order (Show Page by Page Attributes)', 'ultimate-post'),
	},
	{ value: 'comment_count', label: __('Comment Count', 'ultimate-post') },
];
if (ultp_data?.archive != 'archive') {
	__option.push({
		value: 'meta_value_num',
		label: __('Meta Value Number', 'ultimate-post'),
	});
}
const QueryArg = [
	{
		type: 'select',
		key: 'queryType',
		label: __('Sources ( Post Type )', 'ultimate-post'),
		options: postType,
	},
	{
		type: 'search',
		key: 'queryPosts',
		pro: true,
		search: 'posts',
		label: __('Choose Specific Posts', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'queryQuick',
		label: __('Quick Query', 'ultimate-post'),
		options:
			ultp_data.post_type != 'page'
				? [...quickQueryEmpty, ...quickQueryAction, ...quickQuery]
				: [...quickQueryEmpty, ...quickQuery, ...quickQueryAction],
	},
	{
		type: 'search',
		key: 'queryCustomPosts',
		pro: true,
		search: 'allpost',
		label: __('Add Custom Sections', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'queryNumPosts',
		min: -1,
		max: 100,
		label: __('Number of Posts (per Page) ', 'ultimate-post'),
		responsive: true,
	},
	{
		type: 'range',
		key: 'queryOffset',
		min: 0,
		max: 50,
		step: 1,
		label: __('Offset Post', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'queryOrderBy',
		label: __('Order By', 'ultimate-post'),
		options: __option,
	},
	{
		type: 'tag',
		key: 'queryOrder',
		label: __('Order (ASC/DESC)', 'ultimate-post'),
		options: [
			{ value: 'asc', label: __('Ascending', 'ultimate-post') },
			{ value: 'desc', label: __('Descending', 'ultimate-post') },
		],
	},
	{
		type: 'toggle',
		key: 'querySticky',
		label: __('Ignore Sticky Posts', 'ultimate-post'),
	},

	{ type: 'separator' },

	{
		type: 'select',
		key: 'queryTax',
		label: __('Taxonomy', 'ultimate-post'),
		options: [],
	},
	{
		type: 'search',
		key: 'queryTaxValue',
		label: __('Taxonomy Value', 'ultimate-post'),
		multiple: true,
		search: 'taxvalue',
	},
	{
		type: 'search',
		key: 'queryAuthor',
		search: 'author',
		label: __('Author', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'queryUnique',
		label: __('Unique Content Group (Frontend Only)', 'ultimate-post'),
		pro: true,
		options: [
			{ value: '', label: __('Disable', 'ultimate-post') },
			{ value: 'group1', label: __('Content Group 1', 'ultimate-post') },
			{ value: 'group2', label: __('Content Group 2', 'ultimate-post') },
			{ value: 'group3', label: __('Content Group 3', 'ultimate-post') },
			{ value: 'group4', label: __('Content Group 4', 'ultimate-post') },
			{ value: 'group5', label: __('Content Group 5', 'ultimate-post') },
		],
		help: __(
			'Unique Content Group solves the repetition of posts on multiple blocks.[Frontend Only]',
			'ultimate-post'
		),
	},

	{ type: 'separator' },

	{
		type: 'tag',
		key: 'queryRelation',
		label: __('Taxonomy Relation', 'ultimate-post'),
		options: [
			{ value: 'OR', label: __('OR', 'ultimate-post') },
			{ value: 'AND', label: __('AND', 'ultimate-post') },
		],
	},
	{ type: 'text', key: 'metaKey', label: __('Meta Key', 'ultimate-post') },
	{
		type: 'search',
		key: 'queryExclude',
		search: 'postExclude',
		label: __('Exclude Post', 'ultimate-post'),
	},
	{
		type: 'search',
		key: 'queryExcludeTerm',
		search: 'taxExclude',
		label: __('Exclude Term', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'search',
		key: 'queryExcludeAuthor',
		search: 'author',
		label: __('Exclude Author', 'ultimate-post'),
		pro: true,
	},
];

// General - Content
const GeneralArg = [
	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 7,
		step: 1,
		responsive: true,
		label: __('Columns', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 120,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem'],
		label: __('Column Gap', 'ultimate-post'),
	},
	{
		type: 'alignment',
		key: 'contentAlign',
		responsive: false,
		label: __('Alignment', 'ultimate-post'),
		disableJustify: true,
	},
	{
		type: 'tag',
		key: 'contentTag',
		label: __('Content Tag', 'ultimate-post'),
		options: [
			{ value: 'article', label: 'article' },
			{ value: 'section', label: 'section' },
			{ value: 'div', label: 'div' },
		],
	},
	{
		type: 'toggle',
		key: 'openInTab',
		label: __('Links Open in New Tabs', 'ultimate-post'),
	},
];

const GeneralArgWithQuery = [
	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 7,
		step: 1,
		responsive: true,
		label: __('Columns', 'ultimate-post'),
		_inline: true,
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 120,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem'],
		label: __('Column Gap', 'ultimate-post'),
		_inline: true,
	},

	{
		type: 'separator',
		fancy: true,
		label: __('Query Loop', 'product-blocks'),
	},
	...QueryArg,

	{ type: 'separator' },

	{
		type: 'alignment',
		key: 'contentAlign',
		responsive: false,
		label: __('Alignment', 'ultimate-post'),
		disableJustify: true,
	},
	{
		type: 'tag',
		key: 'contentTag',
		label: __('Content Tag', 'ultimate-post'),
		options: [
			{ value: 'article', label: 'article' },
			{ value: 'section', label: 'section' },
			{ value: 'div', label: 'div' },
		],
	},
	{
		type: 'toggle',
		key: 'openInTab',
		label: __('Links Open in New Tabs', 'ultimate-post'),
	},
];

const SpacingArgs = [
	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 7,
		step: 1,
		responsive: true,
		label: __('Columns', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 120,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem'],
		label: __('Column Gap', 'ultimate-post'),
	},

	{ type: 'separator', key: 'spaceSep' },

	{
		type: 'dimension',
		key: 'wrapOuterPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'wrapMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const GridAlignment = [
	{
		icon: <span className="dashicons dashicons-editor-alignleft"></span>,
		title: __('Left', 'ultimate-post'),
		value: 'left',
	},
	{
		icon: <span className="dashicons dashicons-editor-aligncenter"></span>,
		title: __('Center', 'ultimate-post'),
		value: 'center',
	},
	{
		icon: <span className="dashicons dashicons-editor-alignright"></span>,
		title: __('Right', 'ultimate-post'),
		value: 'right',
	},
];

// Content - Style
const OverlayStyleArg = [
	{
		type: 'tag',
		key: 'overlayContentPosition',
		label: __('Content Position', 'ultimate-post'),
		disabled: true,
		options: [
			{ value: 'topPosition', label: __('Top', 'ultimate-post') },
			{ value: 'middlePosition', label: __('Middle', 'ultimate-post') },
			{ value: 'bottomPosition', label: __('Bottom', 'ultimate-post') },
		],
	},
	{
		type: 'color2',
		key: 'overlayBgColor',
		label: __('Content Background', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'dimension',
		key: 'overlayWrapPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Wrap - Style
const WrapStyleArg = [
	{
		type: 'range',
		key: 'contenWraptWidth',
		min: 0,
		max: 800,
		step: 1,
		unit: true,
		responsive: true,
		label: __('Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'contenWraptHeight',
		min: 0,
		max: 500,
		step: 1,
		unit: true,
		responsive: true,
		label: __('Height', 'ultimate-post'),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'contentWrapBg',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'contentWrapBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'contentWrapRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'contentWrapShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'contentWrapHoverBg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'contentWrapHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'contentWrapHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'contentWrapHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'contentWrapInnerPadding',
		label: __('Inner Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'contentWrapPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// entry header - Style
const WrapHeadingStyleArg = [
	{
		type: 'range',
		key: 'headerWidth',
		min: 0,
		max: 600,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem', '%'],
		label: __('Width', 'ultimate-post'),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'headerWrapBg',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'headerWrapBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'headerWrapRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'headerWrapHoverBg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'headerWrapHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'headerWrapHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'range',
		key: 'headerSpaceX',
		min: -100,
		max: 100,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem', '%'],
		label: __('Space X', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'headerSpaceY',
		min: 0,
		max: 150,
		step: 1,
		responsive: true,
		label: __('Space Y', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'headerWrapPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Heading - Content
export const HeadingContentArg = [
	// {type: 'notice', status: "error", text: "Use the PostX Heading Block instead."},
	// {
	//     type:'text_dropdown',
	//     metaKey:'cMetaText',
	//     key:'headingText',
	//     chatgpt:true,
	//     label:__('Add Heading','ultimate-post')
	// },
	{
		type: 'alignment',
		key: 'headingAlign',
		label: __('Alignment', 'ultimate-post'),
		disableJustify: true,
	},
	{
		type: 'select',
		key: 'headingStyle',
		label: __('Heading Style', 'ultimate-post'),
		image: true,
		options: [
			{
				value: 'style1',
				label: __('Style 1', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle1.png',
			},
			{
				value: 'style2',
				label: __('Style 2', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle2.png',
			},
			{
				value: 'style3',
				label: __('Style 3', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle3.png',
			},
			{
				value: 'style4',
				label: __('Style 4', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle4.png',
			},
			{
				value: 'style5',
				label: __('Style 5', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle5.png',
			},
			{
				value: 'style6',
				label: __('Style 6', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle6.png',
			},
			{
				value: 'style7',
				label: __('Style 7', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle7.png',
			},
			{
				value: 'style8',
				label: __('Style 8', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle8.png',
			},
			{
				value: 'style9',
				label: __('Style 9', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle9.png',
			},
			{
				value: 'style10',
				label: __('Style 10', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle10.png',
			},
			{
				value: 'style11',
				label: __('Style 11', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle11.png',
			},
			{
				value: 'style12',
				label: __('Style 12', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle12.png',
			},
			{
				value: 'style13',
				label: __('Style 13', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle13.png',
			},
			{
				value: 'style14',
				label: __('Style 14', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle14.png',
			},
			{
				value: 'style15',
				label: __('Style 15', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle15.png',
			},
			{
				value: 'style16',
				label: __('Style 16', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle16.png',
			},
			{
				value: 'style17',
				label: __('Style 17', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle17.png',
			},
			{
				value: 'style18',
				label: __('Style 18', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle18.png',
			},
			{
				value: 'style19',
				label: __('Style 19', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle19.png',
			},
			{
				value: 'style20',
				label: __('Style 20', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle20.png',
			},
			{
				value: 'style21',
				label: __('Style 21', 'ultimate-post'),
				img: 'assets/img/layouts/heading/hstyle21.png',
			},
		],
	},
	{
		type: 'tag',
		key: 'headingTag',
		label: __('Heading Tag', 'ultimate-post'),
		options: [
			{ value: 'h1', label: 'H1' },
			{ value: 'h2', label: 'H2' },
			{ value: 'h3', label: 'H3' },
			{ value: 'h4', label: 'H4' },
			{ value: 'h5', label: 'H5' },
			{ value: 'h6', label: 'H6' },
			{ value: 'span', label: 'span' },
			{ value: 'p', label: 'p' },
		],
	},
	{
		type: 'text',
		key: 'headingBtnText',
		label: __('Button Text', 'ultimate-post'),
	},
	{ type:'text',key:'headingURL', label:__('Heading URL','ultimate-post'), disableIf: ()=> isDCActive() },
	// { type:'text_dropdown',key:'headingURL',metaKey:"cMetaUrl", label:__('Heading URL','ultimate-post') },
	// Style
	// { type:'separator',key:'separatorStyle',label:__('Heading Style','ultimate-post') },
	{
		type: 'typography',
		key: 'headingTypo',
		label: __('Heading Typography', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingColor',
		label: __('Heading Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingBg',
		label: __('Heading Background', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingBg2',
		label: __('Heading Background 2', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingBorderBottomColor',
		label: __('Heading Border Color', 'ultimate-post'),
		clip: true,
	},
	{
		type: 'color',
		key: 'headingBorderBottomColor2',
		label: __('Heading Border Color 2', 'ultimate-post'),
		clip: true,
	},
	{
		type: 'range',
		key: 'headingBorder',
		label: __('Heading Border Size', 'ultimate-post'),
		min: 1,
		max: 20,
		step: 1,
	},
	{
		type: 'typography',
		key: 'headingBtnTypo',
		label: __('Heading Button Typography', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingBtnColor',
		label: __('Heading Button Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'headingBtnHoverColor',
		label: __('Heading Button Hover Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'headingSpacing',
		label: __('Heading Spacing', 'ultimate-post'),
		min: 1,
		max: 150,
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'headingRadius',
		label: __('Heading Border Radius', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'headingPadding',
		label: __('Heading Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},

	// { type:'separator',key:'subHeadingSeparator',label:__('Sub Heading','ultimate-post') },
	{
		type: 'toggle',
		key: 'subHeadingShow',
		label: __('Sub Heading', 'ultimate-post'),
	},
	// { type:'text_dropdown',isTextArea: true,metaKey:'cMetaSubText',key:'subHeadingText', chatgpt:true, label:__('Sub Heading Text','ultimate-post') },
	// Style
	{
		type: 'typography',
		key: 'subHeadingTypo',
		label: __('Sub Heading Typography', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'subHeadingColor',
		label: __('Sub Heading Color', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'subHeadingSpacing',
		label: __('Sub Heading Spacing', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	// Style
	{
		type: 'toggle',
		key: 'enableWidth',
		label: __('Sub Heading Custom Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'customWidth',
		label: __('Sub Heading Max Width', 'ultimate-post'),
		min: 1,
		max: 1000,
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const headingSettings = [
	'headingText',
	'headingAlign',
	'headingTag',
	'headingStyle',
	'headingBtnText',
	'headingURL',

	'subHeadingShow',
	'subHeadingText',
];

export const headingStyle = [
	'headingTypo',
	'headingColor',
	'headingBg',
	'headingBg2',
	'headingBorderBottomColor',
	'headingBorderBottomColor2',
	'headingBorder',
	'headingBtnColor',
	'headingBtnHoverColor',
	'headingSpacing',
	'headingPadding',
	'headingRadius',

	'subHeadingTypo',
	'subHeadingColor',
	'subHeadingSpacing',
	'enableWidth',
	'customWidth',
];

// Image - Style
export const ImageStyleArg = [
	{
		type: 'select',
		key: 'imgCrop',
		label: __('Large Image Size', 'ultimate-post'),
		options: imageSize,
	},
	{
		type: 'select',
		key: 'imgCropSmall',
		label: __('Small Image Size', 'ultimate-post'),
		options: imageSize,
	},
	{
		type: 'select',
		key: 'imgAnimation',
		label: __('Hover Animation', 'ultimate-post'),
		options: [
			{ value: 'none', label: __('No Animation', 'ultimate-post') },
			{ value: 'zoomIn', label: __('Zoom In', 'ultimate-post') },
			{ value: 'zoomOut', label: __('Zoom Out', 'ultimate-post') },
			{ value: 'opacity', label: __('Opacity', 'ultimate-post') },
			{ value: 'roateLeft', label: __('Rotate Left', 'ultimate-post') },
			{
				value: 'rotateRight',
				label: __('Rotate Right', 'ultimate-post'),
			},
			{ value: 'slideLeft', label: __('Slide Left', 'ultimate-post') },
			{ value: 'slideRight', label: __('Slide Right', 'ultimate-post') },
		],
	},
	{
		type: 'range',
		key: 'imgWidth',
		min: 0,
		max: 2000,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem', '%'],
		label: __('Width', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'imgHeight',
		min: 0,
		max: 2000,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem', '%'],
		label: __('Height', 'ultimate-post'),
	},
	{
		type: 'tag',
		key: 'imageScale',
		label: __('Image scale', 'ultimate-post'),
		options: [
			{ value: '', label: __('None', 'ultimate-post') },
			{ value: 'cover', label: __('Cover', 'ultimate-post') },
			{ value: 'contain', label: __('Contain', 'ultimate-post') },
			{ value: 'fill', label: __('Fill', 'ultimate-post') },
		],
	},
	{
		type: 'tab',
		key: 'imgTab',
		content: [
			{
				name: 'Normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'range',
						key: 'imgGrayScale',
						label: __('Gray Scale', 'ultimate-post'),
						min: 0,
						max: 100,
						step: 1,
						unit: ['%'],
						responsive: true,
					},
					{
						type: 'dimension',
						key: 'imgRadius',
						label: __('Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'imgShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
				],
			},
			{
				name: 'tab2',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'range',
						key: 'imgHoverGrayScale',
						label: __('Hover Gray Scale', 'ultimate-post'),
						min: 0,
						max: 100,
						step: 1,
						unit: ['%'],
						responsive: true,
					},
					{
						type: 'dimension',
						key: 'imgHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'imgHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'imgMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'toggle',
		key: 'imgOverlay',
		label: __('Overlay', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'imgOverlayType',
		label: __('Overlay Type', 'ultimate-post'),
		options: [
			{ value: 'default', label: __('Default', 'ultimate-post') },
			{
				value: 'simgleGradient',
				label: __('Simple Gradient', 'ultimate-post'),
			},
			{ value: 'multiColour', label: __('Multi Color', 'ultimate-post') },
			{ value: 'flat', label: __('Flat', 'ultimate-post') },
			{ value: 'custom', label: __('Custom', 'ultimate-post') },
		],
	},
	{
		type: 'color2',
		key: 'overlayColor',
		label: __('Custom Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'imgOpacity',
		min: 0,
		max: 1,
		step: 0.05,
		label: __('Overlay Opacity', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'imgSrcset',
		label: __('Enable Srcset', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'toggle',
		key: 'imgLazy',
		label: __('Enable Lazy Loading', 'ultimate-post'),
		pro: true,
	},
];

export const imageSettingsKeys = [
	'imgCrop',
	'imgCropSmall',
	'imgAnimation',
	'imgOverlay',
	'imgOverlayType',
	'overlayColor',
	'imgOpacity',
	'imgSrcset',
	'imgLazy',
];

export const imageStyleKeys = [
	'imgWidth',
	'imgHeight',
	'imageScale',
	'imgTab',
	'imgMargin',
];

// Video - Style
const VideoStyleArg = [
	{
		type: 'toggle',
		key: 'popupAutoPlay',
		label: __('Enable Popup Auto Play', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'vidIconPosition',
		options: [
			{ label: 'Center', value: 'center' },
			{ label: 'Bottom Right', value: 'bottomRight' },
			{ label: 'Bottom Left', value: 'bottomLeft' },
			{ label: 'Top Right', value: 'topRight' },
			{ label: 'Top Left', value: 'topLeft' },
			{ label: 'Right Middle', value: 'rightMiddle' },
			{ label: 'Left Middle', value: 'leftMiddle' },
		],
		label: __('Video Icon Position', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'popupIconColor',
		label: __('Icon Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'popupHovColor',
		label: __('Icon Hover Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'iconSize',
		label: __('Icon Size', 'ultimate-post'),
		min: 0,
		max: 500,
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'toggle',
		key: 'enablePopup',
		pro: true,
		label: __('Enable Video Popup', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'popupWidth',
		label: __('Popup Video Width', 'ultimate-post'),
		min: 0,
		max: 100,
		step: 1,
		unit: false,
		responsive: true,
	},
	{
		type: 'toggle',
		key: 'enablePopupTitle',
		label: __('Popup Title Enable', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'popupTitleColor',
		label: __('Title Color', 'ultimate-post'),
	},
	// {
	//     type: 'separator',label: __('Close Icon', 'ultimate-post')
	// },
	{
		type: 'color',
		key: 'closeIconColor',
		label: __('Close Icon Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'closeHovColor',
		label: __('Close Hover Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'closeSize',
		label: __('Close Icon Size', 'ultimate-post'),
		min: 0,
		max: 200,
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const videoColors = [
	'popupIconColor',
	'popupHovColor',
	'popupTitleColor',
	'closeIconColor',
	'closeHovColor',
];

// Title - Style
const TitleStyleArg = [
	{
		type: 'tag',
		key: 'titleTag',
		label: __('Title Tag', 'ultimate-post'),
		options: [
			{ value: 'h1', label: __('H1', 'ultimate-post') },
			{ value: 'h2', label: __('H2', 'ultimate-post') },
			{ value: 'h3', label: __('H3', 'ultimate-post') },
			{ value: 'h4', label: __('H4', 'ultimate-post') },
			{ value: 'h5', label: __('H5', 'ultimate-post') },
			{ value: 'h6', label: __('H6', 'ultimate-post') },
			{ value: 'span', label: __('span', 'ultimate-post') },
			{ value: 'div', label: __('div', 'ultimate-post') },
		],
	},
	{
		type: 'toggle',
		key: 'titlePosition',
		label: __('Meta Under Title', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'titleLength',
		min: 0,
		max: 30,
		step: 1,
		label: __('Max Length', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'titleStyle',
		label: __('Title Hover Effects', 'ultimate-post'),
		options: [
			{ value: 'none', label: __('None', 'ultimate-post') },
			{
				value: 'style1',
				label: __('On Hover Underline', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style2',
				label: __('On Hover Wave', 'ultimate-post'),
				pro: true,
			},
			{ value: 'style3', label: __('Wave', 'ultimate-post'), pro: true },
			{
				value: 'style4',
				label: __('underline', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style5',
				label: __('underline and wave', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style6',
				label: __('Underline Background', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style7',
				label: __('Underline Right To Left', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style8',
				label: __('Underline center', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style9',
				label: __('Underline Background 2', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style10',
				label: __('Underline Hover Spacing', 'ultimate-post'),
				pro: true,
			},
			{
				value: 'style11',
				label: __('Hover word spacing', 'ultimate-post'),
				pro: true,
			},
		],
	},
	{
		type: 'typography',
		key: 'titleTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'titleBackground',
		label: __('Title BG Color', 'ultimate-post'),
	},
	{ type: 'color', key: 'titleColor', label: __('Color', 'ultimate-post') },
	{
		type: 'color',
		key: 'titleHoverColor',
		label: __('Hover Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'titleAnimColor',
		label: __('Animation Color', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'titlePadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Description - Style
const DescStyleArg = [
	{
		type: 'tag',
		key: 'descTag',
		label: __('Title Tag', 'ultimate-post'),
		options: [
			{ value: 'h1', label: __('H1', 'ultimate-post') },
			{ value: 'h2', label: __('H2', 'ultimate-post') },
			{ value: 'h3', label: __('H3', 'ultimate-post') },
			{ value: 'h4', label: __('H4', 'ultimate-post') },
			{ value: 'h5', label: __('H5', 'ultimate-post') },
			{ value: 'h6', label: __('H6', 'ultimate-post') },
			{ value: 'span', label: __('span', 'ultimate-post') },
			{ value: 'div', label: __('div', 'ultimate-post') },
		],
	},
	{ type: 'color', key: 'descColor', label: __('Color', 'ultimate-post') },
	{
		type: 'color',
		key: 'descHoverColor',
		label: __('Hover Color', 'ultimate-post'),
	},
	{
		type: 'typography',
		key: 'descTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'descPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Prefix - Style
const PrefixStyleArg = [
	{
		type: 'text',
		key: 'prefixText',
		label: __('Prefix Text', 'ultimate-post'),
	},
	{ type: 'color', key: 'prefixColor', label: __('Color', 'ultimate-post') },
	{
		type: 'typography',
		key: 'prefixTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'prefixPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Meta - Style
export const MetaStyleArg = [
	{
		type: 'tag',
		key: 'metaPosition',
		label: __('Meta Position', 'ultimate-post'),
		options: [
			{ value: 'top', label: __('Top', 'ultimate-post') },
			{ value: 'bottom', label: __('Bottom', 'ultimate-post') },
		],
	},
	{
		type: 'select',
		key: 'metaStyle',
		label: __('Author Style', 'ultimate-post'),
		options: [
			{ value: 'noIcon', label: __('No Icon', 'ultimate-post') },
			{ value: 'icon', label: __('With Icon', 'ultimate-post') },
			{ value: 'style2', label: __('Style2', 'ultimate-post') },
			{ value: 'style3', label: __('Style3', 'ultimate-post') },
			{ value: 'style4', label: __('Style4', 'ultimate-post') },
			{ value: 'style5', label: __('Style5', 'ultimate-post') },
			{ value: 'style6', label: __('Style6', 'ultimate-post') },
		],
	},
	{
		type: 'toggle',
		key: 'authorLink',
		label: __('Enable Author Link', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'metaSeparator',
		label: __('Separator', 'ultimate-post'),
		options: [
			{ value: 'dot', label: __('Dot', 'ultimate-post') },
			{ value: 'slash', label: __('Slash', 'ultimate-post') },
			{
				value: 'doubleslash',
				label: __('Double Slash', 'ultimate-post'),
			},
			{ value: 'close', label: __('Close', 'ultimate-post') },
			{ value: 'dash', label: __('Dash', 'ultimate-post') },
			{
				value: 'verticalbar',
				label: __('Vertical Bar', 'ultimate-post'),
			},
			{ value: 'emptyspace', label: __('Empty', 'ultimate-post') },
		],
	},
	metaList,
	{
		type: 'text',
		key: 'metaMinText',
		label: __('Minute Text', 'ultimate-post'),
	},
	{
		type: 'text',
		key: 'metaAuthorPrefix',
		label: __('Author Prefix Text', 'ultimate-post'),
	},
	{
		...metaList,
		key: 'metaListSmall',
		label: __('Small Item Meta', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'metaDateFormat',
		label: __('Date/Time Format', 'ultimate-post'),
		options: [
			{ value: 'M j, Y', label: 'Feb 7, 2022' },
			{
				value: 'default_date',
				label: 'WordPress Default Date Format',
				pro: true,
			},
			{
				value: 'default_date_time',
				label: 'WordPress Default Date & Time Format',
				pro: true,
			},
			{ value: 'g:i A', label: '1:12 PM', pro: true },
			{ value: 'F j, Y', label: 'February 7, 2022', pro: true },
			{
				value: 'F j, Y g:i A',
				label: 'February 7, 2022 1:12 PM',
				pro: true,
			},
			{ value: 'M j, Y g:i A', label: 'Feb 7, 2022 1:12 PM', pro: true },
			{ value: 'j M Y', label: '7 Feb 2022', pro: true },
			{ value: 'j M Y g:i A', label: '7 Feb 2022 1:12 PM', pro: true },
			{ value: 'j F Y', label: '7 February 2022', pro: true },
			{
				value: 'j F Y g:i A',
				label: '7 February 2022 1:12 PM',
				pro: true,
			},
			{ value: 'j. M Y', label: '7. Feb 2022', pro: true },
			{ value: 'j. M Y | H:i', label: '7. Feb 2022 | 1:12', pro: true },
			{ value: 'j. F Y', label: '7. February 2022', pro: true },
			{ value: 'j.m.Y', label: '7.02.2022', pro: true },
			{ value: 'j.m.Y g:i A', label: '7.02.2022 1:12 PM', pro: true },
		],
	},
	{
		type: 'range',
		key: 'metaSpacing',
		label: __('Meta Spacing Between', 'ultimate-post'),
		min: 0,
		max: 50,
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'typography',
		key: 'metaTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{ type: 'color', key: 'metaColor', label: __('Color', 'ultimate-post') },
	{
		type: 'color',
		key: 'metaHoverColor',
		label: __('Hover Color', 'ultimate-post'),
	},
	{ type: 'color', key: 'metaBg', label: __('Background', 'ultimate-post') },
	{
		type: 'color',
		key: 'metaSeparatorColor',
		label: __('Separator Color', 'ultimate-post'),
	},
	{ type: 'border', key: 'metaBorder', label: __('Border', 'ultimate-post') },
	{
		type: 'dimension',
		key: 'metaMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'metaPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const metaSettings = [
	'metaStyle',
	'metaPosition',
	'metaSeparator',
	'metaList',
	'authorLink',
	'metaListSmall',
	'metaDateFormat',
	'cMetaRepetableField',
];

export const metaStyle = [
	'metaSpacing',
	'metaTypo',
	'metaColor',
	'metaHoverColor',
	'metaBg',
	'metaSeparatorColor',
	'metaBorder',
	'metaMargin',
	'metaPadding',
];

export const metaText = ['metaAuthorPrefix', 'metaMinText'];

// Excerpt - Style
export const ExcerptStyleArg = [
	{
		type: 'toggle',
		key: 'showSeoMeta',
		label: __('SEO Meta', 'ultimate-post'),
		pro: true,
		help: __(
			'Show Meta from Yoast, RankMath, AIO, SEOPress and Squirrly. Make sure that PostX > Addons > [SEO Plugin] is Enabled.',
			'ultimate-post'
		),
	},
	{
		type: 'toggle',
		key: 'showFullExcerpt',
		label: __('Show Full Excerpt', 'ultimate-post'),
		help: __('Show Excerpt from Post Meta Box.', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'excerptLimit',
		min: 0,
		max: 500,
		step: 1,
		label: __('Excerpt Limit', 'ultimate-post'),
		help: __('Excerpt Limit from Post Content.', 'ultimate-post'),
	},
	{
		type: 'typography',
		key: 'excerptTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{ type: 'color', key: 'excerptColor', label: __('Color', 'ultimate-post') },
	{
		type: 'dimension',
		key: 'excerptPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const excerptSettings = [
	'showSeoMeta',
	'showFullExcerpt',
	'excerptLimit',
];

export const excerptStyle = ['excerptTypo', 'excerptColor', 'excerptPadding'];

// Filter - Content
export const FilterContentArg = [
	// {type: 'notice', status: "error", text: "Use the PostX Advance Filter Block instead."},
	{
		type: 'toggle',
		key: 'filterBelowTitle',
		label: __('Below Title', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'filterType',
		label: __('Filter Type', 'ultimate-post'),
		options: [],
	},
	{
		type: 'search',
		key: 'filterValue',
		label: __('Filter Value', 'ultimate-post'),
		multiple: true,
		search: 'taxvalue',
	},
	{
		type: 'text',
		key: 'filterText',
		label: __('All Content Text', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'filterMobile',
		label: __('Enable Dropdown', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'text',
		key: 'filterMobileText',
		label: __('Menu Text', 'ultimate-post'),
		help: __('Mobile Device Filter Menu Text', 'ultimate-post'),
	},

	{
		type: 'typography',
		key: 'fliterTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'alignment',
		key: 'filterAlign',
		responsive: true,
		label: __('Alignment', 'ultimate-post'),
		disableJustify: true,
	},
	{
		type: 'tab',
		key: 'fTab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'filterColor',
						label: __('Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'filterBgColor',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'filterBorder',
						label: __('Border', 'ultimate-post'),
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'filterHoverColor',
						label: __('Hover Color', 'ultimate-post'),
					},
					{
						type: 'color',
						key: 'filterHoverBgColor',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'filterHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'filterRadius',
		label: __('Border Radius', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},

	{
		type: 'color',
		key: 'filterDropdownColor',
		label: __('Dropdown Text Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'filterDropdownHoverColor',
		label: __('Dropdown Hover Color', 'ultimate-post'),
	},
	{
		type: 'color',
		key: 'filterDropdownBg',
		label: __('Dropdown Background', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'filterDropdownRadius',
		min: 0,
		max: 300,
		step: 1,
		responsive: true,
		unit: ['px', 'em', 'rem', '%'],
		label: __('Dropdown Radius', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'filterDropdownPadding',
		label: __('Dropdown Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},

	{
		type: 'dimension',
		key: 'fliterSpacing',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'fliterPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const filterSettings = [
	'filterBelowTitle',
	'filterType',
	'filterValue',
	'filterText',
	'filterMobile',
	'filterMobileText',
];

export const filterStyle = [
	'fliterTypo',
	'fTab',
	'filterRadius',
	'filterDropdownColor',
	'filterDropdownHoverColor',
	'filterDropdownBg',
	'filterDropdownRadius',
	'fliterSpacing',
	'fliterPadding',
];

// Separator - Style
const SeparatorStyleArg = [
	{
		type: 'color',
		key: 'septColor',
		label: __('Border Color', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'septSize',
		min: 0,
		max: 20,
		step: 1,
		label: __('Border Size', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'septStyle',
		label: __('Border Style', 'ultimate-post'),
		options: [
			{ value: 'none', label: __('None', 'ultimate-post') },
			{ value: 'solid', label: __('Solid', 'ultimate-post') },
			{ value: 'dashed', label: __('Dashed', 'ultimate-post') },
			{ value: 'dotted', label: __('Dotted', 'ultimate-post') },
			{ value: 'double', label: __('Double', 'ultimate-post') },
		],
	},
	{
		type: 'range',
		key: 'septSpace',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		label: __('Spacing', 'ultimate-post'),
	},
];

// Pagination - Content
export const PaginationContentArg = [
	// {type: 'notice', status: "error", text: "Use the Enhanced Pagination instead."},
	{
		type: 'tag',
		key: 'paginationType',
		label: __('Pagination Type', 'ultimate-post'),
		options: [
			{ value: 'loadMore', label: __('Loadmore', 'ultimate-post') },
			{ value: 'navigation', label: __('Navigation', 'ultimate-post') },
			{ value: 'pagination', label: __('Pagination', 'ultimate-post') },
		],
	},
	{
		type: 'text',
		key: 'loadMoreText',
		label: __('Loadmore Text', 'ultimate-post'),
	},
	{
		type: 'text',
		key: 'paginationText',
		label: __('Pagination Text', 'ultimate-post'),
	},
	{
		type: 'alignment',
		key: 'pagiAlign',
		responsive: true,
		label: __('Alignment', 'ultimate-post'),
		disableJustify: true,
	},
	{
		type: 'select',
		key: 'paginationNav',
		label: __('Pagination', 'ultimate-post'),
		options: [
			{ value: 'textArrow', label: __('Text & Arrow', 'ultimate-post') },
			{ value: 'onlyarrow', label: __('Only Arrow', 'ultimate-post') },
		],
	},
	{
		type: 'toggle',
		key: 'paginationAjax',
		label: __('Ajax Pagination', 'ultimate-post'),
	},
	{
		type: 'select',
		key: 'navPosition',
		label: __('Navigation Position', 'ultimate-post'),
		options: [
			{ value: 'topRight', label: __('Top Right', 'ultimate-post') },
			{ value: 'bottomLeft', label: __('Bottom', 'ultimate-post') },
		],
	},

	// Styles
	{
		type: 'typography',
		key: 'pagiTypo',
		label: __('Typography', 'ultimate-post'),
	},
	{
		type: 'range',
		key: 'pagiArrowSize',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		label: __('Arrow Size', 'ultimate-post'),
	},
	{
		type: 'tab',
		key: 'pagiTab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'pagiColor',
						label: __('Color', 'ultimate-post'),
						clip: true,
					},
					{
						type: 'color2',
						key: 'pagiBgColor',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'pagiBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'pagiShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'pagiRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'pagiHoverColor',
						label: __('Hover Color', 'ultimate-post'),
						clip: true,
					},
					{
						type: 'color2',
						key: 'pagiHoverbg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'pagiHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'pagiHoverShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'pagiHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'pagiMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'navMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'pagiPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const pagiSettings = [
	'paginationType',
	'loadMoreText',
	'paginationText',
	'pagiAlign',
	'paginationNav',
	'paginationAjax',
	'navPosition',
];

export const pagiStyles = [
	'pagiTypo',
	'pagiArrowSize',
	'pagiTab',
	'pagiMargin',
	'navMargin',
	'pagiPadding',
];

// General - Advanced
const GeneralAdvanceedArg = [
	{ type: 'text', key: 'advanceId', label: __('ID', 'ultimate-post') },
	{
		type: 'range',
		key: 'advanceZindex',
		min: -100,
		max: 10000,
		step: 1,
		label: __('z-index', 'ultimate-post'),
	},
	{
		type: 'dimension',
		key: 'wrapMargin',
		label: __('Margin', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'wrapOuterPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color2',
						key: 'wrapBg',
						label: __('Background', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'wrapBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'wrapShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'wrapRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color2',
						key: 'wrapHoverBackground',
						label: __('Background', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'wrapHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'wrapHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'wrapHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
];

// Custom CSS - Advanced
const CustomCssAdvancedArg = [
	{
		type: 'textarea',
		key: 'advanceCss',
		placeholder: __(
			'Add {{ULTP}} before the selector to wrap element.',
			'ultimate-post'
		),
	},
];

const ResponsiveAdvancedArg = [
	{
		type: 'toggle',
		key: 'hideExtraLarge',
		label: __('Hide On Extra Large Display', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'toggle',
		key: 'hideTablet',
		label: __('Hide On Tablet', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'toggle',
		key: 'hideMobile',
		label: __('Hide On Mobile', 'ultimate-post'),
		pro: true,
	},
];

const DesignArg = [{ type: 'template', key: 'design' }];

let _tax_option = [
	{ value: 'regular', label: __('Regular (All Taxonomy)', 'ultimate-post') },
	{ value: 'child', label: __('Child Of', 'ultimate-post') },
	{
		value: 'parent',
		label: __('Parent (Only Parent Taxonomy)', 'ultimate-post'),
	},
	{ value: 'custom', label: __('Custom', 'ultimate-post') },
];
const _archive_pram = [
	{
		value: 'immediate_child',
		label: __('Immediate Child (Archive)', 'ultimate-post'),
	},
	{
		value: 'current_level',
		label: __('Current Level (Archive)', 'ultimate-post'),
	},
	{ value: 'allchild', label: __('All Child (Archive)', 'ultimate-post') },
];

_tax_option =
	ultp_data?.archive == 'archive'
		? _tax_option.concat(_archive_pram)
		: _tax_option;

const TaxQueryArg = [
	{
		type: 'select',
		key: 'taxType',
		label: __('Query Type', 'ultimate-post'),
		options: _tax_option,
	},
	{
		type: 'select',
		key: 'taxSlug',
		label: __('Taxonomy Type', 'ultimate-post'),
		multiple: false,
	},
	{
		type: 'select',
		key: 'taxValue',
		label: __('Taxonomy Value', 'ultimate-post'),
		multiple: true,
	},
	{
		type: 'range',
		key: 'queryNumber',
		min: 0,
		max: 200,
		help: __('Set 0 for get all taxonomy.', 'ultimate-post'),
		label: __('Number of Post', 'ultimate-post'),
	},
];

// Taxonomy Wrap - Style
const TaxWrapStyleArg = [
	{
		type: 'select',
		key: 'TaxAnimation',
		label: __('Hover Animation', 'ultimate-post'),
		options: [
			{ value: 'none', label: __('No Animation', 'ultimate-post') },
			{ value: 'zoomIn', label: __('Zoom In', 'ultimate-post') },
			{ value: 'zoomOut', label: __('Zoom Out', 'ultimate-post') },
			{ value: 'opacity', label: __('Opacity', 'ultimate-post') },
			{ value: 'slideLeft', label: __('Slide Left', 'ultimate-post') },
			{ value: 'slideRight', label: __('Slide Right', 'ultimate-post') },
		],
	},
	{
		type: 'toggle',
		key: 'customTaxColor',
		label: __('Taxonomy Specific Color', 'ultimate-post'),
		pro: true,
	},
	{
		type: 'linkbutton',
		key: 'seperatorTaxLink',
		placeholder: __('Choose Color', 'ultimate-post'),
		label: __('Taxonomy Specific (Pro)', 'ultimate-post'),
		text: 'Choose Color',
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __('Normal', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'TaxWrapBg',
						label: __('Background Color', 'ultimate-post'),
					},
					{
						type: 'range',
						key: 'customOpacityTax',
						min: 0,
						max: 1,
						step: 0.05,
						label: __('Overlay Opacity', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'TaxWrapBorder',
						label: __('Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'TaxWrapShadow',
						label: __('BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'TaxWrapRadius',
						label: __('Border Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __('Hover', 'ultimate-post'),
				options: [
					{
						type: 'color',
						key: 'TaxWrapHoverBg',
						label: __('Hover Bg Color', 'ultimate-post'),
					},
					{
						type: 'range',
						key: 'customTaxOpacityHover',
						min: 0,
						max: 1,
						step: 0.05,
						label: __('Hover Opacity', 'ultimate-post'),
					},
					{
						type: 'border',
						key: 'TaxWrapHoverBorder',
						label: __('Hover Border', 'ultimate-post'),
					},
					{
						type: 'boxshadow',
						key: 'TaxWrapHoverShadow',
						label: __('Hover BoxShadow', 'ultimate-post'),
					},
					{
						type: 'dimension',
						key: 'TaxWrapHoverRadius',
						label: __('Hover Radius', 'ultimate-post'),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'TaxWrapPadding',
		label: __('Padding', 'ultimate-post'),
		step: 1,
		unit: true,
		responsive: true,
	},
];

const AdvFilterArgs = [
	{
		data: {
			type: 'tag',
			key: 'relation',
			label: __('Multiple Filter Relation', 'ultimate-post'),
			options: [
				{
					value: 'AND',
					label: __('AND', 'ultimate-post'),
				},
				{
					value: 'OR',
					label: __('OR', 'ultimate-post'),
				},
			],
		}
	},
	{
		data: {
			type: 'alignment',
			key: 'align',
			label: __('Alignment', 'ultimate-post'),
			responsive: true,
			options: ['flex-start', 'center', 'flex-end'],
			icons: ['left', 'center', 'right'],
		},
	},
	{
		data: {
			type: 'range',
			key: 'gapChild',
			min: 0,
			max: 120,
			step: 1,
			responsive: true,
			unit: ['px', 'em', 'rem'],
			label: __('Gap', 'ultimate-post'),
		},
	},
	{
		data: {
			type: 'range',
			key: 'spacingTop',
			min: 0,
			max: 120,
			step: 1,
			responsive: true,
			unit: ['px', 'em', 'rem'],
			label: __('Spacing Top', 'ultimate-post'),
		},
	},
	{
		data: {
			type: 'range',
			key: 'spacingBottom',
			min: 0,
			max: 120,
			step: 1,
			responsive: true,
			unit: ['px', 'em', 'rem'],
			label: __('Spacing Bottom', 'ultimate-post'),
		},
	},
];

export const FeatureToggleArgs = [
	{
		type: isDCActive() ? 'toggle' : '',
		label: __('Enable Dynamic Content', 'ultimate-post'),
		key: 'dcEnabled',
		help: __(
			'Insert dynamic data & custom fields that update automatically.',
			'ultimate-post'
		),
	},
	{
		type: 'advFilterEnable',
		key: 'advFilterEnable',
		label: __('Advanced Filter', 'ultimate-post'),
	},
	{
		type: 'advPagiEnable',
		key: 'advPaginationEnable',
		label: __('Advanced Pagination', 'ultimate-post'),
	},
	{ type: 'toggle', key: 'showImage', label: __('Image', 'ultimate-post') },
	{ type: 'toggle', key: 'titleShow', label: __('Title', 'ultimate-post') },
	{ type: 'toggle', key: 'metaShow', label: __('Meta', 'ultimate-post') },
	{
		type: 'toggle',
		key: 'catShow',
		label: __('Taxonomy/Category', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'excerptShow',
		label: __('Excerpt', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'readMore',
		label: __('Read More', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'headingShow',
		label: __('Heading', 'ultimate-post'),
	},
	{ type: 'toggle', key: 'filterShow', label: __('Filter', 'ultimate-post') },
	{
		type: 'toggle',
		key: 'paginationShow',
		label: __('Pagination', 'ultimate-post'),
	},
];

export const SliderFeatureToggleArgs = [
	{ type: 'toggle', key: 'imageShow', label: __('Image', 'ultimate-post') },
	{ type: 'toggle', key: 'titleShow', label: __('Title', 'ultimate-post') },
	{ type: 'toggle', key: 'metaShow', label: __('Meta', 'ultimate-post') },
	{
		type: 'toggle',
		key: 'catShow',
		label: __('Taxonomy/Category', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'excerptShow',
		label: __('Excerpt', 'ultimate-post'),
	},
	{
		type: 'toggle',
		key: 'readMore',
		label: __('Read More', 'ultimate-post'),
	},
	{ type: 'toggle', key: 'arrows', label: __('Arrows', 'ultimate-post') },
	{ type: 'toggle', key: 'dots', label: __('Dots', 'ultimate-post') },
	{
		type: 'toggle',
		key: 'headingShow',
		label: __('Heading', 'ultimate-post'),
	},
];

export const FeatureToggleArgsNew = ['advFilterEnable', 'advPaginationEnable'];
export const FeatureToggleArgsDep = [
	'headingShow',
	'filterShow',
	'paginationShow',
];

/**
 * @param {Array} content
 */
export function formatSettingsForToolbar(content) {
	return [
		{
			isToolbar: true,
			data: {
				type: 'tab_toolbar',
				content,
			},
		},
	];
}

const Layout = (props) => {
	return (
		<FieldGenerator
			initialOpen={props.initialOpen || false}
			title={props.title == 'inline' ? '' : __('Layout', 'ultimate-post')}
			block={props.block}
			store={props.store}
			col={props.col}
			data={[props.data]}
		/>
	);
};

const HeadingContent = (props) => {
	const oArg = filterFields(props.include, props.exclude, HeadingContentArg);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: headingSettings,
			style: headingStyle,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Heading', 'ultimate-post')}
			store={props.store}
			data={oArg}
			hrIdx={props.hrIdx}
		/>
	);
};

// General
const GeneralContent = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('General', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, GeneralArg)}
		/>
	);
};

const GeneralContentWithQuery = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			dynamicHelpText={props.dynamicHelpText}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('General', 'ultimate-post')}
			store={props.store}
			data={filterFields(
				props.include,
				props.exclude,
				GeneralArgWithQuery
			)}
		/>
	);
};

export function getGridSpacingToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'spacing',
			title: __('Grid Spacing', 'ultimate-post'),
			options: filterFields(props.include, props.exclude, SpacingArgs),
		},
	]);
}

export function getGridAlignmentToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'grid_align',
			title: __('Grid Alignment', 'ultimate-post'),
			options: filterFields(props.include, props.exclude, SpacingArgs),
		},
	]);
}

// Query
const QueryContent = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			dynamicHelpText={props.dynamicHelpText}
			youtube={
				'https://wpxpo.com/docs/postx/postx-features/advanced-query-builder/?utm_source=db-postx-editor&utm_medium=video-docs&utm_campaign=postx-dashboard'
			}
			initialOpen={props.initialOpen || false}
			title={
				props.title == 'inline'
					? ''
					: __('Query Builder', 'ultimate-post')
			}
			store={props.store}
			data={filterFields(props.include, props.exclude, QueryArg)}
		/>
	);
};

const PaginationContent = (props) => {
	const oArgs = filterFields(
		props.include,
		ultp_data.archive == 'archive'
			? ['paginationAjax'].concat(props.exclude || [])
			: props.exclude,
		PaginationContentArg
	);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: pagiSettings,
			style: pagiStyles,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={
				'https://wpxpo.com/docs/postx/postx-features/pagination/?utm_source=db-postx-editor&utm_medium=video-docs&utm_campaign=postx-dashboard'
			}
			initialOpen={props.initialOpen || false}
			title={__('Pagination', 'ultimate-post')}
			store={props.store}
			pro={props.pro}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getPagiToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'pagi',
			title: props.title,
			options: filterFields(
				props.include,
				props.exclude,
				PaginationContentArg
			),
		},
	]);
}

const FilterContent = (props) => {
	const oArgs = filterFields(props.include, props.exclude, FilterContentArg);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: filterSettings,
			style: filterStyle,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={
				'https://wpxpo.com/docs/postx/postx-features/postx-ajax-filtering/?db-postx-editor&utm_medium=video-docs&utm_campaign=postx-dashboard'
			}
			initialOpen={props.initialOpen || false}
			title={__('Filter', 'ultimate-post')}
			store={props.store}
			pro={props.pro}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getFilterToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'filter',
			title: props.title,
			options: filterFields(
				props.include,
				props.exclude,
				FilterContentArg
			),
		},
	]);
}

const ArrowContent = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Arrow', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, ArrowContentArg)}
		/>
	);
};

export function getArrowToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'arrow',
			title: props.title || __('Arrow Style', 'ultimate-post'),
			options: filterFields(
				props.include,
				props.exclude,
				ArrowContentArg
			),
		},
	]);
}

const TitleStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Title', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, TitleStyleArg)}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getTitleToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'title',
			title: props.title || __('Title Style', 'ultimate-post'),
			options: filterFields(props.include, props.exclude, TitleStyleArg),
		},
	]);
}

const DescStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Description', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, DescStyleArg)}
		/>
	);
};

const PrefixStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Prefix', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, PrefixStyleArg)}
			hrIdx={props.hrIdx}
		/>
	);
};

const MetaStyle = (props) => {
	const oArgs = filterFields(props.include, props.exclude, MetaStyleArg);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: metaSettings,
			style: metaStyle,
			text: metaText,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			tabs={[
				{
					name: 'settings',
					title: 'Settings',
					icon: icons.settings3,
				},
				{
					name: 'style',
					title: 'Style',
					icon: icons.style,
				},
				{
					name: 'text',
					title: 'Text',
					icon: icons.textTab,
				},
			]}
			doc={props.doc}
			depend={props.depend}
			youtube={
				'https://wpxpo.com/docs/postx/postx-features/post-meta/?db-postx-editor&utm_medium=video-docs&utm_campaign=postx-dashboard'
			}
			initialOpen={props.initialOpen || false}
			title={__('Meta', 'ultimate-post')}
			store={props.store}
			pro={props.pro}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getMetaToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'meta',
			title: props.title,
			options: filterFields(props.include, props.exclude, MetaStyleArg),
		},
	]);
}

const ImageStyle = (props) => {
	const oArgs = filterFields(props.include, props.exclude, ImageStyleArg);
	let tabData = null;

	if (props.isTab) {
		const settings = [
			'imgCrop',
			'imgCropSmall',
			'imgAnimation',
			'imgOverlay',
			'imgOverlayType',
			'overlayColor',
			'imgOpacity',
			'imgSrcset',
			'imgLazy',
			'fallbackEnable',
			'fallbackImg',
		];

		const style = filterFields2(
			{
				opType: 'discard',
				data: settings,
			},
			oArgs
		).map((item) => item.key);

		tabData = {
			settings: settings,
			style: style,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Image', 'ultimate-post')}
			store={props.store}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function ToolbarSettingsAndStyles({
	store,
	settingsKeys,
	styleKeys,
	oArgs,
	settingsTitle,
	styleTitle,
	incSettings = [],
	exSettings = [],
	incStyle = [],
	exStyle = [],
}) {
	const defSettings = filterFields2(
		{
			opType: 'keep',
			data: settingsKeys,
		},
		oArgs
	);

	const defStyle = filterFields2(
		{
			opType: 'keep',
			data: styleKeys,
		},
		oArgs
	);

	const settings = filterFields(incSettings, exSettings, defSettings);
	const style = filterFields(incStyle, exStyle, defStyle);

	return (
		<>
			<ExtraToolbarSettings
				buttonContent={styleIcon}
				include={formatSettingsForToolbar([
					{
						name: 'tab_style',
						title: styleTitle,
						options: style,
					},
				])}
				store={store}
				label={styleTitle}
			/>
			<ExtraToolbarSettings
				buttonContent={settingsIcon}
				include={formatSettingsForToolbar([
					{
						name: 'tab_settings',
						title: settingsTitle,
						options: settings,
					},
				])}
				store={store}
				label={settingsTitle}
			/>
		</>
	);
}

const VideoStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Video', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, VideoStyleArg)}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getVideoToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'video',
			title: props.title || __('Video Settings', 'ultimate-post'),
			options: filterFields(props.include, props.exclude, VideoStyleArg),
		},
	]);
}

const ExcerptStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={props.title || __('Excerpt', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, ExcerptStyleArg)}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getExcerptToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'excerpt',
			title: props.title,
			options: filterFields(
				props?.include,
				props?.exclude,
				ExcerptStyleArg
			),
		},
	]);
}

const SeparatorStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Separator', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, SeparatorStyleArg)}
		/>
	);
};

const CategoryStyle = (props) => {
	const oArgs = filterFields(props.include, props.exclude, CategoryStyleArg);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: categorySettings,
			style: categoryStyle,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Taxonomy / Category', 'ultimate-post')}
			store={props.store}
			pro={props.pro}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getCategoryToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'cat',
			title: props.title,
			options: filterFields(
				props?.include,
				props?.exclude,
				CategoryStyleArg
			),
		},
	]);
}

const ButtonStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Button Style', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, ButtonStyleArg)}
		/>
	);
};

const WrapStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Content Wrap', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, WrapStyleArg)}
		/>
	);
};

const WrapHeadingStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Entry Header', 'ultimate-post')}
			store={props.store}
			data={filterFields(
				props.include,
				props.exclude,
				WrapHeadingStyleArg
			)}
		/>
	);
};

const OverlayStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Content', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, OverlayStyleArg)}
		/>
	);
};

const DotStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Dot', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, DotStyleArg)}
		/>
	);
};

export function getDotsToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'dot',
			title: props.title,
			options: filterFields(props?.include, props?.exclude, DotStyleArg),
		},
	]);
}

const ReadMoreStyle = (props) => {
	const oArgs = filterFields(props.include, props.exclude, ReadMoreStyleArg);
	let tabData = null;

	if (props.isTab) {
		tabData = {
			settings: readMoreSettings,
			style: readMoreStyle,
		};
	}

	return (
		<FieldGenerator
			isTab={props.isTab}
			tabData={tabData}
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Read More', 'ultimate-post')}
			store={props.store}
			data={oArgs}
			hrIdx={props.hrIdx}
		/>
	);
};

export function getReadMoreToolbarSettings(props) {
	return formatSettingsForToolbar([
		{
			name: 'read-more',
			title: props.title,
			options: filterFields(
				props?.include,
				props?.exclude,
				ReadMoreStyleArg
			),
		},
	]);
}

const CounterStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Count Style', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, CounterStyleArg)}
		/>
	);
};
const GeneralAdvanced = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('General', 'ultimate-post')}
			store={props.store}
			data={filterFields(
				props.include,
				props.exclude,
				GeneralAdvanceedArg
			)}
		/>
	);
};

const CustomCssAdvanced = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Custom CSS', 'ultimate-post')}
			store={props.store}
			data={filterFields(
				props.include,
				props.exclude,
				CustomCssAdvancedArg
			)}
		/>
	);
};

const ResponsiveAdvanced = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Responsive', 'ultimate-post')}
			store={props.store}
			pro={props.pro}
			data={filterFields(
				props.include,
				props.exclude,
				ResponsiveAdvancedArg
			)}
		/>
	);
};

const Design = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={
				'https://wpxpo.com/docs/postx/layout-and-design/?utm_source=db-postx-editor&utm_medium=video-docs&utm_campaign=postx-dashboard'
			}
			initialOpen={props.initialOpen || false}
			title={__('Ready-made Design', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, DesignArg)}
		/>
	);
};

const TaxQuery = (props) => {
	return (
		<FieldGenerator
			initialOpen={props.initialOpen || false}
			store={props.store}
			title={
				props.title == 'inline'
					? ''
					: __('Taxonomy Query', 'ultimate-post')
			}
			data={filterFields(props.include, props.exclude, TaxQueryArg)}
		/>
	);
};

const TaxWrapStyle = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={__('Wrap Style', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, TaxWrapStyleArg)}
		/>
	);
};

export const FeatureToggleStyle = (props) => {
	const oArgs = (props.data || FeatureToggleArgs).map((arg) => {
		let disabled = false;

		if (props.dep && props.dep.includes(arg.key)) {
			return {
				...arg,
				label: (
					<>
						{arg.label}
						<span
							className="ultp-label-tag-dep"
							title="This feature is deprecated may be removed in the future updates. Please use the better alternatives."
						>
							Deprecated
						</span>
					</>
				),
			};
		}

		let tags = <></>;

		if (!ultp_data.active && props.pro && props.pro.includes(arg.key)) {
			disabled = true;
			tags = (
				<span className="ultp-label-tag-pro" title="Pro Feature">
					<a
						href={UltpLinkGenerator(
							'https://www.wpxpo.com/postx/all-features/',
							'blockProFeat',
							ultp_data.affiliate_id
						)}
						target="_blank"
					>
						Pro
					</a>
				</span>
			);
		}

		if (props.new && props.new.includes(arg.key)) {
			tags = (
				<>
					{tags}
					<span
						className="ultp-label-tag-new"
						title="Newly Added Feature"
					>
						New
					</span>
				</>
			);
		}

		return {
			...arg,
			label: (
				<>
					{arg.label}
					{tags}
				</>
			),
			// disabled
		};
	});

	return (
		<TabPanel
			className="ultp-toolbar-tab"
			tabs={[
				{
					name: 'features',
					title: props.label,
				},
			]}
		>
			{(tab) => (
				<FieldGenerator
					isToolbar={true}
					initialOpen={false}
					title={'inline'}
					store={props.store}
					data={filterFields(props.include, props.exclude, oArgs)}
				/>
			)}
		</TabPanel>
	);
};

const CommonSettings = (props) => {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			initialOpen={props.initialOpen || false}
			title={props.title || __('Common', 'ultimate-post')}
			store={props.store}
			data={filterFields(props.include, props.exclude, [])}
		/>
	);
};

// call to append field settings in toolbar popup
const ToolbarCommonSettings = (props) => {
	return (
		<FieldGenerator
			title={'inline'}
			isToolbar={true}
			store={props.store}
			data={filterFields(props.include, props.exclude, [])}
		/>
	);
};

const ToolbarSettings = (props) => {
	return (
		<FieldGenerator
			isToolbar={true}
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			title={'inline'}
			store={props.store}
			data={filterFields(props.include, props.exclude, [])}
		/>
	);
};

export function TypographyTB(props) {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			store={props.store}
			data={[
				{
					type: 'typography_toolbar',
					key: props.attrKey,
					label: props.label,
				},
			]}
		/>
	);
}

export function UltpToolbarDropdown(props) {
	return (
		<FieldGenerator
			doc={props.doc}
			depend={props.depend}
			youtube={props.youtube}
			store={props.store}
			data={[
				{
					type: 'toolbar_dropdown',
					label: props.label,
					options: props.options,
					key: props.attrKey,
				},
			]}
		/>
	);
}

const SliderSetting = (settings = {}) => {
	const common = {
		arrows: true,
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: true,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		cssEase: 'linear',
		lazyLoad: true,
	};
	return Object.assign({}, common, settings);
};

const _dateFormat = (format) => {
	if (format == 'default_date') {
		return ultp_data.date_format;
	} else if (format == 'default_date_time') {
		return ultp_data.date_format + ' ' + ultp_data.time_format;
	} else {
		return format;
	}
};

const renderNavigation = () => {
	return (
		<div className={`ultp-next-prev-wrap ultp-disable-editor-click`}>
			<ul>
				<li>
					<a className={`ultp-prev-action ultp-disable`} href="#">
						{IconPack.leftAngle2}
						<span className={`screen-reader-text`}>
							{__('Previous', 'ultimate-post')}
						</span>
					</a>
				</li>
				<li>
					<a className={`ultp-prev-action`} href="#">
						{IconPack.rightAngle2}
						<span className={`screen-reader-text`}>
							{__('Next', 'ultimate-post')}
						</span>
					</a>
				</li>
			</ul>
		</div>
	);
};

const renderPagination = (
	paginationNav,
	paginationAjax,
	paginationText = '',
	pages = 4
) => {
	const data = paginationText.split('|');
	const prevText = data[0] || __('Previous', 'ultimate-post');
	const nextText = data[1] || __('Next', 'ultimate-post');
	return (
		<div
			className={
				'ultp-pagination-wrap ultp-disable-editor-click' +
				(paginationAjax ? ' ultp-pagination-ajax-action' : '')
			}
		>
			<ul className={`ultp-pagination`}>
				<li>
					<a href="#" className={`ultp-prev-page-numbers`}>
						{IconPack.leftAngle2}
						{paginationNav == 'textArrow' ? ' ' + prevText : ''}
					</a>
				</li>
				{pages > 4 && (
					<li className={`ultp-first-dot`}>
						<a href="#">...</a>
					</li>
				)}
				{new Array(pages > 2 ? 3 : pages).fill(0).map((v, i) => (
					<li key={i}>
						<a href="#">{i + 1}</a>
					</li>
				))}
				{pages > 4 && (
					<li className={`ultp-last-dot`}>
						<a href="#">...</a>
					</li>
				)}
				{pages > 5 && (
					<li className={`ultp-last-pages`}>
						<a href="#">{pages}</a>
					</li>
				)}
				<li>
					<a href="#" className={`ultp-next-page-numbers`}>
						{paginationNav == 'textArrow' ? nextText + ' ' : ''}
						{IconPack.rightAngle2}
					</a>
				</li>
			</ul>
		</div>
	);
};

const renderLoadmore = (loadMoreText) => {
	return (
		<div className={`ultp-loadmore`}>
			<a className={`ultp-loadmore-action ultp-disable-editor-click`}>
				{loadMoreText}
			</a>
		</div>
	);
};

const renderFilter = (filterText, filterType, filterValue) => {
	filterValue = filterValue.length > 2 ? filterValue : '[]';

	return (
		<div
			className={`ultp-filter-wrap ultp-disable-editor-click`}
			data-taxtype={filterType}
		>
			<ul className={`ultp-flex-menu`}>
				{filterText && (
					<li className={`filter-item`}>
						<a className="filter-active" href="#">
							{filterText}
						</a>
					</li>
				)}
				{JSON.parse(filterValue).map((v, k) => {
					let val = v.value ? v.value : v;
					return (
						<li key={k} className={`filter-item`}>
							<a href="#">
								{v?.live_title?.length > 0
									? v.live_title.replace(/-/g, ' ')
									: val.replace(/-/g, ' ')}
							</a>
						</li>
					);
				})}
			</ul>
		</div>
	);
};

const isReload = (prevAttr, attr) => {
	let reload = false;
	const query = [
		'filterShow',
		'filterType',
		'filterValue',
		'queryUnique',
		'queryNumPosts',
		'queryNumber',
		'metaKey',
		'queryType',
		'queryTax',
		'queryTaxValue',
		'queryRelation',
		'queryOrderBy',
		'queryOrder',
		/*'queryInclude',*/ 'queryExclude',
		'queryOffset',
		'queryQuick',
		'taxType',
		'taxSlug',
		'taxValue',
		'queryAuthor',
		'queryCustomPosts',
		'queryPosts',
		'queryExcludeTerm',
		'queryExcludeAuthor',
		'querySticky',
		'taxonomy',
		'fallbackImg',
		'maxTaxonomy',
	];

	for (let i = 0; i < query.length; i++) {
		if (prevAttr[query[i]] != attr[query[i]]) {
			reload = true;
			break;
		}
	}

	return reload;
};

const attrBuild = (attr) => {
	const {
		filterShow,
		filterType,
		filterValue,
		queryNumPosts,
		queryNumber,
		queryType,
		queryTax,
		queryTaxValue,
		queryOrderBy,
		queryOrder,
		queryInclude,
		queryExclude,
		queryOffset,
		metaKey,
		queryQuick,
		queryAuthor,
		queryRelation,
		querySticky,
		queryPosts,
		queryCustomPosts,
		queryExcludeTerm,
		queryExcludeAuthor,
		queryUnique,
		taxonomy,
		fallbackImg,
		maxTaxonomy,
	} = attr;

	let numPosts = queryNumber;

	if (
		typeof queryNumPosts != 'undefined' &&
		typeof queryNumber != 'undefined'
	) {
		const ultpDevice = wp.data.select('core/editor').getDeviceType?.() || 
			wp.data.select(wp.data.select('core/edit-site') ? 'core/edit-site' : 'core/edit-post').__experimentalGetPreviewDeviceType();
			
		const device = ultpDevice;
		if (
			JSON.stringify({
				lg: parseInt(queryNumPosts.lg || ''),
				sm: parseInt(queryNumPosts.sm || ''),
				xs: parseInt(queryNumPosts.xs || ''),
			}) !=
			JSON.stringify({
				lg: parseInt(queryNumber),
				sm: parseInt(queryNumber),
				xs: parseInt(queryNumber),
			})
		) {
			if (device == 'Desktop') {
				numPosts = queryNumPosts.lg;
			} else if (device == 'Tablet') {
				numPosts = queryNumPosts.sm || queryNumPosts.lg;
			} else if (device == 'Mobile') {
				numPosts = queryNumPosts.xs || queryNumPosts.lg;
			}
		}
	}

	return {
		filterShow,
		filterType,
		filterValue,
		queryNumber: numPosts,
		queryType,
		queryTax,
		queryTaxValue,
		queryOrderBy,
		queryOrder,
		queryInclude,
		queryExclude,
		queryOffset,
		metaKey,
		queryQuick,
		queryAuthor,
		queryRelation,
		querySticky,
		queryPosts,
		queryCustomPosts,
		queryExcludeTerm,
		queryExcludeAuthor,
		queryUnique,
		taxonomy,
		fallbackImg,
		maxTaxonomy,
		wpnonce: ultp_data.security,
	};
};

const renderExcerpt = (
	excerpt,
	excerpt_full,
	seo_meta,
	excerptLimit,
	showFullExcerpt,
	showSeoMeta
) => {
	return (
		<div
			className={`ultp-block-excerpt`}
			dangerouslySetInnerHTML={{
				__html: showSeoMeta
					? seo_meta.split(' ').splice(0, excerptLimit).join(' ')
					: showFullExcerpt
						? excerpt_full
						: excerpt.split(' ').splice(0, excerptLimit).join(' ') +
							'...',
			}}
		></div>
	);
};

const renderMeta = (
	meta,
	post,
	metaSeparator,
	metaStyle,
	metaMinText,
	metaAuthorPrefix,
	metaDateFormat,
	authorLink
) => {
	const authorImgOnly = meta.includes('metaAuthor') ? (
		<span className={`ultp-block-author`}>
			<img className={`ultp-meta-author-img`} src={post.avatar_url} />
		</span>
	) : (
		''
	);
	const authorImg = meta.includes('metaAuthor') ? (
		<span className={`ultp-block-author`}>
			<img className={`ultp-meta-author-img`} src={post.avatar_url} />{' '}
			{metaAuthorPrefix}{' '}
			<a href={authorLink ? post.author_link : 'javascript:void(0)'}>
				{post.display_name}
			</a>
		</span>
	) : (
		''
	);
	const authorIcon = meta.includes('metaAuthor') ? (
		<span className={`ultp-block-author`}>
			{IconPack.user}
			<a href={authorLink ? post.author_link : 'javascript:void(0)'}>
				{post.display_name}
			</a>
		</span>
	) : (
		''
	);
	const authorBy = meta.includes('metaAuthor') ? (
		<span className={`ultp-block-author`}>
			{metaAuthorPrefix}
			<a href={authorLink ? post.author_link : 'javascript:void(0)'}>
				{post.display_name}
			</a>
		</span>
	) : (
		''
	);
	const date = meta.includes('metaDate') ? (
		<span className={`ultp-block-date`}>
			{dateI18n(_dateFormat(metaDateFormat), post.time)}
		</span>
	) : (
		''
	);
	const dateIcon = meta.includes('metaDate') ? (
		<span className={`ultp-block-date`}>
			{IconPack.calendar}
			{dateI18n(_dateFormat(metaDateFormat), post.time)}
		</span>
	) : (
		''
	);
	const dateModified = meta.includes('metaDateModified') ? (
		<span className={`ultp-block-date`}>
			{dateI18n(_dateFormat(metaDateFormat), post.timeModified)}
		</span>
	) : (
		''
	);
	const dateModifiedIcon = meta.includes('metaDateModified') ? (
		<span className={`ultp-block-date`}>
			{IconPack.calendar}
			{dateI18n(_dateFormat(metaDateFormat), post.timeModified)}
		</span>
	) : (
		''
	);
	const comments = meta.includes('metaComments') ? (
		<span className={`ultp-post-comment`}>
			{post.comments === 0
				? post.comments + __(' comment', 'ultimate-post')
				: post.comments + __(' comments', 'ultimate-post')}
		</span>
	) : (
		''
	);
	const commentsIcon = meta.includes('metaComments') ? (
		<span className={`ultp-post-comment`}>
			{IconPack.comment}
			{post.comments}
		</span>
	) : (
		''
	);
	const view = meta.includes('metaView') ? (
		<span className={`ultp-post-view`}>
			{post.view == 0 ? '0 view' : post.view + ' views'}
		</span>
	) : (
		''
	);
	const viewIcon = meta.includes('metaView') ? (
		<span className={`ultp-post-view`}>
			{IconPack.eye}
			{post.view}
		</span>
	) : (
		''
	);
	const postTime = meta.includes('metaTime') ? (
		<span className={`ultp-post-time`}>
			{post.post_time} {__('ago', 'ultimate-post')}
		</span>
	) : (
		''
	);
	const postTimeIcon = meta.includes('metaTime') ? (
		<span className={`ultp-post-time`}>
			{IconPack.clock}
			{post.post_time} {__('ago', 'ultimate-post')}
		</span>
	) : (
		''
	);
	const reading = meta.includes('metaRead') ? (
		<span className={`ultp-post-read`}>
			{post.reading_time} {metaMinText}
		</span>
	) : (
		''
	);
	const readingIcon = meta.includes('metaRead') ? (
		<span className={`ultp-post-read`}>
			{IconPack.book}
			{post.reading_time} {metaMinText}
		</span>
	) : (
		''
	);
	return (
		<div
			className={`ultp-block-meta ultp-block-meta-${metaSeparator} ultp-block-meta-${metaStyle}`}
		>
			{metaStyle == 'noIcon' && (
				<Fragment>
					{' '}
					{authorBy} {date} {dateModified} {comments} {view} {reading}{' '}
					{postTime}
				</Fragment>
			)}
			{metaStyle == 'icon' && (
				<Fragment>
					{' '}
					{authorIcon} {dateIcon} {dateModifiedIcon} {commentsIcon}{' '}
					{viewIcon} {postTimeIcon} {readingIcon}
				</Fragment>
			)}
			{metaStyle == 'style2' && (
				<Fragment>
					{' '}
					{authorBy} {dateIcon} {dateModifiedIcon} {commentsIcon}{' '}
					{viewIcon} {postTimeIcon} {readingIcon}
				</Fragment>
			)}
			{metaStyle == 'style3' && (
				<Fragment>
					{' '}
					{authorImg} {dateIcon} {dateModifiedIcon} {commentsIcon}{' '}
					{viewIcon} {postTimeIcon} {readingIcon}
				</Fragment>
			)}
			{metaStyle == 'style4' && (
				<Fragment>
					{' '}
					{authorIcon} {dateIcon} {dateModifiedIcon} {commentsIcon}{' '}
					{viewIcon} {postTimeIcon} {readingIcon}
				</Fragment>
			)}
			{metaStyle == 'style5' && (
				<Fragment>
					{' '}
					<div className={`ultp-meta-media`}>
						{authorImgOnly}
					</div>{' '}
					<div className={`ultp-meta-body`}>
						{authorBy} {dateIcon} {dateModifiedIcon} {commentsIcon}{' '}
						{viewIcon} {postTimeIcon} {readingIcon}
					</div>
				</Fragment>
			)}
			{metaStyle == 'style6' && (
				<Fragment>
					{' '}
					{authorImgOnly} {authorBy} {dateIcon} {dateModifiedIcon}{' '}
					{commentsIcon} {viewIcon} {postTimeIcon} {readingIcon}
				</Fragment>
			)}
		</div>
	);
};

const renderCategory = (
	post,
	catShow,
	catStyle,
	catPosition,
	customCatColor,
	onlyCatColor
) => {
	return post.category && catShow ? (
		<div
			className={`ultp-category-grid ultp-category-${catStyle} ultp-category-${catPosition}`}
		>
			<div
				className={`ultp-category-in ultp-cat-color-${customCatColor}`}
			>
				{post.category.map((v, k) =>
					customCatColor ? (
						onlyCatColor ? (
							<a
								key={k}
								className={`ultp-cat-${v.slug}`}
								style={{ color: v.color || '#CE2746' }}
							>
								{v.name}
							</a>
						) : (
							<a
								key={k}
								className={`ultp-cat-${v.slug} ultp-cat-only-color-${customCatColor}`}
								style={{
									backgroundColor: v.color || '#CE2746',
								}}
							>
								{v.name}
							</a>
						)
					) : (
						<a key={k} className={`ultp-cat-${v.slug}`}>
							{v.name}
						</a>
					)
				)}
			</div>
		</div>
	) : (
		''
	);
};

const renderReadmore = (readMoreText, readMoreIcon, titleLabel = '') => {
	return (
		<div className="ultp-block-readmore">
			<a aria-label={titleLabel}>
				{readMoreText ? readMoreText : __('Read More', 'ultimate-post')}
				{IconPack[readMoreIcon]}
			</a>
		</div>
	);
};

const renderTitle = (title, headingTag, titleLength, titleStyle) => {
	const Tag = `${headingTag}`;
	if (title && titleLength != 0) {
		let arr = title.split(' ');
		title =
			arr.length > titleLength
				? arr.splice(0, titleLength).join(' ') + '...'
				: title;
	}
	return (
		<Tag
			className={`ultp-block-title ${titleStyle === 'none' ? '' : `ultp-title-${titleStyle}`} `}
		>
			<a dangerouslySetInnerHTML={{ __html: title }} />
		</Tag>
	);
};

const titleAnimationArg = [
	{
		position: 1,
		data: {
			type: 'select',
			key: 'titleAnimation',
			label: __('Content Animation', 'ultimate-post'),
			options: [
				{ value: '', label: '- None -' },
				{
					value: 'slideup',
					label: __('Slide Up', 'ultimate-post'),
					pro: true,
				},
				{
					value: 'slidedown',
					label: __('Slide Down', 'ultimate-post'),
					pro: true,
				},
			],
		},
	},
];

const renderList = (data, layout) => {
	const Tag = layout == 'style2' ? 'ol' : 'ul';
	if (data) {
		data = typeof data == 'string' ? JSON.parse(data) : data;
		return (
			data.length > 0 && (
				<Tag className={`ultp-toc-lists`}>
					{data.map((post, k) => {
						return (
							<li key={k}>
								<a href={`#${post.link}`}>{post.content}</a>
								{post.child && renderList(post.child, layout)}
							</li>
						);
					})}
				</Tag>
			)
		);
	}
};

const metaListPro = { ...metaList, pro: true };
const metaStylePro = {
	type: 'select',
	key: 'metaStyle',
	label: __('Meta Style', 'ultimate-post'),
	options: [
		{ value: 'noIcon', label: __('No Icon', 'ultimate-post') },
		{ value: 'icon', label: __('With Icon', 'ultimate-post') },
		{ value: 'style2', label: __('Style2', 'ultimate-post') },
		{ value: 'style3', label: __('Style3', 'ultimate-post') },
		{ value: 'style4', label: __('Style4', 'ultimate-post') },
		{ value: 'style5', label: __('Style5', 'ultimate-post') },
		{ value: 'style6', label: __('Style6', 'ultimate-post') },
	],
	pro: true,
};
const metaSeparatorPro = {
	type: 'select',
	key: 'metaSeparator',
	label: __('Separator', 'ultimate-post'),
	options: [
		{ value: 'dot', label: __('Dot', 'ultimate-post') },
		{ value: 'slash', label: __('Slash', 'ultimate-post') },
		{ value: 'doubleslash', label: __('Double Slash', 'ultimate-post') },
		{ value: 'close', label: __('Close', 'ultimate-post') },
		{ value: 'dash', label: __('Dash', 'ultimate-post') },
		{ value: 'verticalbar', label: __('Vertical Bar', 'ultimate-post') },
		{ value: 'emptyspace', label: __('Empty', 'ultimate-post') },
	],
	pro: true,
};
const metaListSmallPro = {
	...metaList,
	key: 'metaListSmall',
	label: __('Small Item Meta', 'ultimate-post'),
	pro: true,
};
const catPositionPro = {
	type: 'select',
	key: 'catPosition',
	label: __('Category Position', 'ultimate-post'),
	options: [
		{ value: 'aboveTitle', label: __('Above Title', 'ultimate-post') },
		{
			value: 'topLeft',
			label: __('Over Image(Top Left)', 'ultimate-post'),
		},
		{
			value: 'topRight',
			label: __('Over Image(Top Right)', 'ultimate-post'),
		},
		{
			value: 'bottomLeft',
			label: __('Over Image(Bottom Left)', 'ultimate-post'),
		},
		{
			value: 'bottomRight',
			label: __('Over Image(Bottom Right)', 'ultimate-post'),
		},
		{
			value: 'centerCenter',
			label: __('Over Image(Center)', 'ultimate-post'),
		},
	],
	pro: true,
};
const filterTypePro = {
	type: 'select',
	key: 'filterType',
	label: __('Filter Type', 'ultimate-post'),
	options: [],
	pro: true,
};
const filterValuePro = {
	type: 'select',
	key: 'filterValue',
	label: __('Filter Value', 'ultimate-post'),
	options: [],
	multiple: true,
	pro: true,
};
const navPositionPro = {
	type: 'select',
	key: 'navPosition',
	label: __('Navigation Position', 'ultimate-post'),
	options: [
		{ value: 'topRight', label: __('Top Right', 'ultimate-post') },
		{ value: 'bottomLeft', label: __('Bottom', 'ultimate-post') },
	],
	pro: true,
};

const bgVideoBg = (
	videoUrl = '',
	loop = true,
	start = 0,
	end = 0,
	fallback = ''
) => {
	if (videoUrl) {
		let src = '';
		if (videoUrl.includes('youtu')) {
			const regex = /youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/gm;
			const match = regex.exec(videoUrl);
			if (match && match[1]) {
				src =
					'//www.youtube.com/embed/' +
					match[1] +
					'?playlist=' +
					match[1] +
					'&iv_load_policy=3&controls=0&autoplay=1&disablekb=1&rel=0&enablejsapi=1&showinfo=0&wmode=transparent&widgetid=1&playsinline=1&mute=1';
				src += '&loop=' + (loop ? 1 : 0);
				src += start ? '&start=' + start : '';
				src += end ? '&end=' + end : '';
			}
		} else if (videoUrl.includes('vimeo')) {
			const result = videoUrl.match(
				/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/i
			);
			if (result[1]) {
				src =
					'//player.vimeo.com/video/' +
					result[1] +
					'?autoplay=1&title=0&byline=0&portrait=0&transparent=0&background=1';
				src += '&loop=' + (loop ? 1 : 0);
			}
		} else {
			return (
				<div className="ultp-rowbg-video">
					<video
						className="ultp-bgvideo"
						poster={fallback && fallback}
						muted
						loop
						autoPlay
					>
						<source src={videoUrl} />
					</video>
				</div>
			);
		}
		if (src) {
			return (
				<div className="ultp-rowbg-video">
					<iframe src={src} frameBorder="0" allowFullScreen></iframe>
				</div>
			);
		}
		return fallback ? (
			<img src={fallback} alt="Video Fallback Image" />
		) : (
			''
		);
	}
	return fallback ? <img src={fallback} alt="Video Fallback Image" /> : '';
};

const updateChildAttr = (clientId) => {
	const innerBlocks = wp.data.select('core/block-editor').getBlocks(clientId);

	if (innerBlocks.length > 0) {
		innerBlocks.forEach((el, _i) => {
			dispatch('core/block-editor').updateBlockAttributes(el.clientId, {
				updateChild: el.attributes.updateChild ? false : true,
			}); // no need to initialize 'updateChild' key
		});
	}
};
const updateChildAttr2 = (clientId, updatingAttr='') => {
    const innerBlocks  = wp.data.select( 'core/block-editor' ).getBlocks(clientId);
    
    if (innerBlocks.length > 0) {
        innerBlocks.forEach((el, _i) => {
            dispatch( 'core/block-editor' ).updateBlockAttributes(el.clientId, updatingAttr ? updatingAttr : { updateChild: el.attributes.updateChild ? false : true } ); // no need to initialize 'updateChild' key
        });
    }
}

const getWindowDocument = () => {
	const editor_canvas = window.document.getElementsByName('editor-canvas');
	return editor_canvas[0]?.contentDocument
		? editor_canvas[0]?.contentDocument
		: window.document;
};

const getCustomFonts = (styleCss = '', fontFace = false) => {
	if (ultp_data.settings?.ultp_custom_font != 'true') {
		return [];
	}
	const customFonts = ultp_data.custom_fonts;
	const customFontLists = [];
	let fonts = '';

	customFonts?.forEach((item) => {
		const weight = [];

		item.font.forEach((f) => {
			const fontSrc = [];
			weight.push(f.weight);

			if (styleCss.includes(item.title)) {
				if (f.woff) {
					fontSrc.push(`url(${f.woff}) format('woff')`);
				}
				if (f.woff2) {
					fontSrc.push(`url(${f.woff2}) format('woff2')`);
				}
				if (f.ttf) {
					fontSrc.push(`url(${f.ttf}) format('TrueType')`);
				}
				if (f.svg) {
					fontSrc.push(`url(${f.svg}) format('svg')`);
				}
				if (f.eot) {
					fontSrc.push(`url(${f.eot}) format('eot')`);
				}
				fonts += ` @font-face {
                    font-family: "${item.title}";
                    font-weight: ${f.weight};
                    font-display: auto;
                    src: ${fontSrc.join(', ')};
                } `;
			}
		});
		customFontLists.push({ n: item.title, v: weight, f: '' });
	});
	if (fontFace) {
		return fonts + styleCss;
	}
	return customFontLists || [];
};

const isInlineCSS = () => {
	const url = window.location.href;
	if (typeof url != 'undefined') {
		if (
			url.indexOf('path=%2Fpatterns') !== -1 &&
			url.indexOf('site-editor.php') > -1
		) {
			return true;
		}
	}
	return false;
};

const updateCurrentPostId = (setAttributes, reference, currPostId, clientId ) => {
    const settingAttributes = ( val, src ) => {
        if ( val && ( val != currPostId ) ) {
            if ( src === 'wp_block' ) {
                // wp.data.dispatch( 'core/block-editor' ).updateBlockAttributes(clientId, { currentPostId: val?.toString() });
            } else {
                setAttributes( { currentPostId: val?.toString() } )
            }
        }
    }

    const __editorPostId = wp.data.select('core/editor')?.getCurrentPostId() ;
    const fseEditor = wp.data.select('core/edit-site');
    const _fsetype = fseEditor?.getEditedPostType() || '';

    if(reference?.hasOwnProperty('ref')) {
        settingAttributes(reference.ref, 'wp_block');
    } else if(document.querySelector('.widgets-php')) {
        settingAttributes('ultp-widget', 'widget');
    } else if( (_fsetype == 'wp_template_part' || _fsetype == 'wp_template') ) {
        const currentTime = new Date().getTime();
        const timeout = reloadedTime && ( ( (currentTime - reloadedTime) / 1000 ) >= 4 ) ? 0 : 1500;

        setTimeout(() => {
            const fseTemplates = wp.data.select( 'core' )?.getEntityRecords( 'postType', 'wp_template' , {per_page: -1} ) || [];
            const fseTemplateParts = wp.data.select( 'core' )?.getEntityRecords( 'postType', 'wp_template_part' , {per_page: -1} ) || [];
            const fseTemplatePartsData = getAllTemplatePartsInPage();
            let partClientId = '';

            if ( fseTemplatePartsData.hasItems ) {
                partClientId = Object.keys(fseTemplatePartsData).find(item => {
                    return fseTemplatePartsData[item]?.includes(clientId); 
                })
            }
            if ( partClientId ) {
                const partAttr = wp.data.select('core/block-editor').getBlockAttributes(partClientId);
                const fsePart = fseTemplateParts.find(item => item.id.includes('//'+ partAttr?.slug) );
                settingAttributes(fsePart?.wp_id, 'fse-part');
            } else {
                if ( typeof __editorPostId == 'string' && __editorPostId.includes('//') )  {
                    const currentTemplate = fseEditor?.getEditedPostId();
                    const fseTemplate = ( _fsetype == 'wp_template' ? fseTemplates : fseTemplateParts ).find(item => item.id == currentTemplate );
                    settingAttributes(fseTemplate?.wp_id, _fsetype == 'wp_template' ? 'fse-template' : 'fse-part');
                } else {
                    settingAttributes(__editorPostId, '__editorPostId_fse');
                }
            }
        }, timeout);
    } else if( __editorPostId ) {
        settingAttributes(__editorPostId, '__editorPostId');
    }
}

const blockSupportLink = () => {
	if (ultp_data.active) {
		return (
			<div className="ultp-editor-support">
				<div className="ultp-editor-support-content">
					<img
						alt="site logo"
						className="logo"
						src={ultp_data.url + `assets/img/logo-option.svg`}
					/>
					<div className="descp">
						{__('Need quick Human Support?', 'ultimate-post')}
					</div>
					<a
						href="https://www.wpxpo.com/contact?utm_source=db-postx-editor&utm_medium=blocks-support&utm_campaign=postx-dashboard"
						target="_blank"
					>
						{__('Get Support', 'ultimate-post')}
					</a>
				</div>
			</div>
		);
	}

	const text =
		'Unlock all features, blocks, templates, and customization options at a single price.';
	const link =
		'https://www.wpxpo.com/postx/pricing/?utm_source=db-postx-editor&utm_medium=blocks-upgrade&utm_campaign=postx-dashboard';

	return (
		<div className="ultp-editor-support">
			<div className="ultp-editor-support-content">
				<div className="title">
					{__('Upgrade to PostX Pro', 'ultimate-post')}
				</div>
				<div className="descp">{__(text, 'ultimate-post')}</div>
				<a href={link} target="_blank">
					{__('Upgrade Now', 'ultimate-post')}
				</a>
			</div>
		</div>
	);
};

const CustomHtmlTag = ({ tag, children, ...props }) => {
    const TagName = tag;
    return <TagName {...props}>{children}</TagName>;
}

const menuDropIcons = [
    "angle_bottom_left_line",
    "angle_bottom_right_line",
    "angle_top_left_line",
    "angle_top_right_line",
    "leftAngle",
    "rightAngle",
    "leftAngle2",
    "rightAngle2",
    "collapse_bottom_line",
    "arrowUp2",
    "longArrowUp2",
    "arrow_left_circle_line",
    "arrow_bottom_circle_line",
    "arrow_right_circle_line",
    "arrow_top_circle_line",
    "arrow_down_line",
    "leftArrowLg",
    "rightArrowLg",
    "arrow_up_line",
    "down_solid",
    "right_solid",
    "left_solid",
    "up_solid",
    "bottom_right_line",
    "bottom_left_line",
    "top_left_angle_line",
    "top_right_line"
];

export {
    _dateFormat,
    // Adv Filter
    AdvFilterArgs,
    ArrowContent,
    attrBuild,
    bgVideoBg,
    blockSupportLink,
    ButtonStyle,
    CategoryStyle,
    catPositionPro,
    CommonSettings,
    CounterStyle,
    CustomCssAdvanced,
    DescStyle,
    Design,
    DotStyle,
    ExcerptStyle,
    FilterContent,
    filterTypePro,
    filterValuePro,
    GeneralAdvanced,
    GeneralContent,
    GeneralContentWithQuery,
    getCustomFonts,
    getWindowDocument,
    HeadingContent,
    imageSize,
    ImageStyle,
    // Functions
    isInlineCSS,
    isReload,
    Layout,
    // Pro Fields
    metaListPro,
    metaListSmallPro,
    metaSeparatorPro,
    MetaStyle,
    metaStylePro,
    navPositionPro,
    OverlayStyle,
    PaginationContent,
    PrefixStyle,
    QueryContent,
    quickQuery,
    // Content & Style
    ReadMoreStyle,
    renderCategory,
    renderExcerpt,
    renderFilter,
    renderList,
    renderLoadmore,
    renderMeta,
    renderNavigation,
    renderPagination,
    renderReadmore,
    // Render
    renderTitle,
    ResponsiveAdvanced,
    SeparatorStyle,
    // Settings
    SliderSetting,
    // Taxonomy Query
    TaxQuery,
    TaxWrapStyle,
    titleAnimationArg,
    TitleStyle,
    ToolbarSettings,
    updateChildAttr,
    updateCurrentPostId,
    VideoStyle,
    WrapHeadingStyle,
    WrapStyle,

	CustomHtmlTag,
	ToolbarCommonSettings,
	menuDropIcons
};

