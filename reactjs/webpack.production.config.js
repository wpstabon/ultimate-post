'use strict';

var path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

var config = {
	mode: 'production',
	module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: { loader: 'babel-loader' }
            },
			{
				test: /\.(s(a|c)ss)$/,
				use: ['style-loader','css-loader', 'sass-loader']
			}
        ]
    },
	optimization: {
		minimize: true,
		concatenateModules:false,
		minimizer: [new TerserPlugin({
            parallel: true,
				terserOptions: {
					output: {
						comments: /translators:/i,
					},
					compress: {
						passes: 2,
					},
					mangle: {
						reserved: [ '__', '_n', '_nx', '_x' ],
					},
				},
				extractComments: true,
        })]
	}
};

var mainExport = Object.assign({}, config, {
	entry: {
		'./assets/js/editor.blocks' : './src/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

var builderCondition = Object.assign({}, config, {
	entry: {
		'./addons/builder/assets/js/conditions' : './src/conditions/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

var initialSetup = Object.assign({}, config, {
	entry: {
		'./assets/js/ultp_initial_setup_min' : './src/initial_setup/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

var ultpDashboard = Object.assign({}, config, {
	entry: {
		'./assets/js/ultp_dashboard_min' : './src/dashboard/index.js'
	},
	output: {
		path: path.join(__dirname, '../'),
		filename: '[name].js',
	},
});

module.exports = [
    mainExport, builderCondition, initialSetup, ultpDashboard
];